#!/usr/bin/env bash
# vim: set et sw=2:

set -eu
set -o pipefail

tab="$(printf '\t')"
readonly tab
cr="$(printf '\r')"
readonly cr
lf='
'
readonly lf


# Default IFS
IFS=" ${tab}${lf}"

is_verbose=0

die() {
    local code="$1"
    shift
    printf %s\\n "$@" >&2
    exit "${code}"
}
unreachable() {
  local msg="unreachable:${1+" "}${1-}"
  if [ "${BASH_VERSION+set}" = set ]; then
    printf '%s\n' "$0: ${msg}" "stack trace:" >&2
    # shellcheck disable=SC3028,SC3054
    local i=1 n="${#BASH_LINENO[@]}" linenos
    eval 'linenos=("${LINENO}" "${BASH_LINENO[@]}")' # In dash this is a parsing error without eval.
    while [ "${i}" -lt "${n}" ]; do
      # shellcheck disable=SC3054,SC3028
      printf '%s:%s: %s\n' \
        "${BASH_SOURCE[${i}]}" \
        "${linenos[${i}]}" \
        "${FUNCNAME[${i}]}" >&2
      i="$((i + 1))"
    done
    exit 1
  else
    die 1 "$0: ${msg}"
  fi
}
log() {
  test "$#" -eq 1 || unreachable "bad arg count"
  if [ "$is_verbose" = 1 ]; then
    printf %s\\n "$1" >&2
  fi
}
die_if_is_bad_dir() {
    local check="$1"
    local name="$2"
    local bad=""
    case "${check}" in
        (-*) bad="starts with '-'" ;;
        (*"${cr}"*) bad="contains '\\r'" ;;
        (*"${lf}"*) bad="contains '\\n'" ;;
        (*"${tab}"*) bad="contains '\\t'" ;;
        (*" "*) bad="contains ' '" ;;
        (*) ;;
    esac
    if [ -n "${bad}" ]; then
        die 1 "$0: error: ${name} ${bad}"
    fi
}
echo_vmsg_and_run() {
  if [ "${is_verbose}" = 1 ]; then
    echo + "${@@Q}"
  else
    echo + "$1"
  fi
  shift
  "$@" || {
    local exit="$?"
    echo " command failed with code $exit"
    return "$exit"
  }
}

declare -A running_pids
running_pids=( )
job_error_count=0
readonly max_job_error_count=1
max_enqueued="$(nproc || unreachable)"
readonly max_enqueued
need_one_job() {
  [ "${#running_pids[@]}" -ge "$max_enqueued" ] || return 0
  local pid
  # Wait for one process to end.
  wait -p pid -n "${!running_pids[@]}" || {
    job_error_count=$((job_error_count + 1))
    log "command failed! pid $pid"
  }
  unset -v "running_pids[$pid]"
}
async_run() {
  need_one_job
  "$@" &
  local pid="$!"
  log "pid $pid cmd ${*@Q}"
  running_pids["$pid"]=""
}
wait_all_running_pids() {
  local pid
  for pid in "${!running_pids[@]}"; do
    wait -n "${pid}" || {
      job_error_count=$((job_error_count + 1))
      log "command failed! pid $pid"
    }
  done
  running_pids=()
}

readonly build_dir="."
die_if_is_bad_dir "${build_dir}" "Build directory"
readonly build_dir_cache="${build_dir}/.cache.testing.dir"

# Get the root directory of the project
root_dir="$(dirname "$0")"
readonly root_dir
die_if_is_bad_dir "${root_dir}" "Project root directory"

test -n "${root_dir}" || die 1 "$0: error: \$root_dir is empty??"
test -d "${root_dir}" || die 1 "$0: error: \$root_dir is not a directory??"

if [ "${1-}" = "--verbose" ]; then
  shift
  is_verbose=1
fi
readonly is_verbose

test "$#" -eq 0 || die 1 "$0: error: expected zero arguments, got $#"

declare -A cxx_flags_for_file
cxx_flags_for_file=()

# https://github.com/bombela/backward-cpp/blob/master/backward.hpp
if test -f "${root_dir}/backward/backward.cpp"; then
  backward_sources=( backward/backward.cpp )
  # sudo apt install libdw-dev
  backward_libs=( -ldw )
  cxx_flags_for_file["backward/backward.cpp"]="-DBACKWARD_HAS_DW -w -I${root_dir}/backward"
else
  backward_sources=()
  backward_libs=()
fi

# shellcheck disable=SC2207
sources=(
  $(find "${root_dir}/src/yy" -name '*.cpp' -printf src/yy/%P\\n)
  testing.cpp
  "${backward_sources[@]}"
)

if test "${CXX+set}" = set; then
  #shellcheck disable=SC2206
  cxx_compiler=( $CXX )
else
  # cxx_compiler=(zig c++)
  # cxx_compiler=(ccache clang++-17)
  cxx_compiler=(ccache clang++-18)
  # cxx_compiler=(clang++-18)
  # cxx_compiler=(ccache g++-12)
  # cxx_compiler=(ccache /nix/store/4cjqvbp1jbkps185wl8qnbjpf8bdy8j9-gcc-wrapper-13.2.0/bin/g++)
fi
readonly cxx_compiler

compiler_version="$($cxx_compiler --version | head -n1)"

is_clang=0
is_gcc=0
case "$compiler_version" in
  (*"clang"*) is_clang=1 ;;
  (*"g++"*) is_gcc=1 ;;
esac
readonly is_clang is_gcc

cxx_flags=(
  -Wall -Wextra -Wshadow -Wunused -Wunused-parameter -Wswitch -Werror=conversion -Werror=sign-conversion
  -Wpedantic
  -std=c++20
  -I../src/
  -ggdb3 -UNDEBUG -O0 -D_LIBCPP_HARDENING_MODE=_LIBCPP_HARDENING_MODE_FAST -D_GLIBCXX_ASSERTIONS=1
  -fno-permissive
  -fsanitize=undefined
)
link_flags=()
link_libs=( "${backward_libs[@]}" )

if [ "$is_gcc" = 1 ]; then
  cxx_flags+=( -Werror=write-strings )
elif [ "$is_clang" = 1 ]; then
  cxx_flags+=( -Werror=writable-strings )
  cxx_flags+=( -fsanitize=unsigned-integer-overflow )
fi

cache_objs=()
for s in "${sources[@]}"; do
  # If there were errors, stop launching processes.
  [ "${job_error_count}" -lt "${max_job_error_count}" ] || break
  # Make the object name and make the directory.
  o="${build_dir_cache}/obj/${s//"/"/.dir-}.o"
  mkdir -p "$(dirname "${o}")"
  cache_objs+=("${o}")
  # shellcheck disable=SC2086
  async_run echo_vmsg_and_run "testing CXX     ${s}" \
    "${cxx_compiler[@]}" "${cxx_flags[@]}" ${cxx_flags_for_file["${s}"]-} -c "${root_dir}/${s}" -o "${o}"
done

wait_all_running_pids
if [ "${job_error_count}" -gt 0 ]; then
  log "had errors. stopping."
  exit 1
fi

echo_vmsg_and_run "testing CXXLINK testing" \
  "${cxx_compiler[@]}" "${cxx_flags[@]}" "${link_flags[@]}" "${cache_objs[@]}" "${link_libs[@]}" -o "${build_dir}/testing"

test -e testing-gdb.py || ln -s yuceyuf-gdb.py testing-gdb.py
