#!/bin/sh
# vim: set sw=4 et:

# This file will be sourced from

# shellcheck disable=SC3043 # Enable `local` variables.
# shellcheck disable=SC2155
# shellcheck enable=require-variable-braces

# TODO: Copy stdlib files if `--hardcode-stdlib-path` was not provided for `configure`

set -eu

readonly install_dollar_0="$0 install"

# make shellcheck shut up
if false; then
    # root_dir=""
    build_dir=""
fi

cmd_install() {
    echo_install_usage() {
        cat <<EOF
Usage: ${install_dollar_0} [--option=value] --destination=DIR

Description: Install built files into the destination.

Options:
  --uninstall           (optional): Uninstall instead of installing
  --destination=DIR     (required): The ‘/usr’-like installation directory. This
                                    can be a temporary directory. Paths are not hardcoded.
  --strip=...           (optional): "yes" (default), "no"

Environment variables:
  STRIP                         The strip program (defaults to 'strip')
Environment variables can also be set on the command line. The command line takes priority over the environment.
EOF
    }


    : "${STRIP:=""}"
    export STRIP

    local destination=""
    local do_strip="yes"
    local do_uninstall="no"
    local manifest_file=".install_manifest"
    while [ "$#" -gt 0 ]; do
        case "$1" in
            (-h|--help) echo_install_usage; exit 0 ;;
            (--uninstall) do_uninstall="yes" ;;
            (--destination=*) destination="$(get_val "$1")" ;;
            (--strip=*)
                do_strip="$(get_val "$1")"
                case "${do_strip}" in
                    (yes|no) ;;
                    (*) die 1 "${install_dollar_0}: error: invalid --strip value: ${do_strip}" "$(echo_install_usage)" ;;
                esac
                ;;
            (STRIP=*) STRIP="$(get_val "$1")" ;;
            (*=) die 1 "${install_dollar_0}: error: ‘$1’ is not supported" "$(echo_install_usage)" ;;
            (*) die 1 "${install_dollar_0}: error: Unknown argument: ‘$1’" "$(echo_install_usage)" ;;
        esac
        shift
    done

    [ -n "${destination}" ] || die 1 "${install_dollar_0}: error: No --destination" "$(echo_install_usage)"
    # destination must be a full path
    case "${destination}" in
        (/*) ;;
        (*) destination="$(pwd)/${destination}" ;;
    esac
    readonly destination

    [ -f "${build_dir}/.cfg.sh" ] || die 1 "${install_dollar_0}: error: ${build_dir} is not a valid build directory"
    # shellcheck disable=SC2016 disable=SC1091
    . "${build_dir}/.cfg.sh"
    : "${configured_exe_suffix?}"


    echo "- destination is ${destination}"
    echo "- build_dir is ${build_dir}"
    if [ "${do_strip}" = yes ]; then
        # default STRIP value
        if [ ! "${STRIP:+set_and_not_empty}" ]; then
            # If CC is provided, STRIP doesn't have a default value.
            if [ "${CC+set}" ] && [ "${CC}" != "cc" ]; then
                die 1 "${install_dollar_0}: error: STRIP is required when CC is provided. See '${install_dollar_0} --help' for how to set STRIP."
            fi
            STRIP="strip"
        fi
        echo "- STRIP is '${STRIP}'"
    else
        echo "- no strip"
    fi

    mkdirs_cp_chmod() {
        local mode="$1" dir="$2" file="$3"

        local dest_file
        dest_file="${dir}/$(basename "${file}")"

        echo "Install '${dest_file}'"

        mkdir -p -- "${dir}"
        cp -- "${file}" "${dir}/"
        printf '%s\0' "${dest_file}" >> "${manifest_file}"
        chmod "${mode}" -- "${dest_file}"
    }

    strip_binaries_flags="--strip-all"
    # strip_shared_flags="--strip-unneeded"
    # strip_static_flags="--strip-debug"

    mkdirs_cp_chmod_strip_bin() {
        local mode="$1" dir="$2" file="$3"
        mkdirs_cp_chmod "${mode}" "${dir}" "${file}"

        local dest_file
        dest_file="${dir}/$(basename "${file}")"

        if [ "${do_strip}" = yes ]; then
            echo "Strip '${dest_file}'"
            # shellcheck disable=SC2250
            ${STRIP} ${strip_binaries_flags} -- "${dest_file}"
        fi
    }

    echo
    case "${do_uninstall}" in
        (no)
            # install
            # Should we uninstall the old one first?
            truncate --size=0 -- "${manifest_file}"
            mkdirs_cp_chmod_strip_bin 755 "${destination}/bin" "${build_dir}/yuceyuf${configured_exe_suffix}"
            # find "${root_dir/stdlib}" -type f -printf '%P\0' | xargs -0 -I{} "${root_dir}/scripts/lib/mkdirs_cp_chmod" 755 "${destination}/lib/yuceyuf/" "{}" # TODO
            ;;
        (yes)
            # uninstall
            if [ -r "${manifest_file}" ]; then
                xargs -0 -I{} rm -fv -- '{}' < "${manifest_file}"
            else
                die 1 "'${manifest_file}' not found"
            fi
            rm -- "${manifest_file}"
            ;;
        (*) unreachable ;;
    esac

    exit 0
}
