#if 0
set -ex
tmp_out="$0.out"
zig cc -fno-sanitize=undefined -std=gnu11 -Wall -Wextra -Werror -Wconversion -Wshadow -Wuninitialized \
    -Wswitch -Winit-self -g3 -UNDEBUG "$0" -o "${tmp_out}"
exec "${tmp_out}" "$@"
#endif


// TODO: Generate local variables. When using a variable, randomly choose a local or global.

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>


#define ARRAY_COUNT(x) (sizeof((x))/sizeof((x)[0]))
#define CLAMP(x, min, max) ((x) < (min) ? (min) : ((x) > (max) ? (max) : (x)))

typedef size_t usize;
#define unreachable() do { __builtin_unreachable(); } while (false)


static void init_random(void)
{
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
    srand((unsigned int)(tp.tv_nsec) + (unsigned int)(tp.tv_sec));
}

static usize get_random(usize max_exclusive)
{
    return (usize)rand() % max_exclusive;
}

typedef struct {
    usize functions_len;
    usize global_vars_len;
    usize indent;
    usize stmt_depth;
    usize total_stmt_count;
} Program;

static Program p;
static usize const max_stmt_depth = 20;
static usize const max_total_stmt_count = 50000;
static usize block_random_max = 5;

static void indent(void)
{
    printf("%*s", (int)(p.indent * 2), "");
}

typedef enum {
    StmtKind__inc_global_var0,
    StmtKind__inc_global_var1,
    StmtKind__inc_global_var2,
    StmtKind__inc_global_var3,
    StmtKind__inc_global_var4,
    StmtKind__inc_global_var5,
    StmtKind__inc_global_var6,
    StmtKind__inc_global_var7,
    StmtKind__inc_global_var8,
    StmtKind__inc_global_var9,
    StmtKind__inc_global_var10,
    StmtKind__inc_global_var11,
    StmtKind__inc_global_var12,
    StmtKind__inc_global_var13,
    StmtKind__inc_global_var14,
    StmtKind__inc_global_var15,
    StmtKind__inc_global_var16,
    StmtKind__inc_global_var17,
    StmtKind__inc_global_var18,
    StmtKind__inc_global_var19,

    StmtKind__if,
    StmtKind__if_else,
    StmtKind__block,
    StmtKind__COUNT,
} StmtKind;


static void generate_stmt(void)
{
    if (p.total_stmt_count++ > max_total_stmt_count)
        return;
    if (p.stmt_depth > max_stmt_depth)
        return;
    p.stmt_depth += 1;
    switch ((StmtKind)get_random(StmtKind__COUNT)) {
        case StmtKind__COUNT:
            unreachable();
        case StmtKind__inc_global_var0:
        case StmtKind__inc_global_var1:
        case StmtKind__inc_global_var2:
        case StmtKind__inc_global_var3:
        case StmtKind__inc_global_var4:
        case StmtKind__inc_global_var5:
        case StmtKind__inc_global_var6:
        case StmtKind__inc_global_var7:
        case StmtKind__inc_global_var8:
        case StmtKind__inc_global_var9:
        case StmtKind__inc_global_var10:break;
        case StmtKind__inc_global_var11:
        case StmtKind__inc_global_var12:
        case StmtKind__inc_global_var13:
        case StmtKind__inc_global_var14:
        case StmtKind__inc_global_var15:
        case StmtKind__inc_global_var16:
        case StmtKind__inc_global_var17:
        case StmtKind__inc_global_var18:
        case StmtKind__inc_global_var19: {
            indent();
            static char const op[] = "+-*/|&";
            printf("global_%zd = global_%zd %c global_%zd %c %zd;\n",
                   get_random(p.global_vars_len),
                   get_random(p.global_vars_len),
                   op[get_random(ARRAY_COUNT(op)-1)],
                   get_random(p.global_vars_len),
                   op[get_random(ARRAY_COUNT(op)-1)],
                   get_random(100000));
        }
        break;
        case StmtKind__block: {
            indent();
            printf("{\n");
            p.indent += 1;
            usize const more_stmts = get_random(block_random_max);
            for (usize i = 0; i < more_stmts; i++)
                generate_stmt();
            p.indent -= 1;
            indent();
            printf("}\n");
        }
        break;
        case StmtKind__if: {
            indent();
            static char const* op[] = { "==", "!=", ">", ">=", "<", "<=" };
            printf("if (global_%zd %s %zd) {\n",
                   get_random(p.global_vars_len),
                   op[get_random(ARRAY_COUNT(op)-1)],
                   get_random(100000));
            p.indent += 1;
            usize const more_stmts = get_random(block_random_max);
            for (usize i = 0; i < more_stmts; i++)
                generate_stmt();
            p.indent -= 1;
            indent();
            printf("}\n");
        }
        break;
        case StmtKind__if_else: {
            indent();
            usize more_stmts = get_random(block_random_max);
            printf("if (global_%zd == %zd) {\n", get_random(p.global_vars_len), get_random(100000));
            p.indent += 1;
            for (usize i = 0; i < more_stmts; i++)
                generate_stmt();
            p.indent -= 1;
            indent();
            printf("} else {\n");
            p.indent += 1;
            more_stmts = get_random(block_random_max);
            for (usize i = 0; i < more_stmts; i++)
                generate_stmt();
            p.indent -= 1;
            indent();
            printf("}\n");
        }
        break;
    }
    p.stmt_depth -= 1;
}

static void generate_program(usize a, usize b)
{
    block_random_max = b;
    p = (Program){
        .functions_len = a,
        .global_vars_len = 100,
        .stmt_depth = 0,
        .total_stmt_count = 0,
        .indent = 0,
    };
    fprintf(stderr, "Generating %zd stmts, stmts %zd\n", p.functions_len, block_random_max);
    printf("// a = %zu\n", a);
    printf("// b = %zu\n", b);
    printf("// functions = %zu\n", p.functions_len);
    printf("// stmts = %zu\n", block_random_max);
    printf("// vars = %zu\n", p.global_vars_len);
    for (usize i = 0; i < p.global_vars_len; i++) {
        printf("var global_%zd = %zd;\n", i, get_random(1000));
    }
    for (usize idx_fn = 0; idx_fn < p.functions_len; idx_fn++) {
        p.total_stmt_count = 0;
        assert(p.stmt_depth == 0);
        indent();
        printf("fn fn_%zd() void {\n", idx_fn);
        p.indent += 1;
        usize const count_stmt = get_random(block_random_max);
        for (usize idx_stmt = 0; idx_stmt < count_stmt; idx_stmt++)
            generate_stmt();
        p.indent -= 1;
        indent();
        printf("}\n");
    }
    printf("fn main() void {\n");
    p.indent += 1;
    for (usize idx_fn = 0; idx_fn < p.functions_len; idx_fn++) {
        indent();
        printf("fn_%zd();\n", idx_fn);
    }
    p.indent -= 1;
    printf("}\n");
}

int main(int argc, char **argv)
{
    usize fn_count = 1;
    usize stmts = 5;
    if (argc == 2) {
        fn_count = strtoul(argv[1], NULL, 10);
        fn_count = CLAMP(fn_count, 1, 1000000);
    } else if (argc == 3) {
        fn_count = strtoul(argv[1], NULL, 10);
        fn_count = CLAMP(fn_count, 1, 1000000);

        stmts = strtoul(argv[2], NULL, 10);
        stmts = CLAMP(stmts, 2, 20);
    }
    init_random();
    generate_program(fn_count, stmts);
    return 0;
}
