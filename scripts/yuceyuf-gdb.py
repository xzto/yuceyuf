# vim: sw=4 et:
# pylint: disable=format,design,refactoring
# pylint: disable=missing-function-docstring,missing-class-docstring,missing-module-docstring,invalid-name,wrong-import-order

import gdb # pylint: disable=import-error

import itertools

def removeprefix(haystack, needle):
    if haystack.startswith(needle):
        return haystack[len(needle):]
    return haystack

def flatten(it):
    for outer in it:
        for inner in outer:
            yield inner

def nth(ptr, i):
    """Take ptr[i] unless ptr is a pointer to `Void`"""
    target_type = ptr.type.target()
    # If this is an std::is_empty_v type, the address may be invalid memory. Instead of reading undefined memory, do this instead:
    if target_type.code == gdb.TYPE_CODE_STRUCT and len(target_type.fields()) == 0:
        return gdb.parse_and_eval('$rbp').reinterpret_cast(target_type.pointer()).dereference()
    return ptr[i]

def call_method_empty_args(val_ptr, method_name):
    tmp_var = 'tmp_var_for_call_method_empty_args'
    gdb.set_convenience_variable(tmp_var, val_ptr)
    try:
        return gdb.parse_and_eval('${}->{}()'.format(tmp_var, method_name))
    finally:
        gdb.set_convenience_variable(tmp_var, None)

class YyOptionPrinter:
    """Print a yy::Option."""
    def __init__(self, val):
        self.val = val
        if self.val['m'].type.has_key('m_has') and self.val['m'].type.has_key('m_bytes'):
            self.kind = "generic"
            self.maybe_value = self.generic_value()
        elif self.val['m'].type.has_key('m_ptr') and self.val.type.template_argument(0).code == gdb.TYPE_CODE_REF:
            self.kind = "reference"
            self.maybe_value = self.reference_value()
        elif self.val.type.template_argument(0).unqualified().name == 'bool':
            self.kind = "bool"
            self.maybe_value = self.bool_value()
        elif self.val['m'].type.fields()[0].is_base_class and self.val['m'].type.fields()[0].type.has_key('m_value_or_sentinel'):
            self.kind = "trivial sentinel"
            try:
                self.maybe_value = self.trivial_sentinel_value()
            except (RuntimeError, gdb.error):
                self.kind = "unknown"
        else:
            self.kind = "unknown"
        # if self.kind == "unknown":
        #     try:
        #         addr = self.val.address
        #         has_value = bool(call_method_empty_args(addr, 'has_value'))
        #         maybe_value = None
        #         if has_value:
        #             maybe_value = call_method_empty_args(addr, 'unwrap')
        #         self.kind = "idk_but_it_works"
        #         self.maybe_value = maybe_value
        #     except gdb.error:
        #         pass

    def generic_value(self):
        assert self.kind == 'generic'
        has_value = self.val['m']['m_has'].cast(gdb.lookup_type('u8')) == 1
        if has_value:
            sub_type_ptr = self.val.type.template_argument(0).pointer()
            the_value = self.val['m']['m_bytes'].address.reinterpret_cast(sub_type_ptr).dereference()
            return the_value
        return None
    def reference_value(self):
        assert self.kind == 'reference'
        has_value = self.val['m']['m_ptr'].cast(gdb.lookup_type('uintptr_t')) != 0
        if has_value:
            the_value = self.val['m']['m_ptr'].dereference().reference_value()
            return the_value
        return None
    def bool_value(self):
        assert self.kind == 'bool'
        has_value = self.val['m']['m_as_int'] != 2
        if has_value:
            the_value: bool = (int(self.val['m']['m_as_int'])) != 0
            return the_value
        return None
    def trivial_sentinel_value(self):
        assert self.kind == 'trivial sentinel'
        m_base_type = self.val['m'].type.fields()[0].type
        actual_m = self.val['m'].reference_value().cast(m_base_type.reference()).referenced_value()
        value_or_sentinel = actual_m['m_value_or_sentinel']
        has_value = None
        if has_value is None:
            try:
                has_value = bool(call_method_empty_args(self.val.address, 'has_value'))
            except gdb.error as e:
                raise e
        if has_value is None:
            try:
                # This may fail with "template argument 1" is optimized out
                sentinel = actual_m.type.template_argument(1)
                has_value = value_or_sentinel != sentinel
            except (RuntimeError, gdb.error) as e:
                raise e
        assert has_value is not None
        if has_value:
            return value_or_sentinel
        return None

    def to_string(self):
        if self.kind == 'unknown':
            return '<{}>'.format(self.val.type.name)
        if self.maybe_value is not None:
            return 'Some <{}>'.format(self.val.type.name)
        else:
            return 'None <{}>'.format(self.val.type.name)
    def children(self):
        if self.kind == 'unknown':
            return (x for x in [("m", self.val['m'])])
        if self.maybe_value is not None:
            return (x for x in [("value", self.maybe_value)])
        else:
            return ()

class YySpanPrinter:
    """Print a yy::Span."""
    def __init__(self, val):
        self.val = val
        self.ptr = self.val["ptr"]
        self.len = int(self.val["len"])
        self.typename = self.val.type.name
        self.target_type = self.val.type.template_argument(0)
        self.is_sv = self.val.type.unqualified().name in [
            "Sv",
            "SvMut",
        ] or self.target_type.unqualified().name in [
            "char",
            "unsigned char",
        ]

    def to_string(self):
        if self.is_sv:
            return self.ptr.string('utf-8', 'replace', self.len) if self.len > 0 else ""
        return '<{}> of length {} at {}'.format(self.typename, self.len, self.ptr)
    def children(self):
        if self.is_sv:
            return ()
        return ((f'[{i}]', nth(self.ptr, i)) for i in range(self.len))
    def display_hint(self):
        return 'string' if self.is_sv else 'array'

class YyVecUnmanagedPrinter:
    """Print a yy::VecUnmanaged."""
    def __init__(self, val):
        self.val = val
        self.ptr = self.val['m_ptr']
        self.len = int(self.val['m_len'])
        self.capacity = int(self.val['m_capacity'])
        self.typename = self.val.type.name
        self.target_type = self.val.type.template_argument(0)

    def to_string(self):
        return '<{}> of length {} capacity {} at {}'.format(self.typename, self.len, self.capacity, self.ptr)
    def children(self):
        return ((f'[{i}]', nth(self.ptr, i)) for i in range(self.len))
    def display_hint(self):
        return 'array'

class YyMetaTuPrinter:
    """Print a tagged union from yy/meta/tagged_union.hpp."""
    def __init__(self, val):
        self.val = val
        self.tag = self.val['tag']
        self.payload = self.val['payload']
        self.typename = self.val.type.name
        _ = self.val['payload']['__meta_tu_default_init']

    def to_string(self):
        return '<{}>'.format(self.typename)

    def children(self):
        name = removeprefix(str(self.tag), self.tag.type.name + '__')
        return (x for x in [(name, self.payload[name])])

def yy_meta_tu_lookup_function(val):
    try:
        if val.type.has_key('tag') and val.type.has_key('payload') and val.type['payload'].type.has_key('__meta_tu_default_init'):
            return YyMetaTuPrinter(val)
    except TypeError:
        pass
    return None

class YyVoidPrinter:
    def __init__(self, val):
        pass
    def to_string(self):
        return 'Void{}'

class YyNewerHashMapPrinter:
    """Print a yy::NewerHashMap."""

    def __init__(self, val):
        self.val = val
        self.size = int(self.val["m_insertions"]["m_keys"]["m_len"])
        self.typename = self.val.type.name
        self.removed = int(self.val['m_lut']['removed'])
        self.entries_len = int(self.val['m_lut']['entries_len'])
        self.occupied = int(self.val['m_lut']['occupied'])

    def to_string(self):
        return "<{}> of length {} allocator {} lut_entries_len {} removed {} load factor {}".format(
            self.typename, self.size,
            self.val['m_heap'],
            self.entries_len,
            self.removed,
            "{:.2f}".format(100 * int(self.occupied) / int(self.entries_len) if self.entries_len > 0 else 0),
        )

    # def children(self):
    #     return (
    #         (
    #             "[index={}, key={}]".format(i, nth(self.val["m_insertions"]["m_keys"]["m_ptr"], i)),
    #             nth(self.val["m_insertions"]["m_values"]["m_ptr"], i),
    #         )
    #         for i in range(self.size)
    #     )
    # def display_hint(self):
    #     return None

    def children(self):
        return zip(map(lambda i: f"[{i//2}]", itertools.count()), flatten((
            (
                nth(self.val["m_insertions"]["m_keys"]["m_ptr"], i),
                nth(self.val["m_insertions"]["m_values"]["m_ptr"], i),
            )
            for i in range(self.size)
        )))
    def display_hint(self):
        return 'map'


def build_pretty_printer():
    pp = gdb.printing.RegexpCollectionPrettyPrinter('yy')
    pp.add_printer('Option', '^yy::Option<.*>$', YyOptionPrinter)
    pp.add_printer('Span', '^yy::Span<.*>$', YySpanPrinter)
    pp.add_printer('NewerHashMap', '^yy::NewerHashMap<.*>$', YyNewerHashMapPrinter)
    pp.add_printer('VecUnmanaged', '^yy::VecUnmanagedDetail<.*>$', YyVecUnmanagedPrinter)
    # pp.add_printer('Void', '^Void$', YyVoidPrinter)
    return pp

gdb.printing.register_pretty_printer(gdb.current_objfile(), build_pretty_printer())
gdb.current_objfile().pretty_printers.append(yy_meta_tu_lookup_function)
