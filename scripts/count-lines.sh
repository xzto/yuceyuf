#!/bin/sh
# usage: ./scripts/count-lines.sh [extra options to find]
# example usage: ./scripts/count-lines.sh -name '*.[ch]'

set -eu
root_dir="$(dirname "$0")/.."
root_dir="${root_dir%"/scripts/.."}"
test -n "${root_dir}" || { echo "$0: fatal: \$root_dir is empty????" >&2; exit 1; }
test -d "${root_dir}" || { echo "$0: fatal: \$root_dir is not a directory????" >&2; exit 1; }

find -L                     \
    "${root_dir}/src"       \
    "${root_dir}/scripts"   \
    "${root_dir}/configure" \
    -type f                 \
    "$@"                    \
    -print0                 \
    | wc -l --files0-from=/dev/stdin \
    | sort -n               \
