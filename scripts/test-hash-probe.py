#!/usr/bin/env python3

from random import randrange

def is_power_of_two(n):
    return (n & (n-1) == 0) and n != 0


def next_probe_linear(table_size, lut_idx, tries, hash):
    assert is_power_of_two(table_size)
    mask = table_size - 1
    return (lut_idx + 1) & mask

def next_probe_quadratic(table_size, lut_idx, tries, hash):
    assert is_power_of_two(table_size) and tries > 0
    mask = table_size - 1
    return (lut_idx + tries) & mask

def next_probe_quadratic10_linear(table_size, lut_idx, tries, hash):
    assert is_power_of_two(table_size) and tries > 0
    mask = table_size - 1
    return (lut_idx + (tries if tries < 10 else 1)) & mask

def next_probe_hash_linear(table_size, lut_idx, tries, hash):
    assert is_power_of_two(table_size) and tries > 0
    mask = table_size - 1
    return (lut_idx + ((hash >> 16) if tries == 1 else 1)) & mask

def next_probe_hash_quadratic(table_size, lut_idx, tries, hash):
    assert is_power_of_two(table_size) and tries > 0
    mask = table_size - 1
    return (lut_idx + ((hash >> 16) if tries == 1 else (tries - 1))) & mask

def get_loop_size(table_size, probe_fn, hash):
    s = set()
    tries = 0
    lut_idx = hash & (table_size-1)
    while True:
        if len(s) == table_size:
            return tries
        s.add(lut_idx)
        tries += 1
        lut_idx = probe_fn(table_size, lut_idx, tries, hash)


def main():
    the_hash = randrange(2**32)
    print(f"hash = {the_hash:x}")
    for probe_fn in (next_probe_linear, next_probe_quadratic, next_probe_quadratic10_linear, next_probe_hash_linear, next_probe_hash_quadratic):
        for j in range(16):
            table_size = 2**j
            loop_size = get_loop_size(table_size, probe_fn, the_hash)
            loop_size_offset = loop_size - table_size
            print(f"{probe_fn.__name__} {table_size} = {loop_size_offset}")


if __name__ == "__main__":
    main()
