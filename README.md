## Description

A programming language.

---

## Building

### Default build (debug)

```console
$ mkdir tmp-build && cd tmp-build
$ ../configure --hardcode-stdlib-path --build
$ ./yuceyuf --help
```

### Release build

```console
$ mkdir tmp-build && cd tmp-build
$ ../configure --build-type=release --build
$ ../configure install --destination="$HOME/.local"
```

TODO: Detect stdlib path automatically.

### Building with your system package manager (like PKGBUILD)

```sh
do_build() {
  cd source && mkdir tmp-build && cd tmp-build
  # reads CC, CFLAGS, LDFLAGS
  ../configure --build-type=none --build
}
do_install() {
  cd source/tmp-build
  ../configure install --destination="${pkg_destdir}/usr"
  install -m644 -D -t "${pkg_destdir}/usr/share/licenses/yuceyuf/" ../LICENSE
}
```

### Advanced

```console
$ ./configure --help
```

`./configure <args>`: config with `<args>` (removes old config, if any)

`./configure conf <args>`: same as `./configure <args>`

`./configure reconf <args>`: do `./configure <current-config-args> <args>`

`./configure install <args>`: install the build

To build, use the `--build` flag. You can also run ninja directly.

When calling `configure`, the current working directory will be used
for the build directory.

To build with optimizations turned on, use `--build-type=release` or
`CFLAGS=...`.

To clean everyting, run `./configure distclean` from your build
directory. You can also directly remove the build directory.

Some environment variables are read from the environment (see `./configure
--help`). You can also set or append them on the command line.


#### Examples
```console
$ ./configure --hardcode-stdlib-path --build
$ ./configure CC=gcc --hardcode-stdlib-path --build-type=dev_debug && ninja
$ env CC="ccache clang" ./configure --build-type=release --build
$ mkdir tmp-build && cd tmp-build && ../configure --build-type=none && ninja
```

### Note for local development

#### `--hardcode-stdlib-path`

For development, you probably want to use the `configure` flag
`--hardcode-stdlib-path` to hardcode a full path to
`std.yuceyuf` at compile time for development. Example:
```console
$ cd tmp-build && ../configure --hardcode-stdlib-path && ninja
```
Do not use this feature for packaging/releasing the software.

TODO: Detect the path to the executable and detect it in this order.
1. command line argument `--stdlib-path`
2. directories relative to the executable's: `exe_dir/stdlib`,
   `exe_dir/../lib/yuceyuf/stdlib`. Make a subcommand to print these directories.
3. (last) fallback to an environment variable
   `YUCEYUF_DEBUG_STDLIB_PATH_FALLBACK`. This should only be used as a fallback
   for development purposes.

---

## Installing

Run the following command from your build directory:

```console
$ ./configure install --help
```

Absolute paths are not hardcoded into the executable. You should be able
to move the installed destination anywhere.

#### Installation examples
```console
$ ./configure install                  --destination=/usr/local
$ cd tmp-build && ../configure install --destination=tmp-install
$ cd tmp-build && ../configure install --destination="$pkgdir/usr"
$ ./configure --build-type=release --build && ./configure install --destination="$HOME/.local"
```

#### Uninstalling

Call `./configure install` with the same arguments you gave when
installing and add `--uninstall`. The install manifest file needs to be
available.
