#!/bin/sh

set -eu
die() { printf %s\\n "$@" >&2; exit 1; }
unreachable() { die "$0: unreachable"; }
root_dir="$(dirname "$0")"
test -n "${root_dir}" || die "$0: fatal: \$root_dir is empty????"
test -d "${root_dir}" || die "$0: fatal: \$root_dir is not a directory????"
test -f yuceyuf || die "$0: fatal: 'yuceyuf' exe is not in cwd"

echo_and_run() {
  echo "+" "$@" >&2
  "$@"
}

asm_file=""
out_file=""
do_run="0"
while [ "$#" -gt 0 ]; do
  case "$1" in
    (--asm) asm_file="$2"; shift ;;
    (--out) out_file="$2"; shift ;;
    (--run) do_run=1 ;;
    (--) shift; break ;;
    (*) die "$0: fatal: unknown option: '$1'" ;;
  esac
  shift
done

[ -n "$asm_file" ] || asm_file="yuceyuf_output.asm"
if [ -z "$out_file" ]; then
  case "$asm_file" in
    (*.asm) out_file="${asm_file%.asm}" ;;
    (*) out_file="${asm_file}.out" ;;
  esac
fi

case "$out_file" in
  (*/*) ;;
  (*) out_file="./$out_file" ;;
esac

readonly asm_file out_file
test -f "$asm_file" || die "$0: fatal: asm file not found: '$asm_file'"

temp_dir="$(mktemp -d)"
cleanup() {
  test -d "${temp_dir}"
  rm -rf "${temp_dir}"
}

trap 'cleanup' EXIT

readonly asm_obj_file="${temp_dir}/yuceyuf_output.asm.o"

echo_and_run yasm -p nasm -f elf64 -g dwarf2 -r raw -o "$asm_obj_file" "$asm_file"

file_older() { test "$#" -eq 2 || unreachable; test "$1" -ot "$2"; }

file_older "${root_dir}/start_files/start.asm" "./start.asm.o" || \
  echo_and_run yasm -p nasm -f elf64 -g dwarf2 -o "./start.asm.o" "${root_dir}/start_files/start.asm"

file_older "${root_dir}/start_files/print_uint.c" "./print_uint.c.o" || \
  echo_and_run gcc -g3 -nostdlib -fno-stack-protector -mno-red-zone -c "${root_dir}/start_files/print_uint.c" -o "./print_uint.c.o"

echo_and_run ld "./start.asm.o" "$asm_obj_file" "./print_uint.c.o" -o "$out_file"

if [ "$do_run" = 1 ]; then
  echo_and_run "$out_file" "$@"
fi
