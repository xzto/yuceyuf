// This file is for quick tests. Build with build-testing.sh

#include "yy/allocator.hpp"
#include "yy/basic.hpp"
#include "yy/newer_hash_map.hpp"
#include "yy/option.hpp"
#include "yy/sv.hpp"
#include "yy/vec.hpp"
#include "yy/meta/enum.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <array>
#include <ranges>
#include <vector>
#include <memory>


auto constify = [](auto const& x) -> decltype(x) { return x; };


namespace {

struct MyIndex {
    u32 x;
    constexpr auto operator<=>(MyIndex other) const -> std::strong_ordering { return x <=> other.x; }
    constexpr auto operator==(MyIndex other) const -> bool { return x == other.x; }
    YY_DECL_INVALID_OF_INSIDE_CLASS(MyIndex, MyIndex{42});
};

#define YY_META_ENUM__NAME MyEnum
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS \
    YY_META_ENUM__MEMBER(a)       \
    YY_META_ENUM__MEMBER(b)       \
    YY_META_ENUM__MEMBER(c)
#include "yy/meta/enum.hpp"
static_assert(yy::is_meta_enum<MyEnum>);
static_assert(sizeof(MyEnum) == sizeof(u8));

struct Foo {
    int integer;
    ~Foo() { printf("~Foo(%d)\n", integer); }

    Foo() = default;
    Foo(int integer) : integer(integer) {}

    Foo(const Foo& other) : integer(other.integer) { printf("Foo(const Foo& %d)\n", other.integer); }
    Foo(Foo&& other) : integer(other.integer) {
        printf("Foo(Foo&& %d)\n", other.integer);
        other.integer += 10000;
    }
    Foo& operator=(const Foo& other) {
        if (this != &other) {
            printf("%d operator=(const Foo& %d)\n", integer, other.integer);
        }
        return *this;
    }
    Foo& operator=(Foo&& other) {
        if (this != &other) {
            printf("%d operator=(Foo&& %d)\n", integer, other.integer);
            other.integer += 20000;
        }
        return *this;
    }
};


constexpr auto print_ints(auto&& range, char const* msg) -> void {
    printf("%s", msg);
    for (auto const& x : range) {
        printf("%4d%s", int(x), (&x == std::ranges::end(range) - 1) ? "" : ", ");
    }
    printf("\n");
};

UNUSED auto test_vec() -> void {
    using namespace yy;
    static_assert(std::ranges::random_access_range<VecUnmanaged<int>>);

    Allocator heap = page_allocator();
    {
        VecUnmanaged<int> vi{};
        defer {
            vi.destroy_free(heap);
        };
        vi.ensure_unused_capacity(heap, 3);
        vi.push_back(heap, 10);
        vi.push_back(heap, 20);
        vi.push_back(heap, 30);
        {
            auto arr = std::array<int, 3>{1, 2, 3};
            vi.append_range(heap, arr);
            VecUnmanaged<int> x{};
            defer {
                x.destroy_free(heap);
            };
            x.append_range(heap, arr);
            x.append_n_default(heap, 2);
            vi.append_range(heap, x);
        }
        vi.append_range(heap, std::views::iota(1, 3));
        print_ints(vi, "");
        printf("len = %zu, cap = %zu\n", vi.len(), vi.capacity());
    }
    printf("\n");
    {
        Vec<int> vi{heap};
        vi.ensure_unused_capacity(3);
        vi.push_back(10);
        vi.push_back(20);
        vi.push_back(30);
        {
            auto arr = std::array<int, 3>{1, 2, 3};
            vi.append_range({1, 2, 3});
            Vec<int> x{heap};
            x.append_range(arr);
            x.append_n_default(2);
            vi.append_range(x);
        }
        vi.append_range(std::views::iota(1, 3));
        print_ints(vi, "");
        printf("len = %zu, cap = %zu\n", vi.len(), vi.capacity());
    }
    printf("\n");
    {
        VecUnmanaged<int> a{};
        defer {
            a.destroy_free(heap);
        };
        int arr[3] = {1, 2, 3};
        a.append_range(heap, arr);
        auto a1 = a.clone(heap);
        defer {
            a1.destroy_free(heap);
        };
        yy_assert(a1.try_get(2) == 3);
        yy_assert(a1.try_get(3).is_none());
        auto a2 = a.clone_into_managed(heap);
        printf("%p\n", (void*)std::begin(a));
        print_ints(a, "a : ");
        printf("%p\n", (void*)std::begin(a1));
        print_ints(a1, "a1: ");
        printf("%p\n", (void*)std::begin(a2));
        print_ints(a2, "a2: ");
    }
    printf("\n");
    {
        Vec<int> a{heap};
        int arr[3] = {1, 2, 3};
        a.append_range(arr);
        auto a1 = a.clone();
        auto a2 = a.clone_into_unmanaged();
        defer {
            a2.destroy_free(heap);
        };
        printf("%p\n", (void*)std::begin(a));
        print_ints(a, "a : ");
        printf("%p\n", (void*)std::begin(a1));
        print_ints(a1, "a1: ");
        printf("%p\n", (void*)std::begin(a2));
        print_ints(a2, "a2: ");
    }
    printf("Back inserter test\n");
    {
        struct IntMoveReset {
            int x{0};
            operator int() const { return x; }
            IntMoveReset() = default;
            IntMoveReset(int i) : x(i) {}
            IntMoveReset(const IntMoveReset&) = default;
            IntMoveReset(IntMoveReset&& other) {
                x = other.x;
                other.x = 9999;
            }
            IntMoveReset& operator=(const IntMoveReset&) = default;
            IntMoveReset& operator=(IntMoveReset&& other) {
                x = other.x;
                other.x = 777;
                return *this;
            }
        };
        Vec<IntMoveReset> v{heap};
        v.push_back(10);
        {
            auto inserter = std::back_inserter(v);
            *inserter++ = 30;
            *inserter++ = usize{20};
            *inserter++ = 30;
            *inserter++ = 30;
            *inserter++ = 31;
            *inserter++ = 41;
            *inserter++ = 50;
            *inserter++ = 60;
            *inserter++ = 70;
            *inserter++ = 80;
            *inserter++ = 90;
        }
        print_ints(v, "original         : ");

        {
            auto removed_range = std::ranges::remove(v, IntMoveReset(30));
            print_ints(v, "after remove 30  : ");

            yy_assert(removed_range.end() == v.end());
            v.erase(removed_range.begin(), removed_range.end());
            print_ints(v, "after erase range: ");
        }

        auto t = v.erase(v.begin() + 2, v.begin() + 4);
        yy_assert(t == v.begin() + 2);
        print_ints(v, "after erase 2..4 : ");
    }
    {
        VecUnmanaged<int> a{};
        VecUnmanaged<int> b{};
        using std::swap;
        // swap(a, b);
    }
    {
        Vec<int> a{heap};
        Vec<int> b{heap};
        using std::swap;
        // swap(a, b);
    }
    {
        Vec<int> a{heap};
        Vec<int> b = std::move(a);
        Vec<int> c = b.clone();
        c = yy::clone<Vec<int>>(b);
        c = std::move(b);
    }
    {
        struct MyInt {
            int x;
            NODISCARD constexpr auto clone(Void) const -> MyInt { return *this; }
        };
        Vec<MyInt> vi{heap};
        vi.push_back(MyInt{4});
        Vec<Vec<MyInt>> a{heap};
        a.push_back(vi.clone());
        a.push_back(vi.clone());
        auto b0 = a.clone();
        auto b1 = a.clone(Void{});
        auto b2 = a.clone(Vec<MyInt>::other_allocator_tag_t{}, heap);
        auto b3 = a.clone(Vec<MyInt>::other_allocator_tag_t{}, heap, Void{});
        auto b4 = a.clone(decltype(a)::other_allocator_tag_t{}, heap, Vec<MyInt>::other_allocator_tag_t{}, heap);
        auto b5 = a.clone(decltype(a)::other_allocator_tag_t{}, heap, Vec<MyInt>::other_allocator_tag_t{}, heap, Void{});
        yy_assert(std::ranges::all_of(
            std::initializer_list<std::reference_wrapper<const Vec<Vec<MyInt>>>>{b0, b1, b2, b3, b4, b5},
            [&](auto&& x) { return x.get().size() == a.size(); }
        ));
    }
    {
        VecUnmanaged<int> a{};
        VecUnmanaged<int> b{};
        VecUnmanaged<int> c = std::move(a);
        c = std::move(b);
    }
    {
        struct Mono {};
        static_assert(sizeof(Mono) == 1);
        static_assert(alignof(Mono) == 1);
        Mono ms[10];
        static_assert(sizeof(ms) == 10);
        yy_assert(&ms[1] == &ms[0] + 1 && &ms[0] != &ms[1]);
        Vec<Mono> v1{heap};
        v1.append_range(ms);
        Vec<Mono> v2{heap};
        v2.push_back({});
        v1.append_range(v2);
        printf("v1 size: %zu\n", v1.len());
        printf("v2 size: %zu\n", v2.len());
        printf("ms size: %zu\n", std::ranges::size(ms));
        printf("v1 ptr: %p\n", (void*)v1.data());
        yy_assert(v1.size() == v2.size() + std::ranges::size(ms));
    }
    static_assert(requires {
        { (void)0 };
    });

    {
        printf("\n\nVec<Foo>:\n");
        Vec<Foo> a{c_allocator()};
        a.push_back(Foo(0));
        a.push_back(Foo(10));
        a.push_back(Foo(20));
        a.push_back(Foo(30));
        a.push_back(Foo(40));
        a.push_back(Foo(50));
        for (usize i = 0; i < a.size(); i++) {
            printf(" foo[%zu] = %d\n", i, a[i].integer);
        }
        printf("\n~Vec<Foo>:\n");
    }
    printf("\n\n");
    {
        printf("Vec<char>:\n");
        Vec<char> a{c_allocator()};
        a.append_range("Hello");
        printf("%s\n", a.c_str());
        a.append_range({',', ' '});
        printf("%s\n", a.c_str());
        a.append_range("world!");
        printf("%s\n", a.c_str());
        yy_assert(a.size() == 13 && a == "Hello, world!"_sv);
    }
    printf("\n\n");
}

UNUSED auto test_sv() -> void {
    using namespace yy;

    {
        Sv foo = "hello"_sv;
        yy_assert(foo.len == 5);
        yy_assert(sv_eql(foo, sv_lit("hello")));
        Sv a = {};
        Sv b = {};
        yy_assert(a == b);
        yy_assert(a == sv_lit(""));
        for (auto c : foo) {
            fputc(c, stdout);
        }
        printf("\n");
        fwrite(std::ranges::data(foo), std::ranges::size(foo), 1, stdout);
        printf("\n");
        yy_assert(foo.try_get(4) == 'o');
        yy_assert(foo.try_get(5).is_none());
    }

    {
        std::vector<u8> v{1, 2, 3};
        Span<u8> v_sv = v;
        print_ints(v_sv, "");
    }
}

template <typename T>
concept valid_option = requires { Option<T>{}; };

template <typename T> struct is_valid_option : std::false_type {};
template <typename T>
    requires valid_option<T>
struct is_valid_option<T> : std::true_type {};

UNUSED auto test_option() -> void {
    using namespace yy;

    {
        Option<bool> a{};
        printf("bool: %s\n", decltype(a)::traits.debug_name);
        static_assert(sizeof(a) == 1);
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "bool");
        yy_assert(!a.has_value());
        a = false;
        yy_assert(a.has_value());
        a = true;
        yy_assert(a.has_value());
        a = None;
        yy_assert(!a.has_value());
    }

    {
        Option<int> a{};
        printf("int: %s\n", decltype(a)::traits.debug_name);
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "generic");
        static_assert(sizeof(a) == sizeof(int) * 2);
        yy_assert(!a.has_value());
        a = 2;
        yy_assert(a.has_value());
        yy_assert(a.unwrap() == 2);
        a = 42;
        auto mapped1 = a.map([](auto&& x) { return x + 1; });
        auto mapped2 = a.map([](auto&&) -> char const* { return "hello"; });
        yy_assert(mapped1 == 43);
        yy_assert(Sv(mapped2.unwrap()) == sv_lit("hello"));
    }
    {
        int i = 1;
        Option<int*> a{&i};
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "trivial sentinel");
        static_assert(sizeof(a) == sizeof(int*));
        printf("int*: %s\n", decltype(a)::traits.debug_name);
        // a = reinterpret_cast<int*>(0); // panics
        yy_assert(*a.unwrap() == 1);
        int j = 2;
        a = &j;
        auto as_deref = a.as_deref();
        yy_assert(as_deref.unwrap() == 2);
        *a.unwrap() = 62;
        yy_assert(i == 1);
        yy_assert(j == 62);
        yy_assert(as_deref.unwrap() == 62);
        a = None;
        yy_assert(a.as_deref().is_none());
        yy_assert(!a.has_value());
        as_deref.unwrap() = 89;
        yy_assert(j == 89);
    }
    {
        Option<MyIndex> a{};
        static_assert(sizeof(a) == sizeof(MyIndex));
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "trivial sentinel");
        yy_assert(!a.has_value());
        static constexpr auto myindex_invalidof = yy_invalidOf_StructType<MyIndex>::value;
        yy_assert(a.has_value() == (*reinterpret_cast<MyIndex*>(&a) != myindex_invalidof));
        printf("MyIndex: %s\n", decltype(a)::traits.debug_name);
        a = MyIndex{11};
        yy_assert(a.has_value() == (*reinterpret_cast<MyIndex*>(&a) != myindex_invalidof));
        yy_assert(a.as_ref().unwrap() == MyIndex{11});
    }
    {
        Option<int const&> a{};
        static_assert(sizeof(a) == sizeof(MyIndex*));
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "reference");
        printf("reference const: %s\n", decltype(a)::traits.debug_name);
        int i = 21;
        a = i;
        yy_assert(a.unwrap() == 21);
    }
    {
        Option<int&> a{};
        static_assert(sizeof(a) == sizeof(MyIndex*));
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "reference");
        printf("reference mut: %s\n", decltype(a)::traits.debug_name);
        int i = 42;
        a = i;
        a.unwrap()++;
        a = Option<int&>{};
        a = Option<int&>{i};
        yy_assert(a.unwrap() == 43);
        yy_assert(i == 43);
    }
    {
        Option<MyEnum> a{};
        static_assert(sizeof(a) == sizeof(MyEnum));
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "trivial sentinel");
        yy_assert(a.is_none());
        static constexpr auto myenum_invalidof = yy_invalidOf_StructType<MyEnum>::value;
        yy_assert(a.has_value() == (*reinterpret_cast<MyEnum*>(&a) != myenum_invalidof));
        printf("MyEnum: %s\n", decltype(a)::traits.debug_name);
        a = MyEnum__b;
        yy_assert(a.has_value());
        yy_assert(a.has_value() == (*reinterpret_cast<MyEnum*>(&a) != myenum_invalidof));
    }
    {
        // For some reason these two are giving in a failed static assert.
        // static_assert(!is_valid_option<NoneType>());
        // static_assert(!is_valid_option<void>());
        static_assert(is_valid_option<Void>());
        static_assert(is_valid_option<int>());
    }
    {
        struct Empty {};
        Option<Empty> a{};
        static_assert(sizeof(a) == 1);
        static_assert(std::string_view(decltype(a)::traits.debug_name) == "empty");

        // TODO: Option of an empty struct should be size 1
        printf("Empty: %s\n", decltype(a)::traits.debug_name);
    }
    {
        // This test may not be portable.
        static usize count_destroyed;
        static usize count_moved;
        count_destroyed = 0;
        count_moved = 0;
        struct Asdf {
            int x;
            bool moved{false};
            Asdf(int x_) : x(x_) {}
            auto operator=(Asdf const& other) -> Asdf& = delete;
            Asdf(Asdf const& other) = delete;
            auto operator=(Asdf&& other) -> Asdf& {
                if (this != &other) {
                    std::destroy_at(this);
                    std::construct_at(this, std::move(other));
                }
                return *this;
            }
            Asdf(Asdf&& other) : x(other.x) {
                count_moved++;
                yy_assert(!other.moved);
                other.moved = true;
            }
            ~Asdf() { if (!moved) count_destroyed++; }
        };
        yy_assert(count_destroyed == 0);
        yy_assert(count_moved == 0);
        {
            Asdf asdf2{2};
            // trivial sentinel
            Option<Asdf*> a = None;
            *a.unsafe_out_ptr_assume_init() = &asdf2;
            yy_assert(a.as_deref().unwrap().x == 2);
        }
        yy_assert(count_destroyed == 1);
        yy_assert(count_moved == 0);
        {
            Option<Asdf> a = None;
            yy_assert(count_destroyed == 1);
            *a.unsafe_out_ptr_assume_init() = Asdf{3};
            yy_assert(count_destroyed == 2);
            yy_assert(a.unwrap().x == 3);
        }
        yy_assert(count_destroyed == 3);
        yy_assert(count_moved == 2);
        {
            Option<Asdf> a = None;
            // Ignore the result. The destructor for Asdf will run.
            (void) a.unsafe_out_ptr_assume_init();
        }
        yy_assert(count_destroyed == 4);
        yy_assert(count_moved == 3);
        {
            Option<Asdf> a = None;
            yy_assert(count_moved == 3);
            a = Asdf{2};
            yy_assert(count_moved == 4);
        }
        yy_assert(count_destroyed == 5);
        yy_assert(count_moved == 4);
    }
    {
        Option<std::unique_ptr<int>> b = None;
        b = std::make_unique<int>(1);
        b.as_deref().unwrap() = 3;
        yy_assert(*b.unwrap() == 3);
        b.as_ref().as_deref().unwrap() = 9;
        yy_assert(*b.unwrap() == 9);
        b = None;
    }
    {
        {
            Option<std::unique_ptr<int>> a = None;
            a = std::make_unique<int>(1);
            {
                auto as_mut_ref = a.as_ref();
                auto as_deref = as_mut_ref.as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
            {
                auto as_const_ref = constify(a).as_ref();
                auto as_deref = as_const_ref.as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
            {
                auto as_mut_ref = a.as_ref();
                auto as_deref = constify(as_mut_ref).as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
            {
                auto as_const_ref = constify(a).as_ref();
                auto as_deref = constify(as_const_ref).as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
        }
        {
            struct MyBox {
                int* a;
                MyBox(int* x) : a(x) {}
                auto operator*() const& -> int const& { return *a; }
                auto operator*() & -> int & { (void)this; return *a; }
            };
            int some_integer = 2;
            Option<MyBox> a = &some_integer;
            {
                auto as_mut_ref = a.as_ref();
                auto as_deref = as_mut_ref.as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
            {
                auto as_const_ref = constify(a).as_ref();
                auto as_deref = as_const_ref.as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int const &>>);
            }
            {
                auto as_const_ref = a.as_ref();
                auto as_deref = constify(as_const_ref).as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int &>>);
            }
            {
                auto as_const_ref = constify(a).as_ref();
                auto as_deref = constify(as_const_ref).as_deref();
                static_assert(std::same_as<decltype(as_deref), Option<int const &>>);
            }
        }
    }
    {
        struct Foo : yy::MoveOnly {
            NODISCARD constexpr auto clone() const -> Foo { (void)this; return Foo{}; }
        };
        Option<Foo> a = Foo{};
        {
            // "empty" option accepts trivial_raii_maybe_moveonly
            static_assert(sizeof(a) == 1);
            static_assert(std::string_view(decltype(a)::traits.debug_name) == "empty");
        }
        static_assert(yy::has_clone_fn_with_args<Foo>);
        static_assert(yy::has_clone_fn_with_args<Option<Foo>>);
        Option<Foo> a1{a.clone()};
        auto a2 = yy::clone<Option<Foo>>(a1);
        (void) a2;
        // Option<Foo> a3{a}; // compile error
    }
    {
        // Map a thing into its member.
        struct Whatever {
            int value;
        };
        Option<Whatever> opt = Whatever{ 42 };
        auto v = opt.map(&Whatever::value);
        yy_assert(v == 42);
    }
    {
        // Conversions from Option<U> to Option<T>
        Option<int> opt = 412;
        auto another_ref = opt.as_ref().clone();
        static_assert(std::same_as<Option<int&>, decltype(another_ref)>);
        UNUSED auto _1 = Option<int&>(opt.as_ref());
        yy_assert(&_1.unwrap() == &opt.unwrap());
        UNUSED auto _2 = Option<int const&>(opt.as_ref());
        yy_assert(&_1.unwrap() == &_2.unwrap());
        UNUSED auto _3 = Option<int const&>(opt.as_ref().unwrap());
        // auto _4 = Option<int>(opt.as_ref()); // error
        // auto _4 = Option<int&>(Option<int const&>(opt.as_ref())); // error
    }
    {
        // u32 one = 123;
        // UNUSED u32 const& aa = one;
        // // For some reason this is allowed. I'm guessing it does a conversion and then binds a const& to that temporary.
        // UNUSED u64 const& aa1 = aa;

        // // The the does not happen if we had u32& and u64&.
        // u32& mut_aa = one;
        // // error: Non-const lvalue reference to type 'u64' cannot bind to a value of unrelated type 'const u32'
        // u64& mut_aa1 = aa;

        // // Since u32 const& to u64 const& is allowed, option happens to accept it.
        // // Thankfully, the compiler tells us that this is a bug: error: Returning reference to local temporary object
        // // [-Wreturn-stack-address]. I won't worry about this.
        // auto _0 = Option<u64 const&>(Option<u32 const&>(u32(1)));
    }
    {
        Option<int> opt;
        auto cloned_int1 = opt.clone();
        auto cloned_int2 = opt.as_ref().map(yy::clone<int>);
        auto cloned_int3 = opt.as_ref().cloned();
        auto cloned_ref1 = opt.as_ref().clone();
        auto cloned_ref2 = opt.as_ref().map(yy::clone<int&>);
        // cloned_ref3 technically works, but it feels weird.
        auto cloned_ref3 = opt.map(yy::clone<int&>);
        static_assert(std::same_as<Option<int&>, decltype(cloned_ref1)>);
        static_assert(std::same_as<Option<int&>, decltype(cloned_ref2)>);
        static_assert(std::same_as<Option<int&>, decltype(cloned_ref3)>);
        static_assert(std::same_as<Option<int>, decltype(cloned_int1)>);
        static_assert(std::same_as<Option<int>, decltype(cloned_int2)>);
        static_assert(std::same_as<Option<int>, decltype(cloned_int3)>);
    }
    {
        yy_assert(Option<Option<int>>{}.flatten().is_none());
        yy_assert(Option<Option<int>&>{}.flatten_as_ref().is_none());
        yy_assert(Option<Option<int>>{42}.flatten().unwrap() == 42);
    }
}

UNUSED auto test_newer_hash_map() -> void {
    using namespace yy;


    auto print_map_sv = [](NewerHashMap<int, Sv> const& map) {
        printf(">\n");
        for (auto ref : map) {
            printf(" %d -> '" Sv_Fmt "'\n", ref.key, Sv_Arg(ref.value));
        }
        printf("\n");
    };
    auto print_map_foo = [](NewerHashMap<int, Foo> const& map) {
        printf(">\n");
        for (auto ref : map) {
            printf(" %d -> %d\n", ref.key, ref.value.integer);
        }
        printf("\n");
    };


    {
        NewerHashMap<int, Sv> map{c_allocator()};
        print_map_sv(map);

        map.put_no_clobber_emplace({}, 42, sv_lit("Hello, world!"));
        print_map_sv(map);

        map.put_no_clobber_emplace({}, 10, "hi"_sv);
        print_map_sv(map);

        auto get_42 = constify(map).get({}, 42).unwrap();
        yy_assert(get_42.value == sv_lit("Hello, world!"));
        printf(Sv_Fmt "\n", Sv_Arg(get_42.value));
        printf(Sv_Fmt "\n", Sv_Arg(map.get({}, 10).unwrap().value));
        yy_assert(map.get({}, 9999).is_none());

        auto value7 = map.get_or_put_entry({}, 7).map_put_value(Sv("seven", usize{5}));
        print_map_sv(map);

        auto value7_two = map.get_or_put_entry({}, 7).unwrap_get();
        printf("size full %zu\n", map.size());
        print_map_sv(map);
        yy_assert(value7.insertion_idx == value7_two.insertion_idx);


        yy_assert(map.size() == 3);
        yy_assert(map.get({}, 10).has_value());
        map.swap_remove_index(1);
        print_map_sv(map);

        yy_assert(map.get({}, 10).is_none());
        map.truncate(1);
        printf("size truncate1 %zu\n", map.size());
        print_map_sv(map);

        map.clear();
        printf("size clear %zu\n", map.size());
        print_map_sv(map);
    }
    printf("\n\n\nFoo:\n");
    {
        NewerHashMap<int, Foo> map{c_allocator()};
        map.put_no_clobber_emplace({}, 0, 0);
        map.put_no_clobber_emplace({}, 10, 100);
        map.put_no_clobber_emplace({}, 20, 200);
        map.put_no_clobber_emplace({}, 30, 300);
        map.put_no_clobber_emplace({}, 40, 400);
        map.put_no_clobber_emplace({}, 50, 500);
        printf("put:\n");
        print_map_foo(map);

        printf("swap_remove:\n");
        map.swap_remove_key({}, 20);
        print_map_foo(map);
        printf("truncate:\n");
        map.truncate(3);
        print_map_foo(map);

        printf("\n");
        // destructor here
    }
    printf("\n\n\nmap keys/values to vec:\n");
    {
        NewerHashMap<int, int> map{c_allocator()};
        for (auto i : std::views::iota(1, 7)) {
            map.put_no_clobber_emplace({}, i, 10 * i);
        }
        {
            Vec<int> keys{c_allocator()};
            keys.append_range(map | std::views::transform([](auto&& ref) { return ref.key; }));
            Vec<int> values{c_allocator()};
            values.append_range(map | std::views::transform([](auto&& ref) { return ref.value; }));
            print_ints(keys, "keys  :");
            print_ints(values, "values:");
        }
        {
            auto keys = Vec<int>::init_capacity(c_allocator(), map.size());
            auto values = Vec<int>::init_capacity(c_allocator(), map.size());
            std::ranges::for_each(map, [&](auto ref) {
                keys.push_back(ref.key);
                values.push_back(ref.value);
            });
            print_ints(keys, "keys  :");
            print_ints(values, "values:");
        }
    }
    printf("\n\n\nget_or_put_entry: disjoint storage\n\n");
    {
        struct Void {};
        using PseudoKey = std::pair<int, int>;
        struct Context {
            Vec<int>* split_table1;
            Vec<int>* split_table2;
            NODISCARD auto hash(PseudoKey key) const -> u32 {
                (void) this;
                Yy_Hasher h{};
                yy_hasher_append_bv(&h, bv_transmute_ptr_one(&key.first));
                yy_hasher_append_bv(&h, bv_transmute_ptr_one(&key.second));
                return yy_hasher_final32(&h);
            };

            NODISCARD auto eql(PseudoKey const& query_key, Void const& stored_key, usize stored_idx) const -> bool {
                (void)stored_key;
                return query_key.first == (*split_table1)[stored_idx] &&
                       query_key.second == (*split_table2)[stored_idx];
            }
        };

        using HashMap = NewerHashMap<Void, Void, Context>;
        HashMap map{c_allocator()};
        Vec<int> split_table1{c_allocator()};
        Vec<int> split_table2{c_allocator()};
        Context ctx{&split_table1, &split_table2};
        auto const key_fn = [](Context const& ctx, PseudoKey const& pseudo_key, usize new_insertion_idx) -> Void {
            yy_assert(ctx.split_table1->size() == new_insertion_idx);
            yy_assert(ctx.split_table2->size() == new_insertion_idx);
            ctx.split_table1->push_back(pseudo_key.first);
            ctx.split_table2->push_back(pseudo_key.second);
            return Void{};
        };
        Vec<int> integers{c_allocator()};
        integers.append_range(std::views::iota(13, 17));
        integers.append_range(std::views::iota(10, 20));
        for (auto i : integers) {
            auto key = PseudoKey(i, i * 10);
            int strategy = i % 3;
            Option<HashMap::Insertions::RefMut> ref = None;
            auto entry = map.get_or_put_entry(ctx, key);
            auto existing = entry.exists();
            switch (strategy) {
                default:
                    yy_unreachable();
                case 0: {
                    ref = entry.map_put_key_value(key_fn, [](auto&&...){ return Void{}; });
                } break;
                case 1: {
                    ref = entry.map_put_key_value(key_fn, Void{});
                } break;
                case 2: {
                    if (!existing) {
                        ref = std::move(entry).unwrap_put().put_key_value(Void{}, Void{});
                        key_fn(ctx, key, ref.unwrap().insertion_idx);
                    } else {
                        ref = entry.unwrap_get();
                    }
                } break;
            }
            if (existing) {
                printf("existing [%zu] from i %d\n", ref.unwrap().insertion_idx, i);
            } else {
                printf("new [%zu] from i %d\n", ref.unwrap().insertion_idx, i);
            }
        }
        auto get_170 = map.get(ctx, PseudoKey(17, 170));
        yy_assert(get_170.unwrap().insertion_idx == 7);
        for (auto ref : map) {
            printf(
                "[%zu] = (%d, %d)\n", ref.insertion_idx, split_table1[ref.insertion_idx],
                split_table2[ref.insertion_idx]
            );
        }
        yy_assert(std::ranges::equal(
            map | std::views::transform([&](auto ref) { return split_table2[ref.insertion_idx]; }),
            std::initializer_list<int>{130, 140, 150, 160, 100, 110, 120, 170, 180, 190}
        ));
    }
}

UNUSED auto test_2024_04_03() -> void {
    printf("Hello, world!\n");
    {
        // // This code is problematic. For some weird reason, it thinks Arr is size 8. This must be a compiler bug.
        // using Arr = char const[2];
        // auto&& chs = Arr{'f', 'o'};
        // static_assert(sizeof(chs) == 2);
        // SpanConst<char> a = chs;
        // printf("size: %zu\n", a.len);
        // print_ints(a, "");
    }
    {
        // This code is problematic. For some weird reason, it thinks Arr is size 8. This must be a compiler bug.
        SpanConst<char> a = "Hello";
        yy_assert(a.len == 5);
        yy_assert(std::char_traits<char>::compare("Hello", a.ptr, a.len) == 0);
        printf("size: %zu\n", a.len);
        print_ints(a, "");
    }
    {
        auto allocator = c_allocator();
        VecUnmanagedLeaked<int> a{};
        std::array ints{1,2,3,4};
        a.append_range(allocator, ints);
        print_ints(a, "");
    }
}

}  // namespace

auto main(int argc, char const* const* argv) -> int {
    yy::the_original_argc_argv = {argc, argv};
    if ((true)) test_vec();
    if ((false)) test_sv();
    if ((true)) test_option();
    if ((false)) test_newer_hash_map();
    if ((false)) test_2024_04_03();
}
