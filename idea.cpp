// vim: set sw=4 et:

typedef unsigned char u8;
typedef unsigned int u32;

typedef u32 TokenId;
typedef u32 AstNodeId;
typedef u32 IdentifierId;

typedef u8 TokenTag;
// TokenUnpacked will be stored in an array of structs for each field.
struct TokenUnpacked {
    // tag is u8
    TokenTag tag;

    // paylaod
    union {
        struct {
            u32 src_start;
            u32 src_len;
        };
        struct {
            u32 as_i64[2];
        };
        IdentifierId identifier_id;
    } pl;

    struct {
        u32 file_id;
        u32 line0;
        u32 column0;
    } position;
};

template<typename T>
struct ExtraId {
    u32 x;
};

typedef AstNodeId AstNodeIdVec[2]; // index, len
struct FnDeclExtra {
    TokenId name;
    AstNodeIdVec args;
    AstNodeId return_type;
    AstNodeId body;
};

typedef u8 AstNodeTag;
// AstNodeUnpacked will be stored in an array of structs for each field.
struct AstNodeUnpacked {
    // ids that refer to the same file are u32
    TokenId token;

    // tag is u8
    AstNodeTag tag;

    union {
        ExtraId<FnDeclExtra> fn_decl;
        AstNodeId bin_op[2];
        IdentifierId identifier;
        AstNodeId decl_ref;
        AstNodeId decl_val;
        AstNodeIdVec block;
    } pl;
};
