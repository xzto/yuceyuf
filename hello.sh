#!/usr/bin/env bash
# vim: set sw=4 et:

# shellcheck disable=SC3043 # Enable `local` variables.
# shellcheck disable=SC2155
# shellcheck enable=require-variable-braces


set -eu
set -o pipefail

ulimit -c 0

pwd_at_start="$(pwd)"
readonly pwd_at_start

die() {
    local code="$1"
    shift
    while [ "$#" -gt 0 ]; do
        echo "$1" >&2
        shift
    done
    exit "${code}"
}

print_var() {
    local name="$1"
    echo "${name} is ${!name@Q}"
}

: "${yy:=./yuceyuf}"
print_var "yy"

# Get the root directory of the project (relative to pwd_at_start)
root_dir="$(dirname "$0")"
readonly root_dir
case "$0" in
    (*/*) ;;
    (*) die 1 "$0: error: failed getting root directory of the project (this should never happen!)" ;;
esac
is_in_root=1
if [ "$(realpath "${pwd_at_start}")" != "$(realpath "${root_dir}")" ]; then
    is_in_root=0
fi
# shellcheck disable=SC2034
readonly is_in_root
# print_var "root_dir"

yy() {
    # echo + ${yy} "$@"
    ${yy} "$@"
}

rename() {
  local from="$1"
  local to="$2"
  shift 2
  from="$(printf %s "${from}" | sed 's/\//\\\//g')"
  to="$(printf %s "${to}"     | sed 's/\//\\\//g')"
  while [ "$#" -gt 0 ]; do
    local filename_old="$1"; shift
    local filename_new="$(printf %s "${filename_old}" | sed "s/${from}/${to}/")"
    if test -f "${filename_old}"; then
      mv -- "${filename_old}" "${filename_new}"
    fi
  done
}

if ! yy --help >/dev/null 2>&1; then
    die 1 "$0: error: bad yuceyuf executable: ${yy}"
fi

set -x

# yy build "${root_dir}/examples/first.yuceyuf"           # --run --verbose-compilation
# yy build "${root_dir}/examples/operators.yuceyuf"       # --run --verbose-compilation
# yy build "${root_dir}/examples/if.yuceyuf"              # --run --verbose-compilation
# yy build "${root_dir}/examples/global_vars.yuceyuf"     # --run --verbose-compilation
# yy build "${root_dir}/examples/import1.yuceyuf"         # --run --verbose-compilation
# yy build "${root_dir}/examples/canonical_import_path/foo.yuceyuf"         # --run --verbose-compilation
# yy build "${root_dir}/examples/use_stdlib.yuceyuf"      --run --verbose-compilation


{ set +e; } 2>/dev/null
(
    { set -e; } 2>/dev/null
    rm -f yuceyuf_{,hello_}output*
    yy ir-test --verbose-genir --verbose-sema "${root_dir}/examples/ir_test7.yuceyuf" --verbose-codegen --asm-out hello.asm
    # yy ir-test --verbose-genir --verbose-sema "${root_dir}/examples/rule_110.yuceyuf" --asm-out hello.asm
    # yy ir-test --verbose-genir --verbose-sema "${root_dir}/examples/rule_110_second.yuceyuf" --asm-out hello.asm
    # yy ir-test --verbose-sema --stop-at-sema "${root_dir}/examples/rule_110_second.yuceyuf" --asm-out hello.asm

    "${root_dir}/link.sh" --asm hello.asm --out hello

    { echo; echo; } 2>/dev/null
    ./hello
)
{ code="$?"; echo; echo; } 2>/dev/null
{ echo "exit code ${code}"; set -e; } 2>/dev/null
{ echo; echo; } 2>/dev/null

time "${root_dir}/test.py" test ../tests        # --verbose
{ echo; echo; } 2>/dev/null
time "${root_dir}/test.py" test ../examples     # --verbose
{ echo; echo; } 2>/dev/null
