#include "yy/basic.hpp"

#include <cstdlib>
#include <cstdio>
#include <cstdarg>

COLD NOINLINE NORETURN PRINTF_LIKE(3, 4) void yy_panic_handler_fmt(const char *OPT file, int OPT line, const char *fmt, ...)
{
    std::fflush(stdout);
    std::fprintf(stderr, "\npanic: ");
    {
        va_list ap;
        va_start(ap, fmt);
        std::vfprintf(stderr, fmt, ap);
        va_end(ap);
    }
    std::fputc('\n', stderr);
    if (file != NULL) {
        std::fprintf(stderr, "%s:%d: panic\n", file, line);
    } else {
        std::fprintf(stderr, "panic\n");
    }
    if (yy::the_original_argc_argv.argv != nullptr) {
        std::fprintf(stderr, "Command invoked as: ");
        for (int i = 0; i < yy::the_original_argc_argv.argc; i += 1) {
            std::fprintf(stderr, "%s%s", yy::the_original_argc_argv.argv[i], i == yy::the_original_argc_argv.argc-1 ? "" : " ");
        }
        std::fprintf(stderr, "\n");
    }
    std::fflush(stderr);
    if constexpr (!YY_DEFINED_NDEBUG) {
        yy_breakpoint();
    }
    std::abort();
}
