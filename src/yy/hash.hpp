#pragma once

#include "yy/basic.hpp"
#include "yy/sv.hpp"

NODISCARD u32 yy_weak_hash_of_bv(Bv bv);

struct Yy_Hasher {
    u64 state;
    Yy_Hasher();
};

// TODO: Use methods and a constructor.
NODISCARD Yy_Hasher yy_hasher_init(void);
void yy_hasher_append_bv(Yy_Hasher *const h, Bv const bv);
void yy_hasher_append_u32(Yy_Hasher *const h, u32 const x);
void yy_hasher_append_u64(Yy_Hasher *const h, u64 const x);
void yy_hasher_append_usize(Yy_Hasher *const h, usize const x);
NODISCARD u32 yy_hasher_final32(Yy_Hasher const *const h);
