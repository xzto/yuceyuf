// NOTE: This file does not have an include guard on the entire file.
// vim: set et sw=4:

// If compiling this file directly (probably lsp), make sure it compiles correctly.
#if defined(YY_IS_LSP) && defined(__GNUC__) && defined(__INCLUDE_LEVEL__) && __INCLUDE_LEVEL__ == 0
# define YY_META_TAGGED_UNION__NAME TaggedUnionTest
# define YY_META_TAGGED_UNION__ALL_MEMBERS                 \
    YY_META_TAGGED_UNION__MEMBER(Void, eof)                \
    YY_META_TAGGED_UNION__MEMBER(struct{i64 i;}, some_foo) /* inline anon struct */ \
    YY_META_TAGGED_UNION__MEMBER(int, a)                   \
    YY_META_TAGGED_UNION__MEMBER(int, a2)                  \
    YY_META_TAGGED_UNION__MEMBER(Void, b)                  \
    YY_META_TAGGED_UNION__MEMBER(std::string, c) /* not trivial_raii */ \
    /*YY_META_TAGGED_UNION__MEMBER(std::unique_ptr<int>, d)*/ /* no copy ctor */ \
    YY_EMPTY_MACRO
#endif

#ifndef ___INCLUDE_GUARD__yCGI3GM7bMMrpNXqEUiFKKGJ1x3kpJIl9q01rKNx
#define ___INCLUDE_GUARD__yCGI3GM7bMMrpNXqEUiFKKGJ1x3kpJIl9q01rKNx

#include "yy/basic.hpp"

// This file must be included once in the global namespace. Afterwards you can include it again to define a type.
YY_CHECK_GLOBAL_NAMESPACE();

#include "yy/meta/enum.hpp"
#include "yy/option.hpp"

#include <type_traits>

#define YY_META_TU_REMOVE_LEADING_COMMA__IMPL(unused, ...) __VA_ARGS__
// Usage: YY_META_TU_REMOVE_LEADING_COMMA(,a0 ,a1 ,a2 ...) => a0, a1, a2 ...
#define YY_META_TU_REMOVE_LEADING_COMMA(...) YY_META_TU_REMOVE_LEADING_COMMA__IMPL(_ __VA_ARGS__)


// This macro is deprecated. Prefer using `tu_pointer->as<tag_value>().unwrap()` or `tu_pointer->as_tag_short().unwrap()`
#define MetaTu_get(Tu, tu_pointer, tag_short) (std::addressof((tu_pointer)->as<YY_META_ENUM___EnumValue(Tu::Tag, tag_short)>().unwrap()))
// This macro is deprecated. Prefer using `Tu::init<tag_value>(...)` or `Tu::init_tag_short(...)`
#define MetaTu_init(Tu, tag_short, ...) (Tu::JOIN2(init_, tag_short)(__VA_ARGS__))

#endif//___INCLUDE_GUARD__yCGI3GM7bMMrpNXqEUiFKKGJ1x3kpJIl9q01rKNx

// This part is always included.

#if defined(YY_META_TAGGED_UNION__NAME) || defined(YY_META_TAGGED_UNION__ALL_MEMBERS)

#ifndef YY_META_TAGGED_UNION__NAME
# error "not defined: YY_META_TAGGED_UNION__NAME"
#endif
#ifndef YY_META_TAGGED_UNION__ALL_MEMBERS
# error "not defined: YY_META_TAGGED_UNION__ALL_MEMBERS"
#endif

// Example
#if 0
#define YY_META_TAGGED_UNION__NAME Thing
// #define YY_META_ENUM__INT u8 // optional
#define YY_META_TAGGED_UNION__ALL_MEMBERS \
     YY_META_TAGGED_UNION__MEMBER2(int, xint, "int") \
     YY_META_TAGGED_UNION__MEMBER(u32, integer) \
     YY_META_TAGGED_UNION__MEMBER(Foo*, foo)
#include "yy/meta/tagged_union.hpp"
#endif

#define YY_META_TAGGED_UNION__MEMBER(Type, name) YY_META_TAGGED_UNION__MEMBER2(Type, name, #name)

// TODO: static constexpr bool all_payloads_different_types; as<Type>(), Tu(Type), operator=(Type)
// TODO: YY_META_TAGGED_UNION__TAG AnotherPreviouslyDefinedMetaEnum

#define Yy_MetaTu__Self YY_META_TAGGED_UNION__NAME
#define Yy_MetaTu__SelfTag JOIN2(Yy_MetaTu__Self, _Tag)
#define Yy_MetaTu__SelfPayload JOIN2(Yy_MetaTu__Self, _Payload)
#define Yy_MetaTu__Self_tag_for(name)  \
    YY_META_ENUM___EnumValue(Yy_MetaTu__SelfTag, name)
#define Yy_MetaTu__SelfTemplate JOIN2(Yy_MetaTu__Self, _Template)
#define Yy_MetaTu__SelfTraits JOIN2(Yy_MetaTu__Self, _Traits)
#define Yy_MetaTu__SelfMemberPtr JOIN2(Yy_MetaTu__Self, _MemberPtr)

#define YY_META_ENUM__NAME Yy_MetaTu__SelfTag
#define YY_META_ENUM__ALL_MEMBERS YY_META_TAGGED_UNION__ALL_MEMBERS
#define YY_META_TAGGED_UNION__MEMBER2(Type, name, short_str) YY_META_ENUM__MEMBER2(name, short_str)
// NOLINTNEXTLINE(readability-duplicate-include)
#include "yy/meta/enum.hpp" // IWYU pragma: keep
#undef YY_META_TAGGED_UNION__MEMBER2


template <typename cplusplus_bullshit=void>
union Yy_MetaTu__SelfPayload;


template <Yy_MetaTu__SelfTag Tag> struct Yy_MetaTu__SelfTraits;
// Define Yy_MetaTu__SelfTraits and do basic checks on the types.
#define YY_META_TAGGED_UNION__MEMBER2(TypeDefinition, name, short_str_unused)                                         \
    template <>                                                                                                       \
    struct Yy_MetaTu__SelfTraits<Yy_MetaTu__Self_tag_for(name)> {                                                     \
        using type = TypeDefinition;                                                                                  \
        static_assert(!std::is_reference_v<type>);                                                                    \
        static_assert(!std::is_array_v<type>);                                                                        \
        static_assert(std::is_destructible_v<type>);                                                                  \
        static constexpr auto tag = Yy_MetaTu__Self_tag_for(name);                                                    \
        static constexpr bool trivial_raii = yy::trivial_raii<type>;                                                  \
        static constexpr bool is_copy_constructible_v = std::is_copy_constructible_v<type>;                           \
        static constexpr bool is_move_constructible_v = std::is_move_constructible_v<type>;                           \
        static constexpr bool is_default_constructible_v = std::is_default_constructible_v<type>;                     \
        static constexpr bool is_trivially_copy_constructible_v = std::is_trivially_copy_constructible_v<type>;       \
        static constexpr bool is_trivially_move_constructible_v = std::is_trivially_move_constructible_v<type>;       \
        static constexpr bool is_trivially_default_constructible_v = std::is_trivially_default_constructible_v<type>; \
        static constexpr bool is_trivially_destructible_v = std::is_trivially_destructible_v<type>;                   \
        static constexpr bool is_equality_comparable_v = std::equality_comparable<type>;                              \
    };                                                                                                                \
    YY_EMPTY_MACRO
YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2

template <typename cplusplus_bullshit>
union Yy_MetaTu__SelfPayload {
    static_assert(std::same_as<void, cplusplus_bullshit>);

    // Don't mess up the default constructor for this union if any of the types are not trivially constructible.
    struct {
    } __meta_tu_default_init{};
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str) \
        Yy_MetaTu__SelfTraits<Yy_MetaTu__Self_tag_for(name)>::type name;
    YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2

    template <Yy_MetaTu__SelfTag Tag>
    using tag_to_payload_type_traits = Yy_MetaTu__SelfTraits<Tag>;
    template <Yy_MetaTu__SelfTag Tag>
    using tag_to_payload_type_t = tag_to_payload_type_traits<Tag>::type;

    template <typename... Traits>
    struct all_payloads_impl {
        static constexpr Yy_MetaTu__SelfTag tag_array[] = {Traits::tag...};
        static constexpr bool trivial_raii = (... && Traits::trivial_raii);
        static constexpr bool is_copy_constructible_v = (... && Traits::is_copy_constructible_v);
        static constexpr bool is_move_constructible_v = (... && Traits::is_move_constructible_v);
        static constexpr bool is_default_constructible_v = (... && Traits::is_default_constructible_v);
        static constexpr bool is_trivially_copy_constructible_v = (... && Traits::is_trivially_copy_constructible_v);
        static constexpr bool is_trivially_move_constructible_v = (... && Traits::is_trivially_move_constructible_v);
        static constexpr bool is_trivially_default_constructible_v =
            (... && Traits::is_trivially_default_constructible_v);
        static constexpr bool is_trivially_destructible_v = (... && Traits::is_trivially_destructible_v);
        static constexpr bool is_equality_comparable_v = (... && Traits::is_equality_comparable_v);
    };
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused) \
    /* keep the leading comma*/, tag_to_payload_type_traits<Yy_MetaTu__Self_tag_for(name)>
    using all_payloads_t = all_payloads_impl<YY_META_TU_REMOVE_LEADING_COMMA(YY_META_TAGGED_UNION__ALL_MEMBERS)>;
#undef YY_META_TAGGED_UNION__MEMBER2
    static constexpr bool all_payloads_trivial_raii = all_payloads_t::trivial_raii;
    static constexpr bool all_payloads_is_copy_constructible_v = all_payloads_t::is_copy_constructible_v;
    static constexpr bool all_payloads_is_move_constructible_v = all_payloads_t::is_move_constructible_v;
    static constexpr bool all_payloads_is_default_constructible_v = all_payloads_t::is_default_constructible_v;
    static constexpr bool all_payloads_is_trivially_copy_constructible_v =
        all_payloads_t::is_trivially_copy_constructible_v;
    static constexpr bool all_payloads_is_trivially_move_constructible_v =
        all_payloads_t::is_trivially_move_constructible_v;
    static constexpr bool all_payloads_is_trivially_default_constructible_v =
        all_payloads_t::is_trivially_default_constructible_v;
    static constexpr bool all_payloads_is_trivially_destructible_v = all_payloads_t::is_trivially_destructible_v;
    static constexpr bool all_payloads_is_equality_comparable_v = all_payloads_t::is_equality_comparable_v;


    constexpr Yy_MetaTu__SelfPayload(const Yy_MetaTu__SelfPayload&) = default;
    constexpr Yy_MetaTu__SelfPayload(Yy_MetaTu__SelfPayload&&) = default;
    constexpr Yy_MetaTu__SelfPayload& operator=(const Yy_MetaTu__SelfPayload&) = default;
    constexpr Yy_MetaTu__SelfPayload& operator=(Yy_MetaTu__SelfPayload&&) = default;
    constexpr Yy_MetaTu__SelfPayload()
        requires (all_payloads_is_default_constructible_v)
    = default;
    constexpr Yy_MetaTu__SelfPayload()
        requires (!all_payloads_is_default_constructible_v)
    {}
    constexpr ~Yy_MetaTu__SelfPayload()
        requires (all_payloads_is_trivially_destructible_v)
    = default;
    // TODO: workaround for compiler error "constexpr_dtor_subobject"
    // constexpr ~Yy_MetaTu__SelfPayload() requires (!all_payloads_trivial_raii) {}
    ~Yy_MetaTu__SelfPayload()
        requires (!all_payloads_is_trivially_destructible_v)
    {}
};

template <Yy_MetaTu__SelfTag Tag> struct Yy_MetaTu__SelfMemberPtr;
// Define Yy_MetaTu__SelfTraits and do basic checks on the types.
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused) \
    template <>                                                            \
    struct Yy_MetaTu__SelfMemberPtr<Yy_MetaTu__Self_tag_for(name)> {       \
        static constexpr auto member_ptr = &Yy_MetaTu__SelfPayload<>::name;  \
        using type = ::yy::member_type_t<member_ptr>;                      \
    };                                                                     \
    YY_EMPTY_MACRO
YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2


// The template is required to use requires on untemplated functions.
template <typename cplusplus_bullshit=void>
struct Yy_MetaTu__SelfTemplate {
    static_assert(std::same_as<void, cplusplus_bullshit>);

    using Tag = Yy_MetaTu__SelfTag;
    using Payload = Yy_MetaTu__SelfPayload<>;

    Tag tag{yy::meta_enum_count<Tag>};
    Payload payload{};

    template <Tag tag_template> using tag_to_payload_type_traits = Payload::template tag_to_payload_type_traits<tag_template>;
    template <Tag tag_template> using tag_to_payload_type_t = Payload::template tag_to_payload_type_t<tag_template>;
    using all_payloads_t = Payload::all_payloads_t;
    static constexpr bool all_payloads_trivial_raii = all_payloads_t::trivial_raii;
    static constexpr bool all_payloads_is_copy_constructible_v = all_payloads_t::is_copy_constructible_v;
    static constexpr bool all_payloads_is_move_constructible_v = all_payloads_t::is_move_constructible_v;
    static constexpr bool all_payloads_is_default_constructible_v = all_payloads_t::is_default_constructible_v;
    static constexpr bool all_payloads_is_trivially_copy_constructible_v = all_payloads_t::is_trivially_copy_constructible_v;
    static constexpr bool all_payloads_is_trivially_move_constructible_v = all_payloads_t::is_trivially_move_constructible_v;
    static constexpr bool all_payloads_is_trivially_default_constructible_v = all_payloads_t::is_trivially_default_constructible_v;
    static constexpr bool all_payloads_is_trivially_destructible_v = all_payloads_t::is_trivially_destructible_v;
    static constexpr bool all_payloads_is_equality_comparable_v = all_payloads_t::is_equality_comparable_v;


    template <Tag tag_template>
    INLINE NODISCARD constexpr auto is() const& -> bool { return tag == tag_template; }

    template <Tag tag_template>
    INLINE NODISCARD constexpr auto as() && -> Option<tag_to_payload_type_t<tag_template> &&> {
        if (this->tag != tag_template) return None;
        return std::move(payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr));
    }
    template <Tag tag_template>
    INLINE NODISCARD constexpr auto as() const& -> Option<tag_to_payload_type_t<tag_template> const&> {
        if (this->tag != tag_template) return None;
        return payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr);
    }
    template <Tag tag_template>
    INLINE NODISCARD constexpr auto as() & -> Option<tag_to_payload_type_t<tag_template> &> {
        if (this->tag != tag_template) return None;
        return payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr);
    }
    /// init by copy constructing the payload
    template <Tag tag_template>
    INLINE NODISCARD static constexpr auto init(tag_to_payload_type_t<tag_template> const& x) -> Yy_MetaTu__SelfTemplate
        requires std::is_constructible_v<tag_to_payload_type_t<tag_template>, decltype(x)>
    {
        Yy_MetaTu__SelfTemplate result;
        result.tag = tag_template;
        std::construct_at(std::addressof(result.payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr)), x);
        return result;
    }
    /// init by move constructing the payload
    template <Tag tag_template>
    INLINE NODISCARD static constexpr auto init(tag_to_payload_type_t<tag_template>&& x) -> Yy_MetaTu__SelfTemplate
        requires std::is_constructible_v<tag_to_payload_type_t<tag_template>, decltype(x)>
    {
        Yy_MetaTu__SelfTemplate result;
        result.tag = tag_template;
        std::construct_at(std::addressof(result.payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr)), std::move(x));
        return result;
    }
    /// init by constructing the payload in place
    template <Tag tag_template>
    INLINE NODISCARD static constexpr auto init(auto&&... args) -> Yy_MetaTu__SelfTemplate
        requires std::is_constructible_v<tag_to_payload_type_t<tag_template>, decltype(args)...>
    {
        Yy_MetaTu__SelfTemplate result;
        result.tag = tag_template;
        std::construct_at(std::addressof(result.payload.*(Yy_MetaTu__SelfMemberPtr<tag_template>::member_ptr)), std::forward<decltype(args)>(args)...);
        return result;
    }

#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str)                        \
    INLINE NODISCARD constexpr auto JOIN2(is_, name)() const& -> decltype(auto)                           \
    { return std::forward<Yy_MetaTu__SelfTemplate const&>(*this).template is<Yy_MetaTu__Self_tag_for(name)>(); } \
    INLINE NODISCARD constexpr auto JOIN2(as_, name)() const& -> decltype(auto)                           \
    { return std::forward<Yy_MetaTu__SelfTemplate const&>(*this).template as<Yy_MetaTu__Self_tag_for(name)>(); } \
    INLINE NODISCARD constexpr auto JOIN2(as_, name)() & -> decltype(auto)                                \
    { return std::forward<Yy_MetaTu__SelfTemplate &>(*this).template as<Yy_MetaTu__Self_tag_for(name)>(); } \
    INLINE NODISCARD constexpr auto JOIN2(as_, name)() && -> decltype(auto)                               \
    { return std::forward<Yy_MetaTu__SelfTemplate &&>(*this).template as<Yy_MetaTu__Self_tag_for(name)>(); } \
    /* init by constructing the payload in place */ \
    INLINE NODISCARD static constexpr auto JOIN2(init_, name)(auto&&... args) -> Yy_MetaTu__SelfTemplate                      \
    { return Yy_MetaTu__SelfTemplate::init<Yy_MetaTu__Self_tag_for(name)>(std::forward<decltype(args)>(args)...); } \
    /* init by copy constructing the payload */ \
    INLINE NODISCARD static constexpr auto JOIN2(init_, name)(tag_to_payload_type_t<Yy_MetaTu__Self_tag_for(name)> const& x) -> Yy_MetaTu__SelfTemplate \
    { return Yy_MetaTu__SelfTemplate::init<Yy_MetaTu__Self_tag_for(name)>(std::forward<decltype(x)>(x)); } \
    /* init by move constructing the payload */ \
    INLINE NODISCARD static constexpr auto JOIN2(init_, name)(tag_to_payload_type_t<Yy_MetaTu__Self_tag_for(name)> && x) -> Yy_MetaTu__SelfTemplate \
    { return Yy_MetaTu__SelfTemplate::init<Yy_MetaTu__Self_tag_for(name)>(std::forward<decltype(x)>(x)); } \
    YY_EMPTY_MACRO
    YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2

    NODISCARD constexpr auto operator==(Yy_MetaTu__SelfTemplate const& b) const -> bool
        requires (all_payloads_is_equality_comparable_v)
    {
        auto const& a = *this;
        static_assert(all_payloads_is_equality_comparable_v);
        if (a.tag != b.tag)
            return false;
        switch (a.tag) {
            case yy::meta_enum_count_switch_t{}: return true;
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused)     \
            case Yy_MetaTu__Self_tag_for(name):                                     \
                return a.payload.name == b.payload.name;                            \
            YY_EMPTY_MACRO
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }

    /// Call `visitor` once on `as<tag>().unwrap()` and return the invoke result. Asserts the tag is valid.
    NODISCARD constexpr auto visit(auto&& visitor) & -> decltype(auto) {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: yy_unreachable();
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused)             \
            case Yy_MetaTu__Self_tag_for(name):                                             \
                return std::invoke(std::forward<decltype(visitor)>(visitor), payload.name); \
            YY_EMPTY_MACRO
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }
    /// Call `visitor` once on `as<tag>().unwrap()` and return the invoke result. Asserts the tag is valid.
    NODISCARD constexpr auto visit(auto&& visitor) const& -> decltype(auto) {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: yy_unreachable();
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused)             \
            case Yy_MetaTu__Self_tag_for(name):                                             \
                return std::invoke(std::forward<decltype(visitor)>(visitor), payload.name); \
            YY_EMPTY_MACRO
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }

    template <typename TestType>
    NODISCARD constexpr auto type_is() const& -> bool {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: return false;
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused)                           \
            case Yy_MetaTu__Self_tag_for(name):                                                      \
                return std::same_as<TestType, tag_to_payload_type_t<Yy_MetaTu__Self_tag_for(name)>>; \
            YY_EMPTY_MACRO
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }

    constexpr Yy_MetaTu__SelfTemplate(Yy_MetaTu__SelfTemplate && other) requires (all_payloads_is_trivially_move_constructible_v) = default;
    constexpr Yy_MetaTu__SelfTemplate(Yy_MetaTu__SelfTemplate && other) requires (!all_payloads_is_trivially_move_constructible_v && all_payloads_is_move_constructible_v)
    : tag(other.tag)
    {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: {} break;
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused)                 \
            case Yy_MetaTu__Self_tag_for(name): {                                               \
                std::construct_at(std::addressof(payload.name), std::move(other.payload.name)); \
            } break;                                                                            \
        YY_EMPTY_MACRO
                YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }
    constexpr Yy_MetaTu__SelfTemplate(Yy_MetaTu__SelfTemplate const& other) requires (all_payloads_is_trivially_copy_constructible_v) = default;
    constexpr Yy_MetaTu__SelfTemplate(Yy_MetaTu__SelfTemplate const& other) requires (!all_payloads_is_trivially_copy_constructible_v && all_payloads_is_copy_constructible_v)
    : tag(other.tag)
    {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: {} break;
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused) \
            case Yy_MetaTu__Self_tag_for(name): { std::construct_at(std::addressof(payload.name), other.payload.name); } break;
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }

    constexpr auto operator=(Yy_MetaTu__SelfTemplate&& other) -> Yy_MetaTu__SelfTemplate&
        requires (all_payloads_is_trivially_move_constructible_v) = default;
    constexpr auto operator=(Yy_MetaTu__SelfTemplate&& other) -> Yy_MetaTu__SelfTemplate&
        requires (!all_payloads_is_trivially_move_constructible_v && all_payloads_is_move_constructible_v)
    {
        if (this != &other) {
            std::destroy_at(this);
            std::construct_at(this, std::move(other));
        }
        return *this;
    }

    constexpr auto operator=(Yy_MetaTu__SelfTemplate const& other) -> Yy_MetaTu__SelfTemplate&
        requires (all_payloads_is_trivially_copy_constructible_v) = default;
    constexpr auto operator=(Yy_MetaTu__SelfTemplate const& other) -> Yy_MetaTu__SelfTemplate&
        requires (!all_payloads_is_trivially_copy_constructible_v && all_payloads_is_copy_constructible_v)
    {
        if (this != &other) {
            std::destroy_at(this);
            std::construct_at(this, other);
        }
        return *this;
    }

    constexpr Yy_MetaTu__SelfTemplate() {}

    constexpr ~Yy_MetaTu__SelfTemplate() requires (all_payloads_is_trivially_destructible_v) = default;
    // TODO: constexpr destructor
    ~Yy_MetaTu__SelfTemplate() requires (!all_payloads_is_trivially_destructible_v) {
        switch (tag) {
            case yy::meta_enum_count_switch_t{}: {} break;
#define YY_META_TAGGED_UNION__MEMBER2(Type_unused, name, short_str_unused) \
            case Yy_MetaTu__Self_tag_for(name): { std::destroy_at(std::addressof(payload.name)); } break;
            YY_META_TAGGED_UNION__ALL_MEMBERS
#undef YY_META_TAGGED_UNION__MEMBER2
        }
    }
};
using Yy_MetaTu__Self = Yy_MetaTu__SelfTemplate<>;
static_assert(Yy_MetaTu__Self::all_payloads_trivial_raii == yy::trivial_raii<Yy_MetaTu__Self>);
static_assert(Yy_MetaTu__Self::all_payloads_is_equality_comparable_v == std::equality_comparable<Yy_MetaTu__Self>);

#endif

#undef YY_META_TAGGED_UNION__MEMBER

#undef YY_META_TAGGED_UNION__NAME
#undef YY_META_TAGGED_UNION__ALL_MEMBERS

#undef Yy_MetaTu__Self
#undef Yy_MetaTu__SelfTemplate
#undef Yy_MetaTu__SelfTag
#undef Yy_MetaTu__SelfPayload
#undef Yy_MetaTu__SelfTraits
#undef Yy_MetaTu__SelfMemberPtr
#undef Yy_MetaTu__Self_tag_for


#if defined(YY_IS_LSP) && defined(__GNUC__) && defined(__INCLUDE_LEVEL__) && __INCLUDE_LEVEL__ == 0

static inline auto foobar_baz_compiler_errors_tagged_union() -> void {
    // (void) TaggedUnionTest::init_c();
    (void) TaggedUnionTest::init_some_foo({.i=1});
    (void) TaggedUnionTest::init<TaggedUnionTest_Tag__some_foo>({.i=1});
    // (void) TaggedUnionTest::init<yy::meta_enum_count<TaggedUnionTest_Tag>>({.i=1}); // not allowed
    TaggedUnionTest t1;
    TaggedUnionTest t2;
    // (void) (t1 == t2);
    t1 = TaggedUnionTest{};
    t1 = t2;
    static_assert(!TaggedUnionTest::all_payloads_is_equality_comparable_v);
    t1 = std::move(t2);

    // static_assert(TaggedUnionTest::all_payloads_trivial_raii);

    (void) t1.as_a();
    (void) t1.as_b();
    (void) t1.as<TaggedUnionTest_Tag__a2>();
    // (void) t1.as_c();
}

#endif
