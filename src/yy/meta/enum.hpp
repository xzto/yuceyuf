// NOTE: This file does not have an include guard on the entire file.
// vim: set et sw=4:

// Dummy variables for LSP.
#if defined(YY_IS_LSP) && defined(__GNUC__) && defined(__INCLUDE_LEVEL__) && __INCLUDE_LEVEL__ == 0
# define YY_META_ENUM__NAME EnumTest
# define YY_META_ENUM__INT u8
# define YY_META_ENUM__ALL_MEMBERS YY_META_ENUM__MEMBER(a)
#endif

#ifndef ___INCLUDE_GUARD__9KJ5Uhu4YrW3H8jjsuEO5wnsOOQgRUwGnyfw39a6
#define ___INCLUDE_GUARD__9KJ5Uhu4YrW3H8jjsuEO5wnsOOQgRUwGnyfw39a6


#include "yy/basic.hpp"

// This file must be included once in the global namespace. Afterwards you can include it again to define a type.
YY_CHECK_GLOBAL_NAMESPACE();

#include "yy/sv.hpp"

#include <array>

#define YY_META_ENUM___LegacyEnumNameFor(Enum, name) JOIN2(Enum, JOIN2(__, name))
#define YY_META_ENUM___EnumValue(Enum, name) Enum :: name

#define MetaEnum__count_enum_name(Enum) JOIN2(__META_ENUM_COUNT__, Enum)

// This macro is deprecated. Prefer using meta_enum_traits<Enum>
#define MetaEnum_Traits(Enum) decltype(__meta_enum_traits_adl(std::declval<Enum>()))
// This macro is deprecated. Prefer using meta_enum_count<Enum>
#define MetaEnum_count(Enum) MetaEnum_Traits(Enum)::count
// This macro is deprecated. Prefer using meta_enum_count_int<Enum>
#define MetaEnum_count_int(Enum) MetaEnum_Traits(Enum)::count_int
// This macro is deprecated. Prefer using meta_enum_sv_short(e)
#define MetaEnum_sv_short(Enum, e) MetaEnum_Traits(Enum)::sv_short(e)
// This macro is deprecated. Prefer using meta_enum_sv_full(e)
#define MetaEnum_sv_full(Enum, e) MetaEnum_Traits(Enum)::sv_full(e)


namespace yy {

// Use ADL to check if this is an enum.
template <typename E>
concept is_meta_enum = std::is_enum_v<E> && requires {
    { __meta_enum_traits_adl(std::declval<E>()) };
};

template <is_meta_enum E>
using meta_enum_traits = MetaEnum_Traits(E);

template <is_meta_enum E>
static inline constexpr auto meta_enum_count_int = meta_enum_traits<E>::count_int;
template <is_meta_enum E>
static inline constexpr auto meta_enum_count = meta_enum_traits<E>::count;

// `meta_enum_count_switch_t{}` can be used in a switch statement.
struct meta_enum_count_switch_t {
    template <is_meta_enum E> constexpr operator E() const { return meta_enum_count<E>;}
};

inline constexpr auto meta_enum_sv_short(is_meta_enum auto e) -> Sv { return meta_enum_traits<decltype(e)>::sv_short(e); }
inline constexpr auto meta_enum_sv_full(is_meta_enum auto e) -> Sv { return meta_enum_traits<decltype(e)>::sv_full(e); }

template <is_meta_enum E>
struct MetaEnumRange {
    static auto constexpr enum_count = std::underlying_type_t<E>(meta_enum_count<E>);
    struct Iterator {
        using underlying_type = std::underlying_type_t<E>;
        underlying_type iter;
        using value_type = E;
        using iterator_category = std::random_access_iterator_tag;
        using difference_type = std::
            conditional_t<(sizeof(std::underlying_type_t<E>) < 4), i32, std::make_signed_t<std::underlying_type_t<E>>>;
        using unsigned_difference_type = std::make_unsigned_t<difference_type>;
        constexpr Iterator() : iter(0) {}
        constexpr Iterator(E e) : iter(underlying_type(e)) { yy_assert(iter >= 0 && iter <= enum_count); }
        constexpr friend auto operator==(Iterator, Iterator) -> bool = default;
        constexpr friend auto operator<=>(Iterator a, Iterator b) -> std::strong_ordering = default;
        constexpr auto operator*() const -> value_type { yy_assert(iter >= 0 && iter < enum_count); return E(iter); }
        constexpr auto operator++(int) -> Iterator { auto result = *this; ++*this; return result; }
        constexpr auto operator--(int) -> Iterator { auto result = *this; --*this; return result; }
        constexpr auto operator++() -> Iterator& { ++iter; return *this; }
        constexpr auto operator--() -> Iterator& { --iter; return *this; }
        constexpr friend auto operator+(Iterator a, unsigned_difference_type b) -> Iterator {
            return Iterator{static_cast<value_type>(unsigned_difference_type(a.iter) + b)};
        }
        constexpr friend auto operator+(Iterator a, difference_type b) -> Iterator {
            return Iterator{static_cast<value_type>(difference_type(a.iter) + b)};
        }
        constexpr friend auto operator+(difference_type b, Iterator a) -> Iterator {
            return Iterator{static_cast<value_type>(difference_type(a.iter) + b)};
        }
        constexpr friend auto operator+(unsigned_difference_type b, Iterator a) -> Iterator {
            return Iterator{static_cast<value_type>(unsigned_difference_type(a.iter) + b)};
        }
        constexpr friend auto operator-(Iterator a, unsigned_difference_type b) -> Iterator {
            return Iterator{static_cast<value_type>(unsigned_difference_type(a.iter) - b)};
        }
        constexpr friend auto operator-(Iterator a, difference_type b) -> Iterator {
            return Iterator{static_cast<value_type>(difference_type(a.iter) - b)};
        }
        constexpr friend auto operator-(Iterator a, Iterator b) -> difference_type {
            return static_cast<difference_type>(a.iter) - static_cast<difference_type>(b.iter);
        }
        constexpr auto operator[](usize i) const -> value_type { return *(*this + i); }
        constexpr auto operator+=(difference_type x) -> Iterator& { *this = *this + x; return *this; }
        constexpr auto operator+=(unsigned_difference_type x) -> Iterator& { *this = *this + x; return *this; }
        constexpr auto operator-=(difference_type x) -> Iterator& { *this = *this - x; return *this; }
        constexpr auto operator-=(unsigned_difference_type x) -> Iterator& { *this = *this - x; return *this; }
    };
    static_assert(std::random_access_iterator<Iterator>);

    NODISCARD constexpr auto begin() const -> Iterator { return static_cast<E>(0); };
    NODISCARD constexpr auto end() const -> Iterator { return meta_enum_count<E>; };
    NODISCARD constexpr auto size() const -> usize { return static_cast<usize>(enum_count); }
};

template <is_meta_enum E>
inline constexpr auto meta_enum_range() -> MetaEnumRange<E> { return {}; }

template <is_meta_enum E, typename T>
struct meta_enum_array : public std::array<T, meta_enum_count_int<E>> {
    using std::array<T, meta_enum_count_int<E>>::array;
    using ArrayType = std::array<T, meta_enum_count_int<E>>;
    NODISCARD constexpr auto operator[](usize) &      -> T&       = delete;
    NODISCARD constexpr auto operator[](usize) const& -> T const& = delete;
    NODISCARD constexpr auto operator[](E index) &      -> T&       { return as_std_array()[usize(index)]; }
    NODISCARD constexpr auto operator[](E index) const& -> T const& { return as_std_array()[usize(index)]; }
    NODISCARD constexpr auto as_std_array() & -> ArrayType & { return static_cast<ArrayType&>(*this); }
    NODISCARD constexpr auto as_std_array() const& -> ArrayType const& { return static_cast<ArrayType const&>(*this); }

    NODISCARD static constexpr auto filled(T const& x) -> meta_enum_array {
        meta_enum_array result;
        result.fill(x);
        return result;
    }
};

}

template <yy::is_meta_enum E>
struct yy_invalidOf_StructType<E> : public std::integral_constant<E, yy::meta_enum_count<E>> {};

#endif//___INCLUDE_GUARD__9KJ5Uhu4YrW3H8jjsuEO5wnsOOQgRUwGnyfw39a6


// This part is always included.

#if defined(YY_META_ENUM__NAME) || defined(YY_META_ENUM__ALL_MEMBERS) || defined(YY_META_ENUM__INT)

// TODO: Maybe define a macro YY_META_ENUM__INT_DEFAULT and mandate either YY_META_ENUM__INT_DEFAULT or
// YY_META_ENUM__INT. Or make it `u8` by default.

#ifndef YY_META_ENUM__NAME
# error "not defined: YY_META_ENUM__NAME"
#endif
#ifndef YY_META_ENUM__ALL_MEMBERS
# error "not defined: YY_META_ENUM__ALL_MEMBERS"
#endif
// YY_META_ENUM__INT is optional.

// Example
#if 0
#define YY_META_ENUM__NAME Thing
// #define YY_META_ENUM__INT u8 // optional
#define YY_META_ENUM__ALL_MEMBERS \
     YY_META_ENUM__MEMBER2(xreturn, "return") \
     YY_META_ENUM__MEMBER(foo) \
     YY_META_ENUM__MEMBER(bar)
#include "yy/meta/enum.hpp"
#endif

#define YY_META_ENUM__MEMBER(name) YY_META_ENUM__MEMBER2(name, #name)

enum class YY_META_ENUM__NAME
#ifdef YY_META_ENUM__INT
: YY_META_ENUM__INT
#endif
{
#define YY_META_ENUM__MEMBER2(name, short_str) name,
    YY_META_ENUM__ALL_MEMBERS
#undef YY_META_ENUM__MEMBER2
    MetaEnum__count_enum_name(YY_META_ENUM__NAME),
};

// Compatibility: define Enum__foo = Enum::foo
#define YY_META_ENUM__MEMBER2(name, short_str)                                                                 \
    static constexpr YY_META_ENUM__NAME YY_META_ENUM___LegacyEnumNameFor(YY_META_ENUM__NAME, name) = \
        YY_META_ENUM__NAME::name;
YY_META_ENUM__ALL_MEMBERS
#undef YY_META_ENUM__MEMBER2
static constexpr YY_META_ENUM__NAME MetaEnum__count_enum_name(YY_META_ENUM__NAME) =
    YY_META_ENUM__NAME::MetaEnum__count_enum_name(YY_META_ENUM__NAME);


struct JOIN2(__meta_enum_traits_struct__, YY_META_ENUM__NAME);
// __meta_enum_traits_adl doesn't have a body on purpose. It's only meant to be used with decltype to get the traits type.
inline consteval auto __meta_enum_traits_adl(YY_META_ENUM__NAME)
    -> JOIN2(__meta_enum_traits_struct__, YY_META_ENUM__NAME);

struct JOIN2(__meta_enum_traits_struct__, YY_META_ENUM__NAME) {
    using underlying_type = std::underlying_type_t<YY_META_ENUM__NAME>;
    static constexpr YY_META_ENUM__NAME count = YY_META_ENUM__NAME::MetaEnum__count_enum_name(YY_META_ENUM__NAME);
    static constexpr underlying_type count_int = underlying_type(count);
    NODISCARD static auto sv_short(YY_META_ENUM__NAME e) -> Sv {
        usize const i = (usize)e;
        yy_assert(i < usize(count));
        static constexpr Sv const arr[usize(count)] = {
#define YY_META_ENUM__MEMBER2(name, short_str) (short_str),
            YY_META_ENUM__ALL_MEMBERS
#undef YY_META_ENUM__MEMBER2
        };
        return arr[i];

    }

    NODISCARD static auto sv_full(YY_META_ENUM__NAME e) -> Sv
    {
        usize const i = (usize)e;
        yy_assert(i < usize(count));
        static constexpr Sv const arr[usize(count)] = {
#define YY_META_ENUM__MEMBER2(name, short_str) (STRINGIFY(YY_META_ENUM___EnumValue(YY_META_ENUM__NAME, name))),
            YY_META_ENUM__ALL_MEMBERS
#undef YY_META_ENUM__MEMBER2
        };
        return arr[i];
    }
};

static_assert(yy::is_meta_enum<YY_META_ENUM__NAME>);
static_assert(std::ranges::sized_range<yy::MetaEnumRange<YY_META_ENUM__NAME>>);
static_assert(std::ranges::random_access_range<yy::MetaEnumRange<YY_META_ENUM__NAME>>);

#undef YY_META_ENUM__MEMBER
#undef YY_META_ENUM__MEMBER2
#undef YY_META_ENUM__NAME
#undef YY_META_ENUM__INT
#undef YY_META_ENUM__ALL_MEMBERS

#endif


#if defined(YY_IS_LSP) && defined(__GNUC__) && defined(__INCLUDE_LEVEL__) && __INCLUDE_LEVEL__ == 0
inline auto foobar_baz_compiler_errors_meta_enum() {
    for (auto a: yy::meta_enum_range<EnumTest>()) {
        (void)a;
    }
}
#endif
