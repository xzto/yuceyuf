#include "yy/allocator.hpp"

#include <stdlib.h>
#include <string.h>

#include <sys/mman.h> // for mmap, munmap
#include <unistd.h> // for sysconf

/// Safe wrappers that check for many invalid situations.
void allocator_proc_free(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len
)
{
    if (old_mem_ptr == NULL) yy_assert(old_mem_len == 0);
    if (old_mem_len != 0)
        allocator.vtable->free(allocator.opaque_handle, old_mem_ptr, old_mem_len);
}

NODISCARD YyStatus allocator_proc_resize_in_place(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    if (old_mem_ptr == NULL) yy_assert(old_mem_len == 0);
    yy_assert(yy_is_power_of_two(new_align));
    yy_assert(old_mem_ptr != Yy_Allocator_Result__failure);
    if (new_len == 0) {
        // TODO: Technically, `old_mem_ptr` might not have been aligned. I should probably change
        // the allocator interface from `new_align` to `old_mem_align` and always assert old_mem_ptr
        // was already aligned.
        todo_assert(yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr)));
        allocator_proc_free(allocator, old_mem_ptr, old_mem_len);
        return Yy_Ok;
    }
    YyStatus const result = allocator.vtable->resize_in_place(
                                     allocator.opaque_handle,
                                     old_mem_ptr, old_mem_len,
                                     new_align, new_len);
    bool const is_failure = yy_is_err(result);
    bool const old_ptr_is_aligned = yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr));
    if (!old_ptr_is_aligned) {
        yy_assert(is_failure);
    } else if (new_len <= old_mem_len) {
        yy_assert(!is_failure);
    }
    if (!is_failure) {
        if (old_mem_ptr == NULL) yy_assert(new_len == 0);
    }
    return result;
}

NODISCARD Yy_Allocator_Result allocator_proc_realloc(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    if (old_mem_ptr == NULL) yy_assert(old_mem_len == 0);
    yy_assert(yy_is_power_of_two(new_align));
    yy_assert(old_mem_ptr != Yy_Allocator_Result__failure);
    if (new_len == 0) {
        allocator_proc_free(allocator, old_mem_ptr, old_mem_len);
        yy_assert(new_align != 0);
        void *const new_ptr = (void *)new_align;
        return new_ptr;
    }
    Yy_Allocator_Result result = allocator.vtable->realloc(
            allocator.opaque_handle,
            old_mem_ptr, old_mem_len,
            new_align, new_len);
    bool const is_failure = Yy_Allocator_Result__is_failure(result);
    bool const old_ptr_is_aligned = yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr));
    if (new_len == 0 || (new_len <= old_mem_len && old_ptr_is_aligned))
        yy_assert(!is_failure);
    if (!is_failure) {
        yy_assert(yy_is_aligned_power_of_two(new_align, (uintptr_t)(result)));
        if (result == NULL) yy_assert(new_len == 0);
    }
    return result;
}

NODISCARD usize allocator_proc_align_size_forward(
    Yy_Allocator allocator,
    usize size
)
{
    usize const result = allocator.vtable->align_size_forward(allocator.opaque_handle, size);
    if (size == 0 || result == 0) yy_assert(result == 0 && size == 0);
    yy_assert(result >= size);
    return result;
}


NODISCARD usize allocator_align_size_forward(Yy_Allocator allocator, usize size)
{
    return allocator_proc_align_size_forward(allocator, size);
}

NODISCARD Yy_Allocator_Result allocator_alloc_align_size(
    Yy_Allocator allocator,
    usize new_align,
    usize new_len
)
{
    return allocator_proc_realloc(allocator, NULL, 0, new_align, new_len);
}

void allocator_free(
    Yy_Allocator allocator,
    void *old_mem_ptr,
    usize old_mem_len
)
{
    allocator_proc_free(allocator, old_mem_ptr, old_mem_len);
}

void allocator_shrink_in_place(
    Yy_Allocator allocator,
    void *old_mem_ptr,
    usize old_mem_len,
    usize new_len
)
{
    yy_assert(new_len <= old_mem_len);
    if (new_len != old_mem_len) {
        YyStatus ok = allocator_proc_resize_in_place(allocator, old_mem_ptr, old_mem_len, 1, new_len);
        yy_assert(yy_is_ok(ok));
    }
}



static usize get_page_size(void)
{
    // TODO: Atomic
    static usize cached_value = 0;
    usize got_page_size = cached_value;
    if (got_page_size == 0) {
        // Initialize it.
        long const rc = sysconf(_SC_PAGE_SIZE);
        if (rc <= 0)
            yy_panic("sysconf(_SC_PAGE_SIZE) returned <= 0");
        got_page_size = (usize)rc;
        cached_value = got_page_size;
        yy_assert(yy_is_power_of_two(got_page_size));
    }
    yy_assert(got_page_size != 0);
    return got_page_size;
}


// C allocator implementation

static void raw_c_allocator_raw_proc_free(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len
)
{
    yy_assert(opaque_handle == NULL);
    yy_assert(old_mem_len != 0);
    free(old_mem_ptr);
}

NODISCARD static YyStatus raw_c_allocator_raw_proc_resize_in_place(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    yy_assert(new_len != 0);
    yy_assert(opaque_handle == NULL);
    if (!yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr)))
        return Yy_Bad;
    if (new_len <= old_mem_len)
        return Yy_Ok;
    return Yy_Bad;
}

NODISCARD static Yy_Allocator_Result raw_c_allocator_raw_proc_realloc(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    yy_assert(new_len != 0);
    yy_assert(opaque_handle == NULL);
    // old_mem_ptr is invalid when the length is zero.
    void *const old_c_ptr = old_mem_len == 0 ? NULL : old_mem_ptr;
    bool needs_move;
    void *new_ptr;
    if (new_align > alignof(void *)) {
        usize const aligned_new_len = yy_align_forward_power_of_two(new_align, new_len);
        usize const aligned_old_len = yy_align_forward_power_of_two(new_align, old_mem_len);
        if (aligned_new_len == aligned_old_len && yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr)))
            return old_mem_ptr;
        needs_move = true;
        new_ptr = aligned_alloc(new_align, aligned_new_len);
    } else {
        needs_move = false;
        new_ptr = realloc(old_c_ptr, new_len);
    }
    yy_assert(new_ptr != Yy_Allocator_Result__failure);
    if (new_ptr == NULL && new_len != 0) {
        // libc realloc failed
        // `Yy_Allocator`'s API differs from C in this case: shrinking never fails if the alignment is correct.
        if (new_len <= old_mem_len && yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_c_ptr)))
            return old_c_ptr;
        return Yy_Allocator_Result__failure;
    }
    if (needs_move) {
        usize const size_to_copy = yy_min(new_len, old_mem_len);
        if (size_to_copy > 0)
            memcpy(new_ptr, old_mem_ptr, size_to_copy);
        free(old_c_ptr);
    }
    yy_assert(yy_is_aligned_power_of_two(new_align, (uintptr_t)(new_ptr)));
    // success
    return new_ptr;
}

NODISCARD static usize raw_c_allocator_raw_proc_align_size_forward(
    void *opaque_handle,
    usize size
)
{
    (void) opaque_handle;
    usize const page_size = get_page_size();
    usize const threshold_to_page_align = page_size * 4;
    usize const len_alignment = size < threshold_to_page_align ? 2*sizeof(usize) : page_size;
    usize const result = yy_align_forward_power_of_two(len_alignment, size);
    return result;
}

static Yy_Allocator_Vtable const raw_c_allocator_vtable = {
    .free = raw_c_allocator_raw_proc_free,
    .resize_in_place = raw_c_allocator_raw_proc_resize_in_place,
    .realloc = raw_c_allocator_raw_proc_realloc,
    .align_size_forward = raw_c_allocator_raw_proc_align_size_forward,
};

NODISCARD Yy_Allocator c_allocator(void)
{
    Yy_Allocator const result = {
        .opaque_handle = NULL,
        .vtable = &raw_c_allocator_vtable,
    };
    return result;
}



// Page allocator implementation
static void page_allocator_raw_proc_free(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len_unaligned
)
{
    yy_assert(opaque_handle == NULL);
    yy_assert(old_mem_len_unaligned != 0);
    usize const page_size = get_page_size();
    yy_assert(yy_is_power_of_two(page_size));

    usize const fixed_old_mem_len = yy_align_forward_power_of_two(page_size, old_mem_len_unaligned);
    if (fixed_old_mem_len != 0) {
        int const rc = munmap(old_mem_ptr, fixed_old_mem_len);
        if (rc != 0) yy_panic_fmt("munmap returned error: %d", rc);
    }
}

NODISCARD static YyStatus page_allocator_raw_proc_resize_in_place(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len_unaligned,
    usize new_align,
    usize new_len_unaligned
)
{
    yy_assert(opaque_handle == NULL);
    yy_assert(new_len_unaligned != 0);
    usize const page_size = get_page_size();
    yy_assert(yy_is_power_of_two(page_size));

    usize const fixed_new_len = yy_align_forward_power_of_two(page_size, new_len_unaligned);
    usize const fixed_old_mem_len = yy_align_forward_power_of_two(page_size, old_mem_len_unaligned);

    if (!yy_is_aligned_power_of_two(new_align, (uintptr_t)(old_mem_ptr)))
        return Yy_Bad;
    if (fixed_new_len == fixed_old_mem_len)
        return Yy_Ok;
    if (fixed_new_len < fixed_old_mem_len) {
        void *const to_free_ptr = (void *)((uintptr_t)(old_mem_ptr) + fixed_new_len);
        usize const to_free_len = fixed_old_mem_len - fixed_new_len;;
        int const rc = munmap(to_free_ptr, to_free_len);
        if (rc != 0) yy_panic_fmt("munmap returned error: %d", rc);
        return Yy_Ok;
    }
    return Yy_Bad;
}

NODISCARD static Yy_Allocator_Result page_allocator_raw_proc_realloc(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len_unaligned,
    usize new_align,
    usize new_len_unaligned
)
{
    yy_assert(opaque_handle == NULL);
    yy_assert(new_len_unaligned != 0);
    usize const page_size = get_page_size();
    yy_assert(yy_is_power_of_two(page_size));

    usize const fixed_new_len = yy_align_forward_power_of_two(page_size, new_len_unaligned);
    usize const fixed_old_mem_len = yy_align_forward_power_of_two(page_size, old_mem_len_unaligned);

    // try to use the same pointer
    if (yy_is_ok(page_allocator_raw_proc_resize_in_place(opaque_handle,
                 old_mem_ptr, old_mem_len_unaligned,
                 new_align, new_len_unaligned)))
        return old_mem_ptr;

    // bigger size or bigger alignment
    // TODO: `mremap` for linux specific.
    // TODO: choose addr_hint
    void *const addr_hint = NULL;
    yy_assert(fixed_new_len > 0);
    void *const new_ptr = mmap(addr_hint, fixed_new_len, PROT_READ|PROT_WRITE,
                               MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
    if (new_ptr == MAP_FAILED)
        return Yy_Allocator_Result__failure;
    yy_assert(new_ptr != Yy_Allocator_Result__failure);
    yy_assert(new_ptr != NULL);
    if (!yy_is_aligned_power_of_two(new_align, (uintptr_t)(new_ptr)))
        yy_panic_fmt("%s: TODO: arbitrary alignment: %zu", __func__, new_align);
    if (fixed_old_mem_len > 0) {
        usize const size_to_copy = yy_min(old_mem_len_unaligned, new_len_unaligned);
        yy_assert(size_to_copy > 0);
        memcpy(new_ptr, old_mem_ptr, size_to_copy);
        int const rc = munmap(old_mem_ptr, fixed_old_mem_len);
        if (rc != 0) yy_panic_fmt("munmap returned error: %d", rc);
    }
    // success
    return new_ptr;
}

NODISCARD static usize page_allocator_raw_proc_align_size_forward(
    void *opaque_handle,
    usize size
)
{
    yy_assert(opaque_handle == NULL);
    usize const page_size = get_page_size();
    yy_assert(yy_is_power_of_two(page_size));

    usize const result = yy_align_forward_power_of_two(page_size, size);
    return result;
}

static Yy_Allocator_Vtable const page_allocator_vtable = {
    .free = page_allocator_raw_proc_free,
    .resize_in_place = page_allocator_raw_proc_resize_in_place,
    .realloc = page_allocator_raw_proc_realloc,
    .align_size_forward = page_allocator_raw_proc_align_size_forward,
};

NODISCARD Yy_Allocator page_allocator(void)
{
    Yy_Allocator const result = {
        .opaque_handle = NULL,
        .vtable = &page_allocator_vtable,
    };
    return result;
}


NODISCARD SvMut allocator_dup_sv(Yy_Allocator allocator, Sv sv)
{
    char *new_ptr = allocator_alloc_array_aligned(allocator, char, 1, sv.len);
    allocator_assert_ok(new_ptr);
    std::ranges::copy(sv, new_ptr);
    return SvMut(new_ptr, sv.len);
}
