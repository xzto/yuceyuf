#pragma once
// vim: set et sw=4:

#include "yy/basic.hpp"

#include <concepts>
#include <functional>
#include <limits>
#include <new>
#include <type_traits>
#include <utility>

#define TRY_OPTION__IMPL(tmp_try_, x)    \
  (__extension__ ({                      \
    auto tmp_try_ = (x);                 \
    if (tmp_try_.is_none()) [[unlikely]] \
      return None;                       \
    std::move(tmp_try_).unwrap();        \
  }))
// Unwrap `x` if valid. If None, return None.
#define TRY_OPTION(x) TRY_OPTION__IMPL(JOIN2(tmp_try__, __COUNTER__), x)

namespace yy {

static constexpr struct NoneType {
} None;

template <typename T, typename E>
class NODISCARD Result;

namespace options_private {

// TODO: Traits::zeroes_is_none
struct Traits {
    bool as_ref_const{false};
    bool as_ref_mut{false};
    char const* debug_name;
};

template <typename T>
class Members;


/// Option<X&>
template <typename Ref>
    requires std::is_lvalue_reference_v<Ref>
class Members<Ref> {
    using Pointer = std::remove_reference_t<Ref>*;
    Pointer m_ptr{nullptr};

public:
    static constexpr auto traits = Traits{
        .debug_name = "reference",
    };
    INLINE constexpr Members() = default;
    INLINE constexpr ~Members() = default;
    INLINE constexpr Members(Members const& other) = default;
    INLINE constexpr Members(Members&& other) = default;
    INLINE constexpr Members& operator=(Members const& other) = default;
    INLINE constexpr Members& operator=(Members&& other) = default;

    INLINE constexpr Members(NoneType) : m_ptr(nullptr) {}
    INLINE constexpr Members(Ref ref) : m_ptr(std::addressof(ref)) {}

    INLINE NODISCARD constexpr auto has_value() const -> bool { return m_ptr != nullptr; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const& -> Ref { return *m_ptr; }
};

/// Option<bool>
template <>
class Members<bool> {
    union Union {
        u8 as_int{sentinel_none};
        bool as_bool;
    } m;
    static constexpr u8 sentinel_none = 2;

public:
    static constexpr auto traits = Traits{
        .as_ref_const = true,
        .as_ref_mut = true,
        .debug_name = "bool",
    };
    INLINE constexpr Members() = default;
    INLINE constexpr ~Members() = default;
    INLINE constexpr Members(Members const& other) = default;
    INLINE constexpr Members(Members&& other) = default;
    INLINE constexpr Members& operator=(Members const& other) = default;
    INLINE constexpr Members& operator=(Members&& other) = default;

    INLINE constexpr Members(NoneType) : m{Union{.as_int=sentinel_none}} {}
    INLINE constexpr Members(bool boolean) : m{Union{.as_bool=boolean}} {}
    INLINE NODISCARD constexpr auto has_value() const -> bool { return m.as_int != sentinel_none; }
    INLINE NODISCARD constexpr auto unsafe_ref() const& -> bool const& { return m.as_bool; }
    INLINE NODISCARD constexpr auto unsafe_ref() & -> bool& { return m.as_bool; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const& -> bool const& { return unsafe_ref(); }
    INLINE NODISCARD constexpr auto unsafe_unwrap() & -> bool& { return unsafe_ref(); }
};

// Option<T> where T is empty
template <typename T>
    requires std::is_empty_v<T> && std::is_trivially_default_constructible_v<T> && yy::trivial_raii_maybe_moveonly<T> &&
             (std::alignment_of_v<T> == 1)
class Members<T> {
    bool m_has{false};
    [[no_unique_address]] T data;

public:
    static constexpr auto traits = Traits{
        .as_ref_const = true,
        .as_ref_mut = true,
        .debug_name = "empty",
    };
    INLINE constexpr Members() = default;
    INLINE constexpr ~Members() = default;
    INLINE constexpr Members(Members const& other) = default;
    INLINE constexpr Members(Members&& other) = default;
    INLINE constexpr Members& operator=(Members const& other) = default;
    INLINE constexpr Members& operator=(Members&& other) = default;

    INLINE constexpr Members(NoneType) : m_has(false) {}
    INLINE constexpr Members(T) : m_has(true) {}
    INLINE NODISCARD constexpr auto has_value() const -> bool { return m_has; }
    INLINE NODISCARD constexpr auto unsafe_ref() const& -> T const& { return data; }
    INLINE NODISCARD constexpr auto unsafe_ref() & -> T& { return data; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const& -> T const& { return unsafe_ref(); }
    INLINE NODISCARD constexpr auto unsafe_unwrap() & -> T& { return unsafe_ref(); }
};

/// An Option<T> where `sentinel_none` is used to indicate None.
template <typename T, T sentinel_none>
    requires yy::trivial_raii_maybe_moveonly<T>
class MembersTrivialSentinel {
    T m_value_or_sentinel{sentinel_none};

public:
    INLINE NODISCARD constexpr auto unsafe_ref() -> T& { return m_value_or_sentinel; }
    INLINE NODISCARD constexpr auto unsafe_ref() const -> T const& { return m_value_or_sentinel; }
    INLINE constexpr auto unsafe_set_value_uninitialized() -> void { m_value_or_sentinel = yy::uninitialized; }
    static constexpr auto traits = Traits{
        .as_ref_const = true,
        .as_ref_mut = true,
        .debug_name = "trivial sentinel",
    };
    INLINE constexpr MembersTrivialSentinel() = default;
    INLINE constexpr ~MembersTrivialSentinel() = default;
    INLINE constexpr MembersTrivialSentinel(MembersTrivialSentinel const& other) = default;
    INLINE constexpr MembersTrivialSentinel(MembersTrivialSentinel&& other) = default;
    INLINE constexpr MembersTrivialSentinel& operator=(MembersTrivialSentinel const& other) = default;
    INLINE constexpr MembersTrivialSentinel& operator=(MembersTrivialSentinel&& other) = default;
    INLINE constexpr MembersTrivialSentinel& operator=(T other) {
        this->~MembersTrivialSentinel();
        new(this) MembersTrivialSentinel(std::move(other));
        return *this;
    }
    INLINE constexpr MembersTrivialSentinel& operator=(auto&& arg)
        requires std::is_constructible_v<T, decltype(arg)>
    {
        this->~MembersTrivialSentinel();
        new(this) MembersTrivialSentinel(std::move(T(std::forward<decltype(arg)>(arg))));
        return *this;
    }

    INLINE constexpr MembersTrivialSentinel(decltype(nullptr)) requires std::is_pointer_v<T> = delete;
    INLINE constexpr MembersTrivialSentinel(NoneType) : m_value_or_sentinel(sentinel_none) {}
    INLINE constexpr MembersTrivialSentinel(T other) : m_value_or_sentinel(std::move(other)) {
        yy_assert(m_value_or_sentinel != sentinel_none);
    }
    INLINE constexpr MembersTrivialSentinel(std::convertible_to<T> auto&& arg)
    : m_value_or_sentinel(std::forward<decltype(arg)>(arg)) {
        yy_assert(m_value_or_sentinel != sentinel_none);
    }
    template <typename... Args>
    INLINE constexpr MembersTrivialSentinel(std::in_place_t, Args&&... args)
        : m_value_or_sentinel(std::forward<decltype(args)>(args)...) {
        yy_assert(m_value_or_sentinel != sentinel_none);
    }

    INLINE NODISCARD constexpr auto has_value() const -> bool { return m_value_or_sentinel != sentinel_none; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const& -> T const& { return unsafe_ref(); }
    INLINE NODISCARD constexpr auto unsafe_unwrap() & -> T& { return unsafe_ref(); }
};

template <typename T>
    requires std::is_pointer_v<T>
class Members<T> : public MembersTrivialSentinel<T, nullptr> {};

#if 0
template <typename Enum>
    requires std::is_enum_v<Enum>
class Members<Enum> {
    using Int = std::underlying_type_t<Enum>;
    // TODO: Technically, max() may be a valid enum...
    static constexpr Int sentinel_none = std::numeric_limits<Int>::max();
    Int m_value_or_sentinel{sentinel_none};

public:
    INLINE NODISCARD constexpr auto unsafe_value() const -> Enum { return static_cast<Enum>(m_value_or_sentinel); }
    static constexpr auto traits = options_private::Traits{
        .debug_name = "enum",
    };
    INLINE constexpr Members() = default;
    INLINE constexpr ~Members() = default;
    INLINE constexpr Members(Members const& other) = default;
    INLINE constexpr Members(Members&& other) = default;
    INLINE constexpr Members& operator=(Members const& other) = default;
    INLINE constexpr Members& operator=(Members&& other) = default;

    INLINE constexpr Members(NoneType) : m_value_or_sentinel(sentinel_none) {}
    INLINE constexpr Members(Enum other) {
        yy_assert(static_cast<Int>(other) != sentinel_none);
        m_value_or_sentinel = static_cast<Int>(other);
    }

    INLINE NODISCARD constexpr auto has_value() const -> bool { return m_value_or_sentinel != sentinel_none; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const -> Enum { return unsafe_value(); }
};
#endif

/// Option<T> if `yy_invalidOf_StructType<T>` exists.
/// TODO: Find a nice way to make a type expose a sentinel to Option.
template <typename T>
    requires yy::trivial_raii_maybe_moveonly<T> && (!std::is_pointer_v<T>) && requires { yy_invalidOf_StructType<T>::value; }
class Members<T> : public MembersTrivialSentinel<T, yy_invalidOf_StructType<T>::value> {};

// Option<X> generic.
template <typename T>
class Members {
    enum class Has : u8 {
        /// No value. The default.
        no,
        /// Has a value.
        yes,
    };
    Has m_has{Has::no};
    // TODO: Rewrite with a union, using std::construct_at(std::addressof(raw_union.t)).
    alignas(T) std::byte m_bytes[sizeof(T)];

public:
    INLINE NODISCARD constexpr auto unsafe_ref() & -> T& { return *reinterpret_cast<T*>(m_bytes); }
    INLINE NODISCARD constexpr auto unsafe_ref() const& -> T const& { return *reinterpret_cast<T const*>(m_bytes); }
    INLINE NODISCARD constexpr auto unsafe_ref() && -> T&& { return std::move(*reinterpret_cast<T*>(m_bytes)); }
    INLINE constexpr auto unsafe_set_value_uninitialized() -> void { *this = T(yy::uninitialized); }
    static constexpr auto traits = options_private::Traits{
        .as_ref_const = true,
        .as_ref_mut = true,
        .debug_name = "generic",
    };
    INLINE constexpr ~Members()
        requires (std::is_trivially_destructible_v<T>)
    = default;
    INLINE constexpr ~Members()
        requires (!std::is_trivially_destructible_v<T>) && std::is_destructible_v<T>
    {
        if (m_has != Has::no) {
            unsafe_ref().~T();
        }
    }
    INLINE constexpr Members(Members const& other)
        requires (std::is_trivially_copy_constructible_v<T>)
    = default;
    INLINE constexpr Members(Members const& other)
        requires (!std::is_trivially_copy_constructible_v<T>) && std::is_copy_constructible_v<T>
        : m_has(other.m_has) {
        if (m_has == Has::yes) {
            new (m_bytes) T(other.unsafe_unwrap());
        }
    }
    INLINE constexpr Members(Members&& other)
        requires (std::is_trivially_move_constructible_v<T>)
    = default;
    INLINE constexpr Members(Members&& other)
        requires (!std::is_trivially_move_constructible_v<T>) && std::is_move_constructible_v<T>
        : m_has(other.m_has == Has::yes ? Has::yes : Has::no) {
        if (m_has == Has::yes) {
            new (m_bytes) T(std::move(other).unsafe_unwrap());
        }
    }
    INLINE constexpr Members& operator=(Members const& other)
        requires (std::is_trivially_copy_assignable_v<T>)
    = default;
    INLINE constexpr Members& operator=(Members const& other)
        requires (!std::is_trivially_copy_assignable_v<T>) && std::is_copy_constructible_v<T>
    {
        if (this != &other) {
            this->~Members();
            new (this) Members(other);
        }
        return *this;
    }
    INLINE constexpr Members& operator=(Members&& other)
        requires (std::is_trivially_move_assignable_v<T>)
    = default;
    INLINE constexpr Members& operator=(Members&& other)
        requires (!std::is_trivially_move_assignable_v<T>) && std::is_move_constructible_v<T>
    {
        if (this != &other) {
            this->~Members();
            new (this) Members(std::move(other));
        }
        return *this;
    }

    INLINE constexpr Members() = default;
    INLINE constexpr Members(NoneType) : m_has(Has::no) {}
    INLINE constexpr Members(T& other) : m_has(Has::yes) { new (m_bytes) T(other); }
    INLINE constexpr Members(T const& other) : m_has(Has::yes) { new (m_bytes) T(other); }
    INLINE constexpr Members(T&& other) : m_has(Has::yes) { new (m_bytes) T(std::move(other)); }
    INLINE constexpr Members(T const&& other) : m_has(Has::yes) { new (m_bytes) T(std::move(other)); }
    INLINE constexpr Members(std::convertible_to<T> auto&& other) : m_has(Has::yes) {
        new (m_bytes) T(std::forward<decltype(other)>(other));
    }
    template <typename... Args>
    INLINE constexpr Members(std::in_place_t, Args&&... args) : m_has(Has::yes) {
        new (m_bytes) T(std::forward<Args>(args)...);
    }

    INLINE constexpr auto operator=(NoneType) -> Members& {
        this->~Members();
        m_has = Has::no;
        return *this;
    }
    INLINE constexpr auto operator=(auto&& arg) -> Members&
        requires std::is_constructible_v<Members, decltype(arg)>
    {
        this->~Members();
        new (this) Members(std::forward<decltype(arg)>(arg));
        return *this;
    }

    INLINE NODISCARD constexpr auto has_value() const -> bool { return m_has == Has::yes; }
    INLINE NODISCARD constexpr auto unsafe_unwrap() & -> T& { return unsafe_ref(); }
    INLINE NODISCARD constexpr auto unsafe_unwrap() const& -> T const& { return unsafe_ref(); }
    INLINE NODISCARD constexpr auto unsafe_unwrap() && -> T&& { return std::move(*this).unsafe_ref(); }
};


}  // namespace options_private

// TODO: Specialization for void, empty struct, etc.
template <typename T>
class NODISCARD Option {
private:
    static_assert(
        (!yy::same_as_remove_cvref<void, T>) && (!yy::same_as_remove_cvref<NoneType, T>) &&
        (!std::is_rvalue_reference_v<T>) && (!std::is_array_v<T>)
    );
    using Members = options_private::Members<T>;
    Members m;
    static_assert((yy::trivial_raii<T> || std::is_reference_v<T>) == yy::trivial_raii<Members>);

public:
    using value_type = T;

    constexpr ~Option() = default;
    // `Option()` constructor must be explicit because initializing with `= {}` is ambiguous between constructing the
    // `Option` and constructing the inner `T`.
    constexpr explicit Option() : m(None) {}
    constexpr Option(Option&) = default;
    constexpr Option(Option const&) = default;
    constexpr Option(Option&&) = default;
    constexpr Option& operator=(Option const&) = default;
    constexpr Option& operator=(Option&&) = default;
    constexpr Option(NoneType) : m(None) {}

    /// This clones the option itself. To map an Option<T&> to a cloned Option<T>, use `cloned`.
    constexpr auto clone(auto&&... inner_clone_args) const -> Option
        requires yy::can_clone_with_args<T, decltype(inner_clone_args)...>
    {
        if (has_value()) {
            Option result{
                yy::clone<T>(m.unsafe_unwrap(), std::forward<decltype(inner_clone_args)>(inner_clone_args)...)
            };
            return result;
        }
        return None;
    }

    /// Maps an Option<T&> to an Option<T> by `yy::clone` the contents.
    constexpr auto cloned(auto&&... inner_clone_args) const -> Option<std::remove_reference_t<T>>
        requires std::is_lvalue_reference_v<T> && yy::can_clone_with_args<T, decltype(inner_clone_args)...>
    {
        if (has_value()) {
            Option<std::remove_reference_t<T>> result{yy::clone<std::remove_reference_t<T>>(
                m.unsafe_unwrap(), std::forward<decltype(inner_clone_args)>(inner_clone_args)...
            )};
            return result;
        }
        return None;
    }

    template <std::convertible_to<T> U>
    constexpr Option(Option<U>&& other)
        requires (!std::same_as<T, U>) && (!std::is_constructible_v<Members, decltype(other)>) &&
                 ((std::is_lvalue_reference_v<U>) == std::is_lvalue_reference_v<T>)
        : Option(std::forward<decltype(other)>(other).map([](auto&& u) -> T { return std::forward<decltype(u)>(u); })) { }

    template <std::convertible_to<T> U>
    constexpr Option(Option<U>& other)
        requires (!std::same_as<T, U>) && (!std::is_constructible_v<Members, decltype(other)>) &&
                 ((std::is_lvalue_reference_v<U>) == std::is_lvalue_reference_v<T>)
        : Option(std::forward<decltype(other)>(other).map([](auto&& u) -> T { return std::forward<decltype(u)>(u); })) {}

    template <std::convertible_to<T> U>
    constexpr Option(Option<U> const& other)
        requires (!std::same_as<T, U>) && (!std::is_constructible_v<Members, decltype(other)>) &&
                 ((std::is_lvalue_reference_v<U>) == std::is_lvalue_reference_v<T>)
        : Option(std::forward<decltype(other)>(other).map([](auto&& u) -> T { return std::forward<decltype(u)>(u); })) { }


    // TODO: check if these are necessary
    constexpr Option(T x) requires std::is_lvalue_reference_v<T> : m(x) {}
    // constexpr Option(T& x) requires (!std::is_lvalue_reference_v<T>) : m(x) {}
    // constexpr Option(T const& x) requires (!std::is_lvalue_reference_v<T>) : m(x) {}
    constexpr Option(T&& x) requires (!std::is_lvalue_reference_v<T>) : m(std::move(x)) {}
    // constexpr Option(T const&& x) requires (!std::is_lvalue_reference_v<T>) : m(std::move(x)) {}

    constexpr Option(std::convertible_to<T> auto&& arg) : m(std::forward<decltype(arg)>(arg)) {}
    template <typename... Args>
    constexpr Option(std::in_place_t, Args&&... args) : m(std::forward<Args>(args)...) {}



    constexpr auto operator=(auto&& arg) -> Option&
        requires std::is_assignable_v<Members, decltype(arg)>
    {
        m = std::forward<decltype(arg)>(arg);
        return *this;
    }

    static constexpr auto traits = Members::traits;

    NODISCARD constexpr auto has_value() const -> bool { return m.has_value(); }
    NODISCARD constexpr auto is_some() const -> bool { return m.has_value(); }
    NODISCARD constexpr auto is_none() const -> bool { return !m.has_value(); }
    NODISCARD constexpr auto unwrap() & -> decltype(auto) {
        yy_assert(has_value());
        return m.unsafe_unwrap();
    }
    NODISCARD constexpr auto unwrap() const& -> decltype(auto) {
        yy_assert(has_value());
        return m.unsafe_unwrap();
    }
    // TODO: Maybe this should _not_ return an rvalue reference after all. I have tested on godbolt and If we
    // do `auto&& dangling = make_option().unwrap();` we do get a dangling reference. Gcc 13+ seems to give us a warning
    // (-Wdangling-reference, enabled by -Wextra). Clang 18 does _not_ have this warning. This codebase may be full or
    // those bugs and I wouldn't know, because I'm not compiling with a recent version gcc.
    NODISCARD constexpr auto unwrap() && -> decltype(auto) {
        yy_assert(has_value());
        return std::move(m).unsafe_unwrap();
    }
    NODISCARD constexpr auto unwrap() const&& -> decltype(auto) {
        yy_assert(has_value());
        return std::move(m).unsafe_unwrap();
    }

    constexpr auto unwrap_none() const -> void { yy_assert(is_none()); }

    /// Note: `f` can't return an rvalue ref, because we can't make Option out of that.
    template <class F>
        requires std::invocable<F, decltype(std::declval<Option const&>().unwrap())>
    NODISCARD constexpr auto map(F&& f) const&
        -> Option<std::invoke_result_t<F, decltype(std::declval<Option const&>().unwrap())>> {
        if (has_value())
            return std::invoke(std::forward<F>(f), std::forward<Option const&>(*this).m.unsafe_unwrap());
        return None;
    }
    /// Note: `f` can't return an rvalue ref, because we can't make Option out of that.
    template <class F>
        requires std::invocable<F, decltype(std::declval<Option&>().unwrap())>
    NODISCARD constexpr auto map(F&& f) &
        -> Option<std::invoke_result_t<F, decltype(std::declval<Option&>().unwrap())>> {
        if (has_value())
            return std::invoke(std::forward<F>(f), std::forward<Option&>(*this).m.unsafe_unwrap());
        return None;
    }
    /// Note: `f` can't return an rvalue ref, because we can't make Option out of that.
    template <class F>
        requires std::invocable<F, decltype(std::declval<Option&&>().unwrap())>
    NODISCARD constexpr auto map(F&& f) &&
        -> Option<std::invoke_result_t<F, decltype(std::declval<Option&&>().unwrap())>> {
        if (has_value())
            return std::invoke(std::forward<F>(f), std::forward<Option&&>(*this).m.unsafe_unwrap());
        return None;
    }

    NODISCARD constexpr auto unwrap_or(std::convertible_to<T> auto&& defaulted) const& -> T {
        if (has_value())
            return std::forward<Option const&>(*this).m.unsafe_unwrap();
        return std::forward<decltype(defaulted)>(defaulted);
    }
    NODISCARD constexpr auto unwrap_or(std::convertible_to<T> auto&& defaulted) && -> T {
        if (has_value())
            return std::forward<Option&&>(*this).m.unsafe_unwrap();
        return std::forward<decltype(defaulted)>(defaulted);
    }
    NODISCARD constexpr auto unwrap_or_else(auto&& fn) const& -> T
        requires std::is_invocable_r_v<T, decltype(fn)>
    {
        if (has_value())
            return std::forward<Option const&>(*this).m.unsafe_unwrap();
        return std::invoke(std::forward<decltype(fn)>(fn));
    }
    NODISCARD constexpr auto unwrap_or_else(auto&& fn) && -> T
        requires std::is_invocable_r_v<T, decltype(fn)>
    {
        if (has_value())
            return std::forward<Option&&>(*this).m.unsafe_unwrap();
        return std::invoke(std::forward<decltype(fn)>(fn));
    }

    NODISCARD constexpr auto as_ref() & -> Option<T&>
        requires (traits.as_ref_mut)
    {
        if (has_value())
            return Option<T&>(m.unsafe_ref());
        return None;
    }
    NODISCARD constexpr auto as_ref() const& -> Option<T const&>
        requires (traits.as_ref_const)
    {
        if (has_value())
            return Option<T const&>(m.unsafe_ref());
        return None;
    }

    // as_deref: Map T* to T&. Or any dereferenceable to the dereference.
    NODISCARD constexpr auto as_deref() const& -> decltype(auto)
        requires requires (T const& x) { *x; }
    {
        using AsDeref = decltype(*m.unsafe_unwrap());
        if (has_value())
            return Option<AsDeref>(*m.unsafe_unwrap());
        return Option<AsDeref>(None);
    }

    NODISCARD constexpr auto as_deref() & -> decltype(auto)
        requires requires (T& x) { *x; }
    {
        using AsDeref = decltype(*m.unsafe_unwrap());
        if (has_value())
            return Option<AsDeref>(*m.unsafe_unwrap());
        return Option<AsDeref>(None);
    }

    /// NOTE: After calling this function, you should consider the Option to have garbage until you write to the
    /// pointer. Destruction is fine.
    /// Caveat 1: If you don't write to the pointer, the Option may or may not be None.
    /// Caveat 2: If this is a trivial sentinel and you happen to write the value of `Members::sentinel_none`, the
    /// Option will be `None`.
    NODISCARD constexpr auto unsafe_out_ptr_assume_init() -> decltype(auto)
        requires (!std::is_lvalue_reference_v<T>)
    {
        // If there is no value, initialize with a value that is valid for destruction.
        if (is_none())
            m.unsafe_set_value_uninitialized();
        // NOTE: If this is a trivial sentinel, it's not guaranteed that the Option will not be None after calling
        // unsafe_set_value_uninitialized. As long as you write to the pointer, it's fine.
        return std::addressof(m.unsafe_ref());
    }

    NODISCARD constexpr auto unwrap_pointer() -> decltype(auto)
        requires (!std::is_lvalue_reference_v<T>)
    {
        yy_assert(has_value());
        return std::addressof(m.unsafe_ref());
    }

    NODISCARD constexpr auto unwrap_pointer() const -> decltype(auto)
        requires (!std::is_lvalue_reference_v<T>)
    {
        yy_assert(has_value());
        return std::addressof(m.unsafe_ref());
    }

    NODISCARD constexpr auto operator==(NoneType) const -> bool {
        return is_none();
    }
    template <typename U>
        requires (!std::same_as<Option, U>) && std::equality_comparable_with<T, U>
    NODISCARD constexpr auto operator==(U const& other) const -> bool {
        return has_value() ? (unwrap() == other) : false;
    }
    template <typename U>
        requires std::equality_comparable_with<T, U>
    NODISCARD constexpr auto operator==(Option<U> const& other) const -> bool {
        auto this_has_value = has_value();
        auto other_has_value = other.has_value();
        return (this_has_value && other_has_value) ? (unwrap() == other.unwrap()) : (this_has_value == other_has_value);
    }

    NODISCARD constexpr auto extract() -> T {
        auto result = std::move(*this).unwrap();
        *this = None;
        return result;
    }

    template <typename F>
    NODISCARD constexpr auto ok_or_with(F&& err_fn) const& -> Result<T, std::invoke_result_t<F>> {
        if (is_some()) {
            return std::forward<Option const&>(*this).unwrap();
        }
        return std::invoke(std::forward<F>(err_fn));
    }
    template <typename F>
    NODISCARD constexpr auto ok_or_with(F&& err_fn) && -> Result<T, std::invoke_result_t<F>> {
        if (is_some()) {
            return std::forward<Option&&>(*this).unwrap();
        }
        return std::invoke(std::forward<F>(err_fn));
    }

    // Option<Option<U> -> Option<U>.
    NODISCARD constexpr auto flatten() const
        requires std::is_class_v<T> && std::same_as<T, Option<typename T::value_type>>
    {
        return this->is_none() ? None : this->unwrap();
    }

    // Option<Option<U>&> -> Option<U&>. Equivalent to `this->map([](auto& x){return x.as_ref();}).flatten()`;
    NODISCARD constexpr auto flatten_as_ref() const
        requires std::is_reference_v<T> && std::is_class_v<std::remove_reference_t<T>> &&
                 std::same_as<std::remove_reference_t<T>, Option<typename std::remove_reference_t<T>::value_type>>
    {
        return this->is_none() ? None : this->unwrap().as_ref();
    }
};

template <typename T>
Option(T&&) -> Option<T>;
template <typename T>
Option(T&) -> Option<T>;
template <typename T>
Option(T const&) -> Option<T>;

template <std::integral T, T sentinel_int>
inline constexpr auto NonSentinelInteger<T, sentinel_int>::try_from(T value_or_sentinel) -> Option<NonSentinelInteger> {
    return (value_or_sentinel == sentinel_int) ? None : Option<NonSentinelInteger>(NonSentinelInteger(value_or_sentinel));
}

}  // namespace yy

using yy::None;
using yy::NoneType;
using yy::Option;
