#pragma once

#include "yy/basic.hpp"
#include "yy/option.hpp"

#include <algorithm>
#include <ranges>
#include <string_view>

namespace yy {

template <typename T>
struct Span {
    T* ptr;
    usize len;

    using Tconst = T const;
    using Tmut = std::remove_const_t<T>;
    using SpanConst = Span<Tconst>;
    using SpanMut = Span<Tmut>;

    constexpr ~Span() = default;
    constexpr Span() = default;
    constexpr Span(Span const&) = default;
    constexpr Span(Span&&) = default;
    constexpr Span& operator=(const Span&) = default;
    constexpr Span& operator=(Span&&) = default;
    constexpr Span(SpanMut const& other)
        requires std::same_as<T, Tconst>
    : ptr(other.ptr), len(other.len) {}

    constexpr Span(Tconst* ptr_, usize len_)
        requires std::same_as<T, Tconst>
    : ptr(ptr_), len(len_) {}
    constexpr Span(Tmut* ptr_, usize len_) : ptr(ptr_), len(len_) {}

    /// Make a slice of this span from `lo` to the end. Equivalent to `slice(lo, size())`.
    NODISCARD constexpr auto slice(usize lo) const -> Span { return slice(lo, size()); }
    /// Make a slice of this span from `lo` to `hi` (not inclusive).
    NODISCARD constexpr auto slice(usize lo, usize hi) const -> Span {
        yy_assert(hi >= lo && hi <= len);
        return Span(ptr + lo, hi - lo);
    }

    // Construct a span from a contiguous range
    constexpr Span(std::ranges::contiguous_range auto&& range)
        : ptr(std::ranges::data(range)), len(std::ranges::size(range)) {}

    // Don't construct from an initializer_list, as it does not expand the lifetime of the array.
    constexpr Span(std::initializer_list<T> list) = delete;

    /// Delete the array constructor because it conflicts with the null terminator constructor.
    template <usize N>
        requires yy::char_type<T>
    constexpr Span(T (&)[N]) = delete;

    // Construct from a string literal.
    constexpr Span(T* null_terminated_string)
        requires yy::char_type<T>
        : ptr(null_terminated_string), len(std::char_traits<std::remove_const_t<T>>::length(null_terminated_string)) {}

    // Convert to std::string_view.
    template <
        typename CharTraitsOfUnsignedCharIsDeprecatedSaysTheCompiler = Tmut,
        typename = std::enable_if<
            yy::char_type<CharTraitsOfUnsignedCharIsDeprecatedSaysTheCompiler>,
            CharTraitsOfUnsignedCharIsDeprecatedSaysTheCompiler>::type>
    constexpr operator std::basic_string_view<CharTraitsOfUnsignedCharIsDeprecatedSaysTheCompiler>() const
        requires std::same_as<Tmut, CharTraitsOfUnsignedCharIsDeprecatedSaysTheCompiler>
    {
        return {ptr, len};
    }


    // TODO: Custom operator==(char const*) when yy::char_type<T>.
    NODISCARD constexpr auto operator==(SpanConst other) const -> bool { return std::ranges::equal(*this, other); }

    using iterator = T*;
    using const_iterator = Tconst*;
    NODISCARD constexpr auto size() const -> usize { return len; }
    NODISCARD constexpr auto empty() const -> bool { return len == 0; }
    NODISCARD constexpr auto data() const -> T* { return ptr; }
    NODISCARD constexpr auto begin() const -> iterator { return ptr; }
    NODISCARD constexpr auto end() const -> iterator { return ptr + len; }
    NODISCARD constexpr auto cdata() const -> Tconst* { return ptr; }
    NODISCARD constexpr auto cbegin() const -> const_iterator { return ptr; }
    NODISCARD constexpr auto cend() const -> const_iterator { return ptr + len; }
    NODISCARD constexpr auto front() const -> T& { return (*this)[0]; }
    NODISCARD constexpr auto back() const -> T& { yy_assert(size() > 0); return (*this)[size() - 1]; }
    NODISCARD constexpr auto operator[](usize i) const -> T& {
        yy_assert(i < len);
        return ptr[i];
    }
    NODISCARD constexpr auto try_get(usize i) const -> Option<T&> {
        return i < len ? Option<T&>(ptr[i]) : None;
    }

    NODISCARD inline constexpr auto as_bytes() const -> Span<u8 const>;
    // TODO: Don't require char.
    template <typename Target>
        requires (std::is_const_v<T> ? std::is_const_v<Target> : true) &&
                     (std::same_as<T const, u8 const> || std::same_as<T const, char const>) &&
                     (std::same_as<Target const, u8 const> || std::same_as<Target const, char const>) &&
                     (sizeof(T) == sizeof(Target))
    NODISCARD constexpr auto transmute() const -> Span<Target> {
        return Span<Target>(reinterpret_cast<Target*>(ptr), yy::div_exact(len * sizeof(T), sizeof(Target)));
    }
};
Span(std::ranges::contiguous_range auto&& range) -> Span<std::ranges::range_value_t<decltype(range)>>;
template <typename T> Span(T*, usize) -> Span<T>;
template <typename T> Span(std::initializer_list<T> const&) -> Span<T>;
template <yy::char_type T> Span(T*) -> Span<T>;
static_assert(std::ranges::contiguous_range<Span<u8>>);
static_assert(std::ranges::sized_range<Span<u8>>);

template <typename T>
using SpanConst = Span<T const>;

template <typename T>
    requires std::same_as<T, std::remove_const_t<T>>
using SpanMut = Span<T>;

// TODO: Make Sv a separate type from Span.
using Sv = Span<char const>;
using SvMut = Span<char>;
// Bv: Bytes View
using Bv = Span<u8 const>;
// BvMut: Bytes View (mutable)
using BvMut = Span<u8>;

namespace literals {
inline constexpr auto operator ""_sv(char const* ptr, usize len) -> Sv { return Sv(ptr, len); }
inline constexpr auto operator ""_bv(char const* ptr, usize len) -> Bv { return Sv(ptr, len).as_bytes(); }
inline constexpr auto operator ""_span(char const* ptr, usize len) -> SpanConst<char> { return SpanConst<char>(ptr, len); }
inline constexpr auto operator ""_span(wchar_t const* ptr, usize len) -> SpanConst<wchar_t> { return SpanConst<wchar_t>(ptr, len); }
inline constexpr auto operator ""_span(char8_t const* ptr, usize len) -> SpanConst<char8_t> { return SpanConst<char8_t>(ptr, len); }
inline constexpr auto operator ""_span(char16_t const* ptr, usize len) -> SpanConst<char16_t> { return SpanConst<char16_t>(ptr, len); }
inline constexpr auto operator ""_span(char32_t const* ptr, usize len) -> SpanConst<char32_t> { return SpanConst<char32_t>(ptr, len); }
}

}  // namespace yy

using yy::Span;
using yy::SpanConst;
using yy::SpanMut;
using yy::Sv;
using yy::SvMut;
using yy::Bv;
using yy::BvMut;


// Macros for `printf`.
#define Sv_Fmt "%.*s"
#define Sv_Arg(sv) \
    ((void)yy_static_assert_and_zero(sizeof((sv).data()[0]) == 1), (int)(sv).size()), (char const*)(sv).data()


#define sv_lit(lit) ::yy::Sv(lit)

// Deprecated in favor of `a == b`.
NODISCARD constexpr bool sv_eql(Sv a, Sv b) { return a == b; }

NODISCARD inline constexpr Sv sv_slice(Sv sv, usize lo, usize hi) { return sv.slice(lo, hi); }

// TODO: Deprecate sv_contains_char once I move to c++23, which has std::ranges::contains
NODISCARD inline constexpr bool sv_contains_char(Sv sv, char x) { return std::ranges::find(sv, x) != sv.end(); }
NODISCARD inline bool bv_contains_byte(Bv bv, u8 x) { return std::ranges::find(bv, x) != bv.end(); }

// TODO: Deprecate sv_starts_with once I move to c++23, which has std::ranges::starts_with.
NODISCARD constexpr bool sv_starts_with(Sv haystack, Sv needle) {
    if (haystack.len < needle.len)
        return false;
    return sv_eql(haystack.slice(0, needle.len), needle);
}

// transmute `ptr: *T, len: usize` into a byte slice.
template <typename T>
    requires std::is_destructible_v<T> && std::is_standard_layout_v<T> && std::has_unique_object_representations_v<T> &&
                 (!std::is_empty_v<T>) && (!requires (T x) { x.as_bytes(); }) && (!requires (T x) { Span(x); })
inline auto bv_transmute_ptr_len(T const* ptr, usize len) -> Bv {
    return Span<u8 const>{reinterpret_cast<u8 const*>(ptr), len * sizeof(ptr[0])};
}
// transmute `ptr: *T` into a byte slice.
template <typename T>
inline auto bv_transmute_ptr_one(T const* ptr) -> Bv { return bv_transmute_ptr_len<T>(ptr, 1); }

template <typename T>
NODISCARD inline constexpr auto Span<T>::as_bytes() const -> Bv { return bv_transmute_ptr_len(ptr, len); }
