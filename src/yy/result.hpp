#pragma once

#include "yy/basic.hpp"
#include "yy/option.hpp"

#define TRY_RESULT__IMPL(tmp_try_, x)                \
    (__extension__ ({                                \
        auto tmp_try_ = (x);                         \
        if (tmp_try_.is_err()) [[unlikely]]          \
            return std::move(tmp_try_).unwrap_err(); \
        std::move(tmp_try_).unwrap();                \
    }))
// Unwrap `x` if ok. If err, return `unwrap_err()`
#define TRY_RESULT(x) TRY_RESULT__IMPL(JOIN2(tmp_try__, __COUNTER__), x)

namespace yy {

struct in_place_ok_t {};
struct in_place_err_t {};

template <typename T, typename E>
class NODISCARD Result {
    static_assert(!std::convertible_to<T, E> && !std::convertible_to<E, T>);
    static_assert(!std::is_array_v<T>);
    static_assert(!std::is_array_v<E>);
    enum class Has : u8 {
        ok,
        err,
        moved,
    };
    Has m_has;
    union U {
        struct {
        } _{};
        T ok;
        E err;
        constexpr U(const U&) = default;
        constexpr U(U&&) = default;
        constexpr U& operator=(const U&) = default;
        constexpr U& operator=(U&&) = default;
        constexpr U() requires (std::is_default_constructible_v<T> && std::is_default_constructible_v<E>) = default;
        constexpr U() requires (!(std::is_default_constructible_v<T> && std::is_default_constructible_v<E>)) {}
        constexpr ~U()
            requires std::is_trivially_destructible_v<T> && std::is_trivially_destructible_v<E>
        = default;
        constexpr ~U()
            requires (!(std::is_trivially_destructible_v<T> && std::is_trivially_destructible_v<E>)) &&
                     (std::is_destructible_v<T> && std::is_destructible_v<E>)
        {}
    } u;

public:
    constexpr Result(in_place_ok_t, auto&&... ok_args)
        requires std::is_constructible_v<T, decltype(ok_args)...>
        : m_has(Has::ok) {
        std::construct_at(std::addressof(u.ok), std::forward<decltype(ok_args)>(ok_args)...);
    }
    constexpr Result(in_place_err_t, auto&&... err_args)
        requires std::is_constructible_v<T, decltype(err_args)...>
        : m_has(Has::err) {
        std::construct_at(std::addressof(u.err), std::forward<decltype(err_args)>(err_args)...);
    }
    constexpr explicit Result() : m_has(Has::moved) {}
    constexpr Result(T const& ok) : m_has(Has::ok) { new (&u.ok) T(ok); }
    constexpr Result(T&& ok) : m_has(Has::ok) { new (&u.ok) T(std::move(ok)); }
    constexpr Result(E const& err) : m_has(Has::err) { new (&u.err) E(err); }
    constexpr Result(E&& err) : m_has(Has::err) { new (&u.err) E(std::move(err)); }
    constexpr Result(std::convertible_to<E> auto&& err) : m_has(Has::err) { new (&u.err) E(std::forward<decltype(err)>(err)); }
    constexpr Result(std::convertible_to<T> auto&& ok) : m_has(Has::ok) { new (&u.ok) T(std::forward<decltype(ok)>(ok)); }
    constexpr Result(Result const& other)
        requires (std::is_trivially_copy_constructible_v<T> && std::is_trivially_copy_constructible_v<E>)
    = default;
    constexpr Result(Result const& other)
        requires (!(std::is_trivially_copy_constructible_v<T> && std::is_trivially_copy_constructible_v<E>)) &&
                 (std::is_copy_constructible_v<T> && std::is_copy_constructible_v<E>)
    {
        m_has = other.m_has;
        if (m_has == Has::ok) {
            new (&u.ok) T(other.u.ok);
        } else if (m_has == Has::err) {
            new (&u.err) E(other.u.err);
        } else {
            yy_unreachable();
        }
    }
    constexpr Result(Result&& other)
        requires (std::is_trivially_move_constructible_v<T> && std::is_trivially_move_constructible_v<E>)
    = default;
    constexpr Result(Result&& other)
        requires (!(std::is_trivially_move_constructible_v<T> && std::is_trivially_move_constructible_v<E>)) &&
                 (std::is_move_constructible_v<T> && std::is_move_constructible_v<E>)
    {
        m_has = other.m_has;
        if (m_has == Has::ok) {
            new (&u.ok) T(std::move(other).u.ok);
        } else if (m_has == Has::err) {
            new (&u.err) E(std::move(other).u.err);
        } else {
            yy_unreachable();
        }
    }

    constexpr auto operator=(Result const& other) -> Result&
        requires (std::is_trivially_copy_constructible_v<T> && std::is_trivially_copy_constructible_v<E>)
    = default;

    constexpr auto operator=(Result const& other) -> Result&
        requires (!(std::is_trivially_copy_constructible_v<T> && std::is_trivially_copy_constructible_v<E>)) &&
                 (std::is_copy_constructible_v<T>&& std::is_copy_constructible_v<E>)
    {
        if (this != &other) {
            std::destroy_at(this);
            std::construct_at(this, std::forward<decltype(other)>(other));
        }
        return *this;
    }

    constexpr auto operator=(Result&& other) -> Result&
        requires (std::is_trivially_move_constructible_v<T> && std::is_trivially_move_constructible_v<E>)
    = default;

    constexpr auto operator=(Result&& other) -> Result&
        requires (!(std::is_trivially_move_constructible_v<T> && std::is_trivially_move_constructible_v<E>)) &&
                 (std::is_move_constructible_v<T>&& std::is_move_constructible_v<E>)
    {
        if (this != &other) {
            std::destroy_at(this);
            std::construct_at(this, std::forward<decltype(other)>(other));
        }
        return *this;
    }
    constexpr auto operator=(auto&& other) -> Result&
        requires std::is_constructible_v<Result, decltype(other)> &&
                 (!std::same_as<Result, std::remove_cvref_t<decltype(other)>>)
    {
        std::destroy_at(this);
        std::construct_at(this, std::forward<decltype(other)>(other));
        return *this;
    }

    constexpr ~Result() requires std::is_trivially_destructible_v<T> && std::is_trivially_destructible_v<E> = default;
    constexpr ~Result()
        requires (!(std::is_trivially_destructible_v<T> && std::is_trivially_destructible_v<E>)) &&
                 (std::is_destructible_v<T> && std::is_destructible_v<E>)
     {
        if (m_has == Has::ok) {
            std::destroy_at(std::addressof(u.ok));
        } else if (m_has == Has::err) {
            std::destroy_at(std::addressof(u.err));
        } else if (m_has == Has::moved) {
            // do nothing
        } else {
            yy_unreachable();
        }
    }

    NODISCARD constexpr auto is_ok() const -> bool { return m_has == Has::ok; }
    NODISCARD constexpr auto is_err() const -> bool { return m_has == Has::err; }
    NODISCARD constexpr auto as_ok() & -> Option<T&> { return is_ok() ? Option<T&>(unwrap()) : None; }
    NODISCARD constexpr auto as_ok() const& -> Option<T const&> { return is_ok() ? Option<T const&>(unwrap()) : None; }
    NODISCARD constexpr auto as_err() & -> Option<E&> { return is_err() ? Option<E&>(unwrap_err()) : None; }
    NODISCARD constexpr auto as_err() const& -> Option<E const&> { return is_err() ? Option<E const&>(unwrap_err()) : None; }
    // no nodiscard for Void
    constexpr auto unwrap() const& -> T const& requires std::same_as<T, Void> { yy_assert(is_ok()); return u.ok; }
    NODISCARD constexpr auto unwrap() const& -> T const& requires (!std::same_as<T, Void>) { yy_assert(is_ok()); return u.ok; }
    NODISCARD constexpr auto unwrap() & -> T& requires (!std::same_as<T, Void>) { yy_assert(is_ok()); return u.ok; }
    NODISCARD constexpr auto unwrap() && -> T
        requires (!std::same_as<T, Void>)
    {
        yy_assert(is_ok());
        auto result = std::move(*this).u.ok;
        u.ok.~T();
        m_has = Has::moved;
        return result;
    }
    NODISCARD constexpr auto extract() & -> T
        requires (!std::same_as<T, Void>)
    {
        yy_assert(is_ok());
        auto result = std::move(*this).u.ok;
        u.ok.~T();
        m_has = Has::moved;
        return result;
    }
    NODISCARD constexpr auto unwrap_err() const& -> E const& { yy_assert(is_err()); return u.err; }
    NODISCARD constexpr auto unwrap_err() & -> E& { yy_assert(is_err()); return u.err; }
    NODISCARD constexpr auto unwrap_err() && -> E {
        yy_assert(is_err());
        auto result = std::move(*this).u.err;
        u.err.~E();
        m_has = Has::moved;
        return result;
    }
};

}
