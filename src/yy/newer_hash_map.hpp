#pragma once

// TODO: There's a problem with generic PseudoKey and strings: If I make a NewerHashMap<Sv, int>, where the pseudo key
// is a `char const*`, the hash function does a strlen(), every probe where the 32-bit hash matches does a strlen() when
// calling eql, add_insertion does another strlen(). Maybe PseudoKey should be part of Context? If I look at my usage of
// NewerHashMap, it would be fine if that were the case.
// Comparing Key = Sv with PseudoKey = Sv vs char const*:
//  - converting to Sv before calling get_or_put_entry: O(n) -> O(1) (no convertion done here)
//  - hash function: one call: O(n) -> O(2 * n)
//  - eql: likely one call: if length mismatch, O(1) -> O(n), length matches O(worse(n)) -> O(always(n) + worse(n)).
//  - add_insertion: O(1) -> O(n)
// If we make a custom `operator==(Sv, char const*)` that doesn't call strlen, in practice we'll only call a single
// extra strlen() when add_insertion is called. When an insertion is not added, we will not call any extra strlen() when
// compared to converting to Sv before calling get_or_put_entry.
// Another thing we could do: the default context for Sv will have a custom hash(char const*). I don't like that.

#include "yy/allocator.hpp"
#include "yy/basic.hpp"
#include "yy/hash.hpp"
#include "yy/misc.hpp"
#include "yy/vec.hpp"

#include <compare>
#include <concepts>
#include <memory>
#include <new>


namespace yy {

enum class NewerHashMapProbe {
    /// linear probing: better cache efficiency, more clustering
    linear,
    /// quadratic probing: medium cache efficiency, less clustering.
    quadratic,
    /// the first 10 probes are quadratic, all the rest are linear
    quadratic10_linear,
    /// double hash, then linear
    hash_linear,
    /// double hash, then quadratic
    hash_quadratic,
};

template <typename Context, typename Key, typename PseudoKey = Key>
concept NewerHashMapContext = requires(Context const& ctx, Key const& key, PseudoKey const& pseudo_key) {
    { ctx.hash(pseudo_key) } -> std::same_as<u32>;
    { ctx.eql(pseudo_key, key, usize{0}) } -> std::same_as<bool>;
};

template <typename Key>
struct NewerHashMapDefaultContext;

template <typename T>
concept is_trivially_yy_hashable_from_transmute_bytes =
    std::is_standard_layout_v<T> && std::has_unique_object_representations_v<T> &&
    yy::trivial_raii<std::remove_const_t<T>> && std::equality_comparable_with<T, T> &&
    !requires (T x) {
        { Span(x) };
    } && !std::is_pointer_v<T> && !std::is_reference_v<T> && !std::is_empty_v<T> &&
    std::same_as<T, std::remove_extent_t<T>>;

template <typename Key>
    requires is_trivially_yy_hashable_from_transmute_bytes<Key>
struct NewerHashMapDefaultContext<Key> {
    NODISCARD constexpr auto hash(Key const& key) const -> u32 {
        return yy_weak_hash_of_bv(bv_transmute_ptr_one(&key));
    };

    template <typename Q>
    NODISCARD constexpr auto eql(Q const& query_key, Key const& stored_key, usize stored_idx) const -> bool {
        (void)stored_idx;
        return query_key == stored_key;
    }
};

template <typename T>
    requires is_trivially_yy_hashable_from_transmute_bytes<T>
struct NewerHashMapDefaultContext<Span<T>> {
    NODISCARD constexpr auto hash(Span<T> const key) const -> u32 {
        (void)this;
        return yy_weak_hash_of_bv(key.as_bytes());
    };

    template <typename Q>
    NODISCARD constexpr auto eql(Q const& query_key, Span<T> stored_key, usize stored_idx) const -> bool {
        (void)this;
        (void)stored_idx;
        return query_key == stored_key;
    }
};

template <
    typename Key,
    typename Value,
    typename Context = NewerHashMapDefaultContext<Key>,
    NewerHashMapProbe probe = NewerHashMapProbe::linear>
class NewerHashMap {
public:
    using Index = u32;
    using Hash = u32;
    using Size = Index;

    static constexpr float max_load_factor = 0.7f;
    static constexpr float target_load_factor = 0.51f;

    struct Lut;
    struct Bucket {
        Index m_not_insertion_idx{0};
        Hash m_hash{0};
        static constexpr Index sentinel_vacant = 0;
        static constexpr Index sentinel_removed = 1;

        static constexpr struct {
        } ctor_tag{};
        constexpr Bucket(decltype(ctor_tag), Index insertion_idx, Hash hash)
            : m_not_insertion_idx(~insertion_idx), m_hash(hash) {}
        static constexpr auto from_insertion_idx_and_hash(Index insertion_idx, Hash hash) {
            return Bucket(ctor_tag, insertion_idx, hash);
        }
        constexpr Bucket() = default;
        constexpr ~Bucket() = default;
        constexpr Bucket(const Bucket&) = default;
        constexpr Bucket(Bucket&&) = default;
        constexpr Bucket& operator=(const Bucket&) = delete;
        constexpr Bucket& operator=(Bucket&&) = delete;
        NODISCARD constexpr auto insertion_idx() const -> Index { return ~m_not_insertion_idx; }
        NODISCARD constexpr auto hash() const -> Hash { return m_hash; }
        NODISCARD constexpr auto has_key() const -> bool { return m_not_insertion_idx >= 2; }
        NODISCARD constexpr auto is_vacant() const -> bool { return m_not_insertion_idx == sentinel_vacant; }
        NODISCARD constexpr auto is_removed() const -> bool { return m_not_insertion_idx == sentinel_removed; }

    private:
        friend NewerHashMap;
        constexpr auto remove(Lut& lut) -> void {
            yy_assert(has_key());
            lut.removed += 1;
            m_hash = Hash{0};
            m_not_insertion_idx = sentinel_removed;
        }
        constexpr auto replace(Lut& lut, Bucket new_bucket) -> void {
            yy_assert(new_bucket.has_key());
            if (is_removed()) {
                lut.removed -= 1;
            } else if (is_vacant()) {
                lut.occupied += 1;
            }
            m_not_insertion_idx = new_bucket.m_not_insertion_idx;
            m_hash = new_bucket.m_hash;
        }
        constexpr auto reset_insertion_idx(Index new_insertion_idx) -> void {
            m_not_insertion_idx = ~new_insertion_idx;
        }
    };

    struct Lut {
        Size entries_len{0};
        Size occupied{0};
        Size removed{0};
        VecUnmanaged<Bucket> entries{};

        ~Lut() = default;
        Lut() = default;
        Lut(const Lut&) = delete;
        Lut(Lut&& other) {
            using std::swap;
            swap(entries_len, other.entries_len);
            swap(occupied, other.occupied);
            swap(removed, other.removed);
            swap(entries, other.entries);
        }

        Lut& operator=(const Lut&) = delete;
        Lut& operator=(Lut&&) = delete;

        NODISCARD explicit constexpr Lut(Allocator heap, usize new_entries_len = 0) {
            if (new_entries_len == 0)
                return;
            yy_assert(yy_is_power_of_two(new_entries_len));
            entries_len = index_cast(new_entries_len);
            entries.ensure_total_capacity(heap, new_entries_len);
            entries.append_n_default(heap, new_entries_len);
        }

        NODISCARD constexpr auto get_max_tries() const -> Index {
            switch (probe) {
                using enum NewerHashMapProbe;
                case linear:
                case quadratic:
                    return entries_len;
                case quadratic10_linear:
                    return entries_len + 10;
                case hash_linear:
                case hash_quadratic:
                    return entries_len + 1;
            }
        }
        NODISCARD constexpr auto next_probe(Index lut_idx, Index tries, Hash hash) const -> Index {
            yy_assert(entries_len != 0);
            yy_assert(tries > 0);
            Index const mask = static_cast<Index>(entries_len - 1);
            switch (probe) {
                using enum NewerHashMapProbe;
                case linear:
                    return (lut_idx + 1) & mask;
                case quadratic:
                    return (lut_idx + tries) & mask;
                case quadratic10_linear:
                    return (lut_idx + (tries < 10 ? tries : 1)) & mask;
                case hash_linear:
                    return (lut_idx + 1 + (tries == 1 ? (hash >> 16) : 0)) & mask;
                case hash_quadratic:
                    return (lut_idx + (tries == 1 ? (hash >> 16) : (tries - 1))) & mask;
            }
        }

        NODISCARD constexpr auto hash_to_index(Hash hash) const -> Index { return hash & (entries_len - 1); }

    private:
        friend NewerHashMap;

        constexpr auto clear() -> void {
            occupied = {0};
            removed = {0};
            static_assert(std::is_trivially_destructible_v<Bucket>);
            std::uninitialized_default_construct_n(entries.begin(), entries_len);
        }
        constexpr auto destroy(Allocator heap) -> void {
            entries.destroy_free(heap);
            entries_len = {0};
            occupied = {0};
            removed = {0};
        }
        // Resets the other one.
        constexpr auto assign(Allocator heap, Lut&& other) -> void {
            destroy(heap);
            new (this) Lut(std::move(other));
        }
    };

    struct Insertions {
        ~Insertions() = default;
        Insertions() = default;
        // TODO: Implement these.
        Insertions(const Insertions&) = delete;
        Insertions& operator=(const Insertions&) = delete;
        Insertions(Insertions&&) = default;
        Insertions& operator=(Insertions&&) = default;

        // TODO: If we don't need to remove items, we don't need to store the hash. Context should have a
        // `NewerHashMapConfig` struct, which contains `bool store_hash;`, which defaults to true.
        VecUnmanaged<Hash> m_hashes{};
        VecUnmanaged<Key> m_keys{};
        VecUnmanaged<Value> m_values{};

        NODISCARD constexpr auto size() const -> usize {
            check_sizes();
            return m_hashes.size();
        }
        constexpr auto check_sizes() const -> void {
            auto s = m_hashes.size();
            yy_assert(s == m_keys.size() && s == m_values.size());
        }

        constexpr auto ensure_unused_capacity(Allocator heap, usize unused_cap) -> void {
            m_hashes.ensure_unused_capacity(heap, unused_cap);
            m_keys.ensure_unused_capacity(heap, unused_cap);
            m_values.ensure_unused_capacity(heap, unused_cap);
        }
        constexpr auto ensure_unused_capacity_exact(Allocator heap, usize unused_cap) -> void {
            m_hashes.ensure_unused_capacity_exact(heap, unused_cap);
            m_keys.ensure_unused_capacity_exact(heap, unused_cap);
            m_values.ensure_unused_capacity_exact(heap, unused_cap);
        }

        // TODO: `RefMut` and `RefConst` should lock the map pointers.
        struct RefMut {
            usize insertion_idx;
            Hash& hash;
            Key& key;
            Value& value;
        };
        NODISCARD constexpr auto get(usize insertion_idx) -> RefMut {
            yy_assert(insertion_idx < size());
            return {
                .insertion_idx = insertion_idx,
                .hash = m_hashes[insertion_idx],
                .key = m_keys[insertion_idx],
                .value = m_values[insertion_idx],
            };
        }

        // TODO: `RefMut` and `RefConst` should lock the map pointers.
        struct RefConst {
            usize insertion_idx;
            Hash const& hash;
            Key const& key;
            Value const& value;
        };
        NODISCARD constexpr auto get(usize insertion_idx) const -> RefConst {
            yy_assert(insertion_idx < size());
            return {
                .insertion_idx = insertion_idx,
                .hash = m_hashes[insertion_idx],
                .key = m_keys[insertion_idx],
                .value = m_values[insertion_idx],
            };
        }

    private:
        friend NewerHashMap;
        // This function assumes the items were already removed from the Lut.
        constexpr auto pop_back() -> void {
            yy_assert(size() > 0);
            truncate(size() - 1);
        }
        // This function assumes the items were already removed from the Lut.
        constexpr auto truncate(usize const new_size) -> void {
            yy_assert(new_size <= size());
            m_hashes.truncate(new_size);
            m_keys.truncate(new_size);
            m_values.truncate(new_size);
            check_sizes();
        }

        constexpr auto destroy(Allocator heap) -> void {
            m_hashes.destroy_free(heap);
            m_keys.destroy_free(heap);
            m_values.destroy_free(heap);
        }
    };

private:
    Allocator m_heap;
    Lut m_lut;
    Insertions m_insertions{};

    NODISCARD static constexpr auto get_load_factor(usize occupied, usize entries_len) -> float {
        if (entries_len == 0)
            return 0.0f;
        return static_cast<float>(static_cast<double>(occupied) / static_cast<double>(entries_len));
    }
    NODISCARD static constexpr auto index_cast(usize i) -> Index {
        return yy::checked_int_cast<Index>(i).unwrap();
    }
    static constexpr usize MINIMUM_ENTRIES_LEN = 16;
    template <typename K, typename... V>
        requires std::constructible_from<Value, V&&...>
    constexpr auto add_insertion(Hash hash, K&& key, V&&... value) -> Insertions::RefMut {
        usize result = m_insertions.size();
        m_insertions.m_hashes.push_back(m_heap, std::move(hash));
        m_insertions.m_keys.push_back(m_heap, std::forward<K>(key));
        m_insertions.m_values.emplace_back(m_heap, std::forward<V>(value)...);
        m_insertions.check_sizes();
        return m_insertions.get(index_cast(result));
    }
    NODISCARD static constexpr auto get_initial_entries_len_for_expected_count(usize expected_count) -> usize {
        return expected_count == 0
                   ? 0
                   : yy_next_power_of_two((usize)((double)yy_max(MINIMUM_ENTRIES_LEN, expected_count) /
                                                       (double)max_load_factor));
    };

    NODISCARD constexpr auto lut_idx_from_insertion(Index const insertion_idx) const -> Index {
        auto const hash = m_insertions.m_hashes[insertion_idx];
        auto const start_lut_idx = m_lut.hash_to_index(hash);
        auto const max_tries = m_lut.get_max_tries();
        for (Index lut_idx = start_lut_idx, tries = 0; tries < max_tries;
             tries++, lut_idx = m_lut.next_probe(lut_idx, tries, hash)) {
            yy_assert(!m_lut.entries[lut_idx].is_vacant());
            if (m_lut.entries[lut_idx].insertion_idx() != insertion_idx)
                continue;
            return lut_idx;
        }
        yy_unreachable();
    }

public:
    ~NewerHashMap() {
        m_lut.destroy(m_heap);
        m_insertions.destroy(m_heap);
    }
    // TODO: Copy constructor
    NewerHashMap(const NewerHashMap& other) = delete;
    NewerHashMap& operator=(const NewerHashMap& other) = delete;
    NewerHashMap(NewerHashMap&& other)
        : m_heap(other.m_heap), m_lut(std::move(other.m_lut)), m_insertions(std::move(other.m_insertions)) {}
    NewerHashMap& operator=(NewerHashMap&& other) {
        if (this != &other) {
            this->~NewerHashMap();
            new (this) NewerHashMap(std::move(other));
        }
        return *this;
    }
    explicit constexpr NewerHashMap(Allocator heap, usize expected_count = 0)
        : m_heap(heap), m_lut{heap, get_initial_entries_len_for_expected_count(expected_count)}, m_insertions{} {
        m_insertions.ensure_unused_capacity_exact(m_heap, expected_count);
    }

    NODISCARD constexpr auto load_factor() -> float { return get_load_factor(m_lut.occupied, m_lut.entries_len); }

    constexpr auto perform_refill(usize const new_entries_len) -> void {
        yy_assert(new_entries_len > 0 && yy_is_power_of_two(new_entries_len));
        yy_assert(new_entries_len >= m_lut.entries_len);
        auto new_lut = Lut(m_heap, new_entries_len);
        for (auto const& old_bucket : m_lut.entries) {
            if (!old_bucket.has_key())
                continue;
            auto const max_tries = new_lut.get_max_tries();
            Index tries = 0;
            for (Index new_lut_idx = new_lut.hash_to_index(old_bucket.m_hash); tries < max_tries;
                 tries++, new_lut_idx = new_lut.next_probe(new_lut_idx, tries, old_bucket.m_hash)) {
                auto& new_bucket = new_lut.entries[new_lut_idx];
                if (!new_bucket.has_key()) {
                    new_bucket.replace(new_lut, old_bucket);
                    break;
                }
            }
            yy_assert(tries < max_tries);
        }
        m_lut.assign(m_heap, std::move(new_lut));
    }

    // Return whether it did refill.
    NODISCARD constexpr auto maybe_refill(usize const add_n) -> bool {
        if (m_lut.entries_len == 0) {
            yy_assert(m_insertions.size() == 0);
            m_lut.assign(m_heap, Lut(m_heap, get_initial_entries_len_for_expected_count(add_n)));
            return add_n > 0;
        }

        auto constexpr threshold_for_first_grow = MINIMUM_ENTRIES_LEN;
        if (m_lut.occupied + add_n <= threshold_for_first_grow)
            return false;

        auto next_load_factor = get_load_factor(m_lut.occupied + add_n, m_lut.entries_len);
        // Check if we can add n without refilling.
        if (next_load_factor < max_load_factor) {
            return false;
        }

        usize new_entries_len =
            yy_max<usize>(m_lut.entries_len, yy_next_power_of_two(m_lut.occupied - m_lut.removed + add_n));
        while (get_load_factor(m_lut.occupied - m_lut.removed + add_n, new_entries_len) > target_load_factor) {
            new_entries_len = yy::checked_mul(new_entries_len, usize{2}).unwrap();
        }
        perform_refill(new_entries_len);
        return true;
    }
    constexpr auto ensure_unused_capacity(usize const unused_cap) -> void {
        (void)maybe_refill(unused_cap);
        m_insertions.ensure_unused_capacity(m_heap, unused_cap);
    }
    constexpr auto ensure_total_capacity(usize requested_total_capacity) -> void {
        if (requested_total_capacity <= capacity())
            return;
        return ensure_unused_capacity(requested_total_capacity - capacity());
    }
    /// For compatibility with c++ containers. Prefer using `ensure_{total,unused}_capacity[_exact]`.
    constexpr auto reserve(usize requested_total_capacity) -> void { return ensure_total_capacity(requested_total_capacity); }

    NODISCARD constexpr auto get_by_index(usize insertion_idx) -> typename Insertions::RefMut { return m_insertions.get(insertion_idx); }
    NODISCARD constexpr auto get_by_index(usize insertion_idx) const -> typename Insertions::RefConst { return m_insertions.get(insertion_idx); }

    /// TODO: More APIs.
    template <typename Q>
        requires NewerHashMapContext<Context, Key, Q>
    NODISCARD constexpr auto get(Context const& ctx, Q&& key) -> Option<typename Insertions::RefMut> {
        Hash const hash = ctx.hash(key);
        if (m_lut.entries_len == 0)
            return None;
        usize const max_tries = m_lut.get_max_tries();
        for (Index tries = 0, lut_idx = m_lut.hash_to_index(hash); tries < max_tries;
             tries++, lut_idx = m_lut.next_probe(lut_idx, tries, hash)) {
            auto& bucket = m_lut.entries[lut_idx];
            if (bucket.m_hash == hash && bucket.has_key() &&
                ctx.eql(key, m_insertions.get(bucket.insertion_idx()).key, bucket.insertion_idx())) {
                return m_insertions.get(bucket.insertion_idx());
            }
            if (bucket.is_vacant()) {
                break;
            }
        }
        return None;
    }
    template <typename Q>
        requires NewerHashMapContext<Context, Key, Q>
    NODISCARD constexpr auto get(Context const& ctx, Q&& key) const -> Option<typename Insertions::RefConst> {
        Hash const hash = ctx.hash(key);
        if (m_lut.entries_len == 0)
            return None;
        usize const max_tries = m_lut.get_max_tries();
        for (Index tries = 0, lut_idx = m_lut.hash_to_index(hash); tries < max_tries;
             tries++, lut_idx = m_lut.next_probe(lut_idx, tries, hash)) {
            auto& bucket = m_lut.entries[lut_idx];
            if (bucket.m_hash == hash && bucket.has_key() &&
                ctx.eql(key, m_insertions.get(bucket.insertion_idx()).key, bucket.insertion_idx())) {
                return m_insertions.get(bucket.insertion_idx());
            }
            if (bucket.is_vacant()) {
                break;
            }
        }
        return None;
    }



    enum class GetOrPutEntryTag { get, put };
    // friend GetOrPutEntry;
    template <typename PseudoKey>
    class GetOrPutEntry : yy::NoCopyNoMove {
    public:
        friend NewerHashMap;
        class Put : yy::NoCopyNoMove {
        public:
            friend NewerHashMap;
            friend GetOrPutEntry;
            Put(NewerHashMap* map,
                const Context& ctx,
                PseudoKey pseudo_key_ref,
                usize new_insertion_idx,
                usize new_lut_idx,
                Hash hash)
                : map(map),
                  ctx(ctx),
                  pseudo_key_ref(std::forward<PseudoKey>(pseudo_key_ref)),
                  new_insertion_idx(new_insertion_idx),
                  new_lut_idx(new_lut_idx),
                  hash(hash) {}

            NewerHashMap* map;
            Context const& ctx;
            PseudoKey pseudo_key_ref;
            usize new_insertion_idx;
            usize new_lut_idx;
            Hash hash;
        private:
            mutable bool m_did_put{false};

        public:
            template <typename... V>
                requires std::convertible_to<PseudoKey, Key> && (std::constructible_from<Value, V...>)
            auto put_value(V&&... value) const -> Insertions::RefMut {
                return put_key_value(std::forward<PseudoKey>(pseudo_key_ref), std::forward<V>(value)...);
            }

            template <typename K, typename... V>
                requires std::convertible_to<K, Key> && (std::constructible_from<Value, V...>)
            auto put_key_value(K&& key, V&&... value) const -> Insertions::RefMut {
                yy_assert(!m_did_put);
                m_did_put = true;
                auto result = map->add_insertion(hash, std::forward<K>(key), std::forward<V>(value)...);
                yy_assert(result.insertion_idx == new_insertion_idx);
                auto& bucket = map->m_lut.entries[new_lut_idx];
                bucket.replace(map->m_lut, Bucket::from_insertion_idx_and_hash(index_cast(new_insertion_idx), hash));
                return result;
            }
        };

    private:
        GetOrPutEntryTag m_tag;
        union Union {
            struct{}_{};
            Insertions::RefMut get;
            Put put;
            static_assert(std::is_trivially_destructible_v<typename Insertions::RefMut>);
            static_assert(std::is_trivially_destructible_v<Put>);
        } m_as{};
        GetOrPutEntry(Insertions::RefMut ref) : m_tag(GetOrPutEntryTag::get) {
            new (&m_as.get) Insertions::RefMut(std::move(ref));
        }
        GetOrPutEntry(
            NewerHashMap* map,
            Context const& ctx,
            PseudoKey key,
            usize new_insertion_idx,
            usize new_lut_idx,
            Hash hash
        ) : m_tag(GetOrPutEntryTag::put) {
            new (&m_as.put) Put(map, ctx, std::forward<PseudoKey>(key), new_insertion_idx, new_lut_idx, hash);
        }

    public:
        NODISCARD auto tag() const -> GetOrPutEntryTag { return m_tag; }
        NODISCARD auto exists() const -> bool { return m_tag == GetOrPutEntryTag::get; }
        NODISCARD auto unwrap_get() -> Insertions::RefMut {
            yy_assert(m_tag == GetOrPutEntryTag::get);
            return std::move(*this).m_as.get;
        }
        NODISCARD auto unwrap_put() && -> Put const&& {
            yy_assert(m_tag == GetOrPutEntryTag::put);
            return std::move(*this).m_as.put;
        }

        /// When the entry is  `put`, put `Key(pseudo_key)` and `new_value`. They're either a type convertible to Key / Value or
        /// a function that returns them and takes arguments `(Context const&, PseudoKey const&, usize
        /// new_insertion_idx)`
        /// Use this function when the pseudo key is convertible to the stored key.
        template <class NewValue>
            requires std::convertible_to<PseudoKey, Key> &&
                         (std::convertible_to<NewValue, Value> ||
                          std::convertible_to<
                              std::invoke_result_t<NewValue, Context const&, PseudoKey const&, usize>,
                              Value>)
        auto map_put_value(NewValue&& new_value) -> Insertions::RefMut {
            if (m_tag == GetOrPutEntryTag::get)
                return unwrap_get();
            auto&& as_put = std::move(*this).unwrap_put();
            // Don't forward pseudo_key for invoke_or_convert.
            auto&& v = invoke_or_convert<Value>(
                std::forward<NewValue>(new_value), as_put.ctx, as_put.pseudo_key_ref, as_put.new_insertion_idx);
            return as_put.put_key_value(
                std::forward<PseudoKey>(as_put.pseudo_key_ref), std::forward<decltype(v)>(v)
            );
        }
        /// When the entry is  `put`, put `new_key` and `new_value`. They're either a type convertible to Key / Value or
        /// a function that returns them and takes arguments `(Context const&, PseudoKey const&, usize
        /// new_insertion_idx)`
        /// Use this function when the pseudo key is not convertible to the stored key.
        template <class NewKey, class NewValue>
            requires (std::convertible_to<NewValue, Value> ||
                      std::convertible_to<
                        std::invoke_result_t<NewValue, Context const&, PseudoKey const&, usize>, Value>) &&
                     (std::convertible_to<NewKey, Key> ||
                      std::convertible_to<
                        std::invoke_result_t<NewKey, Context const&, PseudoKey const&, usize>, Key>)
        auto map_put_key_value(NewKey&& new_key, NewValue&& new_value) -> Insertions::RefMut {
            if (m_tag == GetOrPutEntryTag::get)
                return unwrap_get();
            auto&& as_put = std::move(*this).unwrap_put();
            // Don't forward pseudo_key for invoke_or_convert.
            auto&& k = invoke_or_convert<Key>(
                std::forward<NewKey>(new_key), as_put.ctx, as_put.pseudo_key_ref, as_put.new_insertion_idx);
            auto&& v = invoke_or_convert<Value>(
                std::forward<NewValue>(new_value), as_put.ctx, as_put.pseudo_key_ref, as_put.new_insertion_idx);
            return as_put.put_key_value(std::forward<decltype(k)>(k), std::forward<decltype(v)>(v));
        }
    };
    /// Return an entry
    template <typename PseudoKey>
        requires NewerHashMapContext<Context, Key, PseudoKey>
    NODISCARD constexpr auto get_or_put_entry(Context const& ctx, PseudoKey&& pseudo_key)
        -> GetOrPutEntry<decltype(pseudo_key)> {
        using Entry = GetOrPutEntry<decltype(pseudo_key)>;
        Hash const hash = ctx.hash(pseudo_key);
        ensure_unused_capacity(1);
        usize const max_tries = m_lut.get_max_tries();
        for (Index tries = 0, lut_idx = m_lut.hash_to_index(hash); tries < max_tries;
                tries++, lut_idx = m_lut.next_probe(lut_idx, tries, hash)) {
            auto& bucket = m_lut.entries[lut_idx];
            if (bucket.m_hash == hash && bucket.has_key() &&
                ctx.eql(pseudo_key, m_insertions.get(bucket.insertion_idx()).key, bucket.insertion_idx())) {
                return Entry(m_insertions.get(bucket.insertion_idx()));
            }
            if (!bucket.has_key()) {
                auto const new_insertion_idx = index_cast(m_insertions.size());
                return Entry(this, ctx, std::forward<PseudoKey>(pseudo_key), new_insertion_idx, lut_idx, hash);
            }
        }
        yy_unreachable();
    }

    template <typename PseudoKey, typename... ValueArgs>
        requires NewerHashMapContext<Context, Key, PseudoKey>
    constexpr auto put_no_clobber_emplace(Context const& ctx, PseudoKey&& key, ValueArgs&&... value_args) -> Insertions::RefMut {
        auto result = get_or_put_entry(ctx, std::forward<PseudoKey>(key))
            .unwrap_put()
            .put_value(std::forward<ValueArgs>(value_args)...);
        return result;
    }

    // NOTE: This code wasn't tested.
    constexpr auto swap_remove_index(usize const insertion_idx) -> void {
        yy_assert(insertion_idx < m_insertions.size());
        auto const removed_idx = index_cast(insertion_idx);
        auto const removed_lut_idx = lut_idx_from_insertion(removed_idx);
        m_lut.entries[removed_lut_idx].remove(m_lut);
        auto const last_idx = index_cast(m_insertions.size() - 1);
        if (last_idx != removed_idx) {
            auto const last_lut_idx = lut_idx_from_insertion(last_idx);
            m_lut.entries[last_lut_idx].reset_insertion_idx(removed_idx);
            auto const last_ins = m_insertions.get(last_idx);
            auto const removed_ins = m_insertions.get(removed_idx);
            removed_ins.hash = std::move(last_ins.hash);
            removed_ins.key = std::move(last_ins.key);
            removed_ins.value = std::move(last_ins.value);
        }
        m_insertions.truncate(last_idx);
    }
    template <typename PseudoKey, typename... ValueArgs>
        requires NewerHashMapContext<Context, Key, PseudoKey>
    constexpr auto swap_remove_key(Context const& ctx, PseudoKey&& key) -> void {
        swap_remove_index(get(ctx, std::forward<PseudoKey>(key)).unwrap().insertion_idx);
    }
    auto truncate(usize const new_len) -> void {
        // TODO: When should we shrink the allocated lut table?
        if (new_len == 0)
            return clear();
        auto const old_len = m_insertions.size();
        yy_assert(new_len <= old_len);
        for (Index removed_insertion_idx = index_cast(old_len); removed_insertion_idx > new_len;) {
            removed_insertion_idx--;
            auto const removed_lut_idx = lut_idx_from_insertion(removed_insertion_idx);
            m_lut.entries[removed_lut_idx].remove(m_lut);
        }
        m_insertions.truncate(new_len);
    }
    auto clear() -> void {
        m_lut.clear();
        m_insertions.truncate(0);
    }

private:
    template <typename MapPointerType>
        requires std::is_same_v<NewerHashMap, std::remove_const_t<std::remove_pointer_t<MapPointerType>>>
    struct iterator_template {
        MapPointerType m_map;
        Index m_insertion_idx{0};
        struct End {};
        constexpr ~iterator_template() = default;
        constexpr iterator_template(const iterator_template&) = default;
        constexpr iterator_template(iterator_template&&) = default;
        constexpr iterator_template& operator=(const iterator_template&) = default;
        constexpr iterator_template& operator=(iterator_template&&) = default;
        constexpr iterator_template(MapPointerType map, Index idx = 0) : m_map(map), m_insertion_idx(idx) {}
        using size_type = usize;
        using difference_type = isize;
        using value_type = Insertions::RefMut;
        using sentinel = iterator_template::End;
        NODISCARD auto operator<=>(iterator_template const& other) const -> std::strong_ordering {
            yy_assert(m_map == other.m_map);
            return m_insertion_idx <=> other.m_insertion_idx;
        }
        NODISCARD auto operator==(iterator_template const& other) const -> bool {
            yy_assert(m_map == other.m_map);
            return m_insertion_idx == other.m_insertion_idx;
        }
        NODISCARD auto operator==(End) const -> bool { return m_insertion_idx == m_map->size(); }
        NODISCARD auto operator-(iterator_template const& other) -> difference_type {
            yy_assert(m_map == other.m_map);
            return static_cast<isize>(m_insertion_idx) - static_cast<isize>(other.m_insertion_idx);
        }
        NODISCARD auto operator++() -> iterator_template& {
            m_insertion_idx += 1;
            return *this;
        }
        constexpr auto operator++(int) -> iterator_template {
            auto result = *this;
            ++(*this);
            return result;
        }
        NODISCARD auto operator*() const -> decltype(auto) { return m_map->m_insertions.get(m_insertion_idx); }
    };
public:

    using iterator = iterator_template<NewerHashMap*>;
    using const_iterator = iterator_template<NewerHashMap const*>;

    NODISCARD constexpr auto size() const -> usize { return m_insertions.size(); }
    NODISCARD constexpr auto capacity() const -> usize { return m_lut.entries_len; }
    // For compatibility with c++ containers.
    NODISCARD constexpr auto max_size() const -> usize {
        constexpr usize key_size = std::is_empty_v<Key> ? 0 : sizeof(Key);
        constexpr usize value_size = std::is_empty_v<Value> ? 0 : sizeof(Value);
        return static_cast<usize>(std::numeric_limits<isize>::max()) / (sizeof(Bucket) + sizeof(Hash) + key_size + value_size);
    }
    NODISCARD constexpr auto empty() const -> bool { return size() == 0; }

    NODISCARD constexpr auto begin() const -> const_iterator { return const_iterator{this}; }
    NODISCARD constexpr auto end() const -> const_iterator::End { return {}; }
    NODISCARD constexpr auto cbegin() const -> const_iterator { return const_iterator{this}; }
    NODISCARD constexpr auto cend() const -> const_iterator::End { return {}; }
    NODISCARD constexpr auto begin() -> iterator { return iterator{this}; }
    NODISCARD constexpr auto end() -> iterator::End { return {}; }
};

static_assert(std::ranges::input_range<NewerHashMap<int, int>>);


}  // namespace yy

using yy::NewerHashMap;
using yy::NewerHashMapDefaultContext;
