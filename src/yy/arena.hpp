#ifndef ___INCLUDE_GUARD__YcSHAyUG9Mkxzsh5uVWfWgxs2allDBQyWIca1CCe
#define ___INCLUDE_GUARD__YcSHAyUG9Mkxzsh5uVWfWgxs2allDBQyWIca1CCe

#include "yy/basic.hpp"
#include "yy/allocator.hpp"

// NOTE: New allocations always go at the end. I don't want this to be a general purpose heap.


// TODO: Make this arena reliable "Any order optimized for LIFO" allocator ("scratch" / "stack")
// so that this can be used as a scratch allocator without `Yy_ArenaSave`. After this is implemented, remove the TODO comments here and document that this is an "Any Order optimized for LIFO" allocator.


// TODO: Remove all usage of yy_arena_{dup,alloc,free,realloc,resize} and replace it with
// the allocator interface.


// TODO: Implement any order for
// TODO: (all of this is imagination, I shouldn't use this Arena for this stuff) Is there a way to
// make this Arena, by using the regular allocator interface without any modifications, able to
// free all allocations regardless of alignment if they are allocated in (In = alloc, Out = free):
//  - Last In First Out order (LIFO)?
//      (state x; alloc a, b, c, d; free d, c, b, a; return to the original state x without any actual leaks)
//      This can be done to _some_ extent currently (2021-11-09), if there's no alignment padding between
//      allocations and no new blocks are created.
//  - First In First Out order (FIFO)?
//      (state x; alloc a, b, c, d; free a, b, c, d; return to the original state x without any actual leaks)
//      Not supported currently (2021-11-09). I might want to implement this.
//  - Any order (General Purpose / gpa)?
//      (state x; alloc a, b, c; free b; alloc d; free a d c; return to the original state x without any actual leaks)
//      Not supported currently (2021-11-09).
//      This might be outside of the scope of this allocator.
//
//  The motivation for supporting "Any order" is that I'd be able to use `Yy_Arena` as
//  scratch allocator _without_ using `Yy_ArenaSave`.


// - TODO: Maybe do this for reliable LIFO order:
//  Every allocation would have a header that says how big of a padding there was between this
//  allocation and the previous one. When you free the last allocation, it would also free the
//  header and `header->padding`, which would get rid of the alignment padding when freeing problem.
//  What to do with a block if it gets released. Do I actually free the block or do I keep
//  it around?
//    One thing I can do is to keep _one_ block around. At the start there is no
//    `arena.kept_block`. When I free something and it goes back to a previous block, I move
//    the block that got released into `arena.kept_block`.
//      When a block must be released:
//        If there is a `kept_block`:
//          Free the released block with the backing allocator.
//          `assert(kept_block->size >= released_block->size);`
//        If there isn't a `kept_block`:
//          Set `kept_block` to the released block.
//      When a new block must be created:
//        If there is a `kept_block`:
//          If `arena->kept_block` can hold the desired size, I use that one instead of allocating
//          a new block. If it can't, I free the kept block with the backing allocator and create
//          a new block.
//          TODO: What about resizing the block in place?
//            If the in place resized block's size is smaller than `kept_block.size` or if there
//            is no `kept_block`, then there is no question and I can try to resize it. But what
//            if the resized block's size is greater than `kept_block.size`?
//              Option A:
//                If resize succeeds, I free the `kept_block` with the backign allocator.
//              Option B:
//                I don't try to resize the block in place. Chances are it will fail anyway,
//                since it probably already tried to resize it (though it might have tried to
//                resize it with a much bigger size).
//          Also set `kept_block` to `NULL`.
//        If there isn't a `kept_block`:
//          Create a new block.
//    I can also use this same logic for `Yy_ArenaSave` (TODO). Currently, saves don't have
//    a `kept_block`, and having a `kept_block` is more efficient than what I'm doing right now.

// - TODO: Maybe do this for reliable Any order:
//  Just like with the LIFO order, there's a header for every allocation.
//  When freeing something that is not at the end of the arena, I get the header after the
//  allocation I want to free and increment its padding to "free" this allocation and the header
//  previous to this allocation.
//  How do I get the header after the allocation I want to free? One way to do it would be to
//  search from the end of the arena until I find it.
//  NOTE: If there is out of order freeing, space might be freed and I might be able to grow
//  in place allocations in the middle of the arena. I _might_ implement that but _only_ if getting
//  the header _after_ the allocation can be done in O(1). New allocations will still only go
//  to the end of the arena.

/// The memory owned by an arena block starts at the `Yy_ArenaBlock` struct.
struct Yy_ArenaBlock {
    /// The size of this block in bytes, including `struct Yy_ArenaBlock`.
    size_t size;
    /// How much memory this block has already used.
    size_t idx;
    /// The previous block. Can be NULL.
    Yy_ArenaBlock *prev;
};

#ifdef NDEBUG
# define YY_ARENA_SAFETY_CHECKS 0
#else
# define YY_ARENA_SAFETY_CHECKS 1
#endif

struct Yy_Arena;
struct Yy_ArenaCpp;
struct Yy_ArenaSave;
struct Yy_ArenaSaveScoped;

NODISCARD Yy_Allocator yy_arena_allocator(Yy_Arena *arena);

/// A linear allocator.
struct Yy_Arena {
    /// The first block of the arena.
    Yy_ArenaBlock *blocks;
    Yy_Allocator backing_allocator;
#if YY_ARENA_SAFETY_CHECKS
    /// Each time `yy_arena_save_begin` is called this is incremented. Each
    /// time `yy_arena_save_restore` or `yy_arena_save_forget` gets
    /// called it checks if the value is right and decrements it. This catches
    /// restoring two saves in the incorrect order. It also verifies on
    /// `yy_arena_deinit` that the count is equal to zero.
    size_t saved_count;
    /// `saved_block_start` and `saved_block_idx` are used to catch a reallocation with memory that was created before the current save.
    /// When `saved_count > 0`, this is the start of the block (the block ptr) when `yy_arena_save_begin` was called.
    uintptr_t saved_block_start;
    /// When `saved_count > 0`, this is the idx of the block when the save happened.
    size_t saved_block_idx;
#endif

    NODISCARD auto allocator() -> Yy_Allocator { return yy_arena_allocator(this); }
    NODISCARD auto scoped_save() -> Yy_ArenaSaveScoped;
};

/// A saved state of a `Yy_Arena`.
struct Yy_ArenaSave {
    Yy_ArenaBlock *block;
#if YY_ARENA_SAFETY_CHECKS
    size_t prev_saved_count;
    uintptr_t prev_saved_block_start;
    size_t prev_saved_block_idx;
#endif
    size_t idx;
};

struct Yy_ArenaSaveScoped {
    Option<Yy_Arena*> arena;
    Yy_ArenaSave save;
    static_assert(std::is_trivial_v<Yy_ArenaSave>);

    Yy_ArenaSaveScoped(const Yy_ArenaSaveScoped&) = delete;
    Yy_ArenaSaveScoped& operator=(const Yy_ArenaSaveScoped&) = delete;
    Yy_ArenaSaveScoped(Yy_ArenaSaveScoped&& other);
    Yy_ArenaSaveScoped& operator=(Yy_ArenaSaveScoped&& other);
    Yy_ArenaSaveScoped(Yy_Arena* arena);
    ~Yy_ArenaSaveScoped();
    auto forget() -> void;
};


/// Initialize an arena by allocating memory for it on the backing allocator. Deinitialize with `yy_arena_deinit`.
NODISCARD Yy_Arena *yy_arena_init_bootstrap(Yy_Allocator backing_allocator, usize initial_size);

/// Deinitialize with `yy_arena_deinit`.
void yy_arena_init(Yy_Arena *arena, Yy_Allocator backing_allocator, size_t initial_size);
void yy_arena_deinit(Yy_Arena *arena);

void yy_arena_save_begin(Yy_Arena *arena, Yy_ArenaSave *ret_saved);
void yy_arena_save_forget(Yy_Arena *arena, Yy_ArenaSave *saved);
void yy_arena_save_restore(Yy_Arena *arena, Yy_ArenaSave *saved_);


struct Yy_ArenaCpp {
    Yy_Arena a;
    static_assert(std::is_trivial_v<Yy_Arena>);
    Yy_ArenaCpp(const Yy_ArenaCpp&) = delete;
    Yy_ArenaCpp& operator=(const Yy_ArenaCpp&) = delete;
    Yy_ArenaCpp(Yy_ArenaCpp&& other);
    Yy_ArenaCpp& operator=(Yy_ArenaCpp&& other);
    ~Yy_ArenaCpp() { yy_arena_deinit(&a); }
    Yy_ArenaCpp(Yy_Allocator heap, usize initial_size = 0) { yy_arena_init(&a, heap, initial_size); }

    NODISCARD auto allocator() -> Yy_Allocator { return yy_arena_allocator(&a); }
    NODISCARD auto scoped_save() -> Yy_ArenaSaveScoped { return a.scoped_save(); }
};

#endif//___INCLUDE_GUARD__YcSHAyUG9Mkxzsh5uVWfWgxs2allDBQyWIca1CCe
