#pragma once

#include <algorithm>
#include <memory>
#include <new>
#include <ranges>
#include <type_traits>
#include <utility>

#include "yy/allocator.hpp"
#include "yy/basic.hpp"


namespace yy {

namespace vec_detail {

struct Config {
    bool leak{false};
};

}

template <typename T>
    requires (!same_as_remove_cvref<T, void> && (("You can use an empty struct.") != nullptr))
class Vec;

template <typename T, vec_detail::Config config_>
    requires (!same_as_remove_cvref<T, void> && (("You can use an empty struct.") != nullptr)) &&
             std::is_move_constructible_v<T> && (!std::is_reference_v<T>)
class VecUnmanagedDetail {
public:
    using Managed = Vec<T>;
    static constexpr auto config = config_;
    friend Managed;

    ~VecUnmanagedDetail()
        requires (!config.leak)
    {
        // Maybe I should just leak it, logging it.
        yy_assert_fmt(
            m_ptr == nullptr,
            "You did not destroy the VecUnmanaged. Either call destroy_free(allocator) or destroy_leak().%s", ""
        );
    }

    ~VecUnmanagedDetail()
        requires (config.leak) && std::is_trivially_destructible_v<T>
    = default;

    ~VecUnmanagedDetail()
        requires (config.leak) && (!std::is_trivially_destructible_v<T>)
    {
        destroy_leak();
    }

    NODISCARD auto span() -> Span<T> { return *this; }
    NODISCARD auto span() const -> Span<T const> { return *this; }
    NODISCARD auto span_const() const -> Span<T const> { return *this; }
    NODISCARD auto leak_to_span() -> Span<T>
        requires std::is_trivially_destructible_v<T>
    {
        auto result = span();
        reset_members();
        return result;
    }
    auto destroy_leak() -> void {
        clear();
        reset_members();
    }
    auto destroy_free(Allocator allocator) -> void
        requires std::is_empty_v<T>
    {
        (void)allocator;
        clear();
        reset_members();
    }
    auto destroy_free(Allocator allocator) -> void
        requires(!std::is_empty_v<T>)
    {
        clear();
        auto const capacity_in_bytes = m_capacity * elem_sizeof();
        allocator_free(allocator, m_ptr, capacity_in_bytes);
        reset_members();
    }
    VecUnmanagedDetail() = default;
    VecUnmanagedDetail(VecUnmanagedDetail const&) = delete;
    VecUnmanagedDetail& operator=(VecUnmanagedDetail const&) = delete;
    VecUnmanagedDetail(VecUnmanagedDetail&& other) requires (!((config.leak) && std::is_trivially_destructible_v<T>))
        : m_ptr(std::move(other.m_ptr)), m_len(std::move(other.m_len)), m_capacity(std::move(other.m_capacity)) {
        other.reset_members();
    }
    VecUnmanagedDetail(VecUnmanagedDetail&& other) requires (config.leak) && std::is_trivially_destructible_v<T>
        = default;
    VecUnmanagedDetail& operator=(VecUnmanagedDetail&& other)
        requires (!((config.leak) && std::is_trivially_destructible_v<T>))
    {
        if (this != &other) {
            this->~VecUnmanagedDetail();
            new (this) VecUnmanagedDetail(std::move(other));
        }
        return *this;
    }
    VecUnmanagedDetail& operator=(VecUnmanagedDetail&& other)
        requires ((config.leak) && std::is_trivially_destructible_v<T>)
    = default;
    static auto take_ownership(T* ptr, usize len, usize capacity) -> VecUnmanagedDetail {
        return VecUnmanagedDetail(m_, ptr, len, capacity);
    }
    INLINE NODISCARD auto to_owned(Allocator allocator) -> Managed {
        auto result = Managed::take_ownership(allocator, m_ptr, m_len, m_capacity);
        reset_members();
        return result;
    }

    static auto init_capacity_exact(Allocator allocator, usize new_capacity) -> VecUnmanagedDetail {
        VecUnmanagedDetail result{};
        result.ensure_unused_capacity_exact(allocator, new_capacity);
        return result;
    }
    static auto init_capacity(Allocator allocator, usize new_capacity) -> VecUnmanagedDetail {
        VecUnmanagedDetail result{};
        result.ensure_unused_capacity(allocator, new_capacity);
        return result;
    }


    NODISCARD auto try_get(usize i) -> Option<T&> {
        return i < m_len ? Option<T&>(m_ptr[i]) : None;
    }
    NODISCARD auto try_get(usize i) const -> Option<T const&> {
        return i < m_len ? Option<T const&>(m_ptr[i]) : None;
    }
    NODISCARD auto operator[](usize i) -> T& {
        yy_assert(i < m_len);
        return m_ptr[i];
    }
    NODISCARD auto operator[](usize i) const -> T const& {
        yy_assert(i < m_len);
        return m_ptr[i];
    }

    INLINE NODISCARD auto len() const -> usize { return m_len; }
    INLINE NODISCARD auto capacity() const -> usize { return m_capacity; }
    INLINE NODISCARD auto unused_capacity() const -> usize { return m_capacity - m_len; }
    // For compatibility with c++ containers.
    INLINE NODISCARD constexpr auto max_size() const -> usize {
        return static_cast<usize>(std::numeric_limits<isize>::max()) / sizeof(T);
    }

    auto clear() -> void {
        std::destroy(begin(), end());
        m_len = 0;
    }

    auto ensure_total_capacity_exact(Allocator allocator, usize requested_total_capacity) -> void {
        auto const old_capacity = m_capacity;
        if (old_capacity < requested_total_capacity) {
            grow_exact(allocator, requested_total_capacity);
        }
    }
    auto ensure_unused_capacity_exact(Allocator allocator, usize requested_unused_capacity) -> void {
        ensure_total_capacity_exact(allocator, len() + requested_unused_capacity);
    }
    auto ensure_total_capacity(Allocator allocator, usize requested_total_capacity) -> void {
        auto const old_capacity = m_capacity;
        if (old_capacity < requested_total_capacity) {
            auto new_cap = old_capacity;
            while (new_cap < requested_total_capacity) {
                new_cap += new_cap / 2 + 8;
            }
            grow_exact(allocator, new_cap);
        }
    }
    auto ensure_unused_capacity(Allocator allocator, usize requested_unused_capacity) -> void {
        ensure_total_capacity(allocator, len() + requested_unused_capacity);
    }

    NODISCARD auto clone(Allocator new_allocator, auto&&... inner_clone_args) const -> VecUnmanagedDetail {
        auto result = VecUnmanagedDetail::init_capacity_exact(new_allocator, len());
        for (auto const& e : *this) {
            // Don't forward rvalue references for inner_clone_args.
            new (&result.m_ptr[result.m_len++]) T(yy::clone<T>(e, inner_clone_args...));
        }
        yy_assert(result.size() == this->size());
        return result;
    }
    NODISCARD auto clone_into_managed(Allocator new_allocator, auto&&... inner_clone_args) const -> Managed {
        auto result = Managed::init_capacity_exact(new_allocator, len());
        for (auto const& e : *this) {
            // Don't forward rvalue references for inner_clone_args.
            new (&result.un.m_ptr[result.un.m_len++]) T(yy::clone<T>(e, inner_clone_args...));
        }
        yy_assert(result.size() == this->size());
        return result;
    }

    /// Delete the array range because it conflicts with the null terminator constructor.
    template <usize N>
        requires yy::char_type<T>
    auto append_range(Allocator allocator, T const (&)[N]) = delete;
    template <usize N>
        requires yy::char_type<T>
    auto append_range(Allocator allocator, T (&)[N]) = delete;

    // Append string literal.
    auto append_range(Allocator allocator, T const* null_terminated_string)
        requires yy::char_type<T>
    {
        auto sv = Span(null_terminated_string);
        append_range(allocator, sv);
    }

    auto append_range(Allocator allocator, std::ranges::input_range auto&& range) -> void {
        if constexpr (requires { std::ranges::size(range); }) {
            ensure_unused_capacity(allocator, std::ranges::size(range));
            for (auto&& i : range) {
                push_back_assume_capacity(std::forward<decltype(i)>(i));
            }
        } else {
            for (auto&& i : range) {
                push_back(allocator, std::forward<decltype(i)>(i));
            }
        }
    }
    auto append_range(Allocator allocator, std::initializer_list<T> range) -> void {
        ensure_unused_capacity(allocator, std::ranges::size(range));
        for (auto&& i : range) {
            push_back_assume_capacity(std::forward<decltype(i)>(i));
        }
    }
    auto append_n_default(Allocator allocator, usize n) -> void {
        ensure_unused_capacity(allocator, n);
        for (usize i = 0; i < n; i++) {
            new (&m_ptr[m_len++]) T();
        }
    }
    /// Resize the Vec so that `size()` is equal to `new_size`. If `new_size` is greater than `size()`, fill the new
    /// values with `new_value`.
    auto resize(Allocator allocator, usize new_size, T const& new_value) -> void {
        if (new_size <= size())
            truncate(new_size);
        ensure_total_capacity(allocator, new_size);
        for (usize i = size(); i < new_size; i++) {
            std::construct_at(m_ptr + i, new_value);
        }
        m_len = new_size;
    }
    /// Set `m_len` to `new_m_len`. It assumes the elements are already initialized.
    auto unsafe_set_m_len(usize new_m_len) -> void
        requires (yy::trivial_raii_maybe_moveonly<T>)
    {
        yy_assert(new_m_len <= capacity());
        m_len = new_m_len;
    }
    auto push_back(Allocator allocator, T&& new_element) -> void {
        ensure_unused_capacity(allocator, 1);
        new (&m_ptr[m_len++]) T(std::move(new_element));
    }
    auto push_back(Allocator allocator, T const& new_element) -> void {
        ensure_unused_capacity(allocator, 1);
        new (&m_ptr[m_len++]) T(new_element);
    }
    auto push_back_assume_capacity(T&& new_element) -> void {
        yy_assert(unused_capacity() >= 1);
        new (&m_ptr[m_len++]) T(std::move(new_element));
    }
    auto push_back_assume_capacity(T const& new_element) -> void {
        yy_assert(unused_capacity() >= 1);
        new (&m_ptr[m_len++]) T(new_element);
    }
    template <typename... Args>
        requires std::constructible_from<T, Args...>
    auto emplace_back(Allocator allocator, Args&&... args) -> void {
        ensure_unused_capacity(allocator, 1);
        new (&m_ptr[m_len++]) T(std::forward<Args>(args)...);
    }
    template <typename... Args>
        requires std::constructible_from<T, Args...>
    auto emplace_back_assume_capacity(Args&&... args) -> void {
        yy_assert(unused_capacity() >= 1);
        new (&m_ptr[m_len++]) T(std::forward<Args>(args)...);
    }

    auto truncate(usize new_len) -> void {
        yy_assert(new_len <= m_len);
        std::destroy(&m_ptr[new_len], &m_ptr[m_len]);
        m_len = new_len;
    }

    NODISCARD auto pop_back() -> T {
        yy_assert(m_len > 0);
        auto ptr = &m_ptr[--m_len];
        auto result = std::move(*ptr);
        ptr->~T();
        return result;
    }

    /// Ensures the Vec's memory is terminated by a '\0' and return the data pointer.
    /// The result pointer may be invalidated if the original Vec calls mutable methods.
    /// The pointer obtained from `c_str()` may only be treated as a pointer to a null-terminated character string if
    /// the string object does not contain other null characters.
    NODISCARD constexpr auto c_str(Allocator allocator) -> T const*
        requires yy::char_type<T>
    {
        ensure_unused_capacity(allocator, 1);
        static_assert(std::is_trivial_v<T>);
        m_ptr[m_len] = 0;
        return data();
    }
    /// Ensures the Vec's memory is terminated by a '\0' and return the data pointer.
    /// The result pointer may be invalidated if the original Vec calls mutable methods.
    /// The pointer obtained from `c_str()` may only be treated as a pointer to a null-terminated character string if
    /// the string object does not contain other null characters.
    NODISCARD constexpr auto c_str_assume_capacity() -> T const*
        requires yy::char_type<T>
    {
        yy_assert(unused_capacity() >= 1);
        static_assert(std::is_trivial_v<T>);
        m_ptr[m_len] = 0;
        return data();
    }

    using iterator = T*;
    using const_iterator = T const*;
    using value_type = T;
    using difference_type = ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;
    INLINE auto erase(iterator pos) -> iterator { return erase(pos, pos + 1); }
    auto erase(iterator first, iterator last) -> iterator {
        yy_assert(first <= last && first >= begin() && last <= end());
        auto const remove_count = static_cast<usize>(last - first);
        yy_assert(remove_count <= m_len);
        auto const old_end = end();
        auto const new_end = old_end - remove_count;
        // Move from [last, old_end) to [first, new_end)
        {
            auto old_pos = last;
            auto new_pos = first;
            for (; old_pos != old_end; ++new_pos, ++old_pos) {
                *new_pos = std::move(*old_pos);
            }
            yy_assert(old_pos == old_end);
            yy_assert(new_pos == new_end);
        }
        // Destroy the now unused elements.
        std::destroy(new_end, old_end);
        m_len -= remove_count;
        return first;
    }
    INLINE NODISCARD auto size() const -> usize { return m_len; }
    INLINE NODISCARD auto begin() -> iterator { return m_ptr; }
    INLINE NODISCARD auto end() -> iterator { return m_ptr + m_len; }
    INLINE NODISCARD auto begin() const -> const_iterator { return m_ptr; }
    INLINE NODISCARD auto end() const -> const_iterator { return m_ptr + m_len; }
    INLINE NODISCARD auto cbegin() const -> const_iterator { return m_ptr; }
    INLINE NODISCARD auto cend() const -> const_iterator { return m_ptr + m_len; }
    INLINE NODISCARD auto data() -> iterator { return begin(); }
    INLINE NODISCARD auto data() const -> const_iterator { return begin(); }
    INLINE NODISCARD auto cdata() const -> const_iterator { return begin(); }
    INLINE NODISCARD auto empty() const -> bool { return m_len == 0; }
    INLINE NODISCARD constexpr auto front() const -> T const& { return (*this)[0]; }
    INLINE NODISCARD constexpr auto back() const -> T const& { yy_assert(size() > 0); return (*this)[size() - 1]; }
    INLINE NODISCARD constexpr auto front() -> T& { return (*this)[0]; }
    INLINE NODISCARD constexpr auto back() -> T& { yy_assert(size() > 0); return (*this)[size() - 1]; }

private:
    T* m_ptr{nullptr};
    usize m_len{0};
    usize m_capacity{0};

    static constexpr struct {
    } m_{};
    VecUnmanagedDetail(decltype(m_), T* ptr, usize len, usize capacity) : m_ptr(ptr), m_len(len), m_capacity(capacity) {}

    static consteval auto elem_sizeof() -> usize
        requires(!std::is_empty_v<T>)
    {
        return sizeof(T);
    }
    static constexpr auto elem_alignof = alignof(T);

    auto reset_members() -> void {
        m_ptr = nullptr;
        m_len = 0;
        m_capacity = 0;
    }

    auto grow_exact(Allocator allocator, usize new_capacity) -> void
        requires std::is_empty_v<T>
    {
        (void)allocator;
        auto const old_capacity = m_capacity;
        yy_assert(m_len <= old_capacity);
        yy_assert(old_capacity <= new_capacity);
        yy_assert(new_capacity <= max_size());
        m_ptr = reinterpret_cast<T*>(elem_alignof);
        m_capacity = new_capacity;
    }

    auto grow_exact(Allocator allocator, usize new_capacity) -> void
        requires(!std::is_empty_v<T>)
    {
        auto const old_capacity = m_capacity;
        auto const old_capacity_in_bytes = old_capacity * elem_sizeof();
        auto const old_len = m_len;
        auto const old_ptr = m_ptr;

        yy_assert(m_len <= old_capacity);
        yy_assert(old_capacity <= new_capacity);
        yy_assert(new_capacity <= max_size());

        // Align forward the capacity so that the allocator doesn't waste space (if supported).
        // TODO: Make a function for this.
        auto new_capacity_in_bytes = allocator_align_size_forward(allocator, new_capacity * elem_sizeof());
        new_capacity = new_capacity_in_bytes / elem_sizeof();
        yy_assert(new_capacity <= max_size());
        new_capacity_in_bytes = new_capacity * elem_sizeof();

        // Try to resize in place
        if (yy_is_err(allocator_proc_resize_in_place(
                allocator, old_ptr, old_capacity_in_bytes, elem_alignof, new_capacity_in_bytes
            ))) {
            // If resize in place doesn't work, allocate new memory, move the current elements, free the old memory.
            auto const new_ptr =
                reinterpret_cast<T*>(allocator_proc_realloc(allocator, nullptr, 0, elem_alignof, new_capacity_in_bytes)
                );
            allocator_oom_check(new_ptr);
            std::uninitialized_move(old_ptr, old_ptr + old_len, new_ptr);
            std::destroy(old_ptr, old_ptr + old_len);
            allocator_proc_free(allocator, old_ptr, old_capacity_in_bytes);
            m_ptr = new_ptr;
        }
        m_capacity = new_capacity;
    }
};

template <typename T>
using VecUnmanagedLeaked = VecUnmanagedDetail<T, vec_detail::Config{.leak = true}>;
template <typename T>
using VecUnmanaged = VecUnmanagedDetail<T, vec_detail::Config{.leak = false}>;
static_assert(std::ranges::contiguous_range<VecUnmanaged<int>>);

template <typename T>
    requires(!same_as_remove_cvref<T, void> && (("You can use an empty struct.") != nullptr))
class Vec {
public:
    using Unmanaged = VecUnmanaged<T>;
    friend Unmanaged;

    ~Vec() { destroy_free(); }

    INLINE NODISCARD auto span() -> Span<T> { return un.span(); }
    INLINE NODISCARD auto span() const -> Span<T const> { return un.span(); }
    INLINE NODISCARD auto span_const() const -> Span<T const> { return un.span_const(); }
    INLINE NODISCARD auto leak_to_span() -> Span<T>
        requires std::is_trivially_destructible_v<T>
    {
        return un.leak_to_span();
    }
    INLINE auto destroy_leak() -> void { return un.destroy_leak(); }
    INLINE auto destroy_free() -> void { return un.destroy_free(m_allocator); }

private:
    Vec() = default;

public:
    explicit Vec(Allocator allocator) : m_allocator(allocator) {}
    Vec(Vec const& other) = delete;
    Vec& operator=(Vec const& other) = delete;
    Vec(Vec&& other) : un(std::move(other.un)), m_allocator(std::move(other.m_allocator)) { other.reset_members(); }
    Vec& operator=(Vec&& other) {
        if (this != &other) {
            this->~Vec();
            new (this) Vec(std::move(other));
        }
        return *this;
    }
    static auto take_ownership(Allocator allocator, T* ptr, usize len, usize capacity) -> Vec {
        return Vec(m_, ptr, len, capacity, allocator);
    }
    INLINE NODISCARD auto to_unmanaged() -> Unmanaged {
        auto result = Unmanaged::take_ownership(un.m_ptr, un.m_len, un.m_capacity);
        reset_members();
        return result;
    }

    static auto init_capacity_exact(Allocator allocator, usize new_capacity) -> Vec {
        Vec result{allocator};
        result.ensure_unused_capacity_exact(new_capacity);
        return result;
    }
    static auto init_capacity(Allocator allocator, usize new_capacity) -> Vec {
        Vec result{allocator};
        result.ensure_unused_capacity(new_capacity);
        return result;
    }


    INLINE NODISCARD auto operator[](usize i) -> T& { return un[i]; }
    INLINE NODISCARD auto operator[](usize i) const -> T const& { return un[i]; }
    INLINE NODISCARD auto try_get(usize i) -> Option<T&> { return un.try_get(i); }
    INLINE NODISCARD auto try_get(usize i) const -> Option<T const&> { return un.try_get(i); }

    INLINE NODISCARD auto len() const -> usize { return un.len(); }
    INLINE NODISCARD auto capacity() const -> usize { return un.capacity(); }
    INLINE NODISCARD auto unused_capacity() const -> usize { return un.unused_capacity(); }
    // For compatibility with c++ containers.
    INLINE NODISCARD constexpr auto max_size() const -> usize {
        return static_cast<usize>(std::numeric_limits<isize>::max()) / sizeof(T);
    }

    INLINE auto clear() -> void { un.clear(); }

    /// For compatibility with c++ containers. Prefer using `ensure_{total,unused}_capacity[_exact]`.
    INLINE auto reserve(usize requested_total_capacity) -> void {
        return un.ensure_total_capacity(m_allocator, requested_total_capacity);
    }
    INLINE auto ensure_total_capacity_exact(usize requested_total_capacity) -> void {
        return un.ensure_total_capacity_exact(m_allocator, requested_total_capacity);
    }
    INLINE auto ensure_unused_capacity_exact(usize requested_unused_capacity) -> void {
        return un.ensure_unused_capacity_exact(m_allocator, requested_unused_capacity);
    }
    INLINE auto ensure_total_capacity(usize requested_total_capacity) -> void {
        return un.ensure_total_capacity(m_allocator, requested_total_capacity);
    }
    INLINE auto ensure_unused_capacity(usize requested_unused_capacity) -> void {
        return un.ensure_unused_capacity(m_allocator, requested_unused_capacity);
    }

    struct other_allocator_tag_t {};
    INLINE NODISCARD auto clone(auto&&... inner_clone_args) const -> Vec
        requires yy::can_clone_with_args<T, decltype(inner_clone_args)...>
    {
        return this->clone(
            other_allocator_tag_t{}, m_allocator, std::forward<decltype(inner_clone_args)>(inner_clone_args)...
        );
    }
    NODISCARD auto clone(other_allocator_tag_t, Allocator new_allocator, auto&&... inner_clone_args) const -> Vec
        requires yy::can_clone_with_args<T, decltype(inner_clone_args)...>
    {
        auto result = Vec::init_capacity_exact(new_allocator, len());
        for (auto const& e : *this) {
            // Don't forward rvalue references for inner_clone_args.
            new (&result.un.m_ptr[result.un.m_len++]) T(yy::clone<T>(e, inner_clone_args...));
        }
        yy_assert(result.size() == this->size());
        return result;
    }
    INLINE NODISCARD auto clone_into_unmanaged(auto&&... inner_clone_args) const -> Unmanaged {
        return this->clone_into_unmanaged(
            other_allocator_tag_t{}, m_allocator, std::forward<decltype(inner_clone_args)>(inner_clone_args)...
        );
    }
    NODISCARD auto clone_into_unmanaged(other_allocator_tag_t, Allocator new_allocator, auto&&... inner_clone_args) const
        -> Unmanaged {
        auto result = Unmanaged::init_capacity_exact(new_allocator, len());
        for (auto const& e : *this) {
            // Don't forward rvalue references for inner_clone_args.
            new (&result.m_ptr[result.m_len++]) T(yy::clone<T>(e, inner_clone_args...));
        }
        yy_assert(result.size() == this->size());
        return result;
    }

    /// Delete the array range because it conflicts with the null terminator constructor.
    template <usize N>
        requires yy::char_type<T>
    auto append_range(T const (&)[N]) = delete;
    template <usize N>
        requires yy::char_type<T>
    auto append_range(T (&)[N]) = delete;

    // Append string literal.
    INLINE auto append_range(T const* null_terminated_string)
        requires yy::char_type<T>
    {
        un.append_range(m_allocator, null_terminated_string);
    }

    INLINE auto append_range(std::ranges::input_range auto&& range) -> void {
        return un.append_range(m_allocator, std::forward<decltype(range)>(range));
    }
    INLINE auto append_range(std::initializer_list<T> range) -> void { return un.append_range(m_allocator, range); }
    INLINE auto append_n_default(usize n) -> void { return un.append_n_default(m_allocator, n); }
    /// Resize the Vec so that `size()` is equal to `new_size`. If `new_size` is greater than `size()`, fill the new
    /// values with `new_value`.
    INLINE auto resize(usize new_size, T const& new_value) -> void {
        return un.resize(m_allocator, new_size, new_value);
    }
    /// Set `m_len` to `new_m_len`. It assumes the elements are already initialized.
    INLINE auto unsafe_set_m_len(usize new_m_len) -> void
        requires (yy::trivial_raii_maybe_moveonly<T>)
    {
        return un.unsafe_set_m_len(new_m_len);
    }
    INLINE auto push_back(T&& new_element) -> void { return un.push_back(m_allocator, std::move(new_element)); }
    INLINE auto push_back(T const& new_element) -> void { return un.push_back(m_allocator, new_element); }
    INLINE auto push_back_assume_capacity(T&& new_element) -> void {
        return un.push_back_assume_capacity(std::move(new_element));
    }
    INLINE auto push_back_assume_capacity(T const& new_element) -> void {
        return un.push_back_assume_capacity(new_element);
    }
    template <typename... Args>
    INLINE auto emplace_back(Args&&... args) -> void {
        return un.emplace_back(m_allocator, std::forward<Args>(args)...);
    }
    template <typename... Args>
    INLINE auto emplace_back_assume_capacity(Args&&... args) -> void {
        return un.emplace_back_assume_capacity(std::forward<Args>(args)...);
    }
    INLINE auto truncate(usize new_len) -> void { return un.truncate(new_len); }
    INLINE NODISCARD auto pop_back() -> T { return un.pop_back(); }

    /// Ensures the Vec's memory is terminated by a '\0' and return the data pointer.
    /// The result pointer may be invalidated if the original Vec calls mutable methods.
    /// The pointer obtained from `c_str()` may only be treated as a pointer to a null-terminated character string if
    /// the string object does not contain other null characters.
    INLINE NODISCARD constexpr auto c_str() -> T const*
        requires yy::char_type<T>
    {
        return un.c_str(m_allocator);
    }
    /// Ensures the Vec's memory is terminated by a '\0' and return the data pointer.
    /// The result pointer may be invalidated if the original Vec calls mutable methods.
    /// The pointer obtained from `c_str()` may only be treated as a pointer to a null-terminated character string if
    /// the string object does not contain other null characters.
    INLINE NODISCARD constexpr auto c_str_assume_capacity() -> T const*
        requires yy::char_type<T>
    {
        return un.c_str_assume_capacity();
    }

    using iterator = T*;
    using const_iterator = T const*;
    using value_type = T;
    using difference_type = ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;
    INLINE auto erase(iterator pos) -> iterator { return erase(pos, pos + 1); }
    INLINE auto erase(iterator first, iterator last) -> iterator { return un.erase(first, last); }
    INLINE NODISCARD auto size() const -> usize { return un.size(); }
    INLINE NODISCARD auto begin() -> iterator { return un.begin(); }
    INLINE NODISCARD auto end() -> iterator { return un.end(); }
    INLINE NODISCARD auto begin() const -> const_iterator { return un.begin(); }
    INLINE NODISCARD auto end() const -> const_iterator { return un.end(); }
    INLINE NODISCARD auto cbegin() const -> const_iterator { return un.cbegin(); }
    INLINE NODISCARD auto cend() const -> const_iterator { return un.cend(); }
    INLINE NODISCARD auto data() -> iterator { return begin(); }
    INLINE NODISCARD auto data() const -> const_iterator { return begin(); }
    INLINE NODISCARD auto cdata() const -> const_iterator { return begin(); }
    INLINE NODISCARD auto empty() const -> bool { return un.m_len == 0; }
    INLINE NODISCARD constexpr auto front() const -> T const& { return un.front(); }
    INLINE NODISCARD constexpr auto back() const -> T const& { return un.back(); }
    INLINE NODISCARD constexpr auto front() -> T& { return un.front(); }
    INLINE NODISCARD constexpr auto back() -> T& { return un.back(); }

    NODISCARD constexpr auto get_allocator() -> Allocator { return m_allocator; }

private:
    Unmanaged un{};
    Allocator m_allocator{};

    static constexpr struct {
    } m_{};
    Vec(decltype(m_), T* ptr, usize len, usize capacity, Allocator allocator)
        : un(Unmanaged::m_, ptr, len, capacity), m_allocator(allocator) {}

    static consteval auto elem_sizeof() -> usize
        requires(!std::is_empty_v<T>)
    {
        return Unmanaged::elem_sizeof();
    }
    static constexpr auto elem_alignof = Unmanaged::elem_alignof;

    INLINE auto reset_members() -> void {
        un.reset_members();
        m_allocator = {};
    }

    INLINE auto grow_exact(usize new_capacity) -> void { return un.grow_exact(m_allocator, new_capacity); }
};
static_assert(std::ranges::contiguous_range<Vec<int>>);
static_assert(std::ranges::output_range<Vec<int>, int>);


namespace vec_private {


template <template <class> typename Container, typename... Args>
concept ContainerAllows = requires { typename Container<Args...>; };

struct Empty {};

static_assert(ContainerAllows<Vec, int>);
static_assert(ContainerAllows<Vec, Empty>);
static_assert(!ContainerAllows<Vec, void>);
static_assert(!ContainerAllows<Vec, void const>);
static_assert(ContainerAllows<VecUnmanaged, int>);
static_assert(ContainerAllows<VecUnmanaged, Empty>);
static_assert(!ContainerAllows<VecUnmanaged, void>);
static_assert(!ContainerAllows<VecUnmanaged, void const>);

static_assert(std::is_move_assignable_v<VecUnmanaged<Vec<int>>>);
static_assert(std::is_move_constructible_v<VecUnmanaged<Vec<int>>>);
static_assert(std::is_move_assignable_v<VecUnmanagedLeaked<Vec<int>>>);
static_assert(std::is_move_constructible_v<VecUnmanagedLeaked<Vec<int>>>);

}  // namespace vec_private

}  // namespace yy

using yy::Vec;
using yy::VecUnmanaged;
using yy::VecUnmanagedLeaked;
using yy::VecUnmanagedDetail;
