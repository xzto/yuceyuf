#ifndef ___INCLUDE_GUARD__wKNY1TBQNbrO99ISrWBZY6gwo9rrg1CBDQrHqRPu
#define ___INCLUDE_GUARD__wKNY1TBQNbrO99ISrWBZY6gwo9rrg1CBDQrHqRPu

#include "yy/basic.hpp"
#include "yy/sv.hpp"
#include "yy/misc.hpp"


// TODO:
// TODO: Allow resize_in_place to fail when shrinking.
// TODO: Realloc/resize/free: Remove parameter `new_align`, add `old_mem_align`.

//  - Permanent (most of the time the variable is named "arena" - not to be confused with the
//   specific implementation of `Yy_Arena`, which can do more than that if needed)
//    - It never frees any memory.
//  - Last In First Out order (LIFO)
//      (state x; alloc a, b, c, d; free d, c, b, a; return to the original state `x` without any
//       actual leaks)
//    - It can only free in LIFO order. Doing otherwise results in a leak.
//  - First In First Out order (FIFO)
//      (state x; alloc a, b, c, d; free a, b, c, d; return to the original state `x` without any
//       actual leaks)
//    - It can only free in FIFO order. Doing otherwise results in a leak.
//  - Any order
//      (e.g., state x; alloc a, b, c; free b; alloc d; free a d c (any particular order);
//       return to the original state `x` without any actual leaks)
//    - It can allocate and free in any order, though it might be optimized for specific
//     circumstances.
//    - Can be optimized for LIFO (most of the time the variable is named "scratch" or "stack")
//    - Can be optimized for FIFO
//    - No particular order (most of the time the variable is named "heap", or "gpa")

// When there's a function that allocates something and the caller is responsible to manage it,
// the allocator is probably just called "allocator" since there's no need to make it specific.

typedef void *Yy_Allocator_Result;

/// Representation of a failed realloc.
/// TODO: Don't use `-1`. Use nullptr instead. This is problematic due to possible type alignment requirements after reinterpret_casts.
#define Yy_Allocator_Result__failure \
    ((Yy_Allocator_Result)((void*)(intptr_t)(-1)))
static inline bool Yy_Allocator_Result__is_failure(Yy_Allocator_Result x)
{
    return x == Yy_Allocator_Result__failure;
}


//! NOTE: `old_mem_len == 0` means that `old_mem_ptr` is garbage. Allocations of size zero are always
//! a special case handled in the wrapper procs `allocator_proc_*`. Realloc and resize_in_place will
//! free the memory when new size == 0. The allocator implementations can assert that
//! `new_len != 0`.

// TODO: `align_size_forward` should not be its own method; instead, it should be inside
// `realloc` and `resize_in_place`. One example that would take advantage of that is the following:
//  an allocator that allocates on a fixed buffer, but falls back to a different allocator when
//  it runs out of space. If `align_size_forward` was called once before `realloc` was called, it
//  wouldn't be able to take advantage of the backing allocator's possible extra space. `realloc`
//  and `resize_in_place` will have an extra `len_align` argument like zig allocators have.

/// Free memory. It never fails.
/// The implementation can assert that `old_mem_len != 0` because that is handled in the wrapper
/// proc.
typedef void (*Yy_Allocator_Proc_Free)(
    void *opaque_allocator,
    void *old_mem_ptr, usize old_mem_len
);

/// Resize without changing the pointer. When `new_len <= old_len` and the pointer is already
/// aligned to `new_align`, it never fails.
/// The implementation can assert that `new_len != 0` because that is handled in the wrapper proc.
/// TODO: Should I also special case `old_mem_len == 0`?
typedef YyStatus (*Yy_Allocator_Proc_ResizeInPlace)(
    void *opaque_allocator,
    void *old_mem_ptr, usize old_mem_len,
    /// `new_align` can never be zero.
    usize new_align,
    usize new_len
);

/// Reallocate. It may change the pointer even when shrinking. When `new_len <= old_len` and
/// the pointer is already aligned to `new_align`, it never fails.
/// The implementation can assert that `new_len != 0` because that is handled in the wrapper proc.
/// TODO: Should I also special case `old_mem_len == 0` and add an `alloc` method?
typedef Yy_Allocator_Result (*Yy_Allocator_Proc_Realloc)(
    void *opaque_allocator,
    void *old_mem_ptr, usize old_mem_len,
    /// `new_align` can never be zero.
    usize new_align,
    usize new_len
);

/// Align a size forward according to the allocator's "page" size. Uses the argument `new_len`.
typedef usize (*Yy_Allocator_Proc_AlignSizeForward)(
    void *opaque_allocator,
    usize len
);

typedef struct {
    Yy_Allocator_Proc_Free free;
    Yy_Allocator_Proc_ResizeInPlace resize_in_place;
    Yy_Allocator_Proc_Realloc realloc;
    Yy_Allocator_Proc_AlignSizeForward align_size_forward;
    // TODO: Some allocators will want to store a header with debug information about the allocation. safety_check_potential_realloc_or_free will check that the allocator is in a valid state for reallocating or freeing that pointer.
    // typedef void (*Yy_Allocator_Proc_SafetyCheckPotentialReallocOrFree)(
    //     void *old_mem_ptr,
    //     usize old_mem_len
    // );
    // Yy_Allocator_Proc_SafetyCheckPotentialReallocOrFree safety_check_potential_realloc_or_free;
} Yy_Allocator_Vtable;

struct Yy_Allocator {
    /// An opaque handle to the underlying allocator data, if there is any.
    void *opaque_handle;
    /// _Always_ use `allocator_proc_*` instead of these function pointers.
    Yy_Allocator_Vtable const *vtable;
};

/// Wrappers that check for many invalid situations _and_ some special cases for zero sized
/// allocations.
void allocator_proc_free(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len
);
NODISCARD YyStatus allocator_proc_resize_in_place(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
);
NODISCARD Yy_Allocator_Result allocator_proc_realloc(
    Yy_Allocator allocator,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
);
NODISCARD usize allocator_proc_align_size_forward(
    Yy_Allocator allocator,
    usize size
);

static inline void allocator_assert_ok(Yy_Allocator_Result x)
{
    yy_assert(!Yy_Allocator_Result__is_failure(x));
}
static inline void allocator_oom_check(Yy_Allocator_Result x)
{
    if (Yy_Allocator_Result__is_failure(x))
        yy_panic("oom");
}

#define allocator_create(allocator, T) \
    allocator_alloc_array(allocator, T, 1)

#define allocator_destroy(allocator, T, ptr) \
    allocator_free_array(allocator, T, ptr, ((void)yy_static_assert_types_and_zero(T, yy_TypeOf((ptr)[0])), 1))

#define allocator_alloc_array(allocator, T, new_len) \
    allocator_alloc_array_aligned(allocator, T, alignof(T), new_len)
#define allocator_alloc_array_aligned(allocator, T, new_align, new_len) \
    allocator_typed_alloc_array_aligned<T>(allocator, new_align, new_len)



#define allocator_free_array(allocator, T, old_arr_ptr, old_arr_len) \
    allocator_typed_free_array<T>(allocator, old_arr_ptr, old_arr_len)


#define allocator_shrink_array(allocator, T, old_arr_ptr, old_arr_len, new_arr_len) \
    allocator_shrink(allocator, old_arr_ptr, ((void)yy_static_assert_types_and_zero(T, yy_TypeOf((old_arr_ptr)[0])), ((old_arr_len)*sizeof(T))), new_arr_len)


#define allocator_realloc_array(allocator, T, old_arr_ptr, old_arr_len, new_len) \
    (allocator_realloc_array_aligned(allocator, T, old_arr_ptr, old_arr_len, alignof(T), new_len))
#define allocator_realloc_array_aligned(allocator, T, old_arr_ptr, old_arr_len, new_align, new_len) \
    (allocator_typed_realloc_array_aligned<T>(allocator, old_arr_ptr, old_arr_len, new_align, new_len))

#define allocator_resize_in_place_array(allocator, T, old_arr_ptr, old_arr_len, new_len) \
    allocator_resize_in_place_array_aligned(allocator, T, old_arr_ptr, old_arr_len, alignof(T), new_len)
#define allocator_resize_in_place_array_aligned(allocator, T, old_arr_ptr, old_arr_len, new_align, new_len) \
    allocator_typed_resize_in_place_array_aligned<T>(allocator, old_arr_ptr, old_arr_len, new_align, new_len)

NODISCARD usize allocator_align_size_forward(Yy_Allocator allocator, usize size);

NODISCARD Yy_Allocator_Result allocator_alloc_align_size(
    Yy_Allocator allocator,
    usize new_align,
    usize new_len
);
void allocator_free(
    Yy_Allocator allocator,
    void *old_mem_ptr,
    usize old_mem_len
);

// TODO: Add `allocator_shrink_realloc`: never fails, _can_ move the pointer
/// Shrink memory _without_ changing the pointer. Can't fail.
void allocator_shrink_in_place(
    Yy_Allocator allocator,
    void *old_mem_ptr,
    usize old_mem_len,
    usize new_len
);



/// A wrapper on realloc and free. This is incompatible with calling realloc/malloc/free directly. You shouldn't pass memory that came from these functions into this allocator or vice versa.
NODISCARD Yy_Allocator c_allocator(void);

/// A wrapper on mmap and munmap. This is incompatible with calling mmap/munmap/mremap directly. You shouldn't pass memory that came from these functions into this allocator or vice versa.
NODISCARD Yy_Allocator page_allocator(void);

NODISCARD SvMut allocator_dup_sv(Yy_Allocator allocator, Sv sv);


template <typename T>
    requires (!std::is_const_v<T>) && yy::trivial_raii_maybe_moveonly<T>
auto allocator_typed_alloc_array_aligned(Yy_Allocator allocator, usize new_align, usize new_len) -> T* {
    auto result = reinterpret_cast<T*>(allocator_alloc_align_size(allocator, new_align, new_len * sizeof(T)));
    return result;
}
template <typename T>
    requires (!std::is_const_v<T>) && std::is_trivially_destructible_v<T>
auto allocator_typed_free_array(Yy_Allocator allocator, T* old_arr_ptr, usize old_arr_len) -> void {
    allocator_free(allocator, old_arr_ptr, old_arr_len * sizeof(T));
}
template <typename T>
    requires (!std::is_const_v<T>) && yy::trivial_raii_maybe_moveonly<T>
auto allocator_typed_realloc_array_aligned(
    Yy_Allocator allocator,
    T* old_arr_ptr,
    usize old_arr_len,
    usize new_align,
    usize new_len
) -> T* {
    auto result = reinterpret_cast<T*>(
        allocator_proc_realloc(allocator, old_arr_ptr, old_arr_len * sizeof(T), new_align, new_len * sizeof(T))
    );
    return result;
}
template <typename T>
    requires std::is_trivially_destructible_v<T> && std::is_trivially_move_constructible_v<T>
auto allocator_typed_resize_in_place_array_aligned(
    Yy_Allocator allocator,
    T* old_arr_ptr,
    usize old_arr_len,
    usize new_align,
    usize new_len
) -> T* {
    auto result = allocator_proc_resize_in_place(allocator, old_arr_ptr, old_arr_len * sizeof(T), new_align, new_len * sizeof(T));
    return result;
}


// Allocate one T with uninitialized memory.
template <typename T>
    requires (!std::is_array_v<T>)
INLINE auto allocator_new_uninit(Yy_Allocator allocator) -> T* {
    auto result = reinterpret_cast<T*>(allocator_alloc_align_size(allocator, alignof(T), sizeof(T)));
    allocator_oom_check(result);
    return result;
}
// Allocate one T and run the constructor
template <typename T, typename... Args>
    requires (!std::is_array_v<T>) && std::constructible_from<T, Args&&...>
INLINE auto allocator_new(Yy_Allocator allocator, Args&&... args) -> T* {
    auto result = allocator_new_uninit<T>(allocator);
    new (result) T(std::forward<Args>(args)...);
    return result;
}
// Allocate one T and default construct
template <typename T>
    requires (!std::is_array_v<T>) && std::is_default_constructible_v<T>
INLINE auto allocator_new_default_construct(Yy_Allocator allocator) -> T* {
    auto result = allocator_new_uninit<T>(allocator);
    new (result) T;
    return result;
}

// Allocate a span with uninitialized memory.
template <typename T>
    requires (!std::is_array_v<T>)
INLINE auto allocator_new_span_uninit(Yy_Allocator allocator, usize const n) -> Span<T> {
    auto result = reinterpret_cast<T*>(allocator_alloc_align_size(allocator, alignof(T), n*sizeof(T)));
    allocator_oom_check(result);
    return {result, n};
}
// Allocate a span and default construct
template <typename T>
    requires (!std::is_array_v<T>)
INLINE auto allocator_new_span_default_construct(Yy_Allocator allocator, usize const n) -> Span<T> {
    auto result = allocator_new_span_uninit<T>(allocator, n);
    std::uninitialized_default_construct(result.begin(), result.end());
    return result;
}

/// Run destructor and deallocate.
template <typename T>
    requires std::destructible<T>
auto allocator_delete(Yy_Allocator allocator, T* old_ptr) -> void {
    std::destroy_at(old_ptr);
    allocator_free(allocator, old_ptr, sizeof(T));
}

template <typename T>
    requires std::destructible<T>
auto allocator_delete_span(Yy_Allocator allocator, Span<T> old_span) -> void {
    std::destroy(old_span.begin(), old_span.end());
    allocator_free(allocator, old_span.data(), sizeof(T)*old_span.size());
}

namespace yy {
using Allocator = Yy_Allocator;
}

#endif//___INCLUDE_GUARD__wKNY1TBQNbrO99ISrWBZY6gwo9rrg1CBDQrHqRPu
