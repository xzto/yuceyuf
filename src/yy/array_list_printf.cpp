#include "yy/array_list_printf.hpp"
#include "yy/sv.hpp"

#include <cstdarg>
#include <cstdio>


void array_list_printf_inner(ArrayList_Of_u8 *self, char const *fmt, va_list ap)
{
    // TODO: remove size limit
    static constexpr usize max_size = 4096;
    self->ensure_unused_capacity(max_size);

    isize const res = vsnprintf((char*)self->begin() + self->size(), max_size, fmt, ap);
    yy_assert(res >= 0);
    usize const written_size = yy_min((usize)res, max_size - 1);
    yy_assert(written_size < max_size);
    self->unsafe_set_m_len(self->size() + written_size);
}

PRINTF_LIKE(2, 3)
void array_list_printf(ArrayList_Of_u8 *self, char const *fmt, ...)
{
    // ZoneScoped;
    va_list ap;
    va_start(ap, fmt);
    array_list_printf_inner(self, fmt, ap);
    va_end(ap);
}

void array_list_u8_write_ptr_len(ArrayList_Of_u8 *self, u8 const *bytes_ptr, usize bytes_len)
{
    // ZoneScoped;
    self->ensure_unused_capacity(bytes_len);
    self->append_range(Span<u8 const>(bytes_ptr, bytes_len));
}
void array_list_u8_write_sv(ArrayList_Of_u8 *self, Sv buf) {
    auto as_bytes = buf.as_bytes();
    array_list_u8_write_ptr_len(self, as_bytes.ptr, as_bytes.len);
}
void array_list_u8_write_bv(ArrayList_Of_u8 *self, Bv buf) {
    array_list_u8_write_ptr_len(self, buf.ptr, buf.len);
}

char const* array_list_u8_leak_into_cstr(ArrayList_Of_u8 *self)
{
    self->push_back(u8('\0'));
    auto result = (char const*) self->data();
    (void) self->leak_to_span();
    return result;
}

void array_list_u8_write_escaped_fancy_sv(ArrayList_Of_u8 *self, Sv sv, Option<usize> max_elems)
{
    static constexpr auto quote_ch = '"';
    self->push_back(quote_ch);
    auto const max_chars_to_print = std::min(sv.len, max_elems.unwrap_or(sv.len));
    for (usize i = 0; i < max_chars_to_print; i++) {
        u8 const ch = char_to_u8(sv[i]);
        if (ch >= ' ' && ch <= '~' && ch != quote_ch) {
            self->push_back(ch);
        } else {
            u8 const byte_low = ch & 0xf;
            u8 const byte_high = (ch >> 4) & 0xf;
            static constexpr Sv table = "0123456789abcdef";
            auto const bytes = std::array{ '\\', 'x', table[byte_high], table[byte_low] };
            array_list_u8_write_sv(self, bytes);
        }
    }
    if (max_chars_to_print < sv.len) {
        Sv const foo = "(...)"_sv;
        array_list_u8_write_sv(self, foo);
    }
    self->push_back(quote_ch);
}
