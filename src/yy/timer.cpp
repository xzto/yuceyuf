#include "yy/timer.hpp"


#include <time.h>

void timer_init(Timer *timer)
{
    *timer = Timer {
        .x = 0
    };
    struct timespec tp;
    int const rc = clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
    if (rc < 0) yy_panic("clock_gettime() < 0");
    u64 const x = (u64)tp.tv_nsec + (u64)tp.tv_sec * ns_per_seconds;
    timer->x = x;
}

NODISCARD u64 timer_lap(Timer *timer)
{
    struct timespec tp;
    int const rc = clock_gettime(CLOCK_MONOTONIC_RAW, &tp);
    if (rc < 0) yy_panic("clock_gettime() < 0");
    u64 const x = (u64)tp.tv_nsec + (u64)tp.tv_sec * ns_per_seconds;
    u64 const result = x - timer->x;
    timer->x = x;
    return result;
}
