#include "yy/hash.hpp"
#include "yy/misc.hpp"


static u64 const fnv1a64_offset_basis = UINT64_C(14695981039346656037);
static u64 const fnv1a64_prime = UINT64_C(1099511628211);

NODISCARD u32 yy_weak_hash_of_bv(Bv bv)
{
    Yy_Hasher h = yy_hasher_init();
    yy_hasher_append_bv(&h, bv);
    return yy_hasher_final32(&h);
}

Yy_Hasher::Yy_Hasher() : state (fnv1a64_offset_basis) {}
NODISCARD Yy_Hasher yy_hasher_init(void)
{
    return Yy_Hasher{};
}

void yy_hasher_append_u32(Yy_Hasher *const h, u32 const x) { yy_hasher_append_bv(h, bv_transmute_ptr_one(&x)); }
void yy_hasher_append_u64(Yy_Hasher *const h, u64 const x) { yy_hasher_append_bv(h, bv_transmute_ptr_one(&x)); }
void yy_hasher_append_usize(Yy_Hasher *const h, usize const x) { yy_hasher_append_bv(h, bv_transmute_ptr_one(&x)); }

void yy_hasher_append_bv(Yy_Hasher *const h, Bv const bv)
{
    // fnv1a
    u64 hash = h->state;
    for (usize i = 0; i < bv.len; i++) {
        u8 const b = bv.ptr[i];
        hash ^= b;
        hash = yy_wrapping_mul(hash, fnv1a64_prime);
    }
    h->state = hash;
}

NODISCARD u32 yy_hasher_final32(Yy_Hasher const *const h)
{
    static_assert(sizeof(h->state) == 8);
    return static_cast<u32>(h->state) ^ static_cast<u32>(h->state >> 32);
}
