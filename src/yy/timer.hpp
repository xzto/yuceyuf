#pragma once

#include "yy/basic.hpp"


// TODO: Maybe this should be a u96?
UNUSED static u64 const ns_per_seconds = (UINT64_C(1000000000));
UNUSED static u64 const ns_per_ms      = (UINT64_C(1000000));
UNUSED static u64 const ns_per_us      = (UINT64_C(1000));
typedef struct {
    u64 x;
} Timer;

// Use `TimeMs_Fmt` and `TimeMs_Arg` with something that came from `timer_lap`
#define TimeSec_Fmt "%" PRIu64 ".%09" PRIu64 "s"
#define TimeSec_Arg(t) (t)/ns_per_seconds, (t)%ns_per_seconds

#define TimeMs_Fmt "%" PRIu64 ".%06" PRIu64 "ms"
#define TimeMs_Arg(t) (t)/ns_per_ms, (t)%ns_per_ms

/// Initializes a timer.
void timer_init(Timer *timer);

/// Return the time since init or since lap was last called. The result is in nanoseconds.
NODISCARD u64 timer_lap(Timer *timer);
