#include "yy/arena.hpp"
#include <string.h>
#include <stdlib.h>

Yy_ArenaCpp::Yy_ArenaCpp(Yy_ArenaCpp&& other) {
    a = other.a;
    other.a = {};
}
Yy_ArenaCpp& Yy_ArenaCpp::operator=(Yy_ArenaCpp&& other) {
    if (this != &other) {
        this->~Yy_ArenaCpp();
        new(this) Yy_ArenaCpp(std::move(other));
    }
    return *this;
}

NODISCARD auto Yy_Arena::scoped_save() -> Yy_ArenaSaveScoped { return Yy_ArenaSaveScoped(this); }

Yy_ArenaSaveScoped::Yy_ArenaSaveScoped(Yy_Arena* arena) : arena(arena) {
    yy_arena_save_begin(arena, &save);
}
Yy_ArenaSaveScoped::~Yy_ArenaSaveScoped() {
    if (arena.has_value())
        yy_arena_save_restore(arena.unwrap(), &save);
}
auto Yy_ArenaSaveScoped::forget() -> void {
    yy_arena_save_forget(arena.unwrap(), &save);
    arena = None;
}
Yy_ArenaSaveScoped::Yy_ArenaSaveScoped(Yy_ArenaSaveScoped&& other) : arena(other.arena), save(other.save) {
    other.arena = None;
    other.save = {};
}
Yy_ArenaSaveScoped& Yy_ArenaSaveScoped::operator=(Yy_ArenaSaveScoped&& other) {
    if (this != &other) {
        this->~Yy_ArenaSaveScoped();
        new(this) Yy_ArenaSaveScoped(std::move(other));
    }
    return *this;
}


NODISCARD Yy_Arena *yy_arena_init_bootstrap(Yy_Allocator backing_allocator, usize initial_size)
{
    Yy_Arena bootstrap_arena;
    yy_arena_init(&bootstrap_arena, backing_allocator, initial_size);
    Yy_Arena *const result = allocator_create(yy_arena_allocator(&bootstrap_arena), Yy_Arena);
    if (Yy_Allocator_Result__is_failure(result))
        yy_panic("oom");
    *result = bootstrap_arena;
    yy_debug_invalidate_ptr_one(&bootstrap_arena);
    return result;
}

// TODO: Specify an alignment for the block.
NODISCARD static Yy_ArenaBlock *
arena_block_create(Yy_Allocator backing_allocator, size_t initial_size)
{
    usize const default_block_size = 64;
    usize const this_block_size =
        allocator_align_size_forward(backing_allocator, yy_max(initial_size, default_block_size));
    yy_assert(this_block_size > sizeof(Yy_ArenaBlock));
    Yy_ArenaBlock *block = static_cast<Yy_ArenaBlock*>(
        allocator_alloc_align_size(backing_allocator, alignof(Yy_ArenaBlock), this_block_size));
    if (Yy_Allocator_Result__is_failure(block)) {
        yy_panic_fmt("%s: oom", __func__);
    }
    *block = Yy_ArenaBlock {
        .size = this_block_size,
        .idx = sizeof(Yy_ArenaBlock),
        .prev = NULL,
    };
    return block;
}

static void
arena_block_destroy(Yy_Allocator backing_allocator, Yy_ArenaBlock *block)
{
    allocator_free(backing_allocator, block, block->size);
}

NODISCARD static usize add_usize_without_overflow(usize a, usize b)
{
    usize result = a;
    if (yy_overflow_add(b, result, &result))
        yy_panic("overflow");
    return result;
}

/// Calculates a size for a new block that must fit at least `min_buf_needed`.
NODISCARD static usize block_growth_formula(
    /// The size of the previous block.
    usize const prev_block_size,
    /// The minimum amount of memory that this block must have available. This is usually the
    /// size of the allocation and extra for alignment.
    usize const min_buf_needed
)
{
    // Calculate the size for this block. The size will be rounded up to the next page boundary. The block size grows exponentially. It takes 4-6 generations for the block size to be multiplied by 10 (with individual allocations that are small compared to the block size).
    // Example with allocations of size 1 align 1:
    // gen 0: 4.00 KB       gen 1: 8.00 KB      gen 2: 16.00 KB     gen 3: 28.00 KB
    // gen 4: 44.00 KB      gen 5: 68.00 KB     gen 6: 104.00 KB    gen 7: 160.00 KB
    // gen 8: 244.00 KB     gen 9: 368.00 KB    gen 10: 556.00 KB   gen 11: 836.00 KB
    // If you have in the same arena a growing dynamic array and many small allocations, it is better to have a constant block size instead of growing it based on the previous size.

    usize result = 0;
    result = add_usize_without_overflow(result, prev_block_size);
    result = add_usize_without_overflow(result, sizeof(Yy_ArenaBlock));
    result = add_usize_without_overflow(result, min_buf_needed);
    result = add_usize_without_overflow(result, result / 2);
    return result;
}

/// Try to resize the block to fit the requested remaining size. Return true on success.
NODISCARD static bool block_request_remaining_size(
    Yy_Allocator const backing_allocator,
    Yy_ArenaBlock *const block, usize const requested_remaining_size)
{
    usize const remaining_size = block->size - block->idx;
    if (remaining_size >= requested_remaining_size)
        return true;
    // TODO: if (block->is_static) return false;

    usize const new_resized_block_size =
        allocator_align_size_forward(
            backing_allocator,
            add_usize_without_overflow(block->size, block_growth_formula(block->size, requested_remaining_size)));

    YyStatus const result = allocator_proc_resize_in_place(
                                     backing_allocator,
                                     block,
                                     block->size,
                                     alignof(Yy_ArenaBlock),
                                     new_resized_block_size);
    if (yy_is_err(result))
        return false;
    yy_assert(new_resized_block_size >= block->size);
    block->size = new_resized_block_size;
    return true;
}

/// Initialize a `Yy_Arena*`. Crashes if OOM.
void
yy_arena_init(Yy_Arena *arena, Yy_Allocator backing_allocator, size_t initial_size)
{
    *arena = Yy_Arena {
        .blocks = initial_size == 0 ? NULL : arena_block_create(backing_allocator, initial_size),
        .backing_allocator = backing_allocator,
#if YY_ARENA_SAFETY_CHECKS
        .saved_count = 0,
        .saved_block_start = 0,
        .saved_block_idx = 0,
#endif
    };
}

/// Deinitialize a `Yy_Arena`.
void
yy_arena_deinit(Yy_Arena *arena_)
{
    // Unbootstrap if necessary, invalidating memory for `arena_`.
    Yy_Arena arena = *arena_;
    yy_debug_invalidate_ptr_one(arena_);
#if YY_ARENA_SAFETY_CHECKS
    yy_assert(arena.saved_count == 0);
#endif
    Yy_ArenaBlock *block = arena.blocks;
    while (block != nullptr) {
        Yy_ArenaBlock *block_to_free = block;
        block = block_to_free->prev;
        arena_block_destroy(arena.backing_allocator, block_to_free);
    }
}


/// Allocate `size` bytes aligned to `align`. Crashes if OOM.
NODISCARD static void *
yy_arena_alloc_size_align(Yy_Arena *const arena, usize const size, usize const align)
{
    yy_assert(yy_is_power_of_two(align));
    // Don't waste space for zero size allocations.
    if (size == 0)
        return NULL;
    usize const min_buf_needed = size + align - 1;
    usize remaining_size = 0;
    if (arena->blocks != nullptr) {
        Yy_ArenaBlock *const first_block = arena->blocks;
        yy_assert(first_block->idx >= sizeof(Yy_ArenaBlock));
        yy_assert(first_block->size >= first_block->idx);
        remaining_size = first_block->size - first_block->idx;
    }
    // Allocate a new block if necessary.
    // TODO: Try to resize the block in place.
    if (remaining_size < min_buf_needed) {
        if (arena->blocks != NULL && block_request_remaining_size(arena->backing_allocator, arena->blocks, min_buf_needed)) {
            // Do nothing
        } else {
            usize const this_arena_size = block_growth_formula(arena->blocks != nullptr ? arena->blocks->size : 0, min_buf_needed);
            Yy_ArenaBlock *const prev_block = arena->blocks;
            arena->blocks = arena_block_create(arena->backing_allocator, this_arena_size);
            arena->blocks->prev = prev_block;
        }
    }
    yy_assert(arena->blocks != nullptr);
    Yy_ArenaBlock *const block = arena->blocks;
    remaining_size = block->size - block->idx;
    yy_assert(remaining_size >= min_buf_needed);
    uintptr_t const result = yy_align_forward_power_of_two(align, (uintptr_t)block + block->idx);
    block->idx = (result + size - (uintptr_t)block);
    return (uint8_t *)result;
}

/// Try to reallocate memory at `old_ptr` of size `old_size` to `new_size` and `new_align` in place.
NODISCARD static YyStatus
yy_arena_resize_in_place_size_align(Yy_Arena *arena, void *old_ptr, size_t old_size, size_t new_size, size_t new_align)
{
    if (old_ptr != 0) {
        uintptr_t const old_start = (uintptr_t)old_ptr;
        yy_assert(new_align > 0);
        bool const old_start_is_already_aligned = yy_is_aligned_power_of_two(new_align, old_start);

        uintptr_t const old_end = old_start + old_size;
#if YY_ARENA_SAFETY_CHECKS
        // Check that the old memory was not created before the current save state.
        {
            bool old_mem_is_valid = false;
            Yy_ArenaBlock *b = arena->blocks;
            for (; (uintptr_t)b != arena->saved_block_start; b = b->prev) {
                // `b` can't possibly be null because `b->prev` must reach
                // `arena->saved_block_start`. If saved_block_start was null, it stops the loop.
                yy_assert(b != NULL);
                // `b` is a valid block.
                uintptr_t const b_start = (uintptr_t)b;
                uintptr_t const b_idx = b_start + b->idx;
                if (old_start >= b_start && old_end <= b_idx) {
                    // old memory is valid.
                    old_mem_is_valid = true;
                    break;
                }
            }
            if (!old_mem_is_valid && (b != nullptr)) {
                yy_assert(arena->saved_count > 0);
                uintptr_t const b_start = (uintptr_t)b;
                yy_assert(b_start == arena->saved_block_start); // redundant assert
                uintptr_t const b_good_start = b_start + arena->saved_block_idx;
                uintptr_t const b_good_end = b_start + b->idx;
                yy_assert(b->idx >= arena->saved_block_idx);
                if (old_start >= b_good_start && old_end <= b_good_end) {
                    // old memory is valid.
                    old_mem_is_valid = true;
                }
            }
            if (!old_mem_is_valid) {
                if (b != NULL) {
                    yy_assert_fmt(false, "%s", "you reallocated memory that was created before the current save");
                } else yy_assert_fmt(false, "%s", "the old memory was not from this arena");
            }
        }
#endif
        // If the old memory is not aligned to `new_align`, I can't use the same pointer.
        if (!old_start_is_already_aligned)
            return Yy_Bad;
        Yy_ArenaBlock *const block = arena->blocks;
        if (block != NULL) {
            uintptr_t const new_end = old_start + new_size;
            uintptr_t const block_start = (uintptr_t)(block);
            uintptr_t const block_at_idx = block_start + block->idx;
            // Check if the old memory ends at the current arena state and it fits in the current arena block. This works for both increasing and decreasing reallocations.
            if (old_end == block_at_idx) {
                if (new_end <= block_start + block->size) {
                    block->idx = new_end - block_start;
                    return Yy_Ok;
                } else {
                    yy_assert(new_end > old_end);
                    if (block_request_remaining_size(arena->backing_allocator, block, new_end - old_end)) {
                        yy_assert(new_end <= block_start + block->size);
                        block->idx = new_end - block_start;
                        return Yy_Ok;
                    }
                }
            }
        }
        if (new_size <= old_size)
            return Yy_Ok;
    } else {
        yy_assert(old_size == 0);
        if (new_size <= old_size)
            return Yy_Ok;
    }
    return Yy_Bad;
}

/// Reallocate memory at `old_ptr` of size `old_size` to `new_size` and `new_align`.
NODISCARD static void *
yy_arena_realloc_size_align(Yy_Arena *arena, void *old_ptr, size_t old_size, size_t new_size, size_t new_align)
{
    if (yy_is_ok(yy_arena_resize_in_place_size_align(arena, old_ptr, old_size, new_size, new_align))) {
        return old_ptr;
    } else {
        // Can't use the same pointer.
        size_t const size_to_copy = yy_min(old_size, new_size);
        void *result = yy_arena_alloc_size_align(arena, new_size, new_align);
        if (size_to_copy > 0) memcpy(result, old_ptr, size_to_copy);
        return result;
    }
    yy_unreachable();
}


/// Save a state of a `Yy_Arena`. Restore it with `yy_arena_save_restore` or forget it with `yy_arena_save_forget`.
void
yy_arena_save_begin(Yy_Arena *arena, Yy_ArenaSave *ret_saved)
{
    *ret_saved = Yy_ArenaSave {
        .block = arena->blocks,
#if YY_ARENA_SAFETY_CHECKS
        .prev_saved_count = arena->saved_count++,
        .prev_saved_block_start = arena->saved_block_start,
        .prev_saved_block_idx = arena->saved_block_idx,
#endif
        .idx = arena->blocks != nullptr ? arena->blocks->idx : 0,
    };
#if YY_ARENA_SAFETY_CHECKS
    arena->saved_block_start = (uintptr_t)arena->blocks;
    arena->saved_block_idx = ret_saved->idx;
#endif
}

/// Forget about a saved arena state. This is needed if you need to restore an arena to a state on error and on success you keep it as is.
void
yy_arena_save_forget(Yy_Arena *arena, Yy_ArenaSave *saved)
{
    (void)arena;
#if YY_ARENA_SAFETY_CHECKS
    yy_assert(saved->prev_saved_count == --arena->saved_count);
    arena->saved_block_start  = saved->prev_saved_block_start;
    arena->saved_block_idx    = saved->prev_saved_block_idx;
#endif
    yy_debug_invalidate_ptr_one(saved);
}

/// Restore a state of `Yy_ArenaSave` that was saved with `yy_arena_save_begin`.
void
yy_arena_save_restore(Yy_Arena *arena, Yy_ArenaSave *saved_)
{
#if YY_ARENA_SAFETY_CHECKS
    yy_assert(saved_->prev_saved_count == --arena->saved_count);
    arena->saved_block_start  = saved_->prev_saved_block_start;
    arena->saved_block_idx    = saved_->prev_saved_block_idx;
#endif
    // NOTE: Don't reallocate something that is born before or that lives longer than the `Yy_ArenaSave`.
    // The first block will not be freed. It will stay as the head of the block list. I will only free the blocks between saved->block and this first_block (not inclusive).
    Yy_ArenaBlock *first_block = arena->blocks;
    // Copy `saved_` into the stack and write garbage to the old value. Write garbage to prevent restoring twice. Make a copy in the stack because `saved_` might be inside a block that will be freed.
    Yy_ArenaSave saved = *saved_;
    yy_debug_invalidate_ptr_one(saved_);
    while (true) {
        if (arena->blocks == NULL) {
            // If this assert crashes, you called restore with wrong arguments (saved does not match arena or you tried to restore to a newer state).
            yy_assert(saved.block == NULL);
            break;
        }
        if (arena->blocks == saved.block) {
            // Assert that the saved position is older than the current position. See the NOTE regarding reallocation.
            yy_assert((uintptr_t)saved.block + saved.idx <= (uintptr_t)arena->blocks + arena->blocks->idx);
            arena->blocks->idx = saved.idx;
            break;
        }
        Yy_ArenaBlock *block_to_free = arena->blocks;
        arena->blocks = block_to_free->prev;
        // Don't free the first block. I'll insert it to the list later.
        if (block_to_free != first_block) {
            arena_block_destroy(arena->backing_allocator, block_to_free);
        } else {
            // Make sure I can't access this value because it might be garbage later.
            first_block->prev = NULL;
        }
    }
    yy_assert(saved.block == arena->blocks);
    if (first_block != arena->blocks) {
        // Insert `first_block` to the list of blocks. This is done to prevent reallocating blocks too often if I save and restore in a loop.
        yy_assert(first_block->size >= sizeof(Yy_ArenaBlock));
        first_block->idx = sizeof(Yy_ArenaBlock);
        first_block->prev = arena->blocks;
        arena->blocks = first_block;
    }
}


static void arena_allocator_raw_proc_free(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len
)
{
    yy_assert(old_mem_len != 0);
    Yy_Arena *const arena = static_cast<Yy_Arena*>(opaque_handle);
    YyStatus ignore = yy_arena_resize_in_place_size_align(arena, old_mem_ptr, old_mem_len, 0, 1);
    (void) ignore;
}

NODISCARD static YyStatus arena_allocator_raw_proc_resize_in_place(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    yy_assert(new_len != 0);
    Yy_Arena *const arena = static_cast<Yy_Arena*>(opaque_handle);
    YyStatus const x = yy_arena_resize_in_place_size_align(
                                arena, old_mem_ptr, old_mem_len, new_len, new_align);
    return x;
}

NODISCARD static Yy_Allocator_Result arena_allocator_raw_proc_realloc(
    void *opaque_handle,
    void *old_mem_ptr, usize old_mem_len,
    usize new_align,
    usize new_len
)
{
    yy_assert(new_len != 0);
    Yy_Arena *const arena = static_cast<Yy_Arena*>(opaque_handle);
    void *const x = yy_arena_realloc_size_align(
                        arena, old_mem_ptr, old_mem_len, new_len, new_align);
    yy_assert(x != Yy_Allocator_Result__failure);
    if (x == NULL && new_len != 0) {
        return Yy_Allocator_Result__failure;
    }
    return x;
}

NODISCARD static usize arena_allocator_raw_proc_align_size_forward(
    void *opaque_handle,
    usize size
)
{
    (void) opaque_handle;
    return size;
}

static Yy_Allocator_Vtable const arena_allocator_vtable = {
    .free = arena_allocator_raw_proc_free,
    .resize_in_place = arena_allocator_raw_proc_resize_in_place,
    .realloc = arena_allocator_raw_proc_realloc,
    .align_size_forward = arena_allocator_raw_proc_align_size_forward,
};

NODISCARD Yy_Allocator yy_arena_allocator(Yy_Arena *arena)
{
    Yy_Allocator const result = {
        .opaque_handle = arena,
        .vtable = &arena_allocator_vtable,
    };
    return result;
}

