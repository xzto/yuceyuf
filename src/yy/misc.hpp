#pragma once

#include "yy/basic.hpp"
#include "yy/option.hpp"
#include "yy/sv.hpp"
#include <functional>
#include <ranges>
#include <concepts>
#include <bit>


#define MemoryUnit_Fmt "%.03f%s"
#define MemoryUnit_Arg(x) (x).number, (x).unit
struct MemoryUnit {
    double number;
    char const* unit;
    static constexpr MemoryUnit from_bytes(usize byte_count) {
        double number = static_cast<double>(byte_count);
        int unit = 0;
        while (number > 1024.0 && unit < 3) {
            number /= 1024.0;
            unit++;
        }
        switch (unit) {
            case 0:
                return MemoryUnit{.number = number, .unit = "B"};
            case 1:
                return MemoryUnit{.number = number, .unit = "KiB"};
            case 2:
                return MemoryUnit{.number = number, .unit = "MiB"};
            case 3:
                return MemoryUnit{.number = number, .unit = "GiB"};
            default:
                yy_unreachable();
        }
    }
};

/// stores the result in x. returns whether overflow happened.
template <typename A, typename B, typename Result>
NODISCARD inline constexpr bool yy_overflow_add(A a, B b, Result* x) { return __builtin_add_overflow(a, b, x); }
/// stores the result in x. returns whether overflow happened.
template <typename A, typename B, typename Result>
NODISCARD inline constexpr bool yy_overflow_sub(A a, B b, Result* x) { return __builtin_sub_overflow(a, b, x); }
/// stores the result in x. returns whether overflow happened.
template <typename A, typename B, typename Result>
NODISCARD inline constexpr bool yy_overflow_mul(A a, B b, Result* x) { return __builtin_mul_overflow(a, b, x); }


template <typename A, typename Result>
NODISCARD inline constexpr YyStatus yy_try_int_cast(A a, Result* x) {
  return yy_overflow_add(a, 0, x) ? Yy_Bad : Yy_Ok;
}

template <std::integral T>
NODISCARD inline constexpr T yy_wrapping_add(T a, T b) { T result; (void) yy_overflow_add(a, b, &result); return result; }
template <std::integral T>
NODISCARD inline constexpr T yy_wrapping_sub(T a, T b) { T result; (void) yy_overflow_sub(a, b, &result); return result; }
template <std::integral T>
NODISCARD inline constexpr T yy_wrapping_mul(T a, T b) { T result; (void) yy_overflow_mul(a, b, &result); return result; }


NODISCARD inline constexpr auto yy_is_power_of_two(std::integral auto value) -> bool
{
    return value != 0 && std::popcount(value) == 1;
}

NODISCARD inline constexpr auto yy_is_aligned_power_of_two(usize alignment, usize value) -> bool
{
    yy_assert(yy_is_power_of_two(alignment));
    return (value & ~(alignment - 1)) == value;
}

/// This could overflow.
NODISCARD inline constexpr auto yy_align_forward_power_of_two(usize alignment, usize value) -> usize
{
    yy_assert(yy_is_power_of_two(alignment));
    usize const foo = alignment - 1;
    usize const result = (value + foo) & ~foo;
    yy_assert(result >= value);
    return result;
}

template <std::unsigned_integral T>
NODISCARD inline constexpr auto yy_next_power_of_two(T const x) -> T { return std::bit_ceil(x); }

// NOTE: sqrt(2048.0) == 45.25 but this function returns 32.
template <std::unsigned_integral T>
NODISCARD inline constexpr auto yy_sqrt_power_of_two_round_down(T const x) -> T
{
    yy_assert(yy_is_power_of_two(x));
    return ((usize)1 << (std::countr_zero(x) / 2));
}
template <std::unsigned_integral T>
NODISCARD inline constexpr auto yy_log2_of_power_of_two(T const x) -> T
{
    yy_assert(yy_is_power_of_two(x));
    return (T)std::countr_zero(x);
}


namespace yy {

struct NoCopyNoMove {
    constexpr NoCopyNoMove() = default;
    constexpr ~NoCopyNoMove() = default;
    constexpr NoCopyNoMove(const NoCopyNoMove&) = delete;
    constexpr NoCopyNoMove& operator=(const NoCopyNoMove&) = delete;
    constexpr NoCopyNoMove(NoCopyNoMove&&) = delete;
    constexpr NoCopyNoMove& operator=(NoCopyNoMove&&) = delete;
};
struct MoveOnly {
    constexpr MoveOnly() = default;
    constexpr ~MoveOnly() = default;
    constexpr MoveOnly(const MoveOnly&) = delete;
    constexpr MoveOnly& operator=(const MoveOnly&) = delete;
    constexpr MoveOnly(MoveOnly&&) = default;
    constexpr MoveOnly& operator=(MoveOnly&&) = default;
};

struct LockCounter;
struct ScopedLockCounter : yy::NoCopyNoMove {
    inline constexpr ~ScopedLockCounter();
private:
    LockCounter* lock;
    friend LockCounter;
    constexpr ScopedLockCounter(LockCounter* lock) : lock(lock) {}
};

struct LockCounter {
    usize count{0};
    constexpr LockCounter() = default;
    NODISCARD constexpr auto scoped_lock() -> ScopedLockCounter {
        lock();
        return { this };
    }
    constexpr auto check_unlocked() const -> void { yy_assert(count == 0); }
    NODISCARD constexpr auto is_locked() const -> bool { return count > 0; }
    constexpr auto lock() -> void { count++; }
    constexpr auto unlock() -> void {
        yy_assert(count > 0);
        count--;
    }
};
constexpr ScopedLockCounter::~ScopedLockCounter() { lock->unlock(); }

template <std::integral To, std::integral From>
NODISCARD inline constexpr auto checked_int_cast(From src) -> Option<To> {
    To result;
    return yy_overflow_add(src, 0, &result) ? None : Option(result);
}

template <std::integral T>
NODISCARD inline constexpr auto checked_add(T a, T b) -> Option<T> {
    T result;
    return yy_overflow_add(a, b, &result) ? None : Option(result);
}
template <std::integral T>
NODISCARD inline constexpr auto checked_sub(T a, T b) -> Option<T> {
    T result;
    return yy_overflow_sub(a, b, &result) ? None : Option(result);
}
template <std::integral T>
NODISCARD inline constexpr auto checked_neg(T b) -> Option<T> {
    T result;
    return yy_overflow_sub(T(0), b, &result) ? None : Option(result);
}
template <std::integral T>
NODISCARD inline constexpr auto checked_mul(T a, T b) -> Option<T> {
    T result;
    return yy_overflow_mul(a, b, &result) ? None : Option(result);
}

template <std::integral T>
NODISCARD inline constexpr auto checked_div(T a, T b) -> Option<T> {
    if constexpr (std::is_signed_v<T>) {
        if (b == 0 || (b == -1 && a == std::numeric_limits<T>::min())) {
            return None;
        }
    } else {
        if (b == 0) {
            return None;
        }
    }
    T result = a / b;
    return result;
}

template <std::integral T>
NODISCARD inline constexpr auto checked_euclidean_mod(T a, T b) -> Option<T> {
    if (b == 0) {
        return None;
    }
    if constexpr (std::is_signed_v<T>) {
        // covers `a == std::numeric_limits<T>::min()`
        if (b == -1)
            return 0;
    }
    if (b > 0) {
        // Should we branch here?
        if (a >= 0) {
            return a % b;
        }
        return ((a % b) + b) % b;
    } else {
        yy_assert(std::is_signed_v<T>);
        auto const minus_a = TRY_OPTION(checked_neg(a));
        auto const minus_b = TRY_OPTION(checked_neg(b));
        return checked_neg(TRY_OPTION(checked_euclidean_mod(minus_a, minus_b))).unwrap();
    }
}

/// Just like std::ranges::find, but return an Option instead of std::ranges::end()
template <
    std::ranges::input_range R,
    typename T,
    typename Proj = std::identity>
    requires std::indirect_binary_predicate<
                 std::ranges::equal_to,
                 std::projected<std::ranges::iterator_t<R>, Proj>,
                 const T*>
NODISCARD constexpr auto try_find(R&& r, T const& value, Proj proj = {}) -> Option<std::ranges::iterator_t<R>> {
    auto first = std::ranges::begin(r);
    auto last = std::ranges::end(r);
    for (; first != last; ++first) {
        if (std::invoke(proj, *first) == value) {
            return first;
        }
    }
    return None;
}

/// Just like std::ranges::find_if, but return an Option instead of std::ranges::end()
template <
    std::ranges::input_range R,
    typename Proj = std::identity,
    std::indirect_unary_predicate<std::projected<std::ranges::iterator_t<R>, Proj>> Predicate>
NODISCARD constexpr auto try_find_if(R&& r, Predicate predicate, Proj proj = {}) -> Option<std::ranges::iterator_t<R>> {
    auto first = std::ranges::begin(r);
    auto last = std::ranges::end(r);
    for (; first != last; ++first) {
        if (std::invoke(predicate, std::invoke(proj, *first))) {
            return first;
        }
    }
    return None;
}

/// Return an Option of the found element index.
template <
    std::ranges::input_range R,
    typename T,
    typename Proj = std::identity,
    typename Index = decltype(std::ranges::size(std::declval<R>()))>
    requires std::indirect_binary_predicate<
                 std::ranges::equal_to,
                 std::projected<std::ranges::iterator_t<R>, Proj>,
                 const T*>
NODISCARD constexpr auto try_find_index(R&& r, T const& value, Proj proj = {}) -> Option<Index> {
    Index i = 0;
    auto first = std::ranges::begin(r);
    auto last = std::ranges::end(r);
    for (; first != last; ++first, ++i) {
        if (std::invoke(proj, *first) == value) {
            return i;
        }
    }
    return None;
}

/// Return an Option of the found element index.
template <
    std::ranges::input_range R,
    typename Proj = std::identity,
    std::indirect_unary_predicate<std::projected<std::ranges::iterator_t<R>, Proj>> Predicate,
    typename Index = decltype(std::ranges::size(std::declval<R>()))>
NODISCARD constexpr auto try_find_if_index(R&& r, Predicate predicate, Proj proj = {}) -> Option<Index> {
    Index i = 0;
    auto first = std::ranges::begin(r);
    auto last = std::ranges::end(r);
    for (; first != last; ++first, ++i) {
        if (std::invoke(predicate, std::invoke(proj, *first))) {
            return i;
        }
    }
    return None;
}


template <typename T>
struct ScopedVariable : yy::NoCopyNoMove {
    T& variable;
    T old_value;
    constexpr ScopedVariable(T& variable_) : variable(variable_), old_value(yy::clone<T>(variable_)) {}

    constexpr ScopedVariable(T& variable_, auto&& new_value) : variable(variable_), old_value(variable_) {
        variable = std::forward<decltype(new_value)>(new_value);
    }
    constexpr ~ScopedVariable() { variable = std::move(old_value); }
};

struct CheckedNoMove : MoveOnly {
    bool no_move{false};
    NODISCARD constexpr auto can_move() const -> bool { return !no_move; }
    constexpr CheckedNoMove() {}
    constexpr ~CheckedNoMove() { yy_assert(can_move()); }
    constexpr CheckedNoMove(const CheckedNoMove&) = delete;
    constexpr CheckedNoMove& operator=(const CheckedNoMove&) = delete;
    constexpr CheckedNoMove(CheckedNoMove&& other) : no_move(other.no_move) { yy_assert(can_move()); }
    constexpr CheckedNoMove& operator=(CheckedNoMove&& other) {
        if (this != &other) {
            yy_assert(can_move());
            yy_assert(other.can_move());
            std::destroy_at(this);
            std::construct_at(this, std::move(other));
        }
        return *this;
    }
};

template <std::integral Integer>
struct CheckedNoMoveRef : MoveOnly {
    Integer refs{0};
    NODISCARD constexpr auto can_move() const -> bool { return refs == 0; }
    constexpr auto ref() -> void { refs++; }
    constexpr auto unref() -> void {
        yy_assert(refs > 0);
        refs--;
    }
    constexpr CheckedNoMoveRef() {}
    constexpr ~CheckedNoMoveRef() { yy_assert(can_move()); }
    constexpr CheckedNoMoveRef(const CheckedNoMoveRef&) = delete;
    constexpr CheckedNoMoveRef& operator=(const CheckedNoMoveRef&) = delete;
    constexpr CheckedNoMoveRef(CheckedNoMoveRef&& other) : refs(other.refs) { yy_assert(can_move()); }
    constexpr CheckedNoMoveRef& operator=(CheckedNoMoveRef&& other) {
        if (this != &other) {
            yy_assert(can_move());
            yy_assert(other.can_move());
            std::destroy_at(this);
            std::construct_at(this, std::move(other));
        }
        return *this;
    }
};

template <typename T, bool has_value_>
struct OrEmpty;

template <typename T>
struct OrEmpty<T, true> {
    [[no_unique_address]] T x;
    constexpr OrEmpty(auto&&... args) : x(std::forward<decltype(args)>(args)...) {}
    NODISCARD constexpr auto get() const& -> Option<T const&> { return x; }
    NODISCARD constexpr auto get() & -> Option<T&> { return x; }
    NODISCARD constexpr auto operator->() & -> T* { return std::addressof(x); }
    NODISCARD constexpr auto operator->() const& -> T const* { return std::addressof(x); }
    NODISCARD constexpr auto unwrap() const& -> T const& { return x; }
    NODISCARD constexpr auto unwrap() & -> T& { return x; }
    NODISCARD constexpr auto unwrap_or(T) const -> T { return x; }
    NODISCARD consteval static auto has_value() -> bool { return true; }
};

template <typename T>
struct OrEmpty<T, false> {
    constexpr OrEmpty(auto&&... ignored_ [[maybe_unused]]) {}
    NODISCARD constexpr auto get() const& -> Option<T const&> { return None; }
    NODISCARD constexpr auto get() & -> Option<T&> { return None; }
    NODISCARD constexpr auto operator->() & -> T* { yy_unreachable(); }
    NODISCARD constexpr auto operator->() const& -> T const* { yy_unreachable(); }
    NODISCARD constexpr auto unwrap() const& -> T const& { yy_unreachable(); }
    NODISCARD constexpr auto unwrap() & -> T& { yy_unreachable(); }
    NODISCARD constexpr auto unwrap_or(T other) const -> T { return other; }
    NODISCARD consteval static auto has_value() -> bool { return false; }
};

// Usage: `[[no_unique_address]] WhenDebug<T> x;`
template <typename T>
using WhenDebug = OrEmpty<T, !YY_DEFINED_NDEBUG>;


template <typename T, T reset_value>
    requires std::is_trivial_v<T>
struct ResettingMove {
    T x;
    constexpr operator T() const { return x; }
    constexpr ResettingMove(T x_) : x(std::move(x_)) {}
    constexpr ResettingMove() = default;
    constexpr ~ResettingMove() = default;
    constexpr ResettingMove(const ResettingMove&) = default;
    constexpr ResettingMove& operator=(const ResettingMove&) = default;
    constexpr ResettingMove(ResettingMove&& other) : x(std::move(other.x)) {
        std::destroy_at(std::addressof(other.x));
        std::construct_at(std::addressof(other.x), reset_value);
    }
    constexpr ResettingMove& operator=(ResettingMove&& other) {
        if (this != &other) {
            std::destroy_at(this);
            std::construct_at(this, std::move(other));
        }
        return *this;
    }
};


}

using yy::LockCounter;
