#ifndef ___INCLUDE_GUARD__y1FRPIgm73mHqezHUNdyPc8hWiXB4z7DwP0NCJCm
#define ___INCLUDE_GUARD__y1FRPIgm73mHqezHUNdyPc8hWiXB4z7DwP0NCJCm

#include <cstddef>
#include <cstdint>
#include <cstring>
#include <cinttypes> // IWYU pragma: keep
#include <climits> // IWYU pragma: keep
#include <functional>
#include <limits>
#include <utility>
#include <type_traits>


/// An empty macro. This is useful for multi line macros, so that the last meaningful line also
/// has a backslash.
#define YY_EMPTY_MACRO

#define JOIN2_A(a, b) a##b
#define JOIN2(a, b) JOIN2_A(a, b)
#define STRINGIFY_A(x) #x
#define STRINGIFY(x) STRINGIFY_A(x)



#if defined(__GNUC__) && 1
# define yy_static_assert_and_zero(cond, ...) ((void)( __extension__ ({static_assert(cond __VA_OPT__(,) __VA_ARGS__); 0; })))
#else
# define yy_static_assert_and_zero(cond, ...) ((void)0)
#endif


#if 1
# define yy_static_assert_types_and_zero(Expected, Got) \
    yy_static_assert_and_zero((std::is_same_v<Expected, Got>), \
        "Expected type `" STRINGIFY(Expected) "` but got `" STRINGIFY(Got) "`")
#else
# define yy_static_assert_types_and_zero(Expected, Got) ((void)0)
#endif

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef ptrdiff_t isize;
typedef size_t usize;
static_assert(sizeof(i8) == 1 && sizeof(u8) == 1,   "Invalid builtin integer types");
static_assert(sizeof(i16) == 2 && sizeof(u16) == 2, "Invalid builtin integer types");
static_assert(sizeof(i32) == 4 && sizeof(u32) == 4, "Invalid builtin integer types");
static_assert(sizeof(i64) == 8 && sizeof(u64) == 8, "Invalid builtin integer types");
static_assert(sizeof(isize) == sizeof(usize),       "Invalid builtin integer types");
static_assert(sizeof(usize) == sizeof(ptrdiff_t),   "sizeof(ptrdiff_t) != sizeof(size_t)");
static_assert(sizeof(void *) == sizeof(usize),      "sizeof(void*) != sizeof(usize)");
#define USIZE_MAX SIZE_MAX
#define ISIZE_MAX PTRDIFF_MAX
#define ISIZE_MIN PTRDIFF_MIN


#define FIELD_PARENT_POINTER(ParentType, field_name, field_ptr) \
    (ParentType*)((usize)(field_ptr) - (usize)(offsetof(ParentType, field_name)))
#define PACKED __attribute__((__packed__))
#define INLINE [[gnu::always_inline]]
#define NOINLINE [[gnu::noinline]]
#define NORETURN [[noreturn]]
#define UNUSED [[maybe_unused]]
#define PRINTF_LIKE(a, b) __attribute__((format(printf, a, b)))
#define COLD [[gnu::cold]]
#define NODISCARD [[nodiscard]]

#define ARRAY_COUNT(x) (sizeof((x))/sizeof((x)[0]))


// Used when variables are "optional" (most of the time pointers, using NULL as the invalid value).
#define OPT
// Used instead of OPT when the sentinel value is not obvious.
#define OPTX(sentinel_value)

// A pointer points to more than one element.
#define MANY


#define yy_TypeOf(value) std::remove_reference_t<decltype(value)>


#if defined(__x86_64__) || defined(__i386__)
# define yy_breakpoint__internal(counter) do {     \
    asm volatile ("int3");                              \
    i32 volatile JOIN2(___debug_break_, counter) = 123; \
    (void)JOIN2(___debug_break_, counter);              \
} while (false)
#else
// No breakpoints for this architecture.
# define yy_breakpoint__internal(counter) std::abort()
#endif
#define yy_breakpoint() yy_breakpoint__internal(__COUNTER__)

#define yy_panic(msg) yy_panic_fmt("%s", msg)
#ifndef NDEBUG
# define YY_DEFINED_NDEBUG 0
# define yy_panic_fmt(fmt, ...) ((void)yy_panic_handler_fmt(__FILE__, __LINE__, fmt __VA_OPT__(,) __VA_ARGS__))
#else
# define YY_DEFINED_NDEBUG 1
# define yy_panic_fmt(fmt, ...) ((void)yy_panic_handler_fmt(NULL, 0, fmt __VA_OPT__(,) __VA_ARGS__))
#endif
#define yy_todo() yy_panic_fmt("TO""DO in `%s`", __func__)

#if !YY_DEFINED_NDEBUG
# define YY_SAFE_SLOW 1
#else
# define YY_SAFE_SLOW 0
#endif

#if YY_SAFE_SLOW
# define yy_unreachable()              yy_panic("Unreachable")
# define yy_unreachable_fmt(fmt, ...)  yy_panic_fmt("Unreachable: " fmt __VA_OPT__(,) __VA_ARGS__)
#else
# define yy_unreachable()              ((void)__builtin_unreachable())
# define yy_unreachable_fmt(fmt, ...)  ((void)__builtin_unreachable())
#endif

#define yy_assert(x) \
    ((__builtin_expect((x), true)) ? (void)0 : yy_unreachable_fmt("Assertion fail: `%s`", #x), (void)0)
#define yy_assert_fmt(x, fmt, ...) \
    ((__builtin_expect((x), true)) ? (void)0 : yy_unreachable_fmt("Assertion fail: `%s`: " fmt, #x __VA_OPT__(,) __VA_ARGS__), (void)0)
#define yy_assert_and_zero(cond) yy_assert(cond)


COLD NOINLINE NORETURN PRINTF_LIKE(3, 4) void yy_panic_handler_fmt(const char *OPT file, int OPT line, const char *fmt, ...);

template <typename T> NODISCARD inline constexpr T yy_min(T a, T b) { return a < b ? a : b; }
template <typename T> NODISCARD inline constexpr T yy_max(T a, T b) { return a > b ? a : b; }

#define yy_at_most(a, b) yy_min(a, b)
#define yy_at_least(a, b) yy_max(a, b)

// NOLINTNEXTLINE
inline void yy_debug_invalidate_bytes(u8 *ptr, usize len)
{
#if YY_SAFE_SLOW
    memset(ptr, 0xaa, len);
#else
    (void) ptr;
    (void) len;
#endif
}

template <typename T>
    requires std::is_trivially_destructible_v<T>
inline auto yy_debug_invalidate_ptr_one(T* x) -> void {
    yy_debug_invalidate_bytes(reinterpret_cast<u8*>(x), sizeof(T));
}

template <typename T>
    requires std::is_trivially_destructible_v<T>
inline auto yy_debug_invalidate_ptr_len(T* x, usize len) -> void {
    yy_debug_invalidate_bytes(reinterpret_cast<u8*>(x), len * sizeof(T));
}


enum class YyStatus_Inner {
    Ok = 0,
    Bad,
};

struct [[nodiscard]] YyStatus {
    YyStatus_Inner x;

    NODISCARD constexpr auto is_ok() const -> bool { return x == YyStatus_Inner::Ok; }
    NODISCARD constexpr auto is_err() const -> bool { return x != YyStatus_Inner::Ok; }
    constexpr auto unwrap() const -> void { yy_assert(is_ok()); }
};

static YyStatus constexpr Yy_Ok = { .x = YyStatus_Inner::Ok };
//static YyStatus const Yy_Bad = { .x = YyStatus_Inner::Bad };
#define Yy_Bad Yy_Bad___get_cold()
COLD static inline constexpr YyStatus Yy_Bad___get_cold() { return { .x = YyStatus_Inner::Bad }; }

NODISCARD static inline constexpr bool yy_is_ok(YyStatus status) { return status.is_ok(); }
NODISCARD static inline constexpr bool yy_is_err(YyStatus status)  { return status.is_err(); }
static inline constexpr void yy_unwrap_ok(YyStatus status) { status.unwrap(); }

// Macros to easily return errors.
#define TRY(x) TRY__(x, JOIN2(__tmp_try_, __COUNTER__))
#define TRY__(x, err_var) CATCH2(x, err_var, return err_var)

#define CATCH(x, ...) CATCH__(x, JOIN2(__tmp_catch_, __COUNTER__), __VA_ARGS__)
#define CATCH__(x, err_var, ...) CATCH2(x, err_var, { (void)err_var; __VA_ARGS__; } )

#define CATCH2(x, err_var, ...) do { YyStatus err_var = (x); if (yy_is_err(err_var)) { __VA_ARGS__; } } while (false)


#define todo() yy_panic("TO""DO")
#define todo_assert(cond) do { if (!(cond)) todo(); } while (false)






// TODO: Generalize yy_invalidOf_StructType
// - [ ] Instead of yy_invalidOf_StructType<T> => some value of T, I want two functions:
//       `auto is_invalid_of(T const&) -> bool` and `auto make_invalid_of() -> T`
// - [ ] Maybe accept a wrapper type: `auto is_invalid_of(WrappedT const&) -> bool`,
//       `auto make_invalid_of() -> WrappedT` and `auto wrap(T) -> WrappedT`.
//       For enums, WrappedT will be the underlying type of the enum.

// NOTE: Don't define this directly. Use the macro YY_DECL_INVALID_OF or YY_DECL_INVALID_OF_INSIDE_CLASS.
template <typename T>
struct yy_invalidOf_StructType;

template <typename T>
concept yy_invalidOf_StructType_was_defined_inside_class = (std::is_class_v<T> || std::is_union_v<T>) && requires {
    T::yy_invalidOf_StructType_make_invalid_of;
};

// Define yy_invalidOf_StructType for classes that defined YY_DECL_INVALID_OF_INSIDE_CLASS.
template <typename T>
    requires yy_invalidOf_StructType_was_defined_inside_class<T>
struct yy_invalidOf_StructType<T> : public std::integral_constant<T, T::yy_invalidOf_StructType_make_invalid_of()> {};


// You must be in the global namespace to call this.
#define YY_DECL_INVALID_OF(Type, invalid_value_expr)                                            \
    template <>                                                                                      \
    struct yy_invalidOf_StructType<Type> : public std::integral_constant<Type, invalid_value_expr> { \
        static_assert(!yy_invalidOf_StructType_was_defined_inside_class<Type>);                      \
    };                                                                                               \
    /* static_assert checks semicolon */ static_assert(true)

// You should only call this inside a class/struct/union.
#define YY_DECL_INVALID_OF_INSIDE_CLASS(Type, invalid_value_expr)                                             \
    [[maybe_unused]] inline consteval auto _yy_invalidOf_StructType_make_invalid_of__check_types() const -> void { \
        /* This is a separate method because we need to use decltype(this).*/                                      \
        static_assert(                                                                                             \
            std::same_as<Type const*, decltype(this)>,                                                             \
            "YY_DECL_INVALID_OF_INSIDE_CLASS: the Type parameter is wrong"                                    \
        );                                                                                                         \
    }                                                                                                              \
    NODISCARD static constexpr auto yy_invalidOf_StructType_make_invalid_of() -> Type {                            \
        return invalid_value_expr;                                                                                 \
    }                                                                                                              \
    /* static_assert checks semicolon */ static_assert(true)


struct Void {
  friend constexpr auto operator==(Void, Void) -> bool = default;
  friend constexpr auto operator<=>(Void, Void) -> std::strong_ordering = default;
};



template <typename F>
class yy_defer_finalizer__ {
    F f;
    bool moved;

public:
    constexpr yy_defer_finalizer__(yy_defer_finalizer__ const&) = delete;
    constexpr yy_defer_finalizer__(yy_defer_finalizer__&& other) : f(std::move(other.f)), moved(other.moved) {
        other.moved = true;
    }
    constexpr yy_defer_finalizer__(int, auto&& g) : f(std::forward<decltype(g)>(g)), moved(false) {}

    constexpr ~yy_defer_finalizer__() {
        if (!moved) {
            f();
        }
    }
};

static constexpr struct {
    template <typename F>
    constexpr yy_defer_finalizer__<F> operator<<(F&& f) const {
        return yy_defer_finalizer__<F>(0, std::forward<F>(f));
    }
} yy_defer_obj__;

#define defer auto JOIN2(__defer_, __COUNTER__) = yy_defer_obj__ << [&]()



namespace yy {

struct TheOriginalArgcArgv {
    int OPT argc;
    char const* const* OPT argv;
};
inline TheOriginalArgcArgv constinit the_original_argc_argv = { .argc = 0, .argv = nullptr };


template <class A, class B>
concept same_as_remove_cvref = std::same_as<std::remove_cvref_t<A>, std::remove_cvref_t<B>>;

template <typename T>
concept trivial_raii = std::is_trivially_move_assignable_v<T> && std::is_trivially_move_constructible_v<T> &&
                       std::is_trivially_copy_assignable_v<T> && std::is_trivially_copy_constructible_v<T> &&
                       std::is_trivially_destructible_v<T>;
template <typename T>
concept trivial_raii_maybe_moveonly =
    std::is_trivially_move_assignable_v<T> && std::is_trivially_move_constructible_v<T> &&
    ((std::is_trivially_copy_assignable_v<T> && std::is_trivially_copy_constructible_v<T>) ||
     (!std::is_copy_assignable_v<T> && !std::is_copy_constructible_v<T>)) &&
    std::is_trivially_destructible_v<T>;

// NOTE: signed char (aka int8_t) and unsigned char (aka uint8_t) are not actually character types
template <typename T>
concept char_type =
    yy::same_as_remove_cvref<T, char> ||
    yy::same_as_remove_cvref<T, wchar_t> ||
    yy::same_as_remove_cvref<T, char8_t> ||
    yy::same_as_remove_cvref<T, char16_t> ||
    yy::same_as_remove_cvref<T, char32_t>;
static_assert(yy::char_type<char>);
static_assert(!yy::char_type<i8>);
static_assert(!yy::char_type<u8>);
static_assert(!yy::char_type<u16>);
static_assert(!yy::char_type<u32>);

static constexpr struct Uninitialized {
    template <typename T>
    __attribute__((__no_sanitize__("undefined")))
    constexpr operator T() const
        requires yy::trivial_raii_maybe_moveonly<T>
    {
        alignas(T) std::byte bytes[sizeof(T)];
        yy_debug_invalidate_ptr_len(&bytes[0], sizeof(T));
        // We need to move if the copy constructor was deleted.
        return std::move(*reinterpret_cast<T*>(&bytes[0]));
    }
    template <typename T>
    constexpr operator T() const
        requires (!yy::trivial_raii_maybe_moveonly<T>) && std::is_default_constructible_v<T>
    {
        // Default construct.
        T result;
        return result;
    }
} uninitialized;

/// Equivalent to `yy_unreachable()`, but as far as the compiler is concerned, it has type `T`.
template <typename T>
[[nodiscard]] [[noreturn]] inline auto never() -> T {
    yy_unreachable();
}

template <typename Result, typename Function, typename... Args>
    requires std::convertible_to<std::invoke_result_t<Function, Args...>, Result>
auto invoke_or_convert(Function&& func, Args&&... args) -> Result {
    return std::invoke(std::forward<Function>(func), std::forward<Args>(args)...);
}

template <typename Result, typename Value, typename... Unused>
    requires std::convertible_to<Value, Result>
auto invoke_or_convert(Value&& value, Unused&&...) -> Result {
    return std::forward<Value>(value);
}

/// Include "yy/option.hpp"
template <typename T>
class NODISCARD Option;

template <std::integral T, T sentinel_int>
struct NonSentinelInteger {
    T x;
    constexpr NonSentinelInteger() requires (T() != sentinel_int) = default;
    /// Construct
    constexpr NonSentinelInteger(T value) : x(value) { yy_assert(x != sentinel_int); }
    constexpr operator T() const { return x; }
    constexpr auto operator<=>(NonSentinelInteger const& other) const = default;

    struct sentinel_ctor_t {};
    constexpr NonSentinelInteger(sentinel_ctor_t) : x(sentinel_int) {}
    static constexpr auto sentinel() -> NonSentinelInteger { return NonSentinelInteger(sentinel_ctor_t{}); }

    YY_DECL_INVALID_OF_INSIDE_CLASS(NonSentinelInteger, NonSentinelInteger::sentinel());

    /// Contruct an Option<NonSentinelInteger> from a value that may be a sentinel. You must include "yy/option.hpp".
    static inline constexpr auto try_from(T value_or_sentinel) -> Option<NonSentinelInteger>;
};

template <std::integral T>
using NonMaxInteger = NonSentinelInteger<T, std::numeric_limits<T>::max()>;
template <std::integral T>
using NonZeroInteger = NonSentinelInteger<T, 0>;

using NonMaxUsize = NonMaxInteger<usize>;
using NonMaxU64 = NonMaxInteger<u64>;
using NonMaxU32 = NonMaxInteger<u32>;


template <typename T, typename... Args>
concept has_clone_fn_with_args = requires (T const& x) {
    { x.clone(std::declval<Args>()...) } -> std::same_as<T>;
};

// TODO: Range transformer `r | transform(x => yy::clone(x, clone_args...))`

/// Generic explicit copy constructor. In generic code, you should call with an explicit template parameter: `clone<T>(x)`.
template <typename T>
    requires (!std::is_reference_v<T>) && (std::is_copy_constructible_v<T> && (!has_clone_fn_with_args<T>))
NODISCARD inline constexpr auto clone(std::type_identity_t<T> const& other) -> T { return T(other); }

// Copy a reference.
template <typename T>
    requires std::is_reference_v<T>
NODISCARD inline constexpr auto clone(std::type_identity_t<T> other) -> T { return std::forward<decltype(other)>(other); }

/// Generic explicit copy constructor. In generic code, you should call with an explicit template parameter: `clone<T>(x)`.
template <typename T, typename... Args>
    requires has_clone_fn_with_args<T, Args...>
NODISCARD inline constexpr auto clone(std::type_identity_t<T> const& other, Args&&... args) -> T { return other.clone(std::forward<Args>(args)...); }

template <typename T, typename... Args>
concept can_clone_with_args = requires (T const& x) {
    { yy::clone<T>(x, std::declval<Args>()...) } -> std::same_as<T>;
};

template <class T, class M> M get_member_type_stub__(M T:: *) { yy_unreachable(); }
/// Usage: member_type_t<&Foo::bar>
/// TODO: This doesn't work for references.
template <auto mem> using member_type_t = decltype(::yy::get_member_type_stub__(mem));

template <std::integral T>
constexpr auto div_exact(T a, T b) -> T {
    if constexpr (std::is_signed_v<T>) {
        yy_assert(!(b == -1 && a == std::numeric_limits<T>::min()));
    }
    yy_assert(b != 0 && a % b == 0);
    return a / b;
}

// helper type for the visitor
template<class... Ts>
struct overloaded : Ts... { using Ts::operator()...; };

template <typename T, usize N>
inline constexpr auto make_array_filled(T const& x) -> std::array<T, N> {
  std::array<T, N> result;
  // TODO: yy::fill_with_clone(Range&&, T const&)
  result.fill(x);
  return result;
}

template <typename T, usize N>
inline constexpr auto decay_array(T (&x)[N]) -> T* { return &x[0]; }

namespace literals {}

}

using namespace yy::literals;

using yy::NonMaxU64;
using yy::NonMaxU32;
using yy::NonMaxUsize;

extern bool verbose_timings;

struct Yy__TestGlobalNamespace;
#define YY_CHECK_GLOBAL_NAMESPACE()                                              \
    struct Yy__TestGlobalNamespace;                                                   \
    static_assert(std::is_same_v<Yy__TestGlobalNamespace, ::Yy__TestGlobalNamespace>, \
        "This is not the global namespace.")

inline constexpr auto char_to_u8(char x) -> u8 { return static_cast<u8>(x); }
inline constexpr auto u8_to_char(u8 x) -> char { return static_cast<char>(x); }

#endif//___INCLUDE_GUARD__y1FRPIgm73mHqezHUNdyPc8hWiXB4z7DwP0NCJCm
