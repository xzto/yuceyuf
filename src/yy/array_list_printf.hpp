#pragma once

#include "yy/vec.hpp"

#include <stdarg.h>

using ArrayList_Of_u8 = Vec<u8>;

void array_list_printf_inner(ArrayList_Of_u8 *self, char const *fmt, va_list ap);
PRINTF_LIKE(2, 3)
void array_list_printf(ArrayList_Of_u8 *self, char const *fmt, ...);
void array_list_u8_write_sv(ArrayList_Of_u8 *self, Sv buf);
void array_list_u8_write_bv(ArrayList_Of_u8 *self, Bv buf);
void array_list_u8_write_ptr_len(ArrayList_Of_u8 *self, u8 const *bytes_ptr, usize bytes_len);
void array_list_u8_write_escaped_fancy_sv(ArrayList_Of_u8 *self, Sv sv, Option<usize> max_elems = None);
char const* array_list_u8_leak_into_cstr(ArrayList_Of_u8 *self);
