#include "yy/basic.hpp"
#include "yuceyuf/codegen_x64.hpp"
#include "yy/array_list_printf.hpp"
#include "yy/timer.hpp"
#include "yy/meta/enum.hpp"
#include "yuceyuf/genir.hpp"
#include "yuceyuf/tracy.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static constexpr bool asm_comments = !false;

NORETURN static void ice(void) {
    yy_panic("error: internal compiler error: this is a bug in the yuceyuf compiler");
}

#define LOG_LAP(t, lap, name) do {                                                          \
    lap = timer_lap(t);                                                                     \
    if (verbose_timings)                                                                    \
        fprintf(stderr, "LAP:\t%s:\t%s\t" TimeMs_Fmt "\n", __func__, name, TimeMs_Arg(lap));  \
} while (false)

#define Inst            Yy_Ir_Inst
#define Inst_Tag        Yy_Ir_Inst_Tag
#define InstIndex       Yy_Ir_InstIndex
#define StringIndex     Yy_Ir_StringIndex
#define ExtraIndex      Yy_Ir_ExtraIndex
#define Extra           Yy_Ir_Extra

static u32 intCastFromUsizeToU32(usize x_usize)
{
    u32 result;
    yy_unwrap_ok(yy_try_int_cast(x_usize, &result));
    return result;
}

PRINTF_LIKE(1, 2)
static void logDebug(char const *fmt, ...)
{
    if (verbose_codegen) {
        va_list ap;
        va_start(ap, fmt);
        fprintf(stderr, "codegen_x64(debug): ");
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "\n");
        va_end(ap);
    }
}

PRINTF_LIKE(1, 2)
static void logError(char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "codegen_x64(error): ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    va_end(ap);
}


COLD static void codegen_x64_add_err_msg__(Yuceyuf_ErrorMessageKind kind, Yy_CodegenX64 *codegen, Genir_ErrorLocation const loc, char const *fmt, va_list ap)
{
    Yuceyuf_AstNode* ast = yy::uninitialized;
    switch (loc.tag) {
        case Genir_ErrorLocation_Tag__ast: {
            ast = *MetaTu_get(Genir_ErrorLocation, &loc, ast);
        } break;
        case Genir_ErrorLocation_Tag__inst: {
            ast = codegen->sema->gi->inst_to_ast_map[*MetaTu_get(Genir_ErrorLocation, &loc, inst)];
        } break;
        default:
            yy_unreachable();
    }
    auto const& lex = ast->parent_ast_file->lexer.lex;
    auto const src_region = ast->src.to_src_region_line_and_column(lex);
    codegen->errors.make_error_va(kind, ast->parent_ast_file->filename, src_region, fmt, ap);
}

COLD PRINTF_LIKE(3, 4)
static void codegen_x64_add_error_msg(Yy_CodegenX64 *codegen, Genir_ErrorLocation const loc, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    codegen_x64_add_err_msg__(Yuceyuf_ErrorMessageKind::hard_error, codegen, loc, fmt, ap);
    va_end(ap);
}

COLD PRINTF_LIKE(3, 4)
static void codegen_x64_add_error_note(Yy_CodegenX64 *codegen, Genir_ErrorLocation const loc, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    codegen_x64_add_err_msg__(Yuceyuf_ErrorMessageKind::note, codegen, loc, fmt, ap);
    va_end(ap);
}

struct LtaNodeId {
    u32 x;
    constexpr LtaNodeId() = default;
    explicit constexpr LtaNodeId(u32 x) : x(x) {}
    explicit constexpr LtaNodeId(u64 x) : x(yy::checked_int_cast<u32>(x).unwrap()) {}
};

struct LtaNode {
    u64 debug_gen_id;
    // `sp_offset(%rsp)`
    // a positive rsp offset
    u64 sp_offset;
    u64 size_aligned;
    bool is_free;

    // prev: towards head
    LtaNodeId prev;
    // next: towards tail
    LtaNodeId next;
};

struct LocalTempAllocator {
    Yy_Allocator heap;
    VecUnmanaged<LtaNodeId> node_free_list;
    static_assert(std::is_trivial_v<LtaNode>);
    VecUnmanaged<LtaNode> allocated_nodes;
    // head: top of the stack (lower addresses, lower positive sp offsets)
    // tail: bottom of the stack (higher addresses, higher positive sp offsets)
    // list_sentinel->next: head
    // list_sentinel->prev: tail
    LtaNodeId list_sentinel;
    u32 list_count;
    u64 next_debug_gen_id;

    LocalTempAllocator(yy::Allocator heap_)
        : heap(heap_), node_free_list{}, allocated_nodes{}, list_sentinel{u32(0)}, list_count{0}, next_debug_gen_id{1} {
        allocated_nodes.emplace_back(heap, yy::uninitialized);
        yy_assert(this->list_sentinel.x == 0);
        yy_assert(this->allocated_nodes.size() == 1);
        auto const sentinel_ptr = &this->allocated_nodes[list_sentinel.x];
        sentinel_ptr->prev = sentinel_ptr->next = list_sentinel;
    }
    ~LocalTempAllocator()
    {
        this->node_free_list.destroy_free(this->heap);
        this->allocated_nodes.destroy_free(this->heap);
    }
    LocalTempAllocator(const LocalTempAllocator&) = delete;
    LocalTempAllocator& operator=(const LocalTempAllocator&) = delete;
    LocalTempAllocator(LocalTempAllocator&&) = default;
    LocalTempAllocator& operator=(LocalTempAllocator&&) = default;
};

NODISCARD static LtaNode* lta_nodePtr(LocalTempAllocator* const lta, LtaNodeId const node)
{
    yy_assert(node.x < lta->allocated_nodes.size());
    return &lta->allocated_nodes[node.x];
}



NODISCARD static LtaNodeId lta_head(LocalTempAllocator const* const lta)
{
    return lta->allocated_nodes[lta->list_sentinel.x].next;
}
NODISCARD static LtaNodeId lta_tail(LocalTempAllocator const* const lta)
{
    return lta->allocated_nodes[lta->list_sentinel.x].prev;
}

NODISCARD static u64 lta_peak_size(LocalTempAllocator *const lta)
{
    if (lta->list_count == 0)
        return 0;
    yy_assert(lta_tail(lta).x != lta->list_sentinel.x);
    LtaNode const tail = *lta_nodePtr(lta, lta_tail(lta));
    return tail.sp_offset + tail.size_aligned;
}

NODISCARD static bool lta_node_is_sentinel(LocalTempAllocator const *const lta, LtaNodeId const node)
{
    return node.x == lta->list_sentinel.x;
}

static void lta_node_insert_after(LocalTempAllocator *const lta, LtaNodeId const after_this, LtaNodeId const new_node)
{
    lta_nodePtr(lta, new_node)->next = lta_nodePtr(lta, after_this)->next;
    lta_nodePtr(lta, new_node)->prev = after_this;
    lta_nodePtr(lta, lta_nodePtr(lta, new_node)->next)->prev = new_node;
    lta_nodePtr(lta, after_this)->next = new_node;
    lta->list_count += 1;
    if (!lta_node_is_sentinel(lta, after_this)) {
        LtaNode const after_this_data = *lta_nodePtr(lta, after_this);
        LtaNode const new_node_data = *lta_nodePtr(lta, new_node);
        yy_assert(after_this_data.sp_offset + after_this_data.size_aligned == new_node_data.sp_offset);
        if (!lta_node_is_sentinel(lta, new_node_data.next)) {
            LtaNode const new_next_data = *lta_nodePtr(lta, new_node_data.next);
            yy_assert(new_node_data.sp_offset + new_node_data.size_aligned == new_next_data.sp_offset);
        }
    }
}

NODISCARD static LtaNodeId lta_create_node(LocalTempAllocator *const lta)
{
    if (lta->node_free_list.size() == 0) {
        lta->allocated_nodes.emplace_back(lta->heap, yy::uninitialized);
        LtaNodeId const new_free_id{lta->allocated_nodes.size() - 1};
        lta->node_free_list.push_back(lta->heap, new_free_id);
    }

    yy_assert(lta->node_free_list.size() > 0);
    auto result = lta->node_free_list.pop_back();
    return result;
}

static void lta_free(LocalTempAllocator *const lta, LtaNodeId const node)
{
    yy_assert(!lta_node_is_sentinel(lta, node));
    lta_nodePtr(lta, node)->is_free = true;

    LtaNode const node_data = *lta_nodePtr(lta, node);

    LtaNodeId free_node = node;
    LtaNode free_node_data = *lta_nodePtr(lta, free_node);
    if (!lta_node_is_sentinel(lta, node_data.prev)) {
        LtaNode const prev_data = *lta_nodePtr(lta, node_data.prev);
        yy_assert(prev_data.sp_offset <= node_data.sp_offset);
        yy_assert(prev_data.sp_offset + prev_data.size_aligned == node_data.sp_offset);
        if (prev_data.is_free) {
            lta_nodePtr(lta, node_data.prev)->size_aligned += node_data.size_aligned;
            lta_nodePtr(lta, node_data.prev)->next = node_data.next;
            lta_nodePtr(lta, node_data.next)->prev = node_data.prev;
            lta->node_free_list.push_back(lta->heap, node);
            lta->list_count -= 1;
            free_node = node_data.prev;
            free_node_data = *lta_nodePtr(lta, free_node);
        }
    }

    if (!lta_node_is_sentinel(lta, free_node_data.next)) {
        LtaNode const next_data = *lta_nodePtr(lta, free_node_data.next);
        yy_assert(free_node_data.sp_offset <= next_data.sp_offset);
        yy_assert(free_node_data.sp_offset + free_node_data.size_aligned == next_data.sp_offset);
        if (next_data.is_free) {
            lta_nodePtr(lta, next_data.prev)->size_aligned += next_data.size_aligned;
            lta_nodePtr(lta, next_data.prev)->next = next_data.next;
            lta_nodePtr(lta, next_data.next)->prev = next_data.prev;
            lta->node_free_list.push_back(lta->heap, free_node_data.next);
            lta->list_count -= 1;
        }
    }
}

NODISCARD static LtaNodeId lta_alloc(LocalTempAllocator *const lta, u64 const size)
{
    // TODO typeIdMemoryAlignOf
    u64 const alignment = 8;
    u64 const size_aligned = yy_align_forward_power_of_two(alignment, size);
    // Find a free node
    for (LtaNodeId node = lta_head(lta); !lta_node_is_sentinel(lta, node); node = lta_nodePtr(lta, node)->next) {
        LtaNode const node_data = *lta_nodePtr(lta, node);
        yy_assert(yy_is_aligned_power_of_two(alignment, node_data.size_aligned));
        if (!node_data.is_free)
            continue;
        if (node_data.size_aligned < size_aligned)
            continue;
        yy_assert(yy_is_aligned_power_of_two(alignment, node_data.sp_offset));
        u64 const remaining_size = node_data.size_aligned - size_aligned;
        if (remaining_size > 0) {
            yy_assert(remaining_size % alignment == 0);
            // Split into `head <- FFFF -> tail` into `UFFF` where F = free, U = used
            lta_nodePtr(lta, node)->is_free = false;
            lta_nodePtr(lta, node)->size_aligned = size_aligned;;
            LtaNodeId const new_free_node = lta_create_node(lta);
            *lta_nodePtr(lta, new_free_node) = LtaNode {
                .debug_gen_id = lta->next_debug_gen_id++,
                .sp_offset = node_data.sp_offset + size_aligned,
                .size_aligned = remaining_size,
                .is_free = true,
                .prev{}, // init below on lta_node_insert_after
                .next{}, // init below on lta_node_insert_after
            };
            lta_node_insert_after(lta, node, new_free_node);
            lta_nodePtr(lta, node)->debug_gen_id = lta->next_debug_gen_id++;
            return node;
        } else {
            yy_assert(node_data.size_aligned == size_aligned);
            lta_nodePtr(lta, node)->is_free = false;
            lta_nodePtr(lta, node)->debug_gen_id = lta->next_debug_gen_id++;
            return node;
        }
    }
    u64 const new_sp_offset = lta_peak_size(lta);
    yy_assert(yy_is_aligned_power_of_two(alignment, new_sp_offset));
    LtaNodeId const new_node = lta_create_node(lta);
    *lta_nodePtr(lta, new_node) = LtaNode {
        .debug_gen_id = lta->next_debug_gen_id++,
        .sp_offset = new_sp_offset,
        .size_aligned = size_aligned,
        .is_free = false,
        .prev{}, // init below on lta_node_insert_after
        .next{}, // init below on lta_node_insert_after
    };
    lta_node_insert_after(lta, lta_tail(lta), new_node);
    return new_node;
}

NODISCARD static LtaNodeId lta_alloc_type(Sema *const sema, LocalTempAllocator *const lta, SemaTypeId const type)
{
    // TODO: typeIdMemoryAlignOf
    u64 const size = typeIdMemorySizeOf(sema, type);
    return lta_alloc(lta, size);
}

UNUSED static void lta_debug_dump(LocalTempAllocator *const lta)
{
    fprintf(stderr, "{\n");
    fprintf(stderr, " lta.list_count = %u\n", lta->list_count);
    for (LtaNodeId node = lta_head(lta); !lta_node_is_sentinel(lta, node); node = lta_nodePtr(lta, node)->next) {
        LtaNode const node_data = *lta_nodePtr(lta, node);
        fprintf(stderr, "  node id = %u\n", node.x);
        fprintf(stderr, "   debug_gen_id = %" PRIu64 "\n", node_data.debug_gen_id);
        fprintf(stderr, "   sp_offset = %" PRIu64 "\n", node_data.sp_offset);
        fprintf(stderr, "   size_aligned = %" PRIu64 "\n", node_data.size_aligned);
        fprintf(stderr, "   is_free = %s\n", node_data.is_free ? "true" : "false");
        fprintf(stderr, "   prev = %u\n", node_data.prev.x);
        fprintf(stderr, "   next = %u\n", node_data.next.x);
    }
    fprintf(stderr, "}\n");
}


using LocalVarStackMap = yy::NewerHashMap<InstIndex, u64>;


// Register definitions

static constexpr u32 MAX_REGISTER_ALLOC = 4;
// #define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) ...
#define TMP_DEFINE_REGISTER_ALL \
    TMP_DEFINE_REGISTER(r12, r12d, r12w, r12b) \
    TMP_DEFINE_REGISTER(r13, r13d, r13w, r13b) \
    TMP_DEFINE_REGISTER(r14, r14d, r14w, r14b) \
    TMP_DEFINE_REGISTER(r15, r15d, r15w, r15b) \
    TMP_DEFINE_REGISTER(rbx, ebx, bx, bl) \
    TMP_DEFINE_REGISTER(rax, eax, ax, al) \
    TMP_DEFINE_REGISTER(rdi, edi, di, dil) \
    TMP_DEFINE_REGISTER(rsi, esi, si, sil) \
    TMP_DEFINE_REGISTER(rdx, edx, dx, dl) \
    TMP_DEFINE_REGISTER(rcx, ecx, cx, cl) \
    TMP_DEFINE_REGISTER(r8, r8d, r8w, r8b) \
    TMP_DEFINE_REGISTER(r9, r9d, r9w, r9b) \
    TMP_DEFINE_REGISTER(r10, r10d, r10w, r10b) \
    TMP_DEFINE_REGISTER(r11, r11d, r11w, r11b) \
YY_EMPTY_MACRO

#define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) YY_META_ENUM__MEMBER(reg64)
#define YY_META_ENUM__NAME Register
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS TMP_DEFINE_REGISTER_ALL
#include "yy/meta/enum.hpp"
#undef TMP_DEFINE_REGISTER

#define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) #reg64,
static char const *const register64_names[MetaEnum_count_int(Register)] = { TMP_DEFINE_REGISTER_ALL };
#undef TMP_DEFINE_REGISTER
#define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) #reg32,
static char const *const register32_names[MetaEnum_count_int(Register)] = { TMP_DEFINE_REGISTER_ALL };
#undef TMP_DEFINE_REGISTER
#define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) #reg16,
static char const *const register16_names[MetaEnum_count_int(Register)] = { TMP_DEFINE_REGISTER_ALL };
#undef TMP_DEFINE_REGISTER
#define TMP_DEFINE_REGISTER(reg64, reg32, reg16, reg8) #reg8,
static char const *const register8_names[MetaEnum_count_int(Register)] = { TMP_DEFINE_REGISTER_ALL };
#undef TMP_DEFINE_REGISTER

#undef TMP_DEFINE_REGISTER_ALL
#undef TMP_DEFINE_REGISTER

NODISCARD static char const *register_name(Register const reg, u32 const bit_size)
{
    switch (bit_size) {
        case 0: yy_unreachable();
        case 8: return register8_names[usize(reg)];
        case 16: return register16_names[usize(reg)];
        case 32: return register32_names[usize(reg)];
        case 64: return register64_names[usize(reg)];
        default: yy_unreachable();
    }
    yy_unreachable();
}

Register constexpr c_callconv_arg_regs[] = {
    Register__rdi,
    Register__rsi,
    Register__rdx,
    Register__rcx,
    Register__r8,
    Register__r9,
};
static u64 const c_callconv_stack_align = 16;



// McValue: Machine code Value
#define YY_META_TAGGED_UNION__NAME McValue
#define YY_META_ENUM__INT u8
#define YY_META_TAGGED_UNION__ALL_MEMBERS                                              \
    YY_META_TAGGED_UNION__MEMBER(i64, imm_i64)                                         \
    YY_META_TAGGED_UNION__MEMBER(Register, reg)                                        \
    YY_META_TAGGED_UNION__MEMBER(Void, zero_sized_value)                               \
    YY_META_TAGGED_UNION__MEMBER(Void, xundefined)                                     \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, fn_address)                                \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, extern_fn_address)                         \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, global_var_loc)                            \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, global_var_value)                          \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, extern_var_loc)                            \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, extern_var_value)                          \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, local_var_loc)                             \
    YY_META_TAGGED_UNION__MEMBER(InstIndex, local_var_value)                           \
    YY_META_TAGGED_UNION__MEMBER(LtaNodeId, lta_value)                                 \
    YY_META_TAGGED_UNION__MEMBER(LtaNodeId, lta_loc)                                   \
    YY_EMPTY_MACRO
#include "yy/meta/tagged_union.hpp"

#define YY_META_ENUM__NAME McvAsmKind
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS                  \
    YY_META_ENUM__MEMBER(none)                     \
    YY_META_ENUM__MEMBER(immediate)                \
    YY_META_ENUM__MEMBER(immediate_deref)          \
    YY_META_ENUM__MEMBER(xregister)                \
    YY_META_ENUM__MEMBER(register_offset)          \
    YY_META_ENUM__MEMBER(register_offset_deref)    \
    YY_EMPTY_MACRO
#include "yy/meta/enum.hpp"

NODISCARD static McvAsmKind mcvAsmKind(McValue_Tag const mcv_tag)
{
    switch (mcv_tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value: return McvAsmKind__none;
        case McValue_Tag__reg: return McvAsmKind__xregister;
        case McValue_Tag__fn_address: return McvAsmKind__immediate;
        case McValue_Tag__extern_fn_address: return McvAsmKind__immediate;
        // TODO: xundefined is not an immediate for structs.
        case McValue_Tag__xundefined: return McvAsmKind__immediate;
        case McValue_Tag__imm_i64: return McvAsmKind__immediate;
        case McValue_Tag__lta_loc: return McvAsmKind__register_offset;
        case McValue_Tag__lta_value: return McvAsmKind__register_offset_deref;
        case McValue_Tag__local_var_loc: return McvAsmKind__register_offset;
        case McValue_Tag__local_var_value: return McvAsmKind__register_offset_deref;
        case McValue_Tag__global_var_loc: return McvAsmKind__immediate;
        case McValue_Tag__global_var_value: return McvAsmKind__immediate_deref;
        case McValue_Tag__extern_var_loc: return McvAsmKind__immediate;
        case McValue_Tag__extern_var_value: return McvAsmKind__immediate_deref;
    }
    yy_unreachable();
}

/// mov .., X
NODISCARD static bool mcvAsmKindSupportsMovRhs(McvAsmKind const mcv_asm_kind)
{

    switch (mcv_asm_kind) {
        case MetaEnum_count(McvAsmKind): yy_unreachable();
        case McvAsmKind__none: yy_unreachable();
        case McvAsmKind__immediate: return true;
        case McvAsmKind__immediate_deref: return true;
        case McvAsmKind__xregister: return true;
        case McvAsmKind__register_offset: return false;
        case McvAsmKind__register_offset_deref: return true;
    }
    yy_unreachable();
}

/// call X
NODISCARD static bool mcvAsmKindSupportsCallOperand(McvAsmKind const mcv_asm_kind)
{
    // I may be wrong about these. Maybe MovRhs is enough.
    switch (mcv_asm_kind) {
        case MetaEnum_count(McvAsmKind): yy_unreachable();
        case McvAsmKind__none: yy_unreachable();
        case McvAsmKind__immediate: return true;
        case McvAsmKind__immediate_deref: return false;
        case McvAsmKind__xregister: return true;
        case McvAsmKind__register_offset: return false;
        case McvAsmKind__register_offset_deref: return false;
    }
    yy_unreachable();
}

/// mov X, ...
NODISCARD static bool mcvAsmKindSupportsMovLhs(McvAsmKind const mcv_asm_kind)
{

    switch (mcv_asm_kind) {
        case MetaEnum_count(McvAsmKind): yy_unreachable();
        case McvAsmKind__none: yy_unreachable();
        case McvAsmKind__immediate: return false;
        case McvAsmKind__immediate_deref: return true;
        case McvAsmKind__xregister: return true;
        case McvAsmKind__register_offset: return false;
        case McvAsmKind__register_offset_deref: return true;
    }
    yy_unreachable();
}

/// lea ..., [X]
NODISCARD static bool mcvAsmKindSupportsLeaRhs(McvAsmKind const mcv_asm_kind)
{
    switch (mcv_asm_kind) {
        case MetaEnum_count(McvAsmKind): yy_unreachable();
        case McvAsmKind__none: yy_unreachable();
        case McvAsmKind__immediate: return true;
        case McvAsmKind__immediate_deref: return false;
        case McvAsmKind__xregister: return true;
        case McvAsmKind__register_offset: return true;
        case McvAsmKind__register_offset_deref: return false;
    }
    yy_unreachable();
}

/// mov [X], ...
NODISCARD static bool mcvAsmKindSupportsMovDerefLhs(McvAsmKind const mcv_asm_kind)
{
    return mcvAsmKindSupportsLeaRhs(mcv_asm_kind);
}

static auto getUndefinedLiteral(Yy_CodegenX64* const codegen, SemaTypeId const type) -> char const* {
    auto bit_size = intCastFromUsizeToU32(typeIdMemorySizeOf(codegen->sema, type)*8);
    char const* result = yy::uninitialized;
    switch (bit_size) {
        case 8: result = "0xaa"; break;
        case 16: result = "0xaaaa"; break;
        case 32: result = "0xaaaaaaaa"; break;
        case 64: result = "0xaaaaaaaaaaaaaaaa"; break;
        default: todo();
    }
    return result;
}


using McvMap = yy::NewerHashMap<InstIndex, McValue>;


struct RegAlloc {
    yy::meta_enum_array<Register, u32> registers;
    static_assert(MAX_REGISTER_ALLOC <= MetaEnum_count_int(Register));
    RegAlloc() { registers.fill(0); }
    auto deinit() -> void {
        for (auto& reg: registers) {
            yy_assert(reg == 0);
        }
    }
};

NODISCARD static Register regalloc_alloc_any(RegAlloc *const ra)
{
    for (u32 i = 0; i < MAX_REGISTER_ALLOC; i++) {
        Register e = Register(i);
        if (ra->registers[e] == 0) {
            ra->registers[e] += 1;
            return e;
        }
    }
    yy_panic_fmt("Out of registers in function `%s`", __func__);
}

NODISCARD static Register regalloc_clone(RegAlloc *const ra, Register const reg)
{
    yy_assert(ra->registers[reg] > 0);
    ra->registers[reg] += 1;
    return reg;
}

static void regalloc_free(RegAlloc *const ra, Register const reg)
{
    yy_assert(reg != MetaEnum_count(Register));
    yy_assert(ra->registers[reg] > 0);
    ra->registers[reg] -= 1;
}

#define Function__struct_field_count 8
struct Function : yy::MoveOnly {
    InstIndex fn_decl_inst;
    u32 registers_pushed_on_top_locals{0};

    LocalTempAllocator lta;
    McvMap mcv_map;
    LocalVarStackMap local_var_stack_map;
    u64 local_vars_frame_size{0};
    u64 local_vars_frame_align{c_callconv_stack_align};

    RegAlloc reg_alloc{};

    Function(Yy_CodegenX64* const codegen, InstIndex const fn_decl_inst)
        : fn_decl_inst(fn_decl_inst), lta{codegen->heap}, mcv_map(codegen->heap), local_var_stack_map(codegen->heap) {
        yy_assert(codegen->sema->gi->insts[fn_decl_inst].tag == Yy_Ir_Inst_Tag__fn_decl);
        static_assert(Function__struct_field_count == 8, "");
    }

    auto deinit() -> void {
        yy_assert(
            this->lta.list_count == 0 ||
            (this->lta.list_count == 1 && lta_nodePtr(&this->lta, lta_head(&this->lta))->is_free)
        );
        yy_assert(this->registers_pushed_on_top_locals == 0);

        static_assert(Function__struct_field_count == 8, "");
        { auto _ = std::move(this->lta); }
        { auto _ = std::move(this->local_var_stack_map); }
        { auto _ = std::move(this->mcv_map); }
        this->reg_alloc.deinit();
    }
};

static u64 allocPermanentStackAlignSize(Function *const fn, u64 const mem_align, u64 const mem_size)
{
    yy_assert(mem_align > 0 && yy_is_power_of_two(mem_align));
    // TODO: Bump stack alignment
    yy_assert(mem_align <= fn->local_vars_frame_align);
    fn->local_vars_frame_align = yy_max(fn->local_vars_frame_align, mem_align);
    fn->local_vars_frame_size += mem_size;
    fn->local_vars_frame_size = yy_align_forward_power_of_two(mem_align, fn->local_vars_frame_size);;
    u64 const this_frame_offset = fn->local_vars_frame_size;
    return this_frame_offset;
}

static void writeInternalYuceyufEscapedLabel(ArrayList_Of_u8 *const writer, Sv const name, InstIndex const id)
{
    if (name.len == 0)
        yy_unreachable();
    for (usize i = 0; i < name.len; i += 1) {
        u8 const ascii = char_to_u8(name.ptr[i]);
        u8 const ascii32 = ascii | 32;
        if ((ascii32 >= 'a' && ascii32 <= 'z') || (ascii >= '0' && ascii <= '9') || ascii == '_') {
            writer->push_back(ascii);
        } else {
            static constexpr Sv hex_table = "0123456789abcdef";
            char const hex_low = hex_table[(ascii >> 0) & 0xf];
            char const hex_high = hex_table[(ascii >> 4) & 0xf];
            auto buf = std::array{ '.', hex_high, hex_low };
            array_list_u8_write_sv(writer, buf);
        }
    }
    // The name must be unique. I could have a function with the same name in multiple modules.
    array_list_printf(writer, "$%u", id.x);
}

static void writeExternSymbolLabel(ArrayList_Of_u8 *const writer, Sv const name)
{
    // TODO: This is wrong: I shouldn't escape the label.
    if (name.len == 0)
        yy_unreachable();
    for (usize i = 0; i < name.len; i += 1) {
        u8 const ascii = char_to_u8(name.ptr[i]);
        u8 const ascii32 = ascii | 32;
        bool const is_az = (ascii32 >= 'a' && ascii32 <= 'z');
        bool const is_09 = (ascii >= '0' && ascii <= '9');
        bool const is_not_7bit = ascii > 0x7f;
        if (!(is_az || is_09 || ascii == '_' || (ascii == '.' && i != 0) || is_not_7bit)) {
            yy_panic_fmt("TODO Escape external symbol: \"" Sv_Fmt "\"", Sv_Arg(name));
        }
    }
    array_list_u8_write_sv(writer, name);
}


static void writeLabelFnDecl(Yy_CodegenX64 *const codegen, InstIndex const fn_decl_idx)
{
    if (false) {
        array_list_printf(&codegen->asm_text, "inst_%u", fn_decl_idx.x);
    } else {
        // Write the actual function name.
        GenIr *const gi = codegen->sema->gi;
        Inst const fn_decl = gi->insts[fn_decl_idx];
        yy_assert(fn_decl.tag == Yy_Ir_Inst_Tag__fn_decl);
        IdentifierId const name_z = genir_extraPl(gi, fn_decl_idx, fn_decl)->name_z;
        Sv const fn_name_sv = genir_identifier_sv(gi, name_z);
        // TODO: Export functions manually, check that there are no conflicts for exported
        // functions. TODO Remove this hack.
        bool const is_export = sv_eql(fn_name_sv, sv_lit("main"));
        if (is_export) {
            writeExternSymbolLabel(&codegen->asm_text, fn_name_sv);
        } else {
            writeInternalYuceyufEscapedLabel(&codegen->asm_text, fn_name_sv, fn_decl_idx);
        }
    }
}

static void writeLabelExternFnDecl(Yy_CodegenX64 *const codegen, InstIndex const decl_inst_idx)
{
    // Write the actual function name.
    GenIr *const gi = codegen->sema->gi;
    Inst const decl_inst = gi->insts[decl_inst_idx];
    yy_assert(decl_inst.tag == Yy_Ir_Inst_Tag__extern_fn_decl);
    IdentifierId const name_z = genir_extraPl(gi, decl_inst_idx, extern_fn_decl)->name_z;
    Sv const decl_name_sv = genir_identifier_sv(gi, name_z);
    // TODO: writeInternalYuceyufEscapedLabel is wrong.
    writeExternSymbolLabel(&codegen->asm_text, decl_name_sv);
}

static void writeLabelGlobalVarDecl(Yy_CodegenX64 *const codegen, InstIndex const decl_inst_idx)
{
    if (false) {
        array_list_printf(&codegen->asm_text, "inst_%u", decl_inst_idx.x);
    } else {
        // Write the actual function name.
        GenIr *const gi = codegen->sema->gi;
        Inst const decl_inst = gi->insts[decl_inst_idx];
        yy_assert(decl_inst.tag == Yy_Ir_Inst_Tag__global_var_decl);
        auto const name_z = genir_extraPl(gi, decl_inst_idx, global_var_decl)->name_z;
        Sv const decl_name_sv = genir_identifier_sv(gi, name_z);
        // TODO Manually export variables.
        writeInternalYuceyufEscapedLabel(&codegen->asm_text, decl_name_sv, decl_inst_idx);
    }
}

static void writeLabelExternVarDecl(Yy_CodegenX64 *const codegen, InstIndex const decl_inst_idx)
{
    // Write the actual function name.
    GenIr *const gi = codegen->sema->gi;
    Inst const decl_inst = gi->insts[decl_inst_idx];
    yy_assert(decl_inst.tag == Yy_Ir_Inst_Tag__extern_var_decl);
    auto const name_z = genir_extraPl(gi, decl_inst_idx, extern_var_decl)->name_z;
    Sv const decl_name_sv = genir_identifier_sv(gi, name_z);
    writeExternSymbolLabel(&codegen->asm_text, decl_name_sv);
}

static void typedStoreValueFromRegister(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    Register const dest_addr_reg, Register const src_reg, SemaTypeId const type)
{
    (void) parent_fn;
    Sema *const sema = codegen->sema;
    u64 const type_size = typeIdMemorySizeOf(sema, type);
    SemaType_Tag const type_tag = decodeTypeTag(sema, type);
    // Store src into dest.
    switch (type_tag) {
        case MetaEnum_count(SemaType_Tag):
            yy_unreachable();
        case SemaType_Tag__xnoreturn:
            yy_unreachable();
        case SemaType_Tag__type:
            ice();
        case SemaType_Tag__xundefined: {
            todo();
        } break;
        case SemaType_Tag__xstruct: {
            yy_panic_fmt("structs can't be stored in registers");
        } break;
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__function:
        case SemaType_Tag__pointer: {
            switch (type_size) {
                case 0: {
                    // do nothing
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; store void (do nothing)\n");
                } break;
                case 1: {
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; store byte\n");
                    array_list_printf(&codegen->asm_text, "mov BYTE [%s], %s\n", register_name(dest_addr_reg, 64), register_name(src_reg, 8));
                } break;
                case 8: {
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; store qword\n");
                    array_list_printf(&codegen->asm_text, "mov QWORD [%s], %s\n", register_name(dest_addr_reg, 64), register_name(src_reg, 64));
                } break;
                default: {
                    todo();
                } break;
            }
        } break;
    }
}
static void typedLoadValueIntoRegister(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    Register const dest_reg, Register const src_addr_reg, SemaTypeId const type)
{
    (void) parent_fn;
    Sema *const sema = codegen->sema;
    u64 const type_size = typeIdMemorySizeOf(sema, type);
    SemaType_Tag const type_tag = decodeTypeTag(sema, type);
    // Load src into dest.
    switch (type_tag) {
        case MetaEnum_count(SemaType_Tag):
            yy_unreachable();
        case SemaType_Tag__xnoreturn:
            yy_unreachable();
        case SemaType_Tag__type:
            ice();
        case SemaType_Tag__xundefined: {
            todo();
        } break;
        case SemaType_Tag__xstruct: {
            yy_panic_fmt("structs can't be stored in registers");
        } break;
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__function:
        case SemaType_Tag__pointer: {
            switch (type_size) {
                case 0: {
                    // do nothing
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; load void (do nothing)\n");
                } break;
                case 1: {
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; load byte\n");
                    array_list_printf(&codegen->asm_text, "xor %s, %s\n", register_name(dest_reg, 64), register_name(dest_reg, 64));
                    array_list_printf(&codegen->asm_text, "mov %s, BYTE [%s]\n", register_name(dest_reg, 8), register_name(src_addr_reg, 64));
                } break;
                case 8: {
                    if (asm_comments)
                        array_list_printf(&codegen->asm_text, "; load qword\n");
                    array_list_printf(&codegen->asm_text, "mov %s, QWORD [%s]\n", register_name(dest_reg, 64), register_name(src_addr_reg, 64));
                } break;
                default: {
                    todo();
                } break;
            }
        } break;
    }
}

static void saveRegisters(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, Register const *const regs_ptr, usize const regs_len)
{
    for (usize i = 0; i < regs_len; i += 1) {
        array_list_printf(&codegen->asm_text, "push %s\n", register_name(regs_ptr[i], 64));
    }
    parent_fn->registers_pushed_on_top_locals += intCastFromUsizeToU32(regs_len);
}

static void restoreRegisters(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, Register const *const regs_ptr, usize const regs_len)
{
    for (usize i = regs_len; i > 0;) {
        i -= 1;
        array_list_printf(&codegen->asm_text, "pop %s\n", register_name(regs_ptr[i], 64));
    }
    parent_fn->registers_pushed_on_top_locals -= intCastFromUsizeToU32(regs_len);
}

NODISCARD static u32 savedRegistersStackOffset(Yy_CodegenX64 *const codegen, Function *const fn)
{
    (void) codegen;
    u32 const rax_byte_size = 8;
    return rax_byte_size * fn->registers_pushed_on_top_locals;
}

static void referenceInstMcvWithGet(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const local, usize const local_mcv_map_ins_idx, InstIndex const referenced_by)
{
    auto const sema = codegen->sema;
    auto& local_sii = sema->inst_info_full(local);
    bool const is_never_used = local_sii.build_last_ref.is_none();
    yy_assert(!is_never_used);
    McValue const mcv = parent_fn->mcv_map.get_by_index(local_mcv_map_ins_idx).value;

    GenIr *const gi = codegen->sema->gi;
    InstIndex const parent_fn_body_idx = genir_getFnDeclBodyInst(gi, parent_fn->fn_decl_inst);
    yy_assert(gi->insts[parent_fn_body_idx].tag == Yy_Ir_Inst_Tag__block);
    yy_assert(local > parent_fn->fn_decl_inst && local < parent_fn_body_idx + genir_extraPl(gi, parent_fn_body_idx, block)->end_offset);

    auto const last_ref = local_sii.build_last_ref;
    yy_assert(last_ref.is_none() || (local <= referenced_by && referenced_by <= last_ref.unwrap()));
    if (last_ref.is_none() || last_ref == referenced_by) {
        parent_fn->mcv_map.swap_remove_index(local_mcv_map_ins_idx);
        if (mcv.tag == McValue_Tag__lta_value) {
            LtaNodeId const lta_node_id = *MetaTu_get(McValue, &mcv, lta_value);
            lta_free(&parent_fn->lta, lta_node_id);
        }
    }
    yy_assert(mcv.tag != McValue_Tag__lta_loc);
}
static void referenceInstMcv(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const local, InstIndex const referenced_by)
{
    bool const is_never_used = codegen->sema->inst_info_full(local).build_last_ref.is_none();
    if (is_never_used) {
        yy_assert(parent_fn->mcv_map.get({}, local).is_none());
        return;
    }
    auto const get = parent_fn->mcv_map.get({}, local).unwrap();
    referenceInstMcvWithGet(codegen, parent_fn, local, get.insertion_idx, referenced_by);
}

static void writeLtaNodeLoc(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    LtaNodeId const lta_node)
{
    u32 const current_saved_registers_offset = savedRegistersStackOffset(codegen, parent_fn);
    LtaNode const lta_node_data = *lta_nodePtr(&parent_fn->lta, lta_node);
    yy_assert(!lta_node_data.is_free);
    u64 const rsp_offset = current_saved_registers_offset + lta_node_data.sp_offset;
    array_list_printf(&codegen->asm_text, "rsp + %" PRIu64, rsp_offset);
}


NODISCARD static Register ltaNodeLocIntoNewRegister(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    LtaNodeId const lta_node)
{
    Register const addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const addr_reg_str = register_name(addr_reg, 64);
    array_list_printf(&codegen->asm_text, "lea %s, [", addr_reg_str);
    writeLtaNodeLoc(codegen, parent_fn, lta_node);
    array_list_printf(&codegen->asm_text, "]\n");
    return addr_reg;
}


static void writeAsmLocalVarOrParamAddress(Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const local_decl)
{
    auto const get = parent_fn->local_var_stack_map.get({}, local_decl).unwrap();
    u64 const frame_offset = get.value;
    array_list_printf(&codegen->asm_text, "rbp - %" PRIu64 "", frame_offset);
}

static void getLocalVarOrParamAddress(Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const local_decl, Register const addr_reg)
{
    // Local variables / parameters are located by an offset from the frame pointer (rbp).
    array_list_printf(&codegen->asm_text, "lea %s, [", register_name(addr_reg, 64));
    writeAsmLocalVarOrParamAddress(codegen, parent_fn, local_decl);
    array_list_printf(&codegen->asm_text, "]\n");
}

static void loadLocalVarOrParamValue(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    Register const dest_reg, InstIndex const local_decl,
    Register const tmp_addr_reg)
{
    SemaTypeId const local_decl_type = codegen->sema->inst_info_full(local_decl)
                                           .specific.as_decl_like()
                                           .unwrap()
                                           .resolved_decl_type.unwrap();
    if (asm_comments) {
        array_list_printf(&codegen->asm_text,
                        "; typed load of var/param " IOF_Fmt " into register %s with tmp addr register %s\n",
                        IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, local_decl)),
                        register_name(dest_reg, 64), register_name(tmp_addr_reg, 64));
    }
    if (!typeIsZeroSized(codegen->sema, local_decl_type)) {
        getLocalVarOrParamAddress(codegen, parent_fn, local_decl, tmp_addr_reg);
        typedLoadValueIntoRegister(codegen, parent_fn, dest_reg, tmp_addr_reg, local_decl_type);
    } else {
        if (asm_comments) {
            array_list_printf(&codegen->asm_text, ";  zero sized load of var/param\n");
        }
    }
}
static void storeLocalVarOrParamValue(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const local_decl, Register const src_reg,
    Register const tmp_addr_reg)
{
    SemaTypeId const local_decl_type = codegen->sema->inst_info_full(local_decl)
                                           .specific.as_decl_like()
                                           .unwrap()
                                           .resolved_decl_type.unwrap();
    if (asm_comments) {
        array_list_printf(&codegen->asm_text,
                        "; typed store of var/param " IOF_Fmt " from register %s with tmp addr register %s\n",
                        IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, local_decl)),
                        register_name(src_reg, 64), register_name(tmp_addr_reg, 64));
    }
    if (!typeIsZeroSized(codegen->sema, local_decl_type)) {
        getLocalVarOrParamAddress(codegen, parent_fn, local_decl, tmp_addr_reg);
        typedStoreValueFromRegister(codegen, parent_fn, tmp_addr_reg, src_reg, local_decl_type);
    } else {
        if (asm_comments) {
            array_list_printf(&codegen->asm_text, ";  zero sized store of var/param\n");
        }
    }
}

static void mcvAsmWrite(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const mcv, SemaTypeId const type)
{
    switch (mcv.tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value:
            yy_unreachable();
        case McValue_Tag__imm_i64: {
            array_list_printf(&codegen->asm_text, "%" PRIi64, *MetaTu_get(McValue, &mcv, imm_i64));
        } break;
        case McValue_Tag__xundefined: {
            auto const value_str = getUndefinedLiteral(codegen, type);
            array_list_printf(&codegen->asm_text, "%s", value_str);
        } break;
        case McValue_Tag__reg: {
            array_list_printf(&codegen->asm_text, "%s", register_name(*MetaTu_get(McValue, &mcv, reg), intCastFromUsizeToU32(typeIdMemorySizeOf(codegen->sema, type) * 8)));
        } break;
        case McValue_Tag__fn_address: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, fn_address);
            writeLabelFnDecl(codegen, decl_inst);
        } break;
        case McValue_Tag__extern_fn_address: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, extern_fn_address);
            writeLabelExternFnDecl(codegen, decl_inst);
        } break;
        case McValue_Tag__lta_loc:  {
            LtaNodeId const lta_node = *MetaTu_get(McValue, &mcv, lta_loc);
            writeLtaNodeLoc(codegen, parent_fn, lta_node);
        } break;
        case McValue_Tag__lta_value:  {
            // TODO: Use the type
            LtaNodeId const lta_node = *MetaTu_get(McValue, &mcv, lta_loc);
            array_list_printf(&codegen->asm_text, "[");
            writeLtaNodeLoc(codegen, parent_fn, lta_node);
            array_list_printf(&codegen->asm_text, "]");
        } break;
        case McValue_Tag__local_var_value: {
            // TODO: Use the type
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, local_var_loc);
            array_list_printf(&codegen->asm_text, "[");
            writeAsmLocalVarOrParamAddress(codegen, parent_fn, decl_inst);
            array_list_printf(&codegen->asm_text, "]");
        } break;
        case McValue_Tag__local_var_loc: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, local_var_loc);
            writeAsmLocalVarOrParamAddress(codegen, parent_fn, decl_inst);
        } break;
        case McValue_Tag__global_var_loc: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, global_var_loc);
            writeLabelGlobalVarDecl(codegen, decl_inst);
        } break;
        case McValue_Tag__global_var_value: {
            // TODO: Use the type
            array_list_printf(&codegen->asm_text, "[");
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, global_var_loc);
            writeLabelGlobalVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]");
        } break;
        case McValue_Tag__extern_var_loc: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, extern_var_loc);
            writeLabelExternVarDecl(codegen, decl_inst);
        } break;
        case McValue_Tag__extern_var_value: {
            // TODO: Use the type
            array_list_printf(&codegen->asm_text, "[");
            InstIndex const decl_inst = *MetaTu_get(McValue, &mcv, extern_var_loc);
            writeLabelExternVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]");
        } break;
    }
}

static McValue instMcv(Function *const parent_fn, InstIndex const inst)
{
    return parent_fn->mcv_map.get({}, inst).unwrap().value;
}

static void genMcvMoveFromStringRegisterIntoMcv(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const dest_mcv, Register const src_reg,
    SemaTypeId const type)
{
    // TODO: assert src_reg size == typeIdMemorySizeOf(type)
    switch (dest_mcv.tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value:
        case McValue_Tag__xundefined:
        case McValue_Tag__imm_i64:
        case McValue_Tag__fn_address:
        case McValue_Tag__extern_fn_address:
        case McValue_Tag__lta_loc:
        case McValue_Tag__local_var_loc:
        case McValue_Tag__extern_var_loc:
        case McValue_Tag__global_var_loc: {
            yy_panic_fmt("invalid genMcvMove destination: `" Sv_Fmt "`", Sv_Arg(MetaEnum_sv_short(McValue_Tag, dest_mcv.tag)));
        } break;
        case McValue_Tag__global_var_value: {
            // mov TYPE [foo], src_reg

            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const decl_inst = *MetaTu_get(McValue, &dest_mcv, global_var_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLabelGlobalVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");

            typedStoreValueFromRegister(codegen, parent_fn, tmp_addr_reg, src_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__extern_var_value: {
            // mov TYPE [foo], src_reg

            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const decl_inst = *MetaTu_get(McValue, &dest_mcv, extern_var_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLabelExternVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");

            typedStoreValueFromRegister(codegen, parent_fn, tmp_addr_reg, src_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__local_var_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const decl_inst = *MetaTu_get(McValue, &dest_mcv, local_var_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeAsmLocalVarOrParamAddress(codegen, parent_fn, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");

            typedStoreValueFromRegister(codegen, parent_fn, tmp_addr_reg, src_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__reg: {
            Register const dest_reg = *MetaTu_get(McValue, &dest_mcv, reg);
            auto bit_size = intCastFromUsizeToU32(typeIdMemorySizeOf(codegen->sema, type)*8);
            array_list_printf(&codegen->asm_text, "mov %s, %s\n", register_name(dest_reg, bit_size), register_name(src_reg, bit_size));
        } break;
        case McValue_Tag__lta_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            LtaNodeId const lta_node = *MetaTu_get(McValue, &dest_mcv, lta_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLtaNodeLoc(codegen, parent_fn, lta_node);
            array_list_printf(&codegen->asm_text, "]\n");

            typedStoreValueFromRegister(codegen, parent_fn, tmp_addr_reg, src_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
    }
}

static void genMcvMoveIntoStringRegister(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    Register const dest_reg, McValue const src_mcv,
    SemaTypeId const type)
{
    switch (src_mcv.tag) {
        case MetaEnum_count(McValue_Tag):
            yy_unreachable();
        case McValue_Tag__zero_sized_value:
            yy_unreachable();
        case McValue_Tag__global_var_loc: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, global_var_loc);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(dest_reg, 64));
            writeLabelGlobalVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");
        } break;
        case McValue_Tag__global_var_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, global_var_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLabelGlobalVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");

            typedLoadValueIntoRegister(codegen, parent_fn, dest_reg, tmp_addr_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__extern_var_loc: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, extern_var_loc);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(dest_reg, 64));
            writeLabelExternVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");
        } break;
        case McValue_Tag__extern_var_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, extern_var_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLabelExternVarDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");

            typedLoadValueIntoRegister(codegen, parent_fn, dest_reg, tmp_addr_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__fn_address: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, fn_address);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(dest_reg, 64));
            writeLabelFnDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");
        } break;
        case McValue_Tag__extern_fn_address: {
            InstIndex const decl_inst = *MetaTu_get(McValue, &src_mcv, fn_address);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(dest_reg, 64));
            writeLabelExternFnDecl(codegen, decl_inst);
            array_list_printf(&codegen->asm_text, "]\n");
        } break;
        case McValue_Tag__imm_i64: {
            i64 const imm = *MetaTu_get(McValue, &src_mcv, imm_i64);
            array_list_printf(&codegen->asm_text, "mov %s, %" PRIi64 "\n", register_name(dest_reg, 64), imm);
        } break;
        case McValue_Tag__lta_loc: {
            LtaNodeId const lta_node = *MetaTu_get(McValue, &src_mcv, lta_loc);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(dest_reg, 64));
            writeLtaNodeLoc(codegen, parent_fn, lta_node);
            array_list_printf(&codegen->asm_text, "]\n");
        } break;
        case McValue_Tag__lta_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            LtaNodeId const lta_node = *MetaTu_get(McValue, &src_mcv, lta_value);
            array_list_printf(&codegen->asm_text, "lea %s, [", register_name(tmp_addr_reg, 64));
            writeLtaNodeLoc(codegen, parent_fn, lta_node);
            array_list_printf(&codegen->asm_text, "]\n");

            typedLoadValueIntoRegister(codegen, parent_fn, dest_reg, tmp_addr_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__local_var_loc: {
            InstIndex const local_decl_inst = *MetaTu_get(McValue, &src_mcv, local_var_loc);
            getLocalVarOrParamAddress(codegen, parent_fn, local_decl_inst, dest_reg);
        } break;
        case McValue_Tag__local_var_value: {
            // TODO: Do this in a single instruction.
            Register const tmp_addr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

            InstIndex const local_decl_inst = *MetaTu_get(McValue, &src_mcv, local_var_value);
            getLocalVarOrParamAddress(codegen, parent_fn, local_decl_inst, tmp_addr_reg);

            typedLoadValueIntoRegister(codegen, parent_fn, dest_reg, tmp_addr_reg, type);

            regalloc_free(&parent_fn->reg_alloc, tmp_addr_reg);
        } break;
        case McValue_Tag__reg: {
            Register const reg = *MetaTu_get(McValue, &src_mcv, reg);
            auto bit_size = intCastFromUsizeToU32(typeIdMemorySizeOf(codegen->sema, type)*8);
            array_list_printf(&codegen->asm_text, "mov %s, %s\n", register_name(dest_reg, bit_size), register_name(reg, bit_size));
        } break;
        case McValue_Tag__xundefined: {
            auto bit_size = intCastFromUsizeToU32(typeIdMemorySizeOf(codegen->sema, type)*8);
            auto value_str = getUndefinedLiteral(codegen, type);
            array_list_printf(&codegen->asm_text, "mov %s, %s\n", register_name(dest_reg, bit_size), value_str);
        } break;
    }
}

static inline McValue mcvAsLoc(McValue const mcv)
{
    switch (mcv.tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value:
        case McValue_Tag__xundefined:
        case McValue_Tag__imm_i64:
        case McValue_Tag__reg:
        case McValue_Tag__fn_address:
        case McValue_Tag__extern_fn_address:
        case McValue_Tag__lta_loc:
        case McValue_Tag__local_var_loc:
        case McValue_Tag__extern_var_loc:
        case McValue_Tag__global_var_loc: {
            yy_panic_fmt("invalid mcv to `%s`", __func__);
        } break;
        case McValue_Tag__lta_value:
            return MetaTu_init(McValue, lta_loc, *MetaTu_get(McValue, &mcv, lta_value));
        case McValue_Tag__global_var_value:
            return MetaTu_init(McValue, global_var_loc, *MetaTu_get(McValue, &mcv, global_var_value));
        case McValue_Tag__extern_var_value:
            return MetaTu_init(McValue, extern_var_loc, *MetaTu_get(McValue, &mcv, extern_var_value));
        case McValue_Tag__local_var_value:
            return MetaTu_init(McValue, local_var_loc, *MetaTu_get(McValue, &mcv, local_var_value));
    }
    yy_unreachable();
}

static inline bool mcvIsInMemory(McValue_Tag const mcv_tag)
{
    switch (mcv_tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value:
        case McValue_Tag__imm_i64:
        case McValue_Tag__xundefined:
        case McValue_Tag__reg:
        case McValue_Tag__fn_address:
        case McValue_Tag__extern_fn_address:
        case McValue_Tag__lta_loc:
        case McValue_Tag__local_var_loc:
        case McValue_Tag__global_var_loc:
        case McValue_Tag__extern_var_loc:
            return false;
        case McValue_Tag__lta_value:
        case McValue_Tag__global_var_value:
        case McValue_Tag__extern_var_value:
        case McValue_Tag__local_var_value:
            return true;
    }
    yy_unreachable();
}

static void genMemoryCopy(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    // dest_ptr, src_ptr: *T
    McValue const dest_ptr, McValue const src_ptr,
    // ptr_child_type = T
    SemaTypeId const ptr_child_type)
{
    u64 const type_size = typeIdMemorySizeOf(codegen->sema, ptr_child_type);
    SemaTypeId const ptr_type = typePointerToAnyopaque(codegen->sema);

    // TODO: Don't always use a memcpy.
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "; genMemoryCopy size %" PRIu64 " {\n", type_size);
    }
    if (type_size != 0) {
        genMcvMoveIntoStringRegister(codegen, parent_fn, Register__rdi, dest_ptr, ptr_type);
        genMcvMoveIntoStringRegister(codegen, parent_fn, Register__rsi, src_ptr, ptr_type);
        array_list_printf(&codegen->asm_text, "mov rdx, %" PRIu64 "\n", (u64)(type_size));
        array_list_printf(&codegen->asm_text, "call _yuceyuf_memcpy\n");
    }
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "; }\n");
    }
}

static void genMemorySet(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const dest_ptr, u8 src_byte, u64 size)
{
    SemaTypeId const ptr_type = typePointerToAnyopaque(codegen->sema);

    if (size != 0) {
        genMcvMoveIntoStringRegister(codegen, parent_fn, Register__rdi, dest_ptr, ptr_type);
        array_list_printf(&codegen->asm_text, "mov rsi, %" PRIu64 "\n", (u64)(src_byte));
        array_list_printf(&codegen->asm_text, "mov rdx, %" PRIu64 "\n", (u64)(size));
        array_list_printf(&codegen->asm_text, "call _yuceyuf_memset\n");
    }
}

/// perform `mov dest, src`
static void genMcvMove(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const dest_mcv, McValue const src_mcv,
    SemaTypeId const type)
{
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "; genMcvMove {\n");
    }
    switch (dest_mcv.tag) {
        case MetaEnum_count(McValue_Tag): yy_unreachable();
        case McValue_Tag__zero_sized_value:
        case McValue_Tag__imm_i64:
        case McValue_Tag__xundefined:
        case McValue_Tag__fn_address:
        case McValue_Tag__extern_fn_address:
        case McValue_Tag__lta_loc:
        case McValue_Tag__local_var_loc:
        case McValue_Tag__extern_var_loc:
        case McValue_Tag__global_var_loc: {
            yy_panic_fmt("invalid genMcvMove destination: `" Sv_Fmt "`", Sv_Arg(MetaEnum_sv_short(McValue_Tag, dest_mcv.tag)));
        } break;
        case McValue_Tag__reg: {
            genMcvMoveIntoStringRegister(codegen, parent_fn, *MetaTu_get(McValue, &dest_mcv, reg), src_mcv, type);
        } break;
        case McValue_Tag__extern_var_value:
        case McValue_Tag__global_var_value:
        case McValue_Tag__local_var_value:
        case McValue_Tag__lta_value: {
            u64 const type_size = typeIdMemorySizeOf(codegen->sema, type);
            if (src_mcv.tag == McValue_Tag__reg) {
                genMcvMoveFromStringRegisterIntoMcv(codegen, parent_fn, dest_mcv, *MetaTu_get(McValue, &src_mcv, reg), type);
            } else if (src_mcv.tag == McValue_Tag__zero_sized_value) {
                // do nothing
            } else if (mcvIsInMemory(src_mcv.tag)) {
                genMemoryCopy(codegen, parent_fn, mcvAsLoc(dest_mcv), mcvAsLoc(src_mcv), type);
            } else if (src_mcv.tag == McValue_Tag__xundefined) {
                genMemorySet(codegen, parent_fn, mcvAsLoc(dest_mcv), 0xaa, type_size);
            } else {
                if (type_size != 0) {
                    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
                    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, src_mcv, type);
                    genMcvMoveFromStringRegisterIntoMcv(codegen, parent_fn, dest_mcv, tmp_reg, type);
                    regalloc_free(&parent_fn->reg_alloc, tmp_reg);
                }
            }
        } break;
    }
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; }\n");
}

/// perform `mov [dest], src`
static void genMcvMoveDeref(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const dest_mcv, McValue const src_mcv,
    SemaTypeId const src_type)
{
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  genMcvMoveDeref {\n");
    // TODO: Don't make a temporary lta and copy it directly, if possible.
    // src_mcv may be an immediate, which won't work for genMemoryCopy / mcvAsLoc
    if (mcvIsInMemory(src_mcv.tag)) {
        genMemoryCopy(codegen, parent_fn, dest_mcv, mcvAsLoc(src_mcv), src_type);
    } else {
        // TODO: Don't use a temporary lta if possible. Move the thing directly.
        LtaNodeId const tmp_lta_node = lta_alloc_type(codegen->sema, &parent_fn->lta, src_type);
        McValue const tmp_mcv_value = MetaTu_init(McValue, lta_value, tmp_lta_node);
        genMcvMove(codegen, parent_fn, tmp_mcv_value, src_mcv, src_type);
        genMemoryCopy(codegen, parent_fn, dest_mcv, mcvAsLoc(tmp_mcv_value), src_type);
        lta_free(&parent_fn->lta, tmp_lta_node);
    }
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  }\n");
}

// genMcvCloneLtaOrImmediate: if immediate, maintain the immediate. else, new lta
NODISCARD static McValue genMcvCloneLtaOrImmediate(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const src_mcv, SemaTypeId const type)
{
    switch (src_mcv.tag) {
        case MetaEnum_count(McValue_Tag):
            yy_unreachable();
        case McValue_Tag__zero_sized_value:
        case McValue_Tag__extern_var_loc:
        case McValue_Tag__global_var_loc:
        case McValue_Tag__local_var_loc:
        case McValue_Tag__fn_address:
        case McValue_Tag__extern_fn_address:
        case McValue_Tag__imm_i64:
        case McValue_Tag__xundefined:
            return src_mcv;
        case McValue_Tag__reg: {
            LtaNodeId const result_lta_node = lta_alloc_type(codegen->sema, &parent_fn->lta, type);
            McValue const result_mcv = MetaTu_init(McValue, lta_value, result_lta_node);
            genMcvMove(codegen, parent_fn, result_mcv, src_mcv, type);
            return result_mcv;
        } break;
        case McValue_Tag__lta_loc: {
            todo(); // Is this even reachable?
        } break;
        case McValue_Tag__lta_value:
        case McValue_Tag__global_var_value:
        case McValue_Tag__extern_var_value:
        case McValue_Tag__local_var_value: {
            LtaNodeId const new_dest_lta_node = lta_alloc_type(codegen->sema, &parent_fn->lta, type);
            McValue const result = MetaTu_init(McValue, lta_value, new_dest_lta_node);
            genMemoryCopy(codegen, parent_fn, mcvAsLoc(result), mcvAsLoc(src_mcv), type);
            return result;
        } break;
    }
    yy_unreachable();
}

NODISCARD static McValue genMcvCloneIntoRegister(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const mcv, SemaTypeId const type)
{
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  genMcvCloneIntoRegister {\n");
    Register const new_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, new_reg, mcv, type);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  }\n");
    return MetaTu_init(McValue, reg, new_reg);
}

static void storeInstMcv(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const inst, McValue src_mcv)
{
    Sema *const sema = codegen->sema;
    auto& inst_sii = sema->inst_info_full(inst);
    bool const is_never_used = inst_sii.build_last_ref.is_none();

    if (is_never_used) {
        yy_assert(parent_fn->mcv_map.get({}, inst).is_none());
        return;
    }
    SemaTypeId const inst_type = inst_sii.type3;

    parent_fn->mcv_map.put_no_clobber_emplace({}, inst, genMcvCloneLtaOrImmediate(codegen, parent_fn, src_mcv, inst_type));
    referenceInstMcv(codegen, parent_fn, inst, inst);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; storeInstMcv " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst)));
}

static void storeInstMcvNoClone(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const inst, McValue src_mcv)
{
    Sema *const sema = codegen->sema;
    auto& inst_sii = sema->inst_info_full(inst);
    bool const is_never_used = inst_sii.build_last_ref.is_none();

    if (is_never_used) {
        yy_assert(parent_fn->mcv_map.get({}, inst).is_none());
        return;
    }
    parent_fn->mcv_map.put_no_clobber_emplace({}, inst, src_mcv);
    referenceInstMcv(codegen, parent_fn, inst, inst);
}

static void storeInstMcvMultiple(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    InstIndex const inst, McValue src_mcv)
{
    Sema *const sema = codegen->sema;
    auto& inst_sii = sema->inst_info_full(inst);
    bool const is_never_used = inst_sii.build_last_ref.is_none();

    if (is_never_used) {
        yy_assert(parent_fn->mcv_map.get({}, inst).is_none());
        return;
    }
    SemaTypeId const inst_type = inst_sii.type3;
    // TODO: If the type size is zero, there's no need to create an lta.
    auto gop = parent_fn->mcv_map.get_or_put_entry({}, inst).map_put_value([&](auto&&...){
        // Create the lta and map it to the mcv map
        LtaNodeId const new_lta_node_id = lta_alloc_type(sema, &parent_fn->lta, inst_type);
        yy_assert(!lta_nodePtr(&parent_fn->lta, new_lta_node_id)->is_free);
        return MetaTu_init(McValue, lta_value, new_lta_node_id);
    });
    genMcvMove(codegen, parent_fn, gop.value, src_mcv, inst_type);
    referenceInstMcv(codegen, parent_fn, inst, inst);
}

NODISCARD static McValue genTypedLoadIntoNewLta(
    Yy_CodegenX64 *const codegen, Function *const parent_fn,
    McValue const ptr_mcv, SemaTypeId const ptr_type)
{
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  genTypedLoadIntoNewLta {\n");
    Sema *const sema = codegen->sema;
    SemaTypeId const ptr_child_type = typeChild(sema, ptr_type);
    LtaNodeId const new_dest_lta_node = lta_alloc_type(sema, &parent_fn->lta, ptr_child_type);
    genMemoryCopy(codegen, parent_fn, MetaTu_init(McValue, lta_loc, new_dest_lta_node), ptr_mcv, ptr_child_type);
    McValue const result = MetaTu_init(McValue, lta_value, new_dest_lta_node);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, ";  }\n");
    return result;
}

NODISCARD static YyStatus irBlockElementOne(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx);

NODISCARD static YyStatus irBlock(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__block);

    // NOTE: parent_block_inst_idx is either a block or == parent_fn.
    (void) parent_block_inst_idx;

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; block " IOF_Fmt " begin {\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    array_list_printf(&codegen->asm_text, "_y_inst_%u_begin:\n", inst_idx.x);
    u32 const block_end_offset = genir_extraPl(gi, inst_idx, block)->end_offset;
    {
        u32 const stop_after_nth = block_end_offset - 1;
        InstIndex const elem_start = inst_idx + 1;
        InstIndex elem_inst_idx = elem_start;
        bool got_noreturn = false;
        while (!got_noreturn && elem_inst_idx < elem_start + stop_after_nth) {
            u32 elem_step = genir_instEndOffset(gi, elem_inst_idx);
            TRY(irBlockElementOne(codegen, parent_fn, inst_idx, elem_inst_idx));
            if (typeIsNoreturn(sema, sema->inst_info_full(elem_inst_idx).type3))
                got_noreturn = true;
            elem_inst_idx += elem_step;
        }
        yy_assert(got_noreturn);
    }
    array_list_printf(&codegen->asm_text, "_y_inst_%u_end:\n", inst_idx.x);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; block " IOF_Fmt " } end\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    return Yy_Ok;
}

NODISCARD static YyStatus irLoop(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__loop);
    InstIndex const loop_inner = yy_ir_inst_loop_getLoopInnerInstIndex(inst_idx);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; loop " IOF_Fmt " begin {\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    array_list_printf(&codegen->asm_text, "_y_inst_%u_begin:\n", inst_idx.x);

    // Generate the inner block
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; loop inner block " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, loop_inner)));
    TRY(irBlockElementOne(codegen, parent_fn, parent_block_inst_idx, loop_inner));

    // Restart the loop
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; restart loop " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    array_list_printf(&codegen->asm_text, "jmp _y_inst_%u_begin\n", inst_idx.x);

    // Label for the end of the `loop`.
    array_list_printf(&codegen->asm_text, "_y_inst_%u_end:\n", inst_idx.x);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; loop " IOF_Fmt " } end\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));

    return Yy_Ok;
}

NODISCARD static YyStatus irCondBranch(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__cond_branch);

    (void) parent_block_inst_idx;

    Yy_Ir_Inst_ExtraPl_CondBranch const cond_branch_extra = *genir_extraPl(gi, inst_idx, cond_branch);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; cond_branch " IOF_Fmt " begin {\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    array_list_printf(&codegen->asm_text, "_y_inst_%u_begin:\n", inst_idx.x);

    InstIndex const cond = genir_instAbsSB(inst_idx, cond_branch_extra.cond);
    InstIndex const then = genir_instAbsSF(inst_idx, cond_branch_extra.then);
    InstIndex const xelse = genir_instAbsSF(inst_idx, cond_branch_extra.xelse);

    {
        // load the condition
        yy_assert(decodeTypeTag(codegen->sema, codegen->sema->inst_info_full(cond).type3) == SemaType_Tag__xbool);
        McValue const cond_mcv = instMcv(parent_fn, cond);
        Register const cond_reg = genMcvCloneIntoRegister(
            codegen, parent_fn, cond_mcv, codegen->sema->inst_info_full(cond).type3).payload.reg;
        referenceInstMcv(codegen, parent_fn, cond, inst_idx);
        char const *const cond_reg_str = register_name(cond_reg, 8);

        // If zero go to `xelse`
        array_list_printf(&codegen->asm_text, "test %s, %s\n", cond_reg_str, cond_reg_str);
        regalloc_free(&parent_fn->reg_alloc, cond_reg);
    }
    array_list_printf(&codegen->asm_text, "jz _y_inst_%u_begin\n", xelse.x);
    // Otherwise, go to `then`.
    // Generate `then`.
    yy_assert(gi->insts[then].tag == Yy_Ir_Inst_Tag__block);
    InstIndex const then_and_xelse_parent_block = parent_block_inst_idx;
    TRY(irBlockElementOne(codegen, parent_fn, then_and_xelse_parent_block, then));
    // Go to the end of the cond_branch to skip the else block.
    array_list_printf(&codegen->asm_text, "jmp _y_inst_%u_end\n", inst_idx.x);

    // Generate `xelse`
    yy_assert(gi->insts[xelse].tag == Yy_Ir_Inst_Tag__block);
    TRY(irBlockElementOne(codegen, parent_fn, then_and_xelse_parent_block, xelse));

    // Label for the ned of the `cond_branch`.
    array_list_printf(&codegen->asm_text, "_y_inst_%u_end:\n", inst_idx.x);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; cond_branch " IOF_Fmt " } end\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    return Yy_Ok;
}

NODISCARD static YyStatus irLocalDeclValOrRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx, bool const by_ref)
{
    (void) parent_block_inst_idx;
    // abstract: Read the value (or get the pointer) and put it into the local variables on the stack.

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_decl_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_decl_val);
    }

    InstIndex const ref = inst.small_data.local_decl_val_or_ref.ref.abs(inst_idx);

    switch (gi->insts[ref].tag) {
        case MetaEnum_count(Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__block: case Yy_Ir_Inst_Tag__cond_branch: case Yy_Ir_Inst_Tag__loop:
        case Yy_Ir_Inst_Tag__break_void: case Yy_Ir_Inst_Tag__break_value:
        case Yy_Ir_Inst_Tag__return_void: case Yy_Ir_Inst_Tag__return_value:
        case Yy_Ir_Inst_Tag__xunreachable:
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
        case Yy_Ir_Inst_Tag__truncate_int: case Yy_Ir_Inst_Tag__promote_int:
        case Yy_Ir_Inst_Tag__ptr_cast:
        case Yy_Ir_Inst_Tag__int_to_ptr:
        case Yy_Ir_Inst_Tag__ptr_to_int:
        case Yy_Ir_Inst_Tag__const_undefined:
        case Yy_Ir_Inst_Tag__const_int: case Yy_Ir_Inst_Tag__const_bool: case Yy_Ir_Inst_Tag__const_cstring:
        case Yy_Ir_Inst_Tag__placeholder_ref:
        case Yy_Ir_Inst_Tag__discard:
        case Yy_Ir_Inst_Tag__analyzed_field_val: case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val: case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__decl_val: case Yy_Ir_Inst_Tag__decl_ref:
        case Yy_Ir_Inst_Tag__local_decl_val: case Yy_Ir_Inst_Tag__local_decl_ref:
        case Yy_Ir_Inst_Tag__call:
        case Yy_Ir_Inst_Tag__address_of: case Yy_Ir_Inst_Tag__deref_ref: case Yy_Ir_Inst_Tag__deref_val:
        case Yy_Ir_Inst_Tag__assign:
        case Yy_Ir_Inst_Tag__ptr_add_int: case Yy_Ir_Inst_Tag__ptr_sub_int: case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add: case Yy_Ir_Inst_Tag__sub: case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap: case Yy_Ir_Inst_Tag__sub_wrap: case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div: case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl: case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and: case Yy_Ir_Inst_Tag__bits_or: case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq: case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt: case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt: case Yy_Ir_Inst_Tag__bool_gte:
        case Yy_Ir_Inst_Tag__bool_not: case Yy_Ir_Inst_Tag__bits_not:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_uintword: case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid: case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__struct_decl:
        case Yy_Ir_Inst_Tag__fn_decl:
        case Yy_Ir_Inst_Tag__extern_fn_decl:
        case Yy_Ir_Inst_Tag__global_var_decl:
        case Yy_Ir_Inst_Tag__extern_var_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__fn_arg_decl:
        case Yy_Ir_Inst_Tag__local_var_decl: {
            SemaTypeId const type = sema->inst_info_full(inst_idx).type3;
            McValue const the_mcv_to_clone =
                by_ref ? MetaTu_init(McValue, local_var_loc, ref)
                : MetaTu_init(McValue, local_var_value, ref);
            McValue const result_mcv = genMcvCloneLtaOrImmediate(codegen, parent_fn, the_mcv_to_clone, type);
            storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);

            return Yy_Ok;
        }
        break;
    }
    yy_unreachable();
}

NODISCARD static YyStatus irDeclValOrRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx, bool const by_ref)
{
    (void) parent_block_inst_idx;
    // abstract: Read the value (or get the pointer) and put it into the local variables on the stack.
    // TODO: `struct Function { u32 tmp_local_count; }`. For now, one temporary local
    // variable for instruction, regardless of whether they output a value or not. Later,
    // I'll want `Function` to have `tmp_locals: Map(InstIndex, void)`, and not generate
    // temporary locals when they're not needed, e.g., in a `block`, `break`, `discard`,
    // etc.

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__decl_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__decl_val);
    }

    auto const ref_decl_id = genir_extrasAtTypedNoAssert<Yy_Ir_Inst_ExtraPl_DeclValOrRef>(
        gi, inst.small_data.decl_val_or_ref.extra_pl_index
    )->ref_decl.decode();
    yy_assert(!gi->decl_ptrs[ref_decl_id]->has_any_hard_errors());
    auto const ref_inst_idx = gi->decl_ptrs[ref_decl_id]->inst;

    SemaTypeId const type = sema->inst_info_full(inst_idx).type3;

    switch (gi->insts[ref_inst_idx].tag) {
        case MetaEnum_count(Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__block:
        case Yy_Ir_Inst_Tag__cond_branch:
        case Yy_Ir_Inst_Tag__loop:
        case Yy_Ir_Inst_Tag__break_void:
        case Yy_Ir_Inst_Tag__break_value:
        case Yy_Ir_Inst_Tag__return_void:
        case Yy_Ir_Inst_Tag__return_value:
        case Yy_Ir_Inst_Tag__xunreachable:
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
        case Yy_Ir_Inst_Tag__truncate_int:
        case Yy_Ir_Inst_Tag__promote_int:
        case Yy_Ir_Inst_Tag__ptr_cast:
        case Yy_Ir_Inst_Tag__int_to_ptr:
        case Yy_Ir_Inst_Tag__ptr_to_int:
        case Yy_Ir_Inst_Tag__const_undefined:
        case Yy_Ir_Inst_Tag__const_int:
        case Yy_Ir_Inst_Tag__const_bool:
        case Yy_Ir_Inst_Tag__const_cstring:
        case Yy_Ir_Inst_Tag__placeholder_ref:
        case Yy_Ir_Inst_Tag__discard:
        case Yy_Ir_Inst_Tag__analyzed_field_val:
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val:
        case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__decl_val:
        case Yy_Ir_Inst_Tag__decl_ref:
        case Yy_Ir_Inst_Tag__local_decl_val:
        case Yy_Ir_Inst_Tag__local_decl_ref:
        case Yy_Ir_Inst_Tag__call:
        case Yy_Ir_Inst_Tag__address_of:
        case Yy_Ir_Inst_Tag__deref_ref:
        case Yy_Ir_Inst_Tag__deref_val:
        case Yy_Ir_Inst_Tag__assign:
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub_wrap:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte:
        case Yy_Ir_Inst_Tag__bool_not:
        case Yy_Ir_Inst_Tag__bits_not:
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__fn_arg_decl:
        case Yy_Ir_Inst_Tag__local_var_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__struct_decl: {
            codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, inst_idx), "TODO: decl_ref/decl_val struct_decl");
            return Yy_Bad;
        }
        break;
        case Yy_Ir_Inst_Tag__fn_decl: {
            if (by_ref) {
                codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, inst_idx), "can't take a reference from a function");
                return Yy_Bad;
            }
            McValue const result_mcv = MetaTu_init(McValue, fn_address, ref_inst_idx);
            storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);
            return Yy_Ok;
        }
        break;
        case Yy_Ir_Inst_Tag__extern_fn_decl: {
            if (by_ref) {
                codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, inst_idx), "can't take a reference from a function");
                return Yy_Bad;
            }
            McValue const result_mcv = MetaTu_init(McValue, extern_fn_address, ref_inst_idx);
            storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);
            return Yy_Ok;
        }
        break;
        case Yy_Ir_Inst_Tag__global_var_decl: {
            McValue const the_mcv_to_clone =
                by_ref ? MetaTu_init(McValue, global_var_loc, ref_inst_idx)
                : MetaTu_init(McValue, global_var_value, ref_inst_idx);
            McValue const result_mcv = genMcvCloneLtaOrImmediate(codegen, parent_fn, the_mcv_to_clone, type);
            storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);
            return Yy_Ok;
        }
        break;
        case Yy_Ir_Inst_Tag__extern_var_decl: {
            McValue const the_mcv_to_clone =
                by_ref ? MetaTu_init(McValue, extern_var_loc, ref_inst_idx)
                : MetaTu_init(McValue, extern_var_value, ref_inst_idx);
            McValue const result_mcv = genMcvCloneLtaOrImmediate(codegen, parent_fn, the_mcv_to_clone, type);
            storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);

            return Yy_Ok;
        }
        break;
    }
    yy_unreachable();
}

NODISCARD static YyStatus irDeclVal(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irDeclValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, false);
}

NODISCARD static YyStatus irDeclRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irDeclValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, true);
}

NODISCARD static YyStatus irAnalyzedFieldValOrRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx, bool const by_ref)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__analyzed_field_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__analyzed_field_val);
    }

    auto const pl = *genir_extrasAtTypedNoAssert<Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef>(
        gi, inst.small_data.analyzed_field_val_or_ref.extra_pl_index
    );
    auto const container = pl.container.abs(inst_idx);
    auto const container_type = sema->inst_info_full(container).type3;
    auto const struct_type_id = typeChild(sema, container_type);
    auto const struct_type = typeBase(sema, struct_type_id)->get<SemaTypePacked::Struct>();
    auto _ = sema->extra_pointers_lock.scoped_lock();
    auto& member = struct_type->resolved_members()[pl.field_idx];
    u64 const field_offset = member.offset;
    auto const container_mcv = instMcv(parent_fn, container);
    auto tmp_reg = genMcvCloneIntoRegister(codegen, parent_fn, container_mcv, container_type).payload.reg;
    yy_assert(sema->target_info.ptr_size_align.size_of*8 == 64);
    array_list_printf(&codegen->asm_text, "add %s, %" PRIu64 "\n", register_name(tmp_reg, 64), field_offset);
    referenceInstMcv(codegen, parent_fn, container, inst_idx);
    auto const field_ptr_type = typePointerTo(sema, member.type);
    McValue const result_mcv =
        by_ref ? genMcvCloneLtaOrImmediate(codegen, parent_fn, MetaTu_init(McValue, reg, tmp_reg), field_ptr_type)
        : genTypedLoadIntoNewLta(codegen, parent_fn, MetaTu_init(McValue, reg, tmp_reg), field_ptr_type);
    storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);

    regalloc_free(&parent_fn->reg_alloc, tmp_reg);
    return Yy_Ok;
}
NODISCARD static YyStatus irAnalyzedFieldVal(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irAnalyzedFieldValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, false);
}

NODISCARD static YyStatus irAnalyzedFieldRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irAnalyzedFieldValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, true);
}

NODISCARD static YyStatus irCall(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call);

    Yy_Ir_Inst_ExtraPl_Call const call_extra_data = *genir_extraPl(gi, inst_idx, call);
    InstIndex const callee = genir_instAbsSB(inst_idx, call_extra_data.callee);
    u32 const arg_count = call_extra_data.arg_count;

    // 1. Before calling a subroutine, the caller should save the contents of certain registers that
    // are designated caller-saved. The caller-saved registers are r10, r11, and any registers that
    // parameters are put into. If you want the contents of these registers to be preserved across
    // the subroutine call, push them onto the stack.
    // TODO: Don't save registers which aren't used.
    Register const saved_registers[] = { Register__r10, Register__r11, Register__r12, Register__rax };
    saveRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));

    if (arg_count > ARRAY_COUNT(c_callconv_arg_regs)) {
        codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, inst_idx), "TODO: more than six parameters");
        return Yy_Bad;
    }
    saveRegisters(codegen, parent_fn, c_callconv_arg_regs, arg_count);

    // 2. To pass parameters to the subroutine, we put up to six of them into registers
    // (in order: rdi, rsi, rdx, rcx, r8, r9). If there are more than six parameters to the
    // subroutine, then push the rest onto the stack in reverse order (i.e. last parameter first) –
    // since the stack grows down, the first of the extra parameters (really the seventh parameter)
    // parameter will be stored at the lowest address (this inversion of parameters was historically
    // used to allow functions to be passed a variable number of parameters).
    // TODO: More than six parameters.
    for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
        Yy_Ir_Inst_ExtraPl_Call_Arg const arg_data = genir_getCallArgData(gi, inst_idx, arg_idx);
        Yy_Ir_InstIndex const arg_inst = genir_instAbsSB(inst_idx, arg_data.inst);
        if (asm_comments)
            array_list_printf(&codegen->asm_text, "; arg %u from " IOF_Fmt "\n", arg_idx, IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, arg_inst)));
        McValue const arg_inst_mcv = instMcv(parent_fn, arg_inst);
        SemaTypeId const arg_type = sema->inst_info_full(arg_inst).type3;
        genMcvMoveIntoStringRegister(codegen, parent_fn, c_callconv_arg_regs[arg_idx], arg_inst_mcv, arg_type);
        referenceInstMcv(codegen, parent_fn, arg_inst, inst_idx);
    }

    // 3. To call the subroutine, use the call instruction. This instruction places the return
    // address on top of the parameters on the stack, and branches to the subroutine code.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load callee " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, callee)));
    {
        McValue const callee_mcv = instMcv(parent_fn, callee);
        if (mcvAsmKindSupportsCallOperand(mcvAsmKind(callee_mcv.tag))) {
            SemaTypeId const callee_type = sema->inst_info_full(callee).type3;
            array_list_printf(&codegen->asm_text, "call ");
            mcvAsmWrite(codegen, parent_fn, callee_mcv, callee_type);
            array_list_printf(&codegen->asm_text, "\n");
        } else {
            SemaTypeId const callee_type = sema->inst_info_full(callee).type3;
            McValue const tmp_reg_mcv = genMcvCloneIntoRegister(codegen, parent_fn, callee_mcv, callee_type);
            array_list_printf(&codegen->asm_text, "call ");
            mcvAsmWrite(codegen, parent_fn, tmp_reg_mcv, callee_type);
            array_list_printf(&codegen->asm_text, "\n");
            regalloc_free(&parent_fn->reg_alloc, *MetaTu_get(McValue, &tmp_reg_mcv, reg));
        }
        referenceInstMcv(codegen, parent_fn, callee, inst_idx);
    }

    // 4. After the subroutine returns, (i.e. immediately following the call instruction) the caller
    // must remove any additional parameters (beyond the six stored in registers) from stack. This
    // restores the stack to its state before the call was performed.
    // TODO: Remove additional parameters from the stack (none).

    // 5. The caller can expect to find the return value of the subroutine in the register RAX.
    auto& inst_sii = sema->inst_info_full(inst_idx);
    bool const is_noreturn = typeIsNoreturn(sema, inst_sii.type3);
    if (is_noreturn) {
        yy_assert(inst_sii.build_last_ref.is_none());
    } else {
        yy_assert(typeIdMemorySizeOf(sema, inst_sii.type3) <= sema->target_info.ptr_size_align.size_of);
        Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
        array_list_printf(&codegen->asm_text, "mov %s, rax\n", register_name(tmp_reg, 64));
        McValue const tmp_reg_mcv = MetaTu_init(McValue, reg, tmp_reg);

        storeInstMcv(codegen, parent_fn, inst_idx, tmp_reg_mcv);
        regalloc_free(&parent_fn->reg_alloc, tmp_reg);
    }

    // 6. The caller restores the contents of caller-saved registers (r10, r11, and any in the
    // parameter passing registers) by popping them off of the stack. The caller can assume that no
    // other registers were modified by the subroutine.
    restoreRegisters(codegen, parent_fn, c_callconv_arg_regs, arg_count);
    restoreRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));

    return Yy_Ok;
}

NODISCARD static YyStatus irAddressOf(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__address_of);
    InstIndex const operand = genir_instAbsSB(inst_idx, inst.small_data.address_of.operand);

    McValue const operand_mcv = instMcv(parent_fn, operand);
    storeInstMcv(codegen, parent_fn, inst_idx, operand_mcv);
    referenceInstMcv(codegen, parent_fn, operand, inst_idx);

    return Yy_Ok;
}

NODISCARD static YyStatus irDerefRefOrVal(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__deref_ref || inst.tag == Yy_Ir_Inst_Tag__deref_val);
    bool const is_ref = inst.tag == Yy_Ir_Inst_Tag__deref_ref;
    InstIndex const operand =
        genir_instAbsSB(inst_idx, is_ref ? inst.small_data.deref_ref.operand : inst.small_data.deref_val.operand);

    auto& operand_sii = sema->inst_info_full(operand);
    yy_assert(typeIsPointer(sema, operand_sii.type3));

    {
        SemaTypeId const operand_type = codegen->sema->inst_info_full(operand).type3;
        McValue const operand_mcv = instMcv(parent_fn, operand);
        McValue const result_mcv =
            is_ref ? genMcvCloneLtaOrImmediate(codegen, parent_fn, operand_mcv, operand_type)
            : genTypedLoadIntoNewLta(codegen, parent_fn, operand_mcv, operand_type);
        referenceInstMcv(codegen, parent_fn, operand, inst_idx);
        storeInstMcvNoClone(codegen, parent_fn, inst_idx, result_mcv);
    }

    return Yy_Ok;
}

NODISCARD static YyStatus irDerefRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irDerefRefOrVal(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}

NODISCARD static YyStatus irDerefVal(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    return irDerefRefOrVal(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}

NODISCARD static YyStatus irBreakVoid(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__break_void);


    InstIndex const break_to = genir_instAbsSB(inst_idx, inst.small_data.break_void.block_to_break_to);
    storeInstMcvMultiple(codegen, parent_fn, break_to, MetaTu_init(McValue, zero_sized_value, Void{}));

    // TODO: Omit this "jmp" if it would jump to the address immediately after.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; break to " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, break_to)));
    array_list_printf(&codegen->asm_text, "jmp _y_inst_%u_end\n", break_to.x);
    return Yy_Ok;
}

NODISCARD static YyStatus irBreakValue(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__break_value);

    Yy_Ir_Inst_ExtraPl_BreakValue const extra = *genir_extraPl(gi, inst_idx, break_value);
    InstIndex const break_to = genir_instAbsSB(inst_idx, extra.block_to_break_to);

    // Store the value
    {
        InstIndex const value = genir_instAbsSB(inst_idx, extra.value);
        storeInstMcvMultiple(codegen, parent_fn, break_to, instMcv(parent_fn, value));
        referenceInstMcv(codegen, parent_fn, value, inst_idx);
    }

    // Jump
    // TODO: Omit this "jmp" if it would jump to the address immediately after.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; break to " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, break_to)));
    array_list_printf(&codegen->asm_text, "jmp _y_inst_%u_end\n", break_to.x);
    return Yy_Ok;
}

NODISCARD static YyStatus irReturnVoid(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__return_void);

    array_list_printf(&codegen->asm_text, "jmp _y_fn_%u_return_label\n", parent_fn->fn_decl_inst.x);
    return Yy_Ok;
}

NODISCARD static YyStatus irReturnValue(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__return_value);

    InstIndex const operand = genir_instAbsSB(inst_idx, inst.small_data.return_value.operand);

    // Store the value in rax.
    genMcvMoveIntoStringRegister(codegen, parent_fn, Register__rax, instMcv(parent_fn, operand), sema->inst_info_full(operand).type3);
    referenceInstMcv(codegen, parent_fn, operand, inst_idx);

    // Jump
    array_list_printf(&codegen->asm_text, "jmp _y_fn_%u_return_label\n", parent_fn->fn_decl_inst.x);
    return Yy_Ok;
}


NODISCARD static YyStatus irXunreachable(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__xunreachable);

    array_list_printf(&codegen->asm_text, "ud2\n");
    return Yy_Ok;
}

NODISCARD static YyStatus irDiscard(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__discard);

    InstIndex const to_discard = genir_instAbsSB(inst_idx, inst.small_data.discard.inst_to_discard);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; discarded " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, to_discard)));

    referenceInstMcv(codegen, parent_fn, to_discard, inst_idx);
    storeInstMcvNoClone(codegen, parent_fn, inst_idx, MetaTu_init(McValue, zero_sized_value, Void{}));
    return Yy_Ok;
}

NODISCARD static YyStatus irPlaceholderRef(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__placeholder_ref);

    InstIndex const refd = genir_instAbsSB(inst_idx, inst.small_data.placeholder_ref);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; placeholder_ref " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, refd)));

    referenceInstMcv(codegen, parent_fn, refd, inst_idx);
    storeInstMcvNoClone(codegen, parent_fn, inst_idx, MetaTu_init(McValue, zero_sized_value, Void{}));
    yy_assert(decodeTypeTag(codegen->sema, codegen->sema->inst_info_full(inst_idx).type3) == SemaType_Tag__xvoid);
    return Yy_Ok;
}

NODISCARD static YyStatus irAssign(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__assign);

    Yy_Ir_Inst_ExtraPl_Assign const extra = *genir_extraPl(gi, inst_idx, assign);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);

    SemaTypeId const rhs_type = sema->inst_info_full(rhs).type3;
    McValue const lhs_mcv = instMcv(parent_fn, lhs);
    McValue const rhs_mcv = instMcv(parent_fn, rhs);
    genMcvMoveDeref(codegen, parent_fn, lhs_mcv, rhs_mcv, rhs_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);
    storeInstMcvNoClone(codegen, parent_fn, inst_idx, MetaTu_init(McValue, zero_sized_value, Void{}));

    return Yy_Ok;
}

NODISCARD static YyStatus irPtrAddInt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;

    yy_assert(genir_instsAt(gi, inst_idx)->tag == Yy_Ir_Inst_Tag__ptr_add_int);
    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const ptr_inst = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const int_inst = genir_instAbsSB(inst_idx, extra.rhs);

    SemaTypeId const ptr_type = sema->inst_info_full(ptr_inst).type3;
    yy_assert(typeIsPointer(sema, ptr_type));

    SemaTypeId const int_type = sema->inst_info_full(int_inst).type3;
    yy_assert(decodeTypeTag(sema, int_type) == SemaType_Tag__uintword);

    yy_assert(typeIdMemorySizeOf(sema, ptr_type) == typeIdMemorySizeOf(sema, int_type));

    SemaTypeId const ptr_child_type = typeChild(sema, ptr_type);
    u64 ptr_child_sizeof = typeIdMemorySizeOf(sema, ptr_child_type);

    // Load pointer
    Register const ptr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, ptr_reg, instMcv(parent_fn, ptr_inst), ptr_type);
    referenceInstMcv(codegen, parent_fn, ptr_inst, inst_idx);

    // Load integer
    Register const int_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, int_reg, instMcv(parent_fn, int_inst), int_type);
    referenceInstMcv(codegen, parent_fn, int_inst, inst_idx);

    // Perform the operation
    yy_assert(sema->target_info.ptr_size_align.size_of*8 == 64);
    array_list_printf(&codegen->asm_text, "lea %s, [%s + %s*%" PRIu64 "]\n",
                      register_name(ptr_reg, 64),
                      register_name(ptr_reg, 64),
                      register_name(int_reg, 64),
                      ptr_child_sizeof);
    regalloc_free(&parent_fn->reg_alloc, int_reg);

    // Store the result.
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, ptr_reg));
    regalloc_free(&parent_fn->reg_alloc, ptr_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irPtrSubInt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;

    yy_assert(genir_instsAt(gi, inst_idx)->tag == Yy_Ir_Inst_Tag__ptr_sub_int);
    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const ptr_inst = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const int_inst = genir_instAbsSB(inst_idx, extra.rhs);

    SemaTypeId const ptr_type = sema->inst_info_full(ptr_inst).type3;
    yy_assert(typeIsPointer(sema, ptr_type));

    SemaTypeId const int_type = sema->inst_info_full(int_inst).type3;
    yy_assert(decodeTypeTag(sema, int_type) == SemaType_Tag__uintword);

    yy_assert(typeIdMemorySizeOf(sema, ptr_type) == typeIdMemorySizeOf(sema, int_type));

    SemaTypeId const ptr_child_type = typeChild(sema, ptr_type);
    u64 ptr_child_sizeof = typeIdMemorySizeOf(sema, ptr_child_type);

    // Load pointer
    Register const ptr_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, ptr_reg, instMcv(parent_fn, ptr_inst), ptr_type);
    referenceInstMcv(codegen, parent_fn, ptr_inst, inst_idx);

    // Load integer
    Register const int_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, int_reg, instMcv(parent_fn, int_inst), int_type);
    referenceInstMcv(codegen, parent_fn, int_inst, inst_idx);

    // Perform the operation
    yy_assert(sema->target_info.ptr_size_align.size_of*8 == 64);
    array_list_printf(&codegen->asm_text, "neg %s\n", register_name(int_reg, 64));
    array_list_printf(&codegen->asm_text, "lea %s, [%s + %s*%" PRIu64 "]\n",
                      register_name(ptr_reg, 64),
                      register_name(ptr_reg, 64),
                      register_name(int_reg, 64),
                      ptr_child_sizeof);
    regalloc_free(&parent_fn->reg_alloc, int_reg);

    // Store the result.
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, ptr_reg));
    regalloc_free(&parent_fn->reg_alloc, ptr_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irPtrSubPtr(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;

    yy_assert(genir_instsAt(gi, inst_idx)->tag == Yy_Ir_Inst_Tag__ptr_sub_ptr);
    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs_inst = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs_inst = genir_instAbsSB(inst_idx, extra.rhs);

    SemaTypeId const ptr_type = sema->inst_info_full(lhs_inst).type3;
    yy_assert(typeIsPointer(sema, ptr_type));
    yy_assert(typeIdEql(sema, sema->inst_info_full(lhs_inst).type3, sema->inst_info_full(rhs_inst).type3));

    SemaTypeId const int_type = sema->inst_info_full(inst_idx).type3;
    yy_assert(decodeTypeTag(sema, int_type) == SemaType_Tag__uintword);

    yy_assert(typeIdMemorySizeOf(sema, ptr_type) == typeIdMemorySizeOf(sema, int_type));

    SemaTypeId const ptr_child_type = typeChild(sema, ptr_type);
    u64 ptr_child_sizeof = typeIdMemorySizeOf(sema, ptr_child_type);
    if (ptr_child_sizeof == 0) {
        // This should be detectable during sema.
        yy_panic("ptr subtract elem sizeof zero");
    }

    // Load pointer
    Register const lhs_reg = Register__rax;
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs_inst), ptr_type);
    referenceInstMcv(codegen, parent_fn, lhs_inst, inst_idx);

    // Load integer
    Register const rhs_reg = Register__rbx;
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs_inst), ptr_type);
    referenceInstMcv(codegen, parent_fn, rhs_inst, inst_idx);

    // Perform the operation
    // subtract, divide
    yy_assert(sema->target_info.ptr_size_align.size_of*8 == 64);
    array_list_printf(&codegen->asm_text, "sub %s, %s\n", register_name(lhs_reg, 64), register_name(rhs_reg, 64));
    array_list_printf(&codegen->asm_text, "mov rbx, %" PRIu64 "\n", ptr_child_sizeof);
    // perform unsigned divide. result rax: div, rdx: mod
    array_list_printf(&codegen->asm_text, "xor rdx, rdx\n");
    array_list_printf(&codegen->asm_text, "div rbx\n");

    // Store the result.
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, Register__rax));

    return Yy_Ok;
}

NODISCARD static YyStatus genBinaryOperatorSimple(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx, char const *const x64_inst_name)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;

    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);

    // TODO: For now, one of them may be a pointer type but later on I'll check both types are identical.
    SemaTypeId const int_type = sema->inst_info_full(lhs).type3;
    yy_assert(sema->inst_info_full(lhs).type3 == sema->inst_info_full(rhs).type3);
    yy_assert(typeIdMemorySizeOf(sema, sema->inst_info_full(lhs).type3) == typeIdMemorySizeOf(sema, sema->inst_info_full(rhs).type3));
    u64 const int_byte_size = typeIdMemorySizeOf(sema, int_type);
    u32 const reg_size = yy::checked_int_cast<u32>(int_byte_size * 8).unwrap();

    // Load lhs
    Register const lhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs), int_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);

    // Load rhs
    Register const rhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs), int_type);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);

    // Perform the operation
    array_list_printf(&codegen->asm_text, "%s %s, %s\n", x64_inst_name, register_name(lhs_reg, reg_size), register_name(rhs_reg, reg_size));
    regalloc_free(&parent_fn->reg_alloc, rhs_reg);

    // Store the result.
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, lhs_reg));
    regalloc_free(&parent_fn->reg_alloc, lhs_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus genUnaryOperatorSimple(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx, char const *const x64_inst_name)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.unary.operand);

    // Load operand
    Register const operand_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    SemaTypeId const int_type = codegen->sema->inst_info_full(operand).type3;
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load operand " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, operand)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, operand_reg, instMcv(parent_fn, operand), int_type);
    referenceInstMcv(codegen, parent_fn, operand, inst_idx);

    // Perform the operation
    array_list_printf(&codegen->asm_text, "%s %s\n", x64_inst_name, register_name(operand_reg, 64));

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, operand_reg));
    regalloc_free(&parent_fn->reg_alloc, operand_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus genBinaryOperatorMul(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    SemaTypeId const int_type = codegen->sema->inst_info_full(lhs).type3;

    // Load lhs
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load lhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, lhs)));
    Register const lhs_reg = Register__rax;
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs), int_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);

    // Load rhs
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load rhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, rhs)));
    Register const rhs_reg = Register__rbx;
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs), int_type);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);

    // Perform the operation
    array_list_printf(&codegen->asm_text, "mul %s\n", register_name(rhs_reg, 64));

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    array_list_printf(&codegen->asm_text, "mov %s, rax\n", register_name(result_reg, 64));
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus genBinaryOperatorDivMod(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    SemaTypeId const int_type = codegen->sema->inst_info_full(lhs).type3;

    // Load lhs
    array_list_printf(&codegen->asm_text, "xor rdx, rdx\n");
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load lhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, lhs)));
    Register const lhs_reg = Register__rax;
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs), int_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);

    // Load rhs
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load rhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, rhs)));
    Register const rhs_reg = Register__rbx;
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs), int_type);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);

    // Perform the operation
    array_list_printf(&codegen->asm_text, "div rbx\n");
    // rax: div, rdx: mod

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    Inst_Tag const inst_tag = gi->insts[inst_idx].tag;
    char const *calculation_reg;
    if (inst_tag == Yy_Ir_Inst_Tag__div) {
        calculation_reg = "rax";
    } else if (inst_tag == Yy_Ir_Inst_Tag__mod) {
        calculation_reg = "rdx";
    } else yy_unreachable();
    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const result_reg_name = register_name(result_reg, 64);
    array_list_printf(&codegen->asm_text, "mov %s, %s\n", result_reg_name, calculation_reg);
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus genBinaryOperatorShift(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx, char const *const x64_inst_name)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    SemaTypeId const int_type = codegen->sema->inst_info_full(lhs).type3;

    // Load lhs
    Register const lhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const lhs_reg_name = register_name(lhs_reg, 64);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load lhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, lhs)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs), int_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);

    // Load rhs
    Register const rhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const rhs8_reg_name = register_name(rhs_reg, 8);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load rhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, rhs)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs), int_type);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);

    // Perform the operation. TODO: Safety check that the rhs isn't too big.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; truncate rhs into cl and shift\n");
    array_list_printf(&codegen->asm_text, "mov cl, %s\n", rhs8_reg_name);
    array_list_printf(&codegen->asm_text, "%s %s, cl\n", x64_inst_name, lhs_reg_name);
    regalloc_free(&parent_fn->reg_alloc, rhs_reg);

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, lhs_reg));
    regalloc_free(&parent_fn->reg_alloc, lhs_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus genBinaryOperatorClearCmpSetcc(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx, char const *const cc_name)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);

    InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    SemaTypeId const int_type = codegen->sema->inst_info_full(lhs).type3;

    // Load lhs
    Register const lhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const lhs_reg_name = register_name(lhs_reg, 64);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load lhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, lhs)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, lhs_reg, instMcv(parent_fn, lhs), int_type);
    referenceInstMcv(codegen, parent_fn, lhs, inst_idx);

    // Load rhs
    Register const rhs_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const rhs_reg_name = register_name(rhs_reg, 64);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load rhs " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, rhs)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, rhs_reg, instMcv(parent_fn, rhs), int_type);
    referenceInstMcv(codegen, parent_fn, rhs, inst_idx);

    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const result_reg_name = register_name(result_reg, 64);
    char const *const result8_reg_name = register_name(result_reg, 8);

    // Perform the operation
    array_list_printf(&codegen->asm_text, "xor %s, %s\n", result_reg_name, result_reg_name);
    array_list_printf(&codegen->asm_text, "cmp %s, %s\n", lhs_reg_name, rhs_reg_name);
    array_list_printf(&codegen->asm_text, "set%s %s\n", cc_name, result8_reg_name);
    regalloc_free(&parent_fn->reg_alloc, lhs_reg);
    regalloc_free(&parent_fn->reg_alloc, rhs_reg);

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irBinaryOperator_add(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // TODO: Safety check
    // TODO: Pointer arithmetic
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "add");
}
NODISCARD static YyStatus irBinaryOperator_sub(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // TODO: Safety check
    // TODO: Pointer arithmetic
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "sub");
}
NODISCARD static YyStatus irBinaryOperator_mul(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // TODO: Safety check
    return genBinaryOperatorMul(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}
NODISCARD static YyStatus irBinaryOperator_add_wrap(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // TODO: Pointer arithmetic
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "add");
}
NODISCARD static YyStatus irBinaryOperator_sub_wrap(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // TODO: Pointer arithmetic
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "sub");
}
NODISCARD static YyStatus irBinaryOperator_mul_wrap(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorMul(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}
NODISCARD static YyStatus irBinaryOperator_div(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorDivMod(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}
NODISCARD static YyStatus irBinaryOperator_mod(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorDivMod(codegen, parent_fn, parent_block_inst_idx, inst_idx);
}
NODISCARD static YyStatus irBinaryOperator_shl(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorShift(codegen, parent_fn, parent_block_inst_idx, inst_idx, "shl");
}
NODISCARD static YyStatus irBinaryOperator_shr(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorShift(codegen, parent_fn, parent_block_inst_idx, inst_idx, "shr");
}
NODISCARD static YyStatus irBinaryOperator_bits_and(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "and");
}
NODISCARD static YyStatus irBinaryOperator_bits_or(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "or");
}
NODISCARD static YyStatus irBinaryOperator_bits_xor(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "xor");
}
NODISCARD static YyStatus irBinaryOperator_bool_eq(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "e");
}
NODISCARD static YyStatus irBinaryOperator_bool_noteq(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "ne");
}
NODISCARD static YyStatus irBinaryOperator_bool_lt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // NOTE: signed integer
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "l");
}
NODISCARD static YyStatus irBinaryOperator_bool_lte(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // NOTE: signed integer
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "le");
}
NODISCARD static YyStatus irBinaryOperator_bool_gt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // NOTE: signed integer
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "g");
}
NODISCARD static YyStatus irBinaryOperator_bool_gte(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    // NOTE: signed integer
    return genBinaryOperatorClearCmpSetcc(codegen, parent_fn, parent_block_inst_idx, inst_idx, "ge");
}

NODISCARD static YyStatus irUnaryOperator_bool_not(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;

    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__bool_not);
    InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.unary.operand);
    SemaTypeId const operand_type = codegen->sema->inst_info_full(operand).type3;

    // Load lhs
    Register const operand_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const operand_reg_name = register_name(operand_reg, 64);
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load operand " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, operand)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, operand_reg, instMcv(parent_fn, operand), operand_type);
    referenceInstMcv(codegen, parent_fn, operand, inst_idx);

    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const result_reg_name = register_name(result_reg, 64);
    char const *const result8_reg_name = register_name(result_reg, 8);

    // Perform the operation
    // I don't xor with `1` because I don't have actual types and I don't guarantee the "boolean"
    // must be zero or one.
    array_list_printf(&codegen->asm_text, "xor %s, %s\n", result_reg_name, result_reg_name);
    array_list_printf(&codegen->asm_text, "test %s, %s\n", operand_reg_name, operand_reg_name);
    array_list_printf(&codegen->asm_text, "setz %s\n", result8_reg_name);
    regalloc_free(&parent_fn->reg_alloc, operand_reg);

    // Store the result.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store the result into " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, inst_idx)));
    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irUnaryOperator_bits_not(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    return genUnaryOperatorSimple(codegen, parent_fn, parent_block_inst_idx, inst_idx, "not");
}

NODISCARD static YyStatus irConstUndefined(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__const_undefined);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, xundefined, {}));

    return Yy_Ok;
}
NODISCARD static YyStatus irConstInt(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__const_int);

    Yy_Ir_Inst_ExtraPl_ConstInt const extra = *genir_extraPl(gi, inst_idx, const_int);

    i64 const i64_value = extra.integer.decode();

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, imm_i64, i64_value));

    return Yy_Ok;
}

static void writeConstCstringLabel(Yy_CodegenX64 *const codegen, InstIndex const inst_idx)
{
    array_list_printf(&codegen->asm_text, "_yuceyuf_cstring_%u", inst_idx.x);
}

static void genDefineConstCstring(Yy_CodegenX64 *const codegen, InstIndex cstring_label, Yy_Ir_StringSized0 string)
{
    GenIr *const gi = codegen->sema->gi;

    writeConstCstringLabel(codegen, cstring_label);

    array_list_printf(&codegen->asm_text, ": ");
    Sv const data0 = genir_strTableGetTempSvFromLen(gi, string.start, string.len0);
    for (u32 i = 0; i < data0.len; i += 1) {
        if (i % 8 == 0)
            array_list_printf(&codegen->asm_text, "db ");
        array_list_printf(&codegen->asm_text, "0x%02x%s%s",
                          data0.ptr[i],
                          i + 1 == data0.len ? "" : ", ",
                          i + 1 != data0.len && (i % 8) == 7 ? "\n " : "");
    }
    yy_assert(data0.ptr[data0.len-1] == '\0');
    array_list_printf(&codegen->asm_text, "\n");
}

NODISCARD static YyStatus irConstCstring(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__const_cstring);

    // Define the string label with the contents.
    array_list_printf(&codegen->asm_text, "[segment .data]\n");
    auto pl = *genir_extraPl(gi, inst_idx, const_cstring);
    genDefineConstCstring(codegen, inst_idx, pl.string);
    array_list_printf(&codegen->asm_text, "[segment .text]\n");

    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const result_reg_name = register_name(result_reg, 64);

    // Move the integer value into the temporary local.
    array_list_printf(&codegen->asm_text, "mov %s, ", result_reg_name);
    writeConstCstringLabel(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, "\n");

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irConstBool(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__const_bool);

    bool const bool_value = inst.small_data.const_bool.value;

    Register const result_reg = regalloc_alloc_any(&parent_fn->reg_alloc);
    char const *const result_reg_name = register_name(result_reg, 8);

    // Move the integer value into the temporary local.
    array_list_printf(&codegen->asm_text, "mov %s, %s\n", result_reg_name, bool_value ? "1" : "0");

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, result_reg));
    regalloc_free(&parent_fn->reg_alloc, result_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irFnArgDecl(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__fn_arg_decl);

    // The code generation for function arguments is inside `irFnBody`.

    return Yy_Ok;
}

NODISCARD static YyStatus irCallBuiltinPrintHelloWorld(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call_builtin_print_hello_world);

    // TODO: save registers
    Register const saved_registers[] = { Register__r10, Register__r11, Register__rax };
    saveRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));
    array_list_printf(&codegen->asm_text, "call yuceyuf_builtin_print_hello_world\n");
    restoreRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));

    storeInstMcvNoClone(codegen, parent_fn, inst_idx, MetaTu_init(McValue, zero_sized_value, Void{}));

    return Yy_Ok;
}

NODISCARD static YyStatus irCallBuiltinPrintUint(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call_builtin_print_uint);
    InstIndex const param0_inst_idx = genir_instAbsSB(inst_idx, inst.small_data.call_builtin_print_uint.arg_inst);

    // 1. Save caller-saved registers and registers used for the function parameters.
    Register const saved_registers[] = { Register__r10, Register__r11, Register__rdi, Register__rax };
    saveRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));

    // 2. Prepare parameters
    SemaTypeId const int_type = codegen->sema->inst_info_full(param0_inst_idx).type3;
    array_list_printf(&codegen->asm_text, "; load param0 " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, param0_inst_idx)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, c_callconv_arg_regs[0], instMcv(parent_fn, param0_inst_idx), int_type);
    referenceInstMcv(codegen, parent_fn, param0_inst_idx, inst_idx);

    // 3. Call the function
    array_list_printf(&codegen->asm_text, "call yuceyuf_builtin_print_uint\n");

    // 4. Restore registers
    restoreRegisters(codegen, parent_fn, saved_registers, ARRAY_COUNT(saved_registers));

    storeInstMcvNoClone(codegen, parent_fn, inst_idx, MetaTu_init(McValue, zero_sized_value, Void{}));

    return Yy_Ok;
}

NODISCARD static YyStatus irTruncateInt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__truncate_int);
    Yy_Ir_Inst_ExtraPl_TruncateInt const extra = *genir_extraPl(gi, inst_idx, truncate_int);

    InstIndex const src_value = genir_instAbsSB(inst_idx, extra.src_value);
    SemaTypeId const src_type = sema->inst_info_full(src_value).type3;
    SemaTypeId const dest_type = sema->inst_info_full(inst_idx).type3;
    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    yy_assert(typeIsInteger(sema, src_type));
    yy_assert(typeIsInteger(sema, dest_type));
    yy_assert(dest_size <= src_size);

    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load src_value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, src_value)));
    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, instMcv(parent_fn, src_value), src_type);
    referenceInstMcv(codegen, parent_fn, src_value, inst_idx);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; perform truncation to %" PRIu64 " bytes\n", dest_size);
    char const *const tmp_reg_name = register_name(tmp_reg, intCastFromUsizeToU32(dest_size*8));
    switch (dest_size) {
        case 1: case 2: case 4: case 8: break;
        default: todo();
    }
    array_list_printf(&codegen->asm_text, "and %s, %s\n", tmp_reg_name, tmp_reg_name);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, tmp_reg));
    regalloc_free(&parent_fn->reg_alloc, tmp_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irPromoteInt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__promote_int);
    Yy_Ir_Inst_ExtraPl_PromoteInt const extra = *genir_extraPl(gi, inst_idx, promote_int);

    InstIndex const src_value = genir_instAbsSB(inst_idx, extra.src_value);
    SemaTypeId const src_type = sema->inst_info_full(src_value).type3;
    SemaTypeId const dest_type = sema->inst_info_full(inst_idx).type3;
    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    yy_assert(typeIsInteger(sema, src_type));
    yy_assert(typeIsInteger(sema, dest_type));
    yy_assert(dest_size >= src_size);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load src_value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, src_value)));

    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, instMcv(parent_fn, src_value), src_type);
    referenceInstMcv(codegen, parent_fn, src_value, inst_idx);

    // TODO: If integer is signed, we must sign promote the integer: movsx reg[dest_size], reg[src_size]
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; (nop) perform int promotion to %" PRIu64 " bytes\n", dest_size);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, tmp_reg));
    regalloc_free(&parent_fn->reg_alloc, tmp_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irPtrCast(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__ptr_cast);
    auto const extra = *genir_extraPl(gi, inst_idx, ptr_cast);

    InstIndex const src_value = genir_instAbsSB(inst_idx, extra.src_value);
    SemaTypeId const src_type = sema->inst_info_full(src_value).type3;
    SemaTypeId const dest_type = sema->inst_info_full(inst_idx).type3;
    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    yy_assert(typeIsPointer(sema, src_type));
    yy_assert(typeIsPointer(sema, dest_type));
    yy_assert(dest_size == src_size);
    yy_assert(dest_size * 8 == 64);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load src_value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, src_value)));

    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, instMcv(parent_fn, src_value), src_type);
    referenceInstMcv(codegen, parent_fn, src_value, inst_idx);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, tmp_reg));
    regalloc_free(&parent_fn->reg_alloc, tmp_reg);

    return Yy_Ok;
}
NODISCARD static YyStatus irIntToPtr(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__int_to_ptr);
    auto const extra = *genir_extraPl(gi, inst_idx, int_to_ptr);

    InstIndex const src_value = genir_instAbsSB(inst_idx, extra.src_value);
    SemaTypeId const src_type = sema->inst_info_full(src_value).type3;
    SemaTypeId const dest_type = sema->inst_info_full(inst_idx).type3;
    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    yy_assert(typeIsInteger(sema, src_type));
    yy_assert(typeIsPointer(sema, dest_type));
    yy_assert(dest_size == src_size);
    yy_assert(dest_size * 8 == 64);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load src_value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, src_value)));

    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, instMcv(parent_fn, src_value), src_type);
    referenceInstMcv(codegen, parent_fn, src_value, inst_idx);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, tmp_reg));
    regalloc_free(&parent_fn->reg_alloc, tmp_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irPtrToInt(
    Yy_CodegenX64 *const codegen, Function *const parent_fn, InstIndex const parent_block_inst_idx,
    InstIndex const inst_idx)
{
    (void) parent_fn;
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__ptr_to_int);
    auto const pl = *genir_smallPl(gi, inst_idx, ptr_to_int);

    InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    SemaTypeId const src_type = sema->inst_info_full(src_value).type3;
    SemaTypeId const dest_type = sema->inst_info_full(inst_idx).type3;
    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    yy_assert(typeIsPointer(sema, src_type));
    yy_assert(typeIsInteger(sema, dest_type));
    yy_assert(dest_size == src_size);
    yy_assert(dest_size * 8 == 64);

    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load src_value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, src_value)));

    Register const tmp_reg = regalloc_alloc_any(&parent_fn->reg_alloc);

    genMcvMoveIntoStringRegister(codegen, parent_fn, tmp_reg, instMcv(parent_fn, src_value), src_type);
    referenceInstMcv(codegen, parent_fn, src_value, inst_idx);

    storeInstMcv(codegen, parent_fn, inst_idx, MetaTu_init(McValue, reg, tmp_reg));
    regalloc_free(&parent_fn->reg_alloc, tmp_reg);

    return Yy_Ok;
}

NODISCARD static YyStatus irLocalVarDecl(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    (void) parent_block_inst_idx;

    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_var_decl);
    Yy_Ir_Inst_ExtraPl_LocalVarDecl const extra = *genir_extraPl(gi, inst_idx, local_var_decl);
    InstIndex const initial_value = genir_instAbsSB(inst_idx, extra.initial_value);

    // Load the initial value into r12.
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; load initial value " IOF_Fmt "\n", IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, initial_value)));

    // Allocate the local variable
    SemaTypeId const decl_type = sema->inst_info_full(inst_idx)
                                     .specific.as_decl_like()
                                     .unwrap()
                                     .resolved_decl_type.unwrap();
    u64 const type_mem_align_of = 8; // TODO typeIdMemoryAlignOf
    u64 const type_mem_size_of = typeIdMemorySizeOf(sema, decl_type);
    u64 const this_frame_offset = allocPermanentStackAlignSize(parent_fn, type_mem_align_of, type_mem_size_of);
    parent_fn->local_var_stack_map.put_no_clobber_emplace({}, inst_idx, this_frame_offset);

    // Store the initial value into the variable..
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "; store initial value\n");
    genMcvMove(codegen, parent_fn, MetaTu_init(McValue, local_var_value, inst_idx), instMcv(parent_fn, initial_value), decl_type);
    referenceInstMcv(codegen, parent_fn, initial_value, inst_idx);

    yy_assert(codegen->sema->inst_info_full(inst_idx).build_last_ref.is_none());

    return Yy_Ok;
}

NODISCARD static YyStatus irBlockElementOne(
    Yy_CodegenX64 *codegen, Function *parent_fn, InstIndex parent_block_inst_idx,
    InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    auto inst_name = MetaEnum_sv_short(Yy_Ir_Inst_Tag, gi->insts[inst_idx].tag);
    ZoneScoped;
    ZoneName((char const*)inst_name.ptr, inst_name.len);
    ZoneValue(parent_fn->fn_decl_inst.x);
    auto fn_body_inst = genir_getFnDeclBodyInst(gi, parent_fn->fn_decl_inst);
    ZoneValue(inst_idx.x - fn_body_inst.x);
    Inst const *const inst = &gi->insts[inst_idx];
    logDebug("%s: " IOF_Fmt " " Sv_Fmt "", __func__, IOF_Arg(genir_instOffsetFunction(fn_body_inst, inst_idx)), Sv_Arg(inst_name));
    if (asm_comments)
        array_list_printf(&codegen->asm_text, "\n; " IOF_Fmt " " Sv_Fmt "\n", IOF_Arg(genir_instOffsetFunction(fn_body_inst, inst_idx)), Sv_Arg(inst_name));
    switch (inst->tag) {
        case MetaEnum_count(Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__fn_decl:
        case Yy_Ir_Inst_Tag__struct_decl:
        case Yy_Ir_Inst_Tag__global_var_decl:
        case Yy_Ir_Inst_Tag__extern_var_decl:
        case Yy_Ir_Inst_Tag__extern_fn_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__local_var_decl:
            return irLocalVarDecl(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__block:
            return irBlock(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__loop:
            return irLoop(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__cond_branch:
            return irCondBranch(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__break_void:
            return irBreakVoid(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__break_value:
            return irBreakValue(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__return_void:
            return irReturnVoid(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__return_value:
            return irReturnValue(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__xunreachable:
            return irXunreachable(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__analyzed_field_val:
            return irAnalyzedFieldVal(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
            return irAnalyzedFieldRef(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__decl_val:
            return irDeclVal(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__decl_ref:
            return irDeclRef(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__local_decl_val:
            return irLocalDeclValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, false);
        case Yy_Ir_Inst_Tag__local_decl_ref:
            return irLocalDeclValOrRef(codegen, parent_fn, parent_block_inst_idx, inst_idx, true);
        case Yy_Ir_Inst_Tag__call:
            return irCall(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__address_of:
            return irAddressOf(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__deref_ref:
            return irDerefRef(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__deref_val:
            return irDerefVal(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__discard:
            return irDiscard(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__placeholder_ref:
            return irPlaceholderRef(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__assign:
            return irAssign(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__ptr_add_int:
            return irPtrAddInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__ptr_sub_int:
            return irPtrSubInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
            return irPtrSubPtr(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__add:
            return irBinaryOperator_add(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__sub:
            return irBinaryOperator_sub(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__mul:
            return irBinaryOperator_mul(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__add_wrap:
            return irBinaryOperator_add_wrap(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__sub_wrap:
            return irBinaryOperator_sub_wrap(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__mul_wrap:
            return irBinaryOperator_mul_wrap(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__div:
            return irBinaryOperator_div(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__mod:
            return irBinaryOperator_mod(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__shl:
            return irBinaryOperator_shl(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__shr:
            return irBinaryOperator_shr(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bits_and:
            return irBinaryOperator_bits_and(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bits_or:
            return irBinaryOperator_bits_or(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bits_xor:
            return irBinaryOperator_bits_xor(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_eq:
            return irBinaryOperator_bool_eq(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_noteq:
            return irBinaryOperator_bool_noteq(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_lt:
            return irBinaryOperator_bool_lt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_lte:
            return irBinaryOperator_bool_lte(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_gt:
            return irBinaryOperator_bool_gt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_gte:
            return irBinaryOperator_bool_gte(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bool_not:
            return irUnaryOperator_bool_not(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__bits_not:
            return irUnaryOperator_bits_not(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__const_undefined:
            return irConstUndefined(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__const_int:
            return irConstInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__const_cstring:
            return irConstCstring(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__const_bool:
            return irConstBool(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__fn_arg_decl:
            return irFnArgDecl(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
            return irCallBuiltinPrintUint(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
            return irCallBuiltinPrintHelloWorld(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__truncate_int:
            return irTruncateInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__promote_int:
            return irPromoteInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__ptr_cast:
            return irPtrCast(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__int_to_ptr:
            return irIntToPtr(codegen, parent_fn, parent_block_inst_idx, inst_idx);
        case Yy_Ir_Inst_Tag__ptr_to_int:
            return irPtrToInt(codegen, parent_fn, parent_block_inst_idx, inst_idx);
    }
    yy_unreachable();
}

NODISCARD static YyStatus irFnBody(
    Yy_CodegenX64 *codegen, Function *parent_fn)
{
    GenIr *const gi = codegen->sema->gi;
    Sema *const sema = codegen->sema;

    InstIndex const body_inst_idx = genir_getFnDeclBodyInst(gi, parent_fn->fn_decl_inst);
    Inst const *const body = &codegen->sema->gi->insts[body_inst_idx];
    yy_assert(body->tag == Yy_Ir_Inst_Tag__block);

    InstIndex const fn_decl_inst = parent_fn->fn_decl_inst;

    Yy_Ir_Inst_ExtraPl_FnDecl const fn_decl_data = *genir_extraPl(gi, fn_decl_inst, fn_decl);

    // 1. Allocate locals in rsp.
    // TODO: align rsp and rbp to the stack alignment.
    array_list_printf(&codegen->asm_text, "push rbp\n");
    array_list_printf(&codegen->asm_text, "mov rbp, rsp\n");
    array_list_printf(&codegen->asm_text, "sub rsp, QWORD [_y_fn_%u_stack_size]\n", fn_decl_inst.x);

    // 2. The callee-saved registers are RBX, RBP, and R12 through R15 (RSP will also be preserved by
    // the call convention, but need not be pushed on the stack during this step).
    // TODO: Don't save registers which aren't used.
    Register const regs_to_save[] = { Register__rbx, Register__r12, Register__r13, Register__r14, Register__r15 };
    saveRegisters(codegen, parent_fn, regs_to_save, ARRAY_COUNT(regs_to_save));

    // The six first parameters come in registers, while the rest are pushed onto the stack in reverse order.
    // TODO: More than six parameters.
    if (fn_decl_data.arg_count > ARRAY_COUNT(c_callconv_arg_regs)) {
        codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, fn_decl_inst), "TODO: more than six parameters");
        return Yy_Bad;
    }

    // Store arguments into the locals.
    for (u32 arg_idx = 0; arg_idx < fn_decl_data.arg_count; arg_idx += 1) {
        InstIndex const arg_decl_inst = body_inst_idx + 1 + arg_idx;
        yy_assert(gi->insts[arg_decl_inst].tag == Yy_Ir_Inst_Tag__fn_arg_decl);
        yy_assert(gi->insts[arg_decl_inst].small_data.fn_arg_decl.arg_idx == arg_idx);

        SemaTypeId const param_type = sema->inst_info_full(arg_decl_inst)
                                          .specific.as_decl_like()
                                          .unwrap()
                                          .resolved_decl_type.unwrap();

        u64 const param_type_mem_align_of = 8; // TODO typeIdMemoryAlignOf
        u64 const param_type_mem_size_of = typeIdMemorySizeOf(sema, param_type);
        u64 const this_frame_offset = allocPermanentStackAlignSize(parent_fn, param_type_mem_align_of, param_type_mem_size_of);
        parent_fn->local_var_stack_map.put_no_clobber_emplace({}, arg_decl_inst, this_frame_offset);
        yy_assert(codegen->sema->inst_info_full(arg_decl_inst).build_last_ref.is_none());

        if (asm_comments)
            array_list_printf(&codegen->asm_text, "; arg %u into " IOF_Fmt "\n", arg_idx, IOF_Arg(genir_instOffsetFunction(parent_fn->fn_decl_inst, arg_decl_inst)));
        storeLocalVarOrParamValue(codegen, parent_fn, arg_decl_inst, c_callconv_arg_regs[arg_idx], Register__r12);
    }

    // 3. Function body.
    TRY(irBlock(codegen, parent_fn, fn_decl_inst, body_inst_idx));
    // Make a label for the return instruction just after the block ends.
    array_list_printf(&codegen->asm_text, "_y_fn_%u_return_label:\n", fn_decl_inst.x);

    // 4. Restore callee saved registers.
    restoreRegisters(codegen, parent_fn, regs_to_save, ARRAY_COUNT(regs_to_save));

    // 5. Deallocate locals, restoring rsp.
    // TODO: Corrupt this memory with 0xaa's for debugging reasons.
    array_list_printf(&codegen->asm_text, "add rsp, QWORD [_y_fn_%u_stack_size]\n", fn_decl_inst.x);
    array_list_printf(&codegen->asm_text, "pop rbp\n");

    // 6. Return. The return value was stored in rax, if there was any.
    {
        auto pl = genir_extraPl(gi, fn_decl_inst, fn_decl);
        auto const fn_name_z = pl->name_z;
        Sv const fn_name_sv = genir_identifier_sv(gi, fn_name_z);
        bool const is_main = sv_eql(fn_name_sv, sv_lit("main"));
        if (is_main) {
            auto fn_type = sema->inst_info_full(fn_decl_inst)
                               .specific.as_decl_like()
                               .unwrap()
                               .resolved_decl_type.unwrap();
            auto return_type = typeBase(sema, fn_type)->get<SemaTypePacked::Function>()->return_type;
            auto return_type_tag = decodeTypeTag(sema, return_type);
            switch (return_type_tag) {
                case MetaEnum_count(SemaType_Tag): yy_unreachable();
                case SemaType_Tag__uintword: break;
                case SemaType_Tag__xnoreturn: break;
                case SemaType_Tag__xvoid: {
                    array_list_printf(&codegen->asm_text, "xor rax, rax\n");
                } break;
                default: {
                    ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
                    array_list_printf(&msg_buf, "bad 'main' return type: expected 'uintword', 'void' or 'noreturn', got '");
                    fmtSemaTypeId(&msg_buf, sema, return_type);
                    array_list_printf(&msg_buf, "'");
                    codegen_x64_add_error_msg(codegen, MetaTu_init(Genir_ErrorLocation, inst, fn_decl_inst),
                                              "%.*s", intCastFromUsizeToU32(msg_buf.size()), msg_buf.data());
                    return Yy_Bad;
                } break;
            }
        }
    }

    array_list_printf(&codegen->asm_text, "ret\n");

    // TODO: lta bump max alignment
    u64 const lta_alignment = 8;
    u64 const total_stack_align = yy_max(parent_fn->local_vars_frame_align, lta_alignment);
    // TODO: Bigger stack alignments
    todo_assert(total_stack_align == c_callconv_stack_align);
    u64 const permanent_vars_size_aligned = yy_align_forward_power_of_two(total_stack_align, parent_fn->local_vars_frame_size);
    u64 const lta_size_aligned = yy_align_forward_power_of_two(total_stack_align, lta_peak_size(&parent_fn->lta));
    u64 const total_stack_size = permanent_vars_size_aligned + lta_size_aligned;
    yy_assert(yy_is_aligned_power_of_two(total_stack_align, total_stack_size));
    array_list_printf(&codegen->asm_text, "[segment .rodata]\n");
    array_list_printf(&codegen->asm_text, "; total_stack_align  = %" PRIu64 "\n", total_stack_align);
    array_list_printf(&codegen->asm_text, "; local_vars_frame_size unaligned = %" PRIu64 "\n", parent_fn->local_vars_frame_size);
    array_list_printf(&codegen->asm_text, "; lta peak size unaligned = %" PRIu64 "\n", lta_peak_size(&parent_fn->lta));
    array_list_printf(&codegen->asm_text, "_y_fn_%u_stack_size: dq %" PRIu64 "\n", fn_decl_inst.x, total_stack_size);
    array_list_printf(&codegen->asm_text, "[segment .text]\n");
    return Yy_Ok;
}

NODISCARD static YyStatus irFnDecl(Yy_CodegenX64 *codegen, InstIndex inst_idx)
{
    ZoneScoped;
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__fn_decl);

    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; fn $%u \"", inst_idx.x);
        writeLabelFnDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" begin {\n");
    }
    array_list_printf(&codegen->asm_text, "[global ");
    writeLabelFnDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, "]\n");
    writeLabelFnDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, ":\n");

    Function fn{codegen, inst_idx};

    TRY(irFnBody(codegen, &fn));
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; fn $%u \"", inst_idx.x);
        writeLabelFnDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" } end\n");
    }

    fn.deinit(); // TODO Leak on compiler error above

    return Yy_Ok;
}

NODISCARD static YyStatus irExternFnDecl(Yy_CodegenX64 *codegen, InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__extern_fn_decl);

    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; extern_fn_decl $%u \"", inst_idx.x);
        writeLabelExternFnDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" begin {\n");
    }
    array_list_printf(&codegen->asm_text, "[extern ");
    writeLabelExternFnDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, "]\n");
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; extern_fn_decl $%u \"", inst_idx.x);
        writeLabelExternFnDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" } end\n");
    }
    return Yy_Ok;
}

NODISCARD static YyStatus irGlobalVarDecl(Yy_CodegenX64 *codegen, InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__global_var_decl);

    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; global_var_decl $%u \"", inst_idx.x);
        writeLabelGlobalVarDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" begin {\n");
    }
    array_list_printf(&codegen->asm_text, "[segment .data]\n");
    array_list_printf(&codegen->asm_text, "[global ");
    writeLabelGlobalVarDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, "]\n");
    writeLabelGlobalVarDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, ": ");

    auto const& sii_specific =
        codegen->sema->inst_info_full(inst_idx).specific.as_decl_like().unwrap();
    auto const init_value_id = sii_specific.comptime_value_for_global_var_decl_init.unwrap();
    auto const& init_value = codegen->sema->comptime_value_pool[init_value_id];
    u64 const actual_type_size =
        typeIdMemorySizeOf(codegen->sema, sii_specific.resolved_decl_type.unwrap());

    switch (init_value.tag) {
        case MetaEnum_count(ComptimeValue_Tag):
            yy_unreachable();
        case ComptimeValue_Tag__xnoreturn:
            yy_unreachable();
        case ComptimeValue_Tag__type:
            ice();
        case ComptimeValue_Tag__xundefined: {
            if (actual_type_size > 0) {
                for (u64 i = 0; i < actual_type_size; i++) {
                    array_list_printf(&codegen->asm_text, "db 0xaa\n");
                }
            } else {
                array_list_printf(&codegen->asm_text, "db 0\n");
            }
        } break;
        case ComptimeValue_Tag__xvoid: {
            array_list_printf(&codegen->asm_text, "db 0\n");
        } break;
        case ComptimeValue_Tag__integer: {
            auto const& integer = init_value.as<ComptimeValue_Tag__integer>().unwrap();
            // TODO: Different integer types, big endian.
            yy_assert(actual_type_size <= 8);
            array_list_printf(&codegen->asm_text, "dq %" PRIi64 "\n", integer.value);
        } break;
        case ComptimeValue_Tag__xbool: {
            auto value = init_value.as<ComptimeValue_Tag__xbool>().unwrap();
            u64 const init_integer = value ? 1 : 0;
            array_list_printf(&codegen->asm_text, "dq %" PRIu64 "\n", init_integer);
        } break;
        case ComptimeValue_Tag__string: {
            // This global is a pointer to the data.
            auto cstring_label = inst_idx;
            array_list_printf(&codegen->asm_text, "dq ");
            writeConstCstringLabel(codegen, cstring_label);
            array_list_printf(&codegen->asm_text, "\n");

            // Define the string label with the contents.
            auto string = init_value.as<ComptimeValue_Tag__string>().unwrap();
            genDefineConstCstring(codegen, cstring_label, string.string);
        }
        break;
    }

    array_list_printf(&codegen->asm_text, "[segment .text]\n");
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; global_var_decl $%u \"", inst_idx.x);
        writeLabelGlobalVarDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" } end\n");
    }
    return Yy_Ok;
}

NODISCARD static YyStatus irExternVarDecl(Yy_CodegenX64 *codegen, InstIndex inst_idx)
{
    GenIr *const gi = codegen->sema->gi;
    Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__extern_var_decl);

    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; extern_var_decl $%u \"", inst_idx.x);
        writeLabelExternVarDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" begin {\n");
    }
    array_list_printf(&codegen->asm_text, "[extern ");
    writeLabelExternVarDecl(codegen, inst_idx);
    array_list_printf(&codegen->asm_text, "]\n");
    if (asm_comments) {
        array_list_printf(&codegen->asm_text, "\n\n; extern_var_decl $%u \"", inst_idx.x);
        writeLabelExternVarDecl(codegen, inst_idx);
        array_list_printf(&codegen->asm_text, "\" } end\n");
    }
    return Yy_Ok;
}

NODISCARD static auto irDeclOne(Yy_CodegenX64* const codegen, DeclId const decl) -> YyStatus {
    Sema* const sema = codegen->sema;
    GenIr* const gi = sema->gi;
    auto const decl_ptr = gi->decl_ptrs[decl];
    yy_assert(!decl_ptr->has_any_hard_errors());
    auto const inst_idx = decl_ptr->inst;
    auto const inst_tag = genir_instsAt(gi, inst_idx)->tag;
    logDebug(
        "%s: $%u / " DeclId_Fmt " " Sv_Fmt "", __func__, inst_idx.x, DeclId_Arg(decl),
        Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst_tag))
    );
    switch (inst_tag) {
        case MetaEnum_count(Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__local_var_decl:
        case Yy_Ir_Inst_Tag__fn_arg_decl:
        case Yy_Ir_Inst_Tag__block:
        case Yy_Ir_Inst_Tag__loop:
        case Yy_Ir_Inst_Tag__cond_branch:
        case Yy_Ir_Inst_Tag__break_void:
        case Yy_Ir_Inst_Tag__break_value:
        case Yy_Ir_Inst_Tag__return_void:
        case Yy_Ir_Inst_Tag__return_value:
        case Yy_Ir_Inst_Tag__xunreachable:
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
        case Yy_Ir_Inst_Tag__truncate_int:
        case Yy_Ir_Inst_Tag__promote_int:
        case Yy_Ir_Inst_Tag__ptr_cast:
        case Yy_Ir_Inst_Tag__int_to_ptr:
        case Yy_Ir_Inst_Tag__ptr_to_int:
        case Yy_Ir_Inst_Tag__const_cstring:
        case Yy_Ir_Inst_Tag__const_int:
        case Yy_Ir_Inst_Tag__const_bool:
        case Yy_Ir_Inst_Tag__const_undefined:
        case Yy_Ir_Inst_Tag__placeholder_ref:
        case Yy_Ir_Inst_Tag__discard:
        case Yy_Ir_Inst_Tag__decl_val:
        case Yy_Ir_Inst_Tag__decl_ref:
        case Yy_Ir_Inst_Tag__local_decl_val:
        case Yy_Ir_Inst_Tag__local_decl_ref:
        case Yy_Ir_Inst_Tag__call:
        case Yy_Ir_Inst_Tag__address_of:
        case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val:
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__analyzed_field_val:
        case Yy_Ir_Inst_Tag__deref_ref:
        case Yy_Ir_Inst_Tag__deref_val:
        case Yy_Ir_Inst_Tag__assign:
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub_wrap:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte:
        case Yy_Ir_Inst_Tag__bool_not:
        case Yy_Ir_Inst_Tag__bits_not:
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__struct_decl:
            return Yy_Ok;
        case Yy_Ir_Inst_Tag__fn_decl:
            return irFnDecl(codegen, inst_idx);
        case Yy_Ir_Inst_Tag__extern_fn_decl:
            return irExternFnDecl(codegen, inst_idx);
        case Yy_Ir_Inst_Tag__global_var_decl:
            return irGlobalVarDecl(codegen, inst_idx);
        case Yy_Ir_Inst_Tag__extern_var_decl:
            return irExternVarDecl(codegen, inst_idx);
    }
}

NODISCARD static auto irAllDecls(Yy_CodegenX64* const codegen) -> YyStatus {
    Sema* const sema = codegen->sema;
    GenIr* const gi = sema->gi;
    auto const decl_count = gi->decl_ptrs.size();
    for (usize i = 0; i < decl_count; i++) {
        auto const decl = DeclId(i);
        TRY(irDeclOne(codegen, decl));
    }
    return Yy_Ok;
}

NODISCARD static YyStatus codegen_x64_irRoot(Yy_CodegenX64 *codegen)
{
    array_list_printf(&codegen->asm_text, "[bits 64]\n");
    array_list_printf(&codegen->asm_text, "[segment .text]\n");
    array_list_printf(&codegen->asm_text, "[extern yuceyuf_builtin_print_hello_world]\n");
    array_list_printf(&codegen->asm_text, "[extern yuceyuf_builtin_print_uint]\n");
    array_list_printf(&codegen->asm_text, "[extern _yuceyuf_memcpy]\n");
    array_list_printf(&codegen->asm_text, "[extern _yuceyuf_memset]\n");
    yy_assert(codegen->sema->gi->insts.size() > 0);
    TRY(irAllDecls(codegen));
    return Yy_Ok;
}

Yy_CodegenX64::~Yy_CodegenX64()
{
    static_assert(Yy_CodegenX64_struct_field_count == 5, "");
}

static_assert(Yy_CodegenX64_struct_field_count == 5, "");
Yy_CodegenX64::Yy_CodegenX64(Yy_Allocator heap, Sema* sema)
    : heap{heap}, arena{heap, 0}, sema{sema}, asm_text{heap}, errors{heap} {}

NODISCARD auto yy_codegen_x64_from_sema(
    Yy_Allocator const heap,
    Sema* const sema,
    Sv asm_out_filename
) -> Yy_CodegenX64 {
    Timer t;
    timer_init(&t);
    u64 lap;
    (void) lap;

    yy_assert(!sema->has_errors());
    Yy_CodegenX64 result{heap, sema};
    auto const ret_codegen = &result;

    {
        ZoneScopedN("codegen build asm");
        if (yy_is_err(codegen_x64_irRoot(ret_codegen))) {
            LOG_LAP(&t, lap, "codegen_x64_irRoot");
            yy_assert(ret_codegen->errors.msgs.size() > 0);
            logError("" Sv_Fmt ": error: codegen_x64_irRoot() failed", Sv_Arg(sema->gi->filename));
            goto final_error;
        }
    }
    LOG_LAP(&t, lap, "codegen_x64_irRoot");
    yy_assert(ret_codegen->errors.msgs.size() == 0);

    if (verbose_codegen) {
        logDebug("x64 asm text =\n\n");
        (void) fwrite(ret_codegen->asm_text.data(), ret_codegen->asm_text.size(), 1, stderr);
        LOG_LAP(&t, lap, "stderr asm text");
    }


    {
        ZoneScopedN("codegen asm out");
        ArrayList_Of_u8 buf_filename{sema->scratch.allocator()};
        array_list_u8_write_sv(&buf_filename, asm_out_filename);
        char const *tmp_out_filename = array_list_u8_leak_into_cstr(&buf_filename);
        FILE *out_asm_file = fopen(tmp_out_filename, "wb");
        if (out_asm_file == NULL) {
            yy_panic_fmt("failed to open file '%s'", tmp_out_filename);
        }
        defer { fclose(out_asm_file); };
        usize fwrite_res = fwrite(ret_codegen->asm_text.data(), 1, ret_codegen->asm_text.size(), out_asm_file);
        if (fwrite_res != ret_codegen->asm_text.size()) {
            yy_panic_fmt("failed to fwrite file '%s'", tmp_out_filename);
        }
        LOG_LAP(&t, lap, "asm output file fopen,fwrite,fclose");
    }

    yy_assert(!result.errors.fatal);
    return result;
final_error:
    result.errors.fatal = true;
    return result;
}
