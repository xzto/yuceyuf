#include "yuceyuf/ast.hpp"
#include "yy/sv.hpp"
#include "yuceyuf/pretty_printer.hpp"

#include <stdio.h>

#include <ranges>

// TODO: Write a code formatter that takes input from stdin.
// TODO: Don't run `ast_resolve_symbols` before formatting.
// TODO: Pretty printer should preserve comments.

typedef struct {
    FILE *stream;
    u32 indentation;
} PrettyPrinter;

static const u32 indent_width = 4;

static void indent(PrettyPrinter *p)
{
    fprintf(p->stream, "%*s", (int)(p->indentation*indent_width), "");
}

static void pp(PrettyPrinter *p, Yuceyuf_AstNode *node, bool stmt_term);

static void pp_arg_list(PrettyPrinter *p, Yuceyuf_AstList args, bool args_has_trailing_comma)
{
    if (args_has_trailing_comma) {
        yy_assert(args.nodes.len > 0);
        fprintf(p->stream, "\n");
        p->indentation += 1;
    }
    for (Yuceyuf_AstNode **iter = args.nodes.begin(); iter != args.nodes.end(); ++iter) {
        auto arg = *iter;
        if (args_has_trailing_comma)
            indent(p);
        pp(p, arg, false);
        if (args_has_trailing_comma) {
            fprintf(p->stream, ",\n");
        } else if (iter + 1 == args.nodes.end()) {
            fprintf(p->stream, ", ");
        }
    }
    if (args_has_trailing_comma) {
        p->indentation -= 1;
        indent(p);
    }
}

static void pp_if(PrettyPrinter *p, Yuceyuf_AstNode_Specific_Payload_If const *eef, bool stmt_term)
{
    fprintf(p->stream, "if (");
    pp(p, eef->cond, false);
    fprintf(p->stream, ")");
    bool const then_is_stmt_block = eef->then->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block;
    bool const then_line_break = (eef->then->pos.line0 != eef->cond_rparen_pos.line0 && !then_is_stmt_block);
    if (then_line_break) {
        fprintf(p->stream, "\n");
        p->indentation += 1;
        indent(p);
    } else {
        fprintf(p->stream, " ");
    }
    pp(p, eef->then, stmt_term);

    if (then_line_break) {
        p->indentation -= 1;
    }

    if (eef->elze.has_value()) {
        auto elze = eef->elze.unwrap();
        bool const elze_is_stmt_block = elze->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block;
        if ((eef->then->end_pos.line0 != eef->elze_keyword_pos.line0 && !elze_is_stmt_block) ||
                (!then_is_stmt_block && stmt_term)) {
            fprintf(p->stream, "\n");
            indent(p);
        } else {
            fprintf(p->stream, " ");
        }

        bool const elze_line_break = (elze->pos.line0 != eef->elze_keyword_pos.line0 && !elze_is_stmt_block);
        if (elze_line_break) {
            fprintf(p->stream, "else\n");
            p->indentation += 1;
            indent(p);
        } else {
            fprintf(p->stream, "else ");
        }
        pp(p, elze, stmt_term);

        if (elze_line_break) {
            p->indentation -= 1;
        }
    }
}

static void pp_xwhile(PrettyPrinter *p, Yuceyuf_AstNode_Specific_Payload_While const *xwhile, bool stmt_term)
{
    fprintf(p->stream, "while (");
    pp(p, xwhile->cond, false);
    fprintf(p->stream, ")");
    if (xwhile->continue_stmt.has_value()) {
        fprintf(p->stream, " : (");
        pp(p, xwhile->continue_stmt.unwrap(), false);
        fprintf(p->stream, ")");
    }
    bool const body_is_stmt_block = xwhile->body->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block;
    bool const body_line_break = (xwhile->body->pos.line0 != xwhile->rparen_pos.line0 && !body_is_stmt_block);
    if (body_line_break) {
        fprintf(p->stream, "\n");
        p->indentation += 1;
        indent(p);
    } else {
        fprintf(p->stream, " ");
    }
    pp(p, xwhile->body, stmt_term);

    if (body_line_break) {
        p->indentation -= 1;
    }
}

/// Print a node.
static void pp(PrettyPrinter *p, Yuceyuf_AstNode *node, bool stmt_term)
{
    // NOTE: Do not indent at the start, do not put a new line at the end.
    switch (node->it.tag) {
        case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
            yy_unreachable();
        case Yuceyuf_AstNode_Specific_Tag__root: {
            yy_assert(stmt_term);
            Yuceyuf_AstList const top_list = node->it.payload.root.top_list;
            Yuceyuf_Lexer_TokenPosition last_top_end_pos = {};
            for (auto top : top_list.nodes) {
                if (top != top_list.nodes.ptr[0]) {
                    // Preserve empty line between tops.
                    if (top->pos.line0 > last_top_end_pos.line0 + 1) {
                        fprintf(p->stream, "\n");
                    }
                }
                yy_assert(p->indentation == 0);
                pp(p, top, true);
                last_top_end_pos = top->end_pos;
                fprintf(p->stream, "\n");
            }
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info:
        case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info:
            todo();
            break;
        case Yuceyuf_AstNode_Specific_Tag__fn_decl: {
            Sv const kw_fn_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__fn);
            Sv const fn_name = node->it.payload.fn_decl.name.original_text;
            indent(p);
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt "() ", Sv_Arg(kw_fn_sv), Sv_Arg(fn_name));
            if (true) yy_panic("TODO: pretty print function parameters");
            yy_assert(node->it.payload.fn_decl.body->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block);
            pp(p, node->it.payload.fn_decl.body, stmt_term);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__mod_decl: {
            if (true) yy_panic("TODO: pretty print mod");
            todo();
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__imported_file: {
            Sv const kw_import_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__ximport);
            Sv const decl_name = node->it.payload.imported_file.decl_name.original_text;
            Sv const file_name = node->it.payload.imported_file.file_name.original_text;
            indent(p);
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt " " Sv_Fmt "", Sv_Arg(kw_import_sv), Sv_Arg(decl_name), Sv_Arg(file_name));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__local_var_decl: {
            Sv const kw_var_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__xvar);
            Sv const var_name = node->it.payload.local_var_decl.name.original_text;
            indent(p);
            // "var name: type = initial_value;"
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt, Sv_Arg(kw_var_sv), Sv_Arg(var_name));
            if (node->it.payload.local_var_decl.ty_expr.has_value()) {
                fprintf(p->stream, ": ");
                pp(p, node->it.payload.local_var_decl.ty_expr.unwrap(), false);
            }
            fprintf(p->stream, " = ");
            pp(p, node->it.payload.local_var_decl.initial_value, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__global_var_decl: {
            Sv const kw_var_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__xvar);
            Sv const var_name = node->it.payload.global_var_decl.name.original_text;
            indent(p);
            // "var name: type = initial_value;"
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt, Sv_Arg(kw_var_sv), Sv_Arg(var_name));
            if (node->it.payload.global_var_decl.ty_expr.has_value()) {
                fprintf(p->stream, ": ");
                pp(p, node->it.payload.global_var_decl.ty_expr.unwrap(), false);
            }
            fprintf(p->stream, " = ");
            pp(p, node->it.payload.global_var_decl.initial_value, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_var_decl: {
            Sv const kw_var_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__xvar);
            Sv const kw_extern_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__xextern);
            Sv const var_name = node->it.payload.extern_var_decl.name.original_text;
            indent(p);
            // "extern var name;"
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt " " Sv_Fmt, Sv_Arg(kw_extern_sv), Sv_Arg(kw_var_sv), Sv_Arg(var_name));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl: {
            // TODO arguments
            todo();
            Sv const kw_fn_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__fn);
            Sv const kw_extern_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__xextern);
            Sv const fn_name = node->it.payload.extern_fn_decl.name.original_text;
            indent(p);
            // "extern fn name;"
            fprintf(p->stream, "" Sv_Fmt " " Sv_Fmt " " Sv_Fmt, Sv_Arg(kw_extern_sv), Sv_Arg(kw_fn_sv), Sv_Arg(fn_name));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__stmt_block: {
            fprintf(p->stream, "{");
            p->indentation += 1;
            Yuceyuf_AstList const stmts = node->it.payload.stmt_block.stmts;
            Yuceyuf_Lexer_TokenPosition last_stmt_end_pos = {};
            for (auto stmt : stmts.nodes) {
                fprintf(p->stream, "\n");
                // Preserve whitespace between statements.
                if (stmt != stmts.nodes.ptr[0] && stmt->pos.line0 > last_stmt_end_pos.line0 + 1) {
                    fprintf(p->stream, "\n");
                }
                indent(p);
                pp(p, stmt, true);
                last_stmt_end_pos = stmt->end_pos;
            }
            p->indentation -= 1;
            if (stmts.nodes.len > 0) {
                fprintf(p->stream, "\n");
                indent(p);
            }
            fprintf(p->stream, "}");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__labeled_block: {
            fprintf(p->stream, "" Sv_Fmt ": {", Sv_Arg(node->it.payload.labeled_block.ident.original_text));
            p->indentation += 1;
            Yuceyuf_AstList const stmts = node->it.payload.labeled_block.stmts;
            Yuceyuf_Lexer_TokenPosition last_stmt_end_pos = {};
            for (auto stmt: stmts.nodes) {
                fprintf(p->stream, "\n");
                // Preserve whitespace between statements.
                if (stmt != stmts.nodes.ptr[0] && stmt->pos.line0 > last_stmt_end_pos.line0 + 1) {
                    fprintf(p->stream, "\n");
                }
                indent(p);
                pp(p, stmt, true);
                last_stmt_end_pos = stmt->end_pos;
            }
            p->indentation -= 1;
            if (stmts.nodes.len > 0) {
                fprintf(p->stream, "\n");
                indent(p);
            }
            fprintf(p->stream, "}");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__return_void: {
            fprintf(p->stream, "return");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__return_expr: {
            fprintf(p->stream, "return ");
            pp(p, node->it.payload.return_expr.expr, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__xcontinue: {
            fprintf(p->stream, "continue");
            auto pl = &node->it.payload.xcontinue;
            if (pl->label.has_value())
                fprintf(p->stream, " :" Sv_Fmt "", Sv_Arg(pl->label.unwrap()->payload.identifier.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__break_void: {
            fprintf(p->stream, "break");
            auto pl = &node->it.payload.break_void;
            if (pl->label.has_value())
                fprintf(p->stream, " :" Sv_Fmt "", Sv_Arg(pl->label.unwrap()->payload.identifier.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__break_expr: {
            fprintf(p->stream, "break ");
            auto pl = &node->it.payload.break_expr;
            if (pl->label.has_value())
                fprintf(p->stream, ":" Sv_Fmt " ", Sv_Arg(pl->label.unwrap()->payload.identifier.original_text));
            pp(p, pl->expr, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call: {
            bool const args_has_trailing_comma = node->it.payload.builtin_fn_call.args_has_trailing_comma;
            Yuceyuf_AstList const args = node->it.payload.builtin_fn_call.args;
            Sv const bfn_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenBuiltinFn, node->it.payload.builtin_fn_call.bfn);
            fprintf(p->stream, "@" Sv_Fmt "(", Sv_Arg(bfn_sv));
            pp_arg_list(p, args, args_has_trailing_comma);
            fprintf(p->stream, ")");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__fn_call: {
            bool const args_has_trailing_comma = node->it.payload.fn_call.args_has_trailing_comma;
            Yuceyuf_AstList const args = node->it.payload.fn_call.args;
            pp(p, node->it.payload.fn_call.func, false);
            fprintf(p->stream, "(");
            pp_arg_list(p, args, args_has_trailing_comma);
            fprintf(p->stream, ")");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__identifier: {
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.identifier.identifier.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__field_access: {
            pp(p, node->it.payload.field_access.container, false);
            fprintf(p->stream, ".");
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.field_access.field.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__mod_field_access: {
            pp(p, node->it.payload.mod_field_access.container, false);
            fprintf(p->stream, "::");
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.mod_field_access.field.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__deref: {
            pp(p, node->it.payload.deref, false);
            fprintf(p->stream, ".*");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__builtin_typ: {
            Sv const typ_sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, node->it.payload.builtin_typ);
            fprintf(p->stream, Sv_Fmt, Sv_Arg(typ_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__paren: {
            fprintf(p->stream, "(");
            pp(p, node->it.payload.paren, false);
            fprintf(p->stream, ")");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__unreach: {
            Sv const sv = MetaEnum_sv_short(Yuceyuf_Lexer_TokenKeyword, Yuceyuf_Lexer_TokenKeyword__unreach);
            fprintf(p->stream, Sv_Fmt, Sv_Arg(sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__integer_literal: {
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.integer_literal.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__char_literal: {
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.char_literal.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__string_literal: {
            fprintf(p->stream, Sv_Fmt, Sv_Arg(node->it.payload.string_literal.original_text));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__bool_literal: {
            fprintf(p->stream, "%s", node->it.payload.bool_literal ? "true" : "false");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__undefined_literal: {
            fprintf(p->stream, "%s", "undefined");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__ignore_result: {
            fprintf(p->stream, "_ = ");
            pp(p, node->it.payload.ignore_result, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix: {
            Sv const op_str = MetaEnum_sv_short(Yuceyuf_Ast_UnaryOp, node->it.payload.op_unary_prefix.tag);
            fprintf(p->stream, Sv_Fmt, Sv_Arg(op_str));
            pp(p, node->it.payload.op_unary_prefix.operand, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__assign: {
            pp(p, node->it.payload.assign.lhs, false);
            fprintf(p->stream, " = ");
            pp(p, node->it.payload.assign.rhs, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__assign_bin_op: {
            Sv const op_str = MetaEnum_sv_short(Yuceyuf_Ast_BinaryOp, node->it.payload.assign_bin_op.op);
            pp(p, node->it.payload.assign_bin_op.lhs, false);
            fprintf(p->stream, " " Sv_Fmt "= ", Sv_Arg(op_str));
            pp(p, node->it.payload.assign_bin_op.rhs, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__op_bin: {
            Sv const op_str = MetaEnum_sv_short(Yuceyuf_Ast_BinaryOp, node->it.payload.op_bin.op);
            pp(p, node->it.payload.op_bin.lhs, false);
            fprintf(p->stream, " " Sv_Fmt " ", Sv_Arg(op_str));
            pp(p, node->it.payload.op_bin.rhs, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__expr_if: {
            pp_if(p, &node->it.payload.expr_if, false);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__stmt_if: {
            pp_if(p, &node->it.payload.stmt_if, stmt_term);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__xwhile: {
            pp_xwhile(p, &node->it.payload.xwhile, stmt_term);
        }
        break;
    }
    if (stmt_term && yuceyuf_ast_node_requires_stmt_terminator(node->it.tag))
        fprintf(p->stream, ";");
}

void pretty_print_ast(Yuceyuf_AstNode *root, FILE *stream)
{
    PrettyPrinter p = {
        .stream = stream,
        .indentation = 0,
    };
    pp(&p, root, true);
}
