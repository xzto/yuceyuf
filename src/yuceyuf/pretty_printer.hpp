#ifndef ___INCLUDE_GUARD__ZIFcms35BNAqOKrsO7i6HddEC2A9zlPWGAJkWXyo
#define ___INCLUDE_GUARD__ZIFcms35BNAqOKrsO7i6HddEC2A9zlPWGAJkWXyo

#include "yuceyuf/ast.hpp"

#include <stdio.h>

void pretty_print_ast(Yuceyuf_AstNode *root, FILE *stream);

#endif//___INCLUDE_GUARD__ZIFcms35BNAqOKrsO7i6HddEC2A9zlPWGAJkWXyo
