#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/sv.hpp"
#include "yy/vec.hpp"
#include "yuceyuf/path_builder.hpp"

#include <string.h>


static bool is_path_sep(char const x)
{
    // TODO: Separate between windows and unix.
    u8 const path_sep = '/';
    return x == path_sep;
}

struct PathBuilder {
    // The path nodes themselves don't own any memory.
    Vec<Sv> nodes;
    PathBuilder(yy::Allocator allocator) : nodes{allocator} {}
};

// Doesn't allocate any memory.
NODISCARD PathIterator path_iter_init(Sv path)
{
    return PathIterator {
        .path = path,
        .idx = 0,
    };
}

static void skip_path_sep(Sv path, usize *const i)
{
    while (*i < path.len && is_path_sep(path.ptr[*i]))
        *i += 1;
}

static void skip_not_path_sep(Sv path, usize *const i)
{
    while (*i < path.len && !is_path_sep(path.ptr[*i]))
        *i += 1;
}

// (empty) => (empty)
// /a/b/c => /, a, b, c
// a/b/c => a, b, c
// a/b/c/ => a, b, c/
// a/b/.././c/ => a, b, "..", ".", c/
// / => /
// . => .
// It will trim multiple slashes.
// Return true on success. Return false when it ends.
// Doesn't allocate any memory.
NODISCARD bool path_iter_next(PathIterator *iter, Sv *result)
{
    yy_debug_invalidate_ptr_one(result);
    if (iter->idx == iter->path.len) {
        yy_debug_invalidate_ptr_one(iter);
        return false;
    }

    usize start = iter->idx;
    usize i = start;
    if (start == 0) {
        if (is_path_sep(iter->path.ptr[start])) {
            *result = sv_slice(iter->path, start, start + 1);
            skip_path_sep(iter->path, &i);
            iter->idx = i;
            yy_assert(result->len > 0);
            return true;
        }
    }

    yy_assert(!is_path_sep(iter->path.ptr[start]));

    skip_not_path_sep(iter->path, &i);
    *result = sv_slice(iter->path, start, i);
    skip_path_sep(iter->path, &i);
    iter->idx = i;
    if (i == iter->path.len && i - start != result->len) {
        *result = sv_slice(iter->path, start, start + result->len + 1);
    }
    yy_assert(result->len > 0);
    return true;
}

NODISCARD u32 path_count_nodes(Sv path)
{
    yy_assert(path.len > 0);
    u32 result = 0;
    PathIterator path_iter = path_iter_init(path);
    Sv node;
    while (path_iter_next(&path_iter, &node)) {
        (void) node;
        result += 1;
    }
    yy_assert(result > 0);
    return result;
}


/// `..` will be appended to the builder.
static void path_builder_add_node_raw(PathBuilder *builder, Sv node)
{
    yy_assert(node.len > 0);
    if (node.len > 0 && is_path_sep(node.ptr[0])) {
        yy_assert(node.len == 1 && builder->nodes.size() == 0);
    }
    if (builder->nodes.size() > 1) {
        Sv const last_node = builder->nodes.back();
        yy_assert(!is_path_sep(last_node.back()));
    }
    builder->nodes.push_back(node);
}

/// `..` will remove one path node.
/// `fixed_parent_node_count` is a fixed amount of path nodes. If a `..` is found it checks that
/// it won't remove `fixed_parent_node_count` nodes.
static YyStatus path_builder_add_node(PathBuilder *builder, Sv node, u32 fixed_parent_node_count)
{
    if (sv_eql(node, sv_lit("."))) {
        return Yy_Ok;
    }
    if (sv_eql(node, sv_lit(".."))) {
        if (builder->nodes.size() > fixed_parent_node_count) {
            yy_assert(builder->nodes.size() > 0);
            Sv const last_node = builder->nodes.back();
            yy_assert(!is_path_sep(last_node.back()));
            yy_assert(!sv_eql(last_node, sv_lit(".")));
            yy_assert(!sv_eql(last_node, sv_lit("..")));
            (void) builder->nodes.pop_back();
            return Yy_Ok;
        }
        return Yy_Bad;
    }
    path_builder_add_node_raw(builder, node);
    return Yy_Ok;
}

// Return the length that would be returned if you call `path_builder_collect_buffer`.
static usize path_builder_collect_length(PathBuilder *builder)
{
    usize final_len = 0;
    for (usize node_idx = 0; node_idx < builder->nodes.size(); node_idx++) {
        Sv const node = builder->nodes[node_idx];
        // Add the size of this component.
        final_len += node.len;
        // Add the path separator if it's not already present.
        // yy_assert(path_separator.len == 1)
        if (node_idx != builder->nodes.size()-1 && !is_path_sep(node[node.size() - 1]))
            final_len += 1;
        if (node_idx != 0)
            yy_assert(node.len > 0 && !is_path_sep(node[0]));
    }
    // TODO: Review what should happen to a zero-sized path here. The current code that uses `path_builder` (2021-11-15) always uses the builder to build a _file_ name, not a _directory_ name. Therefore, the assert is _here_ instead of in the outside code. If some future code were to use `path_builder` to make a directory name, an empty path should be acceptable and this entire file `path_builder.c` must be reviewed.
    yy_assert(final_len > 0);
    return final_len;
}

// Return the length. The length should be equal to `path_builder_collect_length(builder)`. Assert
// that it fits inside `buf_ptr` with `buf_capacity`.
static usize path_builder_collect_buffer(PathBuilder *builder, char *buf_ptr, usize buf_capacity)
{
    usize current_len = 0;
    for (usize node_idx = 0; node_idx < builder->nodes.size(); node_idx++) {
        Sv const node = builder->nodes[node_idx];
        yy_assert(current_len + node.len <= buf_capacity);
        memcpy(&buf_ptr[current_len], node.ptr, node.len);
        current_len += node.len;
        if (node_idx != builder->nodes.size()-1 && !is_path_sep(node.ptr[node.len - 1])) {
            // TODO: Path separator for windows.
            u8 const path_sep = '/';
            yy_assert(current_len + 1 <= buf_capacity);
            buf_ptr[current_len] = path_sep;
            current_len += 1;
        }
    }
    yy_assert(current_len <= buf_capacity);
    // TODO: See comment in `path_builder_collect_length` about zero-sized paths.
    yy_assert(current_len > 0);
    return current_len;
}

UNUSED static Sv path_builder_collect_alloc(PathBuilder *builder, Yy_Allocator allocator)
{
    usize const buf_capacity = path_builder_collect_length(builder);
    char *const buf_ptr = allocator_alloc_array(allocator, char, buf_capacity);
    yy_assert(!Yy_Allocator_Result__is_failure(buf_ptr));
    usize const buf_len = path_builder_collect_buffer(builder, buf_ptr, buf_capacity);
    yy_assert(buf_len == buf_capacity);
    return Sv { buf_ptr, buf_len };
}

NODISCARD Sv path_with_single_slashes(
    Yy_Allocator result_allocator, Yy_Allocator scratch,
    Sv path_with_multiple_slashes)
{
    yy_assert(path_with_multiple_slashes.len > 0);

    // Allocate a buffer that can fit the result.
    auto buf = Vec<char>::init_capacity(result_allocator, path_with_multiple_slashes.len);

    // Build the path
    PathBuilder builder{scratch};
    {
        PathIterator path_iter = path_iter_init(path_with_multiple_slashes);
        Sv node;
        while (path_iter_next(&path_iter, &node)) {
            path_builder_add_node_raw(&builder, node);
        }
    }

    // Collect the built path in the allocated buffer.
    auto collected_size = path_builder_collect_buffer(&builder, buf.data(), buf.capacity());
    buf.unsafe_set_m_len(collected_size);

    // Shrink the buffer.
    Sv result = buf.leak_to_span();
    return result;
}

NODISCARD YyStatus join_paths_and_canonicalize(
    Yy_Allocator result_allocator, Yy_Allocator scratch,
    u32 fixed_parent_node_count, Sv parent, Sv next, Sv *result)
{
    yy_debug_invalidate_ptr_one(result);

    // NOTE: `result_allocator` and `scratch` may alias.
    if (next.len == 0)
        return Yy_Bad;
    if (is_path_sep(next.ptr[next.len - 1]))
        return Yy_Bad;
    bool const next_is_absolute = is_path_sep(next.ptr[0]);
    if (next_is_absolute)
        return Yy_Bad;

    yy_assert(next.len > 0);

    // Allocate a buffer that can fit the result.
    auto buf = Vec<char>::init_capacity(result_allocator, parent.len + next.len + 1);

    // Build the path
    PathBuilder builder{scratch};
    {
        Sv node;
        PathIterator path_iter;
        if (!next_is_absolute) {
            path_iter = path_iter_init(parent);
            while (path_iter_next(&path_iter, &node)) {
                path_builder_add_node_raw(&builder, node);
            }
        }

        path_iter = path_iter_init(next);
        while (path_iter_next(&path_iter, &node)) {
            if (yy_is_err(path_builder_add_node(&builder, node, fixed_parent_node_count))) {
                goto handle_error_deinit_builder;
            }
        }
    }

    // Collect the built path in the allocated buffer.
    buf.unsafe_set_m_len(path_builder_collect_buffer(&builder, buf.data(), buf.capacity()));

    // Shrink the buffer.
    *result = buf.leak_to_span();
    return Yy_Ok;

handle_error_deinit_builder:
    return Yy_Bad;
}


NODISCARD YyStatus path_to_cstr(Sv path, char *buf_ptr, usize buf_len)
{
    // Check if it fits in the buffer.
    if (path.len + 1 > buf_len)
        return Yy_Bad;
    // Check if the path has a '\0' in the middle.
    for (usize i = 0; i < path.len; i++) {
        if (path.ptr[i] == 0)
            return Yy_Bad;
    }
    // Copy into the buffer and zero terminate.
    memcpy(buf_ptr, path.ptr, path.len);
    buf_ptr[path.len] = 0;
    return Yy_Ok;
}

/// "foo/bar" -> Yy_Ok, "foo"
/// "foo//bar//" -> Yy_Ok, "foo"
/// "asdf" -> Yy_Ok, ""
/// "/" -> Yy_Bad
/// "" -> Yy_Bad;
NODISCARD YyStatus path_get_dirname(Sv path, Sv *result_dir)
{
    yy_assert(path.len > 0);

    u8 const sep = '/';
    enum {
        /// path ends with a separator
        State__before_finding_sep,
        /// searching for a separator
        State__finding_sep,
        /// skipping the separator after it was found
        State__skip_sep_and_return,
    } state = (path.ptr[path.len - 1] == sep) ? State__before_finding_sep : State__finding_sep;

    usize i = path.len;
    while (i > 0) {
        i -= 1;
        if (path.ptr[i] == sep) {
            switch (state) {
                case State__before_finding_sep:
                    continue;
                case State__finding_sep:
                    state = State__skip_sep_and_return;
                    break;
                case State__skip_sep_and_return:
                    continue;
            }
        } else {
            switch (state) {
                case State__before_finding_sep:
                    state = State__finding_sep;
                    break;
                case State__finding_sep:
                    continue;
                case State__skip_sep_and_return:
                    *result_dir = sv_slice(path, 0, i + 1);
                    return Yy_Ok;
            }
        }
    }
    switch (state) {
        case State__before_finding_sep:
            // `path` is the root directory.
            return Yy_Bad;
        case State__finding_sep:
            // path has no separator (cwd), return empty string
            *result_dir = sv_slice(path, 0, 0);
            return Yy_Ok;
        case State__skip_sep_and_return:
            // dirname of `path` is the root directory
            *result_dir = sv_slice(path, 0, i + 1);
            return Yy_Ok;
    }
    yy_unreachable();
}
