#ifndef ___INCLUDE_GUARD__Gcl1HitNINNZGYldTADYMHEOUJivVlrWHE68FQrS
#define ___INCLUDE_GUARD__Gcl1HitNINNZGYldTADYMHEOUJivVlrWHE68FQrS

#include "yy/basic.hpp"
#include "yy/option.hpp"
#include "yy/meta/enum.hpp"
#include "yuceyuf/lexer.hpp"



struct Yuceyuf_AstNode;
struct Yuceyuf_Ast_File;


struct Yuceyuf_AstList {
    VecUnmanagedLeaked<Yuceyuf_AstNode*> nodes;
};
static_assert(std::is_move_constructible_v<Yuceyuf_AstList>);
static_assert(std::is_move_assignable_v<Yuceyuf_AstList>);

struct Yuceyuf_AstRoot {
    Yuceyuf_AstList top_list;
};


typedef struct {
    Yuceyuf_AstNode *cond;
    Yuceyuf_AstNode *then;
    Option<Yuceyuf_AstNode*> elze;
    TokenId cond_rparen_token;
    Option<TokenId> elze_keyword_token;
} Yuceyuf_AstNode_Specific_Payload_If;

typedef struct {
    Yuceyuf_AstNode *cond;
    Yuceyuf_AstNode *body;
    Option<Yuceyuf_AstNode*> continue_stmt;
    Option<Yuceyuf_AstNode*> else_stmt;
} Yuceyuf_AstNode_Specific_Payload_While;

#define YY_META_ENUM__NAME Yuceyuf_Ast_BinaryOp
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS             \
    YY_META_ENUM__MEMBER2(bits_and,    "&")   \
    YY_META_ENUM__MEMBER2(bits_or,     "|")   \
    YY_META_ENUM__MEMBER2(bits_xor,    "^")   \
    YY_META_ENUM__MEMBER2(bool_and,    "and") \
    YY_META_ENUM__MEMBER2(bool_or,     "or")  \
    YY_META_ENUM__MEMBER2(bool_gt,     ">")   \
    YY_META_ENUM__MEMBER2(bool_gte,    ">=")  \
    YY_META_ENUM__MEMBER2(bool_lt,     "<")   \
    YY_META_ENUM__MEMBER2(bool_lte,    "<=")  \
    YY_META_ENUM__MEMBER2(bool_eq,     "==")  \
    YY_META_ENUM__MEMBER2(bool_noteq,  "!=")  \
    YY_META_ENUM__MEMBER2(sh_left,     "<<")  \
    YY_META_ENUM__MEMBER2(sh_right,    ">>")  \
    YY_META_ENUM__MEMBER2(add,         "+")   \
    YY_META_ENUM__MEMBER2(sub,         "-")   \
    YY_META_ENUM__MEMBER2(mul,         "*")   \
    YY_META_ENUM__MEMBER2(add_wrap,    "+%")  \
    YY_META_ENUM__MEMBER2(sub_wrap,    "-%")  \
    YY_META_ENUM__MEMBER2(mul_wrap,    "*%")  \
    YY_META_ENUM__MEMBER2(div,         "/")   \
    YY_META_ENUM__MEMBER2(mod,         "%")
#include "yy/meta/enum.hpp"

#define YY_META_ENUM__NAME Yuceyuf_Ast_UnaryOp
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS            \
    YY_META_ENUM__MEMBER2(ptr_to,      "*")  \
    YY_META_ENUM__MEMBER2(address_of,  "&")  \
    YY_META_ENUM__MEMBER2(plus,        "+")  \
    YY_META_ENUM__MEMBER2(plus_wrap,   "+%") \
    YY_META_ENUM__MEMBER2(minus,       "-")  \
    YY_META_ENUM__MEMBER2(minus_wrap,  "-%") \
    YY_META_ENUM__MEMBER2(bool_not,    "!")  \
    YY_META_ENUM__MEMBER2(bits_not,    "~")
#include "yy/meta/enum.hpp"

typedef struct {
    IdentifierId name;
    Option<Yuceyuf_AstNode*> ty_expr;
    Yuceyuf_AstNode* initial_value;
} Yuceyuf_AstNode_Specific_Payload_GlobalVarDecl;

typedef struct {
    IdentifierId name;
    Yuceyuf_AstNode* ty_expr;
} Yuceyuf_AstNode_Specific_Payload_ExternVarDecl;

typedef struct {
    Option<IdentifierId> name;
    Yuceyuf_AstNode *type_expr;
} Yuceyuf_AstNode_Specific_Payload_ExternFnDeclArgInfo;

typedef struct {
    IdentifierId name;
    Yuceyuf_AstNode *return_type;
    // all args are `extern_fn_decl_arg`
    Yuceyuf_AstList args;
} Yuceyuf_AstNode_Specific_Payload_ExternFnDecl;

typedef struct {
    Option<IdentifierId> name;
    Yuceyuf_AstNode* ty_expr;
} Yuceyuf_AstNode_Specific_Payload_FnDeclArgInfo;

typedef struct {
    IdentifierId name;
    Yuceyuf_AstNode *return_type;
    // body: stmt_block
    Yuceyuf_AstNode *body;
    // all args are `fn_decl_arg`
    Yuceyuf_AstList args;
} Yuceyuf_AstNode_Specific_Payload_FnDecl;

typedef struct {
    IdentifierId name;
    Option<Yuceyuf_AstNode*> ty_expr;
    Yuceyuf_AstNode *initial_value;
} Yuceyuf_AstNode_Specific_Payload_LocalVarDecl;

typedef struct {
    IdentifierId name;
    Yuceyuf_AstList contents;
} Yuceyuf_AstNode_Specific_Payload_ModDecl;

struct Yuceyuf_AstNode_Specific_Payload_StructDeclMemberInfo {
    Option<IdentifierId> name;
    Yuceyuf_AstNode* ty_expr;
};
struct Yuceyuf_AstNode_Specific_Payload_StructDecl {
    IdentifierId name;
    Yuceyuf_AstList members;
};

#define YY_META_TAGGED_UNION__NAME Yuceyuf_AstNode_Specific
#define YY_META_ENUM__INT u8
#define YY_META_TAGGED_UNION__ALL_MEMBERS      \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstRoot, root) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_FnDeclArgInfo, fn_decl_arg_info) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_FnDecl, fn_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_ExternFnDeclArgInfo, extern_fn_decl_arg_info) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_ExternFnDecl, extern_fn_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_GlobalVarDecl, global_var_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_ExternVarDecl, extern_var_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_LocalVarDecl, local_var_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_ModDecl, mod_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_StructDecl, struct_decl) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_StructDeclMemberInfo, struct_decl_member_info) \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        IdentifierId decl_name;                     \
        StringLiteralId file_name;                  \
        /* `result_ast_file` starts out NULL */     \
        Yuceyuf_Ast_File *resolved_ast_file;        \
    }, imported_file)                               \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *func;                      \
        Yuceyuf_AstList args; /* List of expr */    \
        bool args_has_trailing_comma;               \
    }, fn_call)                                     \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        TokenBuiltinFn bfn;                         \
        Yuceyuf_AstList args; /* List of expr */    \
        bool args_has_trailing_comma;               \
    }, builtin_fn_call)                             \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstList stmts;                      \
    }, stmt_block)                                  \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstList stmts;                      \
        IdentifierId name;                          \
    }, labeled_block)                               \
    YY_META_TAGGED_UNION__MEMBER(Void, return_void) \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *expr;                      \
    }, return_expr)                                 \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Option<IdentifierId> label;                 \
    }, break_void)                                  \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *expr;                      \
        Option<IdentifierId> label;                 \
    }, break_expr)                                  \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Option<IdentifierId> label;                 \
    }, xcontinue)                                   \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *lhs;                       \
        Yuceyuf_AstNode *rhs;                       \
    }, assign)                                      \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_Ast_BinaryOp op;                    \
        Yuceyuf_AstNode *lhs;                       \
        Yuceyuf_AstNode *rhs;                       \
    }, assign_bin_op)                               \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_Ast_BinaryOp op;                    \
        Yuceyuf_AstNode *lhs;                       \
        Yuceyuf_AstNode *rhs;                       \
    }, op_bin)                                      \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_Ast_UnaryOp tag;                    \
        Yuceyuf_AstNode *operand;                   \
    }, op_unary_prefix)                             \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_While, xwhile)     \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_If, stmt_if)       \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode_Specific_Payload_If, expr_if)       \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *container;                 \
        IdentifierId field;             \
    }, field_access)                                \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        Yuceyuf_AstNode *container;                 \
        IdentifierId field;             \
    }, mod_field_access)                            \
    YY_META_TAGGED_UNION__MEMBER(struct {      \
        IdentifierId identifier;        \
    }, identifier)                                  \
    YY_META_TAGGED_UNION__MEMBER(TokenStringLiteral, string_literal)      \
    YY_META_TAGGED_UNION__MEMBER(TokenIntegerLiteral, integer_literal)    \
    YY_META_TAGGED_UNION__MEMBER(TokenCharLiteral, char_literal)          \
    YY_META_TAGGED_UNION__MEMBER(TokenKeyword, builtin_typ)          \
    YY_META_TAGGED_UNION__MEMBER(bool, bool_literal) \
    YY_META_TAGGED_UNION__MEMBER(Void, undefined_literal) \
    /* an `unreachable` statement might compile into nothing, while `@panic()` always crashes */    \
    YY_META_TAGGED_UNION__MEMBER(Void, unreach) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode *, deref) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode *, ignore_result) \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode *, paren)
#include "yy/meta/tagged_union.hpp"

struct AstSourceRegion {
    TokenId begin;
    TokenId mid;
    TokenId end_inclusive;
    constexpr AstSourceRegion() = default;
    constexpr AstSourceRegion(TokenId single) : begin(single), mid(single), end_inclusive(single) {}
    constexpr AstSourceRegion(TokenId begin, TokenId mid, TokenId end_inclusive)
        : begin(begin), mid(mid), end_inclusive(end_inclusive) {}
    NODISCARD auto to_src_region_line_and_column(LexedFile const& lex) const -> SourceRegionLineColumn {
        auto begin_line_column = lex.token_src(begin).begin.to_line_and_column(lex.line_info);
        return SourceRegionLineColumn{
            .begin = begin_line_column,
            .mid = lex.token_src(mid).begin.to_line_and_column(lex.line_info, begin_line_column),
            .end = lex.token_src(end_inclusive).end.to_line_and_column(lex.line_info, begin_line_column),
        };
    }
};

struct Yuceyuf_AstNode {
    AstSourceRegion src;

    Yuceyuf_Ast_File *parent_ast_file;

    Yuceyuf_AstNode_Specific it;
};

struct Yuceyuf_Ast_File : yy::NoCopyNoMove {
    Yy_Arena *arena;
    /// This filename is canonical.
    Sv filename;
    u32 filename_hash_for_module_lookup;
    /// The number of directory nodes that are part of the package's main file. You can't import
    /// files outside of that directory. e.g., filename "a/b/c.yuceyuf" where "a/b/x.yuceyuf" is the
    /// main file for the package: `package_root_dir_components == 2`.
    u32 package_root_dir_components;
    /// Trailing slashes removed.
    Sv path_dirname;

    LexedFileIterator lexer;

    Yuceyuf_AstNode *root;
    Vec<Yuceyuf_ErrorMessage> errors;

    // `queued_imported_files` will be processed later on `module.c`.
    Vec<Yuceyuf_AstNode *> queued_imported_files;
};

NODISCARD auto yuceyuf_ast_file_unresolved_from_tokens(
    Yy_Arena *arena,
    Sv filename,
    u32 package_root_dir_components,
    LexedFile const& lexed_file
) -> std::pair<Yuceyuf_Ast_File*, YyStatus>;

NODISCARD bool yuceyuf_ast_node_requires_stmt_terminator(Yuceyuf_AstNode_Specific_Tag node_tag);
void yuceyuf_ast_debug_print_root(Yuceyuf_AstNode *root, Sv const filename);


NODISCARD u32 yuceyuf_ast_filename_hash_for_module_lookup(Sv filename);

extern bool verbose_ast;

#endif//___INCLUDE_GUARD__Gcl1HitNINNZGYldTADYMHEOUJivVlrWHE68FQrS
