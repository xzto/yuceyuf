
#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/arena.hpp"
#include "yy/misc.hpp"
#include "yuceyuf/compilation.hpp"
#include "yuceyuf/lexer.hpp"
#include "yuceyuf/ast.hpp"
#include "yuceyuf/genir.hpp"
#include "yuceyuf/sema.hpp"
#include "yuceyuf/codegen_x64.hpp"
// #include "yuceyuf/pretty_printer.hpp"
#include "yuceyuf/path_builder.hpp"
#include "yy/timer.hpp"
#include "yuceyuf/file.hpp"
#include "yuceyuf/tracy.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h> // for isatty

#include <tuple>


#define LOG_LAP(t, lap, name) do {                                                          \
    lap = timer_lap(t);                                                                     \
    if (verbose_timings)                                                                    \
        fprintf(stderr, "LAP:\t%s:\t%s\t" TimeMs_Fmt "\n", __func__, name, TimeMs_Arg(lap));  \
} while (false)


static constexpr char const subcmd_ir_test_cstr[] = "ir-test";

#if 0
static char const subcmd_pretty_print_cstr[] = "pretty-print";
NORETURN static void subcmd_pretty_print_usage(YyStatus err, char const *argv0)
{
    bool const success = yy_is_ok(err);
    FILE *const stream = success ? stdout : stderr;
    // fprintf(stream, "usage: %s %s [options] [--] <filename.yuceyuf>\n", argv0, subcmd_pretty_print_cstr);
    // fprintf(stream, "options:\n");
    // fprintf(stream, "  --                           Stop parsing options.\n");
    // fprintf(stream, "  --stdin\n");
    // fprintf(stream, "  --check\n");
    fprintf(stream, "Usage: %s %s <filename.yuceyuf>\n", argv0, subcmd_pretty_print_cstr);
    exit(success ? 0 : 1);
}

static int subcmd_pretty_print_main(char const *const original_argv0, usize const argc, char const *const *const argv)
{
    if (argc != 1) {
        subcmd_pretty_print_usage(Yy_Bad, original_argv0);
    }

    FILE *const primary_stream = stdout;
    bool const errors_do_color = isatty(fileno(stderr)) == 1;

    char const *const root_filename_cstr = argv[0];

    Yy_Arena arena;
    yy_arena_init(&arena, page_allocator(), 0);

    Sv const root_filename =
        path_with_single_slashes(yy_arena_allocator(&arena), yy_arena_allocator(&arena),
                                 Sv(root_filename_cstr));

    SvMut file_contents;

    int os_err;
    if (yy_is_err(yuceyuf_read_entire_file_into_chars(yy_arena_allocator(&arena), YUCEYUF_AT_FDCWD, root_filename, SIZE_MAX, &file_contents, &os_err))) {
        fprintf(stderr, "Failed reading file ‘" Sv_Fmt "’: %s\n", Sv_Arg(root_filename), strerror(os_err));
        exit(1);
    }

    auto global_comp = GlobalCompilation(c_allocator());

    auto lexer_result = yuceyuf::lexer::lex_file(&global_comp, root_filename, file_contents, &arena);
    if (lexer_result.errors.has_value()) {
        auto& lexed_errors = lexer_result.errors.unwrap();
        fprintf(stderr, "error: failed tokenizing file.\n");
        for (auto& e : lexed_errors.errors) {
            yuceyuf_error_message_print_stderr(&e, errors_do_color);
        }
        exit(1);
    }

    auto& lexed_file = lexer_result.lexed_file.unwrap();

    Yuceyuf_Ast_File ast_file;
    if (yy_is_err(yuceyuf_ast_file_unresolved_from_tokens(
            &arena, root_filename, path_count_nodes(root_filename) - 1, lexed_file, &ast_file
        ))) {
        fprintf(stderr, "error: failed generating ast\n");
        for (usize i = 0; i < ast_file.errors.len; i++) {
            yuceyuf_error_message_print_stderr(&ast_file.errors.ptr[i], errors_do_color);
        }
        exit(1);
    }

    pretty_print_ast(ast_file.root, primary_stream);
    exit(0);
}
#endif

NORETURN static void subcmd_ir_test_usage(YyStatus err, char const *argv0)
{
    bool const success = yy_is_ok(err);
    FILE *const stream = success ? stdout : stderr;
    fprintf(stream, "Usage: %s %s [options] [--] <filename.yuceyuf>\n", argv0, subcmd_ir_test_cstr);
    fprintf(stream, "Options:\n");
    fprintf(stream, "  -h, --help                   Show help.\n");
    fprintf(stream, "  --                           Stop parsing options.\n");
    fprintf(stream, "  --asm-out <filename.asm>\n");
    fprintf(stream, "  --verbose-tokens\n");
    fprintf(stream, "  --verbose-ast\n");
    fprintf(stream, "  --verbose-genir\n");
    fprintf(stream, "  --verbose-sema\n");
    fprintf(stream, "  --verbose-codegen\n");
    fprintf(stream, "  --stop-at-tokens\n");
    fprintf(stream, "  --stop-at-ast\n");
    fprintf(stream, "  --stop-at-genir\n");
    fprintf(stream, "  --stop-at-sema\n");
    fprintf(stream, "  --stop-at-codegen\n");
    fprintf(stream, "  --verbose-timings\n");
    exit(success ? 0 : 1);
}

bool verbose_timings = false;
bool verbose_tokens = false;
bool verbose_ast = false;
bool verbose_genir = false;
bool verbose_sema = false;
bool verbose_codegen = false;
static int subcmd_ir_test_main(char const *const original_argv0, usize const argc, char const *const *const argv)
{
    verbose_timings = false;
    verbose_tokens = false;
    verbose_ast = false;
    verbose_genir = false;
    verbose_sema = false;
    verbose_codegen = false;
    bool stop_at_tokens = false;
    bool stop_at_ast = false;
    bool stop_at_genir = false;
    bool stop_at_sema = false;
    bool stop_at_codegen = false;
    bool stop_parsing_options = false;
    char const *root_filename_cstr = NULL;
    Sv codegen_asm_out = sv_lit("yuceyuf_output.asm");
    for (usize arg_idx = 0; arg_idx < argc; arg_idx++) {
        char const *arg = argv[arg_idx];
        Sv const arg_sv = Sv(arg);
        if (!stop_parsing_options && arg[0] == '-') {
            if (sv_eql(arg_sv, sv_lit("--"))) {
                stop_parsing_options = true;
            } else if (sv_eql(arg_sv, sv_lit("-h"))) {
                subcmd_ir_test_usage(Yy_Ok, original_argv0);
            } else if (sv_eql(arg_sv, sv_lit("--help"))) {
                subcmd_ir_test_usage(Yy_Ok, original_argv0);
            } else if (sv_eql(arg_sv, sv_lit("--verbose-timings"))) {
                verbose_timings = true;
            } else if (sv_eql(arg_sv, sv_lit("--verbose-tokens"))) {
                verbose_tokens = true;
            } else if (sv_eql(arg_sv, sv_lit("--verbose-ast"))) {
                verbose_ast = true;
            } else if (sv_eql(arg_sv, sv_lit("--verbose-genir"))) {
                verbose_genir = true;
            } else if (sv_eql(arg_sv, sv_lit("--verbose-sema"))) {
                verbose_sema = true;
            } else if (sv_eql(arg_sv, sv_lit("--verbose-codegen"))) {
                verbose_codegen = true;
            } else if (sv_eql(arg_sv, sv_lit("--stop-at-tokens"))) {
                stop_at_tokens = true;
            } else if (sv_eql(arg_sv, sv_lit("--stop-at-ast"))) {
                stop_at_ast = true;
            } else if (sv_eql(arg_sv, sv_lit("--stop-at-genir"))) {
                stop_at_genir = true;
            } else if (sv_eql(arg_sv, sv_lit("--stop-at-sema"))) {
                stop_at_sema = true;
            } else if (sv_eql(arg_sv, sv_lit("--stop-at-codegen"))) {
                stop_at_codegen = true;
            } else if (sv_eql(arg_sv, sv_lit("--asm-out"))) {
                if (arg_idx + 1 >= argc) {
                    fprintf(stderr, "error: expected filename after --asm-out\n");
                    subcmd_ir_test_usage(Yy_Bad, original_argv0);
                }
                codegen_asm_out = Sv(argv[arg_idx + 1]);
                if (codegen_asm_out.len <= 0) {
                    fprintf(stderr, "error: empty --asm-out filename: " Sv_Fmt "\n", Sv_Arg(codegen_asm_out));
                    subcmd_ir_test_usage(Yy_Bad, original_argv0);
                }
                arg_idx += 1;
            } else {
                fprintf(stderr, "Unknown option: '%s'\n", arg);
                subcmd_ir_test_usage(Yy_Bad, original_argv0);
            }
        } else {
            if (root_filename_cstr != NULL) {
                fprintf(stderr, "Only one filename is allowed.\n");
                subcmd_ir_test_usage(Yy_Bad, original_argv0);
            } else {
                root_filename_cstr = arg;
            }
        }
    }

    if (root_filename_cstr == NULL) {
        fprintf(stderr, "No filename was given\n");
        subcmd_ir_test_usage(Yy_Bad, original_argv0);
    }

    Timer t;
    timer_init(&t);
    u64 lap;
    (void) lap;

    bool const errors_do_color = isatty(fileno(stderr)) == 1;

    Yy_ArenaCpp arena{page_allocator(), 0};

    Sv const root_filename = path_with_single_slashes(arena.allocator(), arena.allocator(), Sv(root_filename_cstr));

    SvMut file_contents;

    int os_err = yy::uninitialized;
    if (yy_is_err(yuceyuf_read_entire_file_chars(
            arena.allocator(), YUCEYUF_AT_FDCWD, root_filename, SIZE_MAX, &file_contents, &os_err
        ))) {
        yy_assert(os_err != 0);
        fprintf(stderr, "Failed reading file ‘" Sv_Fmt "’: %s\n", Sv_Arg(root_filename), strerror(os_err));
        return 1;
    }
    LOG_LAP(&t, lap, "yuceyuf_read_entire_file_chars");

    auto global_comp = GlobalCompilation(c_allocator());

    // TODO: ZoneScopedN("lexer");
    auto lexer_result = lex_file(&global_comp, root_filename, file_contents, c_allocator());
    if (lexer_result.errors.has_value()) {
        auto& lexed_errors = lexer_result.errors.unwrap();
        fprintf(stderr, "error: failed tokenizing file.\n");
        for (auto& e : lexed_errors.msgs) {
            yuceyuf_error_message_print_stderr(&e, errors_do_color);
        }
        return 1;
    }
    auto& lexed_file = lexer_result.lexed_file.unwrap();

    if (verbose_tokens) {
        fprintf(stderr, "# file ‘" Sv_Fmt "’\n", Sv_Arg(root_filename));
        usize token_count = 0;
        LexedFileIterator iter{lexed_file};
        while (true) {
            auto token_id = iter.next();
            fprintf(stderr, "" Sv_Fmt ":", Sv_Arg(root_filename));
            yuceyuf::lexer::debug_token_print(lexed_file, token_id);
            token_count += 1;
            if (lexed_file.token(token_id).data.tag == yuceyuf::lexer::TokenData_Tag__eof)
                break;
        }
        fprintf(stderr, "# there were %zu tokens\n", token_count);
    }
    LOG_LAP(&t, lap, "yuceyuf_lexer_init_from_contents");
    if (stop_at_tokens) return 0;

    Yuceyuf_Ast_File* ast_file = yy::uninitialized;
    {
        ZoneScopedN("parse");
        YyStatus status = yy::uninitialized;
        std::tie(ast_file, status) = yuceyuf_ast_file_unresolved_from_tokens(
            &arena.a, root_filename, path_count_nodes(root_filename) - 1, lexed_file
        );
        if (yy_is_err(status)) {
            if (verbose_ast)
                fprintf(stderr, "error: failed generating ast\n");
            for (auto const& err_msg : ast_file->errors) {
                yuceyuf_error_message_print_stderr(&err_msg, errors_do_color);
            }
            return 1;
        }
    }
    if (verbose_ast) {
        fprintf(stderr, "# file ‘" Sv_Fmt "’\n", Sv_Arg(root_filename));
        yuceyuf_ast_debug_print_root(ast_file->root, root_filename);
    }
    LOG_LAP(&t, lap, "yuceyuf_ast_file_unresolved_from_tokens");
    if (stop_at_ast) return 0;

    Option<GenIr> gi_instance = None;
    GenIr* gi = nullptr;
    Option<Sema> sema_instance = None;
    Sema* sema = nullptr;
    Option<Yy_CodegenX64> codegen_x64_instance = None;
    Yy_CodegenX64* codegen_x64 = nullptr;

    {
        ZoneScopedN("genir");
        gi_instance = yy_debug_gen_ir_from_ast_file(ast_file);
        gi = gi_instance.unwrap_pointer();
        if (gi->errors.fatal) {
            if (verbose_genir) fprintf(stderr, "error: failed yy_debug_gen_ir_from_ast_file\n");
            yy_assert(gi->has_errors());
            for (auto const& err_msg : gi->errors.msgs) {
                yuceyuf_error_message_print_stderr(&err_msg, errors_do_color);
            }
            return 1;
        }
    }
    // We may have genir errors.
    for (auto const& err_msg : gi->errors.msgs) {
        yuceyuf_error_message_print_stderr(&err_msg, errors_do_color);
    }
    yy_assert(!gi->errors.fatal);
    LOG_LAP(&t, lap, "yy_debug_gen_ir_from_ast_file");
    if (stop_at_genir) return 0;

    {
        ZoneScopedN("sema");
        sema_instance = yy_sema_analyze_ir(c_allocator(), gi);
        sema = sema_instance.unwrap_pointer();
        if (sema->errors.fatal) {
            if (verbose_sema) fprintf(stderr, "error: failed yy_sema_analyze_ir\n");
            yy_assert(sema->has_errors());
            for (auto const& err_msg : sema->errors.msgs) {
                yuceyuf_error_message_print_stderr(&err_msg, errors_do_color);
            }
            return 1;
        }
    }
    yy_assert(!gi->has_errors());
    yy_assert(!sema->has_errors());
    yy_assert(!sema->errors.fatal);
    LOG_LAP(&t, lap, "yy_sema_analyze_ir");
    if (stop_at_sema) return 0;

    {
        ZoneScopedN("codegen");
        codegen_x64_instance = yy_codegen_x64_from_sema(c_allocator(), sema, codegen_asm_out);
        codegen_x64= codegen_x64_instance.unwrap_pointer();
        if (codegen_x64->errors.fatal) {
            if (verbose_codegen) fprintf(stderr, "error: failed yy_codegen_x64_from_genir\n");
            yy_assert(codegen_x64->errors.msgs.size() > 0);
            for (auto const& err_msg : codegen_x64->errors.msgs) {
                yuceyuf_error_message_print_stderr(&err_msg, errors_do_color);
            }
            return 1;
        }
    }
    yy_assert(codegen_x64->errors.msgs.empty());
    yy_assert(!codegen_x64->errors.fatal);
    LOG_LAP(&t, lap, "yy_codegen_x64_from_genir");
    if (stop_at_codegen) return 0;

    return 0;
}


NORETURN static void usage_main(YyStatus err, char const *argv0)
{
    bool const success = yy_is_ok(err);
    FILE *const stream = success ? stdout : stderr;
    fprintf(stream, "Usage: %s <subcommand> [subcommand options]\n", argv0);
    fprintf(stream, "\n");
    fprintf(stream, "Subcommands:\n");
    fprintf(stream, "  %s\n", subcmd_ir_test_cstr);
    exit(success ? 0 : 1);
}

auto yuceyuf_error_message_print_stderr(
    Yuceyuf_ErrorMessage const* err_msg,
    bool do_color
) -> void {
    char const *const csi_reset         = (do_color ? "\x1b[0m"    : "");
    char const *const csi_filename      = (do_color ? "\x1b[1;37m" : "");
    char const *const csi_red           = (do_color ? "\x1b[1;31m" : "");
    char const *const csi_cyan          = (do_color ? "\x1b[1;36m" : "");
    char const *const csi_note_or_error = (do_color ? (err_msg->is_note ? csi_cyan : csi_red) : "");
    char const *const note_or_error     = (err_msg->is_note ? "note" : "error");
    bool const filename_is_dummy = err_msg->filename.is_none();
    bool const src_region_is_dummy = err_msg->src_region.is_none();
    if (filename_is_dummy) {
        yy_assert(src_region_is_dummy);
        fprintf(stderr, "%s%s:%s " Sv_Fmt "\n",
                csi_note_or_error, note_or_error, csi_reset,
                Sv_Arg(err_msg->msg));
    } else if (src_region_is_dummy) {
        auto filename = err_msg->filename.unwrap();
        fprintf(stderr, "%s" Sv_Fmt ":%s %s%s:%s " Sv_Fmt "\n",
                csi_filename,
                Sv_Arg(filename),
                csi_reset,
                csi_note_or_error, note_or_error, csi_reset,
                Sv_Arg(err_msg->msg));
    } else {
        auto filename = err_msg->filename.unwrap();
        yy_assert(filename.len > 0);
        auto src_region_mid_pos = err_msg->src_region.unwrap().mid;
        fprintf(stderr, "%s" Sv_Fmt ":%u:%u:%s %s%s:%s " Sv_Fmt "\n",
                csi_filename,
                Sv_Arg(filename), src_region_mid_pos.line1, src_region_mid_pos.column1,
                csi_reset,
                csi_note_or_error, note_or_error, csi_reset,
                Sv_Arg(err_msg->msg));
    }
}


int main(int const original_argc, char const *const *const original_argv)
{
    yy::the_original_argc_argv = {
        .argc = original_argc,
        .argv = original_argv,
    };
    if (original_argc < 1) {
        yy_panic("argc < 1");
    }
    usize argc = (usize)original_argc;
    char const *const *argv = original_argv;
    for (usize arg_idx = 1; arg_idx < argc; arg_idx += 1) {
        char const *arg = argv[arg_idx];
        Sv const arg_sv = Sv(arg);
        if (arg_sv == "-h") {
            usage_main(Yy_Ok, original_argv[0]);
        } else if (arg_sv == "--help") {
            usage_main(Yy_Ok, original_argv[0]);
        } else if (arg_sv == subcmd_ir_test_cstr) {
            return subcmd_ir_test_main(original_argv[0], (usize)argc - 2, &argv[2]);
        } else {
            fprintf(stderr, "unknown subcommand: '%s'\n", arg);
            usage_main(Yy_Bad, original_argv[0]);
        }
    }
    usage_main(Yy_Bad, original_argv[0]);
    yy_unreachable();
}
