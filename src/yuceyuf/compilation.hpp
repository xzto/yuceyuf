#pragma once

#include "yy/basic.hpp"
#include "yy/arena.hpp"
#include "yy/newer_hash_map.hpp"
#include "yy/sv.hpp"
#include "yy/vec.hpp"

namespace yuceyuf {

using yy::Allocator;

namespace lexer {
struct LexerCtx;
struct LexedFile;
}  // namespace lexer

namespace compilation {

struct FileId {
    constexpr explicit FileId(u32 x) : x(x) {}
    u32 x;
    NODISCARD friend auto operator==(FileId, FileId) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(FileId, FileId{UINT32_MAX});
};
struct IdentifierId {
    constexpr explicit IdentifierId(u32 x) : x(x) {}
    u32 x;
    NODISCARD friend auto operator==(IdentifierId, IdentifierId) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(IdentifierId, IdentifierId{UINT32_MAX});
};


using IdentifierTable = NewerHashMap<Sv, Void>;

struct GlobalCompilation : yy::MoveOnly {
    GlobalCompilation(yy::Allocator allocator);

    IdentifierTable identifiers;
    Vec<lexer::LexedFile*> lexed_files;
    Yy_ArenaCpp arena;

    NODISCARD auto reserve_lexed_file() -> FileId;
    NODISCARD auto unreserve_lexed_file(FileId id, lexer::LexerCtx&& lexer_ctx) -> lexer::LexedFile&;

    NODISCARD auto make_identifier(Sv name) -> IdentifierId;
    NODISCARD auto identifier_sv(IdentifierId id) const -> Sv;
};

}  // namespace compilation
}  // namespace yuceyuf

using namespace yuceyuf::compilation;
