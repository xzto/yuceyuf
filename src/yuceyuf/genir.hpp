#ifndef ___INCLUDE_GUARD__emm6x61ZtAzPopSDaGoHDuKcpZ1fXi4TrYMnH3vU
#define ___INCLUDE_GUARD__emm6x61ZtAzPopSDaGoHDuKcpZ1fXi4TrYMnH3vU

#include "yy/basic.hpp"
#include "yy/newer_hash_map.hpp"
#include "yy/meta/enum.hpp"
#include "yuceyuf/ast.hpp"
#include "yuceyuf/ir.hpp"

struct GenIr;

/// An error can be in an instruction or in ast.
#define YY_META_TAGGED_UNION__NAME Genir_ErrorLocation
#define YY_META_TAGGED_UNION__ALL_MEMBERS                  \
    YY_META_TAGGED_UNION__MEMBER(Yuceyuf_AstNode *, ast)   \
    YY_META_TAGGED_UNION__MEMBER(Yy_Ir_InstIndex, inst)         \
YY_EMPTY_MACRO
#include "yy/meta/tagged_union.hpp"

extern bool verbose_genir;

namespace genir {

template <typename Element, typename Index>
    requires std::is_constructible_v<usize, Index>
struct ArrayFoo {
    VecUnmanagedLeaked<Element> vec;
    constexpr auto operator[](Index idx) -> Element& { return vec[usize(idx)]; }
    constexpr auto operator[](Index idx) const -> Element const& { return vec[usize(idx)]; }

    NODISCARD constexpr auto size() const -> usize { return vec.size(); }
    NODISCARD constexpr auto next_index() const -> Index {
        static_assert(sizeof(Index) == sizeof(std::declval<Index>().x));
        Index result = Index{yy::checked_int_cast<yy::member_type_t<&Index::x>>(vec.size()).unwrap()};
        return result;
    }
};

using IrInstToAstMap = ArrayFoo<Yuceyuf_AstNode*, Yy_Ir_InstIndex>;

struct Decl {
    Yuceyuf_AstNode* ast;
    Option<IdentifierId> opt_name;
    Yy_Ir_InstIndex inst;
    // TODO: has_genir_collect_errors, has_genir_body_errors, has_sema_collect_errors, has_sema_body_errors,
    bool has_genir_hard_errors_todo_collect_vs_gen;
    bool has_sema_collect_hard_errors;
    bool has_sema_analyze_hard_errors;

    NODISCARD auto has_genir_hard_errors() const -> bool { return has_genir_hard_errors_todo_collect_vs_gen; }
    NODISCARD auto has_sema_hard_errors() const -> bool { return has_sema_collect_hard_errors || has_sema_analyze_hard_errors; }
    NODISCARD auto has_any_hard_errors() const -> bool { return has_genir_hard_errors() || has_sema_hard_errors(); }
};

}

using namespace genir;

struct GenIr {
    Sv filename;
    Yuceyuf_Ast_File* ast_file;
    Yy_Allocator heap;
    Yy_ArenaCpp arena;
    Yuceyuf_ErrorBundle errors;
    ArrayFoo<Yy_Ir_Inst, Yy_Ir_InstIndex> insts{};
    VecUnmanagedLeaked<Yy_Ir_Extra> extras{};
    VecUnmanagedLeaked<char> string_table{};
    // This is used for error messages. Indexed by InstIndex.
    ArrayFoo<Yuceyuf_AstNode*, Yy_Ir_InstIndex> inst_to_ast_map{};
    ArrayFoo<Decl*, DeclId> decl_ptrs{};

    u32 debug_root_scope_create_count{0};
    u32 debug_root_scope_destroy_count{0};

    NODISCARD auto has_errors() const -> bool {
        if (errors.soft_count > 0 || errors.hard_count > 0 || errors.fatal)
            yy_assert(errors.msgs.size() > 0);
        else yy_assert(errors.msgs.size() == 0);
        return errors.msgs.size() > 0;
    }

    GenIr(Yuceyuf_Ast_File* ast, Yy_Allocator heap);
    ~GenIr();
    GenIr(const GenIr&) = delete;
    GenIr& operator=(const GenIr&) = delete;
    GenIr(GenIr&&) = default;
    GenIr& operator=(GenIr&&) = default;
};

// TODO: Custom iterator for a block instruction.

NODISCARD auto yy_debug_gen_ir_from_ast_file(Yuceyuf_Ast_File *ast_file) -> GenIr;

NODISCARD auto genir_identifier_sv(GenIr* const gi, IdentifierId id) -> Sv;
NODISCARD Sv genir_strTableGetTempSvFromZ(GenIr const *gi, Yy_Ir_StringIndex str_z_idx);
NODISCARD Sv genir_strTableGetTempSvFromLen(GenIr const *const gi, Yy_Ir_StringIndex const str_idx, u32 const len);
NODISCARD Sv genir_strTableGetTempSvFromSized0(GenIr const *const gi, Yy_Ir_StringSized0 const string);

struct Sema;
void genir_debugPrint(GenIr *gi, Option<Sema*>);

NODISCARD u32 genir_instEndOffset(GenIr *const gi, Yy_Ir_InstIndex const inst_idx);


NODISCARD Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo
genir_getFnDeclArgData(GenIr *const gi, Yy_Ir_InstIndex const fn_decl, u32 const arg_idx);
NODISCARD Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo
genir_getExternFnDeclArgData(GenIr *const gi, Yy_Ir_InstIndex const extern_fn_decl, u32 const arg_idx);

NODISCARD Yy_Ir_Inst_ExtraPl_Call_Arg
genir_getCallArgData(GenIr *const gi, Yy_Ir_InstIndex const call, u32 const arg_idx);


#define genir_smallPl(gi, inst_idx, tag_name) (                                                 \
    yy_assert_and_zero(genir_instsAt(gi, inst_idx)->tag == Yy_Ir_Inst_Tag__##tag_name),    \
    &genir_instsAt(gi, inst_idx)->small_data.tag_name                                           \
)

#define genir_extraPlIndex(gi, inst_idx, tag_name) (genir_smallPl(gi, inst_idx, tag_name)->extra_pl_index)
#define genir_extraPl(gi, inst_idx, tag_name) ( \
    genir_extrasAtTypedNoAssert<decltype(Yy_Ir_Inst_DEBUG_FullUnionOfPayloads::tag_name)>(gi, genir_extraPlIndex(gi, inst_idx, tag_name)) \
)
#define genir_extraPlNoAssert(gi, inst_idx, tag_name) ( \
    genir_extrasAtTypedNoAssert<decltype(Yy_Ir_Inst_DEBUG_FullUnionOfPayloads::tag_name)>(gi, genir_instsAt(gi, inst_idx)->small_data.tag_name.extra_pl_index) \
)


static inline Yy_Ir_Inst* genir_instsAt(GenIr* gi, Yy_Ir_InstIndex idx) { return &gi->insts[idx]; }
static inline Yy_Ir_Inst const* genir_instsAt(GenIr const* gi, Yy_Ir_InstIndex idx) { return &gi->insts[idx]; }
static inline Yy_Ir_Extra* genir_extrasAt(GenIr* gi, Yy_Ir_ExtraIndex idx) { return &gi->extras[idx.x]; }
static inline Yy_Ir_Extra const* genir_extrasAt(GenIr const* gi, Yy_Ir_ExtraIndex idx) { return &gi->extras[idx.x]; }
static inline char* genir_stringsAt(GenIr* gi, Yy_Ir_StringIndex idx) { return &gi->string_table[idx.x]; }
static inline char const* genir_stringsAt(GenIr const* gi, Yy_Ir_StringIndex idx) { return &gi->string_table[idx.x]; }

NODISCARD Yy_Ir_ExtraIndex genir_reserveExtra(GenIr *gi, u32 reserved_len);
void genir_unreserveExtra(GenIr* gi, Yy_Ir_ExtraIndex reserved_idx, Yy_Ir_Extra const* data_ptr, u32 data_len);

template <ir::extra_pl Pl>
NODISCARD auto genir_appendExtraPl(GenIr* gi, Pl const& extra_pl) -> Yy_Ir_ExtraIndex {
    auto constexpr pl_byte_size = sizeof(Pl) / sizeof(Yy_Ir_Extra);
    Yy_Ir_ExtraIndex const result = genir_reserveExtra(gi, pl_byte_size);
    genir_unreserveExtra(gi, result, reinterpret_cast<Yy_Ir_Extra const*>(&extra_pl), pl_byte_size);
    return result;
}



template <ir::extra_pl T>
static inline T * genir_extrasAtTypedNoAssert(GenIr* gi, Yy_Ir_ExtraIndex idx) {
    auto extra_ptr = &gi->extras[idx.x];
    return reinterpret_cast<T*>(extra_ptr);
}
template <ir::extra_pl T>
static inline T const* genir_extrasAtTypedNoAssert(GenIr const* gi, Yy_Ir_ExtraIndex idx) {
    auto extra_ptr = &gi->extras[idx.x];
    return reinterpret_cast<T const*>(extra_ptr);
}

NODISCARD inline Yy_Ir_InstIndex genir_getGlobalVarDeclInitBlockInst(GenIr *gi, Yy_Ir_InstIndex decl_inst)
{
    auto pl = genir_extraPl(gi, decl_inst, global_var_decl);
    return genir_instAbsSF(decl_inst, pl->init_block);
}
NODISCARD inline Yy_Ir_InstIndex genir_getFnDeclBodyInst(GenIr *gi, Yy_Ir_InstIndex fn_decl_inst)
{
    auto pl = genir_extraPl(gi, fn_decl_inst, fn_decl);
    return genir_instAbsSF(fn_decl_inst, pl->body_block_offset);
}
NODISCARD inline Yy_Ir_InstIndex genir_getFnDeclReturnTypeBlockInst(GenIr *gi, Yy_Ir_InstIndex fn_decl_inst)
{
    auto pl = genir_extraPl(gi, fn_decl_inst, fn_decl);
    return genir_instAbsSF(fn_decl_inst, pl->return_type_eval_block_offset);
}
NODISCARD inline Yy_Ir_InstIndex genir_getFnDeclArgDeclInst(GenIr *gi, Yy_Ir_InstIndex fn_decl_inst, u32 arg_idx)
{
    return genir_getFnDeclBodyInst(gi, fn_decl_inst) + 1 + arg_idx;
}

NODISCARD inline Yy_Ir_InstIndex genir_getExternFnDeclReturnTypeBlockInst(GenIr *gi, Yy_Ir_InstIndex extern_fn_decl_inst)
{
    auto pl = genir_extraPl(gi, extern_fn_decl_inst, extern_fn_decl);
    return genir_instAbsSF(extern_fn_decl_inst, pl->return_type_eval_block_offset);
}


namespace ir {

template <ir::extra_pl ParentPl>
ExtraTrailingArray<ParentPl>::ExtraTrailingArray(GenIr* gi, ExtraIndex parent)
    : trailing_start{.x = parent.x + static_cast<u32>(sizeof(ParentPl) / sizeof(Extra))} {
    auto parent_pl = genir_extrasAtTypedNoAssert<ParentPl>(gi, parent);
    trailing_count = parent_pl->trailing_count();
}

template <ir::extra_pl ParentPl>
NODISCARD auto ExtraTrailingArray<ParentPl>::nth_ptr(GenIr* gi, u32 index) const -> ParentPl::TrailingElement* {
    yy_assert(index < trailing_count);
    auto result = genir_extrasAtTypedNoAssert<TrailingElement>(gi, nth_index(index));
    return result;
}

}

#endif//___INCLUDE_GUARD__emm6x61ZtAzPopSDaGoHDuKcpZ1fXi4TrYMnH3vU
