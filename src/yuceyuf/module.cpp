#include "yuceyuf/module.hpp"
#include "yy/misc.hpp"
#include "yuceyuf/path_builder.hpp"
#include "yy/timer.hpp"

#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

COLD static void module_add_err_msg__(bool is_note, Yuceyuf_Module *module, Sv filename, Yuceyuf_Lexer_TokenPosition tok_pos, char const *fmt, va_list ap)
{
    char tmp_buf[1024];

    isize const res = vsnprintf(tmp_buf, sizeof(tmp_buf), fmt, ap);
    yy_assert(res >= 0);
    usize const written_size = yuceyuf_min((usize)res, sizeof(tmp_buf) - 1);

    Sv const msg = sv_const(yuceyuf_arena_dup_sv(module->arena, sv_cstr_len(tmp_buf, written_size)));
    Yuceyuf_ErrorMessage const error = {
        .filename = filename,
        .tok_pos = tok_pos,
        .is_note = is_note,
        .msg = msg,
    };
    array_list_append(&module->errors, error);
}

PRINTF_LIKE(4, 5)
static void module_add_error_msg(Yuceyuf_Module *module, Sv filename, Yuceyuf_Lexer_TokenPosition tok_pos, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    module_add_err_msg__(false, module, filename, tok_pos, fmt, ap);
    va_end(ap);
}

PRINTF_LIKE(4, 5)
static void module_add_error_note(Yuceyuf_Module *module, Sv filename, Yuceyuf_Lexer_TokenPosition tok_pos, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    module_add_err_msg__(true, module, filename, tok_pos, fmt, ap);
    va_end(ap);
}


static u32 const READ_FILE_AND_PARSE__FIRST_FILE_IN_PACKAGE = UINT32_MAX;

NODISCARD static YyStatus read_file_and_parse(
    Yuceyuf_Module *module, Yuceyuf_ModuleFromFilenameOptions const *mff_options,
    Sv OPT prev_filename, Yuceyuf_Lexer_TokenPosition OPT prev_file_import_tok_pos,
    u32 OPT package_root_dir_components,
    Sv canonical_filename,
    Yuceyuf_Ast_File **result_ast_file
)
{
    yy_assert(!(canonical_filename.ptr == Yy_ErrMsg_Filename_DUMMY.ptr && canonical_filename.len == Yy_ErrMsg_Filename_DUMMY.len));

    Timer timer;
    timer_init(&timer);

    if (package_root_dir_components == READ_FILE_AND_PARSE__FIRST_FILE_IN_PACKAGE) {
        package_root_dir_components = path_count_nodes(canonical_filename) - 1;
    }

    usize const start_module_files_len = module->files.len;

    // Read the entire file
    SvMut file_contents;
    int os_err;
    if (yy_is_err(yuceyuf_read_entire_file_into_allocator(yuceyuf_arena_allocator(module->arena), YUCEYUF_AT_FDCWD, canonical_filename, SIZE_MAX, &file_contents, &os_err))) {
        module_add_error_msg(module, prev_filename, prev_file_import_tok_pos,
                             "failed importing file ‘" Sv_Fmt "’: %s", Sv_Arg(canonical_filename), strerror(os_err));
        return Yy_Bad;
    }
    {
        u64 const t = timer_lap(&timer);
        if (mff_options->verbose_timings)
            fprintf(stderr, "%s (" Sv_Fmt ") step `%s` took " TimeMs_Fmt "\n", __func__, Sv_Arg(canonical_filename), "read entire file", TimeMs_Arg(t));
    }


    // Tokenize
    Yuceyuf_Lexer_TokenList *tokens;
    {
        Yuceyuf_ErrorMessage err_msg;
        YyStatus const err =
            yuceyuf_lexer_init_from_contents(canonical_filename, sv_const(file_contents), module->arena, &tokens, &err_msg);
        if (yy_is_err(err)) {
            module_add_error_msg(module, prev_filename, prev_file_import_tok_pos,
                                 "failed tokenizing file ‘" Sv_Fmt "’", Sv_Arg(canonical_filename));
            array_list_append(&module->errors, err_msg);
            return Yy_Bad;
        }
    }
    if (mff_options->verbose_tokens) {
        fprintf(stderr, "# file ‘" Sv_Fmt "’\n", Sv_Arg(canonical_filename));
        Yuceyuf_Lexer_TokenList *tokens_iter = tokens;
        while (true) {
            Yuceyuf_Lexer_Token const *tok = yuceyuf_lexer_next(&tokens_iter);
            yuceyuf_lexer_debug_token_print(tok);
            if (tok->tag == Yuceyuf_Lexer_TokenTag_eof) break;
        }
    }
    {
        u64 const t = timer_lap(&timer);
        if (mff_options->verbose_timings)
            fprintf(stderr, "%s (" Sv_Fmt ") step `%s` took " TimeMs_Fmt "\n", __func__, Sv_Arg(canonical_filename), "tokenize", TimeMs_Arg(t));
    }
    if (mff_options->stop_at_tokens) {
        fprintf(stderr, "(stop_at_tokens)\n");
        exit(0);
    }

    // Parse
    Yuceyuf_Ast_File *const ast_file = yuceyuf_arena_alloc_type(module->arena, Yuceyuf_Ast_File);
    if (yy_is_err(yuceyuf_ast_file_unresolved_from_tokens(module->arena, canonical_filename, package_root_dir_components, tokens, ast_file))) {
        module_add_error_msg(module, ast_file->filename, Yuceyuf_Lexer_TokenPosition_DUMMY, "failed generating ast");
        for (usize i = 0; i < ast_file->errors.len; i++) {
            array_list_append(&module->errors, ast_file->errors.ptr[i]);
        }
        return Yy_Bad;
    }
    yy_assert(ast_file->errors.len == 0);
    {
        u64 const t = timer_lap(&timer);
        if (mff_options->verbose_timings)
            fprintf(stderr, "%s (" Sv_Fmt ") step `%s` took " TimeMs_Fmt "\n", __func__, Sv_Arg(canonical_filename), "parse", TimeMs_Arg(t));
    }
    if (yy_is_err(yuceyuf_ast_file_resolve_symbols(ast_file))) {
        module_add_error_msg(module, ast_file->filename, Yuceyuf_Lexer_TokenPosition_DUMMY, "failed resolving symbols");
        for (usize i = 0; i < ast_file->errors.len; i++) {
            array_list_append(&module->errors, ast_file->errors.ptr[i]);
        }
        return Yy_Bad;
    }
    if (mff_options->verbose_ast) {
        fprintf(stderr, "# file ‘" Sv_Fmt "’\n", Sv_Arg(canonical_filename));
        yuceyuf_ast_debug_print_root(ast_file->root, canonical_filename);
    }
    {
        u64 const t = timer_lap(&timer);
        if (mff_options->verbose_timings)
            fprintf(stderr, "%s (" Sv_Fmt ") step `%s` took " TimeMs_Fmt "\n", __func__, Sv_Arg(canonical_filename), "resolve symbols", TimeMs_Arg(t));
    }

    yy_assert(module->files.len == start_module_files_len);
    array_list_append(&module->files, ast_file);
    *result_ast_file = ast_file;
    return Yy_Ok;
}

NODISCARD static Yuceyuf_Ast_File *OPT module_find_regular_file_for_import(
    Yuceyuf_Module *const module, Sv const canonical_import_path)
{
    u32 const filename_hash_for_module_lookup = yuceyuf_ast_filename_hash_for_module_lookup(canonical_import_path);
    for (usize module_file_idx = 0; module_file_idx < module->files.len; module_file_idx++) {
        Yuceyuf_Ast_File *const module_file = module->files.ptr[module_file_idx];
        if (module_file->filename_hash_for_module_lookup != filename_hash_for_module_lookup)
            continue;
        if (sv_eql(module_file->filename, canonical_import_path))
            return module_file;
    }
    return NULL;
}

NODISCARD static YyStatus module_find_file_or_parse_for_import(
    Yuceyuf_Module *module,
    Yuceyuf_ModuleFromFilenameOptions const *mff_options,
    Yuceyuf_AstNode *import_node)
{
    // TODO: "Any order optimized for LIFO" allocations for `Yy_Arena` (currently 2021-11-09
    // there may be some "leaks" if I use an arena as scratch_allocator)
    Yy_Allocator const scratch_allocator = yuceyuf_arena_allocator(module->arena);
    Yy_Allocator const arena_allocator = yuceyuf_arena_allocator(module->arena);

    yy_assert(import_node->it.tag == Yuceyuf_AstNode_Specific_Tag__imported_file);
    yy_assert(import_node->it.payload.imported_file.resolved_ast_file == NULL);
    Yuceyuf_Lexer_StringLiteral const requested_file_name = import_node->it.payload.imported_file.file_name;

    // If the requested file is "root", return the root file.
    if (sv_eql(requested_file_name.value, sv_lit("root"))) {
        yy_assert(module->files.len > 0);
        import_node->it.payload.imported_file.resolved_ast_file = module->files.ptr[0];
        return Yy_Ok;
    }

    // If the requested file is "std", return the parsed `std.yuceyuf` from `canonical_stdlib_path`.
    if (sv_eql(requested_file_name.value, sv_lit("std"))) {
        if (module->stdlib_ast_file == NULL) {
            if (module->canonical_stdlib_path.ptr == NULL) {
                module_add_error_msg(module, import_node->parent_ast_file->filename, import_node->pos,
                                     "stdlib path is unknown");
                return Yy_Bad;
            }
            // std has not been parsed yet.
            TRY(read_file_and_parse(module, mff_options,
                                    Yy_ErrMsg_Filename_DUMMY, Yuceyuf_Lexer_TokenPosition_DUMMY,
                                    READ_FILE_AND_PARSE__FIRST_FILE_IN_PACKAGE,
                                    module->canonical_stdlib_path, &module->stdlib_ast_file));
            yy_assert(module->stdlib_ast_file != NULL);
        }
        yy_assert(module->stdlib_ast_file != NULL);
        import_node->it.payload.imported_file.resolved_ast_file = module->stdlib_ast_file;
        return Yy_Ok;
    }

    // This is a regular file.
    // Canonicalize the path.
    Sv new_canonical_import_path;
    CATCH(join_paths_and_canonicalize(arena_allocator, scratch_allocator,
                                      import_node->parent_ast_file->package_root_dir_components,
                                      import_node->parent_ast_file->path_dirname,
                                      requested_file_name.value,
                                      &new_canonical_import_path
    ), {
        // TODO: Show why it failed.
        module_add_error_msg(module, import_node->parent_ast_file->filename, import_node->pos,
                             "failed importing file ‘" Sv_Fmt "’: join_paths_and_canonicalize",
                             Sv_Arg(requested_file_name.value));
        return Yy_Bad;
    });

    // Check if the file has already been parsed and imported.
    Yuceyuf_Ast_File *OPT const found_file =
        module_find_regular_file_for_import(module, new_canonical_import_path);
    if (found_file != NULL) {
        allocator_free_array(
            arena_allocator,
            u8, (u8 *)new_canonical_import_path.ptr, new_canonical_import_path.len);
        import_node->it.payload.imported_file.resolved_ast_file = found_file;
        return Yy_Ok;
    }

    // This is a new file. Parse it.
    Yuceyuf_Ast_File *new_ast_file;
    TRY(read_file_and_parse(module, mff_options,
                            import_node->parent_ast_file->filename, import_node->pos,
                            import_node->parent_ast_file->package_root_dir_components,
                            new_canonical_import_path, &new_ast_file));
    yy_assert(new_ast_file != NULL);
    import_node->it.payload.imported_file.resolved_ast_file = new_ast_file;
    return Yy_Ok;
}

/// Make a "module" (the Ast of the entire project) from a filename. In a single compilation only one module exists.
/// TODO: Module is a terrible name.
NODISCARD YyStatus yuceyuf_module_from_filename(
    Yy_Arena *arena,
    Sv root_filename_not_canonical,
    Sv OPT stdlib_path_not_canonical,
    Yuceyuf_ModuleFromFilenameOptions const *mff_options,
    Yuceyuf_Module *result_module
)
{
    // Initialize
    *result_module = Yuceyuf_Module {
        .arena = arena,
        .files = {}, // init later
        .errors = {}, // init later
        .canonical_stdlib_path = {}, // init later
        .stdlib_ast_file = NULL,
    };

    Yy_Allocator const scratch_allocator = yuceyuf_arena_allocator(result_module->arena);
    Yy_Allocator const arena_allocator = yuceyuf_arena_allocator(result_module->arena);

    Timer timer;
    timer_init(&timer);

    array_list_init(&result_module->files, arena_allocator);
    array_list_init(&result_module->errors, arena_allocator);

    // Canonicalize the stdlib path if there is any.
    if (stdlib_path_not_canonical.ptr != NULL) {
        result_module->canonical_stdlib_path =
            path_with_single_slashes(arena_allocator, scratch_allocator, stdlib_path_not_canonical);
    }

    // Canonicalize the root filename.
    Sv const canonical_root_filename =
        path_with_single_slashes(arena_allocator, scratch_allocator, root_filename_not_canonical);

    // Read the first file ("root" package).
    Yuceyuf_Ast_File *first_ast_file;
    if (yy_is_err(read_file_and_parse(result_module, mff_options,
                                      Yy_ErrMsg_Filename_DUMMY, Yuceyuf_Lexer_TokenPosition_DUMMY,
                                      READ_FILE_AND_PARSE__FIRST_FILE_IN_PACKAGE,
                                      canonical_root_filename, &first_ast_file))) {
        yy_assert(result_module->errors.len != 0);
        return Yy_Bad;
    }
    yy_assert(result_module->files.len == 1);

    // Read other imported files.
    for (usize module_file_idx = 0; module_file_idx < result_module->files.len; module_file_idx++) {
        Yuceyuf_Ast_File *const module_file = result_module->files.ptr[module_file_idx];
        yy_assert(module_file->root->it.tag == Yuceyuf_AstNode_Specific_Tag__root);
        for (usize queued_import_idx = 0;
                queued_import_idx < module_file->queued_imported_files.len;
                queued_import_idx++) {
            Yuceyuf_AstNode *const qif = module_file->queued_imported_files.ptr[queued_import_idx];
            yy_assert(qif->it.tag == Yuceyuf_AstNode_Specific_Tag__imported_file);
            yy_assert(qif->it.payload.imported_file.resolved_ast_file == NULL);
            if (yy_is_ok(module_find_file_or_parse_for_import(result_module, mff_options, qif))) {
                yy_assert(qif->it.payload.imported_file.resolved_ast_file != NULL);
            } else {
                // There were errors trying to read and parse the file. We can continue parsing the other files.
                yy_assert(result_module->errors.len > 0);
            }
        }
    }

    // There might have been errors.
    if (result_module->errors.len > 0)
        return Yy_Bad;

    // Do a few redundant checks.
    for (usize module_file_idx = 0; module_file_idx < result_module->files.len; module_file_idx++) {
        Yuceyuf_Ast_File *const module_file = result_module->files.ptr[module_file_idx];
        yy_assert(module_file->errors.len == 0);
        for (usize queued_import_idx = 0;
                queued_import_idx < module_file->queued_imported_files.len;
                queued_import_idx++) {
            Yuceyuf_AstNode *const qif = module_file->queued_imported_files.ptr[queued_import_idx];
            yy_assert(qif->it.tag == Yuceyuf_AstNode_Specific_Tag__imported_file);
            yy_assert(qif->it.payload.imported_file.resolved_ast_file != NULL);
        }
    }

    {
        u64 const t = timer_lap(&timer);
        if (mff_options->verbose_timings)
            fprintf(stderr, "%s step `%s` took " TimeMs_Fmt "\n", __func__, "total", TimeMs_Arg(t));
    }

    // stop_at_ast should stop only after all files have been parsed and their (basic) symbols resolved.
    if (mff_options->stop_at_ast) {
        fprintf(stderr, "(stop_at_ast)\n");
        exit(0);
    }

    // Everything has been parsed and is ready to be compiled.
    yy_assert(result_module->errors.len == 0);
    return Yy_Ok;
}
