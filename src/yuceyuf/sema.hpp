#ifndef ___INCLUDE_GUARD__0eaVELoqvhC01Xm0B3SgTIjcfiGGMo7MRCL28CJb
#define ___INCLUDE_GUARD__0eaVELoqvhC01Xm0B3SgTIjcfiGGMo7MRCL28CJb

#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/array_list_printf.hpp"
#include "yy/meta/enum.hpp"
#include "yuceyuf/genir.hpp"

#include <new>

struct Sema;


enum class BasicInternedComptimeValue: u8 {
    const_integer_0,
    const_integer_1,
    const_integer_neg1,

    const_xtrue,
    const_xfalse,
    const_xundefined,
    const_xnoreturn,
    const_xvoid,

    const_type_type,
    const_type_uintword,
    const_type_xu8,
    const_type_xundefined,
    const_type_xvoid,
    const_type_xbool,
    const_type_xnoreturn,

    const_type_ptr_to_uintword,
    const_type_ptr_to_xu8,
    const_type_ptr_to_xvoid,
    const_type_ptr_to_xbool,

    COUNT_,
};

struct InternedComptimeValueId {
    u32 x;
    friend constexpr auto operator==(InternedComptimeValueId, InternedComptimeValueId) -> bool = default;
    constexpr InternedComptimeValueId() = default;
    constexpr InternedComptimeValueId(u32 x) : x(x) {}
    constexpr InternedComptimeValueId(BasicInternedComptimeValue x) : x(static_cast<u32>(x)) {}
    YY_DECL_INVALID_OF_INSIDE_CLASS(InternedComptimeValueId, InternedComptimeValueId{ UINT32_MAX });
};

struct SemaExtraIndex {
    u32 x;
    friend constexpr auto operator==(SemaExtraIndex, SemaExtraIndex) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(SemaExtraIndex, SemaExtraIndex{ .x = UINT32_MAX });
};
static_assert(sizeof(Option<SemaExtraIndex>) == sizeof(SemaExtraIndex));

struct SemaTypeId {
    SemaExtraIndex sei;
    friend constexpr auto operator==(SemaTypeId, SemaTypeId) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(SemaTypeId, SemaTypeId{ .sei = SemaExtraIndex::yy_invalidOf_StructType_make_invalid_of() });
};


struct TypeMemoryStuff {
    u64 size_of;
    u64 align_of;
};


// TODO: Integer tag with payload for signedness and bits.
#define YY_META_ENUM__NAME SemaType_Tag
#define YY_META_ENUM__ALL_MEMBERS                     \
    YY_META_ENUM__MEMBER(type)                        \
    YY_META_ENUM__MEMBER2(xundefined, "undefined")    \
    YY_META_ENUM__MEMBER2(xvoid, "void")              \
    YY_META_ENUM__MEMBER2(xnoreturn, "noreturn")      \
    YY_META_ENUM__MEMBER(uintword)                    \
    YY_META_ENUM__MEMBER2(xu8, "u8")                  \
    YY_META_ENUM__MEMBER2(xbool, "bool")              \
    YY_META_ENUM__MEMBER2(xstruct, "struct")          \
    YY_META_ENUM__MEMBER(function)                    \
    YY_META_ENUM__MEMBER(pointer)                     \
YY_EMPTY_MACRO
#include "yy/meta/enum.hpp"
static_assert(sizeof(SemaType_Tag) == sizeof(u32) && alignof(SemaType_Tag) == alignof(u32));

namespace SemaTypePacked {

#pragma GCC diagnostic push
#pragma GCC diagnostic error "-Wpadded"

// TODO: concept PackedConcept = ...;
struct Base {
    SemaType_Tag const tag;

    // TODO: && std::has_unique_object_representations_v<Derived>
    template <typename Derived>
        requires std::is_standard_layout_v<Derived> && requires (Derived& d) {
            { d.base } -> yy::same_as_remove_cvref<Base>;
        }
    auto get() const -> Derived const* {
        yy_assert(tag == Derived::tag);
        static_assert(offsetof(Derived, base) == 0);
        return reinterpret_cast<Derived const*>(this);
    }
    template <typename Derived>
        requires std::is_standard_layout_v<Derived> && requires (Derived& d) {
            { d.base } -> yy::same_as_remove_cvref<Base>;
        }
    auto get() -> Derived* {
        yy_assert(tag == Derived::tag);
        static_assert(offsetof(Derived, base) == 0);
        return reinterpret_cast<Derived*>(this);
    }

    Base(SemaType_Tag t) : tag(t) {}
    Base(Base &&) = delete;
    auto operator=(Base &&) -> Base& = delete;
    Base(Base const&) = delete;
    auto operator=(Base const&) -> Base& = delete;
};
static_assert(std::is_standard_layout_v<Base>);

inline constexpr auto tag_is_no_payload(SemaType_Tag tag) -> bool {
    switch (tag) {
        case MetaEnum_count(SemaType_Tag):
            yy_unreachable();
        case SemaType_Tag__type:
        case SemaType_Tag__xundefined:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xnoreturn:
            return true;
        case SemaType_Tag__pointer:
        case SemaType_Tag__function:
        case SemaType_Tag__xstruct:
            return false;
    }
    yy_unreachable();
}

template <typename Derived, SemaType_Tag TAG>
    requires (tag_is_no_payload(TAG))
struct NoPayload {
    static constexpr auto tag = TAG;
    Base base{ tag };

    NoPayload() = default;

    NODISCARD constexpr auto total_extra_count() const -> u32 {
        u32 result = sizeof(*this) / sizeof(u32);
        return result;
    }
    NODISCARD auto as_bytes() const -> Span<u8 const> { return { reinterpret_cast<u8 const*>(this), total_extra_count() * sizeof(u32) }; }

    operator Base const*() const { return &base; }
};

struct Type : NoPayload<Type, SemaType_Tag__type> {};
struct Undefined : NoPayload<Void, SemaType_Tag__xundefined> {};
struct Void : NoPayload<Void, SemaType_Tag__xvoid> {};
struct Bool : NoPayload<Bool, SemaType_Tag__xbool> {};
struct Uintword : NoPayload<Uintword, SemaType_Tag__uintword> {};
struct Uint8 : NoPayload<Uint8, SemaType_Tag__xu8> {};
struct Noreturn : NoPayload<Noreturn, SemaType_Tag__xnoreturn> {};

struct Pointer {
    static constexpr auto tag = SemaType_Tag__pointer;
    Base base{ tag };
    SemaTypeId child_type;

    Pointer(SemaTypeId child) : child_type(child) {}

    NODISCARD constexpr auto total_extra_count() const -> u32 { return sizeof(*this) / sizeof(u32); }
    NODISCARD auto as_bytes() const -> Span<u8 const> { return { reinterpret_cast<u8 const*>(this), total_extra_count() * sizeof(u32) }; }

    operator Base const*() const { return &base; }
};

static_assert(sizeof(Option<NonMaxU64>) == sizeof(NonMaxU64));
struct Struct {
    static constexpr auto tag = SemaType_Tag__xstruct;
    Base base{ tag };

    u32 struct_gen_id;
    ByParts<DeclId> origin_decl_id;
    Yy_Ir_InstIndex origin_inst_idx;
    u32 member_count;
    enum class Status {
        nothing,
        doing_size,
        sized,
    };
    Status status{Status::nothing};
    ByParts<TypeMemoryStuff> cached_size_align{yy::uninitialized};

    // Dynamic size.
    ~Struct() = delete;

    struct Member {
        Option<IdentifierId> name;
        SemaTypeId type;
        ByParts<u64> offset;
    };
    static_assert(alignof(Member) <= alignof(u32));
    static_assert(std::has_unique_object_representations_v<Member>);

    NODISCARD auto resolved_members() const -> Span<Member const> {
        yy_assert(status == Status::sized);
        return {reinterpret_cast<Member const*>(this + 1), member_count};
    }
    NODISCARD auto unresolved_members() const -> Span<Member const> {
        yy_assert(status != Status::sized);
        return {reinterpret_cast<Member const*>(this + 1), member_count};
    }
    NODISCARD auto unresolved_members() -> Span<Member> {
        yy_assert(status != Status::sized);
        return {reinterpret_cast<Member*>(this + 1), member_count};
    }

    NODISCARD auto total_extra_count() const -> u32 { return get_allocation_byte_size(member_count) / static_cast<u32>(sizeof(u32)); }
    NODISCARD static auto get_allocation_byte_size(u32 member_count) -> u32 {
        return static_cast<u32>(sizeof(Struct) + member_count*sizeof(Member));
    }
    NODISCARD auto as_bytes() const -> Span<u8 const> { return { reinterpret_cast<u8 const*>(this), total_extra_count() * sizeof(u32) }; }

    operator Base const*() const { return &base; }
private:
    friend struct StructInit;
    Struct(DeclId origin_decl_id, Yy_Ir_InstIndex origin_inst_idx, u32 struct_gen_id, u32 init_member_count)
        : struct_gen_id(struct_gen_id), origin_decl_id(origin_decl_id), origin_inst_idx(origin_inst_idx), member_count(init_member_count) {}
};
static_assert(std::has_unique_object_representations_v<Struct>);

// A helper to initialize Struct.
struct StructInit {
private:
    Yy_Allocator allocator;
    usize alloc_size;
    Struct* tmp_struct_type;
    u32 member_count;
    u32 _padding{0};
public:
    auto deinit() -> void {
        yy_assert(tmp_struct_type != nullptr);
        allocator_free(allocator, tmp_struct_type, alloc_size);
        tmp_struct_type = nullptr;
        (void) _padding;
    }
    // NOTE: You must manualy call deinit after initialization.
    StructInit(
        Yy_Allocator init_allocator,
        DeclId origin_decl_id,
        Yy_Ir_InstIndex origin_inst_idx,
        u32 struct_gen_id,
        u32 init_member_count
    ) {
        allocator = init_allocator;
        member_count = init_member_count;
        alloc_size = SemaTypePacked::Struct::get_allocation_byte_size(member_count);
        tmp_struct_type = reinterpret_cast<SemaTypePacked::Struct *>(
            allocator_alloc_align_size(allocator, std::alignment_of_v<SemaTypePacked::Struct>, alloc_size));
        allocator_assert_ok(tmp_struct_type);
        new (tmp_struct_type) Struct(origin_decl_id, origin_inst_idx, struct_gen_id, member_count);
        yy_debug_invalidate_ptr_len(
            tmp_struct_type->unresolved_members().data(), tmp_struct_type->unresolved_members().size()
        );
    }
    auto finish() -> Struct const* {
        (void) this;
        return tmp_struct_type;
    }
};

struct Function {
    static constexpr auto tag = SemaType_Tag__function;
    Base base{ tag };

    SemaTypeId return_type;
    u32 arg_count;
    // SemaTypeId trailing_args[0];

    // Function can't be on the stack since it has dynamic size.
    ~Function() = delete;

    NODISCARD auto arg_type_mut(usize const i) -> SemaTypeId& {
        yy_assert(i < arg_count);
        return reinterpret_cast<SemaTypeId*>(this + 1)[i];
    }
    NODISCARD auto arg_type(usize const i) const -> SemaTypeId {
        yy_assert(i < arg_count);
        return reinterpret_cast<SemaTypeId const*>(this + 1)[i];
    }

    NODISCARD auto total_extra_count() const -> u32 { return get_allocation_byte_size(arg_count) / static_cast<u32>(sizeof(u32)); }
    NODISCARD static auto get_allocation_byte_size(u32 arg_count) -> u32 { return static_cast<u32>(sizeof(Function)) + arg_count*static_cast<u32>(sizeof(SemaTypeId)); }
    NODISCARD auto as_bytes() const -> Span<u8 const> { return { reinterpret_cast<u8 const*>(this), total_extra_count() * sizeof(u32) }; }

    operator Base const*() const { return &base; }
private:
    friend struct FunctionInit;
    Function(u32 init_arg_count) : return_type(yy::uninitialized), arg_count(init_arg_count) {}
};

#pragma GCC diagnostic pop

// A helper to initialize Function.
struct FunctionInit {
private:
    Yy_Allocator allocator;
    usize alloc_size;
    Function* tmp_fn_type;
    u32 arg_count;
    u32 next_arg_to_insert{0};
public:
    auto deinit() -> void {
        yy_assert(tmp_fn_type != nullptr);
        allocator_free(allocator, tmp_fn_type, alloc_size);
        tmp_fn_type = nullptr;
    }
    // NOTE: You must manualy call deinit after initialization.
    FunctionInit(Yy_Allocator init_allocator, u32 init_arg_count) {
        allocator = init_allocator;
        arg_count = init_arg_count;
        alloc_size = SemaTypePacked::Function::get_allocation_byte_size(arg_count);
        tmp_fn_type = reinterpret_cast<SemaTypePacked::Function *>(
            allocator_alloc_align_size(allocator, std::alignment_of_v<SemaTypePacked::Function>, alloc_size));
        allocator_assert_ok(tmp_fn_type);
        new (tmp_fn_type) Function(arg_count);
    }
    auto set_arg_type(u32 idx, SemaTypeId type) -> void {
        yy_assert(idx == next_arg_to_insert && idx < arg_count);
        next_arg_to_insert += 1;
        tmp_fn_type->arg_type_mut(idx) = type;
    }
    NODISCARD auto finish(SemaTypeId init_return_type) -> Function const* {
        (void) this;
        yy_assert(next_arg_to_insert == arg_count);
        tmp_fn_type->return_type = init_return_type;
        return tmp_fn_type;
    }
};

}


struct SemaTypeTableContext {
    Sema *sema;
    SemaTypeTableContext(Sema* sema_) : sema(sema_) {}
    NODISCARD auto eql(SemaTypePacked::Base const* const a, SemaTypeId const b_id, usize const b_table_index) const -> bool;
    NODISCARD auto hash(SemaTypePacked::Base const* type) const -> u32;
};

using SemaTypeTable = NewerHashMap<SemaTypeId, Void, SemaTypeTableContext>;

struct ComptimeInteger {
    // TODO: BigInt.
    i64 value;
    Option<SemaTypeId> int_type_new{};
};

struct ComptimeString {
    Yy_Ir_StringSized0 string;
};

#define YY_META_TAGGED_UNION__NAME ComptimeValue
#define YY_META_TAGGED_UNION__ALL_MEMBERS                           \
    YY_META_TAGGED_UNION__MEMBER2(Void, xundefined, "undefined")    \
    YY_META_TAGGED_UNION__MEMBER2(Void, xvoid, "void")              \
    YY_META_TAGGED_UNION__MEMBER2(Void, xnoreturn, "noreturn")      \
    YY_META_TAGGED_UNION__MEMBER(ComptimeInteger, integer)          \
    YY_META_TAGGED_UNION__MEMBER(ComptimeString, string)            \
    YY_META_TAGGED_UNION__MEMBER2(bool, xbool, "bool")              \
    YY_META_TAGGED_UNION__MEMBER(SemaTypeId, type)                  \
YY_EMPTY_MACRO
#include "yy/meta/tagged_union.hpp"

struct SiiBlockLike {
    Option<Yy_Ir_InstIndex> first_breakd_from_inst{None};
};
struct SiiDeclLike {
    Option<SemaTypeId> resolved_decl_type;
    Option<InternedComptimeValueId> comptime_value_for_global_var_decl_init{None};
};

// TODO: Meta tagged union: YY_META_TAGGED_UNION__NO_STORE_TAG: Make `tag` private on debug builds. Don't include
// `tag` in release builds.
#define YY_META_TAGGED_UNION__NAME SiiSpecific
#define YY_META_TAGGED_UNION__ALL_MEMBERS                              \
    YY_META_TAGGED_UNION__MEMBER(Void, none)                           \
    /* For blocks, loops and cond branches. */                              \
    YY_META_TAGGED_UNION__MEMBER(SiiBlockLike, block_like)             \
    /* For `local_var_decl`, `extern_var_decl`, `global_var_decl`, `fn_arg_decl`,
     * `extern_fn_decl` and `fn_decl`. */                                   \
    YY_META_TAGGED_UNION__MEMBER(SiiDeclLike, decl_like)               \
YY_EMPTY_MACRO
#include "yy/meta/tagged_union.hpp"


struct SemaInstInfoNew : yy::MoveOnly {
    // TODO: Proper constructor and stuff
    enum class Status : u8 {
        nothing,
        doing,
        full,
    };
    Status status{Status::nothing};
    // This is <= gi.insts[inst].untyped_use_cuont
    u8 build_use_count;
    // If comptime_only, assert(resolved_comptime_value.has_value()).
    bool comptime_only;

    SemaTypeId type3;

    // The value can _not_ be used in nested blocks.
    // Instructions can only be referenced in the same `block`.
    // This changes every time you reference the instruction.
    // This is used for local instructions.
    Option<Yy_Ir_InstIndex> build_last_ref{};
    SiiSpecific specific;

    // This is for regular values, not for decls.
    Option<InternedComptimeValueId> resolved_comptime_value{};

    auto init_block_like() -> void {
        yy_assert(status == Status::nothing);
        *this = {
            .status = Status::doing,
            .build_use_count = 0,
            .comptime_only = false,
            .type3 = yy::uninitialized,
            .build_last_ref = None,
            .specific = MetaTu_init(SiiSpecific, block_like, {}),
            .resolved_comptime_value = None,
        };
    }

    NODISCARD auto is_nothing() const -> bool { return status == Status::nothing; }
    NODISCARD auto is_doing() const -> bool { return status == Status::doing; }
    NODISCARD auto is_full() const -> bool { return status == Status::full; }
};
static_assert(std::is_default_constructible_v<SemaInstInfoNew>);
static_assert(std::is_trivially_move_assignable_v<SemaInstInfoNew>);

struct SemaTargetInfo {
    TypeMemoryStuff ptr_size_align;
    TypeMemoryStuff uintword_size_align;
};

template <typename Element, typename Index, typename UsizeIndex = void>
    requires std::constructible_from<usize, Index> && (!std::same_as<Index, usize>) &&
             (std::same_as<UsizeIndex, usize> || std::same_as<UsizeIndex, void>)
struct StrongIndexSpan : public Span<Element> {
    using Span<Element>::Span;
    using Span<Element>::operator=;
    using Span<Element>::operator[];
    using Span<Element>::operator==;
    NODISCARD constexpr auto operator[](Index idx) const -> Element& {
        return static_cast<Span<Element> const&>(*this)[usize(idx)];
    }
    NODISCARD constexpr auto operator[](usize idx) const -> Element&
        requires (std::same_as<UsizeIndex, void>)
    = delete;
};

using SemaInstInfoList = StrongIndexSpan<SemaInstInfoNew, Yy_Ir_InstIndex>;


struct ComptimeValuePool {
    explicit ComptimeValuePool(Yy_Allocator allocator) : items(allocator) {}
    Vec<ComptimeValue> items;
    NODISCARD auto operator[](InternedComptimeValueId id) const -> ComptimeValue const& { return items[id.x]; }
    NODISCARD auto push_back(std::convertible_to<ComptimeValue> auto&& v) -> InternedComptimeValueId {
        auto result = yy::checked_int_cast<u32>(items.size()).unwrap();
        items.push_back(std::forward<decltype(v)>(v));
        return {result};
    }
    NODISCARD auto size() const -> usize { return items.size(); }
};

struct Sema {
    GenIr *gi;

    Yy_Allocator heap;
    Yy_ArenaCpp arena;
    Yy_ArenaCpp scratch;

    LockCounter extra_pointers_lock;

    Yuceyuf_ErrorBundle errors;

    VecUnmanagedLeaked<u32> extra{};

    SemaInstInfoList sema_inst_infos;

    SemaExtraIndex basic_interned_types;
    SemaTypeTable types;
    ComptimeValuePool comptime_value_pool;
    NewerHashMap<DeclId, ComptimeValue> decl_comptime_values;
    SemaTargetInfo target_info;

    NODISCARD auto has_errors() const -> bool {
        if (errors.soft_count > 0 || errors.hard_count > 0 || errors.fatal)
            yy_assert(errors.msgs.size() > 0 || gi->has_errors());
        else yy_assert(errors.msgs.size() == 0);
        return gi->has_errors() || errors.msgs.size() > 0;
    }

    Sema(Yy_Allocator heap, GenIr* gi);
    ~Sema();
    Sema(const Sema&) = delete;
    Sema& operator=(const Sema&) = delete;
    Sema(Sema&&) = default;
    Sema& operator=(Sema&&) = default;

    auto inst_info_full(Yy_Ir_InstIndex inst) -> SemaInstInfoNew const& {
        (void) this;
        auto& result = sema_inst_infos[inst];
        yy_assert(result.status == SemaInstInfoNew::Status::full);
        return result;
    }
    auto inst_info_full_mut(Yy_Ir_InstIndex inst) -> SemaInstInfoNew& {
        (void) this;
        auto& result = sema_inst_infos[inst];
        yy_assert(result.status == SemaInstInfoNew::Status::full);
        return result;
    }
};

NODISCARD auto yy_sema_analyze_ir(Yy_Allocator const heap, GenIr *const gi) -> Sema;

NODISCARD bool typeIsUndefined(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsNoreturn(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsVoid(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsType(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsPointer(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIdEql(Sema *const sema, SemaTypeId const a, SemaTypeId const b);
NODISCARD TypeMemoryStuff typeIdMemoryStuff(Sema *const sema, SemaTypeId const type_id);
NODISCARD u64 typeIdMemorySizeOf(Sema *const sema, SemaTypeId const type_id);
NODISCARD bool typeIsZeroSized(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsInteger(Sema *const sema, SemaTypeId const type);
NODISCARD bool typeIsComptimeOnly(Sema *const sema, SemaTypeId const type);
NODISCARD SemaType_Tag decodeTypeTag(Sema const *const sema, SemaTypeId const type);
NODISCARD SemaTypePacked::Base const* typeBase(Sema* const sema, SemaTypeId type);
NODISCARD SemaTypeId typePointerTo(Sema *const sema, SemaTypeId const child_type);
NODISCARD SemaTypeId typePointerToAnyopaque(Sema *const sema);
NODISCARD SemaTypeId typeChild(Sema *const sema, SemaTypeId const type);
auto fmtSemaTypeId(ArrayList_Of_u8 *const writer, Sema *const sema, SemaTypeId const type_id) -> void;
auto fmtComptimeValue(ArrayList_Of_u8 *const writer, Sema *const sema, ComptimeValue const& value) -> void;

extern bool verbose_sema;

#endif//___INCLUDE_GUARD__0eaVELoqvhC01Xm0B3SgTIjcfiGGMo7MRCL28CJb
