#include "yuceyuf/ast.hpp"
#include "yuceyuf/lexer.hpp"
#include "yuceyuf/path_builder.hpp"
#include "yy/hash.hpp"

#include <stdio.h>
#include <stdarg.h>
#include <tuple>


COLD static void ast_add_err_msg__(bool is_note, Yuceyuf_Ast_File *ctx, LexSourceRegion src_region, char const *fmt, va_list ap)
{
    char tmp_buf[1024];

    isize const res = vsnprintf(tmp_buf, sizeof(tmp_buf), fmt, ap);
    yy_assert(res >= 0);
    usize const written_size = yy_min((usize)res, sizeof(tmp_buf) - 1);

    Sv const msg = allocator_dup_sv(ctx->arena->allocator(), Sv(tmp_buf, written_size));
    Yuceyuf_ErrorMessage const error = {
        .filename = ctx->filename,
        .src_region = src_region.to_src_region_line_and_column(ctx->lexer.lex.line_info),
        .is_note = is_note,
        .msg = msg,
    };
    ctx->errors.push_back(error);
}

PRINTF_LIKE(3, 4)
static void ast_add_error_msg(Yuceyuf_Ast_File *ctx, LexSourceRegion src_region, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    ast_add_err_msg__(false, ctx, src_region, fmt, ap);
    va_end(ap);
}

PRINTF_LIKE(3, 4)
static void ast_add_error_note(Yuceyuf_Ast_File *ctx, LexSourceRegion src_region, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    ast_add_err_msg__(true, ctx, src_region, fmt, ap);
    va_end(ap);
}


static void ast_add_error_msg__fancy(Yuceyuf_Ast_File *ctx, Sv msg, TokenId const token_id)
{
    auto& tok = ctx->lexer.lex.token(token_id);
    auto& tok_src = ctx->lexer.lex.token_src(token_id);
    Sv const fancy_sv = tok.fancy_name();
    char const *const fancy_quote_begin = "‘";
    char const *const fancy_quote_end = "’";
    bool do_quote = false;
    switch (tok.data.tag) {
        case MetaEnum_count(TokenData_Tag):
            yy_unreachable();
        case TokenData_Tag__simple:
        case TokenData_Tag__keyword:
        case TokenData_Tag__builtin_fn:
            do_quote = true;
            break;
        case TokenData_Tag__eof:
        case TokenData_Tag__identifier:
        case TokenData_Tag__string_literal:
        case TokenData_Tag__integer_literal:
        case TokenData_Tag__char_literal:
            do_quote = false;
            break;
    }
    ast_add_error_msg(ctx, tok_src, Sv_Fmt " %s%s" Sv_Fmt "%s",
                      Sv_Arg(msg),
                      do_quote ? fancy_quote_begin : "",
                      tok.data.tag == TokenData_Tag__builtin_fn ? "@" : "",
                      Sv_Arg(fancy_sv),
                      do_quote ? fancy_quote_end : "");
}

NODISCARD static YyStatus ast_no_prev_node(
    Yuceyuf_Ast_File *ctx, TokenId const tok, Yuceyuf_AstNode *prev_ast_node)
{
    if (prev_ast_node != NULL) {
        ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), tok);
        return Yy_Bad;
    }
    return Yy_Ok;
}

NODISCARD static YyStatus ast_bin_op_must_have_lhs(
    Yuceyuf_Ast_File *ctx, TokenId const tok, Yuceyuf_AstNode *prev_ast_node)
{
    Sv const fancy_sv = ctx->lexer.lex.token(tok).fancy_name();
    if (prev_ast_node == NULL) {
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(tok), "binary operator ‘" Sv_Fmt "’ without left-hand side expression",
            Sv_Arg(fancy_sv)
        );
        return Yy_Bad;
    }
    return Yy_Ok;
}

NODISCARD static YyStatus ast_unary_prefix_op_must_not_have_lhs(
    Yuceyuf_Ast_File *ctx, TokenId const tok, Yuceyuf_AstNode *prev_ast_node)
{
    Sv const fancy_sv = ctx->lexer.lex.token(tok).fancy_name();
    if (prev_ast_node != NULL) {
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(tok), "unary prefix operator ‘" Sv_Fmt "’ has left-hand side expression",
            Sv_Arg(fancy_sv)
        );
        return Yy_Bad;
    }
    return Yy_Ok;
}

NODISCARD static YyStatus ast_unary_postfix_op_must_have_lhs(
    Yuceyuf_Ast_File *ctx, TokenId const tok, Yuceyuf_AstNode *prev_ast_node)
{
    Sv const fancy_sv = ctx->lexer.lex.token(tok).fancy_name();
    if (prev_ast_node == NULL) {
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(tok),
            "unary postfix operator ‘" Sv_Fmt "’ doesn't have left-hand side expression", Sv_Arg(fancy_sv)
        );
        return Yy_Bad;
    }
    return Yy_Ok;
}


NODISCARD static bool token_is_end_of_expr(Token const& tok)
{
    switch (tok.data.tag) {
        case MetaEnum_count(TokenData_Tag):
            yy_unreachable();
        case TokenData_Tag__eof:
            return true;
        case TokenData_Tag__simple: {
            switch (*MetaTu_get(TokenData, &tok.data, simple)) {
                case TokenSimple__rparen:
                case TokenSimple__rbrace:
                case TokenSimple__rbracket:
                case TokenSimple__semicolon:
                case TokenSimple__comma:
                case TokenSimple__assign:
                case TokenSimple__assign_plus:
                case TokenSimple__assign_minus:
                case TokenSimple__assign_mul:
                case TokenSimple__assign_plus_wrap:
                case TokenSimple__assign_minus_wrap:
                case TokenSimple__assign_mul_wrap:
                case TokenSimple__assign_div:
                case TokenSimple__assign_mod:
                case TokenSimple__assign_bits_and:
                case TokenSimple__assign_bits_or:
                case TokenSimple__assign_bits_xor:
                case TokenSimple__assign_sh_left:
                case TokenSimple__assign_sh_right:
                    return true;
                default:
                    return false;
            }
        }
        break;
        case TokenData_Tag__keyword: {
            switch (*MetaTu_get(TokenData, &tok.data, keyword)) {
                case MetaEnum_count(TokenKeyword):
                    yy_unreachable();
                case TokenKeyword__fn:
                case TokenKeyword__mod:
                case TokenKeyword__xextern:
                case TokenKeyword__xstruct:
                case TokenKeyword__ximport:
                case TokenKeyword__xcontinue:
                case TokenKeyword__xbreak:
                case TokenKeyword__xreturn:
                case TokenKeyword__xvar:
                case TokenKeyword__xwhile:
                case TokenKeyword__xif:
                case TokenKeyword__xtrue:
                case TokenKeyword__xfalse:
                case TokenKeyword__xundefined:
                case TokenKeyword__xnoreturn:
                case TokenKeyword__xvoid:
                case TokenKeyword__xbool:
                case TokenKeyword__uintword:
                case TokenKeyword__xu8:
                case TokenKeyword__unreach:
                case TokenKeyword__bool_and:
                case TokenKeyword__bool_or:
                case TokenKeyword__underscore:
                    return false;
                case TokenKeyword__xelse:
                    return true;
            }
        }
        break;
        case TokenData_Tag__string_literal:
        case TokenData_Tag__integer_literal:
        case TokenData_Tag__char_literal:
        case TokenData_Tag__builtin_fn:
        case TokenData_Tag__identifier:
            return false;
    }
    yy_unreachable();
}

NODISCARD static YyStatus expect_token_simple(Yuceyuf_Ast_File *ctx, TokenSimple simple)
{
    Sv const expected_sv = MetaEnum_sv_short(TokenSimple, simple);
    auto const token_id = ctx->lexer.peek();
    auto& tok = ctx->lexer.lex.token(token_id);
    Sv const got_str = tok.fancy_name();
    if (!tok.is_simple(simple)) {
        ast_add_error_msg(ctx, ctx->lexer.lex.token_src(token_id), "expected ‘" Sv_Fmt "’ but got ‘" Sv_Fmt "’",
                          Sv_Arg(expected_sv), Sv_Arg(got_str));
        return Yy_Bad;
    }
    ctx->lexer.advance();
    return Yy_Ok;
}

NODISCARD static YyStatus expect_token_keyword(Yuceyuf_Ast_File *ctx, TokenKeyword keyword)
{
    Sv const expected_str = MetaEnum_sv_short(TokenKeyword, keyword);
    auto const token_id = ctx->lexer.peek();
    auto& tok = ctx->lexer.lex.token(token_id);
    Sv const got_str = tok.fancy_name();
    if (!tok.is_keyword(keyword)) {
        ast_add_error_msg(ctx, ctx->lexer.lex.token_src(token_id), "expected keyword ‘" Sv_Fmt "’ but got ‘" Sv_Fmt "’",
                          Sv_Arg(expected_str), Sv_Arg(got_str));
        return Yy_Bad;
    }
    ctx->lexer.advance();
    return Yy_Ok;
}

NODISCARD static YyStatus expect_token_identifier(Yuceyuf_Ast_File *ctx, TokenIdentifier *result_identifier)
{
    auto const token_id = ctx->lexer.peek();
    auto& tok = ctx->lexer.lex.token(token_id);
    Sv const got_str = tok.fancy_name();
    if (tok.data.tag != TokenData_Tag__identifier) {
        ast_add_error_msg(ctx, ctx->lexer.lex.token_src(token_id), "expected identifier but got ‘" Sv_Fmt "’",
                          Sv_Arg(got_str));
        return Yy_Bad;
    }
    ctx->lexer.advance();
    *result_identifier = *MetaTu_get(TokenData, &tok.data, identifier);
    return Yy_Ok;
}

// Usage: `auto ptr = create_ast_node(); *ptr = Yuceyuf_AstNode{ ... }`
NODISCARD static Yuceyuf_AstNode *create_ast_node(Yuceyuf_Ast_File *ctx)
{
    Yuceyuf_AstNode* const node = allocator_new_default_construct<Yuceyuf_AstNode>(ctx->arena->allocator());
    return node;
}

static void ast_list_append(Yuceyuf_Ast_File* ctx, Yuceyuf_AstList *list, Yuceyuf_AstNode *node)
{
    list->nodes.push_back(ctx->arena->allocator(), node);
}

NODISCARD static Yuceyuf_AstList ast_list_init_empty(void)
{
    return Yuceyuf_AstList {};
}

NODISCARD static bool peek_simple(Yuceyuf_Ast_File *ctx, TokenSimple simple)
{
    auto const peeked_id = ctx->lexer.peek();
    auto& peeked = ctx->lexer.lex.token(peeked_id);
    return peeked.is_simple(simple);
}

NODISCARD static bool peek_keyword(Yuceyuf_Ast_File *ctx, TokenKeyword keyword)
{
    auto const peeked_id = ctx->lexer.peek();
    auto& peeked = ctx->lexer.lex.token(peeked_id);
    return peeked.is_keyword(keyword);
}

NODISCARD static YyStatus expect_token_identifier_or_underscore(
    Yuceyuf_Ast_File* ctx, Option<TokenIdentifier>* result_identifier
) {
    if (peek_keyword(ctx, TokenKeyword__underscore)) {
        ctx->lexer.advance();
        *result_identifier = None;
        return Yy_Ok;
    } else {
        TRY(expect_token_identifier(ctx, result_identifier->unsafe_out_ptr_assume_init()));
        return Yy_Ok;
    }
}

NODISCARD bool yuceyuf_ast_node_requires_stmt_terminator(Yuceyuf_AstNode_Specific_Tag node_tag)
{
    switch (node_tag) {
        case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
            yy_unreachable();
        case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info:
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info:
        case Yuceyuf_AstNode_Specific_Tag__struct_decl_member_info:
            yy_unreachable();
        case Yuceyuf_AstNode_Specific_Tag__root:
            return false;
        case Yuceyuf_AstNode_Specific_Tag__struct_decl:
        case Yuceyuf_AstNode_Specific_Tag__fn_decl:
        case Yuceyuf_AstNode_Specific_Tag__mod_decl:
            return false;
        case Yuceyuf_AstNode_Specific_Tag__local_var_decl:
        case Yuceyuf_AstNode_Specific_Tag__global_var_decl:
        case Yuceyuf_AstNode_Specific_Tag__extern_var_decl:
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl:
        case Yuceyuf_AstNode_Specific_Tag__imported_file:
            return true;
        case Yuceyuf_AstNode_Specific_Tag__field_access:
        case Yuceyuf_AstNode_Specific_Tag__mod_field_access:
        case Yuceyuf_AstNode_Specific_Tag__identifier:
        case Yuceyuf_AstNode_Specific_Tag__fn_call:
        case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call:
        case Yuceyuf_AstNode_Specific_Tag__integer_literal:
        case Yuceyuf_AstNode_Specific_Tag__char_literal:
        case Yuceyuf_AstNode_Specific_Tag__string_literal:
        case Yuceyuf_AstNode_Specific_Tag__op_bin:
        case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix:
        case Yuceyuf_AstNode_Specific_Tag__bool_literal:
        case Yuceyuf_AstNode_Specific_Tag__undefined_literal:
        case Yuceyuf_AstNode_Specific_Tag__builtin_typ:
        case Yuceyuf_AstNode_Specific_Tag__unreach:
        case Yuceyuf_AstNode_Specific_Tag__paren:
        case Yuceyuf_AstNode_Specific_Tag__expr_if:
        case Yuceyuf_AstNode_Specific_Tag__deref:
            return true;
        case Yuceyuf_AstNode_Specific_Tag__ignore_result:
        case Yuceyuf_AstNode_Specific_Tag__xcontinue:
        case Yuceyuf_AstNode_Specific_Tag__break_void:
        case Yuceyuf_AstNode_Specific_Tag__break_expr:
        case Yuceyuf_AstNode_Specific_Tag__return_void:
        case Yuceyuf_AstNode_Specific_Tag__return_expr:
        case Yuceyuf_AstNode_Specific_Tag__assign:
        case Yuceyuf_AstNode_Specific_Tag__assign_bin_op:
            return true;
        case Yuceyuf_AstNode_Specific_Tag__stmt_block:
        case Yuceyuf_AstNode_Specific_Tag__labeled_block:
        case Yuceyuf_AstNode_Specific_Tag__stmt_if:
        case Yuceyuf_AstNode_Specific_Tag__xwhile:
            return false;
    }
    yy_unreachable();
}

typedef enum {
    StmtTerminator__none,
    StmtTerminator__semicolon,
} StmtTerminator;

NODISCARD static YyStatus expect_stmt_terminator(
    Yuceyuf_Ast_File *ctx, StmtTerminator stmt_term)
{
    switch (stmt_term) {
        case StmtTerminator__none:
            return Yy_Ok;
        case StmtTerminator__semicolon:
            return expect_token_simple(ctx, TokenSimple__semicolon);
    }
    yy_unreachable();
}

NODISCARD static YyStatus expect_stmt_terminator_if_required(
    Yuceyuf_Ast_File *ctx, StmtTerminator stmt_term, Yuceyuf_AstNode const *node)
{
    switch (stmt_term) {
        case StmtTerminator__none:
            return Yy_Ok;
        case StmtTerminator__semicolon: {
            if (yuceyuf_ast_node_requires_stmt_terminator(node->it.tag)) {
                return expect_token_simple(ctx, TokenSimple__semicolon);
            }
            return Yy_Ok;
        }
        break;
    }
    yy_unreachable();
}

/// Parse an expression.
NODISCARD static YyStatus parse_expr(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node);
/// Parse a statement with or without a statement terminator token.
NODISCARD static YyStatus parse_stmt(Yuceyuf_Ast_File *ctx, StmtTerminator stmt_term, Yuceyuf_AstNode **result_ast_node);
/// Parse a statement or local decl with `StmtTerminator__semicolon`.
NODISCARD static YyStatus parse_block_line_stmt_or_decl(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node);

NODISCARD static YyStatus parse_labeled_stmt(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    auto const ident_token_id = ctx->lexer.next();
    auto const& ident_token = ctx->lexer.lex.token(ident_token_id);
    yy_assert(ident_token.data.tag == TokenData_Tag__identifier);
    TRY(expect_token_simple(ctx, TokenSimple__colon));

    // TODO: Parse labeled while loop.
    TRY(expect_token_simple(ctx, TokenSimple__lbrace));
    auto *const block = create_ast_node(ctx);
    *block = Yuceyuf_AstNode {
        .src{ident_token_id},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, labeled_block, {
            .stmts = ast_list_init_empty(),
            .name = MetaTu_get(TokenData, &ident_token.data, identifier)->id,
        }),
    };
    while (true) {
        if (peek_simple(ctx, TokenSimple__rbrace))
            break;
        Yuceyuf_AstNode *stmt_or_decl;
        TRY(parse_block_line_stmt_or_decl(ctx, &stmt_or_decl));
        ast_list_append(ctx, &block->it.payload.stmt_block.stmts, stmt_or_decl);
    }
    auto const rbrace_token_id = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rbrace));
    block->src.end_inclusive = rbrace_token_id;
    *result_ast_node = block;
    return Yy_Ok;
}

NODISCARD static YyStatus parse_stmt_block(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    auto const lbrace_token_id = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__lbrace));
    Yuceyuf_AstNode *const block = create_ast_node(ctx);
    *block = Yuceyuf_AstNode {
        .src{lbrace_token_id},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, stmt_block, { .stmts = ast_list_init_empty(), }),
    };
    while (true) {
        if (peek_simple(ctx, TokenSimple__rbrace))
            break;
        Yuceyuf_AstNode *stmt_or_decl;
        TRY(parse_block_line_stmt_or_decl(ctx, &stmt_or_decl));
        ast_list_append(ctx, &block->it.payload.stmt_block.stmts, stmt_or_decl);
    }
    auto const rbrace_token_id = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rbrace));
    block->src.end_inclusive = rbrace_token_id;
    *result_ast_node = block;
    return Yy_Ok;
}

NODISCARD static YyStatus parse_if(
    Yuceyuf_Ast_File* ctx,
    Yuceyuf_AstNode_Specific_Tag tag,
    StmtTerminator stmt_term,
    Yuceyuf_AstNode** result_ast_node
) {
    yy_assert(tag == Yuceyuf_AstNode_Specific_Tag__stmt_if || tag == Yuceyuf_AstNode_Specific_Tag__expr_if);
    auto const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xif));
    TRY(expect_token_simple(ctx, TokenSimple__lparen));
    Yuceyuf_AstNode* cond;
    TRY(parse_expr(ctx, &cond));
    auto const cond_rparen_token = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rparen));

    bool const is_stmt = tag == Yuceyuf_AstNode_Specific_Tag__stmt_if;
    Yuceyuf_AstNode* then;
    if (is_stmt)
        TRY(parse_stmt(ctx, stmt_term, &then));
    else
        TRY(parse_expr(ctx, &then));

    Option<Yuceyuf_AstNode*> elze = None;
    TokenId end_token{0};  // set later
    Option<TokenId> elze_keyword_token = None;
    if (peek_keyword(ctx, TokenKeyword__xelse)) {
        elze_keyword_token = ctx->lexer.next();
        if (is_stmt)
            TRY(parse_stmt(ctx, stmt_term, elze.unsafe_out_ptr_assume_init()));
        else
            TRY(parse_expr(ctx, elze.unsafe_out_ptr_assume_init()));
        end_token = elze.unwrap()->src.end_inclusive;
    } else {
        end_token = then->src.end_inclusive;
    }
    Yuceyuf_AstNode_Specific_Payload_If const payload = {
        .cond = cond,
        .then = then,
        .elze = elze,
        .cond_rparen_token = cond_rparen_token,
        .elze_keyword_token = elze_keyword_token,
    };
    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode{
        .src{first, first, end_token},
        .parent_ast_file = ctx,
        .it = is_stmt ? MetaTu_init(Yuceyuf_AstNode_Specific, stmt_if, payload)
                      : MetaTu_init(Yuceyuf_AstNode_Specific, expr_if, payload),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_while(
    Yuceyuf_Ast_File* ctx, StmtTerminator stmt_term, Yuceyuf_AstNode** result_ast_node
) {
    auto const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xwhile));
    TRY(expect_token_simple(ctx, TokenSimple__lparen));
    Yuceyuf_AstNode* cond;
    TRY(parse_expr(ctx, &cond));
    TRY(expect_token_simple(ctx, TokenSimple__rparen));

    Option<Yuceyuf_AstNode*> continue_stmt = None;
    if (peek_simple(ctx, TokenSimple__colon)) {
        TRY(expect_token_simple(ctx, TokenSimple__colon));
        TRY(expect_token_simple(ctx, TokenSimple__lparen));
        TRY(parse_stmt(ctx, StmtTerminator__none, continue_stmt.unsafe_out_ptr_assume_init()));
        TRY(expect_token_simple(ctx, TokenSimple__rparen));
    }

    Yuceyuf_AstNode* body;
    TRY(parse_stmt(ctx, stmt_term, &body));
    auto end_token = body->src.end_inclusive;

    Option<Yuceyuf_AstNode*> else_stmt = None;
    if (peek_keyword(ctx, TokenKeyword__xelse)) {
        TRY(expect_token_keyword(ctx, TokenKeyword__xelse));
        TRY(parse_stmt(ctx, stmt_term, else_stmt.unsafe_out_ptr_assume_init()));
        end_token = else_stmt.unwrap()->src.end_inclusive;
    }

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode{
        .src{first, first, end_token},
        .parent_ast_file = ctx,
        .it = MetaTu_init(
            Yuceyuf_AstNode_Specific, xwhile,
            {
                .cond = cond,
                .body = body,
                .continue_stmt = continue_stmt,
                .else_stmt = else_stmt,
            }
        ),
    };
    return Yy_Ok;
}

// TODO: parse_stmt: should have along with stmt_term, another flag, which says whether this stmt is
// inside a list of statements or not. I need to parse block, if, while, etc differently in a stmt
// here because they should stop the expression on semicolon _or_ the closing curly brace. It needs
// to stop in the closing curly brace because after it, there will be more statements. There are
// some rare situations in my language where a statement is never followed by more statements. For
// example: in a switch case statement and a `while`'s `continue_stmt`. Both of these two always
// parse a single statement and end in a token that ends expressions (rbrace, rparen, comma, etc),
// so the logic to stop parsing on rbrace is not necessary.
// Example code that would take advantage of this (labeled blocks are TODO):
// `while (...) : (if (a) b else x: {break :x some_func;} (d))`. Currently, it says "expected
// `rparen` to close continue stmt, found `lparen`". It should parse the function call correctly in
// this case. If you use an unnamed block instead of a labeled one in similar expressions, it would
// fail type checking (void type), but it should still parse correctly because that's what the user
// would expect, since you could parse it if the else expression was in parentheses.
// `parse_stmt` should have a flag that says whether it wants to stop after the closing brace is
// found. Basically, if that's true, instead of making a special case for lbrace, xif and xwhile, it
// would just call `parse_expr`. However, parse_expr still needs to know that you want a statement.
// I should pass flags to parse_expr that say whether it wants an expression or a statement.
NODISCARD static YyStatus parse_stmt(Yuceyuf_Ast_File *ctx, StmtTerminator stmt_term, Yuceyuf_AstNode **result_ast_node)
{
    auto const first = ctx->lexer.peek();
    if (peek_simple(ctx, TokenSimple__lbrace)) {
        TRY(parse_stmt_block(ctx, result_ast_node));
        yy_assert(!yuceyuf_ast_node_requires_stmt_terminator((*result_ast_node)->it.tag));
        return Yy_Ok;
    }
    if (peek_keyword(ctx, TokenKeyword__underscore)) {
        ctx->lexer.advance();
        TRY(expect_token_simple(ctx, TokenSimple__assign));
        Yuceyuf_AstNode *expr;
        TRY(parse_expr(ctx, &expr));
        *result_ast_node = create_ast_node(ctx);
        **result_ast_node = Yuceyuf_AstNode {
            .src{first, first, expr->src.end_inclusive},
            .parent_ast_file = ctx,
            .it = MetaTu_init(Yuceyuf_AstNode_Specific, ignore_result, expr),
        };
        TRY(expect_stmt_terminator(ctx, stmt_term));
        yy_assert(yuceyuf_ast_node_requires_stmt_terminator((*result_ast_node)->it.tag));
        return Yy_Ok;
    }
    if (peek_keyword(ctx, TokenKeyword__xif)) {
        TRY(parse_if(ctx, Yuceyuf_AstNode_Specific_Tag__stmt_if, stmt_term, result_ast_node));
        yy_assert(!yuceyuf_ast_node_requires_stmt_terminator((*result_ast_node)->it.tag));
        return Yy_Ok;
    }
    if (peek_keyword(ctx, TokenKeyword__xwhile)) {
        TRY(parse_while(ctx, stmt_term, result_ast_node));
        yy_assert(!yuceyuf_ast_node_requires_stmt_terminator((*result_ast_node)->it.tag));
        return Yy_Ok;
    }
    if (ctx->lexer.lex.token(first).data.tag == TokenData_Tag__identifier) {
        auto const second = ctx->lexer.peek_nth(1);
        if (ctx->lexer.lex.token(second).is_simple(TokenSimple__colon)) {
            TRY(parse_labeled_stmt(ctx, result_ast_node));
            yy_assert(!yuceyuf_ast_node_requires_stmt_terminator((*result_ast_node)->it.tag));
            return Yy_Ok;
        }
    }

    TRY(parse_expr(ctx, result_ast_node));
    auto const peeked_for_assign_id = ctx->lexer.peek();
    auto const& peeked_for_assign = ctx->lexer.lex.token(peeked_for_assign_id);
    if (peeked_for_assign.data.tag == TokenData_Tag__simple) {
        static_assert(MetaEnum_count_int(TokenSimple) == 47, "change this code");
        static_assert(MetaEnum_count_int(Yuceyuf_Ast_BinaryOp) == 21, "change this code");
        Option<Yuceyuf_Ast_BinaryOp> bin_op_for_assign = None;
        bool is_regular_assign = false;
        switch (*MetaTu_get(TokenData, &peeked_for_assign.data, simple)) {
            default:
            {} break;
            case TokenSimple__assign:
                is_regular_assign = true;
                break;
            case TokenSimple__assign_plus:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__add;
                break;
            case TokenSimple__assign_minus:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__sub;
                break;
            case TokenSimple__assign_mul:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__mul;
                break;
            case TokenSimple__assign_plus_wrap:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__add_wrap;
                break;
            case TokenSimple__assign_minus_wrap:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__sub_wrap;
                break;
            case TokenSimple__assign_mul_wrap:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__mul_wrap;
                break;
            case TokenSimple__assign_div:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__div;
                break;
            case TokenSimple__assign_mod:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__mod;
                break;
            case TokenSimple__assign_bits_and:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__bits_and;
                break;
            case TokenSimple__assign_bits_or:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__bits_or;
                break;
            case TokenSimple__assign_bits_xor:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__bits_xor;
                break;
            case TokenSimple__assign_sh_left:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__sh_left;
                break;
            case TokenSimple__assign_sh_right:
                bin_op_for_assign = Yuceyuf_Ast_BinaryOp__sh_right;
                break;
        }
        if (is_regular_assign) {
            // assignment
            ctx->lexer.advance();
            Yuceyuf_AstNode *const assignment_location = *result_ast_node;
            Yuceyuf_AstNode *expr;
            TRY(parse_expr(ctx, &expr));
            *result_ast_node = create_ast_node(ctx);
            **result_ast_node = Yuceyuf_AstNode {
                .src{first, peeked_for_assign_id, expr->src.end_inclusive},
                .parent_ast_file = ctx,
                .it = MetaTu_init(Yuceyuf_AstNode_Specific, assign, {
                    .lhs = assignment_location,
                    .rhs = expr,
                }),
            };
        } else if (bin_op_for_assign.has_value()) {
            // assignment
            ctx->lexer.advance();
            Yuceyuf_AstNode *const assignment_location = *result_ast_node;
            Yuceyuf_AstNode *expr;
            TRY(parse_expr(ctx, &expr));
            *result_ast_node = create_ast_node(ctx);
            **result_ast_node = Yuceyuf_AstNode {
                .src{first, peeked_for_assign_id, expr->src.end_inclusive},
                .parent_ast_file = ctx,
                .it = MetaTu_init(Yuceyuf_AstNode_Specific, assign_bin_op, {
                    .op = bin_op_for_assign.unwrap(),
                    .lhs = assignment_location,
                    .rhs = expr,
                }),
            };
        }
    }
    TRY(expect_stmt_terminator_if_required(ctx, stmt_term, *result_ast_node));
    return Yy_Ok;
}

// block_var ::= "var" identifier (":" type_expr)? "=" expr ";"
NODISCARD static YyStatus parse_block_var(
    Yuceyuf_Ast_File *const ctx, Yuceyuf_AstNode **const result_ast_node)
{
    auto const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xvar));

    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    Option<Yuceyuf_AstNode*> ty_expr = None;
    if (peek_simple(ctx, TokenSimple__colon)) {
        ctx->lexer.advance();
        TRY(parse_expr(ctx, ty_expr.unsafe_out_ptr_assume_init()));
    }

    TRY(expect_token_simple(ctx, TokenSimple__assign));

    Yuceyuf_AstNode *initial_value;
    TRY(parse_expr(ctx, &initial_value));

    // TODO: read `parse_global_var_decl`'s comment about `end_pos`
    auto const semicolon = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__semicolon));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, semicolon},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, local_var_decl, {
            .name = name.id,
            .ty_expr = ty_expr,
            .initial_value = initial_value,
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_block_line_stmt_or_decl(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    if (peek_keyword(ctx, TokenKeyword__xvar)) {
        return parse_block_var(ctx, result_ast_node);
    }
    return parse_stmt(ctx, StmtTerminator__semicolon, result_ast_node);
}

NODISCARD static YyStatus parse_global_var_decl(
    Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    auto const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xvar));

    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    Option<Yuceyuf_AstNode*> ty_expr = None;
    if (peek_simple(ctx, TokenSimple__colon)) {
        ctx->lexer.advance();
        TRY(parse_expr(ctx, ty_expr.unsafe_out_ptr_assume_init()));
    }

    TRY(expect_token_simple(ctx, TokenSimple__assign));

    Yuceyuf_AstNode *initial_value;
    TRY(parse_expr(ctx, &initial_value));

    // TODO: I'm not sure whether a `global_var_decl`'s `end_pos` should be the position of the semicolon or the end position of the initial_value. Since `end_pos` is only ever used for formatting code, and the only thing used in `end_pos` is the line number, it should only make a difference if `semicolon_pos.line0 != initial_value.end_pos.line0`. The only reason this might be the case (for correctly formatted code) is if the token before the semicolon consumes the entire line and makes it impossible for the semicolon to be put in the same line (i.e., comments and zig-like multi-line string literals (not implemented yet)).
    auto const semicolon = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__semicolon));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, semicolon},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, global_var_decl, {
            .name = name.id,
            .ty_expr = ty_expr,
            .initial_value = initial_value,
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_root_extern_var(
    Yuceyuf_Ast_File *const ctx, TokenId const first,
    Yuceyuf_AstNode **const result_ast_node)
{
    yy_assert(ctx->lexer.lex.token(first).is_keyword(TokenKeyword__xextern));
    TRY(expect_token_keyword(ctx, TokenKeyword__xvar));

    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    TRY(expect_token_simple(ctx, TokenSimple__colon));
    Yuceyuf_AstNode *ty_expr;
    TRY(parse_expr(ctx, &ty_expr));

    auto const semicolon = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__semicolon));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, semicolon},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, extern_var_decl, {
            .name = name.id,
            .ty_expr = ty_expr,
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_root_extern_fn(
    Yuceyuf_Ast_File *const ctx, TokenId const first,
    Yuceyuf_AstNode **const result_ast_node)
{
    yy_assert(ctx->lexer.lex.token(first).is_keyword(TokenKeyword__xextern));
    TRY(expect_token_keyword(ctx, TokenKeyword__fn));

    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    TRY(expect_token_simple(ctx, TokenSimple__lparen));
    // TODO: `bool args_has_trailing_comma;`
    Yuceyuf_AstList args = ast_list_init_empty();
    while (true) {
        if (peek_simple(ctx, TokenSimple__rparen)) {
            break;
        }

        TokenId const peeked = ctx->lexer.peek();

        Option<TokenIdentifier> arg_name = None;
        TRY(expect_token_identifier_or_underscore(ctx, &arg_name));

        TRY(expect_token_simple(ctx, TokenSimple__colon));
        Yuceyuf_AstNode *type_expr;
        TRY(parse_expr(ctx, &type_expr));

        Yuceyuf_AstNode *arg_node = create_ast_node(ctx);
        *arg_node = Yuceyuf_AstNode {
            .src{peeked, peeked, type_expr->src.end_inclusive},
            .parent_ast_file = ctx,
            .it = MetaTu_init(Yuceyuf_AstNode_Specific, extern_fn_decl_arg_info, {
                .name = arg_name.map([](auto& x){ return x.id; }),
                .type_expr = type_expr,
            }),
        };
        ast_list_append(ctx, &args, arg_node);

        if (peek_simple(ctx, TokenSimple__comma)) {
            ctx->lexer.advance();
            continue;
        } else {
            break;
        }
    }
    TRY(expect_token_simple(ctx, TokenSimple__rparen));
    if (peek_simple(ctx, TokenSimple__lbrace)) {
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(ctx->lexer.peek()), "expected function return type, got '" Sv_Fmt "'",
            Sv_Arg(MetaEnum_sv_short(TokenSimple, TokenSimple__lbrace))
        );
        return Yy_Bad;
    }
    Yuceyuf_AstNode *return_type;
    TRY(parse_expr(ctx, &return_type));

    TokenId const semicolon = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__semicolon));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, semicolon},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, extern_fn_decl, {
            .name = name.id,
            .return_type = return_type,
            .args = std::move(args),
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_root_extern(
    Yuceyuf_Ast_File *const ctx, Yuceyuf_AstNode **const result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xextern));

    if (peek_keyword(ctx, TokenKeyword__xvar)) {
        return parse_root_extern_var(ctx, first, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__fn)) {
        return parse_root_extern_fn(ctx, first, result_ast_node);
    }

    ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), ctx->lexer.peek());
    return Yy_Bad;
}

NODISCARD static YyStatus parse_root_struct(
    Yuceyuf_Ast_File *const ctx, Yuceyuf_AstNode **const result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__xstruct));

    TokenIdentifier struct_name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &struct_name));

    TRY(expect_token_simple(ctx, TokenSimple__lbrace));
    auto members = ast_list_init_empty();
    while (true) {
        if (peek_simple(ctx, TokenSimple__rbrace)) {
            break;
        }

        TokenId const peeked = ctx->lexer.peek();

        Option<TokenIdentifier> member_name;
        TRY(expect_token_identifier_or_underscore(ctx, &member_name));
        TRY(expect_token_simple(ctx, TokenSimple__colon));
        Yuceyuf_AstNode *ty_expr;
        TRY(parse_expr(ctx, &ty_expr));

        Yuceyuf_AstNode *member_node = create_ast_node(ctx);
        *member_node = Yuceyuf_AstNode {
            .src{peeked, peeked, ty_expr->src.end_inclusive},
            .parent_ast_file = ctx,
            .it = MetaTu_init(Yuceyuf_AstNode_Specific, struct_decl_member_info, {
                .name = member_name.map([](auto& x){ return x.id; }),
                .ty_expr = ty_expr,
            }),
        };
        ast_list_append(ctx, &members, member_node);

        if (peek_simple(ctx, TokenSimple__comma)) {
            ctx->lexer.advance();
            continue;
        } else {
            break;
        }

    }
    TokenId const rbrace = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rbrace));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, rbrace},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, struct_decl, {
            .name = struct_name.id,
            .members = std::move(members),
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_fn_decl(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__fn));
    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    // Parse arguments
    // all_args ::= "(" one_arg ( "," one_arg )* ","? ")"
    // one_arg ::= identifier
    TRY(expect_token_simple(ctx, TokenSimple__lparen));
    // TODO: `bool args_has_trailing_comma;`
    Yuceyuf_AstList args = ast_list_init_empty();
    while (true) {
        if (peek_simple(ctx, TokenSimple__rparen)) {
            break;
        }

        TokenId const peeked = ctx->lexer.peek();

        Option<TokenIdentifier> arg_name;
        TRY(expect_token_identifier_or_underscore(ctx, &arg_name));

        TRY(expect_token_simple(ctx, TokenSimple__colon));
        Yuceyuf_AstNode *ty_expr;
        TRY(parse_expr(ctx, &ty_expr));

        Yuceyuf_AstNode *arg_node = create_ast_node(ctx);
        *arg_node = Yuceyuf_AstNode {
            .src{peeked, peeked, ty_expr->src.end_inclusive},
            .parent_ast_file = ctx,
            .it = MetaTu_init(Yuceyuf_AstNode_Specific, fn_decl_arg_info, {
                .name = arg_name.map([](auto& x){ return x.id; }),
                .ty_expr = ty_expr,
            }),
        };
        ast_list_append(ctx, &args, arg_node);

        if (peek_simple(ctx, TokenSimple__comma)) {
            ctx->lexer.advance();
            continue;
        } else {
            break;
        }
    }
    TRY(expect_token_simple(ctx, TokenSimple__rparen));
    if (peek_simple(ctx, TokenSimple__lbrace)) {
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(ctx->lexer.peek()), "expected function return type, got '" Sv_Fmt "'",
            Sv_Arg(MetaEnum_sv_short(TokenSimple, TokenSimple__lbrace))
        );
        return Yy_Bad;
    }
    Yuceyuf_AstNode *return_type;
    TRY(parse_expr(ctx, &return_type));

    Yuceyuf_AstNode *body;
    TRY(parse_stmt_block(ctx, &body));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, body->src.end_inclusive},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, fn_decl, {
            .name = name.id,
            .return_type = return_type,
            .body = body,
            .args = std::move(args),
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_fn_call_args(
    Yuceyuf_Ast_File *ctx, Yuceyuf_AstList *result_arg_list, bool *result_has_trailing_comma, TokenId* result_end_token)
{
    TRY(expect_token_simple(ctx, TokenSimple__lparen));
    while (true) {
        if (peek_simple(ctx, TokenSimple__rparen)) {
            *result_has_trailing_comma = result_arg_list->nodes.size() > 0;
            break;
        }
        Yuceyuf_AstNode *arg;
        TRY(parse_expr(ctx, &arg));
        ast_list_append(ctx, result_arg_list, arg);
        if (peek_simple(ctx, TokenSimple__comma)) {
            ctx->lexer.advance();
            continue;
        } else {
            *result_has_trailing_comma = false;
            break;
        }
    }
    auto const rparen_tok = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rparen));
    *result_end_token = rparen_tok;
    return Yy_Ok;
}

NODISCARD static i8 precedence_op_unary_prefix(Yuceyuf_Ast_UnaryOp op)
{
    switch (op) {
        case MetaEnum_count(Yuceyuf_Ast_UnaryOp):
            yy_unreachable();
        case Yuceyuf_Ast_UnaryOp__ptr_to:
        case Yuceyuf_Ast_UnaryOp__plus:
        case Yuceyuf_Ast_UnaryOp__plus_wrap:
        case Yuceyuf_Ast_UnaryOp__minus:
        case Yuceyuf_Ast_UnaryOp__minus_wrap:
        case Yuceyuf_Ast_UnaryOp__bool_not:
        case Yuceyuf_Ast_UnaryOp__bits_not:
        case Yuceyuf_Ast_UnaryOp__address_of:
            return -1;
    }
    yy_unreachable();
}

NODISCARD static i8 precedence_op_bin(Yuceyuf_Ast_BinaryOp op)
{
    switch (op) {
        case MetaEnum_count(Yuceyuf_Ast_BinaryOp):
            yy_unreachable();
        case Yuceyuf_Ast_BinaryOp__mul:
        case Yuceyuf_Ast_BinaryOp__mul_wrap:
        case Yuceyuf_Ast_BinaryOp__div:
        case Yuceyuf_Ast_BinaryOp__mod:
            return 10;
        case Yuceyuf_Ast_BinaryOp__add:
        case Yuceyuf_Ast_BinaryOp__add_wrap:
        case Yuceyuf_Ast_BinaryOp__sub:
        case Yuceyuf_Ast_BinaryOp__sub_wrap:
            return 15;
        case Yuceyuf_Ast_BinaryOp__sh_left:
        case Yuceyuf_Ast_BinaryOp__sh_right:
            return 16;
        case Yuceyuf_Ast_BinaryOp__bits_and:
        case Yuceyuf_Ast_BinaryOp__bits_xor:
        case Yuceyuf_Ast_BinaryOp__bits_or:
            return 17;
        case Yuceyuf_Ast_BinaryOp__bool_gt:
        case Yuceyuf_Ast_BinaryOp__bool_gte:
        case Yuceyuf_Ast_BinaryOp__bool_lt:
        case Yuceyuf_Ast_BinaryOp__bool_lte:
            return 20;
        case Yuceyuf_Ast_BinaryOp__bool_eq:
        case Yuceyuf_Ast_BinaryOp__bool_noteq:
            return 25;
        case Yuceyuf_Ast_BinaryOp__bool_and:
            return 30;
        case Yuceyuf_Ast_BinaryOp__bool_or:
            return 31;
    }
    yy_unreachable();
}


/// Fixes precedence at `*fix_root_ptr` and stores the new root node there.
static void fix_precedence_op_bin(Yuceyuf_AstNode **fix_root_ptr)
{
    // Fix precedence at `*fix_root_ptr`.
    // "a X Y"
    // a is a node
    // X is a binary operator
    // Y is a rhs of X.
    // if Y is a binary operator "b Y c", this expression is written out like this:
    // "a X b Y c"
    // Take for example "a + b * c" or "a / b - c". We'd want it to be equal to "a + (b * c)" and
    // "(a / b) - c", respectively.
    //
    // if (precedence(x) < precedence(y)), rotate x and y (this will set root to Y) and fix again for root at x.
    Yuceyuf_AstNode *const x = *fix_root_ptr;
    yy_assert(x->it.tag == Yuceyuf_AstNode_Specific_Tag__op_bin);
    Yuceyuf_AstNode *const y = x->it.payload.op_bin.rhs;
    if (y->it.tag == Yuceyuf_AstNode_Specific_Tag__op_bin) {
        // rhs is "b Y c"
        if (precedence_op_bin(x->it.payload.op_bin.op) <= precedence_op_bin(y->it.payload.op_bin.op)) {
            // std.debug.print(" ~~~~~~~ Precedence binary!\n", .{});
            Yuceyuf_AstNode *const b = y->it.payload.op_bin.lhs;
            y->it.payload.op_bin.lhs = x;
            x->it.payload.op_bin.rhs = b;
            *fix_root_ptr = y;
            x->src.end_inclusive = x->it.payload.op_bin.rhs->src.end_inclusive;
            // maybe fix precedence again for root at `y->it.payload.op_bin.lhs`.
            fix_precedence_op_bin(&y->it.payload.op_bin.lhs);
        }
    }
}

/// Fixes precedence at `*fix_root_ptr` and stores the new root node there.
static void fix_precedence_op_unary_prefix(Yuceyuf_AstNode **fix_root_ptr)
{
    yy_assert((*fix_root_ptr)->it.tag == Yuceyuf_AstNode_Specific_Tag__op_unary_prefix);

    // Basically the same thing as fix_precedence_op_binary except that instead of `a X b Y c` we
    // have `X b Y c`.
    Yuceyuf_AstNode *const x = *fix_root_ptr;
    yy_assert(x->it.tag == Yuceyuf_AstNode_Specific_Tag__op_unary_prefix);
    Yuceyuf_AstNode *const y = x->it.payload.op_unary_prefix.operand;
    if (y->it.tag == Yuceyuf_AstNode_Specific_Tag__op_bin) {
        // rhs is "b Y c"
        if (precedence_op_unary_prefix(x->it.payload.op_unary_prefix.tag) <= precedence_op_bin(y->it.payload.op_bin.op)) {
            // std.debug.print(" ~~~~~~~ Precedence binary!\n", .{});
            Yuceyuf_AstNode *const b = y->it.payload.op_bin.lhs;
            y->it.payload.op_bin.lhs = x;
            x->it.payload.op_unary_prefix.operand = b;
            *fix_root_ptr = y;
            x->src.end_inclusive = x->it.payload.op_unary_prefix.operand->src.end_inclusive;
            // maybe fix precedence again for root at `y->it.payload.op_bin.lhs`.
            fix_precedence_op_unary_prefix(&y->it.payload.op_bin.lhs);
        }
    }
    ;
}

// TODO: This function may return a binary operator if precedence needs fixing.
NODISCARD static YyStatus parse_unary_prefix_op(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **prev_ast_node)
{
    auto const first = ctx->lexer.peek();
    TRY(ast_unary_prefix_op_must_not_have_lhs(ctx, first, *prev_ast_node));
    ctx->lexer.advance();
    yy_assert(*prev_ast_node == NULL);

    auto const& first_token = ctx->lexer.lex.token(first);
    Option<Yuceyuf_Ast_UnaryOp> op = None;
    yy_assert(first_token.data.tag == TokenData_Tag__simple);
    switch (*MetaTu_get(TokenData, &first_token.data, simple)) {
        case MetaEnum_count(TokenSimple):
            yy_unreachable();
        case TokenSimple__lparen:
        case TokenSimple__rparen:
        case TokenSimple__lbrace:
        case TokenSimple__rbrace:
        case TokenSimple__lbracket:
        case TokenSimple__rbracket:
        case TokenSimple__dot:
        case TokenSimple__deref:
        case TokenSimple__semicolon:
        case TokenSimple__colon:
        case TokenSimple__colon_colon:
        case TokenSimple__comma:
        case TokenSimple__assign:
        case TokenSimple__mul_wrap:
        case TokenSimple__div:
        case TokenSimple__mod:
        case TokenSimple__bits_or:
        case TokenSimple__bits_xor:
        case TokenSimple__lt:
        case TokenSimple__lte:
        case TokenSimple__gt:
        case TokenSimple__gte:
        case TokenSimple__eq:
        case TokenSimple__noteq:
        case TokenSimple__sh_left:
        case TokenSimple__sh_right:
        case TokenSimple__assign_plus:
        case TokenSimple__assign_minus:
        case TokenSimple__assign_mul:
        case TokenSimple__assign_plus_wrap:
        case TokenSimple__assign_minus_wrap:
        case TokenSimple__assign_mul_wrap:
        case TokenSimple__assign_div:
        case TokenSimple__assign_mod:
        case TokenSimple__assign_bits_and:
        case TokenSimple__assign_bits_or:
        case TokenSimple__assign_bits_xor:
        case TokenSimple__assign_sh_left:
        case TokenSimple__assign_sh_right:
            yy_unreachable();
        case TokenSimple__mul: op = Yuceyuf_Ast_UnaryOp__ptr_to; break;
        case TokenSimple__ampersand: op = Yuceyuf_Ast_UnaryOp__address_of; break;
        case TokenSimple__plus: op = Yuceyuf_Ast_UnaryOp__plus; break;
        case TokenSimple__minus: op = Yuceyuf_Ast_UnaryOp__minus; break;
        case TokenSimple__plus_wrap: op = Yuceyuf_Ast_UnaryOp__plus_wrap; break;
        case TokenSimple__minus_wrap: op = Yuceyuf_Ast_UnaryOp__minus_wrap; break;
        case TokenSimple__bits_not: op = Yuceyuf_Ast_UnaryOp__bits_not; break;
        case TokenSimple__bool_not: op = Yuceyuf_Ast_UnaryOp__bool_not; break;
    }
    yy_assert(op.has_value());
    Yuceyuf_AstNode *operand;
    TRY(parse_expr(ctx, &operand));
    Yuceyuf_AstNode *new_node = create_ast_node(ctx);
    *new_node = Yuceyuf_AstNode {
        .src{first, first, operand->src.end_inclusive},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, op_unary_prefix, {
            .tag = op.unwrap(),
            .operand = operand,
        }),
    };
    fix_precedence_op_unary_prefix(&new_node);
    *prev_ast_node = new_node;
    return Yy_Ok;
}

NODISCARD static YyStatus parse_bin_op(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **prev_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(ast_bin_op_must_have_lhs(ctx, first, *prev_ast_node));
    ctx->lexer.advance();
    yy_assert(*prev_ast_node != NULL);

    auto const& first_token = ctx->lexer.lex.token(first);
    Option<Yuceyuf_Ast_BinaryOp> op = None;
    switch (first_token.data.tag) {
        default:
            yy_unreachable();
        case TokenData_Tag__keyword: {
            switch (*MetaTu_get(TokenData, &first_token.data, keyword)) {
                case MetaEnum_count(TokenKeyword):
                    yy_unreachable();
                case TokenKeyword__fn:
                case TokenKeyword__mod:
                case TokenKeyword__xextern:
                case TokenKeyword__xstruct:
                case TokenKeyword__ximport:
                case TokenKeyword__xbreak:
                case TokenKeyword__xcontinue:
                case TokenKeyword__xreturn:
                case TokenKeyword__xvar:
                case TokenKeyword__xwhile:
                case TokenKeyword__xif:
                case TokenKeyword__xelse:
                case TokenKeyword__xtrue:
                case TokenKeyword__xfalse:
                case TokenKeyword__xundefined:
                case TokenKeyword__xnoreturn:
                case TokenKeyword__xvoid:
                case TokenKeyword__xbool:
                case TokenKeyword__uintword:
                case TokenKeyword__xu8:
                case TokenKeyword__unreach:
                case TokenKeyword__underscore:
                    yy_unreachable();
                case TokenKeyword__bool_and: op = Yuceyuf_Ast_BinaryOp__bool_and; break;
                case TokenKeyword__bool_or: op = Yuceyuf_Ast_BinaryOp__bool_or; break;
            }
        }
        break;
        case TokenData_Tag__simple: {
            switch (*MetaTu_get(TokenData, &first_token.data, simple)) {
                case MetaEnum_count(TokenSimple):
                    yy_unreachable();
                case TokenSimple__lparen:
                case TokenSimple__rparen:
                case TokenSimple__lbrace:
                case TokenSimple__rbrace:
                case TokenSimple__lbracket:
                case TokenSimple__rbracket:
                case TokenSimple__dot:
                case TokenSimple__deref:
                case TokenSimple__semicolon:
                case TokenSimple__colon:
                case TokenSimple__colon_colon:
                case TokenSimple__comma:
                case TokenSimple__assign:
                case TokenSimple__assign_plus:
                case TokenSimple__assign_minus:
                case TokenSimple__assign_mul:
                case TokenSimple__assign_plus_wrap:
                case TokenSimple__assign_minus_wrap:
                case TokenSimple__assign_mul_wrap:
                case TokenSimple__assign_div:
                case TokenSimple__assign_mod:
                case TokenSimple__assign_bits_and:
                case TokenSimple__assign_bits_or:
                case TokenSimple__assign_bits_xor:
                case TokenSimple__assign_sh_left:
                case TokenSimple__assign_sh_right:
                    yy_unreachable();
                case TokenSimple__bits_not:
                case TokenSimple__bool_not:
                    yy_unreachable();
                    break;
                case TokenSimple__plus: op = Yuceyuf_Ast_BinaryOp__add; break;
                case TokenSimple__minus: op = Yuceyuf_Ast_BinaryOp__sub; break;
                case TokenSimple__mul: op = Yuceyuf_Ast_BinaryOp__mul; break;
                case TokenSimple__plus_wrap: op = Yuceyuf_Ast_BinaryOp__add_wrap; break;
                case TokenSimple__minus_wrap: op = Yuceyuf_Ast_BinaryOp__sub_wrap; break;
                case TokenSimple__mul_wrap: op = Yuceyuf_Ast_BinaryOp__mul_wrap; break;
                case TokenSimple__div: op = Yuceyuf_Ast_BinaryOp__div; break;
                case TokenSimple__mod: op = Yuceyuf_Ast_BinaryOp__mod; break;
                case TokenSimple__ampersand: op = Yuceyuf_Ast_BinaryOp__bits_and; break;
                case TokenSimple__bits_or: op = Yuceyuf_Ast_BinaryOp__bits_or; break;
                case TokenSimple__bits_xor: op = Yuceyuf_Ast_BinaryOp__bits_xor; break;
                case TokenSimple__lt: op = Yuceyuf_Ast_BinaryOp__bool_lt; break;
                case TokenSimple__lte: op = Yuceyuf_Ast_BinaryOp__bool_lte; break;
                case TokenSimple__gt: op = Yuceyuf_Ast_BinaryOp__bool_gt; break;
                case TokenSimple__gte: op = Yuceyuf_Ast_BinaryOp__bool_gte; break;
                case TokenSimple__eq: op = Yuceyuf_Ast_BinaryOp__bool_eq; break;
                case TokenSimple__noteq: op = Yuceyuf_Ast_BinaryOp__bool_noteq; break;
                case TokenSimple__sh_left: op = Yuceyuf_Ast_BinaryOp__sh_left; break;
                case TokenSimple__sh_right: op = Yuceyuf_Ast_BinaryOp__sh_right; break;
            }
        }
        break;
    }
    yy_assert(op.has_value());
    auto lhs = *prev_ast_node;
    Yuceyuf_AstNode *rhs;
    TRY(parse_expr(ctx, &rhs));
    Yuceyuf_AstNode *new_node = create_ast_node(ctx);
    *new_node = Yuceyuf_AstNode {
        .src{lhs->src.begin, first, rhs->src.end_inclusive},
        .parent_ast_file = ctx,
        .it = Yuceyuf_AstNode_Specific::init_op_bin({
            .op = op.unwrap(),
            .lhs = lhs,
            .rhs = rhs,
        }),
    };
    fix_precedence_op_bin(&new_node);
    *prev_ast_node = new_node;
    return Yy_Ok;
}

NODISCARD static YyStatus token_as_identifier(
    Yuceyuf_Ast_File *ctx,
    TokenId const field,
    TokenIdentifier *result_identifier)
{
    auto const& field_token = ctx->lexer.lex.token(field);
    switch (field_token.data.tag) {
        case TokenData_Tag__identifier: {
            *result_identifier = *MetaTu_get(TokenData, &field_token.data, identifier);
            return Yy_Ok;
        }
        break;
        case TokenData_Tag__integer_literal: {
            auto const src = ctx->lexer.lex.token_src(field);
            auto const sv = ctx->lexer.lex.contents.slice(src.begin.x, src.end.x);
            auto const identifier_id = ctx->lexer.lex.global->make_identifier(sv);
            *result_identifier = TokenIdentifier{.id = identifier_id};
            return Yy_Ok;
        }
        break;
        case TokenData_Tag__keyword: {
            Sv const sv = MetaEnum_sv_short(TokenKeyword, *MetaTu_get(TokenData, &field_token.data, keyword));
            auto const identifier_id = ctx->lexer.lex.global->make_identifier(sv);
            *result_identifier = TokenIdentifier { .id = identifier_id };
            return Yy_Ok;
        }
        break;
        default: {
            ast_add_error_msg__fancy(ctx, sv_lit("expected identifier but got"), field);
            return Yy_Bad;
        }
        break;
    }
    yy_unreachable();
}

NODISCARD static YyStatus parse_expr(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    Yuceyuf_AstNode *OPT prev_ast_node = NULL;
    while (true) {
        TokenId const first = ctx->lexer.peek();
        auto const& first_token = ctx->lexer.lex.token(first);
        if (token_is_end_of_expr(first_token)) {
            if (prev_ast_node == NULL) {
                ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), first);
                return Yy_Bad;
            }
            goto loop_end;
        }

        switch (first_token.data.tag) {
            case MetaEnum_count(TokenData_Tag):
                yy_unreachable();
            case TokenData_Tag__eof:
                yy_unreachable();
                break;
            case TokenData_Tag__string_literal: {
                TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                ctx->lexer.advance();
                Yuceyuf_AstNode *const node = create_ast_node(ctx);
                *node = Yuceyuf_AstNode {
                    .src{first},
                    .parent_ast_file = ctx,
                    .it = MetaTu_init(Yuceyuf_AstNode_Specific, string_literal, first_token.data.payload.string_literal)
                };
                prev_ast_node = node;
            }
            break;
            case TokenData_Tag__integer_literal: {
                TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                ctx->lexer.advance();

                Yuceyuf_AstNode *const node = create_ast_node(ctx);
                *node = Yuceyuf_AstNode {
                    .src{first},
                    .parent_ast_file = ctx,
                    .it = MetaTu_init(Yuceyuf_AstNode_Specific, integer_literal, first_token.data.payload.integer_literal)
                };
                prev_ast_node = node;
            }
            break;
            case TokenData_Tag__char_literal: {
                TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                ctx->lexer.advance();

                Yuceyuf_AstNode *const node = create_ast_node(ctx);
                *node = Yuceyuf_AstNode {
                    .src{first},
                    .parent_ast_file = ctx,
                    .it = MetaTu_init(Yuceyuf_AstNode_Specific, char_literal, first_token.data.payload.char_literal)
                };
                prev_ast_node = node;
            }
            break;
            case TokenData_Tag__keyword: {
                switch (first_token.data.payload.keyword) {
                    case MetaEnum_count(TokenKeyword):
                        yy_unreachable();
                    case TokenKeyword__xelse:
                        yy_unreachable();
                    case TokenKeyword__underscore:
                    case TokenKeyword__xextern:
                    case TokenKeyword__xstruct:
                    case TokenKeyword__ximport:
                    case TokenKeyword__mod:
                    case TokenKeyword__fn: {
                        ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), first);
                        return Yy_Bad;
                    }
                    break;
                    case TokenKeyword__xvar: {
                        ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), first);
                        return Yy_Bad;
                    }
                    break;
                    case TokenKeyword__xreturn: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();

                        auto const second = ctx->lexer.peek();
                        auto const& second_token = ctx->lexer.lex.token(second);
                        if (token_is_end_of_expr(second_token)) {
                            // return_void
                            prev_ast_node = create_ast_node(ctx);
                            *prev_ast_node = Yuceyuf_AstNode {
                                .src{first},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, return_void, Void{}),
                            };
                        } else {
                            // return_expr
                            Yuceyuf_AstNode *expr;
                            TRY(parse_expr(ctx, &expr));
                            prev_ast_node = create_ast_node(ctx);
                            *prev_ast_node = Yuceyuf_AstNode {
                                .src{first, first, expr->src.end_inclusive},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, return_expr, {
                                    .expr = expr,
                                }),
                            };
                        }
                    }
                    break;
                    case TokenKeyword__xbreak: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();

                        auto second = ctx->lexer.peek();
                        Option<IdentifierId> label = None;
                        if (ctx->lexer.lex.token(second).is_simple(TokenSimple__colon)) {
                            auto third = ctx->lexer.peek_nth(1);
                            auto const& third_token = ctx->lexer.lex.token(third);
                            if (third_token.data.tag == TokenData_Tag__identifier) {
                                label = MetaTu_get(TokenData, &third_token.data, identifier)->id;
                                ctx->lexer.advance();
                                ctx->lexer.advance();
                                second = ctx->lexer.peek();
                            }
                        }
                        if (token_is_end_of_expr(ctx->lexer.lex.token(second))) {
                            // break_void
                            prev_ast_node = create_ast_node(ctx);
                            *prev_ast_node = Yuceyuf_AstNode {
                                .src{first, first, TokenId{ctx->lexer.peek().x - 1}},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, break_void, { .label = label }),
                            };
                        } else {
                            // break_expr
                            Yuceyuf_AstNode *expr;
                            TRY(parse_expr(ctx, &expr));
                            prev_ast_node = create_ast_node(ctx);
                            *prev_ast_node = Yuceyuf_AstNode {
                                .src{first, first, expr->src.end_inclusive},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, break_expr, {
                                    .expr = expr,
                                    .label = label,
                                }),
                            };
                        }
                    }
                    break;
                    case TokenKeyword__xcontinue: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();

                        TokenId second = ctx->lexer.peek();
                        Option<IdentifierId> label = None;
                        if (ctx->lexer.lex.token(second).is_simple(TokenSimple__colon)) {
                            auto third = ctx->lexer.peek_nth(1);
                            auto const& third_token = ctx->lexer.lex.token(third);
                            if (third_token.data.tag == TokenData_Tag__identifier) {
                                label = MetaTu_get(TokenData, &third_token.data, identifier)->id;
                                ctx->lexer.advance();
                                ctx->lexer.advance();
                                second = ctx->lexer.peek();
                            }
                        }
                        prev_ast_node = create_ast_node(ctx);
                        *prev_ast_node = Yuceyuf_AstNode {
                            .src{first, first, TokenId{ctx->lexer.peek().x - 1}},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, xcontinue, { .label = label }),
                        };
                    }
                    break;
                    case TokenKeyword__unreach: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();
                        prev_ast_node = create_ast_node(ctx);
                        *prev_ast_node = Yuceyuf_AstNode {
                            .src{first},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, unreach, Void{}),
                        };
                    }
                    break;
                    case TokenKeyword__xif: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        TRY(parse_if(ctx, Yuceyuf_AstNode_Specific_Tag__expr_if, StmtTerminator__none, &prev_ast_node));
                    }
                    break;
                    case TokenKeyword__xwhile: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        TRY(parse_while(ctx, StmtTerminator__none, &prev_ast_node));
                    }
                    break;
                    case TokenKeyword__xtrue:
                    case TokenKeyword__xfalse: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();
                        bool const literal_value = first_token.data.payload.keyword == TokenKeyword__xtrue ? true : false;
                        Yuceyuf_AstNode *const node = create_ast_node(ctx);
                        *node = Yuceyuf_AstNode {
                            .src{first},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, bool_literal, literal_value),
                        };
                        prev_ast_node = node;
                    }
                    break;
                    case TokenKeyword__xundefined: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();
                        Yuceyuf_AstNode *const node = create_ast_node(ctx);
                        *node = Yuceyuf_AstNode {
                            .src{first},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, undefined_literal, {}),
                        };
                        prev_ast_node = node;
                    }
                    break;
                    case TokenKeyword__xnoreturn:
                    case TokenKeyword__xvoid:
                    case TokenKeyword__xbool:
                    case TokenKeyword__uintword:
                    case TokenKeyword__xu8: {
                        TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                        ctx->lexer.advance();
                        TokenKeyword const kw = *MetaTu_get(TokenData, &first_token.data, keyword);
                        prev_ast_node = create_ast_node(ctx);
                        *prev_ast_node = Yuceyuf_AstNode {
                            .src{first},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, builtin_typ, kw),
                        };
                    }
                    break;
                    case TokenKeyword__bool_and:
                    case TokenKeyword__bool_or:
                        TRY(parse_bin_op(ctx, &prev_ast_node));
                        break;
                }
            }
            break;
            case TokenData_Tag__builtin_fn: {
                TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                ctx->lexer.advance();

                // Create the node and initialize it with empty arguments
                Yuceyuf_AstNode *const node = create_ast_node(ctx);
                *node = Yuceyuf_AstNode {
                    .src{first},
                    .parent_ast_file = ctx,
                    .it = MetaTu_init(Yuceyuf_AstNode_Specific, builtin_fn_call, {
                        .bfn = first_token.data.payload.builtin_fn,
                        .args = ast_list_init_empty(),
                        .args_has_trailing_comma = false, // init later
                    }),
                };

                // Parse the arguments
                TRY(parse_fn_call_args(ctx, &node->it.payload.builtin_fn_call.args,
                                       &node->it.payload.builtin_fn_call.args_has_trailing_comma,
                                       &node->src.end_inclusive));
                prev_ast_node = node;
            }
            break;
            case TokenData_Tag__identifier: {
                TRY(ast_no_prev_node(ctx, first, prev_ast_node));
                auto second = ctx->lexer.peek_nth(1);
                if (ctx->lexer.lex.token(second).is_simple(TokenSimple__colon)) {
                    TRY(parse_labeled_stmt(ctx, &prev_ast_node));
                } else {
                    ctx->lexer.advance();
                    Yuceyuf_AstNode *const node = create_ast_node(ctx);
                    *node = Yuceyuf_AstNode {
                        .src{first},
                        .parent_ast_file = ctx,
                        .it = MetaTu_init(Yuceyuf_AstNode_Specific, identifier, {
                            .identifier = MetaTu_get(TokenData, &first_token.data, identifier)->id,
                        }),
                    };
                    prev_ast_node = node;
                }
            }
            break;
            case TokenData_Tag__simple: {
                switch (first_token.data.payload.simple) {
                    case MetaEnum_count(TokenSimple):
                        yy_unreachable();
                    case TokenSimple__assign:
                    case TokenSimple__rparen:
                    case TokenSimple__rbrace:
                    case TokenSimple__rbracket:
                    case TokenSimple__semicolon:
                    case TokenSimple__comma:
                    case TokenSimple__assign_plus:
                    case TokenSimple__assign_minus:
                    case TokenSimple__assign_mul:
                    case TokenSimple__assign_plus_wrap:
                    case TokenSimple__assign_minus_wrap:
                    case TokenSimple__assign_mul_wrap:
                    case TokenSimple__assign_div:
                    case TokenSimple__assign_mod:
                    case TokenSimple__assign_bits_and:
                    case TokenSimple__assign_bits_or:
                    case TokenSimple__assign_bits_xor:
                    case TokenSimple__assign_sh_left:
                    case TokenSimple__assign_sh_right:
                        yy_unreachable();
                    case TokenSimple__colon: {
                        ast_add_error_msg__fancy(ctx, sv_lit("unexpected"), first);
                        return Yy_Bad;
                    }
                    break;
                    case TokenSimple__colon_colon: {
                        if (prev_ast_node == NULL) {
                            ast_add_error_msg(
                                ctx, ctx->lexer.lex.token_src(first),
                                "mod field access without left-hand side expression"
                            );
                            return Yy_Bad;
                        }
                        ctx->lexer.advance();
                        TokenId const field_tok = ctx->lexer.peek();
                        TokenIdentifier field_as_identifier = yy::uninitialized;
                        TRY(token_as_identifier(ctx, field_tok, &field_as_identifier));
                        ctx->lexer.advance();
                        Yuceyuf_AstNode *const container = prev_ast_node;
                        Yuceyuf_AstNode *const node = create_ast_node(ctx);
                        *node = Yuceyuf_AstNode {
                            .src{container->src.begin, first, field_tok},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, mod_field_access, {
                                .container = container,
                                .field = field_as_identifier.id,
                            }),
                        };
                        prev_ast_node = node;
                    }
                    break;
                    case TokenSimple__lbrace: {
                        if (prev_ast_node != NULL) {
                            goto loop_end;
                        }
                        Yuceyuf_AstNode *node;
                        TRY(parse_stmt_block(ctx, &node));
                        prev_ast_node = node;
                    }
                    break;
                    case TokenSimple__lbracket:
                        todo();
                        break;
                    case TokenSimple__dot: {
                        if (prev_ast_node == NULL) {
                            ast_add_error_msg(
                                ctx, ctx->lexer.lex.token_src(first), "field access without left-hand side expression"
                            );
                            return Yy_Bad;
                        }
                        ctx->lexer.advance();
                        TokenId const field_tok = ctx->lexer.peek();
                        TokenIdentifier field_as_identifier = yy::uninitialized;
                        TRY(token_as_identifier(ctx, field_tok, &field_as_identifier));
                        ctx->lexer.advance();
                        Yuceyuf_AstNode *const container = prev_ast_node;
                        Yuceyuf_AstNode *const node = create_ast_node(ctx);
                        *node = Yuceyuf_AstNode {
                            .src{container->src.begin, first, field_tok},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, field_access, {
                                .container = container,
                                .field = field_as_identifier.id,
                            }),
                        };
                        prev_ast_node = node;
                    }
                    break;
                    case TokenSimple__lparen: {
                        if (prev_ast_node != NULL) {
                            // function call
                            Yuceyuf_AstNode *const func_expr = prev_ast_node;

                            // Create the node and initialize it with empty arguments
                            Yuceyuf_AstNode *const node = create_ast_node(ctx);
                            *node = Yuceyuf_AstNode {
                                .src{func_expr->src.begin, first, yy::uninitialized},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, fn_call, {
                                    .func = func_expr,
                                    .args = ast_list_init_empty(),
                                    .args_has_trailing_comma = false, // init later
                                }),
                            };

                            // Parse the arguments
                            TRY(parse_fn_call_args(ctx, &node->it.payload.fn_call.args,
                                                   &node->it.payload.fn_call.args_has_trailing_comma,
                                                   &node->src.end_inclusive));
                            prev_ast_node = node;
                        } else {
                            // parenthesized expression
                            ctx->lexer.advance();
                            Yuceyuf_AstNode *expr;
                            TRY(parse_expr(ctx, &expr));

                            TokenId const rparen_tok = ctx->lexer.peek();
                            TRY(expect_token_simple(ctx, TokenSimple__rparen));
                            Yuceyuf_AstNode *const node = create_ast_node(ctx);
                            *node = Yuceyuf_AstNode {
                                .src{first, first, rparen_tok},
                                .parent_ast_file = ctx,
                                .it = MetaTu_init(Yuceyuf_AstNode_Specific, paren, expr),
                            };
                            prev_ast_node = node;
                        }
                    }
                    break;
                    case TokenSimple__deref: {
                        TRY(ast_unary_postfix_op_must_have_lhs(ctx, first, prev_ast_node));
                        ctx->lexer.advance();
                        Yuceyuf_AstNode *const node = create_ast_node(ctx);
                        *node = Yuceyuf_AstNode {
                            .src{prev_ast_node->src.begin, first, first},
                            .parent_ast_file = ctx,
                            .it = MetaTu_init(Yuceyuf_AstNode_Specific, deref, prev_ast_node),
                        };
                        prev_ast_node = node;
                    }
                    break;
                    case TokenSimple__plus:
                    case TokenSimple__plus_wrap:
                    case TokenSimple__minus:
                    case TokenSimple__minus_wrap: {
                        if (prev_ast_node == NULL)
                            TRY(parse_unary_prefix_op(ctx, &prev_ast_node));
                        else
                            TRY(parse_bin_op(ctx, &prev_ast_node));
                    }
                    break;
                    case TokenSimple__ampersand: {
                        if (prev_ast_node == NULL)
                            TRY(parse_unary_prefix_op(ctx, &prev_ast_node));
                        else
                            TRY(parse_bin_op(ctx, &prev_ast_node));
                    }
                    break;
                    case TokenSimple__mul: {
                        if (prev_ast_node != NULL) {
                            TRY(parse_bin_op(ctx, &prev_ast_node));
                        } else {
                            TRY(parse_unary_prefix_op(ctx, &prev_ast_node));
                        }
                    } break;
                    case TokenSimple__mul_wrap:
                    case TokenSimple__div:
                    case TokenSimple__mod:
                    case TokenSimple__bits_or:
                    case TokenSimple__bits_xor:
                    case TokenSimple__lt:
                    case TokenSimple__lte:
                    case TokenSimple__gt:
                    case TokenSimple__gte:
                    case TokenSimple__eq:
                    case TokenSimple__noteq:
                    case TokenSimple__sh_left:
                    case TokenSimple__sh_right:
                        TRY(parse_bin_op(ctx, &prev_ast_node));
                        break;
                    case TokenSimple__bits_not:
                    case TokenSimple__bool_not: {
                        TRY(parse_unary_prefix_op(ctx, &prev_ast_node));
                    }
                    break;
                }
            }
            break;
        }
    }
loop_end:
    yy_assert(prev_ast_node != NULL);
    *result_ast_node = prev_ast_node;
    return Yy_Ok;
}

NODISCARD static YyStatus parse_imported_file(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__ximport));
    TokenIdentifier decl_name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &decl_name));
    TokenId const filename_token_id = ctx->lexer.peek();
    auto const& filename_token = ctx->lexer.lex.token(filename_token_id);
    if (filename_token.data.tag != TokenData_Tag__string_literal) {
        Sv const got_str = filename_token.fancy_name();
        ast_add_error_msg(
            ctx, ctx->lexer.lex.token_src(filename_token_id), "expected string literal but got ‘" Sv_Fmt "’",
            Sv_Arg(got_str)
        );
        return Yy_Bad;
    }
    ctx->lexer.advance();
    TokenId const semicolon = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__semicolon));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, semicolon},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, imported_file, {
            .decl_name = decl_name.id,
            .file_name = MetaTu_get(TokenData, &filename_token.data, string_literal)->id,
            .resolved_ast_file = nullptr,
        }),
    };

    ctx->queued_imported_files.push_back(*result_ast_node);
    return Yy_Ok;
}

NODISCARD static YyStatus parse_one_root(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node);

NODISCARD static YyStatus parse_root_mod(
    Yuceyuf_Ast_File *const ctx,
    Yuceyuf_AstNode **const result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    TRY(expect_token_keyword(ctx, TokenKeyword__mod));

    TokenIdentifier name = yy::uninitialized;
    TRY(expect_token_identifier(ctx, &name));

    Yuceyuf_AstList contents = ast_list_init_empty();
    TRY(expect_token_simple(ctx, TokenSimple__lbrace));
    while (true) {
        if (peek_simple(ctx, TokenSimple__rbrace)) {
            break;
        }
        Yuceyuf_AstNode *item;
        TRY(parse_one_root(ctx, &item));
        ast_list_append(ctx, &contents, item);
    }
    TokenId const rbrace_tok = ctx->lexer.peek();
    TRY(expect_token_simple(ctx, TokenSimple__rbrace));

    *result_ast_node = create_ast_node(ctx);
    **result_ast_node = Yuceyuf_AstNode {
        .src{first, first, rbrace_tok},
        .parent_ast_file = ctx,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, mod_decl, {
            .name = name.id,
            .contents = std::move(contents),
        }),
    };
    return Yy_Ok;
}

NODISCARD static YyStatus parse_one_root(Yuceyuf_Ast_File *ctx, Yuceyuf_AstNode **result_ast_node)
{
    TokenId const first = ctx->lexer.peek();
    if (peek_keyword(ctx, TokenKeyword__fn)) {
        return parse_fn_decl(ctx, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__xextern)) {
        return parse_root_extern(ctx, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__xstruct)) {
        return parse_root_struct(ctx, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__xvar)) {
        return parse_global_var_decl(ctx, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__ximport)) {
        return parse_imported_file(ctx, result_ast_node);
    }
    if (peek_keyword(ctx, TokenKeyword__mod)) {
        return parse_root_mod(ctx, result_ast_node);
    }

    ast_add_error_msg(ctx, ctx->lexer.lex.token_src(first), "unexpected token in root");
    return Yy_Bad;
}

NODISCARD static YyStatus ast_unresolved_from_tokens(Yuceyuf_Ast_File *ctx)
{
    while (true) {
        if (ctx->lexer.lex.token(ctx->lexer.peek()).data.tag == TokenData_Tag__eof)
            break;
        Yuceyuf_AstNode *node;
        TRY(parse_one_root(ctx, &node));
        ast_list_append(ctx, &ctx->root->it.payload.root.top_list, node);
    }
    return Yy_Ok;
}

NODISCARD u32 yuceyuf_ast_filename_hash_for_module_lookup(Sv filename)
{
    return yy_weak_hash_of_bv(filename.as_bytes());
}

NODISCARD auto yuceyuf_ast_file_unresolved_from_tokens(
    Yy_Arena *arena,
    Sv filename,
    u32 package_root_dir_components,
    LexedFile const& lexed_file
) -> std::pair<Yuceyuf_Ast_File*, YyStatus>
{
    // TODO: Properly support dummy `filename`s here.
    Sv dirname_of_filename;
    CATCH(path_get_dirname(filename, &dirname_of_filename), yy_unreachable());
    {
        u8 const path_sep = '/';
        for (usize i = 1; i < filename.len; i++) {
            yy_assert(!(filename.ptr[i] == path_sep && filename.ptr[i - 1] == path_sep));
        }
        if (dirname_of_filename.len > 1) {
            yy_assert(dirname_of_filename.ptr[dirname_of_filename.len - 1] != path_sep);
        }
    }
    auto const result_ast_file = allocator_new_uninit<Yuceyuf_Ast_File>(arena->allocator());
    new(result_ast_file) Yuceyuf_Ast_File {
        .arena = arena,
        .filename = filename,
        .filename_hash_for_module_lookup = yuceyuf_ast_filename_hash_for_module_lookup(filename),
        .package_root_dir_components = package_root_dir_components,
        .path_dirname = dirname_of_filename,
        .lexer = LexedFileIterator{lexed_file},
        .root = nullptr, // init later
        .errors{arena->allocator()},
        .queued_imported_files{arena->allocator()},
    };
    result_ast_file->root = create_ast_node(result_ast_file);
    *result_ast_file->root = Yuceyuf_AstNode {
        .src{TokenId{0}, TokenId{0}, yy::uninitialized},
        .parent_ast_file = result_ast_file,
        .it = MetaTu_init(Yuceyuf_AstNode_Specific, root, { .top_list = ast_list_init_empty() }),
    };
    CATCH2(ast_unresolved_from_tokens(result_ast_file), err, {
        yy_assert(result_ast_file->errors.size() > 0);
        return std::make_pair(result_ast_file, err);
    });
    TokenId const eof_tok = result_ast_file->lexer.peek();;
    yy_assert(result_ast_file->lexer.lex.token(eof_tok).data.tag == TokenData_Tag__eof);
    result_ast_file->root->src.end_inclusive = eof_tok;

    return {result_ast_file, Yy_Ok};
}

typedef struct {
    Sv filename;
    u32 indent;
    usize visited_nodes;
} AstDebugPrintCtx;

#define ast_debug_print_indent1_node(ctx, node, fmt) do { \
    ast_debug_print_indent1(ctx, fmt);                    \
    (ctx)->indent += 1;                                   \
    ast_debug_print_node(ctx, node);                      \
    (ctx)->indent -= 1;                                   \
} while (false)
#define ast_debug_print_indent1_list(ctx, list, fmt) do { \
    ast_debug_print_indent1(ctx, fmt);                    \
    (ctx)->indent += 1;                                   \
    ast_debug_print_list(ctx, list);                      \
    (ctx)->indent -= 1;                                   \
} while (false)
#define ast_debug_print_indent1(ctx, fmt) fprintf(stderr, "%*s| " fmt "\n", (int)(2*(ctx)->indent), "")
#define ast_debug_print_indent2(ctx, fmt, ...) fprintf(stderr, "%*s| " fmt "\n", (int)(2*(ctx)->indent), "", __VA_ARGS__)

#define ast_debug_print_indent1_pos(ctx, tok_loc, fmt) fprintf(stderr, "%*s| " fmt "\t# " FilePos_Fmt "\n", (int)(2*(ctx)->indent), "", FilePos_Arg((ctx)->filename, tok_loc))
#define ast_debug_print_indent2_pos(ctx, tok_loc, fmt, ...) fprintf(stderr, "%*s| " fmt "\t# " FilePos_Fmt "\n", (int)(2*(ctx)->indent), "", __VA_ARGS__, FilePos_Arg((ctx)->filename, tok_loc))

static void ast_debug_print_list(AstDebugPrintCtx *ctx, Yuceyuf_AstList const *list);
static void ast_debug_print_node(AstDebugPrintCtx *ctx, Yuceyuf_AstNode const *node);

static void ast_debug_print_node(AstDebugPrintCtx *ctx, Yuceyuf_AstNode const *node)
{
    ctx->visited_nodes += 1;
    Sv const tag_sv = MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag);
    auto const node_pos = node->parent_ast_file->lexer.lex.token_src(node->src.mid)
                              .begin.to_line_and_column(node->parent_ast_file->lexer.lex.line_info);
    ast_debug_print_indent2_pos(ctx, node_pos, Sv_Fmt, Sv_Arg(tag_sv));
    ctx->indent += 1;
    auto* const global_comp = node->parent_ast_file->lexer.lex.global;
    switch (node->it.tag) {
        case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
            yy_unreachable();
            break;
        case Yuceyuf_AstNode_Specific_Tag__root: {
            ast_debug_print_list(ctx, &node->it.payload.root.top_list);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_decl_arg_info);
            if (pl.name.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.name.unwrap());
                ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
            ast_debug_print_indent1_node(ctx, pl.ty_expr, "type:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_fn_decl_arg_info);
            if (pl.name.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.name.unwrap());
                ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
            yy_assert(pl.type_expr != NULL);
            ast_debug_print_indent1_node(ctx, pl.type_expr, "type:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__struct_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, struct_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_indent1_list(ctx, &pl.members, "members:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__struct_decl_member_info: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, struct_decl_member_info);
            if (pl.name.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.name.unwrap());
                ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
            ast_debug_print_indent1_node(ctx, pl.ty_expr, "type:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__fn_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_indent1_list(ctx, &pl.args, "args:");
            ast_debug_print_indent1_node(ctx, pl.return_type, "return_type:");
            ast_debug_print_indent1_node(ctx, pl.body, "body:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_fn_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_indent1_list(ctx, &pl.args, "args:");
            ast_debug_print_indent1_node(ctx, pl.return_type, "return_type:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__local_var_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, local_var_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            if (pl.ty_expr.has_value())
                ast_debug_print_indent1_node(ctx, pl.ty_expr.unwrap(), "type:");
            ast_debug_print_indent1_node(ctx, pl.initial_value, "initial value:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__global_var_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, global_var_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            if (pl.ty_expr.has_value())
                ast_debug_print_indent1_node(ctx, pl.ty_expr.unwrap(), "type:");
            ast_debug_print_indent1_node(ctx, pl.initial_value, "initial value:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__extern_var_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_var_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_indent1_node(ctx, pl.ty_expr, "type:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__imported_file: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, imported_file);
            auto const name_sv = global_comp->identifier_sv(pl.decl_name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            auto const filename_sv = node->parent_ast_file->lexer.lex.string_literal(pl.file_name);
            ast_debug_print_indent2(ctx, "file: " Sv_Fmt "", Sv_Arg(filename_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__fn_call: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_call);
            ast_debug_print_indent1_node(ctx, pl.func, "func:");
            ast_debug_print_indent1_list(ctx, &pl.args, "args:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, builtin_fn_call);
            Sv const bfn_sv = MetaEnum_sv_short(TokenBuiltinFn, pl.bfn);
            ast_debug_print_indent2(ctx, "func: ‘@" Sv_Fmt "’", Sv_Arg(bfn_sv));
            ast_debug_print_indent1_list(ctx, &pl.args, "args:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__stmt_block: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, stmt_block);
            ast_debug_print_list(ctx, &pl.stmts);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__labeled_block: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, labeled_block);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_list(ctx, &pl.stmts);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__mod_decl: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_decl);
            auto const name_sv = global_comp->identifier_sv(pl.name);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            ast_debug_print_indent1_list(ctx, &pl.contents, "contents:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__field_access: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, field_access);
            auto const field_sv = global_comp->identifier_sv(pl.field);
            ast_debug_print_indent1_node(ctx, pl.container, "container:");
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(field_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__mod_field_access: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_field_access);
            auto const field_sv = global_comp->identifier_sv(pl.field);
            ast_debug_print_indent1_node(ctx, pl.container, "container:");
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(field_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__deref: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, deref);
            ast_debug_print_node(ctx, pl);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__identifier: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, identifier);
            auto const name_sv = global_comp->identifier_sv(pl.identifier);
            ast_debug_print_indent2(ctx, "name: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__paren: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, paren);
            ast_debug_print_node(ctx, pl);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__return_expr: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, return_expr);
            ast_debug_print_node(ctx, pl.expr);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__xcontinue: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, xcontinue);
            if (pl.label.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.label.unwrap());
                ast_debug_print_indent2(ctx, "label: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__break_void: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, break_void);
            if (pl.label.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.label.unwrap());
                ast_debug_print_indent2(ctx, "label: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__break_expr: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, break_expr);
            if (pl.label.has_value()) {
                auto const name_sv = global_comp->identifier_sv(pl.label.unwrap());
                ast_debug_print_indent2(ctx, "label: ‘" Sv_Fmt "’", Sv_Arg(name_sv));
            }
            ast_debug_print_node(ctx, pl.expr);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_unary_prefix);
            Sv const op_name = MetaEnum_sv_short(Yuceyuf_Ast_UnaryOp, pl.tag);
            ast_debug_print_indent2(ctx, "op: ‘" Sv_Fmt "’", Sv_Arg(op_name));
            ast_debug_print_indent1_node(ctx, pl.operand, "operand:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__assign: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, assign);
            ast_debug_print_indent1_node(ctx, pl.lhs, "lhs:");
            ast_debug_print_indent1_node(ctx, pl.rhs, "rhs:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__assign_bin_op: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, assign_bin_op);
            Sv const op_name = MetaEnum_sv_short(Yuceyuf_Ast_BinaryOp, pl.op);
            ast_debug_print_indent2(ctx, "op: ‘" Sv_Fmt "’", Sv_Arg(op_name));
            ast_debug_print_indent1_node(ctx, pl.lhs, "lhs:");
            ast_debug_print_indent1_node(ctx, pl.rhs, "rhs:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__op_bin: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_bin);
            Sv const op_name = MetaEnum_sv_short(Yuceyuf_Ast_BinaryOp, pl.op);
            ast_debug_print_indent2(ctx, "op: ‘" Sv_Fmt "’", Sv_Arg(op_name));
            ast_debug_print_indent1_node(ctx, pl.lhs, "lhs:");
            ast_debug_print_indent1_node(ctx, pl.rhs, "rhs:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__ignore_result: {
            ast_debug_print_node(ctx, node->it.payload.ignore_result);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__integer_literal: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, integer_literal);
            ast_debug_print_indent2(ctx, "number: ‘%" PRIi64 "’", pl.number);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__char_literal: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, char_literal);
            ast_debug_print_indent2(ctx, "codepoint: ‘%d’", pl.codepoint);
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__string_literal: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, string_literal);
            auto sv = node->parent_ast_file->lexer.lex.string_literal(pl.id);
            ast_debug_print_indent2(ctx, "value: ‘" Sv_Fmt "’", Sv_Arg(sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__undefined_literal: {
            ast_debug_print_indent2(ctx, "value: ‘%s’", "undefined");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__bool_literal: {
            ast_debug_print_indent2(ctx, "value: ‘%s’", node->it.payload.bool_literal ? "true" : "false");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__builtin_typ: {
            Sv const typ_sv = MetaEnum_sv_short(TokenKeyword, node->it.payload.builtin_typ);
            ast_debug_print_indent2(ctx, "typ: ‘" Sv_Fmt "’", Sv_Arg(typ_sv));
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__expr_if: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, expr_if);
            ast_debug_print_indent1_node(ctx, pl.cond, "cond:");
            ast_debug_print_indent1_node(ctx, pl.then, "then:");
            if (pl.elze.has_value())
                ast_debug_print_indent1_node(ctx, pl.elze.unwrap(), "else:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__stmt_if: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, stmt_if);
            ast_debug_print_indent1_node(ctx, pl.cond, "cond:");
            ast_debug_print_indent1_node(ctx, pl.then, "then:");
            if (pl.elze.has_value())
                ast_debug_print_indent1_node(ctx, pl.elze.unwrap(), "else:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__xwhile: {
            auto const& pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, xwhile);
            ast_debug_print_indent1_node(ctx, pl.cond, "cond:");
            ast_debug_print_indent1_node(ctx, pl.body, "body:");
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__return_void:
        case Yuceyuf_AstNode_Specific_Tag__unreach:
            break;
    }
    ctx->indent -= 1;
}

static void ast_debug_print_list(AstDebugPrintCtx *ctx, Yuceyuf_AstList const *list)
{
    for (Yuceyuf_AstNode* iter : list->nodes) {
        ast_debug_print_node(ctx, iter);
    }
}

void yuceyuf_ast_debug_print_root(Yuceyuf_AstNode *root, Sv const filename)
{
    yy_assert(root->it.tag == Yuceyuf_AstNode_Specific_Tag__root);
    AstDebugPrintCtx ctx = {
        .filename = filename,
        .indent = 0,
        .visited_nodes = 0,
    };
    ast_debug_print_node(&ctx, root);
    fprintf(stderr, "# there were %zu ast nodes\n", ctx.visited_nodes);
}
