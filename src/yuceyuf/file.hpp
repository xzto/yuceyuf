#pragma once

#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/sv.hpp"

struct Yuceyuf_File {
    int fd;
};

extern Yuceyuf_File const YUCEYUF_AT_FDCWD;

NODISCARD YyStatus yuceyuf_read_entire_file_bytes(
    Yy_Allocator allocator,
    Yuceyuf_File at_dir_fd, Sv filename,
    usize maximum_file_size, BvMut *result_contents, int *const result_os_error);

NODISCARD inline YyStatus yuceyuf_read_entire_file_chars(
    Yy_Allocator allocator,
    Yuceyuf_File at_dir_fd, Sv filename,
    usize maximum_file_size, SvMut *result_contents, int *const result_os_error) {
    BvMut result_bytes;
    auto result_status = yuceyuf_read_entire_file_bytes(
        allocator, at_dir_fd, filename, maximum_file_size, &result_bytes, result_os_error
    );
    if (result_status.is_ok()) {
        *result_contents = result_bytes.transmute<char>();
    }
    return result_status;
}
