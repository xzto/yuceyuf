#pragma once

#include <cstdarg>
#include "yy/basic.hpp"
#include "yy/arena.hpp"
#include "yy/newer_hash_map.hpp"
#include "yy/sv.hpp"
#include "yy/vec.hpp"
#include "yy/meta/enum.hpp"
#include "yy/meta/tagged_union.hpp"
#include "yuceyuf/compilation.hpp"

#define FilePos_Fmt Sv_Fmt":%u:%u"
#define FilePos_Arg(filename, pos) Sv_Arg(filename), (pos).line1, (pos).column1

namespace yuceyuf::lexer {

struct LineInfo;

struct LineColumn {
    u32 line1;
    /// column is a byte offset
    u32 column1;
};

struct FileOffset {
    explicit FileOffset(u32 x) : x(x) {}
    u32 x;
    NODISCARD friend auto operator==(FileOffset, FileOffset) -> bool = default;
    NODISCARD friend auto operator-(FileOffset a, FileOffset b) -> u32 { return a.x - b.x; }
    NODISCARD friend auto operator-(FileOffset a, u32 b) -> FileOffset { return FileOffset{a.x - b}; }
    NODISCARD friend auto operator+(FileOffset a, u32 b) -> FileOffset { return FileOffset{a.x + b}; }
    NODISCARD friend auto operator<=>(FileOffset, FileOffset) = default;

    NODISCARD auto to_line_and_column(LineInfo const& line_info, Option<LineColumn> hint = None) const -> LineColumn;
};

struct SourceRegionLineColumn;
struct LexSourceRegion {
    FileOffset begin;
    /// `end` is exclusive.
    FileOffset end;

    NODISCARD auto to_src_region_line_and_column(LineInfo const& line_info) const -> SourceRegionLineColumn;
};

struct SourceRegionLineColumn {
    LineColumn begin;
    LineColumn mid;
    LineColumn end;
};

struct LineInfo {
    Span<const FileOffset> line0_start_offset;
    static_assert(yy::trivial_raii<FileOffset>);
};

struct StringLiteralId {
    constexpr StringLiteralId() = default;
    constexpr explicit StringLiteralId(u32 x) : x(x) {}
    u32 x;
    NODISCARD friend auto operator==(StringLiteralId, StringLiteralId) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(StringLiteralId, StringLiteralId{UINT32_MAX});
};

struct TokenId {
    constexpr TokenId() = default;
    constexpr explicit TokenId(u32 x) : x(x) {}
    u32 x;
    NODISCARD friend auto operator==(TokenId, TokenId) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(TokenId, TokenId{UINT32_MAX});
};


struct TokenIntegerLiteral {
    i64 number;
};

struct TokenCharLiteral {
    u32 codepoint;
};

/// Identifiers never contain '\0'.
struct TokenIdentifier {
    IdentifierId id;
};

struct TokenStringLiteral {
    StringLiteralId id;
};

#define YY_META_ENUM__NAME TokenSimple
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS              \
    YY_META_ENUM__MEMBER2(lparen,      "(")    \
    YY_META_ENUM__MEMBER2(rparen,      ")")    \
    YY_META_ENUM__MEMBER2(lbrace,      "{")    \
    YY_META_ENUM__MEMBER2(rbrace,      "}")    \
    YY_META_ENUM__MEMBER2(lbracket,    "[")    \
    YY_META_ENUM__MEMBER2(rbracket,    "]")    \
    YY_META_ENUM__MEMBER2(dot,         ".")    \
    YY_META_ENUM__MEMBER2(deref,       ".*")   \
    YY_META_ENUM__MEMBER2(semicolon,   ";")    \
    YY_META_ENUM__MEMBER2(colon,       ":")    \
    YY_META_ENUM__MEMBER2(colon_colon, "::")   \
    YY_META_ENUM__MEMBER2(comma,       ",")    \
    YY_META_ENUM__MEMBER2(assign,      "=")    \
    YY_META_ENUM__MEMBER2(plus,        "+")    \
    YY_META_ENUM__MEMBER2(minus,       "-")    \
    YY_META_ENUM__MEMBER2(mul,         "*")    \
    YY_META_ENUM__MEMBER2(plus_wrap,   "+%")   \
    YY_META_ENUM__MEMBER2(minus_wrap,  "-%")   \
    YY_META_ENUM__MEMBER2(mul_wrap,    "*%")   \
    YY_META_ENUM__MEMBER2(div,         "/")    \
    YY_META_ENUM__MEMBER2(mod,         "%")    \
    YY_META_ENUM__MEMBER2(assign_plus,     "+=")   \
    YY_META_ENUM__MEMBER2(assign_minus,    "-=")   \
    YY_META_ENUM__MEMBER2(assign_mul,      "*=")   \
    YY_META_ENUM__MEMBER2(assign_plus_wrap,  "+%=")   \
    YY_META_ENUM__MEMBER2(assign_minus_wrap, "-%=")   \
    YY_META_ENUM__MEMBER2(assign_mul_wrap,   "*%=")   \
    YY_META_ENUM__MEMBER2(assign_div,      "/=")   \
    YY_META_ENUM__MEMBER2(assign_mod,      "%=")   \
    YY_META_ENUM__MEMBER2(bool_not,        "!")    \
    YY_META_ENUM__MEMBER2(bits_not,        "~")    \
    YY_META_ENUM__MEMBER2(ampersand,       "&")    \
    YY_META_ENUM__MEMBER2(bits_or,         "|")    \
    YY_META_ENUM__MEMBER2(bits_xor,        "^")    \
    YY_META_ENUM__MEMBER2(assign_bits_and, "&=")   \
    YY_META_ENUM__MEMBER2(assign_bits_or,  "|=")   \
    YY_META_ENUM__MEMBER2(assign_bits_xor, "^=")   \
    YY_META_ENUM__MEMBER2(lt,          "<")    \
    YY_META_ENUM__MEMBER2(lte,         "<=")   \
    YY_META_ENUM__MEMBER2(gt,          ">")    \
    YY_META_ENUM__MEMBER2(gte,         ">=")   \
    YY_META_ENUM__MEMBER2(eq,          "==")   \
    YY_META_ENUM__MEMBER2(noteq,       "!=")   \
    YY_META_ENUM__MEMBER2(sh_left,         "<<")   \
    YY_META_ENUM__MEMBER2(sh_right,        ">>")   \
    YY_META_ENUM__MEMBER2(assign_sh_left,  "<<=")  \
    YY_META_ENUM__MEMBER2(assign_sh_right, ">>=")
#include "yy/meta/enum.hpp"

#define YY_META_ENUM__NAME TokenKeyword
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS                      \
    YY_META_ENUM__MEMBER(fn)                           \
    YY_META_ENUM__MEMBER(mod)                          \
    YY_META_ENUM__MEMBER2(xstruct,     "struct")       \
    YY_META_ENUM__MEMBER2(xextern,     "extern")       \
    YY_META_ENUM__MEMBER2(ximport,     "import")       \
    YY_META_ENUM__MEMBER2(xreturn,     "return")       \
    YY_META_ENUM__MEMBER2(xbreak,      "break")        \
    YY_META_ENUM__MEMBER2(xcontinue,   "continue")     \
    YY_META_ENUM__MEMBER2(xvar,        "var")          \
    YY_META_ENUM__MEMBER2(xwhile,      "while")        \
    YY_META_ENUM__MEMBER2(xif,         "if")           \
    YY_META_ENUM__MEMBER2(xelse,       "else")         \
    YY_META_ENUM__MEMBER2(xtrue,       "true")         \
    YY_META_ENUM__MEMBER2(xfalse,      "false")        \
    YY_META_ENUM__MEMBER2(xnoreturn,   "noreturn")     \
    YY_META_ENUM__MEMBER2(xvoid,       "void")         \
    YY_META_ENUM__MEMBER2(xbool,       "bool")         \
    YY_META_ENUM__MEMBER2(xundefined,  "undefined")    \
    YY_META_ENUM__MEMBER(uintword)                     \
    YY_META_ENUM__MEMBER2(xu8, "u8")                   \
    YY_META_ENUM__MEMBER2(unreach,     "unreachable")  \
    YY_META_ENUM__MEMBER2(bool_and,    "and")          \
    YY_META_ENUM__MEMBER2(bool_or,     "or")           \
    YY_META_ENUM__MEMBER2(underscore,  "_")
#include "yy/meta/enum.hpp"

#define YY_META_ENUM__NAME TokenBuiltinFn
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS           \
    YY_META_ENUM__MEMBER(panic)             \
    YY_META_ENUM__MEMBER(halt)              \
    YY_META_ENUM__MEMBER(bool_to_int)       \
    YY_META_ENUM__MEMBER(truncate)          \
    YY_META_ENUM__MEMBER(as)                \
    YY_META_ENUM__MEMBER(print_hello_world) \
    YY_META_ENUM__MEMBER(print_ip)          \
    YY_META_ENUM__MEMBER(print_uint)        \
    YY_META_ENUM__MEMBER(ptr_to_int)        \
    YY_META_ENUM__MEMBER(int_to_ptr)        \
    YY_META_ENUM__MEMBER(ptr_cast)          \
YY_EMPTY_MACRO
#include "yy/meta/enum.hpp"

#define YY_META_TAGGED_UNION__NAME TokenData
#define YY_META_ENUM__INT u8
#define YY_META_TAGGED_UNION__ALL_MEMBERS                              \
    YY_META_TAGGED_UNION__MEMBER(Void, eof)                            \
    YY_META_TAGGED_UNION__MEMBER(TokenSimple, simple)                  \
    YY_META_TAGGED_UNION__MEMBER(TokenKeyword, keyword)                \
    YY_META_TAGGED_UNION__MEMBER(TokenBuiltinFn, builtin_fn)           \
    YY_META_TAGGED_UNION__MEMBER(TokenIdentifier, identifier)          \
    YY_META_TAGGED_UNION__MEMBER(TokenIntegerLiteral, integer_literal) \
    YY_META_TAGGED_UNION__MEMBER(TokenCharLiteral, char_literal)       \
    YY_META_TAGGED_UNION__MEMBER(TokenStringLiteral, string_literal)   \
YY_EMPTY_MACRO
#include "yy/meta/tagged_union.hpp"


struct Token {
    TokenData data;

    NODISCARD auto is_simple(TokenSimple simple) const -> bool {
        return data.tag == yuceyuf::lexer::TokenData_Tag__simple && data.payload.simple == simple;
    }
    NODISCARD auto is_keyword(TokenKeyword kw) const -> bool {
        return data.tag == yuceyuf::lexer::TokenData_Tag__keyword && data.payload.keyword == kw;
    }

    NODISCARD auto fancy_name() const -> Sv;
};

struct LexerCtx;
struct LexedFile : yy::MoveOnly {
    LexedFile(LexerCtx&&);

    GlobalCompilation* global;
    Yy_ArenaCpp arena;
    FileId id;
    Sv filename;
    Sv contents;
    // TODO: MultiVec<A, B, ...>
    Span<const Token> m_tokens;
    Span<const LexSourceRegion> m_token_srcs;
    Span<const Sv> m_string_literals;
    LineInfo line_info;
    static_assert(yy::trivial_raii<Token>);
    static_assert(yy::trivial_raii<LexSourceRegion>);
    static_assert(yy::trivial_raii<Sv>);
    static_assert(yy::trivial_raii<LineInfo>);
    NODISCARD auto token(TokenId const token_id) const -> Token const& { return m_tokens[token_id.x]; }
    NODISCARD auto token_src(TokenId const token_id) const -> LexSourceRegion const& { return m_token_srcs[token_id.x]; }
    NODISCARD auto string_literal(StringLiteralId const string_id) const -> Sv { return m_string_literals[string_id.x]; }
};

struct LexedFileIterator {
    explicit LexedFileIterator(LexedFile const& lex) : lex(lex) {
        yy_assert(lex.m_tokens.size() > 0);
    }
    LexedFile const& lex;
    TokenId next_id{TokenId{0}};

    auto advance() -> void;
    NODISCARD auto next() -> TokenId;
    NODISCARD auto peek() const -> TokenId { return peek_nth(0); }
    NODISCARD auto peek_nth(u32 nth) const -> TokenId;
};

static constexpr usize ERROR_LIMIT = 20;
enum class Yuceyuf_ErrorMessageKind {
    note,
    soft_error,
    hard_error,
};

struct Yuceyuf_ErrorMessage {
    Option<Sv> filename;
    Option<SourceRegionLineColumn> src_region;
    bool is_note;
    Sv msg;
};

struct Yuceyuf_ErrorBundle {
    explicit Yuceyuf_ErrorBundle(Allocator allocator) : arena(allocator, 0), msgs{} {}

    Yy_ArenaCpp arena;
    // Vec in the arena. ErrorMessage.msg in the arena.
    VecUnmanagedLeaked<Yuceyuf_ErrorMessage> msgs;
    usize soft_count{0};
    usize hard_count{0};
    // Fatal is set at the end, meaning that we can't continue to the next stage.
    bool fatal{false};

    PRINTF_LIKE(5, 6)
    auto make_error(
        Yuceyuf_ErrorMessageKind kind,
        Option<Sv> filename,
        Option<SourceRegionLineColumn> src_region,
        char const* fmt,
        ...
    ) -> void {
        va_list ap;
        va_start(ap, fmt);
        make_error_va(kind, filename, src_region, fmt, ap);
        va_end(ap);
    }
    auto make_error_va(
        Yuceyuf_ErrorMessageKind kind,
        Option<Sv> filename,
        Option<SourceRegionLineColumn> src_region,
        char const* fmt,
        va_list ap
    ) -> void;
};


struct LexFileResult {
    // TODO: Result<T, E>
    Option<LexedFile&> lexed_file;
    Option<Yuceyuf_ErrorBundle> errors;
    LexFileResult(LexedFile& lexed_file) : lexed_file(lexed_file), errors{None} {}
    LexFileResult(Yuceyuf_ErrorBundle&& errors)
        : lexed_file{None}, errors(std::move(errors)) {}
};
NODISCARD auto lex_file(
    GlobalCompilation* global,
    Sv filename,
    Sv contents,
    Allocator heap
) -> LexFileResult;



auto debug_token_print(LexedFile const& lex, TokenId const token_id) -> void;



}


using namespace yuceyuf::lexer;

auto yuceyuf_error_message_print_stderr(
    Yuceyuf_ErrorMessage const* err_msg,
    bool do_color
) -> void;





extern bool verbose_tokens;
