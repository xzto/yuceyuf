#include "yy/basic.hpp"
#include "yy/arena.hpp"
#include "yy/array_list_printf.hpp"
#include "yy/sv.hpp"
#include "yuceyuf/lexer.hpp"
#include "yuceyuf/compilation.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// TODO: Pad `file_contents` with zeroes at the end so that we don't need bounds checking on some operations, like in
// `contents.try_get(i + 0) == '/' && contents.try_get(i + 1) == '/'. '\0' was chosen because it's not a valid token. If
// we find '\0' it's either the end of the buffer or an invalid token in the middle of the buffer.

namespace yuceyuf::lexer {

auto Yuceyuf_ErrorBundle::make_error_va(
    Yuceyuf_ErrorMessageKind kind,
    Option<Sv> filename,
    Option<SourceRegionLineColumn> src_region,
    char const* fmt,
    va_list ap
) -> void {
    auto const allocator = arena.allocator();
    ArrayList_Of_u8 buf{allocator};
    array_list_printf_inner(&buf, fmt, ap);
    auto msg = buf.leak_to_span().transmute<char const>();
    msgs.push_back(allocator, Yuceyuf_ErrorMessage{
        .filename = filename,
        .src_region = src_region,
        .is_note = kind == Yuceyuf_ErrorMessageKind::note,
        .msg = msg,
    });
    switch (kind) {
        using enum Yuceyuf_ErrorMessageKind;
        case note: {} break;
        case soft_error: {
            soft_count++;
        } break;
        case hard_error: {
            hard_count++;
        } break;
    }
}

NODISCARD auto FileOffset::to_line_and_column(LineInfo const& line_info, Option<LineColumn> hint) const -> LineColumn {
    // TODO: Use the hint to search faster.
    // TODO: If no hint, do a binary search.
    (void) hint;
    auto const offset = *this;
    auto const line_count = line_info.line0_start_offset.size();
    yy_assert(line_count > 0);
    for (u32 test_line0 = 0; test_line0 < line_count; test_line0++) {
        auto const test_line_start_offset = line_info.line0_start_offset[test_line0];
        if (test_line_start_offset > offset) {
            u32 const actual_line0 = test_line0 - 1;
            auto const actual_line_start_offset = line_info.line0_start_offset[actual_line0];
            yy_assert(offset >= actual_line_start_offset);
            return {.line1 = test_line0 - 1 + 1, .column1 = offset - actual_line_start_offset + 1};
        }
    }
    u32 const actual_line0 = yy::checked_int_cast<u32>(line_count).unwrap() - 1;
    auto const actual_line_start_offset = line_info.line0_start_offset[actual_line0];
    return {.line1 = actual_line0 + 1, .column1 = offset - actual_line_start_offset + 1};
}

NODISCARD auto LexSourceRegion::to_src_region_line_and_column(LineInfo const& line_info) const -> SourceRegionLineColumn {
    auto line_begin = begin.to_line_and_column(line_info, None);
    auto line_end = end.to_line_and_column(line_info, line_begin);
    return SourceRegionLineColumn{
        .begin = line_begin,
        .mid = line_begin,
        .end = line_end,
    };
}

struct LexerCtx {
    ~LexerCtx() = default;
    GlobalCompilation* global;

    Allocator heap;
    Yy_ArenaCpp arena;
    Yy_ArenaCpp scratch;
    FileId file_id;
    Sv filename;
    Sv contents;
    usize contents_idx;

    NODISCARD auto line_info() -> LineInfo { return LineInfo{.line0_start_offset = line0_start_offset}; }
    VecUnmanagedLeaked<FileOffset> line0_start_offset;
    VecUnmanagedLeaked<Token> tokens;
    VecUnmanagedLeaked<LexSourceRegion> token_srcs;
    VecUnmanagedLeaked<Sv> string_literals; // indexes by StringLiteralId

    Yuceyuf_ErrorBundle errors;

    enum class NextTokenStatus {
        xcontinue,
        eof,
        error,
    };

    NODISCARD auto current_src() const -> LexSourceRegion {
        auto offset = FileOffset{(u32)contents_idx};
        return LexSourceRegion(offset, offset);
    }

    auto make_line() -> void {
        line0_start_offset.push_back(arena.allocator(), FileOffset{(u32)contents_idx});
    }
    NODISCARD auto make_identifier(Sv const name) -> IdentifierId {
        (void) this;
        return global->make_identifier(name);
    }
    /// NOTE: `str` is inside `this->arena`.
    NODISCARD auto make_string_literal(Sv const str) -> StringLiteralId {
        auto result = StringLiteralId(yy::checked_int_cast<u32>(string_literals.size()).unwrap());
        string_literals.push_back(arena.allocator(), str);
        return result;
    }

    NODISCARD static auto is_ident_begin(u8 x) -> bool {
        u8 const x32 = x | 32;
        // NOLINTNEXTLINE
        return ((x32 >= 'a') & (x32 <= 'z')) | (x == '_') | (x > 127);
    }
    NODISCARD static auto is_ident_middle(u8 x) -> bool{
        u8 const x32 = x | 32;
        // NOLINTNEXTLINE
        return ((x32 >= 'a') & (x32 <= 'z')) | (x == '_') | ((x >= '0') & (x <= '9')) | (x > 127);
    }

    /// Returns true when it skipped something.
    NODISCARD auto try_skip_whitespace() -> bool {
        usize i = 0;
        while (contents_idx + i < contents.len) {
            u8 const ch = char_to_u8(contents.ptr[contents_idx + i]);
            if (!(ch == ' ' || ch == '\t'))
                break;
            i += 1;
        }
        if (i > 0) {
            contents_idx += i;
            return true;
        }
        return false;
    }

    /// Returns true when it skipped something. It leaves the lexer at the start of the next line.
    NODISCARD auto try_skip_one_newline() -> bool {
        // Matches either "\r", or "\r\n", or "\n".
        u8 newline_size = 0;
        if (contents.try_get(contents_idx + newline_size) == '\r')
            newline_size += 1;
        if (contents.try_get(contents_idx + newline_size) == '\n')
            newline_size += 1;
        if (newline_size > 0) {
            contents_idx += newline_size;
            make_line();
            return true;
        }
        return false;
    }

    /// Returns true when it skipped something. It leaves the lexer at the end of the line.
    NODISCARD auto try_skip_comment() -> bool {
        if (contents.try_get(contents_idx) == '/' && contents.try_get(contents_idx + 1) == '/') {
            usize i = 2;
            while (contents_idx + i < contents.len) {
                u8 const x = char_to_u8(contents.ptr[contents_idx + i]);
                if (x == '\r' || x == '\n')
                    break;
                i += 1;
            }
            contents_idx += i;
            return true;
        }
        return false;
    }


    auto append_token(Token token, LexSourceRegion src) -> void {
        yy_assert(tokens.size() == token_srcs.size());
        auto const a = arena.allocator();
        tokens.push_back(a, token);
        token_srcs.push_back(a, src);
    }


    COLD PRINTF_LIKE(3, 4) auto add_error_msg(Option<LexSourceRegion> src, char const* fmt, ...) -> void {
        va_list ap;
        va_start(ap, fmt);
        errors.make_error_va(
            Yuceyuf_ErrorMessageKind::hard_error, filename,
            src.map([&](auto x) { return x.to_src_region_line_and_column(line_info()); }), fmt, ap
        );
        va_end(ap);
    }

    NODISCARD static auto char_to_hex(u8 const ascii) -> Option<u8> {
        u8 const ascii32 = ascii | 32;
        if (ascii32 >= 'a' && ascii32 <= 'z')
            return static_cast<u8>(ascii32 - 'a' + 10);
        if (ascii >= '0' && ascii <= '9')
            return static_cast<u8>(ascii - '0');
        return None;
    }

    NODISCARD static auto char_to_digit(u8 const ascii, u8 const base) -> Option<u8> {
        if (base == 16) {
            return char_to_hex(ascii);
        } else if (base <= 10) {
            if (!(ascii >= '0' && ascii <= '0' + base - 1))
                return None;
            return static_cast<u8>(ascii - '0');
        } else {
            yy_unreachable();
        }
    }

    NODISCARD auto lex_integer_literal() -> NextTokenStatus {
        auto const begin_offset = FileOffset{(u32)contents_idx};
        u8 base = 10;
        usize start = 0;
        // Check base prefix
        if (contents_idx + 1 < contents.len) {
            auto const cont_ptr = contents.ptr;
            auto const cont_idx = contents_idx;
            if (cont_ptr[cont_idx] == '0') {
                if (cont_ptr[cont_idx + 1] == 'x') {
                    base = 16;
                    start = 2;
                } else if (cont_ptr[cont_idx + 1] == 'o') {
                    base = 8;
                    start = 2;
                } else if (cont_ptr[cont_idx + 1] == 'b') {
                    base = 2;
                    start = 2;
                } else if (cont_ptr[cont_idx + 1] == 'd') {
                    base = 10;
                    start = 2;
                } else {
                    // base 10 integer
                }
            }
        }
        usize i = start;
        i64 result_number = 0;
        bool got_any_digits_at_all = false;
        for (; true; i += 1) {
            if (contents_idx + i >= contents.len)
                break;
            u8 const ascii = char_to_u8(contents.ptr[contents_idx + i]);
            if (ascii == '_')
                continue;
            auto const digit_opt = char_to_digit(ascii, base);
            if (digit_opt.is_none())
                break;
            auto const digit = digit_opt.unwrap();
            got_any_digits_at_all = true;
            if (__builtin_mul_overflow(result_number, base, &result_number) ||
                __builtin_add_overflow(result_number, digit, &result_number)) {
                add_error_msg(current_src(), "bad integer literal: too big");
                return NextTokenStatus::error;
            }
        }
        if (!got_any_digits_at_all) {
            add_error_msg(current_src(), "bad integer literal: no digits");
            return NextTokenStatus::error;
        }
        // `i` is either out of bounds, or at the first character that isn't a digit
        yy_assert(i > start);

        // Success
        contents_idx += (u32)i;
        auto const end_offset = FileOffset{(u32)contents_idx};
        auto src = LexSourceRegion{begin_offset, end_offset};
        append_token(Token{.data = MetaTu_init(TokenData, integer_literal, {.number = result_number})}, src);
        return NextTokenStatus::xcontinue;
    }

    enum class LexStringLiteralMode {
        identifier,
        double_quote,
        single_quote,
    };

    NODISCARD auto
    lex_string_literal_helper(Allocator allocator, LexStringLiteralMode const mode, Sv* out_str) -> NextTokenStatus {
        using enum LexStringLiteralMode;
        bool const is_identifier = mode == identifier;
        bool const accept_zero = !is_identifier;
        bool const is_single_quote = mode == single_quote;
        u8 const quote_char = is_single_quote ? '\'' : '"';

        usize const quote_start = is_identifier ? 1 : 0;
        yy_assert(char_to_u8(contents[contents_idx + quote_start]) == quote_char);
        Sv err_detail{};
        ArrayList_Of_u8 str_builder{allocator};
        usize i = quote_start + 1;
        while (true) {
            if (contents_idx + i >= contents.len) {
                err_detail = "unexpected end of file in string/char literal";
                goto error;
            }
            u8 const ch = char_to_u8(contents.ptr[contents_idx + i]);
            switch (ch) {
                case '\r':
                case '\n': {
                    err_detail = "line breaks are not allowed";
                    goto error;
                } break;
                case '"':
                case '\'': {
                    if (ch == quote_char) {
                        goto end_loop;
                    } else {
                        str_builder.push_back(ch);
                        i += 1;
                    }
                } break;
                case '\0': {
                    err_detail = "unescaped '\\0'";
                    goto error;
                } break;
                case '\\': {
                    usize const backslash_idx = contents_idx + i;
                    if (!(backslash_idx + 1 < contents.len)) {
                        err_detail = "unexpected end of file in string/char literal";
                        goto error;
                    }
                    switch (contents.ptr[backslash_idx + 1]) {
                        case '\\':
                            str_builder.push_back(u8('\\'));
                            i += 2;
                            break;
                        case 'n':
                            str_builder.push_back(u8('\n'));
                            i += 2;
                            break;
                        case 'r':
                            str_builder.push_back(u8('\r'));
                            i += 2;
                            break;
                        case 't':
                            str_builder.push_back(u8('\t'));
                            i += 2;
                            break;
                        case '\'':
                            str_builder.push_back(u8('\''));
                            i += 2;
                            break;
                        case '\"':
                            str_builder.push_back(u8('\"'));
                            i += 2;
                            break;
                        case '0': {
                            if (!accept_zero) {
                                err_detail = "this can't have '\\0'";
                                goto error;
                            }
                            str_builder.push_back(u8('\0'));
                            i += 2;
                        } break;
                        case 'x': {
                            if (!(backslash_idx + 3 < contents.len)) {
                                err_detail = "unexpected end of file in string/char literal";
                                goto error;
                            }
                            u8 const ch_high = char_to_u8(contents.ptr[backslash_idx + 2]);
                            u8 const ch_low = char_to_u8(contents.ptr[backslash_idx + 3]);
                            auto digit_high_opt = char_to_hex(ch_high);
                            auto digit_low_opt = char_to_hex(ch_low);
                            if (digit_high_opt.is_none() || digit_low_opt.is_none()) {
                                err_detail = "invalid hex digit";
                                goto error;
                            }
                            u8 const byte = (u8)((u8)(digit_high_opt.unwrap() << 4) | digit_low_opt.unwrap());
                            if (byte == 0 && !accept_zero) {
                                err_detail = "this can't have '\\0'";
                                goto error;
                            }
                            str_builder.push_back(byte);
                            i += 4;
                        } break;
                        default: {
                            err_detail = "unexpected escape character";
                            goto error;
                        } break;
                    }
                } break;
                default: {
                    // Reject unescaped control sequences.
                    if (ch != '\t' && (ch >= 0 && ch < 32)) {
                        ArrayList_Of_u8 tmp_buf{scratch.allocator()};
                        array_list_printf(&tmp_buf, "invalid character '\\x%02x'", ch);
                        err_detail = tmp_buf.leak_to_span().transmute<char const>();
                        goto error;
                    }
                    str_builder.push_back(ch);
                    i += 1;
                } break;
            }
        }
    end_loop:
        yy_assert(char_to_u8(contents.try_get(contents_idx + i).unwrap()) == quote_char);
        if (is_identifier && str_builder.size() == 0) {
            err_detail = "this identifier can't be empty";
            goto error;
        }
        // `i + 1` to skip the closing '"'
        contents_idx += (u32)i + 1;
        *out_str = str_builder.leak_to_span().transmute<char const>();
        return NextTokenStatus::xcontinue;
    error:;
        yy_assert(!err_detail.empty());
        auto const error_offset = FileOffset{(u32)contents_idx + (u32)i};
        add_error_msg(LexSourceRegion(error_offset, error_offset), "bad string literal: " Sv_Fmt "", Sv_Arg(err_detail));
        return NextTokenStatus::error;
    }

    NODISCARD NextTokenStatus lex_string_identifier() {
        yy_assert(contents[contents_idx] == '@' && contents[contents_idx + 1] == '"');
        auto const begin_offset = FileOffset{(u32)contents_idx};
        auto _ = scratch.scoped_save();
        Sv name = yy::uninitialized;
        switch (lex_string_literal_helper(scratch.allocator(), LexStringLiteralMode::identifier, &name)) {
            case NextTokenStatus::xcontinue: {
            } break;
            case NextTokenStatus::error:
                return NextTokenStatus::error;
            case NextTokenStatus::eof:
                yy_unreachable();
        }
        if (sv_contains_char(name, '\0')) {
            add_error_msg(current_src(), "identifier contains '\\0'");
            return NextTokenStatus::error;
        }
        auto const end_offset = FileOffset{(u32)contents_idx};
        auto const src = LexSourceRegion{begin_offset, end_offset};
        auto const identifier_id = make_identifier(name);
        append_token(Token{.data = MetaTu_init(TokenData, identifier, {.id = identifier_id})}, src);
        return NextTokenStatus::xcontinue;
    }

    NODISCARD auto lex_string_literal() -> NextTokenStatus {
        yy_assert(contents[contents_idx] == '"');
        auto const begin_offset = FileOffset{(u32)contents_idx};
        Sv str = yy::uninitialized;
        switch (lex_string_literal_helper(arena.allocator(), LexStringLiteralMode::double_quote, &str)) {
            case NextTokenStatus::xcontinue: {
            } break;
            case NextTokenStatus::error:
                return NextTokenStatus::error;
            case NextTokenStatus::eof:
                yy_unreachable();
        }
        auto const end_offset = FileOffset{(u32)contents_idx};
        auto const src = LexSourceRegion{begin_offset, end_offset};
        auto const string_id = make_string_literal(str);
        append_token(Token{.data = MetaTu_init(TokenData, string_literal, {.id = string_id})}, src);
        return NextTokenStatus::xcontinue;
    }

    NODISCARD auto lex_char_literal() -> NextTokenStatus {
        yy_assert(contents[contents_idx] == '\'');
        auto const begin_offset = FileOffset{(u32)contents_idx};
        auto _ = scratch.scoped_save();
        Sv str = yy::uninitialized;
        switch (lex_string_literal_helper(scratch.allocator(), LexStringLiteralMode::single_quote, &str)) {
            case NextTokenStatus::xcontinue: {
            } break;
            case NextTokenStatus::error:
                return NextTokenStatus::error;
            case NextTokenStatus::eof:
                yy_unreachable();
        }
        if (str.len == 0) {
            add_error_msg(current_src(), "char literal is empty");
            return NextTokenStatus::error;
        }
        // TODO: decode one utf8 codepoint
        if (str.len > 1) {
            add_error_msg(current_src(), "char literal has too many bytes (TODO decode utf8)");
            return NextTokenStatus::error;
        }
        u32 const codepoint = char_to_u8(str[0]);
        auto const end_offset = FileOffset{(u32)contents_idx};
        auto const src = LexSourceRegion{begin_offset, end_offset};
        append_token(Token{.data = MetaTu_init(TokenData, char_literal, {.codepoint = codepoint})}, src);
        return NextTokenStatus::xcontinue;
    }

    /// If a token is found, return true and append the token to the list.
    NODISCARD auto simple_helper1_12(u8 ch1, TokenSimple s1, u8 ch2, TokenSimple s12) -> bool {
        auto const begin_offset = FileOffset{(u32)contents_idx};
        yy_assert(contents_idx + 0 < contents.len);
        if (contents.try_get(contents_idx + 0) == ch1) {
            TokenSimple simple = yy::uninitialized;
            if (contents.try_get(contents_idx + 1) == ch2) {
                simple = s12;
                contents_idx += 2;
            } else {
                simple = s1;
                contents_idx += 1;
            }
            auto const end_offset = FileOffset{(u32)contents_idx};
            auto const src = LexSourceRegion{begin_offset, end_offset};
            append_token(Token{.data = MetaTu_init(TokenData, simple, simple)}, src);
            return true;
        }
        return false;
    }

    /// If a token is found, return true and append the token to the list.
    NODISCARD auto simple_helper1_12_13_123(
        u8 ch1,
        TokenSimple s1,
        u8 ch2,
        TokenSimple s12,
        u8 ch3,
        TokenSimple s13,
        TokenSimple s123
    ) -> bool {
        auto const begin_offset = FileOffset{(u32)contents_idx};
        yy_assert(contents_idx + 0 < contents.len);
        if (contents.try_get(contents_idx + 0) == ch1) {
            TokenSimple simple = yy::uninitialized;
            if (contents.try_get(contents_idx + 1) == ch2) {
                if (contents.try_get(contents_idx + 2) == ch3) {
                    simple = s123;
                    contents_idx += 3;
                } else {
                    simple = s12;
                    contents_idx += 2;
                }
            } else if (contents.try_get(contents_idx + 1) == ch3) {
                simple = s13;
                contents_idx += 2;
            } else {
                simple = s1;
                contents_idx += 1;
            }
            auto const end_offset = FileOffset{(u32)contents_idx};
            auto const src = LexSourceRegion{begin_offset, end_offset};
            append_token(Token{.data = MetaTu_init(TokenData, simple, simple)}, src);
            return true;
        }
        return false;
    }

    NODISCARD auto next_token() -> NextTokenStatus {
        while (true) {
            // Check eof
            if (contents_idx >= contents.len) {
                append_token(Token{.data = MetaTu_init(TokenData, eof, {})}, current_src());
                return NextTokenStatus::eof;
            }

            // TODO: Check that column0 is not too big.
            // TODO: Check that line0 is not too big.

            if (try_skip_whitespace())
                continue;
            if (try_skip_comment())
                continue;
            if (try_skip_one_newline())
                continue;

            u8 const ch = char_to_u8(contents[contents_idx]);
            auto const begin_offset = FileOffset{(u32)contents_idx};

            // Check builtin fn
            if (ch == '@') {
                if (contents.try_get(contents_idx + 1) == '"')
                    return lex_string_identifier();
                usize const ident_start = contents_idx + 1;
                usize ident_idx = 1;
                while (contents_idx + ident_idx < contents.len &&
                       is_ident_middle(char_to_u8(contents.ptr[contents_idx + ident_idx]))) {
                    ident_idx += 1;
                }
                if (ident_idx == 1) {
                    add_error_msg(current_src(), "no identifier after '@'");
                    return NextTokenStatus::error;
                }
                contents_idx += (u32)ident_idx;
                Sv const name = contents.slice(ident_start, contents_idx);
                for (auto const bfn : yy::meta_enum_range<TokenBuiltinFn>()) {
                    Sv const sv = MetaEnum_sv_short(TokenBuiltinFn, bfn);
                    if (sv_eql(sv, name)) {
                        auto const end_offset = FileOffset{(u32)contents_idx};
                        auto const src = LexSourceRegion{begin_offset, end_offset};
                        append_token(Token{.data = MetaTu_init(TokenData, builtin_fn, bfn)}, src);
                        return NextTokenStatus::xcontinue;
                    }
                }
                add_error_msg(current_src(), "invalid builtin function ‘@" Sv_Fmt "’", Sv_Arg(name));
                return NextTokenStatus::error;
            }

            // Check integer literal
            if (ch >= '0' && ch <= '9')
                return lex_integer_literal();

            // String literal
            if (ch == '"')
                return lex_string_literal();

            // Char literal
            if (ch == '\'')
                return lex_char_literal();

            // Check identifier
            if (is_ident_begin(ch)) {
                usize ident_start = contents_idx;
                while (contents_idx < contents.len && is_ident_middle(char_to_u8(contents.ptr[contents_idx]))) {
                    contents_idx += 1;
                }
                Sv const ident_sv = contents.slice(ident_start, contents_idx);
                // Check keyword
                // TODO: Use a hash table or at least a table of Map(keyword_length, Map(String, Keyword))
                {
                    for (auto const kw : yy::meta_enum_range<TokenKeyword>()) {
                        Sv const sv = MetaEnum_sv_short(TokenKeyword, kw);
                        if (sv_eql(sv, ident_sv)) {
                            auto const end_offset = FileOffset{(u32)contents_idx};
                            auto const src = LexSourceRegion{begin_offset, end_offset};
                            append_token(Token{.data = MetaTu_init(TokenData, keyword, kw)}, src);
                            return NextTokenStatus::xcontinue;
                        }
                    }
                }
                if (sv_contains_char(ident_sv, '\0')) {
                    add_error_msg(current_src(), "identifier contains '\\0'");
                    return NextTokenStatus::error;
                }
                auto const identifier_id = make_identifier(ident_sv);
                auto const end_offset = FileOffset{(u32)contents_idx};
                auto const src = LexSourceRegion{begin_offset, end_offset};
                append_token(Token{.data = MetaTu_init(TokenData, identifier, {.id = identifier_id})}, src);
                return NextTokenStatus::xcontinue;
            }

            {
                static_assert(MetaEnum_count_int(TokenSimple) == 47, "change this code");
                TokenSimple simple;
                usize simple_len = 1;  // will be set to zero later if this isn't a `simple`
                if (false) {
                } else if (ch == '(')
                    simple = TokenSimple__lparen;
                else if (ch == ')')
                    simple = TokenSimple__rparen;
                else if (ch == '{')
                    simple = TokenSimple__lbrace;
                else if (ch == '}')
                    simple = TokenSimple__rbrace;
                else if (ch == '[')
                    simple = TokenSimple__lbracket;
                else if (ch == ']')
                    simple = TokenSimple__rbracket;
                else if (ch == ';')
                    simple = TokenSimple__semicolon;
                else if (ch == ',')
                    simple = TokenSimple__comma;
                else if (ch == '~')
                    simple = TokenSimple__bits_not;
                else {
                    if (simple_helper1_12(
                            '.', TokenSimple__dot, '*', TokenSimple__deref
                        ) ||
                        simple_helper1_12(
                            ':', TokenSimple__colon, ':', TokenSimple__colon_colon
                        ) ||
                        simple_helper1_12(
                            '/', TokenSimple__div, '=', TokenSimple__assign_div
                        ) ||
                        simple_helper1_12(
                            '%', TokenSimple__mod, '=', TokenSimple__assign_mod
                        ) ||
                        simple_helper1_12(
                            '&', TokenSimple__ampersand, '=',
                            TokenSimple__assign_bits_and
                        ) ||
                        simple_helper1_12(
                            '|', TokenSimple__bits_or, '=', TokenSimple__assign_bits_or
                        ) ||
                        simple_helper1_12(
                            '^', TokenSimple__bits_xor, '=',
                            TokenSimple__assign_bits_xor
                        ) ||
                        simple_helper1_12(
                            '!', TokenSimple__bool_not, '=', TokenSimple__noteq
                        ) ||
                        simple_helper1_12(
                            '=', TokenSimple__assign, '=', TokenSimple__eq
                        ) ||
                        simple_helper1_12_13_123(
                            '+', TokenSimple__plus, '%', TokenSimple__plus_wrap, '=',
                            TokenSimple__assign_plus, TokenSimple__assign_plus_wrap
                        ) ||
                        simple_helper1_12_13_123(
                            '-', TokenSimple__minus, '%', TokenSimple__minus_wrap, '=',
                            TokenSimple__assign_minus, TokenSimple__assign_minus_wrap
                        ) ||
                        simple_helper1_12_13_123(
                            '*', TokenSimple__mul, '%', TokenSimple__mul_wrap, '=',
                            TokenSimple__assign_mul, TokenSimple__assign_mul_wrap
                        ) ||
                        simple_helper1_12_13_123(
                            '<', TokenSimple__lt, '<', TokenSimple__sh_left, '=',
                            TokenSimple__lte, TokenSimple__assign_sh_left
                        ) ||
                        simple_helper1_12_13_123(
                            '>', TokenSimple__gt, '>', TokenSimple__sh_right, '=',
                            TokenSimple__gte, TokenSimple__assign_sh_right
                        )) {
                        return NextTokenStatus::xcontinue;
                    } else {
                        simple_len = 0;
                    }
                }
                if (simple_len > 0) {
                    contents_idx += simple_len;
                    auto const end_offset = FileOffset{(u32)contents_idx};
                    auto const src = LexSourceRegion{begin_offset, end_offset};
                    append_token( Token{ .data = MetaTu_init(TokenData, simple, simple) }, src);
                    return NextTokenStatus::xcontinue;
                }
            }

            // Invalid token
            {
                u8 const byte = ch;
                u8 const printable = (byte >= ' ' && byte <= '~') ? byte : ' ';
                add_error_msg(current_src(), "invalid token: '%c' (0x%02x)", printable, byte);
                return NextTokenStatus::error;
            }
        }
        yy_unreachable();
    }
};

NODISCARD auto Token::fancy_name() const -> Sv {
    switch (data.tag) {
        case MetaEnum_count(TokenData_Tag):
            yy_unreachable();
        case TokenData_Tag__simple:
            return yy::meta_enum_sv_short(data.as_simple().unwrap());
        case TokenData_Tag__keyword:
            return yy::meta_enum_sv_short(data.as_keyword().unwrap());
        case TokenData_Tag__builtin_fn:
            return yy::meta_enum_sv_short(data.as_builtin_fn().unwrap());
        case TokenData_Tag__eof:
            return sv_lit("end of file");
        case TokenData_Tag__identifier:
            return sv_lit("identifier");
        case TokenData_Tag__string_literal:
            return sv_lit("string literal");
        case TokenData_Tag__integer_literal:
            return sv_lit("integer literal");
        case TokenData_Tag__char_literal:
            return sv_lit("char literal");
    }
    yy_unreachable();
}

// TODO: On error, we should return the partially lexed file.
NODISCARD auto lex_file(GlobalCompilation* global, Sv filename, Sv contents, Allocator heap) -> LexFileResult {
    // Skip UTF8 BOM.
    constexpr Sv utf8_bom = "\xef\xbb\xbf";
    static_assert(utf8_bom.size() == 3);
    if (sv_starts_with(contents, utf8_bom))
        contents = contents.slice(utf8_bom.len);
    auto file_id = global->reserve_lexed_file();
    auto ctx = LexerCtx{
        .global = global,
        .heap = heap,
        .arena{heap, 4096},
        .scratch{heap, 2048},
        .file_id{file_id},
        .filename{filename},
        .contents{contents},
        .contents_idx = 0,
        .line0_start_offset{},
        .tokens{},
        .token_srcs{},
        .string_literals{},
        .errors{heap},
    };
    if (ctx.contents.len >= UINT32_MAX) {
        ctx.add_error_msg(None, "file is too big");
        return std::move(ctx.errors);
    }

    ctx.make_line();

    bool reached_eof = false;
    while (!reached_eof) {
        switch (ctx.next_token()) {
            case LexerCtx::NextTokenStatus::xcontinue:
                continue;
            case LexerCtx::NextTokenStatus::eof: {
                reached_eof = true;
            } break;
            case LexerCtx::NextTokenStatus::error: {
                yy_assert(ctx.errors.msgs.len() > 0);
                return std::move(ctx.errors);
            } break;
        }
    }
    yy_assert(ctx.errors.msgs.len() == 0);
    return global->unreserve_lexed_file(file_id, std::move(ctx));
}

LexedFile::LexedFile(LexerCtx&& ctx)
    : global(ctx.global),
      arena(std::move(ctx.arena)),
      id(ctx.file_id),
      filename(ctx.filename),
      contents(ctx.contents),
      m_tokens(ctx.tokens.leak_to_span()),
      m_token_srcs(ctx.token_srcs.leak_to_span()),
      m_string_literals(ctx.string_literals.leak_to_span()),
      line_info{ .line0_start_offset = ctx.line0_start_offset.leak_to_span()} {
    yy_assert(m_tokens.size() > 0 && m_tokens.size() == m_token_srcs.size());
    yy_assert(ctx.errors.msgs.size() == 0);
}


auto LexedFileIterator::advance() -> void {
    if (lex.token(next_id).data.tag == TokenData_Tag__eof)
        return;
    next_id = TokenId{next_id.x + 1};
}

NODISCARD auto LexedFileIterator::peek_nth(u32 nth) const -> TokenId {
    auto result = TokenId{std::min((u32)lex.m_tokens.size() - 1, next_id.x + nth)};
    return result;
}

NODISCARD auto LexedFileIterator::next() -> TokenId {
    auto result = peek();
    advance();
    return result;
}



auto debug_token_print(LexedFile const& lex, TokenId const token_id) -> void {
    auto const& token = lex.token(token_id);
    auto const& token_src = lex.token_src(token_id);
    auto loc = token_src.begin.to_line_and_column(lex.line_info);
    fprintf(stderr, "%u:%u: \t", loc.line1, loc.column1);
    switch (token.data.tag) {
        case MetaEnum_count(TokenData_Tag):
            yy_unreachable();
        case TokenData_Tag__eof: {
            fprintf(stderr, "(end of file)\n");
        } break;
        case TokenData_Tag__simple: {
            Sv const simple_sv = yy::meta_enum_sv_short(*MetaTu_get(TokenData, &token.data, simple));
            yy_assert(simple_sv.len > 0);
            fprintf(stderr, "simple  ‘" Sv_Fmt "’\n", Sv_Arg(simple_sv));
        } break;
        case TokenData_Tag__keyword: {
            Sv const keyword_sv = yy::meta_enum_sv_short(*MetaTu_get(TokenData, &token.data, keyword));
            yy_assert(keyword_sv.len > 0);
            fprintf(stderr, "keyword  ‘" Sv_Fmt "’\n", Sv_Arg(keyword_sv));
        } break;
        case TokenData_Tag__builtin_fn: {
            Sv const sv = yy::meta_enum_sv_short(*MetaTu_get(TokenData, &token.data, builtin_fn));
            yy_assert(sv.len > 0);
            fprintf(stderr, "builtin_fn  ‘" Sv_Fmt "’\n", Sv_Arg(sv));
        } break;
        case TokenData_Tag__identifier: {
            auto const ident_id = MetaTu_get(TokenData, &token.data, identifier)->id;
            auto const ident_sv = lex.global->identifier_sv(ident_id);
            fprintf(stderr, "identifier id %u, '" Sv_Fmt "'\n", ident_id.x, Sv_Arg(ident_sv));
        } break;
        case TokenData_Tag__string_literal: {
            auto const string_literal_id = MetaTu_get(TokenData, &token.data, string_literal)->id;
            auto const string_sv = lex.string_literal(string_literal_id);
            fprintf(stderr, "string literal  ‘" Sv_Fmt "’\n", Sv_Arg(string_sv));
        } break;
        case TokenData_Tag__integer_literal: {
            i64 as_i64 = MetaTu_get(TokenData, &token.data, integer_literal)->number;
            fprintf(stderr, "integer literal  ‘%" PRIi64 "’\n", as_i64);
        } break;
        case TokenData_Tag__char_literal: {
            Sv original_text = lex.contents.slice(token_src.begin.x, token_src.end.x);
            fprintf(stderr, "char literal  ‘" Sv_Fmt "’\n", Sv_Arg(original_text));
        } break;
    }
}

}  // namespace yuceyuf::lexer
