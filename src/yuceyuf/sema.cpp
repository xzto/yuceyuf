#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/array_list_printf.hpp"
#include "yy/hash.hpp"
#include "yy/misc.hpp"
#include "yuceyuf/ir.hpp"
#include "yuceyuf/genir.hpp"
#include "yuceyuf/sema.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <memory>

using yy::meta_enum_count;
using yy::meta_enum_count_int;
using yy::meta_enum_sv_short;

using InstIndex = Yy_Ir_InstIndex;

static u32 intCastFromUsizeToU32(usize x_usize)
{
    u32 result;
    yy_unwrap_ok(yy_try_int_cast(x_usize, &result));
    return result;
}


#define ranges_intersect_ptr_len(a_ptr, a_len, b_ptr, b_len)         \
    ranges_intersect((uintptr_t)(a_ptr), (a_len)*sizeof((a_ptr)[0]), \
                     (uintptr_t)(b_ptr), (b_len)*sizeof((b_ptr)[0]))

// Low is inclusive. High is exclusive.
NODISCARD static inline bool ranges_intersect(usize const a_start, usize const a_len, usize const b_start, usize const b_len)
{
    usize const left_start  = (a_start < b_start) ? a_start : b_start;
    usize const left_len    = (a_start < b_start) ? a_len   : b_len;
    usize const right_start = (a_start < b_start) ? b_start : a_start;
    usize const right_len   = (a_start < b_start) ? b_len   : a_len;

    if (left_len == 0 || right_len == 0)
        return false;

    return left_start + left_len > right_start;
}


/// Log a single line
PRINTF_LIKE(1, 2)
static void logDebug(char const *fmt, ...)
{
    if (verbose_sema) {
        va_list ap;
        va_start(ap, fmt);
        fprintf(stderr, "sema(debug): ");
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "\n");
        va_end(ap);
    }
}

/// Log a single line
PRINTF_LIKE(1, 2)
static void logTrace(char const *fmt, ...)
{
    if ((false)) {
        va_list ap;
        va_start(ap, fmt);
        fprintf(stderr, "sema(trace): ");
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "\n");
        va_end(ap);
    }
}

/// Log a single line
PRINTF_LIKE(1, 2)
static void logError(char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "sema(error): ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    va_end(ap);
}


COLD
static void sema_add_err_msg__(
    Yuceyuf_ErrorMessageKind const kind,
    Sema *const sema, Genir_ErrorLocation const loc,
    char const *const fmt, va_list ap)
{
    Yuceyuf_AstNode* ast = yy::uninitialized;
    switch (loc.tag) {
        case Genir_ErrorLocation_Tag__ast: {
            ast = *MetaTu_get(Genir_ErrorLocation, &loc, ast);
        } break;
        case Genir_ErrorLocation_Tag__inst: {
            ast = sema->gi->inst_to_ast_map[*MetaTu_get(Genir_ErrorLocation, &loc, inst)];
        } break;
        default:
            yy_unreachable();
    }
    auto const& lex = ast->parent_ast_file->lexer.lex;
    auto const src_region = ast->src.to_src_region_line_and_column(lex);
    sema->errors.make_error_va(kind, ast->parent_ast_file->filename, src_region, fmt, ap);
}

NODISCARD static Genir_ErrorLocation errorLoc(Yuceyuf_AstNode* const node)
{
    return MetaTu_init(Genir_ErrorLocation, ast, node);
}
NODISCARD static Genir_ErrorLocation errorLoc(Yy_Ir_InstIndex const inst_idx)
{
    return MetaTu_init(Genir_ErrorLocation, inst, inst_idx);
}

COLD PRINTF_LIKE(3, 4)
static void sema_add_hard_error_msg(Sema *const sema, Genir_ErrorLocation const loc, char const *const fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    sema_add_err_msg__(Yuceyuf_ErrorMessageKind::hard_error, sema, loc, fmt, ap);
    va_end(ap);
}
COLD PRINTF_LIKE(3, 4)
static void sema_add_soft_error_msg(Sema *const sema, Genir_ErrorLocation const loc, char const *const fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    sema_add_err_msg__(Yuceyuf_ErrorMessageKind::soft_error, sema, loc, fmt, ap);
    va_end(ap);
}

COLD PRINTF_LIKE(3, 4)
static void sema_add_error_note(Sema *const sema, Genir_ErrorLocation const loc, char const *const fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    sema_add_err_msg__(Yuceyuf_ErrorMessageKind::note, sema, loc, fmt, ap);
    va_end(ap);
}

NODISCARD COLD static YyStatus sema_new_soft_error(Sema *const sema)
{
    if (sema->errors.msgs.size() >= ERROR_LIMIT) {
        sema->errors.hard_count += 1;
        return Yy_Bad;
    }
    return Yy_Ok;
}


NODISCARD static bool semaTypeIdIsValid(SemaTypeId const type)
{
    return type.sei.x != 0;
}

NODISCARD static inline SemaExtraIndex semaExtraIndexOffset(SemaExtraIndex const extra, u32 const offset)
{
    return SemaExtraIndex {
        .x = extra.x + offset
    };
}


NODISCARD static inline SemaExtraIndex semaAllocExtra(Sema *const sema, u32 const size)
{
    sema->extra_pointers_lock.check_unlocked();
    u32 new_one_past_last;
    if (__builtin_add_overflow(sema->extra.size(), size, &new_one_past_last)) {
        yy_panic("semaAllocExtra overflow");
    }
    u32 result;
    yy_unwrap_ok(yy_try_int_cast(sema->extra.size(), &result));
    sema->extra.append_n_default(sema->heap, size);
    yy_assert(sema->extra.size() == new_one_past_last);
    yy_debug_invalidate_ptr_len(&sema->extra[result], size);
    return SemaExtraIndex {
        .x = result
    };
}

NODISCARD static SemaTypeId basicInternedType(Sema *const sema, SemaType_Tag const type_tag);

static auto init_comptime_value_pool(Sema* sema) -> void {
    yy_assert(sema->comptime_value_pool.size() == 0);
    usize const count = static_cast<usize>(BasicInternedComptimeValue::COUNT_);
    sema->comptime_value_pool.items.ensure_unused_capacity(count);
    for (usize i = 0; i < count; i++) {
        auto make = [&](ComptimeValue&& v){
            auto interned = sema->comptime_value_pool.push_back(std::move(v));
            yy_assert(interned.x == i);
        };
        auto const e = static_cast<BasicInternedComptimeValue>(i);
        switch (e) {
            using enum BasicInternedComptimeValue;
            case COUNT_: yy_unreachable();
            case const_integer_0: make(MetaTu_init(ComptimeValue, integer, { .value = 0 , .int_type_new = None })); break;
            case const_integer_1: make(MetaTu_init(ComptimeValue, integer, { .value = 1, .int_type_new = None })); break;
            case const_integer_neg1: make(MetaTu_init(ComptimeValue, integer, { .value = -1, .int_type_new = None })); break;
            case const_xtrue: make(MetaTu_init(ComptimeValue, xbool, true)); break;
            case const_xfalse: make(MetaTu_init(ComptimeValue, xbool, false)); break;
            case const_xundefined: make(MetaTu_init(ComptimeValue, xundefined, {})); break;
            case const_xnoreturn: make(MetaTu_init(ComptimeValue, xnoreturn, {})); break;
            case const_xvoid: make(MetaTu_init(ComptimeValue, xvoid, {})); break;
            case const_type_type: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__type))); break;
            case const_type_uintword: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__uintword))); break;
            case const_type_xu8: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__xu8))); break;
            case const_type_xundefined: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__xundefined))); break;
            case const_type_xvoid: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__xvoid))); break;
            case const_type_xbool: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__xbool))); break;
            case const_type_xnoreturn: make(MetaTu_init(ComptimeValue, type, basicInternedType(sema, SemaType_Tag__xnoreturn))); break;
            case const_type_ptr_to_uintword: make(MetaTu_init(ComptimeValue, type, typePointerTo(sema, basicInternedType(sema, SemaType_Tag__uintword)))); break;
            case const_type_ptr_to_xu8: make(MetaTu_init(ComptimeValue, type, typePointerTo(sema, basicInternedType(sema, SemaType_Tag__xu8)))); break;
            case const_type_ptr_to_xvoid: make(MetaTu_init(ComptimeValue, type, typePointerTo(sema, basicInternedType(sema, SemaType_Tag__xvoid)))); break;
            case const_type_ptr_to_xbool: make(MetaTu_init(ComptimeValue, type, typePointerTo(sema, basicInternedType(sema, SemaType_Tag__xbool)))); break;
        }
    }
}

static auto intern_comptime_value(Sema* sema, ComptimeValue const& comptime_value) -> InternedComptimeValueId {
    Option<BasicInternedComptimeValue> basic = None;
    switch (comptime_value.tag) {
        case meta_enum_count<ComptimeValue_Tag>: yy_unreachable();
        case ComptimeValue_Tag__string: {} break;
        case ComptimeValue_Tag__integer: {
            auto const& cv = comptime_value.as<ComptimeValue_Tag__integer>().unwrap();
            if (cv.int_type_new.is_none()) {
                if (cv.value == 0) {
                    basic = BasicInternedComptimeValue::const_integer_0;
                } else if (cv.value == 1) {
                    basic = BasicInternedComptimeValue::const_integer_1;
                } else if (cv.value == -1) {
                    basic = BasicInternedComptimeValue::const_integer_neg1;
                }
            }
        } break;
        case ComptimeValue_Tag__xbool: {
            bool const x = comptime_value.as<ComptimeValue_Tag__xbool>().unwrap();
            basic = x ? BasicInternedComptimeValue::const_xtrue : BasicInternedComptimeValue::const_xfalse;
        } break;
        case ComptimeValue_Tag__xundefined: basic = BasicInternedComptimeValue::const_xundefined; break;
        case ComptimeValue_Tag__xvoid: basic = BasicInternedComptimeValue::const_xvoid; break;
        case ComptimeValue_Tag__xnoreturn: basic = BasicInternedComptimeValue::const_xnoreturn; break;
        case ComptimeValue_Tag__type: {
            auto const type_id = comptime_value.as<ComptimeValue_Tag__type>().unwrap();
            switch (decodeTypeTag(sema, type_id)) {
                case meta_enum_count<SemaType_Tag>: yy_unreachable();
                case SemaType_Tag__function:
                case SemaType_Tag__xstruct: {} break;
                case SemaType_Tag__type: basic = BasicInternedComptimeValue::const_type_type; break;
                case SemaType_Tag__uintword: basic = BasicInternedComptimeValue::const_type_uintword; break;
                case SemaType_Tag__xu8: basic = BasicInternedComptimeValue::const_type_xu8; break;
                case SemaType_Tag__xundefined: basic = BasicInternedComptimeValue::const_type_xundefined; break;
                case SemaType_Tag__xvoid: basic = BasicInternedComptimeValue::const_type_xvoid; break;
                case SemaType_Tag__xbool: basic = BasicInternedComptimeValue::const_type_xbool; break;
                case SemaType_Tag__xnoreturn: basic = BasicInternedComptimeValue::const_type_xnoreturn; break;
                case SemaType_Tag__pointer: {
                    switch (decodeTypeTag(sema, typeChild(sema, type_id))) {
                        case SemaType_Tag__uintword: basic = BasicInternedComptimeValue::const_type_ptr_to_uintword; break;
                        case SemaType_Tag__xu8: basic = BasicInternedComptimeValue::const_type_ptr_to_xu8; break;
                        case SemaType_Tag__xvoid: basic = BasicInternedComptimeValue::const_type_ptr_to_xvoid; break;
                        case SemaType_Tag__xbool: basic = BasicInternedComptimeValue::const_type_ptr_to_xbool; break;
                        default: {} break;
                    }
                } break;
            }
        } break;
    }
    if (basic.has_value())
        return basic.unwrap();
    auto interned = sema->comptime_value_pool.push_back(comptime_value);
    return interned;
}

static auto make_inst_info_local_typed_full(
    Sema* sema,
    InstIndex inst_idx,
    SemaTypeId type,
    Option<InternedComptimeValueId> resolved_comptime_value,
    Option<bool> must_comptime_only = None
) -> void {
    auto const sii = &sema->sema_inst_infos[inst_idx];
    yy_assert(sii->is_nothing());
    auto const is_comptime_only = typeIsComptimeOnly(sema, type) || must_comptime_only.unwrap_or(false);

    *sii = SemaInstInfoNew{
        .status = SemaInstInfoNew::Status::full,
        .build_use_count = 0,
        .comptime_only = is_comptime_only,
        .type3 = type,
        .build_last_ref = None,
        .specific = MetaTu_init(SiiSpecific, none, {}),
        .resolved_comptime_value = resolved_comptime_value,
    };
}
static auto make_inst_info_local_typed_full_comptime(
    Sema* sema,
    InstIndex inst_idx,
    SemaTypeId type,
    InternedComptimeValueId resolved_comptime_value
) -> void {
    return make_inst_info_local_typed_full(sema, inst_idx, type, resolved_comptime_value, true);
}

static auto make_inst_info_local_decl_full(
    Sema* sema,
    InstIndex inst_idx,
    SemaTypeId decl_type,
    Option<bool> must_comptime_only = None
) -> void {
    auto const sii = &sema->sema_inst_infos[inst_idx];
    yy_assert(sii->is_nothing());
    auto const is_comptime_only = typeIsComptimeOnly(sema, decl_type) || must_comptime_only.unwrap_or(false);
    *sii = SemaInstInfoNew{
        .status = SemaInstInfoNew::Status::full,
        .build_use_count = 0,
        .comptime_only = is_comptime_only,
        .type3 = basicInternedType(sema, SemaType_Tag__xvoid),
        .build_last_ref = None,
        .specific = MetaTu_init(SiiSpecific, decl_like, { .resolved_decl_type = decl_type }),
        .resolved_comptime_value = BasicInternedComptimeValue::const_xvoid,
    };
}
static auto make_inst_info_global_decl_full(
    Sema* sema,
    InstIndex inst_idx,
    SemaTypeId decl_type,
    Option<bool> must_comptime_only = None
) -> void {
    auto const sii = &sema->sema_inst_infos[inst_idx];
    yy_assert(sii->is_nothing());
    auto const is_comptime_only = typeIsComptimeOnly(sema, decl_type) || must_comptime_only.unwrap_or(false);
    *sii = SemaInstInfoNew{
        .status = SemaInstInfoNew::Status::full,
        .build_use_count = 0,
        .comptime_only = is_comptime_only,
        .type3 = basicInternedType(sema, SemaType_Tag__xvoid),
        .build_last_ref = None,
        .specific = MetaTu_init(SiiSpecific, decl_like, { .resolved_decl_type = decl_type }),
        .resolved_comptime_value = BasicInternedComptimeValue::const_xvoid,
    };
}
static auto make_inst_info_global_var_decl_full(
    Sema* sema,
    InstIndex inst_idx,
    SemaTypeId decl_type,
    InternedComptimeValueId init_value,
    Option<bool> must_comptime_only = None
) -> void {
    auto const sii = &sema->sema_inst_infos[inst_idx];
    yy_assert(sii->is_nothing());
    auto const is_comptime_only = typeIsComptimeOnly(sema, decl_type) || must_comptime_only.unwrap_or(false);
    *sii = SemaInstInfoNew{
        .status = SemaInstInfoNew::Status::full,
        .build_use_count = 0,
        .comptime_only = is_comptime_only,
        .type3 = basicInternedType(sema, SemaType_Tag__xvoid),
        .build_last_ref = None,
        .specific = MetaTu_init(SiiSpecific, decl_like, { .resolved_decl_type = decl_type, .comptime_value_for_global_var_decl_init = init_value }),
        .resolved_comptime_value = BasicInternedComptimeValue::const_xvoid,
    };
}


static auto rewriteInstTag(Sema* sema, InstIndex inst_idx, Yy_Ir_Inst_Tag new_tag) -> void {
    auto gi = sema->gi;
    genir_instsAt(gi, inst_idx)->tag = new_tag;
    auto& sii = sema->sema_inst_infos[inst_idx];
    yy_assert(sii.build_use_count == 0 && sii.build_last_ref.is_none());
    sii = SemaInstInfoNew{};
}
static auto rewriteInstTagExtraIndex(
    Sema* sema, InstIndex inst_idx, Yy_Ir_Inst_Tag new_tag, Yy_Ir_ExtraIndex new_extra
) -> void {
    auto gi = sema->gi;
    auto inst_ptr = genir_instsAt(gi, inst_idx);
    inst_ptr->tag = new_tag;
    inst_ptr->small_data.only_extra_pl_index.extra_pl_index = new_extra;
    auto& sii = sema->sema_inst_infos[inst_idx];
    yy_assert(sii.build_use_count == 0 && sii.build_last_ref.is_none());
    sii = SemaInstInfoNew{};
}
static auto rewriteInstTagSmallPl(
    Sema* sema, InstIndex inst_idx, Yy_Ir_Inst_Tag new_tag, Yy_Ir_Inst_SmallPl pl
) -> void {
    auto gi = sema->gi;
    auto inst_ptr = genir_instsAt(gi, inst_idx);
    inst_ptr->tag = new_tag;
    inst_ptr->small_data = pl;
    auto& sii = sema->sema_inst_infos[inst_idx];
    yy_assert(sii.build_use_count == 0 && sii.build_last_ref.is_none());
    sii = SemaInstInfoNew{};
}

// Decrease untyped_use_count for refs.
static auto rewriteInstRemoveUntypedInstRef(Sema* sema, InstIndex used_from, std::initializer_list<InstIndex> refs) -> void {
    (void) used_from;
    auto const gi = sema->gi;
    for (auto it : refs) {
        gi->insts[it].untyped_use_count = yy::checked_sub(gi->insts[it].untyped_use_count, u8{1}).unwrap();
    }
}
// Increase untyped_use_count for refs.
static auto rewriteInstAddUntypedInstRef(Sema* sema, InstIndex used_from, std::initializer_list<InstIndex> refs) -> void {
    (void) used_from;
    auto const gi = sema->gi;
    for (auto it : refs) {
        gi->insts[it].untyped_use_count += 1;
    }
}

// NOTE: this has to be kept in sync with `encodeTypeMaybeIntern`
NODISCARD SemaType_Tag decodeTypeTag(Sema const *const sema, SemaTypeId const type)
{
    yy_assert(semaTypeIdIsValid(type));
    SemaType_Tag const type_tag = (SemaType_Tag)sema->extra[type.sei.x];
    return type_tag;
}

/// typeBase will return you a pointer inside sema->extra. Be careful because some operations like semaAllocExtra may
/// invalidate the pointer.
NODISCARD SemaTypePacked::Base const* typeBase(Sema* const sema, SemaTypeId type) {
    yy_assert(semaTypeIdIsValid(type));
    return reinterpret_cast<SemaTypePacked::Base const*>(&sema->extra[type.sei.x]);
}
NODISCARD static SemaTypePacked::Base* typeBaseMut(Sema* const sema, SemaTypeId type) {
    yy_assert(semaTypeIdIsValid(type));
    return reinterpret_cast<SemaTypePacked::Base*>(&sema->extra[type.sei.x]);
}

// NOTE: This has to be kept in sync with `decodeType`
NODISCARD static SemaTypeId encodeTypeMaybeIntern(Sema *const sema, SemaTypePacked::Base const* type)
{
    static_assert(sizeof(SemaTypeId) == sizeof(u32), "");
    {
        // Check that `type` didn't come from the extra array. If it did, when we call semaAllocExtra it may invalidate `type`'s memory.
        auto ptr = reinterpret_cast<uintptr_t>(type);
        auto extra_begin = reinterpret_cast<uintptr_t>(sema->extra.begin());
        auto extra_end = reinterpret_cast<uintptr_t>(sema->extra.end());
        yy_assert(!(ptr >= extra_begin && ptr < extra_end));
    }
    switch (type->tag) {
        case yy::meta_enum_count_switch_t{}:
            yy_unreachable();
        // No payload
        case SemaType_Tag__type:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xundefined:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__xnoreturn:
            return basicInternedType(sema, type->tag);
        case SemaType_Tag__xstruct: {
            auto gop = sema->types.get_or_put_entry(sema, type);
            if (gop.exists())
                return gop.unwrap_get().key;
            auto&& put = std::move(gop).unwrap_put();

            auto pl = type->get<SemaTypePacked::Struct>();
            auto extra_count = pl->total_extra_count();
            SemaTypeId const sti = { .sei = semaAllocExtra(sema, extra_count) };
            memcpy(&sema->extra[sti.sei.x], type, extra_count * sizeof(u32));

            put.put_key_value(sti, Void{});
            return sti;
        } break;
        case SemaType_Tag__function: {
            auto gop = sema->types.get_or_put_entry(sema, type);
            if (gop.exists())
                return gop.unwrap_get().key;
            auto&& put = std::move(gop).unwrap_put();

            auto pl = type->get<SemaTypePacked::Function>();
            auto extra_count = pl->total_extra_count();
            SemaTypeId const sti = { .sei = semaAllocExtra(sema, extra_count) };
            memcpy(&sema->extra[sti.sei.x], type, extra_count * sizeof(u32));

            put.put_key_value(sti, Void{});
            return sti;
        } break;
        case SemaType_Tag__pointer: {
            auto gop = sema->types.get_or_put_entry(sema, type);
            if (gop.exists())
                return gop.unwrap_get().key;
            auto&& put = std::move(gop).unwrap_put();

            auto pl = type->get<SemaTypePacked::Pointer>();
            auto extra_count = pl->total_extra_count();
            SemaTypeId const sti = { .sei = semaAllocExtra(sema, extra_count) };
            memcpy(&sema->extra[sti.sei.x], type, extra_count * sizeof(u32));

            put.put_key_value(sti, Void{});
            return sti;
        }
        break;
    }
    yy_unreachable();
}

// NOTE: This function must be kept in sync with `semaInitInternedTypes`
NODISCARD static SemaTypeId basicInternedType(Sema *const sema, SemaType_Tag const type_tag)
{
    switch (type_tag) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag__xstruct:
        case SemaType_Tag__function:
        case SemaType_Tag__pointer:
            yy_unreachable();
        case SemaType_Tag__type:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xundefined:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__xnoreturn:
            return SemaTypeId {
                .sei = semaExtraIndexOffset(sema->basic_interned_types, (u32)type_tag)
            };
    }
    yy_unreachable();
}

NODISCARD bool typeIdEql(Sema *const sema, SemaTypeId const a, SemaTypeId const b)
{
    (void) sema;
    return a.sei.x == b.sei.x;
}

NODISCARD bool typeIsType(Sema *const sema, SemaTypeId const type)
{
    return decodeTypeTag(sema, type) == SemaType_Tag__type;
}
NODISCARD bool typeIsUndefined(Sema *const sema, SemaTypeId const type)
{
    return decodeTypeTag(sema, type) == SemaType_Tag__xundefined;
}
NODISCARD bool typeIsNoreturn(Sema *const sema, SemaTypeId const type)
{
    return decodeTypeTag(sema, type) == SemaType_Tag__xnoreturn;
}
NODISCARD bool typeIsVoid(Sema *const sema, SemaTypeId const type)
{
    return decodeTypeTag(sema, type) == SemaType_Tag__xvoid;
}
NODISCARD bool typeIsPointer(Sema *const sema, SemaTypeId const type)
{
    return decodeTypeTag(sema, type) == SemaType_Tag__pointer;
}

NODISCARD bool typeIsZeroSized(Sema *const sema, SemaTypeId const type)
{
    return typeIdMemorySizeOf(sema, type) == 0;
}

NODISCARD bool typeIsInteger(Sema *const sema, SemaTypeId const type)
{
    switch (decodeTypeTag(sema, type)) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
            return true;
        default:
            return false;
    }
}

NODISCARD bool typeIsComptimeOnly(Sema *const sema, SemaTypeId const type)
{
    return typeIsType(sema, type);
}

NODISCARD SemaTypeId typePointerTo(Sema *const sema, SemaTypeId const child_type)
{
    SemaTypePacked::Pointer const type{child_type};
    SemaTypeId const sti = encodeTypeMaybeIntern(sema, type);
    return sti;
}

NODISCARD SemaTypeId typePointerToAnyopaque(Sema *const sema)
{
    return typePointerTo(sema, basicInternedType(sema, SemaType_Tag__xu8));
}

NODISCARD SemaTypeId typeChild(Sema *const sema, SemaTypeId const type)
{
    auto base = typeBase(sema, type);
    switch (base->tag) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag::type:
        case SemaType_Tag::xundefined:
        case SemaType_Tag::xvoid:
        case SemaType_Tag::xbool:
        case SemaType_Tag::uintword:
        case SemaType_Tag::xu8:
        case SemaType_Tag::function:
        case SemaType_Tag::xstruct:
        case SemaType_Tag::xnoreturn:
            yy_panic_fmt("No child dec_type dec_type tag `" Sv_Fmt "`", Sv_Arg(meta_enum_sv_short(base->tag)));
        case SemaType_Tag::pointer:
            return base->get<SemaTypePacked::Pointer>()->child_type;
    }
    yy_unreachable();
}

NODISCARD static auto makeStructMembers(Sema* const sema, SemaTypeId const type_id, Option<TypeMemoryStuff*> result)
    -> YyStatus;

NODISCARD TypeMemoryStuff typeIdMemoryStuff(Sema *const sema, SemaTypeId const type_id)
{
    SemaType_Tag const type = decodeTypeTag(sema, type_id);
    switch (type) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag__type:
            yy_panic("error: internal compiler error: this is a bug in the yuceyuf compiler");
        case SemaType_Tag__xundefined:
            yy_panic("internal compiler error: can't take size of 'undefined' type");
        case SemaType_Tag__xvoid:
            return { .size_of = 0, .align_of = 1 };
        case SemaType_Tag__xnoreturn:
            yy_panic("internal compiler error: can't take size of noreturn type");
        case SemaType_Tag__uintword:
            return sema->target_info.uintword_size_align;
        case SemaType_Tag__xu8:
            return { .size_of = 1, .align_of = 1 };
        case SemaType_Tag__xbool:
            return { .size_of = 1, .align_of = 1 };
        case SemaType_Tag__function:
        case SemaType_Tag__pointer:
            return sema->target_info.ptr_size_align;
        case SemaType_Tag__xstruct: {
            TypeMemoryStuff result = yy::uninitialized;
            if (yy_is_err(makeStructMembers(sema, type_id, &result))) {
                todo();
            }
            return result;
        } break;
    }
    yy_unreachable();
}
NODISCARD u64 typeIdMemorySizeOf(Sema *const sema, SemaTypeId const type_id)
{
    return typeIdMemoryStuff(sema, type_id).size_of;
}


auto fmtComptimeValue(ArrayList_Of_u8 *const writer, Sema *const sema, ComptimeValue const& value) -> void
{
    switch (value.tag) {
        case meta_enum_count<ComptimeValue_Tag>:
            yy_unreachable();
        case ComptimeValue_Tag__xundefined: {
            auto const sv = meta_enum_sv_short(value.tag);
            array_list_printf(writer, "" Sv_Fmt "", Sv_Arg(sv));
        } break;
        case ComptimeValue_Tag__xnoreturn:
        case ComptimeValue_Tag__xvoid: {
            auto const sv = meta_enum_sv_short(value.tag);
            array_list_printf(writer, "" Sv_Fmt ".{}", Sv_Arg(sv));
        } break;
        case ComptimeValue_Tag__type: {
            fmtSemaTypeId(writer, sema, value.as<ComptimeValue_Tag__type>().unwrap());
        } break;
        case ComptimeValue_Tag__xbool: {
            auto const boolean = value.as<ComptimeValue_Tag__xbool>().unwrap();
            array_list_printf(writer, "%s", boolean ? "true" : "false");
        } break;
        case ComptimeValue_Tag__integer: {
            auto integer = value.as<ComptimeValue_Tag__integer>().unwrap();
            if (integer.int_type_new.has_value()) {
                array_list_printf(writer, "@as(");
                fmtSemaTypeId(writer, sema, integer.int_type_new.unwrap());
                array_list_printf(writer, ", %" PRIi64 ")", integer.value);
            } else {
                array_list_printf(writer, "%" PRIi64 "", integer.value);
            }
        } break;
        case ComptimeValue_Tag__string: {
            auto string = value.as<ComptimeValue_Tag__string>().unwrap();
            auto sv = genir_strTableGetTempSvFromSized0(sema->gi, string.string);
            yy_assert(sv.len > 0 && sv[sv.len - 1] == 0);
            sv.len -= 1;
            // TODO: Proper quoting
            array_list_u8_write_escaped_fancy_sv(writer, sv, usize{40});
        } break;
    }
}

auto fmtSemaTypeId(ArrayList_Of_u8 *const writer, Sema *const sema, SemaTypeId const type_id) -> void
{
    auto type = typeBase(sema, type_id);
    auto _ = sema->extra_pointers_lock.scoped_lock();
    switch (type->tag) {
        case meta_enum_count<SemaType_Tag>:
        case SemaType_Tag__type:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xbool:
        case SemaType_Tag__xnoreturn:
            array_list_printf(writer, Sv_Fmt, Sv_Arg(meta_enum_sv_short(type->tag)));
            break;
        case SemaType_Tag__xundefined:
            array_list_printf(writer, "@TypeOf(undefined)");
            break;
        case SemaType_Tag__function: {
            auto const pl = type->get<SemaTypePacked::Function>();
            array_list_printf(writer, "fn (");
            for (u32 i = 0; i < pl->arg_count; i += 1) {
                SemaTypeId const arg = pl->arg_type(i);
                fmtSemaTypeId(writer, sema, arg);
                if (i + 1 < pl->arg_count)
                    array_list_printf(writer, ", ");
            }
            array_list_printf(writer, ") ");
            fmtSemaTypeId(writer, sema, pl->return_type);
        }
        break;
        case SemaType_Tag__pointer: {
            auto const pl = type->get<SemaTypePacked::Pointer>();
            array_list_printf(writer, "*mut ");
            fmtSemaTypeId(writer, sema, pl->child_type);
        }
        break;
        case SemaType_Tag__xstruct: {
            auto const pl = type->get<SemaTypePacked::Struct>();
            auto const decl_id = pl->origin_decl_id.decode();
            auto const decl_ptr = sema->gi->decl_ptrs[decl_id];
            auto const decl_name_sv = decl_ptr->opt_name.map([&](auto&& x){
                return genir_identifier_sv(sema->gi, x);
            }).unwrap_or("<unnamed>"_sv);
            array_list_printf(writer, "struct " Sv_Fmt, Sv_Arg(decl_name_sv));
        } break;
    }
}

char const* debugTypeString(Sema* const sema, SemaTypeId const type_id);
char const* debugTypeString(Sema* const sema, SemaTypeId const type_id)
{
    ArrayList_Of_u8 buffer{c_allocator()};
    fmtSemaTypeId(&buffer, sema, type_id);
    // Leak buf.
    return array_list_u8_leak_into_cstr(&buffer);
}


static void hasherAppendSemaTypePacked(Yy_Hasher *const hash, Sema *const sema, SemaTypePacked::Base const* const type)
{
    (void) sema;
    switch (type->tag) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag__type:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xundefined:
        case SemaType_Tag__xbool:
        case SemaType_Tag__xnoreturn: {
            // No payload.
            yy_assert(SemaTypePacked::tag_is_no_payload(type->tag));
            Bv as_bytes = bv_transmute_ptr_one(type);
            yy_hasher_append_bv(hash, as_bytes);
        } break;
        case SemaType_Tag__xstruct: {
            auto pl = type->get<SemaTypePacked::Struct>();
            struct ToHash {
                SemaType_Tag tag;
                u32 gen_id;
            } const to_hash{.tag = pl->base.tag, .gen_id = pl->struct_gen_id};
            yy_hasher_append_bv(hash, bv_transmute_ptr_one(&to_hash));
        } break;
        case SemaType_Tag__function: {
            auto pl = type->get<SemaTypePacked::Function>();
            yy_hasher_append_bv(hash, pl->as_bytes());
        } break;
        case SemaType_Tag__pointer: {
            auto pl = type->get<SemaTypePacked::Pointer>();
            yy_hasher_append_bv(hash, pl->as_bytes());
        } break;
    }
}

NODISCARD auto SemaTypeTableContext::eql(SemaTypePacked::Base const* const a, SemaTypeId const b_id, usize const b_table_index) const -> bool
{
    (void) b_table_index;
    SemaTypePacked::Base const* b = typeBase(sema, b_id);
    auto _ = sema->extra_pointers_lock.scoped_lock();
    if (a->tag != b->tag)
        return false;
    switch (a->tag) {
        case meta_enum_count<SemaType_Tag>:
            yy_unreachable();
        case SemaType_Tag__type:
        case SemaType_Tag__xundefined:
        case SemaType_Tag__xvoid:
        case SemaType_Tag__xnoreturn:
        case SemaType_Tag__uintword:
        case SemaType_Tag__xu8:
        case SemaType_Tag__xbool:
            return true;
        case SemaType_Tag__xstruct: {
            auto const a_pl = a->get<SemaTypePacked::Struct>();
            auto const b_pl = b->get<SemaTypePacked::Struct>();
            return a_pl->struct_gen_id == b_pl->struct_gen_id;
        } break;
        case SemaType_Tag__function: {
            auto const a_pl = a->get<SemaTypePacked::Function>();
            auto const b_pl = b->get<SemaTypePacked::Function>();
            if (a_pl->arg_count != b_pl->arg_count)
                return false;
            if (!typeIdEql(sema, a_pl->return_type, b_pl->return_type))
                return false;
            for (u32 i = 0; i < a_pl->arg_count; i += 1) {
                auto const arg_a = a_pl->arg_type(i);
                auto const arg_b = b_pl->arg_type(i);
                if (!typeIdEql(sema, arg_a, arg_b))
                    return false;
            }
            return true;
        }
        break;
        case SemaType_Tag__pointer: {
            auto const a_pl = a->get<SemaTypePacked::Pointer>();
            auto const b_pl = b->get<SemaTypePacked::Pointer>();
            if (!typeIdEql(sema, a_pl->child_type, b_pl->child_type))
                return false;
            return true;
        }
        break;
    }
    yy_unreachable();
}

NODISCARD auto SemaTypeTableContext::hash(SemaTypePacked::Base const* type) const -> u32 {
    Yy_Hasher hash = yy_hasher_init();
    hasherAppendSemaTypePacked(&hash, sema, type);
    return yy_hasher_final32(&hash);
}


NODISCARD static SemaTypeId
comptimeValueSemaType(Sema* const sema, ComptimeValue const* value)
{
    switch (value->tag) {
        case meta_enum_count<ComptimeValue_Tag>:
            yy_unreachable();
        case ComptimeValue_Tag__xnoreturn:
            yy_panic("error: internal compiler error: this is a bug in the yuceyuf compiler");
        case ComptimeValue_Tag__type: return basicInternedType(sema, SemaType_Tag__type);
        case ComptimeValue_Tag__xvoid: return basicInternedType(sema, SemaType_Tag__xvoid);
        case ComptimeValue_Tag__xundefined: return basicInternedType(sema, SemaType_Tag__xundefined);
        case ComptimeValue_Tag__xbool: return basicInternedType(sema, SemaType_Tag__xbool);
        case ComptimeValue_Tag__string: return typePointerTo(sema, basicInternedType(sema, SemaType_Tag__xu8));
        case ComptimeValue_Tag__integer: {
            auto const& integer = value->as<ComptimeValue_Tag__integer>().unwrap();
            return integer.int_type_new.unwrap_or(basicInternedType(sema, SemaType_Tag__uintword));
        } break;
    }
    yy_unreachable();
}

struct ComptimeInstMapping {
    bool present{false};
    u8 use_count;
    InternedComptimeValueId value;

    // Default constructor sets present to false.
    ComptimeInstMapping() = default;
    ComptimeInstMapping(InternedComptimeValueId v) : present(true), use_count(0), value(v) {}
};
static_assert(yy::trivial_raii<ComptimeInstMapping>);

struct ComptimeContext {
    Sema* sema;
    InstIndex head;
    u32 head_end_offset;
    Yy_Allocator heap;
    // TODO: Use Option here.
    // TODO: static_assert(Option::traits::zeroes_is_none)
    // TODO: Maybe use a hash map (or just an arraylist of key-val pair) here?
    ComptimeInstMapping MANY* inst_mappings; // size head_end_offset
    usize debug_max_inst_mappings_present;
    usize debug_current_inst_mappings_present;

    usize debug_scope_create_count;
    usize debug_scope_destroy_count;
};

NODISCARD static auto comptimeInstMapping_tableIndex(ComptimeContext* ctx, InstIndex inst) -> u32 {
    yy_assert(inst >= ctx->head && inst < ctx->head + ctx->head_end_offset);
    return inst.x - ctx->head.x;
}

static auto comptimeInstMapping_removeDestroy(
    ComptimeContext* ctx, InstIndex inst
) -> void {
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    yy_assert(table_ptr->present);
    ctx->debug_current_inst_mappings_present -= 1;
    *table_ptr = ComptimeInstMapping{};
}
static auto comptimeInstMapping_removeKeepPointer(
    ComptimeContext* ctx, InstIndex inst
) -> void {
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    yy_assert(table_ptr->present);
    table_ptr->present = false;
    ctx->debug_current_inst_mappings_present -= 1;
}

static auto comptimeInstMapping_putNoClobber(
    ComptimeContext* ctx, InstIndex inst, InternedComptimeValueId value
) -> void {
    auto const sema = ctx->sema;
    auto const is_noreturn = ctx->sema->comptime_value_pool[value].tag == ComptimeValue_Tag__xnoreturn;
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    yy_assert(!table_ptr->present);
    if (!is_noreturn) {
        ctx->debug_current_inst_mappings_present += 1;
        ctx->debug_max_inst_mappings_present =
            std::max(ctx->debug_max_inst_mappings_present, ctx->debug_current_inst_mappings_present);
        *table_ptr = ComptimeInstMapping{ value };
        make_inst_info_local_typed_full_comptime(
            sema, inst, comptimeValueSemaType(sema, &ctx->sema->comptime_value_pool[value]), value
        );
    } else {
        make_inst_info_local_typed_full_comptime(
            sema, inst, basicInternedType(sema, SemaType_Tag__xnoreturn), value
        );
    }
}

NODISCARD static auto comptimeInstMapping_getNoRefOrPutValue(
    ComptimeContext* ctx, InstIndex inst, InternedComptimeValueId value
) -> ComptimeInstMapping* {
    yy_assert(ctx->sema->comptime_value_pool[value].tag != ComptimeValue_Tag__xnoreturn);
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    if (table_ptr->present)
        return table_ptr;
    *table_ptr = ComptimeInstMapping{ value };
    ctx->debug_current_inst_mappings_present += 1;
    ctx->debug_max_inst_mappings_present =
        std::max(ctx->debug_max_inst_mappings_present, ctx->debug_current_inst_mappings_present);
    return table_ptr;
}

NODISCARD static auto comptimeInstMapping_maybeGetPtr(
    ComptimeContext* ctx, InstIndex inst
) -> Option<ComptimeInstMapping*> {
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    if (table_ptr->present) {
        return table_ptr;
    }
    return None;
}

NODISCARD static auto comptimeInstMapping_getAndRef(
    ComptimeContext* ctx, InstIndex inst
) -> InternedComptimeValueId {
    auto table_idx = comptimeInstMapping_tableIndex(ctx, inst);
    auto table_ptr = &ctx->inst_mappings[table_idx];
    yy_assert(table_ptr->present);
    table_ptr->use_count += 1;
    // If this is the last ref, remove it.
    if (table_ptr->use_count == ctx->sema->gi->insts[inst].untyped_use_count) {
        comptimeInstMapping_removeKeepPointer(ctx, inst);
    }
    return table_ptr->value;
}


struct ComptimeScope {
    Option<ComptimeScope*> parent;
    InstIndex block_like_inst;
};

NODISCARD static ComptimeContext ComptimeEvalContext_init(Sema* sema, InstIndex head)
{
    ComptimeContext result = {
        .sema = sema,
        .head = head,
        .head_end_offset = genir_instEndOffset(sema->gi, head),
        .heap = sema->heap,
        .inst_mappings = nullptr, // init below
        .debug_max_inst_mappings_present = 0,
        .debug_current_inst_mappings_present = 0,
        .debug_scope_create_count = 0,
        .debug_scope_destroy_count = 0,
    };
    result.inst_mappings = allocator_new_span_default_construct<ComptimeInstMapping>(sema->heap, result.head_end_offset).ptr;
    return result;
}

static void ComptimeEvalContext_deinit(ComptimeContext *const ctx)
{
    yy_assert(ctx->debug_scope_create_count == ctx->debug_scope_destroy_count);
    allocator_delete_span(ctx->heap, Span{ ctx->inst_mappings, ctx->head_end_offset });
}

static void comptimeScopeDestroy(ComptimeContext *ctx, ComptimeScope *scope)
{
    (void) scope;
    ctx->debug_scope_destroy_count += 1;
}

NODISCARD static ComptimeScope comptimeScopeCreate(ComptimeContext *ctx, Option<ComptimeScope*> parent, InstIndex block_like_inst)
{
    ctx->debug_scope_create_count += 1;
    return ComptimeScope {
        .parent = parent,
        .block_like_inst = block_like_inst,
    };
}

NODISCARD static YyStatus comptimeEvalInst_block(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *parent_scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result);
NODISCARD static YyStatus comptimeEvalInst(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result);

NODISCARD static YyStatus comptimeEvalInst_break_value(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId *const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_extraPl(gi, inst_idx, break_value);

    auto breakd_value = comptimeInstMapping_getAndRef(ctx, genir_instAbsSB(inst_idx, pl.value));
    comptimeInstMapping_putNoClobber(ctx, genir_instAbsSB(inst_idx, pl.block_to_break_to), breakd_value);

    // Don't map the noreturn value.
    *result = BasicInternedComptimeValue::const_xnoreturn;
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_break_void(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_smallPl(gi, inst_idx, break_void);

    InternedComptimeValueId const breakd_value = BasicInternedComptimeValue::const_xvoid;
    comptimeInstMapping_putNoClobber(ctx, pl.block_to_break_to.abs(inst_idx), breakd_value);

    *result = BasicInternedComptimeValue::const_xnoreturn;
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_const_undefined(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    yy_assert(genir_instsAt(gi, inst_idx)->tag == Yy_Ir_Inst_Tag__const_undefined);

    *result = BasicInternedComptimeValue::const_xundefined;
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_const_bool(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_smallPl(gi, inst_idx, const_bool);

    bool const bool_value = pl.value;
    *result = bool_value ? BasicInternedComptimeValue::const_xtrue : BasicInternedComptimeValue::const_xfalse;
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_const_int(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_extraPl(gi, inst_idx, const_int);

    auto i64_value = pl.integer.decode();
    *result = intern_comptime_value(sema, MetaTu_init(ComptimeValue, integer, { .value = i64_value, .int_type_new = None }));
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_const_cstring(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_extraPl(gi, inst_idx, const_cstring);

    *result = intern_comptime_value(sema, MetaTu_init(ComptimeValue, string, { .string = pl.string }));
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_bool_not(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto inst = *genir_instsAt(gi, inst_idx);
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__bool_not);

    auto operand_inst = genir_instAbsSB(inst_idx, inst.small_data.unary.operand);
    auto operand_value = comptimeInstMapping_getAndRef(ctx, operand_inst);
    if (sema->comptime_value_pool[operand_value].tag != ComptimeValue_Tag__xbool) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(
            &msg_buf, "mismatched types: expected 'bool', got '" Sv_Fmt "'",
            Sv_Arg(meta_enum_sv_short(sema->comptime_value_pool[operand_value].tag))
        );
        sema_add_hard_error_msg(sema, errorLoc(operand_inst), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "for this bool negation");
        return Yy_Bad;
    }
    bool operand_as_bool = sema->comptime_value_pool[operand_value].as<ComptimeValue_Tag__xbool>().unwrap();
    bool new_bool = !operand_as_bool;
    *result = new_bool ? BasicInternedComptimeValue::const_xtrue : BasicInternedComptimeValue::const_xfalse;
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst_xunreachable(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) ctx;
    (void) scope;
    (void) result;
    auto gi = sema->gi;
    auto pl = *genir_smallPl(gi, inst_idx, xunreachable);
    (void) pl;

    sema_add_hard_error_msg(sema, errorLoc(inst_idx), "reached unreachable code during compile time evaluation");
    return Yy_Bad;
}
NODISCARD static YyStatus comptimeEvalInst_type_ptr_to(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_smallPl(gi, inst_idx, type_ptr_to);

    auto child_inst = genir_instAbsSB(inst_idx, pl.operand);
    auto child_value = comptimeInstMapping_getAndRef(ctx, child_inst);
    if (sema->comptime_value_pool[child_value].tag != ComptimeValue_Tag__type) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected 'type', got '" Sv_Fmt "'", Sv_Arg(meta_enum_sv_short(sema->comptime_value_pool[child_value].tag)));
        // TODO: Track the origin of the value.
        sema_add_hard_error_msg(sema, errorLoc(child_inst), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "for this pointer type");
        return Yy_Bad;
    }
    auto child_type = sema->comptime_value_pool[child_value].as<ComptimeValue_Tag__type>().unwrap();

    auto result_type = typePointerTo(sema, child_type);
    *result = intern_comptime_value(sema, MetaTu_init(ComptimeValue, type, result_type));
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}

NODISCARD static YyStatus comptimeEvalInst_decl_val(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_extraPl(gi, inst_idx, decl_val);
    *result = intern_comptime_value(sema, sema->decl_comptime_values.get({}, pl.ref_decl.decode()).unwrap().value);
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}

NODISCARD static YyStatus comptimeEvalInst_cond_branch(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    (void) scope;
    auto gi = sema->gi;
    auto pl = *genir_extraPl(gi, inst_idx, cond_branch);

    auto cond_inst = genir_instAbsSB(inst_idx, pl.cond);
    auto cond_value = comptimeInstMapping_getAndRef(ctx, cond_inst);
    if (sema->comptime_value_pool[cond_value].tag != ComptimeValue_Tag__xbool) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected 'bool', got '" Sv_Fmt "'",
                          Sv_Arg(meta_enum_sv_short(sema->comptime_value_pool[cond_value].tag)));
        // TODO: Track the origin of the value.
        sema_add_hard_error_msg(sema, errorLoc(cond_inst), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    auto cond_bool = sema->comptime_value_pool[cond_value].as<ComptimeValue_Tag__xbool>().unwrap();
    auto choose_block = cond_bool ? genir_instAbsSF(inst_idx, pl.then) : genir_instAbsSF(inst_idx, pl.xelse);
    InternedComptimeValueId choose_value = yy::uninitialized;
    TRY(comptimeEvalInst(ctx, sema, scope, choose_block, &choose_value));
    yy_assert(sema->comptime_value_pool[choose_value].tag == ComptimeValue_Tag__xnoreturn);
    auto get = comptimeInstMapping_maybeGetPtr(ctx, inst_idx);
    if (get.is_none()) {
        *result = BasicInternedComptimeValue::const_xnoreturn;
        comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    } else {
        *result = get.unwrap()->value;
    }
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInstMakeType(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, SemaTypeId type_id, InternedComptimeValueId* const result)
{
    (void) sema;
    (void) scope;
    *result = intern_comptime_value(sema, MetaTu_init(ComptimeValue, type, type_id));
    comptimeInstMapping_putNoClobber(ctx, inst_idx, *result);
    return Yy_Ok;
}
NODISCARD static YyStatus comptimeEvalInst(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    auto gi = sema->gi;
    auto const inst_tag = genir_instsAt(gi, inst_idx)->tag;
    switch (inst_tag) {
        case meta_enum_count<Yy_Ir_Inst_Tag>:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__fn_decl:
        case Yy_Ir_Inst_Tag__struct_decl:
        case Yy_Ir_Inst_Tag__fn_arg_decl:
        case Yy_Ir_Inst_Tag__global_var_decl:
        case Yy_Ir_Inst_Tag__extern_var_decl:
        case Yy_Ir_Inst_Tag__extern_fn_decl:
        case Yy_Ir_Inst_Tag__return_void:
        case Yy_Ir_Inst_Tag__return_value:
        case Yy_Ir_Inst_Tag__local_var_decl:
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
        case Yy_Ir_Inst_Tag__truncate_int:
        case Yy_Ir_Inst_Tag__promote_int:
        case Yy_Ir_Inst_Tag__ptr_cast:
        case Yy_Ir_Inst_Tag__int_to_ptr:
        case Yy_Ir_Inst_Tag__ptr_to_int:
        case Yy_Ir_Inst_Tag__discard:
        case Yy_Ir_Inst_Tag__placeholder_ref:
        case Yy_Ir_Inst_Tag__analyzed_field_val:
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val:
        case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__decl_ref:
        case Yy_Ir_Inst_Tag__local_decl_val:
        case Yy_Ir_Inst_Tag__local_decl_ref:
        case Yy_Ir_Inst_Tag__call:
        case Yy_Ir_Inst_Tag__address_of:
        case Yy_Ir_Inst_Tag__deref_ref:
        case Yy_Ir_Inst_Tag__deref_val:
        case Yy_Ir_Inst_Tag__assign:
        case Yy_Ir_Inst_Tag__loop:
        case Yy_Ir_Inst_Tag__bits_not:
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub_wrap:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte:
        {
            fprintf(stderr, "TODO: %s " Sv_Fmt "\n", __func__, Sv_Arg(meta_enum_sv_short(inst_tag)));
            yy_panic("TODO");
        }
        break;
        case Yy_Ir_Inst_Tag__decl_val: {
            return comptimeEvalInst_decl_val(ctx, sema, scope, inst_idx, result);
        } break;
        case Yy_Ir_Inst_Tag__block:
            return comptimeEvalInst_block(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__cond_branch:
            return comptimeEvalInst_cond_branch(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__xunreachable:
            return comptimeEvalInst_xunreachable(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__const_undefined:
            return comptimeEvalInst_const_undefined(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__const_int:
            return comptimeEvalInst_const_int(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__const_bool:
            return comptimeEvalInst_const_bool(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__const_cstring:
            return comptimeEvalInst_const_cstring(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__bool_not:
            return comptimeEvalInst_bool_not(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__break_void:
            return comptimeEvalInst_break_void(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__break_value:
            return comptimeEvalInst_break_value(ctx, sema, scope, inst_idx, result);
        case Yy_Ir_Inst_Tag__type_u8:
            return comptimeEvalInstMakeType(ctx, sema, scope, inst_idx, basicInternedType(sema, SemaType_Tag__xu8), result);
        case Yy_Ir_Inst_Tag__type_uintword:
            return comptimeEvalInstMakeType(ctx, sema, scope, inst_idx, basicInternedType(sema, SemaType_Tag__uintword), result);
        case Yy_Ir_Inst_Tag__type_xbool:
            return comptimeEvalInstMakeType(ctx, sema, scope, inst_idx, basicInternedType(sema, SemaType_Tag__xbool), result);
        case Yy_Ir_Inst_Tag__type_xvoid:
            return comptimeEvalInstMakeType(ctx, sema, scope, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), result);
        case Yy_Ir_Inst_Tag__type_xnoreturn:
            return comptimeEvalInstMakeType(ctx, sema, scope, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), result);
        case Yy_Ir_Inst_Tag__type_ptr_to:
            return comptimeEvalInst_type_ptr_to(ctx, sema, scope, inst_idx, result);
        }
    yy_unreachable();
}
NODISCARD static YyStatus comptimeEvalInst_block(
    ComptimeContext *ctx, Sema* const sema, ComptimeScope *parent_scope, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    auto gi = sema->gi;
    auto block_pl = *genir_extraPl(gi, inst_idx, block);
    auto block_end_offset = block_pl.end_offset;
    auto new_scope = comptimeScopeCreate(ctx, parent_scope, inst_idx);

    // Eval each instruction in the block.
    Option<InstIndex> noreturn_inst_opt = None;
    {
        Option<InstIndex> prev_iter = None;
        for (InstIndex next_iter = inst_idx + 1;
        next_iter < inst_idx + block_end_offset;
        (void)0) {
            auto const iter = next_iter;
            next_iter += genir_instEndOffset(gi, next_iter);

            if (noreturn_inst_opt.has_value()) {
                if (next_iter >= inst_idx + block_end_offset) {
                    yy_assert(instMayBeImplicitEndOfBlock(genir_instsAt(gi, iter)->tag));
                    break;
                }
                auto noreturn_inst_tag = genir_instsAt(gi, noreturn_inst_opt.unwrap())->tag;
                // Don't show unreachable code errors due to `cond_branch` or `loop`.
                if (noreturn_inst_tag == Yy_Ir_Inst_Tag__cond_branch || noreturn_inst_tag == Yy_Ir_Inst_Tag__loop) {
                    break;
                }
                TRY(sema_new_soft_error(sema));
                sema_add_soft_error_msg(sema, errorLoc(iter), "unreachable code");
                if (prev_iter.has_value())
                    sema_add_error_note(sema, errorLoc(prev_iter.unwrap()), "last reachable code was here");
                break;
            }

            InternedComptimeValueId value = yy::uninitialized;
            TRY(comptimeEvalInst(ctx, sema, &new_scope, iter, &value));
            if (sema->comptime_value_pool[value].tag == ComptimeValue_Tag__xnoreturn) {
                noreturn_inst_opt = iter;
                yy_assert(comptimeInstMapping_maybeGetPtr(ctx, iter).is_none());
            } else if (sema->gi->insts[iter].untyped_use_count == 0) {
                // Check for unused values.
                // If the value is not void and never used (untyped info), throw an error.
                auto const the_type = comptimeValueSemaType(sema, &sema->comptime_value_pool[value]);
                if (!typeIsVoid(sema, the_type)) {
                    TRY(sema_new_soft_error(sema));
                    ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
                    array_list_printf(&msg_buf, "unused result of type '");
                    fmtSemaTypeId(&msg_buf, sema, the_type);
                    array_list_printf(&msg_buf, "'");
                    sema_add_soft_error_msg(sema, errorLoc(iter), Sv_Fmt, Sv_Arg(msg_buf));
                }
                comptimeInstMapping_removeDestroy(ctx, iter);
            }
            prev_iter = iter;
        }
    }
    yy_assert(noreturn_inst_opt.has_value());

    // Remove live values from the inst mapping. They may be live due to comptime branches.
    auto const noreturn_inst = noreturn_inst_opt.unwrap();
    for (InstIndex iter = inst_idx + 1; iter < noreturn_inst; iter += genir_instEndOffset(gi, iter)) {
        auto opt_get = comptimeInstMapping_maybeGetPtr(ctx, iter);
        if (opt_get.is_none())
            continue;
        // If `get.use_count == untyped_use_count`, we would have removed this mapping already.
        auto get = opt_get.unwrap();
        yy_assert(get->present);
        yy_assert(get->use_count < sema->gi->insts[iter].untyped_use_count);
        comptimeInstMapping_removeDestroy(ctx, iter);
    }

    auto get = comptimeInstMapping_maybeGetPtr(ctx, inst_idx);
    if (get.is_none()) {
        *result = BasicInternedComptimeValue::const_xnoreturn;
    } else {
        *result = get.unwrap()->value;
    }

    comptimeScopeDestroy(ctx, &new_scope);
    return Yy_Ok;
}

NODISCARD static YyStatus semaEvalBlockHead(
    Sema* const sema, Yy_Ir_InstIndex const inst_idx, InternedComptimeValueId* const result)
{
    ComptimeContext ctx = ComptimeEvalContext_init(sema, inst_idx);
    ComptimeScope head_scope = comptimeScopeCreate(&ctx, None, inst_idx);
    TRY(comptimeEvalInst(&ctx, sema, &head_scope, inst_idx, result));
    if (sema->comptime_value_pool[*result].tag == ComptimeValue_Tag__xnoreturn) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "can't instantiate a 'noreturn' type");
        // TODO: Track the origin of the value.
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    comptimeScopeDestroy(&ctx, &head_scope);
    ComptimeEvalContext_deinit(&ctx);
    return Yy_Ok;
}
NODISCARD static YyStatus semaEvalBlockAsType(
    Sema* const sema, Yy_Ir_InstIndex const inst_idx, SemaTypeId* const result)
{
    InternedComptimeValueId value = yy::uninitialized;
    TRY(semaEvalBlockHead(sema, inst_idx, &value));
    if (sema->comptime_value_pool[value].tag != ComptimeValue_Tag__type) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(
            &msg_buf, "expected 'type', got '" Sv_Fmt "'",
            Sv_Arg(meta_enum_sv_short(sema->comptime_value_pool[value].tag))
        );
        // TODO: Track the origin of the value.
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    *result = sema->comptime_value_pool[value].as<ComptimeValue_Tag__type>().unwrap();
    return Yy_Ok;
}


NODISCARD static auto makeStructMembers(Sema* const sema, SemaTypeId const type_id, Option<TypeMemoryStuff*> result)
    -> YyStatus {
    using Struct = SemaTypePacked::Struct;
    auto const gi = sema->gi;
    Yy_Ir_InstIndex inst_idx = yy::uninitialized;
    Yy_Ir_Inst_ExtraPl_StructDecl pl = yy::uninitialized;
    {
        auto strukt = typeBaseMut(sema, type_id)->get<Struct>();
        auto _ = sema->extra_pointers_lock.scoped_lock();
        switch (strukt->status) {
            using enum Struct::Status;
            case nothing: {
                strukt->status = doing_size;
            } break;
            case doing_size: {
                yy_panic("TODO: Error message for type dependency loop");
                return Yy_Bad;
            } break;
            case sized: {
                if (result.has_value())
                    *result.unwrap() = strukt->cached_size_align;
                return Yy_Ok;
            } break;
        }
        inst_idx = strukt->origin_inst_idx;
        pl = *genir_extraPl(sema->gi, inst_idx, struct_decl);
        yy_assert(pl.decl_id.decode() == strukt->origin_decl_id);
        yy_assert(pl.member_count == strukt->member_count);
    }

    auto const members_extra =
        ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_StructDecl>(gi, genir_extraPlIndex(gi, inst_idx, struct_decl));
    yy_assert(pl.member_count == members_extra.trailing_count);
    u64 build_align_of = 1;
    u64 build_size_of = 0;
    for (u32 i = 0; i < pl.member_count; i++) {
        auto member_extra = members_extra.nth_ptr(gi, i);
        auto member_type_eval_block = member_extra->type_eval_block.abs(inst_idx);
        SemaTypeId member_type_id = yy::uninitialized;
        // semaEvalBlockAsType may invalidate pointers. Don't cache the struct pointer.
        TRY(semaEvalBlockAsType(sema, member_type_eval_block, &member_type_id));
        if (typeIsNoreturn(sema, member_type_id)) {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "struct member can't be of type 'noreturn'");
            return Yy_Bad;
        }
        // semaEvalBlockAsType may invalidate pointers. Don't cache the struct pointer.
        auto const member_type_memory_size = typeIdMemoryStuff(sema, member_type_id);
        build_align_of = std::max(build_align_of, member_type_memory_size.align_of);
        auto const member_offset = yy_align_forward_power_of_two(member_type_memory_size.align_of, build_size_of);
        build_size_of = yy_align_forward_power_of_two(
            member_type_memory_size.align_of, member_offset + member_type_memory_size.size_of
        );
        // Do not cache typeBaseMut.
        typeBaseMut(sema, type_id)->get<Struct>()->unresolved_members()[i] = Struct::Member{
            .name = member_extra->name_z,
            .type = member_type_id,
            .offset = member_offset,
        };
    }

    auto strukt = typeBaseMut(sema, type_id)->get<Struct>();
    auto _ = sema->extra_pointers_lock.scoped_lock();
    strukt->cached_size_align = {
        .size_of = build_size_of,
        .align_of = build_align_of,
    };
    if (result.has_value())
        *result.unwrap() = strukt->cached_size_align;
    strukt->status = Struct::Status::sized;
    return Yy_Ok;
}


// NOTE: This function must be kept in sync with `basicInternedType` and `encodeTypeMaybeIntern`
static void semaInitBasicInternedTypes(Sema *const sema)
{
    // Allocate one per type tag. Some will be unused.
    yy_assert(sizeof(SemaTypePacked::Base) == sizeof(SemaType_Tag));
    sema->basic_interned_types = semaAllocExtra(sema, meta_enum_count_int<SemaType_Tag>);
    for (auto const type_tag : yy::meta_enum_range<SemaType_Tag>()) {
        SemaTypeId const sti = SemaTypeId {
            .sei = semaExtraIndexOffset(sema->basic_interned_types, (u32)type_tag)
        };
        switch (type_tag) {
            case yy::meta_enum_count<SemaType_Tag>:
                yy_unreachable();
            case SemaType_Tag__xstruct:
            case SemaType_Tag__function:
            case SemaType_Tag__pointer: {
                // This will never be interned.
                yy_debug_invalidate_ptr_one(&sema->extra[sti.sei.x]);
            }
            break;
            case SemaType_Tag__type:
            case SemaType_Tag__uintword:
            case SemaType_Tag__xu8:
            case SemaType_Tag__xvoid:
            case SemaType_Tag__xundefined:
            case SemaType_Tag__xbool:
            case SemaType_Tag__xnoreturn: {
                static_assert(sizeof(SemaTypePacked::Base) == sizeof(u32));
                sema->extra[sti.sei.x] = (u32)type_tag;
                yy_assert(SemaTypePacked::tag_is_no_payload(type_tag));
                auto const tmp_base = SemaTypePacked::Base(type_tag);
                sema->types.get_or_put_entry(sema, &tmp_base).unwrap_put().put_key_value(sti, Void{});
            }
            break;
        }
    }
}


struct Function {
    Yy_Ir_InstIndex fn_decl_inst_idx;
};

NODISCARD static YyStatus expectType(
    Sema *const sema,
    SemaTypeId const got_type, Yy_Ir_InstIndex const got_inst_loc,
    Yy_Ir_InstIndex const referenced_by,
    SemaTypeId const expected_type)
{
    yy_assert(semaTypeIdIsValid(got_type));
    yy_assert(semaTypeIdIsValid(expected_type));
    if (!typeIdEql(sema, got_type, expected_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "mismatched types: expected '");
        fmtSemaTypeId(&msg_buf, sema, expected_type);
        array_list_printf(&msg_buf, "', got '");
        fmtSemaTypeId(&msg_buf, sema, got_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(got_inst_loc), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(referenced_by), "referenced here");
        return Yy_Bad;
    }
    return Yy_Ok;
}

NODISCARD static YyStatus mergeTypes(
    Sema *const sema,
    Yy_Ir_InstIndex const break_to_inst_loc,
    SemaTypeId const a_type, Yy_Ir_InstIndex const a_break_inst_loc,
    SemaTypeId const b_type, Yy_Ir_InstIndex const b_break_inst_loc
)
{
    auto& sii = sema->sema_inst_infos[break_to_inst_loc];
    sii.resolved_comptime_value = None;
    yy_assert(!sii.comptime_only);
    yy_assert(sii.is_doing() && sii.specific.tag == SiiSpecific_Tag__block_like);
    if (!typeIdEql(sema, a_type, b_type)) {
        sema_add_hard_error_msg(sema, errorLoc(break_to_inst_loc), "failed to merge types");
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};

        msg_buf.clear();
        array_list_printf(&msg_buf, "type a here: '");
        fmtSemaTypeId(&msg_buf, sema, a_type);
        array_list_printf(&msg_buf, "'");
        sema_add_error_note(sema, errorLoc(a_break_inst_loc), Sv_Fmt, Sv_Arg(msg_buf));

        msg_buf.clear();
        array_list_printf(&msg_buf, "type b here: '");
        fmtSemaTypeId(&msg_buf, sema, b_type);
        array_list_printf(&msg_buf, "'");
        sema_add_error_note(sema, errorLoc(b_break_inst_loc), Sv_Fmt, Sv_Arg(msg_buf));

        return Yy_Bad;
    }
    return Yy_Ok;
}

NODISCARD static YyStatus mergeBreaks(Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst_Tag const inst_tag = gi->insts[inst_idx].tag;
    yy_assert(inst_tag == Yy_Ir_Inst_Tag__block || inst_tag == Yy_Ir_Inst_Tag__cond_branch || inst_tag == Yy_Ir_Inst_Tag__loop);
    auto& sii = sema->sema_inst_infos[inst_idx];
    yy_assert(sii.status == SemaInstInfoNew::Status::doing);
    auto& specific = sii.specific.as_block_like().unwrap();
    if (specific.first_breakd_from_inst.is_none()) {
        sii.type3 = basicInternedType(sema, SemaType_Tag__xnoreturn);
        sii.resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    }
    sii.status = SemaInstInfoNew::Status::full;
    return Yy_Ok;
}

static void referenceInst(Sema *const sema, Yy_Ir_InstIndex const refd, Yy_Ir_InstIndex const ref_by, Yy_Ir_InstIndex const ref_by_parent_block)
{
    u32 const parent_block_end_offset = genir_instEndOffset(sema->gi, ref_by_parent_block);
    yy_assert(refd < ref_by);
    yy_assert(ref_by_parent_block < refd && refd.x - ref_by_parent_block.x < parent_block_end_offset);
    yy_assert(ref_by_parent_block < ref_by && ref_by.x - ref_by_parent_block.x < parent_block_end_offset);
    auto& sii = sema->inst_info_full_mut(refd);
    sii.build_last_ref = ref_by;
    sii.build_use_count++;
}


NODISCARD static YyStatus analyzeFnItemMany(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex parent_inst_idx, Yy_Ir_InstIndex const start, u32 const stop_after_nth);
NODISCARD static YyStatus analyzeFnItemOne(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Option<Yy_Ir_InstIndex> const parent_block);

NODISCARD static YyStatus analyzeAssign(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst_ExtraPl_Assign const extra = *genir_extraPl(gi, inst_idx, assign);

    Yy_Ir_InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    Yy_Ir_InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);

    auto& lhs_sii = sema->inst_info_full_mut(lhs);
    yy_assert(typeIsPointer(sema, lhs_sii.type3));
    referenceInst(sema, lhs, inst_idx, parent_block);
    auto lhs_child_type = typeChild(sema, lhs_sii.type3);

    auto& rhs_sii = sema->inst_info_full_mut(rhs);
    TRY(expectType(sema, rhs_sii.type3, rhs, inst_idx, lhs_child_type));
    referenceInst(sema, rhs, inst_idx, parent_block);

    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), None);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeBinarySimple(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Option<SemaType_Tag> const in_type, SemaType_Tag const out_type, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);
    Yy_Ir_InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    Yy_Ir_InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    auto& lhs_sii = sema->inst_info_full_mut(lhs);
    auto& rhs_sii = sema->inst_info_full_mut(rhs);
    if (in_type.has_value()) {
        auto in = basicInternedType(sema, in_type.unwrap());
        TRY(expectType(sema, lhs_sii.type3, lhs, inst_idx, in));
        TRY(expectType(sema, rhs_sii.type3, rhs, inst_idx, in));
    } else {
        // TODO: Better error message
        TRY(expectType(sema, lhs_sii.type3, lhs, rhs, rhs_sii.type3));
    }
    referenceInst(sema, lhs, inst_idx, parent_block);
    referenceInst(sema, rhs, inst_idx, parent_block);

    if (lhs_sii.resolved_comptime_value.has_value() && rhs_sii.resolved_comptime_value.has_value()) {
        // TODO: do the binary operation, rewrite the instruction if not in a comptime scope.
    }
    auto resolved_comptime_value = None; // TODO
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, out_type), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeBinaryIntegerOrPointer(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    // We may rewrite this instruction.
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);
    Yy_Ir_InstIndex const lhs = genir_instAbsSB(inst_idx, extra.lhs);
    Yy_Ir_InstIndex const rhs = genir_instAbsSB(inst_idx, extra.rhs);
    auto& lhs_sii = sema->inst_info_full_mut(lhs);
    auto& rhs_sii = sema->inst_info_full_mut(rhs);
    SemaType_Tag const lhs_type_tag = decodeTypeTag(sema, lhs_sii.type3);
    SemaType_Tag const rhs_type_tag = decodeTypeTag(sema, rhs_sii.type3);
    SemaTypeId const uintword_type = basicInternedType(sema, SemaType_Tag__uintword);
    SemaTypeId out_type = yy::uninitialized;
    auto inst_tag = gi->insts[inst_idx].tag;
    auto inst_is_add = (inst_tag == Yy_Ir_Inst_Tag__add) || (inst_tag == Yy_Ir_Inst_Tag__add_wrap);
    auto inst_is_sub = (inst_tag == Yy_Ir_Inst_Tag__sub) || (inst_tag == Yy_Ir_Inst_Tag__sub_wrap);
    yy_assert(inst_is_add || inst_is_sub);
    if (lhs_type_tag == SemaType_Tag__pointer || rhs_type_tag == SemaType_Tag__pointer) {
        if (lhs_type_tag == SemaType_Tag__pointer && rhs_type_tag == SemaType_Tag__pointer) {
            if (inst_is_add) {
                sema_add_hard_error_msg(sema, errorLoc(inst_idx), "can't add '(pointer)' and '(pointer)'");
                sema_add_error_note(sema, errorLoc(lhs), "left-hand-side here");
                sema_add_error_note(sema, errorLoc(rhs), "right-hand-side here");
                return Yy_Bad;
            } else if (inst_is_sub) {
                out_type = uintword_type;
                TRY(expectType(sema, lhs_sii.type3, lhs, inst_idx, rhs_sii.type3));
                rewriteInstTag(sema, inst_idx, Yy_Ir_Inst_Tag__ptr_sub_ptr);
            } else yy_unreachable();
        } else if (lhs_type_tag == SemaType_Tag__pointer) {
            yy_assert(rhs_type_tag != SemaType_Tag__pointer);
            yy_assert(inst_is_add || inst_is_sub);
            out_type = lhs_sii.type3;
            TRY(expectType(sema, rhs_sii.type3, rhs, inst_idx, uintword_type));
            rewriteInstTag(sema, inst_idx, inst_is_add ? Yy_Ir_Inst_Tag__ptr_add_int : Yy_Ir_Inst_Tag__ptr_sub_int);
        } else if (rhs_type_tag == SemaType_Tag__pointer) {
            if (inst_is_sub) {
                // Can't subtract with pointer rhs.
                TRY(expectType(sema, rhs_sii.type3, rhs, inst_idx, uintword_type));
                yy_unreachable();
            }
            yy_assert(inst_is_add);
            out_type = rhs_sii.type3;
            TRY(expectType(sema, lhs_sii.type3, lhs, inst_idx, uintword_type));
            auto const new_extra_pl = Yy_Ir_Inst_ExtraPl_Binary{
                .lhs = rhs.asOffsetBackwardFrom(inst_idx),
                .rhs = lhs.asOffsetBackwardFrom(inst_idx),
            };
            auto const new_extra_idx = genir_appendExtraPl(sema->gi, new_extra_pl);
            rewriteInstTagExtraIndex(sema, inst_idx, Yy_Ir_Inst_Tag__ptr_add_int, new_extra_idx);
        } else yy_unreachable();
    } else {
        out_type = lhs_sii.type3;
        if (!typeIsInteger(sema, lhs_sii.type3)) {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            auto const tag_sv = yy::meta_enum_sv_short(inst_tag);
            array_list_printf(&msg_buf, "can't " Sv_Fmt " '", Sv_Arg(tag_sv));
            fmtSemaTypeId(&msg_buf, sema, lhs_sii.type3);
            array_list_printf(&msg_buf, "' and '");
            fmtSemaTypeId(&msg_buf, sema, rhs_sii.type3);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
            sema_add_error_note(sema, errorLoc(lhs), "left operand here");
            sema_add_error_note(sema, errorLoc(rhs), "right operand here");
            return Yy_Bad;
        }
        // both integers
        TRY(expectType(sema, lhs_sii.type3, lhs, inst_idx, lhs_sii.type3));
        TRY(expectType(sema, rhs_sii.type3, rhs, inst_idx, lhs_sii.type3));
    }
    referenceInst(sema, lhs, inst_idx, parent_block);
    referenceInst(sema, rhs, inst_idx, parent_block);

    yy_assert(semaTypeIdIsValid(out_type));
    auto resolved_comptime_value = None; // TODO: Comptime Value
    make_inst_info_local_typed_full(sema, inst_idx, out_type, resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeUnarySimple(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, SemaType_Tag const inout_type, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.unary.operand);
    auto& operand_sii = sema->inst_info_full_mut(operand);
    TRY(expectType(sema, operand_sii.type3, operand, inst_idx, basicInternedType(sema, inout_type)));
    referenceInst(sema, operand, inst_idx, parent_block);

    auto resolved_comptime_value = None; // TODO: Comptime Value
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, inout_type), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeAddressOf(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__address_of);

    Yy_Ir_InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.address_of.operand);

    auto& operand_sii = sema->inst_info_full_mut(operand);
    referenceInst(sema, operand, inst_idx, parent_block);
    auto resolved_comptime_value = None; // TODO: Comptime Value
    make_inst_info_local_typed_full(sema, inst_idx, operand_sii.type3, resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeDerefValOrRef(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const operand, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst_Tag const inst_tag = gi->insts[inst_idx].tag;
    yy_assert(inst_tag == Yy_Ir_Inst_Tag__deref_val || inst_tag == Yy_Ir_Inst_Tag__deref_ref);

    auto& operand_sii = sema->inst_info_full_mut(operand);
    auto const ptr_type_id = operand_sii.type3;
    auto const ptr_type = typeBase(sema, ptr_type_id);
    auto _ = sema->extra_pointers_lock.scoped_lock();
    if (ptr_type->tag != SemaType_Tag__pointer) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected pointer type, got '");
        fmtSemaTypeId(&msg_buf, sema, operand_sii.type3);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    auto const ptr_type_pl = ptr_type->get<SemaTypePacked::Pointer>();
    SemaTypeId const ptr_child_type = ptr_type_pl->child_type;
    referenceInst(sema, operand, inst_idx, parent_block);

    auto const result_type = (inst_tag == Yy_Ir_Inst_Tag__deref_val) ? ptr_child_type : ptr_type_id;
    auto resolved_comptime_value = None; // TODO: Comptime Value
    make_inst_info_local_typed_full(sema, inst_idx, result_type, resolved_comptime_value);
    return Yy_Ok;
}


NODISCARD static YyStatus analyzeDerefRef(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__deref_ref);

    Yy_Ir_InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.deref_ref.operand);
    return analyzeDerefValOrRef(sema, fn, inst_idx, operand, parent_block);
}

NODISCARD static YyStatus analyzeDerefVal(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__deref_val);

    Yy_Ir_InstIndex const operand = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.deref_val.operand);
    return analyzeDerefValOrRef(sema, fn, inst_idx, operand, parent_block);
}

NODISCARD static YyStatus analyzeDiscard(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__discard);

    Yy_Ir_InstIndex const to_discard = genir_instAbsSB(inst_idx, inst.small_data.discard.inst_to_discard);
    referenceInst(sema, to_discard, inst_idx, parent_block);
    auto resolved_comptime_value = BasicInternedComptimeValue::const_xvoid;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzePlaceholderRef(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__placeholder_ref);

    // NOTE: This instruction may get rewritten later, in which case it will be reanalyzed.
    Yy_Ir_InstIndex const refd = genir_instAbsSB(inst_idx, inst.small_data.placeholder_ref);
    referenceInst(sema, refd, inst_idx, parent_block);
    auto resolved_comptime_value = BasicInternedComptimeValue::const_xvoid;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeXunreachable(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__xunreachable);

    auto resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeReturnVoid(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__return_void);

    auto& fn_sii = sema->inst_info_full(fn->fn_decl_inst_idx);
    auto const fn_ret_type =
        typeBase(sema, fn_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap())
            ->get<SemaTypePacked::Function>()
            ->return_type;
    auto _ = sema->extra_pointers_lock.scoped_lock();
    TRY(expectType(
        sema,
        basicInternedType(sema, SemaType_Tag__xvoid), inst_idx,
        fn->fn_decl_inst_idx, fn_ret_type));

    auto resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeReturnValue(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__return_value);

    Yy_Ir_InstIndex const value = genir_instAbsSB(inst_idx, inst.small_data.return_value.operand);

    auto& value_sii = sema->inst_info_full_mut(value);
    referenceInst(sema, value, inst_idx, parent_block);

    auto& fn_sii = sema->inst_info_full(fn->fn_decl_inst_idx);
    auto const fn_ret_type =
        typeBase(sema, fn_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap())
            ->get<SemaTypePacked::Function>()
            ->return_type;
    auto _ = sema->extra_pointers_lock.scoped_lock();
    TRY(expectType(sema, value_sii.type3, inst_idx, fn->fn_decl_inst_idx, fn_ret_type));

    auto resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeBreakVoid(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__break_void);

    Yy_Ir_InstIndex const break_to = gi->insts[inst_idx].small_data.break_void.block_to_break_to.abs(inst_idx);
    yy_assert(break_to < inst_idx && inst_idx < break_to + genir_instEndOffset(gi, break_to));
    auto& break_to_sii = sema->sema_inst_infos[break_to];
    yy_assert(break_to_sii.is_doing());
    SemaTypeId const void_type = basicInternedType(sema, SemaType_Tag__xvoid);
    auto& specific = break_to_sii.specific.as_block_like().unwrap();
    if (specific.first_breakd_from_inst.has_value()) {
        TRY(mergeTypes(
            sema, break_to,
            break_to_sii.type3, specific.first_breakd_from_inst.unwrap(),
            void_type, inst_idx
        ));
    } else {
        break_to_sii.type3 = void_type;
        break_to_sii.resolved_comptime_value = BasicInternedComptimeValue::const_xvoid;;
        specific.first_breakd_from_inst = inst_idx;
    }

    auto resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeBreakValue(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__break_value);
    Yy_Ir_Inst_ExtraPl_BreakValue const pl = *genir_extraPl(gi, inst_idx, break_value);

    Yy_Ir_InstIndex const break_to = genir_instAbsSB(inst_idx, pl.block_to_break_to);
    yy_assert(break_to < inst_idx && inst_idx < break_to + genir_instEndOffset(gi, break_to));
    Yy_Ir_InstIndex const value = genir_instAbsSB(inst_idx, pl.value);
    auto& value_sii = sema->inst_info_full_mut(value);

    auto& break_to_sii = sema->sema_inst_infos[break_to];
    yy_assert(break_to_sii.is_doing());
    yy_assert(!typeIsNoreturn(sema, value_sii.type3));
    auto& specific = break_to_sii.specific.as_block_like().unwrap();
    if (specific.first_breakd_from_inst.has_value()) {
        TRY(mergeTypes(
            sema, break_to,
            break_to_sii.type3, specific.first_breakd_from_inst.unwrap(),
            value_sii.type3, inst_idx
        ));
    } else {
        break_to_sii.type3 = value_sii.type3;
        break_to_sii.resolved_comptime_value = value_sii.resolved_comptime_value;
        specific.first_breakd_from_inst = inst_idx;
    }
    referenceInst(sema, value, inst_idx, parent_block);

    auto resolved_comptime_value = BasicInternedComptimeValue::const_xnoreturn;
    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn), resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeBlock(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Option<Yy_Ir_InstIndex> const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    yy_assert(inst_idx.x < gi->insts.size());
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__block);

    Yy_Ir_Inst_ExtraPl_Block const pl = *genir_extraPl(gi, inst_idx, block);
    auto& sii = sema->sema_inst_infos[inst_idx];
    yy_assert(sii.is_nothing());
    sii.init_block_like();
    TRY(analyzeFnItemMany(sema, fn, inst_idx, inst_idx + 1, pl.end_offset - 1));

    TRY(mergeBreaks(sema, fn, inst_idx));
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeCondBranch(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__cond_branch);

    Yy_Ir_Inst_ExtraPl_CondBranch const pl = *genir_extraPl(gi, inst_idx, cond_branch);
    auto& sii = sema->sema_inst_infos[inst_idx];
    sii.init_block_like();

    Yy_Ir_InstIndex const cond = genir_instAbsSB(inst_idx, pl.cond);
    Yy_Ir_InstIndex const then = genir_instAbsSF(inst_idx, pl.then);
    Yy_Ir_InstIndex const xelse = genir_instAbsSF(inst_idx, pl.xelse);
    yy_assert(gi->insts[then].tag == Yy_Ir_Inst_Tag__block);
    yy_assert(gi->insts[xelse].tag == Yy_Ir_Inst_Tag__block);

    // Analyze the condition
    TRY(expectType(sema, sema->inst_info_full_mut(cond).type3, cond, inst_idx, basicInternedType(sema, SemaType_Tag__xbool)));
    referenceInst(sema, cond, inst_idx, parent_block);

    // Analyze the `then` block. It will break into the `cond_branch` therefore it is always noreturn (don't referenceInst it).
    TRY(analyzeFnItemOne(sema, fn, then, parent_block));
    TRY(expectType(sema, sema->inst_info_full_mut(then).type3, then, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn)));

    // Analyze the `xelse` block. It will break into the `cond_branch` therefore it is always noreturn (don't referenceInst it).
    TRY(analyzeFnItemOne(sema, fn, xelse, parent_block));
    TRY(expectType(sema, sema->inst_info_full_mut(xelse).type3, xelse, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn)));

    TRY(mergeBreaks(sema, fn, inst_idx));
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeLoop(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__loop);

    auto& sii = sema->sema_inst_infos[inst_idx];
    sii.init_block_like();

    Yy_Ir_InstIndex const loop_inner = yy_ir_inst_loop_getLoopInnerInstIndex(inst_idx);
    yy_assert(gi->insts[loop_inner].tag == Yy_Ir_Inst_Tag__block);

    // Analyze the inner block.
    TRY(analyzeFnItemOne(sema, fn, loop_inner, parent_block));
    auto& loop_inner_sii = sema->inst_info_full_mut(loop_inner);
    yy_assert(typeIsNoreturn(sema, loop_inner_sii.type3) || typeIsVoid(sema, loop_inner_sii.type3));

    TRY(mergeBreaks(sema, fn, inst_idx));
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeLocalVarDecl(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_var_decl);

    auto const pl = *genir_extraPl(gi, inst_idx, local_var_decl);
    Yy_Ir_InstIndex const initial_value = genir_instAbsSB(inst_idx, pl.initial_value);

    auto& init_value_sii = sema->inst_info_full_mut(initial_value);

    SemaTypeId const init_type_id = init_value_sii.type3;
    yy_assert(!typeIsNoreturn(sema, init_type_id));
    SemaTypeId the_type_id = yy::uninitialized;
    if (pl.type_block.is_none()) {
        // Inferred type
        the_type_id = init_type_id;
        if (typeIsUndefined(sema, init_type_id)) {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "variable with inferred type can't have value 'undefined'. you should give an explicit type.");
            return Yy_Bad;
        }
    } else {
        // Explicit variable type
        auto type_block_abs = pl.type_block.unwrap().abs(inst_idx);
        TRY(semaEvalBlockAsType(sema, type_block_abs, &the_type_id));
        if (typeIsNoreturn(sema, the_type_id)) {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "local variable can't be of type 'noreturn'");
            return Yy_Bad;
        }
    }
    if (!typeIsUndefined(sema, init_type_id)) {
        TRY(expectType(sema, init_type_id, initial_value, inst_idx, the_type_id));
    }
    referenceInst(sema, initial_value, inst_idx, parent_block);

    if (typeIsType(sema, the_type_id)) {
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), "local variable can't be of type 'type'");
        return Yy_Bad;
    }

    make_inst_info_local_decl_full(sema, inst_idx, the_type_id);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeFnArgDecl(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) parent_block;
    GenIr *const gi = sema->gi;

    u32 const arg_idx = genir_smallPl(gi, inst_idx, fn_arg_decl)->arg_idx;

    auto& fn_sii = sema->inst_info_full(fn->fn_decl_inst_idx);
    auto const arg_type =
        typeBase(sema, fn_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap())
            ->get<SemaTypePacked::Function>()
            ->arg_type(arg_idx);

    make_inst_info_local_decl_full(sema, inst_idx, arg_type);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeLocalDeclValOrRef(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, bool const by_ref, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_decl_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__local_decl_val);
    }

    Yy_Ir_InstIndex const ref = genir_instAbsSB(inst_idx, inst.small_data.local_decl_val_or_ref.ref);

    SemaTypeId the_type_id = yy::uninitialized;
    Yy_Ir_Inst const ref_inst = gi->insts[ref];
    switch (ref_inst.tag) {
        case meta_enum_count<Yy_Ir_Inst_Tag>:
            yy_unreachable();
        default:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__global_var_decl: case Yy_Ir_Inst_Tag__extern_var_decl:
        case Yy_Ir_Inst_Tag__fn_decl: case Yy_Ir_Inst_Tag__extern_fn_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__local_var_decl: case Yy_Ir_Inst_Tag__fn_arg_decl: {
            auto& ref_sii = sema->inst_info_full_mut(ref);
            the_type_id = ref_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap();
            // Dont touch `last_ref`.
        } break;
    }
    SemaTypeId pointer_type_id = typePointerTo(sema, the_type_id);

    auto resolved_comptime_value = None; // TODO: Comptime Value
    // TODO: Comptime variables.
    make_inst_info_local_typed_full(sema, inst_idx, by_ref ? pointer_type_id : the_type_id, resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeDeclValOrRef(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, bool const by_ref, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__decl_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__decl_val);
    }

    auto const ref_decl_id = genir_extrasAtTypedNoAssert<Yy_Ir_Inst_ExtraPl_DeclValOrRef>(
        gi, inst.small_data.decl_val_or_ref.extra_pl_index
    )->ref_decl.decode();
    // If sema collect failed on that decl, we can't continue.
    if (gi->decl_ptrs[ref_decl_id]->has_sema_collect_hard_errors) {
        logDebug("stopping %s on inst $%u because we refd decl " DeclId_Fmt "", __func__, inst_idx.x, DeclId_Arg(ref_decl_id));
        yy_assert(sema->errors.hard_count > 0);
        return Yy_Bad;
    }
    auto const ref_inst_idx = gi->decl_ptrs[ref_decl_id]->inst;

    SemaTypeId the_type_id = yy::uninitialized;
    Yy_Ir_Inst const ref_inst = gi->insts[ref_inst_idx];
    switch (ref_inst.tag) {
        case meta_enum_count<Yy_Ir_Inst_Tag>:
            yy_unreachable();
        default:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__local_var_decl: case Yy_Ir_Inst_Tag__fn_arg_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__global_var_decl: case Yy_Ir_Inst_Tag__extern_var_decl: {
            auto& ref_sii = sema->sema_inst_infos[ref_inst_idx];
            the_type_id = ref_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap();
            // Dont touch `last_ref`.
        }
        break;
        case Yy_Ir_Inst_Tag__extern_fn_decl: case Yy_Ir_Inst_Tag__fn_decl: {
            if (by_ref) {
                sema_add_hard_error_msg(sema, errorLoc(inst_idx), "can't take a reference from a function");
                return Yy_Bad;
            }
            auto& ref_sii = sema->sema_inst_infos[ref_inst_idx];
            the_type_id = ref_sii.specific.as_decl_like().unwrap().resolved_decl_type.unwrap();
            // Dont touch `last_ref`.
        }
        break;
    }
    SemaTypeId pointer_type_id = typePointerTo(sema, the_type_id);

    auto resolved_comptime_value = None; // TODO: Comptime Value
    // TODO: Comptime variables.
    make_inst_info_local_typed_full(sema, inst_idx, by_ref ? pointer_type_id : the_type_id, resolved_comptime_value);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeDeclVal(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    return analyzeDeclValOrRef(sema, fn, inst_idx, false, parent_block);
}
NODISCARD static YyStatus analyzeDeclRef(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    return analyzeDeclValOrRef(sema, fn, inst_idx, true, parent_block);
}

NODISCARD static YyStatus analyzeUntypedFieldValOrRef(
    Sema* const sema,
    Function* const fn,
    Yy_Ir_InstIndex const inst_idx,
    bool const by_ref,
    Yy_Ir_InstIndex const parent_block
) {
    (void)fn;
    (void)parent_block;
    GenIr* const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    if (by_ref) {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__untyped_field_ref);
    } else {
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__untyped_field_val);
    }

    auto const pl = *genir_extrasAtTypedNoAssert<Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef>(
        gi, inst.small_data.untyped_field_val_or_ref.extra_pl_index
    );
    auto const container = pl.container.abs(inst_idx);
    auto const old_container = container;
    auto const field_ident = pl.field;
    // The container is always by ref, so we always have a pointer to something.
    todo_assert(sema->inst_info_full_mut(container).resolved_comptime_value.is_none());
    auto struct_ptr_type = sema->inst_info_full_mut(container).type3;
    auto struct_type_id = typeChild(sema, struct_ptr_type);
    auto const old_struct_type_id = struct_type_id;
    bool was_pointer = false;
    if (typeIsPointer(sema, struct_type_id)) {
        // Left hand side was a pointer. Do auto dereference.
        struct_ptr_type = struct_type_id;
        struct_type_id = typeChild(sema, struct_type_id);
        was_pointer = true;
    }
    // TODO: If pointer (container is pointer to pointer), dereference it (needs air rewriting).
    if (decodeTypeTag(sema, struct_type_id) != SemaType_Tag__xstruct) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected struct or pointer to struct, got '");
        fmtSemaTypeId(&msg_buf, sema, old_struct_type_id);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(container), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        // check double pointer
        if (was_pointer && typeIsPointer(sema, struct_type_id) &&
            decodeTypeTag(sema, typeChild(sema, struct_type_id)) == SemaType_Tag__xstruct) {
            sema_add_error_note(sema, errorLoc(inst_idx), "consider dereferencing with `.*` before accessing the field");
        }
        return Yy_Bad;
    }
    // TODO: Handle makeStructMembers errors.
    yy_unwrap_ok(makeStructMembers(sema, struct_type_id, None));
    SemaTypeId member_type = yy::uninitialized;
    Yy_Ir_InstIndex new_container = container;
    {
        using Struct = SemaTypePacked::Struct;
        auto struct_type = typeBase(sema, struct_type_id)->get<Struct>();
        auto _ = sema->extra_pointers_lock.scoped_lock();
        auto const members = struct_type->resolved_members();
        auto found = yy::try_find_index(members, field_ident, &Struct::Member::name);
        if (found.is_none()) {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            auto const field_sv = genir_identifier_sv(sema->gi, field_ident);
            array_list_printf(&msg_buf, "unknown field " Sv_Fmt " for '", Sv_Arg(field_sv));
            fmtSemaTypeId(&msg_buf, sema, struct_type_id);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
            return Yy_Bad;
        }
        auto const member_idx = yy::checked_int_cast<u32>(found.unwrap()).unwrap();


        if (was_pointer) {
            // Change placeholder to deref_val(%container);
            // Instead of using pl.container, use the placeholder.
            new_container = inst_idx - 1u;
            yy_assert(gi->insts[new_container].tag == Yy_Ir_Inst_Tag__placeholder_ref);
            yy_assert(sema->inst_info_full_mut(new_container).build_last_ref.is_none());
            yy_assert(gi->insts[new_container].untyped_use_count == 0);
            yy_assert(gi->insts[old_container].untyped_use_count >= 2);
            rewriteInstRemoveUntypedInstRef(sema, inst_idx, {old_container});
            yy_assert(sema->inst_info_full_mut(old_container).build_use_count == 1);
            sema->inst_info_full_mut(old_container).build_use_count -= 1;
            sema->inst_info_full_mut(old_container).build_last_ref = None;
            referenceInst(sema, old_container, new_container, parent_block);
            rewriteInstAddUntypedInstRef(sema, inst_idx, {new_container});
            rewriteInstTagSmallPl(
                sema, new_container, Yy_Ir_Inst_Tag__deref_val,
                Yy_Ir_Inst_SmallPl{.deref_val = {old_container.asOffsetBackwardFrom(new_container)}}
            );
            make_inst_info_local_typed_full(sema, new_container, struct_ptr_type, None);
        } else {
            yy_assert(struct_ptr_type == sema->inst_info_full_mut(container).type3);
        }
        auto const new_extra_idx = genir_appendExtraPl(
            sema->gi,
            Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef{
                .container = new_container.asOffsetBackwardFrom(inst_idx),
                .field_idx = member_idx,
            }
        );
        rewriteInstTagExtraIndex(
            sema, inst_idx, by_ref ? Yy_Ir_Inst_Tag__analyzed_field_ref : Yy_Ir_Inst_Tag__analyzed_field_val,
            new_extra_idx
        );
        member_type = members[member_idx].type;
    }

    auto const pointer_type = typePointerTo(sema, member_type);
    referenceInst(sema, new_container, inst_idx, parent_block);
    auto resolved_comptime_value = None; // TODO: Comptime values.
    make_inst_info_local_typed_full(sema, inst_idx, by_ref ? pointer_type : member_type, resolved_comptime_value);
    return Yy_Ok;
}
NODISCARD static YyStatus analyzeUntypedFieldVal(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    return analyzeUntypedFieldValOrRef(sema, fn, inst_idx, false, parent_block);
}
NODISCARD static YyStatus analyzeUntypedFieldRef(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    return analyzeUntypedFieldValOrRef(sema, fn, inst_idx, true, parent_block);
}

NODISCARD static YyStatus analyzeCall(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call);

    Yy_Ir_Inst_ExtraPl_Call const pl = *genir_extraPl(gi, inst_idx, call);
    Yy_Ir_InstIndex const callee = genir_instAbsSB(inst_idx, pl.callee);

    // Check that the callee is a function
    auto& callee_sii = sema->inst_info_full_mut(callee);
    referenceInst(sema, callee, inst_idx, parent_block);
    SemaTypeId const callee_type_id = callee_sii.type3;
    auto const callee_type = typeBase(sema, callee_type_id);
    auto _ = sema->extra_pointers_lock.scoped_lock();
    if (callee_type->tag != SemaType_Tag__function) {
        {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "expected function with %u parameters, got '", pl.arg_count);
            fmtSemaTypeId(&msg_buf, sema, callee_type_id);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(callee), Sv_Fmt, Sv_Arg(msg_buf));
            sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        }
        return Yy_Bad;
    }
    auto const callee_type_pl = callee_type->get<SemaTypePacked::Function>();

    // Check that the call has the correct number of arguments.
    if (callee_type_pl->arg_count != pl.arg_count) {
        // TODO: "note: function was declared here"
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), "expected %u arguments, got %u, for function call",
                                callee_type_pl->arg_count, pl.arg_count);
        {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "callee type: '");
            fmtSemaTypeId(&msg_buf, sema, callee_type_id);
            array_list_printf(&msg_buf, "'");
            sema_add_error_note(sema, errorLoc(callee), Sv_Fmt, Sv_Arg(msg_buf));
        }
        return Yy_Bad;
    }

    // Check function arguments.
    static_assert(sizeof(SemaTypeId) == sizeof(u32), "");
    for (u32 i = 0; i < pl.arg_count; i += 1) {
        Yy_Ir_Inst_ExtraPl_Call_Arg const call_arg_data = genir_getCallArgData(gi, inst_idx, i);
        Yy_Ir_InstIndex const arg_inst = genir_instAbsSB(inst_idx, call_arg_data.inst);
        auto& call_arg_sii = sema->inst_info_full_mut(arg_inst);
        referenceInst(sema, arg_inst, inst_idx, parent_block);

        SemaTypeId const expected_arg_type = callee_type_pl->arg_type(i);

        // TODO: Better error message: something like
        //  error: bad parameter 2 type: expected A, got B
        //  note: callee type: ...
        //  note: parameter declared here (inside the function declaration)
        TRY(expectType(sema, call_arg_sii.type3, arg_inst, inst_idx, expected_arg_type));
    }

    make_inst_info_local_typed_full(sema, inst_idx, callee_type_pl->return_type, None);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeCallBuiltinPrintHelloWorld(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    (void) parent_block;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call_builtin_print_hello_world);

    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), BasicInternedComptimeValue::const_xvoid);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeCallBuiltinPrintUint(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__call_builtin_print_uint);

    Yy_Ir_InstIndex const param = genir_instAbsSB(inst_idx, gi->insts[inst_idx].small_data.call_builtin_print_uint.arg_inst);

    auto& param_sii = sema->inst_info_full_mut(param);
    TRY(expectType(sema, param_sii.type3, param, inst_idx, basicInternedType(sema, SemaType_Tag__uintword)));
    referenceInst(sema, param, inst_idx, parent_block);

    make_inst_info_local_typed_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__xvoid), BasicInternedComptimeValue::const_xvoid);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeTruncateInt(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__truncate_int);

    Yy_Ir_Inst_ExtraPl_TruncateInt const pl = *genir_extraPl(gi, inst_idx, truncate_int);

    SemaTypeId dest_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, pl.dest_type_block), &dest_type));
    Yy_Ir_InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    auto& src_value_sii = sema->inst_info_full_mut(src_value);
    SemaTypeId const src_type = src_value_sii.type3;

    if (!typeIsInteger(sema, dest_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected integer type for `@truncate`, got '");
        fmtSemaTypeId(&msg_buf, sema, dest_type);
        array_list_printf(&msg_buf, "'");
        // TODO: Better error location
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    if (!typeIsInteger(sema, src_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected integer type for `@truncate`, got '");
        fmtSemaTypeId(&msg_buf, sema, src_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(src_value), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        return Yy_Bad;
    }

    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    if (!typeIdEql(sema, src_type, dest_type)) {
        if (dest_size > src_size) {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "can't truncate '");
            fmtSemaTypeId(&msg_buf, sema, src_type);
            array_list_printf(&msg_buf, "' into '");
            fmtSemaTypeId(&msg_buf, sema, dest_type);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
            sema_add_error_note(sema, errorLoc(inst_idx), "`@truncate` requires the destination type to be smaller or equal to the source type");
            return Yy_Bad;
        }
    }

    referenceInst(sema, src_value, inst_idx, parent_block);
    if (src_value_sii.resolved_comptime_value.has_value()) {
        auto const& src = sema->comptime_value_pool[src_value_sii.resolved_comptime_value.unwrap()];
        todo_assert(src.tag != ComptimeValue_Tag__xundefined);
        auto const& integer = src.as_integer().unwrap();
        auto const dest_bits = dest_size * 8;
        yy_assert(dest_bits < 128);
        yy_assert(sizeof(integer.value)*8 == 64);
        i64 mask = static_cast<i64>((__uint128_t{1} << dest_bits) - 1);
        i64 new_int_value = integer.value & mask;
        auto new_integer = MetaTu_init(ComptimeValue, integer, { .value = new_int_value, .int_type_new = dest_type });
        auto resolved_comptime_value = intern_comptime_value(sema, new_integer);
        make_inst_info_local_typed_full(sema, inst_idx, dest_type, resolved_comptime_value);
    } else {
        make_inst_info_local_typed_full(sema, inst_idx, dest_type, None);
    }
    return Yy_Ok;
}

NODISCARD static YyStatus analyzePromoteInt(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__promote_int);

    Yy_Ir_Inst_ExtraPl_PromoteInt const pl = *genir_extraPl(gi, inst_idx, promote_int);

    SemaTypeId dest_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, pl.dest_type_block), &dest_type));
    Yy_Ir_InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    auto& src_value_sii = sema->inst_info_full_mut(src_value);
    SemaTypeId const src_type = src_value_sii.type3;

    if (!typeIsInteger(sema, dest_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected integer type for integer promotion, got '");
        fmtSemaTypeId(&msg_buf, sema, dest_type);
        array_list_printf(&msg_buf, "'");
        // TODO: Better error location
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }
    if (!typeIsInteger(sema, src_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected integer type for integer promotion, got '");
        fmtSemaTypeId(&msg_buf, sema, src_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(src_value), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        return Yy_Bad;
    }

    u64 const dest_size = typeIdMemorySizeOf(sema, dest_type);
    u64 const src_size = typeIdMemorySizeOf(sema, src_type);
    if (!typeIdEql(sema, src_type, dest_type)) {
        if (dest_size < src_size) {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "can't coerce '");
            fmtSemaTypeId(&msg_buf, sema, src_type);
            array_list_printf(&msg_buf, "' into '");
            fmtSemaTypeId(&msg_buf, sema, dest_type);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
            return Yy_Bad;
        }
    }

    referenceInst(sema, src_value, inst_idx, parent_block);
    if (src_value_sii.resolved_comptime_value.has_value()) {
        auto const& src = sema->comptime_value_pool[src_value_sii.resolved_comptime_value.unwrap()];
        todo_assert(src.tag != ComptimeValue_Tag__xundefined);
        auto const& integer = src.as_integer().unwrap();
        auto const dest_bits = dest_size * 8;
        yy_assert(dest_bits < 128);
        yy_assert(sizeof(integer.value)*8 == 64);
        i64 mask = static_cast<i64>((__uint128_t{1} << dest_bits) - 1);
        if ((integer.value & mask) == integer.value) {
            auto new_integer = MetaTu_init(ComptimeValue, integer, { .value = integer.value, .int_type_new = dest_type });
            auto resolved_comptime_value = intern_comptime_value(sema, new_integer);
            make_inst_info_local_typed_full(sema, inst_idx, dest_type, resolved_comptime_value);
        } else {
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "value %" PRIi64 " doesn't fit into type '", integer.value);
            fmtSemaTypeId(&msg_buf, sema, dest_type);
            array_list_printf(&msg_buf, "'");
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
            return Yy_Bad;
        }
    } else {
        make_inst_info_local_typed_full(sema, inst_idx, dest_type, None);
    }
    return Yy_Ok;
}
NODISCARD static YyStatus analyzePtrCast(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__ptr_cast);

    auto const pl = *genir_extraPl(gi, inst_idx, ptr_cast);

    SemaTypeId dest_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, pl.dest_type_block), &dest_type));
    Yy_Ir_InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    auto& src_value_sii = sema->inst_info_full_mut(src_value);
    todo_assert(src_value_sii.resolved_comptime_value.is_none());
    SemaTypeId const src_type = src_value_sii.type3;

    if (!typeIsPointer(sema, src_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected pointer type for @ptr_cast, got '");
        fmtSemaTypeId(&msg_buf, sema, src_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(src_value), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        return Yy_Bad;
    }
    if (!typeIsPointer(sema, dest_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected pointer type for @ptr_cast, got '");
        fmtSemaTypeId(&msg_buf, sema, dest_type);
        array_list_printf(&msg_buf, "'");
        // TODO: Better error location
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }

    referenceInst(sema, src_value, inst_idx, parent_block);
    make_inst_info_local_typed_full(sema, inst_idx, dest_type, None);
    return Yy_Ok;
}
NODISCARD static YyStatus analyzeIntToPtr(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__int_to_ptr);

    auto const pl = *genir_extraPl(gi, inst_idx, int_to_ptr);

    SemaTypeId dest_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, pl.dest_type_block), &dest_type));
    Yy_Ir_InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    auto& src_value_sii = sema->inst_info_full_mut(src_value);
    SemaTypeId const src_type = src_value_sii.type3;
    todo_assert(src_value_sii.resolved_comptime_value.is_none());

    if (!typeIsInteger(sema, src_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected integer type for @int_to_ptr, got '");
        fmtSemaTypeId(&msg_buf, sema, src_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(src_value), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        return Yy_Bad;
    }
    if (!typeIsPointer(sema, dest_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected pointer type for @int_to_ptr, got '");
        fmtSemaTypeId(&msg_buf, sema, dest_type);
        array_list_printf(&msg_buf, "'");
        // TODO: Better error location
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        return Yy_Bad;
    }

    referenceInst(sema, src_value, inst_idx, parent_block);
    make_inst_info_local_typed_full(sema, inst_idx, dest_type, None);
    return Yy_Ok;
}
NODISCARD static YyStatus analyzePtrToInt(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex const inst_idx, Yy_Ir_InstIndex const parent_block)
{
    (void) fn;
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__ptr_to_int);

    auto const pl = *genir_smallPl(gi, inst_idx, ptr_to_int);

    SemaTypeId const dest_type = basicInternedType(sema, SemaType_Tag__uintword);
    Yy_Ir_InstIndex const src_value = genir_instAbsSB(inst_idx, pl.src_value);
    auto& src_value_sii = sema->inst_info_full_mut(src_value);
    SemaTypeId const src_type = src_value_sii.type3;
    todo_assert(src_value_sii.resolved_comptime_value.is_none());

    if (!typeIsPointer(sema, src_type)) {
        ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
        array_list_printf(&msg_buf, "expected pointer type for @ptr_to_int, got '");
        fmtSemaTypeId(&msg_buf, sema, src_type);
        array_list_printf(&msg_buf, "'");
        sema_add_hard_error_msg(sema, errorLoc(src_value), Sv_Fmt, Sv_Arg(msg_buf));
        sema_add_error_note(sema, errorLoc(inst_idx), "referenced here");
        return Yy_Bad;
    }

    referenceInst(sema, src_value, inst_idx, parent_block);
    make_inst_info_local_typed_full(sema, inst_idx, dest_type, None);
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeFnItemOne(
    Sema *const sema, Function *fn, Yy_Ir_InstIndex const inst_idx, Option<Yy_Ir_InstIndex> const parent_block)
{
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    logTrace("%s: " IOF_Fmt " " Sv_Fmt "", __func__,
             IOF_Arg(genir_instOffsetFunction(fn->fn_decl_inst_idx, inst_idx)),
             Sv_Arg(meta_enum_sv_short(inst.tag)));
    // the first block in a function has no parent.
    if (parent_block.is_none())
        yy_assert(inst.tag == Yy_Ir_Inst_Tag__block);
    if (parent_block.has_value())
        yy_assert(gi->insts[parent_block.unwrap()].tag == Yy_Ir_Inst_Tag__block);

    switch (inst.tag) {
        case meta_enum_count<Yy_Ir_Inst_Tag>:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__decl_error_skip:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to: {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "can't instantiate 'type' at runtime");
            return Yy_Bad;
        } break;
        case Yy_Ir_Inst_Tag__root:
        case Yy_Ir_Inst_Tag__extern_fn_decl:
        case Yy_Ir_Inst_Tag__extern_var_decl:
        case Yy_Ir_Inst_Tag__global_var_decl:
        case Yy_Ir_Inst_Tag__fn_decl:
        case Yy_Ir_Inst_Tag__struct_decl:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__analyzed_field_val:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__block: return analyzeBlock(sema, fn, inst_idx, parent_block);
        case Yy_Ir_Inst_Tag__cond_branch: return analyzeCondBranch(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__loop: return analyzeLoop(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__local_var_decl: return analyzeLocalVarDecl(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__fn_arg_decl: return analyzeFnArgDecl(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__untyped_field_val: return analyzeUntypedFieldVal(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__untyped_field_ref: return analyzeUntypedFieldRef(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__decl_val: return analyzeDeclVal(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__decl_ref: return analyzeDeclRef(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__local_decl_val: return analyzeLocalDeclValOrRef(sema, fn, inst_idx, false, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__local_decl_ref: return analyzeLocalDeclValOrRef(sema, fn, inst_idx, true, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__const_cstring: {
            auto const value = genir_extraPl(gi, inst_idx, const_cstring)->string;
            SemaTypeId const ptr_child_type = basicInternedType(sema, SemaType_Tag__xu8);
            SemaTypeId const ptr_type = typePointerTo(sema, ptr_child_type);
            auto resolved_comptime_value = intern_comptime_value(sema, MetaTu_init(ComptimeValue, string, {value}));
            make_inst_info_local_typed_full(sema, inst_idx, ptr_type, resolved_comptime_value);
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__const_int: {
            auto const int_value = genir_extraPl(gi, inst_idx, const_int)->integer.decode();
            auto resolved_comptime_value =
                intern_comptime_value(sema, MetaTu_init(ComptimeValue, integer, {.value = int_value}));
            make_inst_info_local_typed_full( sema, inst_idx, basicInternedType(sema, SemaType_Tag__uintword), resolved_comptime_value);
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__const_bool: {
            auto const bool_value = genir_smallPl(gi, inst_idx, const_bool)->value;
            make_inst_info_local_typed_full(
                sema, inst_idx, basicInternedType(sema, SemaType_Tag__xbool),
                bool_value ? BasicInternedComptimeValue::const_xtrue : BasicInternedComptimeValue::const_xfalse
            );
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__const_undefined: {
            make_inst_info_local_typed_full(
                sema, inst_idx, basicInternedType(sema, SemaType_Tag__xundefined),
                BasicInternedComptimeValue::const_xundefined
            );
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
            yy_panic("TODO: Genir doesn't generate these yet.");
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__sub_wrap:
            return analyzeBinaryIntegerOrPointer(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
            return analyzeBinarySimple(sema, fn, inst_idx, SemaType_Tag__uintword, SemaType_Tag__uintword, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
            return analyzeBinarySimple(sema, fn, inst_idx, None, SemaType_Tag__xbool, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
            return analyzeBinarySimple(sema, fn, inst_idx, SemaType_Tag__uintword, SemaType_Tag__xbool, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__bits_not: return analyzeUnarySimple(sema, fn, inst_idx, SemaType_Tag__uintword, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__bool_not: return analyzeUnarySimple(sema, fn, inst_idx, SemaType_Tag__xbool, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__address_of: return analyzeAddressOf(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__deref_ref: return analyzeDerefRef(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__deref_val: return analyzeDerefVal(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__discard: return analyzeDiscard(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__placeholder_ref: return analyzePlaceholderRef(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__assign: return analyzeAssign(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__xunreachable: return analyzeXunreachable(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__break_void: return analyzeBreakVoid(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__break_value: return analyzeBreakValue(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__return_void: return analyzeReturnVoid(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__return_value: return analyzeReturnValue(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__call: return analyzeCall(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world: return analyzeCallBuiltinPrintHelloWorld(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__call_builtin_print_uint: return analyzeCallBuiltinPrintUint(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__truncate_int: return analyzeTruncateInt(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__promote_int: return analyzePromoteInt(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__ptr_cast: return analyzePtrCast(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__int_to_ptr: return analyzeIntToPtr(sema, fn, inst_idx, parent_block.unwrap());
        case Yy_Ir_Inst_Tag__ptr_to_int: return analyzePtrToInt(sema, fn, inst_idx, parent_block.unwrap());
    }
    yy_unreachable();
}

NODISCARD static YyStatus analyzeFnItemMany(
    Sema *const sema, Function *const fn, Yy_Ir_InstIndex parent_inst_idx, Yy_Ir_InstIndex const start, u32 const stop_after_nth)
{
    yy_assert(sema->gi->insts[parent_inst_idx].tag == Yy_Ir_Inst_Tag__block);
    yy_assert(start == parent_inst_idx + 1);
    yy_assert(genir_instEndOffset(sema->gi, parent_inst_idx) == stop_after_nth + 1);

    Yy_Ir_InstIndex iter_inst_idx = start;
    Option<Yy_Ir_InstIndex> prev_inst_idx = None;
    yy_assert(stop_after_nth > 0);
    Option<Yy_Ir_InstIndex> noreturn_inst_idx_opt = None;
    while (iter_inst_idx < start + stop_after_nth) {
        if (noreturn_inst_idx_opt.has_value()) {
            // The last instruction is implicit. This isn't an error.
            if (iter_inst_idx + 1 == start + stop_after_nth) {
                Yy_Ir_Inst_Tag const inst_tag = sema->gi->insts[iter_inst_idx].tag;
                yy_assert(instMayBeImplicitEndOfBlock(inst_tag));
                break;
            }
            // TODO: If noreturn_inst_idx is cond_branch or loop, and it was
            // evaluated at compile time, we shouldn't give an error.
            TRY(sema_new_soft_error(sema));
            sema_add_soft_error_msg(sema, errorLoc(iter_inst_idx), "unreachable code");
            if (prev_inst_idx.has_value())
                sema_add_error_note(sema, errorLoc(prev_inst_idx.unwrap()), "last reachable code was here");
            break;
        }
        TRY(analyzeFnItemOne(sema, fn, iter_inst_idx, parent_inst_idx));
        auto& sii = sema->inst_info_full_mut(iter_inst_idx);
        yy_assert(semaTypeIdIsValid(sii.type3));
        if (typeIsNoreturn(sema, sii.type3)) {
            noreturn_inst_idx_opt = iter_inst_idx;
        } else if (sema->gi->insts[iter_inst_idx].untyped_use_count == 0 && !typeIsVoid(sema, sii.type3)) {
            TRY(sema_new_soft_error(sema));
            ArrayList_Of_u8 msg_buf{sema->scratch.allocator()};
            array_list_printf(&msg_buf, "unused result of type '");
            fmtSemaTypeId(&msg_buf, sema, sii.type3);
            array_list_printf(&msg_buf, "'");
            sema_add_soft_error_msg(sema, errorLoc(iter_inst_idx), Sv_Fmt, Sv_Arg(msg_buf));
        }
        prev_inst_idx = iter_inst_idx;
        iter_inst_idx += genir_instEndOffset(sema->gi, iter_inst_idx);
    }
    if (noreturn_inst_idx_opt.is_none()) {
        // The block _must_ end with a noreturn instruction.
        sema_add_hard_error_msg(sema, errorLoc(parent_inst_idx), "internal compiler error: no break from block");
        return Yy_Bad;
    }
    auto const noreturn_inst_idx = noreturn_inst_idx_opt.unwrap();
    yy_assert(sema->inst_info_full_mut(noreturn_inst_idx).build_last_ref.is_none());
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeFnDecl(Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__fn_decl);
    yy_assert(genir_extraPl(gi, inst_idx, fn_decl)->decl_id == decl_id);
    Function fn = {
        .fn_decl_inst_idx = inst_idx,
    };

    Yy_Ir_InstIndex const body_inst_idx = genir_getFnDeclBodyInst(gi, inst_idx);
    TRY(analyzeFnItemOne(sema, &fn, body_inst_idx, None));

    // Assert that the body block is noreturn.
    yy_assert(gi->insts[body_inst_idx].tag == Yy_Ir_Inst_Tag__block);
    auto& body_sii = sema->inst_info_full_mut(body_inst_idx);
    TRY(expectType(sema, body_sii.type3, body_inst_idx, inst_idx, basicInternedType(sema, SemaType_Tag__xnoreturn)));

    return Yy_Ok;
}

NODISCARD static YyStatus analyzeStructDecl(Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__struct_decl);
    yy_assert(genir_extraPl(gi, inst_idx, struct_decl)->decl_id == decl_id);

    auto const type = sema->decl_comptime_values.get({}, decl_id).unwrap().value.as_type().unwrap();
    yy_unwrap_ok(makeStructMembers(sema, type, None));
    {
        auto strukt = typeBaseMut(sema, type)->get<SemaTypePacked::Struct>();
        auto _1 = sema->extra_pointers_lock.scoped_lock();
        auto _2 = sema->scratch.scoped_save();
        auto const resolved_members = strukt->resolved_members();
        auto map = NewerHashMap<IdentifierId, u32>{sema->scratch.allocator(), resolved_members.size()};
        for (u32 i = 0; i < resolved_members.size(); i++) {
            auto& member = resolved_members[i];
            if (!member.name.has_value()) {
                continue;
            }
            auto const name = member.name.unwrap();
            auto entry = map.get_or_put_entry({}, name);
            if (!entry.exists()) {
                std::move(entry).unwrap_put().put_value(i);
                continue;
            }
            auto const name_sv = genir_identifier_sv(gi, name);
            auto const ast_members =
                gi->inst_to_ast_map[inst_idx]->it.as_struct_decl().unwrap().members.nodes.span_const();
            auto const this_ast = ast_members[i];
            auto const other_ast = ast_members[entry.unwrap_get().value];
            sema_add_hard_error_msg(sema, errorLoc(this_ast), "redeclaration of struct field '" Sv_Fmt "'", Sv_Arg(name_sv));
            sema_add_error_note(sema, errorLoc(other_ast), "first field declared here");
            return Yy_Bad;
        }
    }

    // Do nothing
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeGlobalVarDecl(Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__global_var_decl);
    yy_assert(genir_extraPl(gi, inst_idx, global_var_decl)->decl_id == decl_id);

    // Do nothing
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeExternVarDecl(Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__extern_var_decl);
    yy_assert(genir_extraPl(gi, inst_idx, extern_var_decl)->decl_id == decl_id);

    // Do nothing
    return Yy_Ok;
}

NODISCARD static YyStatus analyzeExternFnDecl(Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__extern_fn_decl);
    yy_assert(genir_extraPl(gi, inst_idx, extern_fn_decl)->decl_id == decl_id);

    // Do nothing
    return Yy_Ok;
}


NODISCARD static YyStatus analyzeDeclOne(Sema *const sema, DeclId const decl)
{
    GenIr *const gi = sema->gi;
    auto decl_ptr = gi->decl_ptrs[decl];
    if (decl_ptr->has_sema_collect_hard_errors)
        return Yy_Bad;
    yy_assert(!decl_ptr->has_genir_hard_errors_todo_collect_vs_gen);
    auto inst_idx = decl_ptr->inst;
    Yy_Ir_Inst_Tag const inst_tag = gi->insts[inst_idx].tag;
    auto const inst_tag_sv = meta_enum_sv_short(inst_tag);
    logTrace("%s: $%u " Sv_Fmt "", __func__, inst_idx.x, Sv_Arg(inst_tag_sv));
    switch (inst_tag) {
        default:
            todo();
        case Yy_Ir_Inst_Tag__decl_error_skip: {
            return Yy_Bad;
        } break;
        case Yy_Ir_Inst_Tag__root: {
            // gi->decl_ptrs linearizes all decls. No need to recurse.
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__struct_decl:
            return analyzeStructDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__global_var_decl:
            return analyzeGlobalVarDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__extern_var_decl:
            return analyzeExternVarDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__fn_decl:
            return analyzeFnDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__extern_fn_decl:
            return analyzeExternFnDecl(sema, decl, inst_idx);
    }
    yy_unreachable();
}

NODISCARD static YyStatus analyzeAllDecls(Sema* const sema)
{
    auto gi = sema->gi;
    usize decl_count = gi->decl_ptrs.size();
    for (usize i = 0; i < decl_count; i++) {
        auto decl = DeclId(i);
        auto decl_ptr = gi->decl_ptrs[decl];
        if (yy_is_err(analyzeDeclOne(sema, decl))) {
            logDebug("sema: analyze decl failed: " DeclId_Fmt "", DeclId_Arg(decl));
            decl_ptr->has_sema_analyze_hard_errors = true;
            if (sema->errors.hard_count >= ERROR_LIMIT) {
                return Yy_Bad;
            }
        }
    }
    return Yy_Ok;
}

NODISCARD static YyStatus collectFnDecl(
    Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__fn_decl);

    Yy_Ir_Inst_ExtraPl_FnDecl const pl = *genir_extraPl(gi, inst_idx, fn_decl);
    yy_assert(pl.decl_id == decl_id);

    // Build the function argument type array.
    static_assert(sizeof(SemaTypeId) == sizeof(u32), "");
    auto fn_type_builder = SemaTypePacked::FunctionInit(sema->scratch.allocator(), pl.arg_count);
    for (u32 i = 0; i < pl.arg_count; i += 1) {
        Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo const arg = genir_getFnDeclArgData(gi, inst_idx, i);
        SemaTypeId arg_type = yy::uninitialized;
        TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, arg.type_eval_block), &arg_type));
        if (typeIsNoreturn(sema, arg_type)) {
            auto arg_inst_idx = genir_getFnDeclArgDeclInst(gi, inst_idx, i);
            sema_add_hard_error_msg(sema, errorLoc(arg_inst_idx), "function parameter can't be of type 'noreturn'");
            return Yy_Bad;
        }
        fn_type_builder.set_arg_type(i, arg_type);
    }

    // Build the return type
    Yy_Ir_InstIndex const return_type_block = genir_getFnDeclReturnTypeBlockInst(gi, inst_idx);
    SemaTypeId return_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, return_type_block, &return_type));

    // Build the function type.
    auto tmp_fn_type = fn_type_builder.finish(return_type);
    SemaTypeId const fn_type = encodeTypeMaybeIntern(sema, *tmp_fn_type);
    fn_type_builder.deinit();

    make_inst_info_global_decl_full(sema, inst_idx, fn_type);
    return Yy_Ok;
}

NODISCARD static YyStatus collectExternFnDecl(
    Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__extern_fn_decl);

    Yy_Ir_Inst_ExtraPl_ExternFnDecl const pl = *genir_extraPl(gi, inst_idx, extern_fn_decl);
    yy_assert(pl.decl_id == decl_id);

    // Build the function argument type array.
    static_assert(sizeof(SemaTypeId) == sizeof(u32), "");
    auto fn_type_builder = SemaTypePacked::FunctionInit(sema->scratch.allocator(), pl.arg_count);
    for (u32 i = 0; i < pl.arg_count; i += 1) {
        Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo const arg = genir_getExternFnDeclArgData(gi, inst_idx, i);
        SemaTypeId arg_type = yy::uninitialized;
        TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, arg.type_eval_block), &arg_type));
        if (typeIsNoreturn(sema, arg_type)) {
            // TODO: The error should point to the declaration of the parameter.
            sema_add_hard_error_msg(sema, errorLoc(inst_idx),
                                    "extern function parameter can't be of type 'noreturn'");
            return Yy_Bad;
        } else if (typeIsVoid(sema, arg_type)) {
            // TODO: The error should point to the declaration of the parameter.
            sema_add_hard_error_msg(sema, errorLoc(inst_idx),
                                    "extern function parameter can't be of type 'void'");
            return Yy_Bad;
        }
        fn_type_builder.set_arg_type(i, arg_type);
    }

    // Build the return type
    Yy_Ir_InstIndex const return_type_block = genir_getExternFnDeclReturnTypeBlockInst(gi, inst_idx);
    SemaTypeId return_type = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, return_type_block, &return_type));

    // Build the function type.
    auto tmp_fn_type = fn_type_builder.finish(return_type);
    SemaTypeId const fn_type = encodeTypeMaybeIntern(sema, *tmp_fn_type);
    fn_type_builder.deinit();

    make_inst_info_global_decl_full(sema, inst_idx, fn_type);
    return Yy_Ok;
}

NODISCARD static YyStatus collectStructDecl(
    Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr* const gi = sema->gi;

    auto const pl = *genir_extraPl(gi, inst_idx, struct_decl);
    yy_assert(pl.decl_id == decl_id);
    // TODO: don't use decl id for the gen id.
    u32 const struct_gen_id = yy::checked_int_cast<u32>(decl_id.x).unwrap();
    auto struct_builder =
        SemaTypePacked::StructInit(sema->scratch.allocator(), decl_id, inst_idx, struct_gen_id, pl.member_count);
    defer { struct_builder.deinit(); };
    auto const struct_type_id = encodeTypeMaybeIntern(sema, *struct_builder.finish());
    auto const struct_type_value = MetaTu_init(ComptimeValue, type, struct_type_id);
    sema->decl_comptime_values.put_no_clobber_emplace({}, decl_id, struct_type_value);
    make_inst_info_global_decl_full(sema, inst_idx, basicInternedType(sema, SemaType_Tag__type));
    return Yy_Ok;
}

NODISCARD static YyStatus collectGlobalVarDecl(
    Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    yy_assert(gi->insts[inst_idx].tag == Yy_Ir_Inst_Tag__global_var_decl);

    auto const pl = *genir_extraPl(gi, inst_idx, global_var_decl);
    yy_assert(pl.decl_id == decl_id);
    auto const init_block = genir_instAbsSF(inst_idx, pl.init_block);

    // TODO: If the type was explicit, delay resolving the initial value until analyzeGlobalVarDecl.
    InternedComptimeValueId resolved_init_value = yy::uninitialized;
    TRY(semaEvalBlockHead(sema, init_block, &resolved_init_value));
    auto const init_type_id = comptimeValueSemaType(sema, &sema->comptime_value_pool[resolved_init_value]);

    SemaTypeId the_type_id = yy::uninitialized;
    if (pl.type_block.is_none()) {
        // Inferred type
        the_type_id = init_type_id;
        if (typeIsUndefined(sema, init_type_id)) {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "global variable with inferred type can't have value 'undefined'. yuo should give an explicit type.");
            return Yy_Bad;
        }
    } else {
        // Explicit variable type
        auto type_block_abs = pl.type_block.unwrap().abs(inst_idx);
        TRY(semaEvalBlockAsType(sema, type_block_abs, &the_type_id));
        if (typeIsNoreturn(sema, the_type_id)) {
            sema_add_hard_error_msg(sema, errorLoc(inst_idx), "global variable can't be of type 'noreturn'");
            return Yy_Bad;
        }
    }

    if (!typeIsUndefined(sema, init_type_id)) {
        TRY(expectType(sema, init_type_id, init_block, inst_idx, the_type_id));
    }

    if (typeIsType(sema, the_type_id)) {
        sema_add_hard_error_msg(sema, errorLoc(inst_idx), "global variable can't be of type 'type'");
        return Yy_Bad;
    }
    make_inst_info_global_var_decl_full(sema, inst_idx, the_type_id, resolved_init_value);
    return Yy_Ok;
}

NODISCARD static YyStatus collectExternVarDecl(
    Sema *const sema, DeclId const decl_id, Yy_Ir_InstIndex const inst_idx)
{
    GenIr *const gi = sema->gi;
    Yy_Ir_Inst const inst = gi->insts[inst_idx];
    yy_assert(inst.tag == Yy_Ir_Inst_Tag__extern_var_decl);

    auto const pl = *genir_extraPl(gi, inst_idx, extern_var_decl);
    yy_assert(pl.decl_id == decl_id);

    SemaTypeId the_type_id = yy::uninitialized;
    TRY(semaEvalBlockAsType(sema, genir_instAbsSF(inst_idx, pl.type_block), &the_type_id));
    SemaType_Tag const the_type_tag = decodeTypeTag(sema, the_type_id);
    switch (the_type_tag) {
        default:
        {} break;
        case SemaType_Tag__xnoreturn:
        case SemaType_Tag__xvoid:
            sema_add_hard_error_msg(sema, errorLoc(inst_idx),
                                    "extern variable can't be of type '" Sv_Fmt "'",
                                    Sv_Arg(meta_enum_sv_short(the_type_tag)));
            return Yy_Bad;
    }

    make_inst_info_global_decl_full(sema, inst_idx, the_type_id, None);
    return Yy_Ok;
}

NODISCARD static YyStatus collectDeclOne(Sema *const sema, DeclId const decl)
{
    GenIr *const gi = sema->gi;
    auto decl_ptr = gi->decl_ptrs[decl];
    if (decl_ptr->has_genir_hard_errors_todo_collect_vs_gen) {
        return Yy_Bad;
    }
    auto inst_idx = decl_ptr->inst;
    Yy_Ir_Inst_Tag const inst_tag = gi->insts[inst_idx].tag;
    logTrace("%s: $%u " Sv_Fmt "", __func__, inst_idx.x, Sv_Arg(meta_enum_sv_short(inst_tag)));
    switch (inst_tag) {
        default:
            todo();
        case Yy_Ir_Inst_Tag__decl_error_skip:
            return Yy_Bad;
        case Yy_Ir_Inst_Tag__root: {
            // gi->decl_ptrs linearizes all decls. No need to recurse.
            return Yy_Ok;
        } break;
        case Yy_Ir_Inst_Tag__struct_decl:
            return collectStructDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__global_var_decl:
            return collectGlobalVarDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__extern_var_decl:
            return collectExternVarDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__fn_decl:
            return collectFnDecl(sema, decl, inst_idx);
        case Yy_Ir_Inst_Tag__extern_fn_decl:
            return collectExternFnDecl(sema, decl, inst_idx);
    }
    yy_unreachable();
}

NODISCARD static YyStatus collectAllDecls(Sema* const sema)
{
    auto gi = sema->gi;
    usize decl_count = gi->decl_ptrs.size();
    for (usize i = 0; i < decl_count; i++) {
        auto decl = DeclId(i);
        auto decl_ptr = gi->decl_ptrs[decl];
        if (yy_is_err(collectDeclOne(sema, decl))) {
            logDebug("sema: collect decl failed: " DeclId_Fmt "", DeclId_Arg(decl));
            decl_ptr->has_sema_collect_hard_errors = true;
            if (sema->errors.hard_count >= ERROR_LIMIT) {
                return Yy_Bad;
            }
        }
    }
    return Yy_Ok;
}


Sema::Sema(Yy_Allocator heap, GenIr* gi)
    : gi(gi),
      heap(heap),
      arena{heap, 0},
      scratch{heap, 0},
      errors{heap},
      extra{},
      sema_inst_infos{},       // init below
      basic_interned_types{},  // init below
      types(heap),
      comptime_value_pool(heap),
      decl_comptime_values(heap),
      target_info{
          .ptr_size_align = {.size_of = 8, .align_of = 8},
          .uintword_size_align = {.size_of = 8, .align_of = 8},
      }
{
    this->sema_inst_infos =
        allocator_new_span_default_construct<SemaInstInfoNew>(this->arena.allocator(), gi->insts.size());

    {
        // Reserve the index 0.
        SemaExtraIndex const unused = semaAllocExtra(this, 1);
        (void)unused;
    }

    semaInitBasicInternedTypes(this);
    init_comptime_value_pool(this);
}

Sema::~Sema() {
    std::destroy(this->sema_inst_infos.begin(), this->sema_inst_infos.end());
}

NODISCARD auto yy_sema_analyze_ir(Yy_Allocator const heap, GenIr *const gi) -> Sema
{
    Sema result{heap, gi};
    auto ret_sema = &result;

    {
        Yy_Ir_InstIndex const top_root_inst_idx{0};
        Yy_Ir_Inst const top_root_inst = gi->insts[top_root_inst_idx];
        yy_assert(top_root_inst.tag == Yy_Ir_Inst_Tag__root);
        if (collectAllDecls(ret_sema).is_err()) goto final_error;
        if (analyzeAllDecls(ret_sema).is_err()) goto final_error;
    }

    // Stop even on soft errors.
    if (ret_sema->has_errors()) {
        logDebug("" Sv_Fmt ": error: yy_sema_analyze_ir() failed with %zu hard, %zu soft errors",
                 Sv_Arg(ret_sema->gi->filename), ret_sema->errors.hard_count, ret_sema->errors.soft_count);
        goto final_error;
    }

    yy_assert(ret_sema->errors.hard_count == 0);
    yy_assert(ret_sema->errors.msgs.size() == 0);

    if (verbose_sema) {
        genir_debugPrint(ret_sema->gi, ret_sema);
        fprintf(stderr, "sema info:\n");
        fprintf(stderr, "| sizeof(SemaInstInfoNew): %zu\n", sizeof(SemaInstInfoNew));
        fprintf(stderr, "| extra: [%zu]u32\n", ret_sema->extra.size());
        fprintf(stderr, "| type count: %zu\n", ret_sema->types.size());
        fprintf(stderr, "| comptime_value_pool size: %zu\n", ret_sema->comptime_value_pool.size());
        fprintf(stderr, "| sizeof(ComptimeValue): %zu\n", sizeof(ComptimeValue));

        usize arena_size = 0;
        for (Yy_ArenaBlock* iter = ret_sema->arena.a.blocks; iter != NULL; iter = iter->prev) {
            arena_size += iter->idx;
        }

        auto memory_usage = MemoryUnit::from_bytes(
            arena_size +
            sizeof(u32) * ret_sema->extra.size() +
            sizeof(SemaTypeId) * (ret_sema->types.size()) +
            (usize)((double)sizeof(SemaTypeTable::Bucket) * (double)(ret_sema->types.size()) / (double)ret_sema->types.load_factor()) +
            ret_sema->comptime_value_pool.size() * sizeof(ComptimeValue)
        );
        fprintf(stderr, "sema memory usage: " MemoryUnit_Fmt "\n", MemoryUnit_Arg(memory_usage));
        fprintf(stderr, "\n");
    }

    yy_assert(!result.errors.fatal);
    return result;
final_error:
    result.errors.fatal = true;
    return result;
}
