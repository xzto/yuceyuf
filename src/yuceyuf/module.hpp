#ifndef ___INCLUDE_GUARD__ItBneOieXBzUmND8W1mGqpI2RcrZgnVGFlVqOvDE
#define ___INCLUDE_GUARD__ItBneOieXBzUmND8W1mGqpI2RcrZgnVGFlVqOvDE


#include "yy/basic.hpp"
#include "yuceyuf/ast.hpp"

typedef struct {
    Yy_Arena *arena;
    ArrayList(Yuceyuf_Ast_File *) files;
    Yuceyuf_ErrorMessageList errors;

    /// If no stdlib is present, this is {0}. If stdlib path was given, this is the canonical
    /// path to `std.yuceyuf`.
    Sv OPT canonical_stdlib_path;
    /// If no stdlib is present, this is always NULL. If stdlib path was given, it will be set
    /// on the first import.
    Yuceyuf_Ast_File *OPT stdlib_ast_file;
} Yuceyuf_Module;


typedef struct {
    bool verbose_tokens;
    bool stop_at_tokens;
    bool verbose_ast;
    bool stop_at_ast;
    bool verbose_timings;
} Yuceyuf_ModuleFromFilenameOptions;

NODISCARD YyStatus yuceyuf_module_from_filename(
    Yy_Arena *arena,
    Sv root_filename_not_canonical,
    Sv OPT stdlib_path_not_canonical,
    Yuceyuf_ModuleFromFilenameOptions const *options,
    Yuceyuf_Module *result_module
);



#endif//___INCLUDE_GUARD__ItBneOieXBzUmND8W1mGqpI2RcrZgnVGFlVqOvDE
