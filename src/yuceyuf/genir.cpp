#include "yy/basic.hpp"
#include "yy/array_list_printf.hpp"
#include "yy/newer_hash_map.hpp"
#include "yy/timer.hpp"
#include "yy/misc.hpp"

#include "yuceyuf/genir.hpp"
#include "yuceyuf/sema.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>

#define LOG_LAP(t, lap, name) do {                                                          \
    lap = timer_lap(t);                                                                     \
    if (verbose_timings)                                                                    \
        fprintf(stderr, "LAP:\t%s:\t%s\t" TimeMs_Fmt "\n", __func__, name, TimeMs_Arg(lap));  \
} while (false)

using InstIndex = Yy_Ir_InstIndex;
using Inst = Yy_Ir_Inst;
using Inst_Tag = Yy_Ir_Inst_Tag;
using StringIndex = Yy_Ir_StringIndex;
using ExtraIndex = Yy_Ir_ExtraIndex;
using Extra = Yy_Ir_Extra;

#define genir_ice() yy_panic("Internal Compiler Error: this is a bug in the yuceyuf compiler")


#define Inst_init(name, ...) \
    (Inst { .tag = Yy_Ir_Inst_Tag__##name, .untyped_use_count = 0, .small_data = { .name = __VA_ARGS__ } })

#define genir_reserveExtraPl(gi, pl_name) \
    genir_reserveExtra(gi, sizeof(decltype(Yy_Ir_Inst_DEBUG_FullUnionOfPayloads::pl_name))/sizeof(Extra))
#define genir_reserveExtraArrayPl(gi, count, pl_name) \
    genir_reserveExtra(gi, count * (sizeof(decltype(Yy_Ir_Inst_DEBUG_FullUnionOfPayloads::pl_name))/sizeof(Extra)))

template <ir::extra_pl Pl>
auto genir_unreserveExtraPl(GenIr* gi, ExtraIndex reserved_idx, Pl const* extra_pl_ptr) -> void {
    genir_unreserveExtra(gi, reserved_idx, reinterpret_cast<Extra const*>(extra_pl_ptr), sizeof(Pl) / sizeof(Extra));
}

static void genir_unreserveExtraArray(GenIr* gi, ExtraIndex reserved_idx, u32 elem_idx, Extra const* data_ptr, u32 data_len);

template <ir::extra_pl Pl>
auto genir_unreserveExtraArrayPl(GenIr* gi, ExtraIndex reserved_idx, u32 array_idx, Pl const* extra_pl_ptr) -> void {
    genir_unreserveExtraArray(
        gi, reserved_idx, array_idx, reinterpret_cast<Extra const*>(extra_pl_ptr), sizeof(Pl) / sizeof(Extra)
    );
}


using AstNode = Yuceyuf_AstNode;



// TODO: Checking for unused things and side effects.
//
// ```
//  const Flags = packed struct {
//      /// Whether it got referenced by another instruction (TODO make sure not to forget to
//      /// set this to true for implicit dependencies like in `fn_decl`'s).
//      /// TODO: Make sure that infinite loops don't get optimized away.
//      /// TODO: Experiment with making stores to memory not produce any side effect.
//      /// TODO: Experiment with removing unneeded instructions.
//      /// How to set it:
//      /// 1. loop backwards on the list of instructions (per file) (it's faster, but forwards works too)
//      /// 2. if the current instruction references another (`other`) instruction, do
//      ///    `other.referenced = max(other.referenced, current.referenced)`
//      /// 3. continue
//      /// I need to differentiate between `auto` and `yes` if only for compile errors for unused things (decls).
//      referenced: enum { no, auto, yes },
//      /// Either this instruction itself produces a side effect, or this value is referenced
//      /// by an instruction that produces a side effect. If this instruction produces no side
//      /// effect and no one references it, it doesn't have to be codegen'd.
//      needed_for_other_side_effect: bool,
//  };
// ```
// `Flags` would be inside every `Inst`. I would only analyze it.

static u32 intCastFromUsizeToU32(usize x_usize)
{
    u32 result;
    yy_unwrap_ok(yy_try_int_cast(x_usize, &result));
    return result;
}


/// Log a single line
PRINTF_LIKE(1, 2)
static void logDebug(char const *fmt, ...)
{
    if (verbose_genir) {
        va_list ap;
        va_start(ap, fmt);
        fprintf(stderr, "gen_ir(debug): ");
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "\n");
        va_end(ap);
    }
}

/// Log a single line
PRINTF_LIKE(1, 2)
static void logTrace(char const *fmt, ...)
{
    if ((false)) {
        va_list ap;
        va_start(ap, fmt);
        fprintf(stderr, "gen_ir(debug): ");
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, "\n");
        va_end(ap);
    }
}

/// Log a single line
PRINTF_LIKE(1, 2)
static void logError(char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "gen_ir(error): ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    va_end(ap);
}

COLD static void genir_add_err_msg__(
    Yuceyuf_ErrorMessageKind const kind,
    GenIr *gi, Genir_ErrorLocation const loc, char const *fmt, va_list ap)
{
    Yuceyuf_AstNode* ast = yy::uninitialized;
    switch (loc.tag) {
        case Genir_ErrorLocation_Tag__ast: {
            ast = *MetaTu_get(Genir_ErrorLocation, &loc, ast);
        } break;
        case Genir_ErrorLocation_Tag__inst: {
            ast = gi->inst_to_ast_map[*MetaTu_get(Genir_ErrorLocation, &loc, inst)];
        } break;
        default:
            yy_unreachable();
    }
    auto const& lex = ast->parent_ast_file->lexer.lex;
    auto const src_region = ast->src.to_src_region_line_and_column(lex);
    gi->errors.make_error_va(kind, gi->filename, src_region, fmt, ap);
}

COLD PRINTF_LIKE(3, 4)
static void genir_add_hard_error_msg(GenIr *gi, Genir_ErrorLocation const loc, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    genir_add_err_msg__(Yuceyuf_ErrorMessageKind::hard_error, gi, loc, fmt, ap);
    va_end(ap);
}
COLD PRINTF_LIKE(3, 4)
static void genir_add_soft_error_msg(GenIr *gi, Genir_ErrorLocation const loc, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    genir_add_err_msg__(Yuceyuf_ErrorMessageKind::soft_error, gi, loc, fmt, ap);
    va_end(ap);
}

COLD PRINTF_LIKE(3, 4)
static void genir_add_error_note(GenIr *gi, Genir_ErrorLocation const loc, char const *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    genir_add_err_msg__(Yuceyuf_ErrorMessageKind::note, gi, loc, fmt, ap);
    va_end(ap);
}


/// Try to add a new error and keep compilation going unless there are too many errors already.
/// Soft errors: it can continue compiling to find more errors, but it will fail at the end.
/// Hard errors: it must stop compilation of this top level declaration.
/// (TODO) Fatal errors: It must stop all compilation of all items in the queue. This will be useful
/// when I add a proper queue of multiple files.
COLD NODISCARD static YyStatus genir_new_soft_error(GenIr *const gi)
{
    if (gi->errors.msgs.size() >= ERROR_LIMIT) {
        gi->errors.hard_count += 1;
        return Yy_Bad;
    }
    return Yy_Ok;
}


NODISCARD auto genir_identifier_sv(GenIr* const gi, IdentifierId id) -> Sv {
    return gi->ast_file->lexer.lex.global->identifier_sv(id);
}


struct Scope_Base;
struct Scope_Root;
struct Scope_Head;

struct ScopeSymbolValue {
    Yuceyuf_AstNode *ast;
    enum class Tag : u8 { inst, decl };
    union {
        InstIndex inst;
        DeclId decl;
    } as;
    Tag tag;

    bool referenced;
    bool is_ambiguous;
    /// mod_scope is owned
    Option<Scope_Root*> mod_scope;
};


using ScopeSymbols = yy::NewerHashMap<IdentifierId, ScopeSymbolValue>;


#define YY_META_ENUM__NAME Scope_Base_Tag
#define YY_META_ENUM__ALL_MEMBERS \
     YY_META_ENUM__MEMBER(root) \
     YY_META_ENUM__MEMBER(head) \
     YY_META_ENUM__MEMBER(block)
#include "yy/meta/enum.hpp"
struct Scope_Base {
    Scope_Base_Tag tag;
    Option<Scope_Base*> parent;
    Yuceyuf_AstNode* node_loc;

    auto closest_head() -> Scope_Head*;

    struct iterator {
        Option<Scope_Base*> it;
        auto operator==(iterator const&) const -> bool = default;
        auto operator++() -> iterator& {
            it = it.unwrap()->parent;
            return *this;
        }
        auto operator*() -> Scope_Base* { return it.unwrap(); }
    };
    auto begin() -> iterator { return { this }; }
    auto end() -> iterator { (void)this; return { None }; }
};

#define scope_get_checked(base_ptr, the_tag)                       \
    (yy_assert((base_ptr)->tag == Scope_Base_Tag__##the_tag), \
     FIELD_PARENT_POINTER(JOIN2(Scope__typedef_, the_tag), base, base_ptr))

struct Scope_Root : yy::NoCopyNoMove {
    Scope_Base base;
    ScopeSymbols symbols;
    // list of insertion idxs which contain mod_scope.
    VecUnmanaged<u32> mod_decls_to_free{};
    bool has_error_decls{false};

    Scope_Root(Option<Scope_Base*> parent, AstNode* node_loc, Yy_Allocator heap)
        : base({
              .tag = Scope_Base_Tag__root,
              .parent = parent,
              .node_loc = node_loc,
          }),
          symbols(heap) {}
};
typedef Scope_Root Scope__typedef_root;

struct Scope_Head {
    Scope_Base base;
    ScopeSymbols symbols;
    // This instruction is reserved and will be unreserved just before scopeDestroy is called.
    // fn_decl instruction or a block
    InstIndex head_reserved_inst;
    bool is_function;
    u32 return_or_break_count_upper_limit;
};
typedef Scope_Head Scope__typedef_head;

struct Scope_Block {
    Scope_Base base;
    Scope_Head *head;
    // This instruction is reserved and will be unreserved just before scopeDestroy is called.
    InstIndex block_like_reserved_inst;
    u32 break_count_upper_limit{0};
    u32 scope_stack_size;
    Option<IdentifierId> named_label;
    Inst_Tag inst_tag;
    /// the Scope_Block of the while loop body. when breaking to it, the continue_stmt of the while loop is immediately after it.
    Option<Scope_Block*> loop_body_scope{None};
};
typedef Scope_Block Scope__typedef_block;

auto Scope_Base::closest_head() -> Scope_Head* {
    switch (tag) {
        case MetaEnum_count(Scope_Base_Tag):
            yy_unreachable();
        case Scope_Base_Tag__root: {
            genir_ice();
        } break;
        case Scope_Base_Tag__block:
            return scope_get_checked(this, block)->head;
        case Scope_Base_Tag__head:
            return scope_get_checked(this, head);
    }
    yy_unreachable();
}


NODISCARD static bool scopeWantsUnusedDeclErrors(Scope_Base_Tag const tag)
{
    switch (tag) {
        case MetaEnum_count(Scope_Base_Tag): yy_unreachable();
        case Scope_Base_Tag__root: return false;
        case Scope_Base_Tag__head: return true;
        case Scope_Base_Tag__block: return true;
    }
    yy_unreachable();
}

NODISCARD static bool scopeIsAst(Scope_Base_Tag const tag)
{
    switch (tag) {
        case MetaEnum_count(Scope_Base_Tag): yy_unreachable();
        case Scope_Base_Tag__root: return true;
        case Scope_Base_Tag__head: return false;
        case Scope_Base_Tag__block: return false;
    }
    yy_unreachable();
}

NODISCARD static bool scopeHasRefPatches(Scope_Base_Tag const tag)
{
    return scopeIsAst(tag);
}

NODISCARD static Scope_Root* scopeCreateTopRoot(GenIr *const gi, Yuceyuf_AstNode* node_loc)
{
    gi->debug_root_scope_create_count += 1;
    Scope_Root* const result = allocator_new<Scope_Root>(gi->arena.allocator(), None, node_loc, gi->heap);
    return result;
}
NODISCARD static Scope_Root *scopeCreateMod(GenIr *const gi, Scope_Root *const parent, Yuceyuf_AstNode* node_loc)
{
    gi->debug_root_scope_create_count += 1;
    Scope_Root* const result = allocator_new<Scope_Root>(gi->arena.allocator(), &parent->base, node_loc, gi->heap);
    return result;
}
NODISCARD static Scope_Head scopeCreateHeadActualFunction(GenIr *const gi, Scope_Root *const parent, Yuceyuf_AstNode* node_loc, InstIndex const fn_decl_reserved_inst)
{
    yy_assert(parent != NULL);
    Scope_Head result = {
        .base = {
            .tag = Scope_Base_Tag__head,
            .parent = &parent->base,
            .node_loc = node_loc,
        },
        .symbols = ScopeSymbols{gi->heap},
        .head_reserved_inst = fn_decl_reserved_inst,
        .is_function = true,
        .return_or_break_count_upper_limit = 0,
    };
    return result;
}
NODISCARD static Scope_Head scopeCreateHead(GenIr *const gi, Scope_Base *const parent, Yuceyuf_AstNode* node_loc, InstIndex const head_reserved_inst)
{
    yy_assert(parent != NULL);
    Scope_Head result = {
        .base = {
            .tag = Scope_Base_Tag__head,
            .parent = parent,
            .node_loc = node_loc,
        },
        .symbols = ScopeSymbols{gi->heap},
        .head_reserved_inst = head_reserved_inst,
        .is_function = false,
        .return_or_break_count_upper_limit = 0,
    };
    return result;
}
NODISCARD static Scope_Block scopeCreateBlock(GenIr *const gi, Scope_Base *const parent, Yuceyuf_AstNode* node_loc, InstIndex const block_like_inst, Inst_Tag inst_tag)
{
    (void) gi;
    yy_assert(parent != NULL);
    Scope_Head *head = parent->closest_head();
    Scope_Block result = {
        .base = {
            .tag = Scope_Base_Tag__block,
            .parent = parent,
            .node_loc = node_loc,
        },
        .head = head,
        .block_like_reserved_inst = block_like_inst,
        .scope_stack_size = intCastFromUsizeToU32(head->symbols.size()),
        .named_label = None,
        .inst_tag = inst_tag,
    };
    return result;
}
NODISCARD static Option<Scope_Base*> scopeTryLookupMetaNamedLabel(Scope_Base *const scope, IdentifierId const label);
NODISCARD static Option<Scope_Base*> scopeTryLookupMetaLoop(Scope_Base *const scope);
NODISCARD static YyStatus scopeCreateLabeledBlock(GenIr *const gi, Scope_Base *const parent, Yuceyuf_AstNode* node_loc, InstIndex const block_like_inst, Inst_Tag inst_tag, IdentifierId label, Scope_Block* ret_result)
{
    yy_assert(parent != NULL);
    Scope_Head *head = parent->closest_head();
    auto existing_label = scopeTryLookupMetaNamedLabel(parent, label);
    if (existing_label.has_value()) {
        // shadowing: error
        Sv const name_sv = genir_identifier_sv(gi, label);
        genir_add_hard_error_msg(
            gi, MetaTu_init(Genir_ErrorLocation, ast, node_loc),
            "redeclaration of label '" Sv_Fmt "'", Sv_Arg(name_sv));
        genir_add_error_note(
            gi, MetaTu_init(Genir_ErrorLocation, ast, existing_label.unwrap()->node_loc),
            "label '" Sv_Fmt "' declared here", Sv_Arg(name_sv));
        return Yy_Bad;
    }
    *ret_result = Scope_Block{
        .base = {
            .tag = Scope_Base_Tag__block,
            .parent = parent,
            .node_loc = node_loc,
        },
        .head = head,
        .block_like_reserved_inst = block_like_inst,
        .scope_stack_size = intCastFromUsizeToU32(head->symbols.size()),
        .named_label = label,
        .inst_tag = inst_tag,
    };
    return Yy_Ok;
}
NODISCARD static Scope_Block scopeCreateCondBranch(GenIr *const gi, Scope_Base *const parent, Yuceyuf_AstNode* node_loc, InstIndex const block_inst)
{
    return scopeCreateBlock(gi, parent, node_loc, block_inst, Yy_Ir_Inst_Tag__cond_branch);
}
NODISCARD static Scope_Block scopeCreateLoop(GenIr *const gi, Scope_Base *const parent, Yuceyuf_AstNode* node_loc, InstIndex const block_inst)
{
    return scopeCreateBlock(gi, parent, node_loc, block_inst, Yy_Ir_Inst_Tag__loop);
}

typedef struct {
    bool found;
    Scope_Base *scope;
    u32 ss_insertion_idx;
    ScopeSymbolValue *ss_value_ptr;
} ScopeLookupDeclResult;

NODISCARD static ScopeLookupDeclResult scopeLookupDeclAdvanced(Scope_Base *const scope, IdentifierId const name, bool is_direct)
{
    for (Option<Scope_Base*> iter1234 = scope; iter1234.has_value(); ) {
        auto const iter = iter1234.unwrap();
        if (is_direct && iter != scope)
            break;
        switch (iter->tag) {
            case MetaEnum_count(Scope_Base_Tag):
                yy_unreachable();
            case Scope_Base_Tag__root: {
                Scope_Root *as_root = scope_get_checked(iter, root);
                auto const get_opt = as_root->symbols.get({}, name);
                if (get_opt.has_value()) {
                    auto& get = get_opt.unwrap();
                    get.value.referenced = true;
                    return ScopeLookupDeclResult {
                        .found = true,
                        .scope = iter,
                        .ss_insertion_idx = intCastFromUsizeToU32(get.insertion_idx),
                        .ss_value_ptr = &get.value,
                    };
                }
                iter1234 = iter->parent;
            } break;
            case Scope_Base_Tag__block: {
                Scope_Block *as_block = scope_get_checked(iter, block);
                auto const get_opt = as_block->head->symbols.get({}, name);
                if (get_opt.has_value()) {
                    auto& get = get_opt.unwrap();
                    get.value.referenced = true;
                    return ScopeLookupDeclResult {
                        .found = true,
                        .scope = iter,
                        .ss_insertion_idx = intCastFromUsizeToU32(get.insertion_idx),
                        .ss_value_ptr = &get.value,
                    };
                }
                iter1234 = as_block->head->base.parent;
            } break;
            case Scope_Base_Tag__head: {
                Scope_Head *as_head = scope_get_checked(iter, head);
                auto const get_opt = as_head->symbols.get({}, name);
                if (get_opt.has_value()) {
                    auto& get = get_opt.unwrap();
                    get.value.referenced = true;
                    return ScopeLookupDeclResult {
                        .found = true,
                        .scope = iter,
                        .ss_insertion_idx = intCastFromUsizeToU32(get.insertion_idx),
                        .ss_value_ptr = &get.value,
                    };
                }
                iter1234 = iter->parent;
            }
            break;
        }
    }
    ScopeLookupDeclResult result;
    yy_debug_invalidate_ptr_one(&result);
    result.found = false;
    return result;
}

NODISCARD static YyStatus resolveIdentifier(GenIr *const gi, Scope_Base *const scope, AstNode *const node_loc, IdentifierId const name, ScopeLookupDeclResult *const ret_result)
{
    ScopeLookupDeclResult const lookup = scopeLookupDeclAdvanced(scope, name, false);
    if (!lookup.found) {
        Sv const name_sv = genir_identifier_sv(gi, name);
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node_loc), "undeclared identifier '" Sv_Fmt "'", Sv_Arg(name_sv));
        return Yy_Bad;
    }
    if (lookup.ss_value_ptr->is_ambiguous) {
        yy_assert(lookup.scope->tag == Scope_Base_Tag__root);
        Sv const name_sv = genir_identifier_sv(gi, name);
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node_loc), "ambigous identifier '" Sv_Fmt "'", Sv_Arg(name_sv));
        // TODO: show the other declaration too
        genir_add_error_note(gi, MetaTu_init(Genir_ErrorLocation, ast, lookup.ss_value_ptr->ast), "declared here");
        genir_add_error_note(gi, MetaTu_init(Genir_ErrorLocation, ast, lookup.ss_value_ptr->ast), "consider using a fully qualified name");
        return Yy_Bad;
    }
    *ret_result = lookup;
    return Yy_Ok;
}

NODISCARD static ScopeLookupDeclResult scopeLookupDeclDirect(Scope_Base *const scope, IdentifierId const name)
{
    return scopeLookupDeclAdvanced(scope, name, true);
}

static void setDeclInst(GenIr *const gi, DeclId const decl_id, InstIndex const inst)
{
    gi->decl_ptrs[decl_id]->inst = inst;
}


NODISCARD static YyStatus scopeDestroy(GenIr *const gi, Scope_Base *const scope)
{
    yy_assert(scope->node_loc != NULL);

    if (scopeWantsUnusedDeclErrors(scope->tag)) {
        yy_assert(!scopeIsAst(scope->tag));
        ScopeSymbols *symbols = yy::uninitialized;
        usize symbols_start = 0;
        switch (scope->tag) {
            case MetaEnum_count(Scope_Base_Tag):
            case Scope_Base_Tag__root:
                yy_unreachable();
                break;
            case Scope_Base_Tag__head: {
                symbols = &scope_get_checked(scope, head)->symbols;
                symbols_start = 0;
            } break;
            case Scope_Base_Tag__block: {
                auto block = scope_get_checked(scope, block);
                symbols = &block->head->symbols;
                symbols_start = block->scope_stack_size;
            } break;
        }
        for (usize i = symbols_start; i < symbols->size(); i += 1) {
            auto get = symbols->get_by_index(i);
            if (get.value.referenced)
                continue;
            IdentifierId const name = get.key;
            yy_assert(get.value.tag == ScopeSymbolValue::Tag::inst);
            InstIndex const unused_inst = get.value.as.inst;
            Sv const name_sv = genir_identifier_sv(gi, name);
            // Soft error
            if (yy_is_err(genir_new_soft_error(gi)))
                break;
            genir_add_soft_error_msg(gi, MetaTu_init(Genir_ErrorLocation, inst, unused_inst), "unused local `" Sv_Fmt "`", Sv_Arg(name_sv));
        }
    }

    switch (scope->tag) {
        case MetaEnum_count(Scope_Base_Tag): yy_unreachable(); break;
        case Scope_Base_Tag__root: {
            gi->debug_root_scope_destroy_count += 1;
            yy_assert(scopeHasRefPatches(scope->tag));
            Scope_Root *const root = scope_get_checked(scope, root);

            // destroy mod decls.
            for (auto to_free: root->mod_decls_to_free) {
                ScopeSymbolValue const *const value = &root->symbols.get_by_index(to_free).value;
                yy_assert(value->mod_scope.has_value());
                auto mod_scope = value->mod_scope.unwrap();
                TRY(scopeDestroy(gi, &mod_scope->base)); // TODO leak on compiler error
                allocator_delete(gi->arena.allocator(), mod_scope);
            }

            { auto _ = std::move(root->mod_decls_to_free).to_owned(gi->heap); }
            { auto _ = std::move(root->symbols); }
        } break;
        case Scope_Base_Tag__head: {
            Scope_Head *const head = scope_get_checked(scope, head);
            yy_assert(!scopeHasRefPatches(scope->tag));
            auto _ = std::move(head->symbols);
        } break;
        case Scope_Base_Tag__block: {
            Scope_Block *const block = scope_get_checked(scope, block);
            yy_assert(block->inst_tag == genir_instsAt(gi, block->block_like_reserved_inst)->tag);
            yy_assert(!scopeHasRefPatches(scope->tag));
            ScopeSymbols *const symbols = &block->head->symbols;
            symbols->truncate(block->scope_stack_size);
            yy_debug_invalidate_ptr_one(block);
        } break;
    }

    return Yy_Ok;
}

NODISCARD static DeclId makeDecl(GenIr *const gi, AstNode *node, Option<IdentifierId> opt_name)
{
    static_assert(yy::trivial_raii_maybe_moveonly<Decl>);
    auto const this_id = DeclId(gi->decl_ptrs.size());
    auto const decl_ptr = allocator_new<Decl>(gi->arena.allocator(), Decl{
        .ast = node,
        .opt_name = opt_name,
        .inst = yy::uninitialized,
        .has_genir_hard_errors_todo_collect_vs_gen = false,
        .has_sema_collect_hard_errors = false,
        .has_sema_analyze_hard_errors = false,
    });
    gi->decl_ptrs.vec.push_back(gi->heap, decl_ptr);
    return this_id;
}

NODISCARD static YyStatus scopeAddDeclAst(
    GenIr *const gi, Scope_Root *const scope, Yuceyuf_AstNode *const ast, IdentifierId const name)
{
    ScopeLookupDeclResult const shadow = scopeLookupDeclAdvanced(&scope->base, name, false);
    if (shadow.found) {
        if (shadow.scope == &scope->base) {
            Sv const name_sv = genir_identifier_sv(gi, name);
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, ast), "redeclaration of '" Sv_Fmt "'", Sv_Arg(name_sv));
            Genir_ErrorLocation prev_decl_error_loc = MetaTu_init(Genir_ErrorLocation, ast, shadow.ss_value_ptr->ast);
            genir_add_error_note(gi, prev_decl_error_loc, "previous declaration was here");
            return Yy_Bad;
        }
    }
    DeclId new_decl = makeDecl(gi, ast, name);
    scope->symbols.put_no_clobber_emplace({}, name, ScopeSymbolValue {
        .ast = ast,
        .as = { .decl = new_decl },
        .tag = ScopeSymbolValue::Tag::decl,
        .referenced = false,
        .is_ambiguous = shadow.found,
        .mod_scope = None,
    });
    return Yy_Ok;
}

NODISCARD static YyStatus scopeAddDeclInst(
    GenIr *const gi, Scope_Base *const scope, InstIndex const inst, IdentifierId const name)
{
    ScopeLookupDeclResult const shadow = scopeLookupDeclAdvanced(scope, name, false);
    if (shadow.found) {
        Sv const name_sv = genir_identifier_sv(gi, name);
        TRY(genir_new_soft_error(gi));
        genir_add_soft_error_msg(gi, MetaTu_init(Genir_ErrorLocation, inst, inst), "redeclaration of '" Sv_Fmt "'", Sv_Arg(name_sv));
        Genir_ErrorLocation prev_decl_error_loc = MetaTu_init(Genir_ErrorLocation, ast, shadow.ss_value_ptr->ast);
        genir_add_error_note(gi, prev_decl_error_loc, "previous declaration was here");
        return Yy_Ok;
    } else {
        ScopeSymbols *symbols = &scope->closest_head()->symbols;
        symbols->put_no_clobber_emplace({}, name, ScopeSymbolValue {
            .ast = gi->inst_to_ast_map[inst],
            .as = { .inst = inst },
            .tag = ScopeSymbolValue::Tag::inst,
            .referenced = false,
            .is_ambiguous = false,
            .mod_scope = None,
        });
        return Yy_Ok;
    }
}

NODISCARD static YyStatus scopeAddDeclMod(
    GenIr *const gi, Scope_Root *const parent_scope, Yuceyuf_AstNode *const node, IdentifierId const name, Scope_Root *const mod)
{
    yy_assert(parent_scope != NULL);
    ScopeLookupDeclResult const shadow = scopeLookupDeclAdvanced(&parent_scope->base, name, false);
    if (shadow.found) {
        if (shadow.scope == &parent_scope->base) {
            Sv const name_sv = genir_identifier_sv(gi, name);
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "redeclaration of '" Sv_Fmt "'", Sv_Arg(name_sv));
            Genir_ErrorLocation prev_decl_error_loc = MetaTu_init(Genir_ErrorLocation, ast, shadow.ss_value_ptr->ast);
            genir_add_error_note(gi, prev_decl_error_loc, "previous declaration was here");
            return Yy_Bad;
        }
    }
    DeclId new_decl = makeDecl(gi, node, name);
    usize const put_idx = parent_scope->symbols.put_no_clobber_emplace({}, name, ScopeSymbolValue {
        .ast = node,
        .as = { .decl = new_decl },
        .tag = ScopeSymbolValue::Tag::decl,
        .referenced = false,
        .is_ambiguous = false,
        .mod_scope = mod,
    }).insertion_idx;
    parent_scope->mod_decls_to_free.push_back(gi->heap, intCastFromUsizeToU32(put_idx));
    return Yy_Ok;
}

NODISCARD static Option<Scope_Base*> scopeTryLookupMetaNamedLabel(Scope_Base *const scope, IdentifierId const label) {
    for (Scope_Base *s : *scope) {
        switch (s->tag) {
            case MetaEnum_count(Scope_Base_Tag):
                yy_unreachable();
            case Scope_Base_Tag__root:
                yy_unreachable();
            case Scope_Base_Tag__block: {
                if (scope_get_checked(s, block)->named_label == label) {
                    return s;
                }
            }
            break;
            case Scope_Base_Tag__head: {
                return None;
            }
            break;
        }
    }
    yy_unreachable();
}

NODISCARD static Option<Scope_Base*> scopeTryLookupMetaLoop(Scope_Base *const scope) {
    for (Scope_Base *s : *scope) {
        switch (s->tag) {
            case MetaEnum_count(Scope_Base_Tag):
                yy_unreachable();
            case Scope_Base_Tag__root:
                yy_unreachable();
            case Scope_Base_Tag__block: {
                if (scope_get_checked(s, block)->inst_tag == Yy_Ir_Inst_Tag__loop) {
                    return s;
                }
            }
            break;
            case Scope_Base_Tag__head: {
                return None;
            }
            break;
        }
    }
    yy_unreachable();
}

NODISCARD static YyStatus scopeLookupMetaInst(
    GenIr *const gi, Scope_Base *const scope, Yuceyuf_AstNode *const ast_loc, InstIndex const label,
    Scope_Base **const ret_block_scope)
{
    yy_debug_invalidate_ptr_one(ret_block_scope);
    for (Scope_Base *s : *scope) {
        switch (s->tag) {
            case MetaEnum_count(Scope_Base_Tag):
                yy_unreachable();
            case Scope_Base_Tag__root:
                yy_unreachable();
            case Scope_Base_Tag__block: {
                if (scope_get_checked(s, block)->block_like_reserved_inst == label) {
                    *ret_block_scope = s;
                    return Yy_Ok;
                }
            }
            break;
            case Scope_Base_Tag__head: {
                // Hard error
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, ast_loc),
                    "TODO error message: scopeLookupMetaInst crossed a head boundary");
                return Yy_Bad;
            }
            break;
        }
    }
    yy_unreachable();
}

NODISCARD static YyStatus scopeLookupMetaActualFunction(
    GenIr *const gi, Scope_Base *const scope, Yuceyuf_AstNode *const ast_loc, Scope_Head **const ret_fn_scope)
{
    yy_debug_invalidate_ptr_one(ret_fn_scope);
    switch (scope->tag) {
        case MetaEnum_count(Scope_Base_Tag):
            yy_unreachable();
        case Scope_Base_Tag__root: {
            // Hard error
            // TODO: Maybe this is unreachable.
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, ast_loc), "instruction outside of function");
            return Yy_Bad;
        }
        break;
        case Scope_Base_Tag__block: {
            *ret_fn_scope = scope_get_checked(scope, block)->head;
            return Yy_Ok;
        } break;
        case Scope_Base_Tag__head: {
            auto head = scope_get_checked(scope, head);
            if (!head->is_function) {
                genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, ast_loc), "instruction outside of function");
                return Yy_Bad;
            }
            *ret_fn_scope = head;
            return Yy_Ok;
        } break;
    }
    yy_unreachable();
}
NODISCARD static YyStatus scopeLookupMetaHead(
    GenIr *const gi, Scope_Base *const scope, Yuceyuf_AstNode *const ast_loc, Scope_Head **const ret_fn_scope)
{
    yy_debug_invalidate_ptr_one(ret_fn_scope);
    switch (scope->tag) {
        case MetaEnum_count(Scope_Base_Tag):
            yy_unreachable();
        case Scope_Base_Tag__root: {
            // Hard error
            // TODO: Maybe this is unreachable.
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, ast_loc), "instruction outside of function");
            return Yy_Bad;
        }
        break;
        case Scope_Base_Tag__block:
            *ret_fn_scope = scope_get_checked(scope, block)->head;
            return Yy_Ok;
        case Scope_Base_Tag__head:
            *ret_fn_scope = scope_get_checked(scope, head);
            return Yy_Ok;
    }
    yy_unreachable();
}

// ---------------------------------

// TODO: This should return Yy_Ir_InstOffsetSelfForward
NODISCARD u32 genir_instEndOffset(GenIr *const gi, Yy_Ir_InstIndex const inst_idx)
{
    Inst const *const inst = &gi->insts[inst_idx];
    switch (inst->tag) {
        case MetaEnum_count(Yy_Ir_Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__fn_arg_decl:
        case Yy_Ir_Inst_Tag__analyzed_field_val:
        case Yy_Ir_Inst_Tag__analyzed_field_ref:
        case Yy_Ir_Inst_Tag__untyped_field_val:
        case Yy_Ir_Inst_Tag__untyped_field_ref:
        case Yy_Ir_Inst_Tag__decl_val:
        case Yy_Ir_Inst_Tag__decl_ref:
        case Yy_Ir_Inst_Tag__local_decl_val:
        case Yy_Ir_Inst_Tag__local_decl_ref:
        case Yy_Ir_Inst_Tag__call:
        case Yy_Ir_Inst_Tag__address_of:
        case Yy_Ir_Inst_Tag__deref_ref:
        case Yy_Ir_Inst_Tag__deref_val:
        case Yy_Ir_Inst_Tag__const_cstring:
        case Yy_Ir_Inst_Tag__const_int:
        case Yy_Ir_Inst_Tag__const_bool:
        case Yy_Ir_Inst_Tag__const_undefined:
        case Yy_Ir_Inst_Tag__discard:
        case Yy_Ir_Inst_Tag__placeholder_ref:
        case Yy_Ir_Inst_Tag__assign:
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub_wrap:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte:
        case Yy_Ir_Inst_Tag__bool_not:
        case Yy_Ir_Inst_Tag__bits_not:
        case Yy_Ir_Inst_Tag__call_builtin_print_uint:
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world:
        case Yy_Ir_Inst_Tag__ptr_to_int:
        case Yy_Ir_Inst_Tag__break_void:
        case Yy_Ir_Inst_Tag__break_value:
        case Yy_Ir_Inst_Tag__return_void:
        case Yy_Ir_Inst_Tag__return_value:
        case Yy_Ir_Inst_Tag__xunreachable:
            return 1;
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn:
        case Yy_Ir_Inst_Tag__type_ptr_to:
            return 1;
        case Yy_Ir_Inst_Tag__truncate_int: {
            auto dest_type_block = genir_extraPl(gi, inst_idx, truncate_int)->dest_type_block.abs(inst_idx);
            u32 const dest_type_block_end_offset = genir_extraPl(gi, dest_type_block, block)->end_offset;
            u32 const this_end_offset = dest_type_block.x + dest_type_block_end_offset - inst_idx.x;
            return this_end_offset;
        } break;
        case Yy_Ir_Inst_Tag__promote_int: {
            auto dest_type_block = genir_extraPl(gi, inst_idx, promote_int)->dest_type_block.abs(inst_idx);
            u32 const dest_type_block_end_offset = genir_extraPl(gi, dest_type_block, block)->end_offset;
            u32 const this_end_offset = dest_type_block.x + dest_type_block_end_offset - inst_idx.x;
            return this_end_offset;
        } break;
        case Yy_Ir_Inst_Tag__ptr_cast: {
            auto dest_type_block = genir_extraPl(gi, inst_idx, ptr_cast)->dest_type_block.abs(inst_idx);
            u32 const dest_type_block_end_offset = genir_extraPl(gi, dest_type_block, block)->end_offset;
            u32 const this_end_offset = dest_type_block.x + dest_type_block_end_offset - inst_idx.x;
            return this_end_offset;
        } break;
        case Yy_Ir_Inst_Tag__int_to_ptr: {
            auto dest_type_block = genir_extraPl(gi, inst_idx, int_to_ptr)->dest_type_block.abs(inst_idx);
            u32 const dest_type_block_end_offset = genir_extraPl(gi, dest_type_block, block)->end_offset;
            u32 const this_end_offset = dest_type_block.x + dest_type_block_end_offset - inst_idx.x;
            return this_end_offset;
        } break;
        case Yy_Ir_Inst_Tag__block:
            return genir_extraPl(gi, inst_idx, block)->end_offset;
        case Yy_Ir_Inst_Tag__root:
            return genir_extraPl(gi, inst_idx, root)->end_offset;
        case Yy_Ir_Inst_Tag__decl_error_skip:
            return genir_extraPl(gi, inst_idx, decl_error_skip)->end_offset;
        case Yy_Ir_Inst_Tag__cond_branch: {
            Yy_Ir_Inst_ExtraPl_CondBranch const extra_pl = *genir_extraPl(gi, inst_idx, cond_branch);
            InstIndex const then = extra_pl.then.abs(inst_idx);
            InstIndex const xelse_idx = extra_pl.xelse.abs(inst_idx);
            yy_assert(gi->insts[then].tag == Yy_Ir_Inst_Tag__block);
            u32 const xelse_end_offset = genir_extraPl(gi, xelse_idx, block)->end_offset;
            InstIndex const xelse_end = xelse_idx + xelse_end_offset;
            u32 const this_end_offset = xelse_end.x - inst_idx.x;
            return this_end_offset;
        }
        break;
        case Yy_Ir_Inst_Tag__loop: {
            InstIndex const loop_inner_inst_idx = yy_ir_inst_loop_getLoopInnerInstIndex(inst_idx);
            yy_assert(gi->insts[loop_inner_inst_idx].tag == Yy_Ir_Inst_Tag__block);
            u32 const loop_inner_end_offset = genir_extraPl(gi, loop_inner_inst_idx, block)->end_offset;
            InstIndex const loop_inner_end = loop_inner_inst_idx + loop_inner_end_offset;
            u32 const this_end_offset = loop_inner_end.x - inst_idx.x;
            return this_end_offset;
        }
        break;
        case Yy_Ir_Inst_Tag__fn_decl: {
            // NOTE: This must be kept in sync with `genir_astFnDecl`.
            InstIndex const body_idx = genir_getFnDeclBodyInst(gi, inst_idx);
            u32 const body_end_offset = genir_extraPl(gi, body_idx, block)->end_offset;
            u32 const this_end_offset = body_idx.x + body_end_offset - inst_idx.x;
            return this_end_offset;
        }
        break;
        case Yy_Ir_Inst_Tag__struct_decl: {
            // NOTE: This must be kept in sync with `genir_astStructDecl`.
            return genir_extraPl(gi, inst_idx, struct_decl)->end_offset;
        }
        break;
        case Yy_Ir_Inst_Tag__global_var_decl: {
            // NOTE: This must be kept in sync with `genir_astGlobalVarDecl`.
            InstIndex const init_block_idx = genir_getGlobalVarDeclInitBlockInst(gi, inst_idx);
            u32 const init_block_end_offset = genir_extraPl(gi, init_block_idx, block)->end_offset;
            u32 const this_end_offset = init_block_idx.x + init_block_end_offset - inst_idx.x;
            return this_end_offset;
        }
        break;
        case Yy_Ir_Inst_Tag__local_var_decl: {
            // NOTE: This must be kept in sync with `genir_astLocalVarDecl`.
            auto type_self_offset = genir_extraPl(gi, inst_idx, local_var_decl)->type_block;
            if (type_self_offset.has_value()) {
                auto type_block = type_self_offset.unwrap().abs(inst_idx);
                auto type_end_offset = genir_extraPl(gi, type_block, block)->end_offset;
                return type_block.x + type_end_offset - inst_idx.x;
            } else {
                return 1;
            }
        } break;
        case Yy_Ir_Inst_Tag__extern_var_decl: {
            // NOTE: This must be kept in sync with `genir_astExternVarDecl`.
            auto type_block = genir_extraPl(gi, inst_idx, extern_var_decl)->type_block.abs(inst_idx);
            auto type_end_offset = genir_extraPl(gi, type_block, block)->end_offset;
            return type_block.x + type_end_offset - inst_idx.x;
        } break;
        case Yy_Ir_Inst_Tag__extern_fn_decl: {
            // NOTE: This must be kept in sync with `genir_astExternFnDecl`.
            auto ret_block = genir_extraPl(gi, inst_idx, extern_fn_decl)->return_type_eval_block_offset.abs(inst_idx);
            auto ret_end_offset = genir_extraPl(gi, ret_block, block)->end_offset;
            return ret_block.x + ret_end_offset - inst_idx.x;
        } break;
    }
    yy_unreachable();
}


/// Reserve an instruction that will be set later with `genir_unreserveInst`
NODISCARD static InstIndex genir_reserveInst(GenIr *gi)
{
    InstIndex result = yy::uninitialized;
    if (yy_is_err(yy_try_int_cast(gi->insts.size(), &result.x))) {
        yy_panic("genir insts overflow");
    }
    gi->insts.vec.push_back(gi->heap, Inst {
        .tag = std::numeric_limits<Inst_Tag>::max(),
        .untyped_use_count = 0,
        .small_data = { .unused = 0 },
    });
    gi->inst_to_ast_map.vec.push_back(gi->heap, nullptr);
    yy_assert(gi->insts.size() == gi->inst_to_ast_map.size());
    return result;
}

static auto referenceInst(GenIr* gi, InstIndex referenced, InstIndex refd_by) -> void {
    (void) refd_by;
    gi->insts[referenced].untyped_use_count += 1;
}
/// Set the value of an instruction that was reserved by `genir_reserveInst`.
static void genir_unreserveInst(
    GenIr *gi, InstIndex inst_idx, AstNode *ast_node, Inst inst_value,
    std::initializer_list<InstIndex> const& refs)
{
    yy_assert(gi->insts[inst_idx].tag == std::numeric_limits<Yy_Ir_Inst_Tag>::max());
    gi->insts[inst_idx] = inst_value;
    gi->inst_to_ast_map[inst_idx] = ast_node;
    for (auto ref : refs) {
        referenceInst(gi, ref, inst_idx);
    }
}

NODISCARD static InstIndex genir_appendInst(
    GenIr *gi, AstNode *ast_node, Inst inst,
    std::initializer_list<InstIndex> const& refs)
{
    InstIndex const result = genir_reserveInst(gi);
    genir_unreserveInst(gi, result, ast_node, inst, refs);
    return result;
}

NODISCARD static StringIndex genir_strTableAlloc(GenIr *gi, u32 byte_count)
{
    StringIndex new_one_past_last;
    // TODO: change this after I move the codebase to c++20
    if (__builtin_add_overflow(gi->string_table.size(), byte_count, &new_one_past_last.x)) {
        yy_panic("genir string table overflow");
    }
    StringIndex result = {yy::checked_int_cast<u32>(gi->string_table.size()).unwrap()};
    gi->string_table.resize(gi->heap, new_one_past_last.x, 0);
    return result;
}

/// Also append an additional '\0'.
NODISCARD static StringIndex genir_strTableAppendBytesAndZ(GenIr *gi, u8 const *bytes_ptr, usize bytes_len)
{
    u32 const len = intCastFromUsizeToU32(bytes_len);
    StringIndex const result = genir_strTableAlloc(gi, len + 1);
    for (usize i = 0; i < len; i += 1) {
        gi->string_table[result.x + i] = u8_to_char(bytes_ptr[i]);
    }
    gi->string_table[result.x + len] = 0;
    return result;
}

NODISCARD static StringIndex genir_strTableAppendSvAndZ(GenIr *gi, Sv sv)
{
    return genir_strTableAppendBytesAndZ(gi, sv.as_bytes().ptr, sv.len);
}

/// This returns a pointer to memory that may be moved. Use the pointer and forget about it as
/// soon as possible.
NODISCARD Sv genir_strTableGetTempSvFromZ(GenIr const *gi, StringIndex str_z_idx)
{
    yy_assert(str_z_idx.x < gi->string_table.size());
    char const *const ptr = &gi->string_table[str_z_idx.x];
    usize const len = strlen(ptr);
    yy_assert(str_z_idx.x + len < gi->string_table.size());
    return Sv(ptr, len);
}

NODISCARD Sv genir_strTableGetTempSvFromLen(GenIr const *const gi, Yy_Ir_StringIndex const str_idx, u32 const len)
{
    yy_assert(str_idx.x + len <= gi->string_table.size());
    char const *const ptr = &gi->string_table[str_idx.x];
    return Sv(ptr, len);
}

NODISCARD Sv genir_strTableGetTempSvFromSized0(GenIr const *const gi, Yy_Ir_StringSized0 const string)
{
    return genir_strTableGetTempSvFromLen(gi, string.start, string.len0);
}

/// Reserve extra space. It must be initialized with `genir_unreserveExtra`.
NODISCARD Yy_Ir_ExtraIndex genir_reserveExtra(GenIr *gi, u32 reserved_len)
{
    ExtraIndex new_one_past_last;
    // TODO: change this after I move the codebase to c++20
    if (__builtin_add_overflow(gi->extras.size(), reserved_len, &new_one_past_last.x)) {
        yy_panic("genir extras overflow");
    }
    ExtraIndex result = {yy::checked_int_cast<u32>(gi->extras.size()).unwrap()};
    gi->extras.resize(gi->heap, new_one_past_last.x, yy::uninitialized);
    if (reserved_len > 0) {
        // For safety, store `reserved_len` into the first reserved extra value. Flip the bits
        // before storing it to make it easier to catch bugs. This value is only used
        // for `genir_unreserveExtra`'s assert.
        *genir_extrasAt(gi, result) = Extra { .as_u32 = ~reserved_len };
    }
    return result;
}

/// Initialize reserved extra space from `genir_reserveExtra`.
void genir_unreserveExtra(
    GenIr *gi, Yy_Ir_ExtraIndex reserved_idx, Yy_Ir_Extra const *data_ptr, u32 data_len)
{
    yy_assert(reserved_idx.x + data_len <= gi->extras.size());
    if (data_len > 0) {
        // Unflip the flipped bits on genir_reserveExtra.
        yy_assert(~genir_extrasAt(gi, reserved_idx)[0].as_u32 == data_len);
    }
    // Copy as u8 to avoid type aliasing problems.
    auto data_ptr_as_u8 = reinterpret_cast<u8 const*>(data_ptr);
    auto extras_array_start_ptr_as_u8 = reinterpret_cast<u8*>(&gi->extras[reserved_idx.x]);
    memcpy(extras_array_start_ptr_as_u8, data_ptr_as_u8, data_len * sizeof(Extra));
}

static void genir_unreserveExtraArray(
    GenIr *const gi, ExtraIndex const reserved_idx, u32 const elem_idx,
    Extra const *const data_ptr, u32 const data_len)
{
    yy_assert(reserved_idx.x + elem_idx*data_len <= gi->extras.size());
    // Don't do any safety checks.
    ExtraIndex const elem_extra_idx = { .x = reserved_idx.x + elem_idx*data_len };
    auto data_ptr_as_u8 = reinterpret_cast<u8 const*>(data_ptr);
    auto extras_array_start_ptr_as_u8 = reinterpret_cast<u8*>(&gi->extras[elem_extra_idx.x]);
    memcpy(extras_array_start_ptr_as_u8, data_ptr_as_u8, data_len * sizeof(Extra));
}

NODISCARD static ExtraIndex genir_appendExtra(GenIr *gi, Extra const *data_ptr, u32 data_len)
{
    ExtraIndex const result = genir_reserveExtra(gi, data_len);
    genir_unreserveExtra(gi, result, data_ptr, data_len);
    return result;
}


NODISCARD static u32 instOffsetToCurrent(GenIr const *gi, InstIndex from)
{
    return intCastFromUsizeToU32(gi->insts.size()) - from.x;
}


/// `genir_expr` will call `genir_astFooBar`. The innermost function (FooBar) will check that
/// it can produce said result, and will decide on the instruction if there's a difference between
/// `value` and `ref`.
// TODO: `never_noreturn` flag, `never_tmp_ref` flag
typedef enum {
    // Maybe `InstResultKind__callee` will be useful?
    // InstResultKind__unreachable,
    /// Must be used as a statement
    InstResultKind__stmt,
    InstResultKind__value,
    InstResultKind__ref,
} InstResultKind;

NODISCARD static char const *irk_to_cstr(InstResultKind irk)
{
    switch (irk) {
        case InstResultKind__stmt:
            return "stmt";
        case InstResultKind__value:
            return "value";
        case InstResultKind__ref:
            return "ref";
    }
    yy_unreachable();
}

static void hard_error_todo_tmp_ref(GenIr *const gi, AstNode *const node_loc)
{
    genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node_loc), "TODO tmp_ref for making temporary references");
}

NODISCARD static YyStatus soft_error_unused_result(GenIr *const gi, AstNode *const node_loc)
{
    TRY(genir_new_soft_error(gi));
    genir_add_soft_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node_loc), "unused result");
    return Yy_Ok;
}

NODISCARD static Inst_Tag typeInstTagFromTypeAst(AstNode *const node)
{
    todo_assert(node->it.tag == Yuceyuf_AstNode_Specific_Tag__builtin_typ);
    TokenKeyword const kw = *MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, builtin_typ);
    switch (kw) {
        case MetaEnum_count(TokenKeyword):
        case TokenKeyword__fn:
        case TokenKeyword__xstruct:
        case TokenKeyword__mod:
        case TokenKeyword__xextern:
        case TokenKeyword__ximport:
        case TokenKeyword__xreturn:
        case TokenKeyword__xbreak:
        case TokenKeyword__xcontinue:
        case TokenKeyword__xvar:
        case TokenKeyword__xwhile:
        case TokenKeyword__xif:
        case TokenKeyword__xelse:
        case TokenKeyword__xtrue:
        case TokenKeyword__xfalse:
        case TokenKeyword__xundefined:
        case TokenKeyword__unreach:
        case TokenKeyword__bool_and:
        case TokenKeyword__bool_or:
        case TokenKeyword__underscore:
            yy_unreachable();
        case TokenKeyword__uintword:
            return Yy_Ir_Inst_Tag__type_uintword;
        case TokenKeyword__xu8:
            return Yy_Ir_Inst_Tag__type_u8;
        case TokenKeyword__xvoid:
            return Yy_Ir_Inst_Tag__type_xvoid;
        case TokenKeyword__xbool:
            return Yy_Ir_Inst_Tag__type_xbool;
        case TokenKeyword__xnoreturn:
            return Yy_Ir_Inst_Tag__type_xnoreturn;
    }
    yy_unreachable();
}


NODISCARD static YyStatus genir_expr(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result);
NODISCARD static YyStatus astBlockStmtOrDecl(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node, InstIndex *const ret_result);
NODISCARD static YyStatus genExprValueViaBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, Option<InstIndex> const break_to,
    InstIndex *const ret_result);
NODISCARD static YyStatus genExprValueViaFunctionlessBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node,
    InstIndex *const ret_result);

NODISCARD static YyStatus genir_astExprBuiltinFnCall(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    // Initialize it to an invalid value since the compiler can't catch an uninitialized value
    // in this function.
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, builtin_fn_call);
    switch (node_pl->bfn) {
        case MetaEnum_count(TokenBuiltinFn):
            yy_unreachable();
        default:
            logDebug(Sv_Fmt, Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
            yy_todo();
        case TokenBuiltinFn__print_uint: {
            if (node_pl->args.nodes.size() != 1) {
                // TODO: This could stop compiling this _function_ but continue on other functions.
                // Do I have to stop compiling even this _function_? Well, that depends on the
                // builtin. For builtins like print_uint, I could consider all the extra arguments
                // as `InstResultKind__value` and add another Ir instruction:
                // `error_extra_argument(inst: InstIndex [val]) -> [stmt]`. They will get symbol resolved and
                // everything. This instruction will only appear when there are errors in genir, so
                // sema and codegen will never find it. If there were zero arguments I could emit an
                // instruction called `$missing = error_missing_argument()` and pass that to the
                // print_uint builtin: `$_ = call_builtin_print_uint($missing)`. Some builtlins are
                // different, like @field and @import, but I think I can do the exact same thing for
                // them.
                // TODO: Make this a soft error.
                // When there are extra arguments, add another instruction: `error_extra_argument(value: genir_expr(Irk_value))`.
                // When there are missing arguments, add `%missing0 = error_missing_argument()`, and pass those `%missing` values to the builtin.
                // When sema analyzes error_extra_argument or error_missing_argument, it will fail on the spot.
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 1 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // stmt and value: do nothing
                case InstResultKind__stmt:
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }
            InstIndex arg_inst;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &arg_inst));
            InstIndex const result_inst = genir_reserveInst(gi);
            genir_unreserveInst(gi, result_inst, node, Inst_init(call_builtin_print_uint, {
                .arg_inst = genir_instOffsetSB(result_inst, arg_inst),
            }), {arg_inst});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__print_hello_world: {
            (void) scope;
            // fn @print_hello_world() void;
            if (node_pl->args.nodes.size() != 0) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 1 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // stmt and value: do nothing
                case InstResultKind__stmt:
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }
            InstIndex const result = genir_appendInst(gi, node, Inst {
                .tag = Yy_Ir_Inst_Tag__call_builtin_print_hello_world,
                .untyped_use_count = 0,
                .small_data = { .unused = 0 },
            }, {});
            *ret_result = result;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__truncate: {
            if (node_pl->args.nodes.size() != 2) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 2 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // error: unused result
                case InstResultKind__stmt: {
                    // Add the error and keep going.
                    TRY(soft_error_unused_result(gi, node));
                }
                break;
                // value: do nothing
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }

            InstIndex src_value;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[1], &src_value));

            InstIndex const result_inst = genir_reserveInst(gi);
            InstIndex dest_type_block;
            TRY(genExprValueViaFunctionlessBlock(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &dest_type_block));
            Yy_Ir_Inst_ExtraPl_TruncateInt const extra_pl_data = {
                .dest_type_block = genir_instOffsetSF(result_inst, dest_type_block),
                .src_value = genir_instOffsetSB(result_inst, src_value),
            };
            ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
            genir_unreserveInst(gi, result_inst, node, Inst_init(truncate_int, {
                .extra_pl_index = extra_pl_index
            }), {src_value});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__as: {
            if (node_pl->args.nodes.size() != 2) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 2 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // error: unused result
                case InstResultKind__stmt: {
                    // Add the error and keep going.
                    TRY(soft_error_unused_result(gi, node));
                }
                break;
                // value: do nothing
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }

            InstIndex src_value;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[1], &src_value));

            InstIndex const result_inst = genir_reserveInst(gi);
            InstIndex dest_type_block;
            TRY(genExprValueViaFunctionlessBlock(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &dest_type_block));
            Yy_Ir_Inst_ExtraPl_PromoteInt const extra_pl_data = {
                .dest_type_block = genir_instOffsetSF(result_inst, dest_type_block),
                .src_value = genir_instOffsetSB(result_inst, src_value),
            };
            ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
            genir_unreserveInst(gi, result_inst, node, Inst_init(promote_int, {
                .extra_pl_index = extra_pl_index
            }), {src_value});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__ptr_cast: {
            if (node_pl->args.nodes.size() != 2) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 2 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // error: unused result
                case InstResultKind__stmt: {
                    // Add the error and keep going.
                    TRY(soft_error_unused_result(gi, node));
                }
                break;
                // value: do nothing
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }

            InstIndex src_value;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[1], &src_value));

            InstIndex const result_inst = genir_reserveInst(gi);
            InstIndex dest_type_block;
            TRY(genExprValueViaFunctionlessBlock(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &dest_type_block));
            Yy_Ir_Inst_ExtraPl_PtrCast const extra_pl_data = {
                .dest_type_block = genir_instOffsetSF(result_inst, dest_type_block),
                .src_value = genir_instOffsetSB(result_inst, src_value),
            };
            ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
            genir_unreserveInst(gi, result_inst, node, Inst_init(ptr_cast, {
                .extra_pl_index = extra_pl_index
            }), {src_value});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__int_to_ptr: {
            if (node_pl->args.nodes.size() != 2) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 2 argument, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // error: unused result
                case InstResultKind__stmt: {
                    // Add the error and keep going.
                    TRY(soft_error_unused_result(gi, node));
                }
                break;
                // value: do nothing
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }

            InstIndex src_value;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[1], &src_value));

            InstIndex const result_inst = genir_reserveInst(gi);
            InstIndex dest_type_block;
            TRY(genExprValueViaFunctionlessBlock(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &dest_type_block));
            Yy_Ir_Inst_ExtraPl_IntToPtr const extra_pl_data = {
                .dest_type_block = genir_instOffsetSF(result_inst, dest_type_block),
                .src_value = genir_instOffsetSB(result_inst, src_value),
            };
            ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
            genir_unreserveInst(gi, result_inst, node, Inst_init(int_to_ptr, {
                .extra_pl_index = extra_pl_index
            }), {src_value});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
        case TokenBuiltinFn__ptr_to_int: {
            if (node_pl->args.nodes.size() != 1) {
                genir_add_hard_error_msg(
                    gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                    "expected 1 arguments, found %zu, for builtin @" Sv_Fmt "",
                    node_pl->args.nodes.size(), Sv_Arg(MetaEnum_sv_short(TokenBuiltinFn, node_pl->bfn)));
                return Yy_Bad;
            }
            // Validate irk
            switch (irk) {
                // error: unused result
                case InstResultKind__stmt: {
                    // Add the error and keep going.
                    TRY(soft_error_unused_result(gi, node));
                }
                break;
                // value: do nothing
                case InstResultKind__value:
                {} break;
                case InstResultKind__ref: {
                    hard_error_todo_tmp_ref(gi, node);
                    return Yy_Bad;
                }
                break;
            }

            InstIndex src_value;
            TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->args.nodes[0], &src_value));

            InstIndex const result_inst = genir_reserveInst(gi);
            genir_unreserveInst(gi, result_inst, node, Inst_init(ptr_to_int, {
                .src_value = genir_instOffsetSB(result_inst, src_value),
            }), {src_value});
            *ret_result = result_inst;
            return Yy_Ok;
        }
        break;
    }
    yy_unreachable();
}

NODISCARD static YyStatus genConstInt(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node_loc, i64 const i64_value,
    InstIndex *const ret_result)
{
    (void) scope;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node_loc));
        }
        break;
        // value: do nothing
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node_loc);
            return Yy_Bad;
        }
        break;
    }

    Yy_Ir_Inst_ExtraPl_ConstInt const extra_pl_data = { .integer = i64_value };
    ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
    InstIndex const const_int_inst = genir_appendInst(gi, node_loc, Inst_init(const_int, {
        .extra_pl_index = extra_pl_index,
    }), {});

    *ret_result = const_int_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprIntegerLiteral(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, integer_literal);
    i64 i64_value;
    yy_unwrap_ok(yy_try_int_cast(node_pl->number, &i64_value));
    return genConstInt(gi, scope, irk, node, i64_value, ret_result);
}

NODISCARD static YyStatus genir_astExprCharLiteral(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, char_literal);
    i64 i64_value;
    yy_unwrap_ok(yy_try_int_cast(node_pl->codepoint, &i64_value));
    return genConstInt(gi, scope, irk, node, i64_value, ret_result);
}

NODISCARD static YyStatus genConstCstring(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node_loc, Sv const value,
    InstIndex *const ret_result)
{
    (void) scope;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node_loc));
        }
        break;
        // value: do nothing
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node_loc);
            return Yy_Bad;
        }
        break;
    }

    // TODO: Don't use the identifier table for this. I should write into extra.
    StringIndex const string_idx = genir_strTableAppendSvAndZ(gi, value);

    Yy_Ir_Inst_ExtraPl_ConstCstring const extra_pl_data = {
        .string = {
            .start = string_idx,
            .len0 = intCastFromUsizeToU32(value.len + 1),
        },
    };
    ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
    InstIndex const const_cstring_inst = genir_appendInst(gi, node_loc, Inst_init(const_cstring, {
        .extra_pl_index = extra_pl_index,
    }), {});

    *ret_result = const_cstring_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprStringLiteral(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto* node_pl = &node->it.as_string_literal().unwrap();
    return genConstCstring(gi, scope, irk, node, node->parent_ast_file->lexer.lex.string_literal(node_pl->id), ret_result);
}

NODISCARD static YyStatus genConstBool(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node_loc, bool const bool_value,
    InstIndex *const ret_result)
{
    (void) scope;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node_loc));
        }
        break;
        // value: do nothing
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node_loc);
            return Yy_Bad;
        }
        break;
    }

    InstIndex const result_inst = genir_appendInst(gi, node_loc, Inst_init(const_bool, {
        .value = bool_value,
    }), {});

    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprBoolLiteral(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, bool_literal);
    return genConstBool(gi, scope, irk, node, *node_pl, ret_result);
}
NODISCARD static YyStatus genir_astExprUndefinedLiteral(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    yy_assert(node->it.tag == Yuceyuf_AstNode_Specific_Tag__undefined_literal);
    (void) scope;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: do nothing
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex const result_inst = genir_appendInst(gi, node, Inst_init(const_undefined, {}), {});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprIgnoreResult(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, ignore_result);

    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex rhs;
    // Should I add `InstResultKind__value_to_be_discarded`? That would only be useful if I
    // wanted to generate different instructions depending on that. I don't think I need it. I
    // don't need this new irk variant to implement error messages either since it would only
    // be used for the discard instruction.
    TRY(genir_expr(gi, scope, InstResultKind__value, *node_pl, &rhs));

    InstIndex const discard_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, discard_inst, node, Inst_init(discard, {
        .inst_to_discard = genir_instOffsetSB(discard_inst, rhs),
    }), {rhs});

    *ret_result = discard_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprAssign(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, assign);

    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    // Order of evaluation: left to right.
    // TODO: reject tmp_ref
    InstIndex lhs;
    TRY(genir_expr(gi, scope, InstResultKind__ref, node_pl->lhs, &lhs));

    InstIndex rhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->rhs, &rhs));

    InstIndex const assign_inst = genir_reserveInst(gi);
    Yy_Ir_Inst_ExtraPl_Assign const extra_pl_data = {
        .lhs = genir_instOffsetSB(assign_inst, lhs),
        .rhs = genir_instOffsetSB(assign_inst, rhs),
    };
    ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
    genir_unreserveInst(gi, assign_inst, node, Inst_init(assign, {
        .extra_pl_index = extra_pl_index,
    }), {lhs, rhs});

    *ret_result = assign_inst;
    return Yy_Ok;
}

NODISCARD static Inst_Tag astBinOpToInstTag(Yuceyuf_Ast_BinaryOp const op)
{
    switch (op) {
        case MetaEnum_count(Yuceyuf_Ast_BinaryOp):
            yy_unreachable();
        case Yuceyuf_Ast_BinaryOp__add:
            return Yy_Ir_Inst_Tag__add;
        case Yuceyuf_Ast_BinaryOp__sub:
            return Yy_Ir_Inst_Tag__sub;
        case Yuceyuf_Ast_BinaryOp__mul:
            return Yy_Ir_Inst_Tag__mul;
        case Yuceyuf_Ast_BinaryOp__add_wrap:
            return Yy_Ir_Inst_Tag__add_wrap;
        case Yuceyuf_Ast_BinaryOp__sub_wrap:
            return Yy_Ir_Inst_Tag__sub_wrap;
        case Yuceyuf_Ast_BinaryOp__mul_wrap:
            return Yy_Ir_Inst_Tag__mul_wrap;
        case Yuceyuf_Ast_BinaryOp__div:
            return Yy_Ir_Inst_Tag__div;
        case Yuceyuf_Ast_BinaryOp__mod:
            return Yy_Ir_Inst_Tag__mod;
        case Yuceyuf_Ast_BinaryOp__sh_left:
            return Yy_Ir_Inst_Tag__shl;
        case Yuceyuf_Ast_BinaryOp__sh_right:
            return Yy_Ir_Inst_Tag__shr;
        case Yuceyuf_Ast_BinaryOp__bits_and:
            return Yy_Ir_Inst_Tag__bits_and;
        case Yuceyuf_Ast_BinaryOp__bits_or:
            return Yy_Ir_Inst_Tag__bits_or;
        case Yuceyuf_Ast_BinaryOp__bits_xor:
            return Yy_Ir_Inst_Tag__bits_xor;
        case Yuceyuf_Ast_BinaryOp__bool_eq:
            return Yy_Ir_Inst_Tag__bool_eq;
        case Yuceyuf_Ast_BinaryOp__bool_noteq:
            return Yy_Ir_Inst_Tag__bool_noteq;
        case Yuceyuf_Ast_BinaryOp__bool_lt:
            return Yy_Ir_Inst_Tag__bool_lt;
        case Yuceyuf_Ast_BinaryOp__bool_lte:
            return Yy_Ir_Inst_Tag__bool_lte;
        case Yuceyuf_Ast_BinaryOp__bool_gt:
            return Yy_Ir_Inst_Tag__bool_gt;
        case Yuceyuf_Ast_BinaryOp__bool_gte:
            return Yy_Ir_Inst_Tag__bool_gte;

        case Yuceyuf_Ast_BinaryOp__bool_and:
        case Yuceyuf_Ast_BinaryOp__bool_or: {
            // This is handled differently because they short circuit.
            yy_unreachable();
        }
        break;
    }
    yy_unreachable();
}

NODISCARD static YyStatus genir_astExprBoolShortCircuit(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result);

NODISCARD static YyStatus genBinary(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node,
    Inst_Tag const inst_tag,
    InstIndex const lhs, InstIndex const rhs,
    InstIndex *const ret_result)
{
    (void) scope;
    InstIndex const result_inst = genir_reserveInst(gi);
    Yy_Ir_Inst_ExtraPl_Binary const extra_pl_data = {
        .lhs = genir_instOffsetSB(result_inst, lhs),
        .rhs = genir_instOffsetSB(result_inst, rhs),
    };

    ExtraIndex const extra_pl_index = genir_appendExtraPl(gi, extra_pl_data);
    genir_unreserveInst(gi, result_inst, node, Inst {
        .tag = inst_tag,
        .untyped_use_count = 0,
        .small_data = { .binary = { .extra_pl_index = extra_pl_index } }
    }, {lhs, rhs});

    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprOpBin(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_bin);

    switch (node_pl->op) {
        default:
        {} break;
        case Yuceyuf_Ast_BinaryOp__bool_and:
        case Yuceyuf_Ast_BinaryOp__bool_or:
            return genir_astExprBoolShortCircuit(gi, scope, irk, node, ret_result);
    }

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    // Order of evaluation: left to right.
    InstIndex lhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->lhs, &lhs));

    InstIndex rhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->rhs, &rhs));

    InstIndex result_inst;
    Inst_Tag const inst_tag = astBinOpToInstTag(node_pl->op);
    TRY(genBinary(gi, scope, node, inst_tag, lhs, rhs, &result_inst));

    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprAssignOpBin(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, assign_bin_op);

    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    // Order of evaluation: left to right.
    // TODO: lhs_ref is being used twice. ("docs/to-be-references.md")
    // TODO: reject tmp_ref
    InstIndex lhs_ref;
    TRY(genir_expr(gi, scope, InstResultKind__ref, node_pl->lhs, &lhs_ref));

    InstIndex rhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->rhs, &rhs));

    InstIndex const addrof_lhs_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, addrof_lhs_inst, node_pl->lhs, Inst_init(address_of, {
        .operand = genir_instOffsetSB(addrof_lhs_inst, lhs_ref),
    }), {lhs_ref});
    InstIndex const lhs_val = genir_reserveInst(gi);
    genir_unreserveInst(gi, lhs_val, node_pl->lhs, Inst_init(deref_val, {
        .operand = genir_instOffsetSB(lhs_val, addrof_lhs_inst),
    }), {addrof_lhs_inst});

    Inst_Tag const binary_inst_tag = astBinOpToInstTag(node_pl->op);
    InstIndex binary_inst;
    TRY(genBinary(gi, scope, node, binary_inst_tag, lhs_val, rhs, &binary_inst));

    InstIndex const assign_inst = genir_reserveInst(gi);
    Yy_Ir_Inst_ExtraPl_Assign const assign_extra_data = {
        .lhs = genir_instOffsetSB(assign_inst, lhs_ref),
        .rhs = genir_instOffsetSB(assign_inst, binary_inst),
    };
    ExtraIndex const assign_extra = genir_appendExtraPl(gi, assign_extra_data);
    genir_unreserveInst(gi, assign_inst, node, Inst_init(assign, {
        .extra_pl_index = assign_extra,
    }), {lhs_ref, binary_inst});

    *ret_result = assign_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprPtrTo(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_unary_prefix);
    yy_assert(node_pl->tag == Yuceyuf_Ast_UnaryOp__ptr_to);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex operand;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->operand, &operand));

    InstIndex const result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst_init(type_ptr_to, {
        .operand = genir_instOffsetSB(result_inst, operand),
    }), {operand});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprAddressOf(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_unary_prefix);
    yy_assert(node_pl->tag == Yuceyuf_Ast_UnaryOp__address_of);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok: address_of
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex operand;
    TRY(genir_expr(gi, scope, InstResultKind__ref, node_pl->operand, &operand));

    InstIndex const result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst_init(address_of, {
        .operand = genir_instOffsetSB(result_inst, operand),
    }), {operand});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprBinaryLeftZero(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk,
    AstNode *const node_loc, AstNode *const operand_node, Inst_Tag const inst_tag,
    InstIndex *const ret_result)
{
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node_loc));
        }
        break;
        // value: ok
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node_loc);
            return Yy_Bad;
        }
        break;
    }

    // Order of evaluation: left to right.
    // Generate a `const_int(0)`
    InstIndex lhs;
    TRY(genConstInt(gi, scope, InstResultKind__value, node_loc, 0, &lhs));

    InstIndex rhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, operand_node, &rhs));

    InstIndex result_inst;
    TRY(genBinary(gi, scope, node_loc, inst_tag, lhs, rhs, &result_inst));

    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprOpUnaryPrefix(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_unary_prefix);

    Inst_Tag inst_tag = yy::uninitialized;
    switch (node_pl->tag) {
        case MetaEnum_count(Yuceyuf_Ast_UnaryOp):
            yy_unreachable();
        case Yuceyuf_Ast_UnaryOp__ptr_to:
            return genir_astExprPtrTo(gi, scope, irk, node, ret_result);
        case Yuceyuf_Ast_UnaryOp__address_of:
            return genir_astExprAddressOf(gi, scope, irk, node, ret_result);
        case Yuceyuf_Ast_UnaryOp__plus:
            return genir_astExprBinaryLeftZero(gi, scope, irk, node, node_pl->operand, Yy_Ir_Inst_Tag__add, ret_result);
        case Yuceyuf_Ast_UnaryOp__plus_wrap:
            return genir_astExprBinaryLeftZero(gi, scope, irk, node, node_pl->operand, Yy_Ir_Inst_Tag__add_wrap, ret_result);
        case Yuceyuf_Ast_UnaryOp__minus:
            return genir_astExprBinaryLeftZero(gi, scope, irk, node, node_pl->operand, Yy_Ir_Inst_Tag__sub, ret_result);
        case Yuceyuf_Ast_UnaryOp__minus_wrap:
            return genir_astExprBinaryLeftZero(gi, scope, irk, node, node_pl->operand, Yy_Ir_Inst_Tag__sub_wrap, ret_result);
        case Yuceyuf_Ast_UnaryOp__bool_not:
            inst_tag = Yy_Ir_Inst_Tag__bool_not;
            break;
        case Yuceyuf_Ast_UnaryOp__bits_not:
            inst_tag = Yy_Ir_Inst_Tag__bits_not;
            break;
    }

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex operand;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->operand, &operand));

    InstIndex const result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst {
        .tag = inst_tag,
        .untyped_use_count = 0,
        .small_data = { .unary = { .operand = genir_instOffsetSB(result_inst, operand) } }
    }, {operand});

    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprDeref(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, deref);

    bool is_ref = false;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok: deref_val
        case InstResultKind__value:
        {}
        break;
        // ref: ok: deref_ref
        case InstResultKind__ref: {
            is_ref = true;
        }
        break;
    }

    InstIndex operand;
    TRY(genir_expr(gi, scope, InstResultKind__value, *node_pl, &operand));

    if (is_ref) {
        InstIndex const result_inst = genir_reserveInst(gi);
        genir_unreserveInst(
            gi, result_inst, node, Inst_init(deref_ref, {.operand = genir_instOffsetSB(result_inst, operand)}),
            {operand}
        );
        *ret_result = result_inst;
        return Yy_Ok;
    }
    InstIndex const result_inst = genir_reserveInst(gi);
    genir_unreserveInst(
        gi, result_inst, node, Inst_init(deref_val, {.operand = genir_instOffsetSB(result_inst, operand)}), {operand}
    );
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprFieldAccess(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, field_access);

    bool is_ref = false;
    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        case InstResultKind__value:
        {}
        break;
        case InstResultKind__ref: {
            is_ref = true;
        }
        break;
    }

    InstIndex container = yy::uninitialized;
    TRY(genir_expr(gi, scope, InstResultKind__ref, node_pl->container, &container));
    auto const field = node_pl->field;


    InstIndex const placeholder_inst = genir_reserveInst(gi);
    genir_unreserveInst(
        gi, placeholder_inst, node_pl->container,
        Inst_init(placeholder_ref, {container.asOffsetBackwardFrom(placeholder_inst)}), {container}
    );
    InstIndex const result_inst = genir_reserveInst(gi);
    auto const extra = genir_appendExtraPl(
        gi,
        Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef{.container = genir_instOffsetSB(result_inst, container), .field = field}
    );
    if (is_ref) {
        genir_unreserveInst(gi, result_inst, node, Inst_init(untyped_field_ref, {.extra_pl_index = extra}), {container});
        *ret_result = result_inst;
        return Yy_Ok;
    }
    genir_unreserveInst(gi, result_inst, node, Inst_init(untyped_field_val, {.extra_pl_index = extra}), {container});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static InstIndex genBeginBlock(GenIr *const gi)
{
    ExtraIndex const block_extra = genir_reserveExtraPl(gi, block);
    InstIndex const block_inst = genir_reserveInst(gi);
    // Store the extra index in there. On genEndBlock it will load it.
    gi->insts[block_inst].small_data.block.extra_pl_index = block_extra;
    return block_inst;
}

NODISCARD static YyStatus genEndBlockReturnVoid(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node_loc, InstIndex const block_inst)
{
    Scope_Head *the_fn_scope;
    TRY(scopeLookupMetaActualFunction(gi, scope, node_loc, &the_fn_scope));
    the_fn_scope->return_or_break_count_upper_limit += 1;

    InstIndex const implicit_ret = genir_appendInst(gi, node_loc, Inst {
        .tag = Yy_Ir_Inst_Tag__return_void,
        .untyped_use_count = 0,
        .small_data = { .unused = 0 },
    }, {});
    (void) implicit_ret;

    ExtraIndex const block_extra = gi->insts[block_inst].small_data.block.extra_pl_index;
    u32 const block_end_offset = instOffsetToCurrent(gi, block_inst);
    yy_assert(&the_fn_scope->base == scope);
    auto extra_pl = Yy_Ir_Inst_ExtraPl_Block {
        .end_offset = block_end_offset,
        .break_count_upper_limit = 0,
    };
    genir_unreserveExtraPl(gi, block_extra, &extra_pl);
    genir_unreserveInst(gi, block_inst, node_loc, Inst_init(block, { .extra_pl_index = block_extra }), {});

    return Yy_Ok;
}

NODISCARD static YyStatus genEndBlockBreakVoid(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node_loc, InstIndex const block_inst, Option<InstIndex> const break_to)
{
    InstIndex const actual_break_to = break_to.unwrap_or(block_inst);

    Scope_Base *break_to_scope;
    TRY(scopeLookupMetaInst(gi, scope, node_loc, actual_break_to, &break_to_scope));
    scope_get_checked(break_to_scope, block)->break_count_upper_limit += 1;

    Scope_Base *block_scope;
    TRY(scopeLookupMetaInst(gi, scope, node_loc, block_inst, &block_scope));
    yy_assert(block_scope == scope);

    InstIndex const implicit_break_void = genir_reserveInst(gi);
    genir_unreserveInst(gi, implicit_break_void, node_loc, Inst_init(break_void, {
        .block_to_break_to = genir_instOffsetSB(implicit_break_void, actual_break_to),
    }), {});

    ExtraIndex const block_extra = gi->insts[block_inst].small_data.block.extra_pl_index;
    u32 const block_end_offset = instOffsetToCurrent(gi, block_inst);
    u32 const break_count_upper_limit = scope_get_checked(block_scope, block)->break_count_upper_limit;
    auto extra_pl = Yy_Ir_Inst_ExtraPl_Block{
        .end_offset = block_end_offset,
        .break_count_upper_limit = break_count_upper_limit,
    };
    genir_unreserveExtraPl(gi, block_extra, &extra_pl);
    genir_unreserveInst(gi, block_inst, node_loc, Inst_init(block, { .extra_pl_index = block_extra }), {});

    return Yy_Ok;
}

NODISCARD static YyStatus genEndBlockBreakValue(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node_loc, InstIndex const block_inst, Option<InstIndex> const break_to,
    InstIndex const value_inst)
{
    InstIndex const actual_break_to = break_to.unwrap_or(block_inst);

    Scope_Base *break_to_scope;
    TRY(scopeLookupMetaInst(gi, scope, node_loc, actual_break_to, &break_to_scope));
    scope_get_checked(break_to_scope, block)->break_count_upper_limit += 1;

    Scope_Base *block_scope;
    TRY(scopeLookupMetaInst(gi, scope, node_loc, block_inst, &block_scope));
    yy_assert(block_scope == scope);

    InstIndex const break_value_inst = genir_reserveInst(gi);
    Yy_Ir_Inst_ExtraPl_BreakValue const break_extra_pl = {
        .block_to_break_to = genir_instOffsetSB(break_value_inst, actual_break_to),
        .value = genir_instOffsetSB(break_value_inst, value_inst),
    };
    ExtraIndex const break_extra = genir_appendExtraPl(gi, break_extra_pl);
    genir_unreserveInst(gi, break_value_inst, node_loc, Inst_init(break_value, { .extra_pl_index = break_extra }), {value_inst});

    ExtraIndex const block_extra = gi->insts[block_inst].small_data.block.extra_pl_index;
    u32 const block_end_offset = instOffsetToCurrent(gi, block_inst);
    u32 const break_count_upper_limit = scope_get_checked(block_scope, block)->break_count_upper_limit;
    auto extra_pl = Yy_Ir_Inst_ExtraPl_Block{
        .end_offset = block_end_offset,
        .break_count_upper_limit = break_count_upper_limit,
    };
    genir_unreserveExtraPl(gi, block_extra, &extra_pl);
    genir_unreserveInst(gi, block_inst, node_loc, Inst_init(block, { .extra_pl_index = block_extra }), {});

    return Yy_Ok;
}

// `node_loc` is only for source locations.
NODISCARD static YyStatus genEmptyBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node_loc, Option<InstIndex> const break_to,
    InstIndex *const ret_result)
{
    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node_loc);
            return Yy_Bad;
        }
        break;
    }

    InstIndex const block_inst = genBeginBlock(gi);
    Scope_Block block_scope = scopeCreateBlock(gi, scope, node_loc, block_inst, Yy_Ir_Inst_Tag__block);
    TRY(genEndBlockBreakVoid(gi, &block_scope.base, node_loc, block_inst, break_to));
    TRY(scopeDestroy(gi, &block_scope.base));

    *ret_result = block_inst;
    return Yy_Ok;
}

// NOTE: This code must be kept in sync with `genir_astExprStmtBlock`.
// If `break_to` is not invalid, it will store the result into `break_to` instead
// of the new block. Otherwise, it stores the result into the newly created block.
NODISCARD static YyStatus genExprValueViaBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, Option<InstIndex> const break_to,
    InstIndex *const ret_result)
{
    // // If `break_to` is invalid and `node` is a stmt_block, there's no need to generate an
    // // additional block for ast stmt_block. This is an optimization. I don't want to enable this
    // // right now for debugging reasons.
    // if (!break_to_is_valid && node->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block) {
    //     return genir_expr(gi, irk, node, ret_result);
    // }

    switch (irk) {
        // stmt, value, ref: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    // Make a new `block` instruction which contains only one AstNode for the thing and a
    // break_value/break_void that breaks it immediately after.
    InstIndex const block_inst = genBeginBlock(gi);
    Scope_Block block_scope = scopeCreateBlock(gi, scope, node, block_inst, Yy_Ir_Inst_Tag__block);

    // Generate the inner value.
    InstIndex inner_value_inst;
    TRY(genir_expr(gi, &block_scope.base, irk, node, &inner_value_inst));

    // Generate the `break_value` or `break_void` instruction.
    if (irk == InstResultKind__stmt) {
        TRY(genEndBlockBreakVoid(gi, &block_scope.base, node, block_inst, break_to));
    } else {
        TRY(genEndBlockBreakValue(gi, &block_scope.base, node, block_inst, break_to, inner_value_inst));
    }
    TRY(scopeDestroy(gi, &block_scope.base)); // TODO leak on compiler error

    *ret_result = block_inst;
    return Yy_Ok;
}
NODISCARD static YyStatus genExprValueViaFunctionlessBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node,
    InstIndex *const ret_result)
{
    auto block_inst = genBeginBlock(gi);
    // TODO: Is it fine to have two scopes pointing to the same instruction?
    Scope_Head head_scope = scopeCreateHead(gi, scope, node, block_inst);
    Scope_Block block_scope = scopeCreateBlock(gi, &head_scope.base, node, block_inst, Yy_Ir_Inst_Tag__block);

    // Generate the inner value.
    InstIndex inner_value_inst;
    TRY(genir_expr(gi, &block_scope.base, irk, node, &inner_value_inst));

    // Generate the `break_value` or `break_void` instruction.
    if (irk == InstResultKind__stmt) {
        TRY(genEndBlockBreakVoid(gi, &block_scope.base, node, block_inst, None));
    } else {
        TRY(genEndBlockBreakValue(gi, &block_scope.base, node, block_inst, None, inner_value_inst));
    }
    TRY(scopeDestroy(gi, &block_scope.base)); // TODO leak on compiler error
    TRY(scopeDestroy(gi, &head_scope.base)); // TODO leak on compiler error

    *ret_result = block_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprBoolShortCircuit(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, op_bin);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: ok
        case InstResultKind__value:
        {}
        break;
        // ref: TODO
        case InstResultKind__ref: {
            // TODO Maybe error instead of tmp_ref?
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    bool is_or;
    switch (node_pl->op) {
        default:
            yy_unreachable();
        case Yuceyuf_Ast_BinaryOp__bool_and:
            is_or = false;
            break;
        case Yuceyuf_Ast_BinaryOp__bool_or:
            is_or = true;
            break;
    }

    InstIndex lhs;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->lhs, &lhs));

    InstIndex const cond = lhs;

    // Reserve the cond_branch instruction and extra. This _must_ be reserved after the condition
    // and before the then and else blocks.
    InstIndex const cond_branch_inst = genir_reserveInst(gi);
    ExtraIndex const cond_branch_extra = genir_reserveExtraPl(gi, cond_branch);
    Scope_Block cond_branch_scope = scopeCreateCondBranch(gi, scope, node, cond_branch_inst);

    // `Then` expr
    InstIndex then;
    InstIndex xelse;
    if (is_or) {
        // or: cond_branch(cond: lhs, then: true, xelse: rhs)
        // Generate the then block.
        then = genBeginBlock(gi);
        Scope_Block then_scope = scopeCreateBlock(gi, &cond_branch_scope.base, node, then, Yy_Ir_Inst_Tag__block);
        InstIndex the_bool;
        TRY(genConstBool(gi, &then_scope.base, InstResultKind__value, node, true, &the_bool));
        TRY(genEndBlockBreakValue(gi, &then_scope.base, node, then, cond_branch_inst, the_bool));
        TRY(scopeDestroy(gi, &then_scope.base));

        // Generate the xelse block.
        TRY(genExprValueViaBlock(gi, &cond_branch_scope.base, InstResultKind__value, node_pl->rhs, cond_branch_inst, &xelse));
    } else {
        // and: cond_branch(cond: lhs, then: rhs, xelse: false)
        // Generate the then block.
        TRY(genExprValueViaBlock(gi, &cond_branch_scope.base, InstResultKind__value, node_pl->rhs, cond_branch_inst, &then));

        // Generate the xelse block.
        xelse = genBeginBlock(gi);
        Scope_Block xelse_scope = scopeCreateBlock(gi, &cond_branch_scope.base, node, xelse, Yy_Ir_Inst_Tag__block);
        InstIndex the_bool;
        TRY(genConstBool(gi, &xelse_scope.base, InstResultKind__value, node, false, &the_bool));
        TRY(genEndBlockBreakValue(gi, &xelse_scope.base, node, xelse, cond_branch_inst, the_bool));
        TRY(scopeDestroy(gi, &xelse_scope.base));
    }

    // unreserve the `cond_branch` instruction
    Yy_Ir_Inst_ExtraPl_CondBranch const cond_branch_pl = {
        .cond = genir_instOffsetSB(cond_branch_inst, cond),
        .then = genir_instOffsetSF(cond_branch_inst, then),
        .xelse = genir_instOffsetSF(cond_branch_inst, xelse),
        .break_count_upper_limit = cond_branch_scope.break_count_upper_limit,
    };

    genir_unreserveExtraPl(gi, cond_branch_extra, &cond_branch_pl);
    genir_unreserveInst(gi, cond_branch_inst, node, Inst_init(cond_branch, {
        .extra_pl_index = cond_branch_extra,
    }), {cond});
    TRY(scopeDestroy(gi, &cond_branch_scope.base));

    *ret_result = cond_branch_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprXwhile(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, xwhile);
    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        // ref: TODO
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    // A while loop should look like this.
    // ```
    // $loop = loop({
    //   $loop_inner = block({
    //     $cond = ...
    //     $cond_branch = cond_branch($cond, then: {$_ = block({
    //       // a `continue` ast inside $body, lowers into `break_void($body)`
    //       // a `break` ast inside $body, lowers into `break_void($loop)`
    //       $body = block({ ... })
    //       $continue = block({ ... })
    //       $_ = break_void($cond_branch)
    //     })}, xelse: {$_ = block({
    //         // break loop because condition was false
    //         // put else clause code here with `genExprValueViaBlock`
    //         $_ = break_void($loop)
    //     })})
    //     $_ = break_void($loop_inner)
    //   })
    // })
    // ```

    // NOTE: Remember that the things inside the `loop` block can not reference instruction values
    // from outside that block.


    // TODO: Review node locations.
    // $loop
    InstIndex const loop = genir_reserveInst(gi);
    Scope_Block loop_scope = scopeCreateLoop(gi, scope, node, loop);
    {
        InstIndex const loop_inner = genBeginBlock(gi);
        yy_assert(loop_inner == yy_ir_inst_loop_getLoopInnerInstIndex(loop));
        Scope_Block loop_inner_scope = scopeCreateBlock(gi, &loop_scope.base, node, loop_inner, Yy_Ir_Inst_Tag__block);

        // $cond: Generate the condition
        InstIndex cond;
        TRY(genir_expr(gi, &loop_inner_scope.base, InstResultKind__value, node_pl->cond, &cond));

        // $cond_branch
        InstIndex const cond_branch = genir_reserveInst(gi);
        ExtraIndex const cond_branch_extra = genir_reserveExtraPl(gi, cond_branch);
        Scope_Block cond_branch_scope = scopeCreateCondBranch(gi, &loop_inner_scope.base, node, cond_branch);
        InstIndex then = yy::uninitialized;
        InstIndex xelse = yy::uninitialized;
        {
            then = genBeginBlock(gi);
            Scope_Block then_scope = scopeCreateBlock(gi, &cond_branch_scope.base, node, then, Yy_Ir_Inst_Tag__block);
            {
                // $body: Generate the body.
                {
                    InstIndex const body_inst = genBeginBlock(gi);
                    Scope_Block block_scope = scopeCreateBlock(gi, &then_scope.base, node_pl->body, body_inst, Yy_Ir_Inst_Tag__block);
                    loop_scope.loop_body_scope = &block_scope;
                    InstIndex inner_value_inst;
                    TRY(genir_expr(gi, &block_scope.base, InstResultKind__stmt, node_pl->body, &inner_value_inst));
                    TRY(genEndBlockBreakVoid(gi, &block_scope.base, node_pl->body, body_inst, None));
                    TRY(scopeDestroy(gi, &block_scope.base));
                    loop_scope.loop_body_scope = None;
                }

                // $continue: Generate the continue statement, if there's one.
                if (node_pl->continue_stmt.has_value()) {
                    InstIndex continue_stmt = yy::uninitialized;
                    TRY(genExprValueViaBlock(gi, &then_scope.base, InstResultKind__stmt, node_pl->continue_stmt.unwrap(),
                                             None, &continue_stmt));
                }
            }
            TRY(genEndBlockBreakVoid(gi, &then_scope.base, node, then, cond_branch));
            TRY(scopeDestroy(gi, &then_scope.base));

            // The else statement of the loop, or an empty block.
            if (node_pl->else_stmt.has_value()) {
                TRY(genExprValueViaBlock(gi, &cond_branch_scope.base, irk, node_pl->else_stmt.unwrap(), loop, &xelse));
            } else {
                TRY(genEmptyBlock(gi, &cond_branch_scope.base, irk, node, loop, &xelse));
            }
        }
        // unreserve the `cond_branch` instruction
        Yy_Ir_Inst_ExtraPl_CondBranch const cond_branch_pl = {
            .cond = genir_instOffsetSB(cond_branch, cond),
            .then = genir_instOffsetSF(cond_branch, then),
            .xelse = genir_instOffsetSF(cond_branch, xelse),
            .break_count_upper_limit = cond_branch_scope.break_count_upper_limit,
        };
        genir_unreserveExtraPl(gi, cond_branch_extra, &cond_branch_pl);
        genir_unreserveInst(gi, cond_branch, node, Inst_init(cond_branch, {
            .extra_pl_index = cond_branch_extra,
        }), {cond});
        TRY(scopeDestroy(gi, &cond_branch_scope.base));

        TRY(genEndBlockBreakVoid(gi, &loop_inner_scope.base, node, loop_inner, None));
        TRY(scopeDestroy(gi, &loop_inner_scope.base));
    }
    genir_unreserveInst(gi, loop, node, Inst_init(loop, {
        .break_count_upper_limit = loop_scope.break_count_upper_limit,
    }), {});
    TRY(scopeDestroy(gi, &loop_scope.base));

    *ret_result = loop;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprIf(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    yy_assert(node->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_if ||
                   node->it.tag == Yuceyuf_AstNode_Specific_Tag__expr_if);
    Yuceyuf_AstNode_Specific_Payload_If const *node_pl;
    switch (node->it.tag) {
        case Yuceyuf_AstNode_Specific_Tag__stmt_if:
            node_pl = &node->it.payload.stmt_if;
            break;
        case Yuceyuf_AstNode_Specific_Tag__expr_if:
            node_pl = &node->it.payload.expr_if;
            break;
        default:
            yy_unreachable();
    }

    switch (irk) {
        // stmt, value, ref: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    // Generate the condition
    InstIndex cond;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->cond, &cond));

    // Reserve the cond_branch instruction and extra. This _must_ be reserved after the condition
    // and before the then and else blocks.
    InstIndex const cond_branch_inst = genir_reserveInst(gi);
    ExtraIndex const cond_branch_extra = genir_reserveExtraPl(gi, cond_branch);
    Scope_Block cond_branch_scope = scopeCreateCondBranch(gi, scope, node, cond_branch_inst);

    // `Then` expr
    InstIndex then;
    TRY(genExprValueViaBlock(gi, &cond_branch_scope.base, irk, node_pl->then, cond_branch_inst, &then));

    // `Else` expr. The else block _must_ be generated after the `then` block.
    InstIndex xelse;
    if (node_pl->elze.is_none()) {
        // TODO Maybe there's a better source location than `node`.
        TRY(genEmptyBlock(gi, &cond_branch_scope.base, irk, node, cond_branch_inst, &xelse));
    } else {
        TRY(genExprValueViaBlock(gi, &cond_branch_scope.base, irk, node_pl->elze.unwrap(), cond_branch_inst, &xelse));
    }


    // unreserve the `cond_branch` instruction
    Yy_Ir_Inst_ExtraPl_CondBranch const cond_branch_pl = {
        .cond = genir_instOffsetSB(cond_branch_inst, cond),
        .then = genir_instOffsetSF(cond_branch_inst, then),
        .xelse = genir_instOffsetSF(cond_branch_inst, xelse),
        .break_count_upper_limit = cond_branch_scope.break_count_upper_limit,
    };

    genir_unreserveExtraPl(gi, cond_branch_extra, &cond_branch_pl);
    genir_unreserveInst(gi, cond_branch_inst, node, Inst_init(cond_branch, {
        .extra_pl_index = cond_branch_extra,
    }), {cond});
    TRY(scopeDestroy(gi, &cond_branch_scope.base));

    *ret_result = cond_branch_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprContinue(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, xcontinue);
    (void) node_pl;

    switch (irk) {
        // stmt, value, ref: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    Scope_Block* loop_scope_block = NULL;
    if (node_pl->label.has_value()) {
        IdentifierId const label = node_pl->label.unwrap();
        auto loop_scope = scopeTryLookupMetaNamedLabel(scope, label);
        if (loop_scope.is_none()) {
            auto const label_sv = genir_identifier_sv(gi, label);
            genir_add_hard_error_msg(
                gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                "undeclared label '" Sv_Fmt "'", Sv_Arg(label_sv));
            return Yy_Bad;
        }
        loop_scope_block = scope_get_checked(loop_scope.unwrap(), block);
        if (loop_scope_block->inst_tag != Yy_Ir_Inst_Tag__loop) {
            auto const label_sv = genir_identifier_sv(gi, label);
            genir_add_hard_error_msg(
                gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                "label '" Sv_Fmt "' is not a loop", Sv_Arg(label_sv));
            genir_add_error_note(
                gi, MetaTu_init(Genir_ErrorLocation, ast, loop_scope_block->base.node_loc),
                "label '" Sv_Fmt "' declared here", Sv_Arg(label_sv));
            return Yy_Bad;
        }
    } else {
        auto loop_scope = scopeTryLookupMetaLoop(scope);
        if (loop_scope.is_none()) {
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "continue outside of loop");
            return Yy_Bad;
        }
        loop_scope_block = scope_get_checked(loop_scope.unwrap(), block);
    }

    yy_assert(loop_scope_block != NULL);
    yy_assert(loop_scope_block->inst_tag == Yy_Ir_Inst_Tag__loop);

    if (loop_scope_block->loop_body_scope.is_none()) {
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "continue outside of loop body");
        genir_add_error_note(gi, MetaTu_init(Genir_ErrorLocation, ast, loop_scope_block->base.node_loc), "loop here");
        return Yy_Bad;
    }

    auto block = loop_scope_block->loop_body_scope.unwrap();
    block->break_count_upper_limit += 1;
    auto break_to_block = block->block_like_reserved_inst;
    auto result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst_init(break_void, {
        .block_to_break_to = genir_instOffsetSB(result_inst, break_to_block),
    }), {});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprBreakVoid(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, break_void);
    (void) node_pl;

    switch (irk) {
        // stmt, value, ref: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    Scope_Base* break_to_scope = NULL;
    if (node_pl->label.has_value()) {
        IdentifierId const label = node_pl->label.unwrap();
        auto tmp = scopeTryLookupMetaNamedLabel(scope, label);
        if (tmp.is_none()) {
            Sv const label_sv = genir_identifier_sv(gi, label);
            genir_add_hard_error_msg(
                gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                "undeclared label '" Sv_Fmt "'", Sv_Arg(label_sv));
            return Yy_Bad;
        }
        break_to_scope = tmp.unwrap();
    } else {
        auto tmp = scopeTryLookupMetaLoop(scope);
        if (tmp.is_none()) {
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "break outside of loop");
            return Yy_Bad;
        }
        break_to_scope = tmp.unwrap();
    }

    auto block = scope_get_checked(break_to_scope, block);
    block->break_count_upper_limit += 1;
    auto break_to_block = block->block_like_reserved_inst;

    auto result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst_init(break_void, {
        .block_to_break_to = genir_instOffsetSB(result_inst, break_to_block),
    }), {});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprBreakExpr(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, break_expr);
    (void) node_pl;

    switch (irk) {
        // stmt, value, ref: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    InstIndex value_inst;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->expr, &value_inst));

    Scope_Base* break_to_scope = NULL;
    if (node_pl->label.has_value()) {
        IdentifierId const label = node_pl->label.unwrap();
        auto tmp = scopeTryLookupMetaNamedLabel(scope, label);
        if (tmp.is_none()) {
            Sv const label_sv = genir_identifier_sv(gi, label);
            genir_add_hard_error_msg(
                gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                "undeclared label '" Sv_Fmt "'", Sv_Arg(label_sv));
            return Yy_Bad;
        }
        break_to_scope = tmp.unwrap();
    } else {
        auto tmp = scopeTryLookupMetaLoop(scope);
        if (tmp.is_none()) {
            genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "break outside of loop");
            return Yy_Bad;
        }
        break_to_scope = tmp.unwrap();
    }

    auto block = scope_get_checked(break_to_scope, block);
    block->break_count_upper_limit += 1;
    auto break_to_block = block->block_like_reserved_inst;

    InstIndex const break_value_inst = genir_reserveInst(gi);
    Yy_Ir_Inst_ExtraPl_BreakValue const break_extra_pl = {
        .block_to_break_to = genir_instOffsetSB(break_value_inst, break_to_block),
        .value = genir_instOffsetSB(break_value_inst, value_inst),
    };
    ExtraIndex const break_extra = genir_appendExtraPl(gi, break_extra_pl);
    genir_unreserveInst(gi, break_value_inst, node, Inst_init(break_value, { .extra_pl_index = break_extra }), {value_inst});
    *ret_result = break_value_inst;
    return Yy_Ok;
}
NODISCARD static YyStatus genir_astExprReturnVoid(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, return_void);
    (void) node_pl;

    Scope_Head *the_fn_scope;
    TRY(scopeLookupMetaActualFunction(gi, scope, node, &the_fn_scope));
    the_fn_scope->return_or_break_count_upper_limit += 1;

    // TODO: Check properly that this is inside a function by passing a Scope parameter to
    // `genir_ast*` functions.
    switch (irk) {
        // stmt, value, ref: ok
        // TODO: Maybe `irk` should also have a boolean indicating that the expressino must not be
        // `noreturn`. This would be useful for errors for `return return return ...`. This
        // genir_astExprReturnVoid would always need the target to accept a `noreturn` value. The
        // operand of `return_value` would always take `irk + never_noreturn`. If an Ast that
        // returns `noreturn` was asked with `irk.never_noreturn`, it's an error. Some instructions
        // don't care, like a conditional branch's `then` and `xelse`. Also, it would be good to
        // return whether the instruction resulted in noreturn. This way, conditional branches would
        // also be able to check for `never_noreturn`. TODO I should implement this. (new note: This
        // is easier to implement in Sema. This is already implemented if you ignore that
        // `return foo();` should work even if `foo()` is `noreturn`. And if you want that to be
        // allowed, you must do it in sema _anyway_. Also doing it in genir wouldn't benefit me in
        // any way unless this language was like zig where sema doeasn't check all functions but
        // genir does.)
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    *ret_result = genir_appendInst(gi, node, Inst {
        .tag = Yy_Ir_Inst_Tag__return_void,
        .untyped_use_count = 0,
        .small_data = { .unused = 0 },
    }, {});
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprReturnExpr(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, return_expr);

    Scope_Head *the_fn_scope;
    TRY(scopeLookupMetaActualFunction(gi, scope, node, &the_fn_scope));
    the_fn_scope->return_or_break_count_upper_limit += 1;

    switch (irk) {
        // stmt, value, ref: ok
        // TODO: See comment on genir_astExprReturnVoid about `irk never_noreturn`
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }
    InstIndex operand;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->expr, &operand));
    InstIndex const result_inst = genir_reserveInst(gi);
    genir_unreserveInst(gi, result_inst, node, Inst_init(return_value, {
        .operand = genir_instOffsetSB(result_inst, operand),
    }), {operand});
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprParen(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, paren);
    return genir_expr(gi, scope, irk, *node_pl, ret_result);
}

NODISCARD static YyStatus genir_astExprUnreach(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, unreach);
    (void) node_pl;
    (void) scope;
    switch (irk) {
        // stmt, value, ref: ok
        // TODO: See comment on genir_astExprReturnVoid about `irk never_noreturn`.
        case InstResultKind__stmt:
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }
    *ret_result = genir_appendInst(gi, node, Inst {
        .tag = Yy_Ir_Inst_Tag__xunreachable,
        .untyped_use_count = 0,
        .small_data = { .unused = 0 },
    }, {});
    return Yy_Ok;
}

NODISCARD static YyStatus genIdentifierFromLookup(
    GenIr *const gi, InstResultKind const irk, AstNode *const node, IdentifierId const name_z, ScopeLookupDeclResult const lookup, InstIndex *const ret_result)
{
    yy_assert(lookup.found);

    if (lookup.ss_value_ptr->ast->it.tag == Yuceyuf_AstNode_Specific_Tag__mod_decl) {
        auto name_sv = genir_identifier_sv(gi, name_z);
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node),
                                 "invalid mod_decl reference: '" Sv_Fmt "'",
                                 Sv_Arg(name_sv));
        return Yy_Bad;
    }

    InstIndex const result_inst = genir_reserveInst(gi);
    switch (lookup.ss_value_ptr->tag) {
        case ScopeSymbolValue::Tag::inst: {
            InstIndex const ref_inst = lookup.ss_value_ptr->as.inst;
            auto ref_inst_tag = gi->insts[ref_inst].tag;
            yy_assert(ref_inst_tag == Yy_Ir_Inst_Tag__local_var_decl || ref_inst_tag == Yy_Ir_Inst_Tag__fn_arg_decl);

            Inst_Tag result_inst_tag;
            if (irk == InstResultKind__ref) {
                result_inst_tag = Yy_Ir_Inst_Tag__local_decl_ref;
            } else if (irk == InstResultKind__value || irk == InstResultKind__stmt) {
                result_inst_tag = Yy_Ir_Inst_Tag__local_decl_val;
            } else yy_unreachable();
            genir_unreserveInst(gi, result_inst, node, Inst {
                .tag = result_inst_tag,
                .untyped_use_count = 0,
                .small_data = { .local_decl_val_or_ref = { .ref = ref_inst.asOffsetBackwardFrom(result_inst) } },
            }, {});
        } break;
        case ScopeSymbolValue::Tag::decl: {
            auto ref_decl = lookup.ss_value_ptr->as.decl;
            Inst_Tag result_inst_tag;
            if (irk == InstResultKind__ref) {
                result_inst_tag = Yy_Ir_Inst_Tag__decl_ref;
            } else if (irk == InstResultKind__value || irk == InstResultKind__stmt) {
                result_inst_tag = Yy_Ir_Inst_Tag__decl_val;
            } else yy_unreachable();
            Yy_Ir_Inst_ExtraPl_DeclValOrRef const extra_data = { .ref_decl = ref_decl };
            auto const extra_index = genir_appendExtraPl(gi, extra_data);
            genir_unreserveInst(gi, result_inst, node, Inst {
                .tag = result_inst_tag,
                .untyped_use_count = 0,
                .small_data = { .decl_val_or_ref = { .extra_pl_index = extra_index } },
            }, {});
        } break;
    }
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprIdentifier(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, identifier);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value, ref: ok
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    IdentifierId const name_z = node_pl->identifier;

    ScopeLookupDeclResult lookup;
    TRY(resolveIdentifier(gi, scope, node, name_z, &lookup));
    InstIndex result_inst;
    TRY(genIdentifierFromLookup(gi, irk, node, name_z, lookup, &result_inst));
    *ret_result = result_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus resolveModFieldAccess(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node, IdentifierId const field_name, ScopeLookupDeclResult *const ret_lookup)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_field_access);
    AstNode *const container = node_pl->container;

    ScopeLookupDeclResult container_lookup;
    if (container->it.tag == Yuceyuf_AstNode_Specific_Tag__mod_field_access) {
        auto container_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &container->it, mod_field_access);
        IdentifierId const up_container_field_name = container_pl->field;
        TRY(resolveModFieldAccess(gi, scope, container, up_container_field_name, &container_lookup));
    } else if (container->it.tag == Yuceyuf_AstNode_Specific_Tag__identifier) {
        auto const container_ident_name = MetaTu_get(Yuceyuf_AstNode_Specific, &container->it, identifier)->identifier;
        TRY(resolveIdentifier(gi, scope, container, container_ident_name, &container_lookup));
    } else {
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "invalid container for '::'");
        return Yy_Bad;
    }

    yy_assert(container_lookup.found);

    if (container_lookup.ss_value_ptr->ast->it.tag != Yuceyuf_AstNode_Specific_Tag__mod_decl) {
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "invalid container for '::'");
        genir_add_error_note(gi, MetaTu_init(Genir_ErrorLocation, ast, container_lookup.ss_value_ptr->ast), "container defined here");
        return Yy_Bad;
    }

    Scope_Root *const container_mod_scope = container_lookup.ss_value_ptr->mod_scope.unwrap();

    ScopeLookupDeclResult const lookup = scopeLookupDeclDirect(&container_mod_scope->base, field_name);
    if (!lookup.found) {
        Sv const name_sv = genir_identifier_sv(gi, field_name);
        genir_add_hard_error_msg(gi, MetaTu_init(Genir_ErrorLocation, ast, node), "undeclared mod field '" Sv_Fmt "'", Sv_Arg(name_sv));
        genir_add_error_note(gi, MetaTu_init(Genir_ErrorLocation, ast, container_lookup.ss_value_ptr->ast), "container defined here");
        return Yy_Bad;
    }

    *ret_lookup = lookup;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprModFieldAccess(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_field_access);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value, ref: ok
        case InstResultKind__value:
        case InstResultKind__ref:
        {} break;
    }

    ScopeLookupDeclResult lookup;
    auto const field_name = node_pl->field;
    TRY(resolveModFieldAccess(gi, scope, node, field_name, &lookup));

    InstIndex result_inst;
    TRY(genIdentifierFromLookup(gi, irk, node, field_name, lookup, &result_inst));

    *ret_result = result_inst;
    return Yy_Ok;
}


NODISCARD static YyStatus genir_astExprBuiltinTyp(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    (void) scope;
    yy_assert(node->it.tag == Yuceyuf_AstNode_Specific_Tag__builtin_typ);

    switch (irk) {
        // error: unused result
        case InstResultKind__stmt: {
            // Add the error and keep going.
            TRY(soft_error_unused_result(gi, node));
        }
        break;
        // value: do nothing
        case InstResultKind__value:
        {} break;
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    Inst_Tag const inst_tag = typeInstTagFromTypeAst(node);
    InstIndex const result = genir_appendInst(gi, node, Inst {
        .tag = inst_tag,
        .untyped_use_count = 0,
        .small_data = { .unused = 0 },
    }, {});
    *ret_result = result;
    return Yy_Ok;
}

NODISCARD static ExtraIndex reserveTrailingExtraCallArgsArray(
    GenIr *const gi, ExtraIndex const call_extra, u32 const arg_count)
{
    ExtraIndex const result = genir_reserveExtraArrayPl(gi, arg_count, call_arg);
    yy_assert(result == ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_Call>(gi, call_extra).trailing_start);
    return result;
}

NODISCARD static YyStatus genir_astExprFnCall(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_call);

    switch (irk) {
        // stmt: ok
        case InstResultKind__stmt:
        {} break;
        // value: ok
        case InstResultKind__value:
        {} break;
        case InstResultKind__ref: {
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    // Order of evaluation: left to right

    // Generate the callee
    InstIndex callee;
    // TODO: Is it `value` or `ref`?
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->func, &callee));

    // Generate arguments from left to right.
    u32 const arg_count = intCastFromUsizeToU32(node_pl->args.nodes.size());
    // TODO: Allocate scratch space.
    InstIndex arg_insts[128];
    yy_assert(arg_count <= ARRAY_COUNT(arg_insts));
    {
        for (usize arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
            Yuceyuf_AstNode *arg_node = node_pl->args.nodes[arg_idx];
            TRY(genir_expr(gi, scope, InstResultKind__value, arg_node, &arg_insts[arg_idx]));
        }
    }

    // Reserve `call` extras in the correct order
    InstIndex const call = genir_reserveInst(gi);
    ExtraIndex const call_extra = genir_reserveExtraPl(gi, call);
    ExtraIndex const call_args_array_extra = reserveTrailingExtraCallArgsArray(gi, call_extra, arg_count);

    // Unreserve the call extra.
    Yy_Ir_Inst_ExtraPl_Call const call_extra_data = {
        .callee = genir_instOffsetSB(call, callee),
        .arg_count = arg_count,
    };
    genir_unreserveExtraPl(gi, call_extra, &call_extra_data);

    // Unreserve the argument list.
    for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
        Yy_Ir_Inst_ExtraPl_Call_Arg const arg_data = {
            .inst = genir_instOffsetSB(call, arg_insts[arg_idx]),
        };
        genir_unreserveExtraArrayPl(gi, call_args_array_extra, arg_idx, &arg_data);
    }

    // Append the call instruction.
    genir_unreserveInst(gi, call, node, Inst_init(call, {
        .extra_pl_index = call_extra,
    }), {callee});
    for (usize arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
        referenceInst(gi, arg_insts[arg_idx], call);
    }

    *ret_result = call;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExprLabeledBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, labeled_block);

    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        case InstResultKind__ref: {
            // TODO
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }

    InstIndex const block_inst = genBeginBlock(gi);
    Scope_Block block_scope;
    TRY(scopeCreateLabeledBlock(gi, scope, node, block_inst, Yy_Ir_Inst_Tag__block, node_pl->name, &block_scope));

    // for every ast node, generate instructions
    for (Yuceyuf_AstNode *stmt : node_pl->stmts.nodes) {
        // TODO: Detect unreachable code right here.
        InstIndex stmt_inst;
        TRY(astBlockStmtOrDecl(gi, &block_scope.base, stmt, &stmt_inst));
        (void) stmt_inst;
    }
    // There will always be an implicit break_void (or similar instruction) at the end of every block.
    // If the block happened to already have a `noreturn` instruction, this instruction would be
    // unreachable, but we won't display an error because we _know_ this `break_void` was implicit.
    TRY(genEndBlockBreakVoid(gi, &block_scope.base, node, block_inst, None));
    TRY(scopeDestroy(gi, &block_scope.base)); // TODO leak on compiler errors

    *ret_result = block_inst;
    return Yy_Ok;
}

// NOTE: This code must be kept in sync with `genExprValueViaBlock`.
NODISCARD static YyStatus genir_astExprStmtBlock(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, stmt_block);

    switch (irk) {
        // stmt, value: ok
        case InstResultKind__stmt:
        case InstResultKind__value:
        {} break;
        case InstResultKind__ref: {
            // TODO
            hard_error_todo_tmp_ref(gi, node);
            return Yy_Bad;
        }
        break;
    }


    InstIndex const block_inst = genBeginBlock(gi);
    Scope_Block block_scope = scopeCreateBlock(gi, scope, node, block_inst, Yy_Ir_Inst_Tag__block);

    // for every ast node, generate instructions
    for (Yuceyuf_AstNode *stmt : node_pl->stmts.nodes) {
        // TODO: Detect unreachable code right here.
        InstIndex stmt_inst;
        TRY(astBlockStmtOrDecl(gi, &block_scope.base, stmt, &stmt_inst));
        (void) stmt_inst;
    }
    // There will always be an implicit break_void (or similar instruction) at the end of every block.
    // If the block happened to already have a `noreturn` instruction, this instruction would be
    // unreachable, but we won't display an error because we _know_ this `break_void` was implicit.
    TRY(genEndBlockBreakVoid(gi, &block_scope.base, node, block_inst, None));
    TRY(scopeDestroy(gi, &block_scope.base)); // TODO leak on compiler errors

    *ret_result = block_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astLocalVarDecl(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, local_var_decl);

    InstIndex initial_value_inst;
    TRY(genir_expr(gi, scope, InstResultKind__value, node_pl->initial_value, &initial_value_inst));

    InstIndex const local_var_decl_inst = genir_reserveInst(gi);

    // TODO: Technically, I could reuse the `assign` and `decl_ref` instructions to generate the
    // initialization of this variable. For now, I'll keep it like this.
    auto const name_z = node_pl->name;

    Option<Yy_Ir_InstOffsetSelfForward> type_block_offset = None;
    if (node_pl->ty_expr.has_value()) {
        InstIndex type_block_abs;
        TRY(genExprValueViaFunctionlessBlock(
            gi, scope, InstResultKind__value, node_pl->ty_expr.unwrap(), &type_block_abs));
        type_block_offset = genir_instOffsetSF(local_var_decl_inst, type_block_abs);
    }

    Yy_Ir_Inst_ExtraPl_LocalVarDecl const extra_data = {
        .name_z = name_z,
        .initial_value = genir_instOffsetSB(local_var_decl_inst, initial_value_inst),
        .type_block = type_block_offset,
    };
    ExtraIndex const extra_index = genir_appendExtraPl(gi, extra_data);
    genir_unreserveInst(
        gi, local_var_decl_inst, node, Inst_init(local_var_decl, {.extra_pl_index = extra_index}), {initial_value_inst}
    );

    TRY(scopeAddDeclInst(gi, scope, local_var_decl_inst, name_z));

    *ret_result = local_var_decl_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus astBlockStmtOrDecl(
    GenIr *const gi, Scope_Base *const scope, AstNode *const node, InstIndex *const ret_result)
{
    if (node->it.tag == Yuceyuf_AstNode_Specific_Tag__local_var_decl) {
        return genir_astLocalVarDecl(gi, scope, node, ret_result);
    }
    return genir_expr(gi, scope, InstResultKind__stmt, node, ret_result);
}

/// This can also get pure statements if you provide `InstResultKind__stmt`.
NODISCARD static YyStatus genir_expr(
    GenIr *const gi, Scope_Base *const scope, InstResultKind const irk, AstNode *const node, InstIndex *const ret_result)
{
    switch (node->it.tag) {
        case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
            yy_unreachable();
        case Yuceyuf_AstNode_Specific_Tag__root:
        case Yuceyuf_AstNode_Specific_Tag__imported_file:
        case Yuceyuf_AstNode_Specific_Tag__mod_decl:
        case Yuceyuf_AstNode_Specific_Tag__struct_decl:
        case Yuceyuf_AstNode_Specific_Tag__struct_decl_member_info:
        case Yuceyuf_AstNode_Specific_Tag__fn_decl:
        case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info:
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info:
        case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl:
        case Yuceyuf_AstNode_Specific_Tag__global_var_decl:
        case Yuceyuf_AstNode_Specific_Tag__extern_var_decl:
        case Yuceyuf_AstNode_Specific_Tag__local_var_decl: {
            logError("%s: unexpected `" Sv_Fmt "`. This is a parser bug.", __func__,
                     Sv_Arg(MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag)));
            genir_ice();
        }
        break;
        case Yuceyuf_AstNode_Specific_Tag__builtin_typ: return genir_astExprBuiltinTyp(gi ,scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__field_access: return genir_astExprFieldAccess(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__mod_field_access: return genir_astExprModFieldAccess(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__fn_call: return genir_astExprFnCall(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__identifier: return genir_astExprIdentifier(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__paren: return genir_astExprParen(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__ignore_result: return genir_astExprIgnoreResult(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__integer_literal: return genir_astExprIntegerLiteral(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__char_literal: return genir_astExprCharLiteral(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__string_literal: return genir_astExprStringLiteral(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__bool_literal: return genir_astExprBoolLiteral(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__undefined_literal: return genir_astExprUndefinedLiteral(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__stmt_block: return genir_astExprStmtBlock(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__labeled_block: return genir_astExprLabeledBlock(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call: return genir_astExprBuiltinFnCall(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__assign: return genir_astExprAssign(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__stmt_if: return genir_astExprIf(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__expr_if: return genir_astExprIf(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__unreach: return genir_astExprUnreach(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__op_bin: return genir_astExprOpBin(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__assign_bin_op: return genir_astExprAssignOpBin(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix: return genir_astExprOpUnaryPrefix(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__deref: return genir_astExprDeref(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__xwhile: return genir_astExprXwhile(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__return_void: return genir_astExprReturnVoid(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__return_expr: return genir_astExprReturnExpr(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__xcontinue: return genir_astExprContinue(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__break_void: return genir_astExprBreakVoid(gi, scope, irk, node, ret_result);
        case Yuceyuf_AstNode_Specific_Tag__break_expr: return genir_astExprBreakExpr(gi, scope, irk, node, ret_result);
    }
    yy_unreachable();
}

NODISCARD static ExtraIndex reserveTrailingExtraArrayStructDeclMembers(
    GenIr *const gi, ExtraIndex const struct_decl_extra, u32 const arg_count)
{
    ExtraIndex const result = genir_reserveExtraArrayPl(gi, arg_count, struct_decl_member_info);
    yy_assert(result == ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_StructDecl>(gi, struct_decl_extra).trailing_start);
    return result;
}
NODISCARD static YyStatus genir_astStructDecl(
    GenIr *const gi, Scope_Root *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, struct_decl);

    // Reserve instruction and extras in the correct order.
    u32 const member_count = intCastFromUsizeToU32(node_pl->members.nodes.size());
    InstIndex const struct_decl_inst = genir_reserveInst(gi);
    ExtraIndex const struct_decl_extra = genir_reserveExtraPl(gi, struct_decl);
    ExtraIndex const members_extra = reserveTrailingExtraArrayStructDeclMembers(gi, struct_decl_extra, member_count);
    auto const struct_decl_name_z = node_pl->name;
    auto const decl_id = scope->symbols.get({}, struct_decl_name_z).unwrap().value.as.decl;

    // Order of instructions: struct_decl, members types...

    // Members
    // Make StructDecl_MemberInfo
    for (u32 member_idx = 0; member_idx < member_count; member_idx += 1) {
        Yuceyuf_AstNode *member_node = node_pl->members.nodes[member_idx];
        auto const member_node_pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &member_node->it, struct_decl_member_info);
        Option<IdentifierId> const member_name_z = member_node_pl.name;
        InstIndex member_type_eval_block;
        TRY(genExprValueViaFunctionlessBlock(
            gi, &scope->base, InstResultKind__value, member_node_pl.ty_expr, &member_type_eval_block));
        auto const member_data = Yy_Ir_Inst_ExtraPl_StructDecl_MemberInfo{
            .name_z = member_name_z,
            .type_eval_block = genir_instOffsetSF(struct_decl_inst, member_type_eval_block),
        };
        genir_unreserveExtraArrayPl(gi, members_extra, member_idx, &member_data);
    }

    // Unreserve the struct_decl instruction and extra.
    auto const struct_decl_end_offset = instOffsetToCurrent(gi, struct_decl_inst);
    auto const struct_decl_extra_data = Yy_Ir_Inst_ExtraPl_StructDecl {
        .decl_id = decl_id,
        .end_offset = struct_decl_end_offset,
        .name_z = struct_decl_name_z,
        .member_count = member_count,
    };
    genir_unreserveExtraPl(gi, struct_decl_extra, &struct_decl_extra_data);
    genir_unreserveInst(gi, struct_decl_inst, node, Inst_init(struct_decl, {
        .extra_pl_index = struct_decl_extra,
    }), {});
    setDeclInst(gi, decl_id, struct_decl_inst);

    *ret_result = struct_decl_inst;
    return Yy_Ok;
}

NODISCARD static ExtraIndex reserveTrailingExtraArrayFnDeclArgs(
    GenIr *const gi, ExtraIndex const fn_decl_extra, u32 const arg_count)
{
    ExtraIndex const result = genir_reserveExtraArrayPl(gi, arg_count, fn_decl_arg_info);
    yy_assert(result == ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_FnDecl>(gi, fn_decl_extra).trailing_start);
    return result;
}
NODISCARD static YyStatus genir_astFnDecl(
    GenIr *const gi, Scope_Root *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_decl);

    // Reserve instruction and extras in the correct order.
    u32 const arg_count = intCastFromUsizeToU32(node_pl->args.nodes.size());
    InstIndex const fn_decl_inst = genir_reserveInst(gi);
    ExtraIndex const fn_decl_extra = genir_reserveExtraPl(gi, fn_decl);
    ExtraIndex const args_extra = reserveTrailingExtraArrayFnDeclArgs(gi, fn_decl_extra, arg_count);
    auto const fn_decl_name_z = node_pl->name;
    auto const decl_id = scope->symbols.get({}, fn_decl_name_z).unwrap().value.as.decl;

    // Order of instructions: fn_decl, arg types..., ret type, body

    // Args
    // Make FnDecl_ArgInfo
    for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
        Yuceyuf_AstNode *arg_node = node_pl->args.nodes[arg_idx];
        auto const arg_node_pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &arg_node->it, fn_decl_arg_info);
        Option<IdentifierId> const arg_name_z = arg_node_pl.name;
        InstIndex arg_type_eval_block;
        TRY(genExprValueViaFunctionlessBlock(
            gi, &scope->base, InstResultKind__value, arg_node_pl.ty_expr, &arg_type_eval_block));
        Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo const arg_data = {
            .name_z = arg_name_z,
            .type_eval_block = genir_instOffsetSF(fn_decl_inst, arg_type_eval_block),
        };
        genir_unreserveExtraArrayPl(gi, args_extra, arg_idx, &arg_data);
    }

    // Return type
    InstIndex return_type_eval_block;
    TRY(genExprValueViaFunctionlessBlock(gi, &scope->base, InstResultKind__value, node_pl->return_type, &return_type_eval_block));

    // Generate the function body.
    InstIndex const body_block = genBeginBlock(gi);
    Scope_Head fn_scope = scopeCreateHeadActualFunction(gi, scope, node, fn_decl_inst);
    {
        // Generate `fn_arg_decl` instructions.
        for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
            Yuceyuf_AstNode *arg_node = node_pl->args.nodes[arg_idx];
            auto const arg_node_pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &arg_node->it, fn_decl_arg_info);
            Option<IdentifierId> const arg_name_z = arg_node_pl.name;

            InstIndex const fn_arg_decl_inst =
                genir_appendInst(gi, arg_node, Inst_init(fn_arg_decl, {.arg_idx = arg_idx}), {});
            yy_assert(fn_arg_decl_inst == body_block + 1 + arg_idx);
            if (arg_name_z.has_value())
                TRY(scopeAddDeclInst(gi, &fn_scope.base, fn_arg_decl_inst, arg_name_z.unwrap()));
        }
        // Generate the body
        yy_assert(node_pl->body->it.tag == Yuceyuf_AstNode_Specific_Tag__stmt_block);
        InstIndex inner_block_inst;
        // TODO Maybe this block should be inlined.
        TRY(genir_astExprStmtBlock(gi, &fn_scope.base, InstResultKind__stmt, node_pl->body, &inner_block_inst));
        TRY(genEndBlockReturnVoid(gi, &fn_scope.base, node_pl->body, body_block));
    }

    u32 const return_count_upper_limit = fn_scope.return_or_break_count_upper_limit;
    // There's always the implicit `return_void`.
    yy_assert(return_count_upper_limit >= 1);

    // Unreserve the fn_decl instruction and extra.
    Yy_Ir_Inst_ExtraPl_FnDecl const fn_decl_extra_data = {
        .decl_id = decl_id,
        .name_z = fn_decl_name_z,
        .return_count_upper_limit = return_count_upper_limit,
        .return_type_eval_block_offset = genir_instOffsetSF(fn_decl_inst, return_type_eval_block),
        .body_block_offset = genir_instOffsetSF(fn_decl_inst, body_block),
        .arg_count = arg_count,
    };
    genir_unreserveExtraPl(gi, fn_decl_extra, &fn_decl_extra_data);
    genir_unreserveInst(gi, fn_decl_inst, node, Inst_init(fn_decl, {
        .extra_pl_index = fn_decl_extra,
    }), {});
    TRY(scopeDestroy(gi, &fn_scope.base)); // TODO leak on compiler errors
    setDeclInst(gi, decl_id, fn_decl_inst);

    *ret_result = fn_decl_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astExternVarDecl(
    GenIr *const gi, Scope_Root *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_var_decl);

    // NOTE: This must be the first generated instruction.
    InstIndex const decl_inst = genir_reserveInst(gi);
    ExtraIndex const pl_extra = genir_reserveExtraPl(gi, extern_var_decl);
    auto const name_z = node_pl->name;
    auto const decl_id = scope->symbols.get({}, name_z).unwrap().value.as.decl;

    yy_assert(node_pl->ty_expr != NULL);
    InstIndex type_block_abs;
    TRY(genExprValueViaFunctionlessBlock(gi, &scope->base, InstResultKind__value, node_pl->ty_expr, &type_block_abs));
    auto type_block_offset = genir_instOffsetSF(decl_inst, type_block_abs);

    Yy_Ir_Inst_ExtraPl_ExternVarDecl const decl_pl = {
        .decl_id = decl_id,
        .name_z = name_z,
        .type_block = type_block_offset,
    };
    genir_unreserveExtraPl(gi, pl_extra, &decl_pl);
    genir_unreserveInst(gi, decl_inst, node, Inst_init(extern_var_decl, {
        .extra_pl_index = pl_extra,
    }), {});
    setDeclInst(gi, decl_id, decl_inst);

    *ret_result = decl_inst;
    return Yy_Ok;
}

NODISCARD static ExtraIndex reserveTrailingExtraArrayExternFnDeclArgs(
    GenIr *const gi, ExtraIndex const extern_fn_decl_extra, u32 const arg_count)
{
    ExtraIndex const result = genir_reserveExtraArrayPl(gi, arg_count, extern_fn_decl_arg_info);
    yy_assert(result == ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_ExternFnDecl>(gi, extern_fn_decl_extra).trailing_start);
    return result;
}


NODISCARD static YyStatus genir_astExternFnDecl(
    GenIr *const gi, Scope_Root *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_fn_decl);

    // Reserve instruction and extras in the correct order.
    u32 const arg_count = intCastFromUsizeToU32(node_pl->args.nodes.size());
    InstIndex const extern_fn_decl_inst = genir_reserveInst(gi);
    ExtraIndex const extern_fn_decl_extra = genir_reserveExtraPl(gi, extern_fn_decl);
    ExtraIndex const args_extra = reserveTrailingExtraArrayExternFnDeclArgs(gi, extern_fn_decl_extra, arg_count);
    auto const fn_name_z = node_pl->name;
    auto const decl_id = scope->symbols.get({}, fn_name_z).unwrap().value.as.decl;

    // Order of instructions: fn_decl, arg types..., ret type

    // Args
    {
        // Make ExternFnDecl_ArgInfo
        for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
            Yuceyuf_AstNode *arg_node = node_pl->args.nodes[arg_idx];
            auto const arg_node_pl = *MetaTu_get(Yuceyuf_AstNode_Specific, &arg_node->it, extern_fn_decl_arg_info);
            Option<IdentifierId> const arg_name_z = arg_node_pl.name;
            InstIndex arg_type_eval_block;
            TRY(genExprValueViaFunctionlessBlock(
                gi, &scope->base, InstResultKind__value, arg_node_pl.type_expr, &arg_type_eval_block));
            Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo const arg_data = {
                .name_z = arg_name_z,
                .type_eval_block = genir_instOffsetSF(extern_fn_decl_inst, arg_type_eval_block),
            };
            genir_unreserveExtraArrayPl(gi, args_extra, arg_idx, &arg_data);
        }
    }

    // Return type
    InstIndex return_type_eval_block;
    TRY(genExprValueViaFunctionlessBlock(gi, &scope->base, InstResultKind__value, node_pl->return_type, &return_type_eval_block));

    Yy_Ir_Inst_ExtraPl_ExternFnDecl const extern_fn_decl_extra_data = {
        .decl_id = decl_id,
        .name_z = fn_name_z,
        .return_type_eval_block_offset = genir_instOffsetSF(extern_fn_decl_inst, return_type_eval_block),
        .arg_count = arg_count,
    };
    genir_unreserveExtraPl(gi, extern_fn_decl_extra, &extern_fn_decl_extra_data);
    genir_unreserveInst(gi, extern_fn_decl_inst, node, Inst_init(extern_fn_decl, {
        .extra_pl_index = extern_fn_decl_extra,
    }), {});
    setDeclInst(gi, decl_id, extern_fn_decl_inst);

    *ret_result = extern_fn_decl_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astGlobalVarDecl(
    GenIr *const gi, Scope_Root *const scope, AstNode *const node, InstIndex *const ret_result)
{
    auto node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, global_var_decl);

    (void) scope;

    // NOTE: This must be the first generated instruction.
    InstIndex const decl_inst = genir_reserveInst(gi);
    ExtraIndex const pl_extra = genir_reserveExtraPl(gi, global_var_decl);
    auto const name_z = node_pl->name;
    auto const decl_id = scope->symbols.get({}, name_z).unwrap().value.as.decl;

    auto type_block_offset = Option<Yy_Ir_InstOffsetSelfForward>(None);
    if (node_pl->ty_expr.has_value()) {
        InstIndex type_block_abs;
        TRY(genExprValueViaFunctionlessBlock(
            gi, &scope->base, InstResultKind__value, node_pl->ty_expr.unwrap(), &type_block_abs));
        type_block_offset = genir_instOffsetSF(decl_inst, type_block_abs);
    }

    InstIndex init_block;
    TRY(genExprValueViaFunctionlessBlock(gi, &scope->base, InstResultKind__value, node_pl->initial_value, &init_block));

    Yy_Ir_Inst_ExtraPl_GlobalVarDecl const decl_pl = {
        .decl_id = decl_id,
        .name_z = name_z,
        .type_block = type_block_offset,
        .init_block = genir_instOffsetSF(decl_inst, init_block),
    };
    genir_unreserveExtraPl(gi, pl_extra, &decl_pl);
    genir_unreserveInst(gi, decl_inst, node, Inst_init(global_var_decl, {
        .extra_pl_index = pl_extra,
    }), {});
    setDeclInst(gi, decl_id, decl_inst);

    *ret_result = decl_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astRoot(GenIr *gi, Scope_Root *root_scope, Yuceyuf_AstNode *const root_node, DeclId root_decl_id, InstIndex *const ret_result);
NODISCARD static YyStatus genir_astModDecl(
    GenIr *const gi, Scope_Root *const parent_scope, AstNode *const node, InstIndex *const ret_result)
{
    auto* node_pl = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_decl);

    auto const name_z = node_pl->name;

    Scope_Root *mod_scope = yy::uninitialized;
    DeclId mod_decl_id = yy::uninitialized;
    {
        auto get = parent_scope->symbols.get({}, name_z).unwrap();
        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
        mod_decl_id = get.value.as.decl;
        mod_scope = get.value.mod_scope.unwrap();
    }
    InstIndex mod_root_inst;
    TRY(genir_astRoot(gi, mod_scope, node, mod_decl_id, &mod_root_inst));

    if (mod_scope->has_error_decls) {
        parent_scope->has_error_decls = true;
        gi->decl_ptrs[mod_decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
    }

    *ret_result = mod_root_inst;
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astRoot(GenIr *gi, Scope_Root *root_scope, Yuceyuf_AstNode *const root_node, DeclId root_decl_id, InstIndex *const ret_result)
{
    Yuceyuf_AstList const& contents = (root_node->it.is_root()) ? root_node->it.as_root().unwrap().top_list
                                      : (root_node->it.is_mod_decl())
                                          ? root_node->it.as_mod_decl().unwrap().contents
                                          : yy::never<Yuceyuf_AstList const&>();

    InstIndex const root_inst = genir_reserveInst(gi);
    auto inner = [&]()->YyStatus{
        logTrace("root #children: %zu", contents.nodes.size());
        for (usize i = 0; i < contents.nodes.size(); i++) {
            AstNode* node = contents.nodes[i];
            logTrace("root.children[%zu]: " Sv_Fmt "", i, Sv_Arg(MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag)));
            auto const error_skip_idx = gi->insts.next_index();
            switch (node->it.tag) {
                case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
                    yy_unreachable();
                case Yuceyuf_AstNode_Specific_Tag__root:
                case Yuceyuf_AstNode_Specific_Tag__struct_decl_member_info:
                case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info:
                case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info:
                case Yuceyuf_AstNode_Specific_Tag__local_var_decl:
                case Yuceyuf_AstNode_Specific_Tag__fn_call:
                case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call:
                case Yuceyuf_AstNode_Specific_Tag__stmt_block:
                case Yuceyuf_AstNode_Specific_Tag__labeled_block:
                case Yuceyuf_AstNode_Specific_Tag__return_void:
                case Yuceyuf_AstNode_Specific_Tag__return_expr:
                case Yuceyuf_AstNode_Specific_Tag__xcontinue:
                case Yuceyuf_AstNode_Specific_Tag__break_void:
                case Yuceyuf_AstNode_Specific_Tag__break_expr:
                case Yuceyuf_AstNode_Specific_Tag__assign_bin_op:
                case Yuceyuf_AstNode_Specific_Tag__assign:
                case Yuceyuf_AstNode_Specific_Tag__op_bin:
                case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix:
                case Yuceyuf_AstNode_Specific_Tag__xwhile:
                case Yuceyuf_AstNode_Specific_Tag__stmt_if:
                case Yuceyuf_AstNode_Specific_Tag__expr_if:
                case Yuceyuf_AstNode_Specific_Tag__field_access:
                case Yuceyuf_AstNode_Specific_Tag__mod_field_access:
                case Yuceyuf_AstNode_Specific_Tag__identifier:
                case Yuceyuf_AstNode_Specific_Tag__integer_literal:
                case Yuceyuf_AstNode_Specific_Tag__char_literal:
                case Yuceyuf_AstNode_Specific_Tag__string_literal:
                case Yuceyuf_AstNode_Specific_Tag__builtin_typ:
                case Yuceyuf_AstNode_Specific_Tag__bool_literal:
                case Yuceyuf_AstNode_Specific_Tag__undefined_literal:
                case Yuceyuf_AstNode_Specific_Tag__unreach:
                case Yuceyuf_AstNode_Specific_Tag__deref:
                case Yuceyuf_AstNode_Specific_Tag__ignore_result:
                case Yuceyuf_AstNode_Specific_Tag__paren:
                    yy_unreachable();
                case Yuceyuf_AstNode_Specific_Tag__imported_file: {
                    logDebug(Sv_Fmt, Sv_Arg(MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag)));
                    todo();
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__mod_decl: {
                    InstIndex mod_root_inst;
                    if (yy_is_err(genir_astModDecl(gi, root_scope, node, &mod_root_inst))) {
                        auto const name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, mod_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        yy_assert(gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen);
                        yy_assert(root_scope->has_error_decls);
                        return Yy_Bad;
                    } else {
                        yy_assert(mod_root_inst == error_skip_idx);
                    }
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__struct_decl: {
                    InstIndex decl_inst;
                    if (yy_is_err(genir_astStructDecl(gi, root_scope, node, &decl_inst))) {
                        auto name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, struct_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
                        auto error_skip_extra_data =
                            Yy_Ir_Inst_ExtraPl_DeclErrorSkip{ .decl_id = decl_id, .end_offset = instOffsetToCurrent(gi, error_skip_idx), };
                        auto error_skip_extra_index = genir_appendExtraPl(gi, error_skip_extra_data);
                        gi->insts[error_skip_idx] = Inst_init(decl_error_skip, { .extra_pl_index = error_skip_extra_index });
                        setDeclInst(gi, decl_id, error_skip_idx);
                        root_scope->has_error_decls = true;
                        if (gi->errors.hard_count >= ERROR_LIMIT)
                            return Yy_Bad;
                    } else {
                        yy_assert(decl_inst == error_skip_idx);
                    }
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__fn_decl: {
                    InstIndex decl_inst;
                    if (yy_is_err(genir_astFnDecl(gi, root_scope, node, &decl_inst))) {
                        auto name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, fn_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
                        auto error_skip_extra_data =
                            Yy_Ir_Inst_ExtraPl_DeclErrorSkip{ .decl_id = decl_id, .end_offset = instOffsetToCurrent(gi, error_skip_idx), };
                        auto error_skip_extra_index = genir_appendExtraPl(gi, error_skip_extra_data);
                        gi->insts[error_skip_idx] = Inst_init(decl_error_skip, { .extra_pl_index = error_skip_extra_index });
                        setDeclInst(gi, decl_id, error_skip_idx);
                        root_scope->has_error_decls = true;
                        if (gi->errors.hard_count >= ERROR_LIMIT)
                            return Yy_Bad;
                    } else {
                        yy_assert(decl_inst == error_skip_idx);
                    }
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__global_var_decl: {
                    InstIndex decl_inst;
                    if (yy_is_err(genir_astGlobalVarDecl(gi, root_scope, node, &decl_inst))) {
                        auto const name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, global_var_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
                        auto error_skip_extra_data = Yy_Ir_Inst_ExtraPl_DeclErrorSkip{
                            .decl_id = decl_id,
                            .end_offset = instOffsetToCurrent(gi, error_skip_idx),
                        };
                        auto error_skip_extra_index = genir_appendExtraPl(gi, error_skip_extra_data);
                        gi->insts[error_skip_idx] = Inst_init(decl_error_skip, { .extra_pl_index = error_skip_extra_index });
                        setDeclInst(gi, decl_id, error_skip_idx);
                        root_scope->has_error_decls = true;
                        if (gi->errors.hard_count >= ERROR_LIMIT)
                            return Yy_Bad;
                    } else {
                        yy_assert(decl_inst == error_skip_idx);
                    }
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__extern_var_decl: {
                    InstIndex decl_inst;
                    if (yy_is_err(genir_astExternVarDecl(gi, root_scope, node, &decl_inst))) {
                        auto name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_var_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
                        auto error_skip_extra_data = Yy_Ir_Inst_ExtraPl_DeclErrorSkip{
                            .decl_id = decl_id,
                            .end_offset = instOffsetToCurrent(gi, error_skip_idx),
                        };
                        auto error_skip_extra_index = genir_appendExtraPl(gi, error_skip_extra_data);
                        gi->insts[error_skip_idx] = Inst_init(decl_error_skip, { .extra_pl_index = error_skip_extra_index });
                        setDeclInst(gi, decl_id, error_skip_idx);
                        root_scope->has_error_decls = true;
                        if (gi->errors.hard_count >= ERROR_LIMIT)
                            return Yy_Bad;
                    } else {
                        yy_assert(decl_inst == error_skip_idx);
                    }
                }
                break;
                case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl: {
                    InstIndex decl_inst;
                    if (yy_is_err(genir_astExternFnDecl(gi, root_scope, node, &decl_inst))) {
                        auto name = MetaTu_get(Yuceyuf_AstNode_Specific, &node->it, extern_fn_decl)->name;
                        auto const get = root_scope->symbols.get({}, name).unwrap();
                        yy_assert(get.value.ast == node && get.value.tag == ScopeSymbolValue::Tag::decl);
                        auto decl_id = get.value.as.decl;
                        gi->decl_ptrs[decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
                        auto error_skip_extra_data = Yy_Ir_Inst_ExtraPl_DeclErrorSkip{
                            .decl_id = decl_id,
                            .end_offset = instOffsetToCurrent(gi, error_skip_idx),
                        };
                        auto error_skip_extra_index = genir_appendExtraPl(gi, error_skip_extra_data);
                        gi->insts[error_skip_idx] = Inst_init(decl_error_skip, { .extra_pl_index = error_skip_extra_index });
                        setDeclInst(gi, decl_id, error_skip_idx);
                        root_scope->has_error_decls = true;
                        if (gi->errors.hard_count >= ERROR_LIMIT)
                            return Yy_Bad;
                    } else {
                        yy_assert(decl_inst == error_skip_idx);
                    }
                }
                break;
            }
        }
        return Yy_Ok;
    };

    if (yy_is_err(inner())) {
        yy_assert(gi->decl_ptrs[root_decl_id]->has_genir_hard_errors_todo_collect_vs_gen);
        yy_assert(root_scope->has_error_decls);
        return Yy_Bad;
    }

    if (root_scope->has_error_decls) {
        gi->decl_ptrs[root_decl_id]->has_genir_hard_errors_todo_collect_vs_gen = true;
    }

    Yy_Ir_Inst_ExtraPl_Root extra_data = {
        .decl_id = root_decl_id,
        .end_offset = instOffsetToCurrent(gi, root_inst),
    };
    auto extra_index = genir_appendExtraPl(gi, extra_data);
    genir_unreserveInst(gi, root_inst, root_node, Inst_init(root, { .extra_pl_index = extra_index }), {});
    setDeclInst(gi, root_decl_id, root_inst);
    *ret_result = root_inst;

    // We may have inner errors.
    return Yy_Ok;
}

NODISCARD static YyStatus collectDeclsAstRoot(
    GenIr *const gi, Scope_Root *const root_scope, Yuceyuf_AstNode *const root_node)
{
    Yuceyuf_AstList const& contents = (root_node->it.is_root()) ? root_node->it.as_root().unwrap().top_list
                                      : (root_node->it.is_mod_decl())
                                          ? root_node->it.as_mod_decl().unwrap().contents
                                          : yy::never<Yuceyuf_AstList const&>();

    logTrace("%s: root #children: %zu", __func__, contents.nodes.size());
    for (usize i = 0; i < contents.nodes.size(); i++) {
        AstNode* node = contents.nodes[i];
        logTrace("%s: root.children[%zu]: " Sv_Fmt "", __func__, i, Sv_Arg(MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag)));
        switch (node->it.tag) {
            case MetaEnum_count(Yuceyuf_AstNode_Specific_Tag):
                yy_unreachable();
            case Yuceyuf_AstNode_Specific_Tag__root:
            case Yuceyuf_AstNode_Specific_Tag__struct_decl_member_info:
            case Yuceyuf_AstNode_Specific_Tag__fn_decl_arg_info:
            case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl_arg_info:
            case Yuceyuf_AstNode_Specific_Tag__local_var_decl:
            case Yuceyuf_AstNode_Specific_Tag__fn_call:
            case Yuceyuf_AstNode_Specific_Tag__builtin_fn_call:
            case Yuceyuf_AstNode_Specific_Tag__stmt_block:
            case Yuceyuf_AstNode_Specific_Tag__labeled_block:
            case Yuceyuf_AstNode_Specific_Tag__return_void:
            case Yuceyuf_AstNode_Specific_Tag__return_expr:
            case Yuceyuf_AstNode_Specific_Tag__xcontinue:
            case Yuceyuf_AstNode_Specific_Tag__break_void:
            case Yuceyuf_AstNode_Specific_Tag__break_expr:
            case Yuceyuf_AstNode_Specific_Tag__assign_bin_op:
            case Yuceyuf_AstNode_Specific_Tag__assign:
            case Yuceyuf_AstNode_Specific_Tag__op_bin:
            case Yuceyuf_AstNode_Specific_Tag__op_unary_prefix:
            case Yuceyuf_AstNode_Specific_Tag__xwhile:
            case Yuceyuf_AstNode_Specific_Tag__stmt_if:
            case Yuceyuf_AstNode_Specific_Tag__expr_if:
            case Yuceyuf_AstNode_Specific_Tag__field_access:
            case Yuceyuf_AstNode_Specific_Tag__mod_field_access:
            case Yuceyuf_AstNode_Specific_Tag__identifier:
            case Yuceyuf_AstNode_Specific_Tag__integer_literal:
            case Yuceyuf_AstNode_Specific_Tag__char_literal:
            case Yuceyuf_AstNode_Specific_Tag__string_literal:
            case Yuceyuf_AstNode_Specific_Tag__builtin_typ:
            case Yuceyuf_AstNode_Specific_Tag__bool_literal:
            case Yuceyuf_AstNode_Specific_Tag__undefined_literal:
            case Yuceyuf_AstNode_Specific_Tag__unreach:
            case Yuceyuf_AstNode_Specific_Tag__deref:
            case Yuceyuf_AstNode_Specific_Tag__ignore_result:
            case Yuceyuf_AstNode_Specific_Tag__paren:
                yy_unreachable();
            case Yuceyuf_AstNode_Specific_Tag__imported_file: {
                logDebug(Sv_Fmt, Sv_Arg(MetaEnum_sv_short(Yuceyuf_AstNode_Specific_Tag, node->it.tag)));
                todo();
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__mod_decl: {
                auto const& pl = node->it.as_mod_decl().unwrap();
                Scope_Root *const mod_scope = scopeCreateMod(gi, root_scope, node);
                TRY(collectDeclsAstRoot(gi, mod_scope, node));
                auto const name = pl.name;
                TRY(scopeAddDeclMod(gi, root_scope, node, name, mod_scope));
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__struct_decl: {
                auto const& pl = node->it.as_struct_decl().unwrap();
                auto const name = pl.name;
                TRY(scopeAddDeclAst(gi, root_scope, node, name));
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__fn_decl: {
                auto const& pl = node->it.as_fn_decl().unwrap();
                auto const name = pl.name;
                TRY(scopeAddDeclAst(gi, root_scope, node, name));
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__global_var_decl: {
                auto const& pl = node->it.as_global_var_decl().unwrap();
                auto const name = pl.name;
                TRY(scopeAddDeclAst(gi, root_scope, node, name));
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__extern_var_decl: {
                auto const& pl = node->it.as_extern_var_decl().unwrap();
                auto const name = pl.name;
                TRY(scopeAddDeclAst(gi, root_scope, node, name));
            }
            break;
            case Yuceyuf_AstNode_Specific_Tag__extern_fn_decl: {
                auto const& pl = node->it.as_extern_fn_decl().unwrap();
                auto const name = pl.name;
                TRY(scopeAddDeclAst(gi, root_scope, node, name));
            }
            break;
        }
    }
    return Yy_Ok;
}

NODISCARD static YyStatus genir_astTopRoot(GenIr *const gi, Yuceyuf_AstNode *const top_root_node)
{
    yy_assert(gi->errors.msgs.size() == 0);
    yy_assert(gi->errors.hard_count == 0);

    Timer t;
    timer_init(&t);
    UNUSED u64 lap;

    {
        auto top_root_decl_id = makeDecl(gi, top_root_node, None);
        // TODO: Workaround for RAII with early return: leak it.
        Scope_Root* root_scope = scopeCreateTopRoot(gi, top_root_node);

        TRY(collectDeclsAstRoot(gi, root_scope, top_root_node));
        LOG_LAP(&t, lap, "collectDeclsAstRoot");

        InstIndex top_root_inst;
        TRY(genir_astRoot(gi, root_scope, top_root_node, top_root_decl_id, &top_root_inst));
        LOG_LAP(&t, lap, "genir_astRoot");

        yy_assert(top_root_inst.x == 0);
        yy_assert(top_root_decl_id.x == 0);
        if (gi->errors.msgs.size() > 0) {
            yy_assert(
                (root_scope->has_error_decls &&
                 gi->decl_ptrs[top_root_decl_id]->has_genir_hard_errors_todo_collect_vs_gen) ||
                gi->errors.soft_count > 0
            );
        }
        TRY(scopeDestroy(gi, &root_scope->base)); // TODO Leak on compiler error above.
        allocator_delete(gi->arena.allocator(), root_scope);
    }

    // We may have inner errors
    return Yy_Ok;
}

NODISCARD static Sv debugDeclNameSv(GenIr *const gi, Option<InstIndex> const current_fn_decl, InstIndex const decl)
{
    switch (gi->insts[decl].tag) {
        case MetaEnum_count(Yy_Ir_Inst_Tag):
            yy_unreachable();
        default:
            yy_unreachable();
        case Yy_Ir_Inst_Tag__fn_arg_decl: {
            u32 const arg_idx = genir_smallPl(gi, decl, fn_arg_decl)->arg_idx;
            Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo const arg_data = genir_getFnDeclArgData(gi, current_fn_decl.unwrap(), arg_idx);
            return arg_data.name_z.map([&](IdentifierId z){
                return genir_identifier_sv(gi, z);
            }).unwrap_or(sv_lit("(no name)"));
        }
        break;
        case Yy_Ir_Inst_Tag__local_var_decl: {
            Yy_Ir_Inst_ExtraPl_LocalVarDecl const pl = *genir_extraPl(gi, decl, local_var_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__struct_decl: {
            auto const& pl = *genir_extraPl(gi, decl, struct_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__fn_decl: {
            Yy_Ir_Inst_ExtraPl_FnDecl const pl = *genir_extraPl(gi, decl, fn_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__global_var_decl: {
            Yy_Ir_Inst_ExtraPl_GlobalVarDecl const pl = *genir_extraPl(gi, decl, global_var_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__extern_var_decl: {
            Yy_Ir_Inst_ExtraPl_ExternVarDecl const pl = *genir_extraPl(gi, decl, extern_var_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__extern_fn_decl: {
            Yy_Ir_Inst_ExtraPl_ExternFnDecl const pl = *genir_extraPl(gi, decl, extern_fn_decl);
            return genir_identifier_sv(gi, pl.name_z);
        }
        break;
        case Yy_Ir_Inst_Tag__decl_error_skip: {
            auto const pl = *genir_extraPl(gi, decl, decl_error_skip);
            auto decl_id = pl.decl_id.decode();
            return gi->decl_ptrs[decl_id]->opt_name.map([&](IdentifierId z){
                return genir_identifier_sv(gi, z);
            }).unwrap_or(sv_lit("(decl_error_skip no name)"));
        }
        break;
    }
}

struct DebugPrintInstsContext {
    DebugPrintInstsContext(Option<Sema*> sema, Yy_Allocator scratch_parent) : sema(sema), scratch(scratch_parent) {}
    u32 indent{1};

    InstIndex current_block_head{0};
    Option<InstIndex> current_function_head{None};
    Option<Sema*> sema;
    Yy_ArenaCpp scratch;
};

static u32 const debug_print_insts_indent_size = 2;

static void debugPrintInstsMany(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex start, u32 stop_after_nth, char const *pre, char const *post);
static void debugPrintInst(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex inst_idx);
static void debugPrintInstWithHead(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex inst_idx, Option<InstIndex> new_function_head);

// return the amount of instructions printed
// TODO: Expose this function and add a new parameter `bool only_one` which will only print _one_
// instruction (blocks will not print inner insts).
static void debugPrintInstNoLeadingIndentNoFinalNewline(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex inst_idx)
{
    ctx->indent += 1;
    Inst const inst = gi->insts[inst_idx];
    usize const inst_use_count = inst.untyped_use_count;
    fprintf(stderr, "(used %zu) ", inst_use_count);
    switch (inst.tag) {
        case MetaEnum_count(Yy_Ir_Inst_Tag):
            yy_unreachable();
        case Yy_Ir_Inst_Tag__call_builtin_print_hello_world: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "()",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
        }
        break;
        case Yy_Ir_Inst_Tag__const_int: {
            Yy_Ir_Inst_ExtraPl_ConstInt const extra_pl = *genir_extraPl(gi, inst_idx, const_int);
            i64 const i64_value = extra_pl.integer.decode();
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(%" PRIi64 ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    i64_value);
        }
        break;
        case Yy_Ir_Inst_Tag__const_cstring: {
            Yy_Ir_Inst_ExtraPl_ConstCstring const extra_pl = *genir_extraPl(gi, inst_idx, const_cstring);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "((start=%u, len0=%u): ",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    extra_pl.string.start.x, extra_pl.string.len0);
            Sv data = genir_strTableGetTempSvFromSized0(gi, extra_pl.string);
            // Ignore the zero terminator.
            yy_assert(data.len > 0 && data.ptr[data.len - 1] == 0);
            data.len -= 1;
            ArrayList_Of_u8 buf{ctx->scratch.allocator()};
            array_list_u8_write_escaped_fancy_sv(&buf, data, usize{40});
            fprintf(stderr, Sv_Fmt, Sv_Arg(buf));
            fprintf(stderr, ")");
        }
        break;
        case Yy_Ir_Inst_Tag__const_bool: {
            bool const value = inst.small_data.const_bool.value;
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(%s)",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    value ? "true" : "false");
        }
        break;
        case Yy_Ir_Inst_Tag__const_undefined: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "()",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
        }
        break;
        case Yy_Ir_Inst_Tag__type_uintword:
        case Yy_Ir_Inst_Tag__type_u8:
        case Yy_Ir_Inst_Tag__type_xbool:
        case Yy_Ir_Inst_Tag__type_xvoid:
        case Yy_Ir_Inst_Tag__type_xnoreturn: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "()",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
        }
        break;
        case Yy_Ir_Inst_Tag__type_ptr_to: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.address_of.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__placeholder_ref: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.placeholder_ref)));
        }
        break;
        case Yy_Ir_Inst_Tag__discard: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.discard.inst_to_discard)));
        }
        break;
        case Yy_Ir_Inst_Tag__assign: {
            Yy_Ir_Inst_ExtraPl_Assign const extra = *genir_extraPl(gi, inst_idx, assign);
            Yy_Ir_InstOffsetFunction const lhs = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.lhs);
            Yy_Ir_InstOffsetFunction const rhs = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.rhs);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", " IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(lhs),
                    IOF_Arg(rhs));
        }
        break;
        case Yy_Ir_Inst_Tag__ptr_add_int:
        case Yy_Ir_Inst_Tag__ptr_sub_int:
        case Yy_Ir_Inst_Tag__ptr_sub_ptr:
        case Yy_Ir_Inst_Tag__add:
        case Yy_Ir_Inst_Tag__sub:
        case Yy_Ir_Inst_Tag__mul:
        case Yy_Ir_Inst_Tag__add_wrap:
        case Yy_Ir_Inst_Tag__sub_wrap:
        case Yy_Ir_Inst_Tag__mul_wrap:
        case Yy_Ir_Inst_Tag__div:
        case Yy_Ir_Inst_Tag__mod:
        case Yy_Ir_Inst_Tag__shl:
        case Yy_Ir_Inst_Tag__shr:
        case Yy_Ir_Inst_Tag__bits_and:
        case Yy_Ir_Inst_Tag__bits_or:
        case Yy_Ir_Inst_Tag__bits_xor:
        case Yy_Ir_Inst_Tag__bool_eq:
        case Yy_Ir_Inst_Tag__bool_noteq:
        case Yy_Ir_Inst_Tag__bool_lt:
        case Yy_Ir_Inst_Tag__bool_lte:
        case Yy_Ir_Inst_Tag__bool_gt:
        case Yy_Ir_Inst_Tag__bool_gte: {
            Yy_Ir_Inst_ExtraPl_Binary const extra = *genir_extraPlNoAssert(gi, inst_idx, binary);
            Yy_Ir_InstOffsetFunction const lhs = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.lhs);
            Yy_Ir_InstOffsetFunction const rhs = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.rhs);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", " IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(lhs),
                    IOF_Arg(rhs));
        }
        break;
        case Yy_Ir_Inst_Tag__analyzed_field_val:
        case Yy_Ir_Inst_Tag__analyzed_field_ref: {
            auto const extra = *genir_extraPlNoAssert(gi, inst_idx, analyzed_field_val_or_ref);
            auto const container = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.container);
            auto const field_idx = extra.field_idx;
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", %u)",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(container),
                    field_idx);
        } break;
        case Yy_Ir_Inst_Tag__untyped_field_val:
        case Yy_Ir_Inst_Tag__untyped_field_ref: {
            auto const extra = *genir_extraPlNoAssert(gi, inst_idx, untyped_field_val_or_ref);
            auto const container = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.container);
            auto const field_sv = genir_identifier_sv(gi, extra.field);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", " Sv_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(container),
                    Sv_Arg(field_sv));
        } break;
        case Yy_Ir_Inst_Tag__bool_not:
        case Yy_Ir_Inst_Tag__bits_not: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.unary.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__call: {
            Yy_Ir_Inst_ExtraPl_Call const call_extra_data = *genir_extraPl(gi, inst_idx, call);
            Yy_Ir_InstOffsetFunction const callee = genir_instOffsetFSB(ctx->current_block_head, inst_idx, call_extra_data.callee);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", [",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(callee));
            u32 const arg_count = call_extra_data.arg_count;
            for (u32 arg_idx = 0; arg_idx < arg_count; arg_idx += 1) {
                Yy_Ir_Inst_ExtraPl_Call_Arg const arg_data = genir_getCallArgData(gi, inst_idx, arg_idx);
                Yy_Ir_InstOffsetFunction const arg_inst = genir_instOffsetFSB(ctx->current_block_head, inst_idx, arg_data.inst);
                fprintf(stderr, "" IOF_Fmt "%s",
                        IOF_Arg(arg_inst),
                        (arg_idx + 1 == arg_count) ? "" : ", ");
            }
            fprintf(stderr, "])");
        }
        break;
        case Yy_Ir_Inst_Tag__address_of: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.address_of.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__deref_ref: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.deref_ref.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__deref_val: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.deref_val.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__call_builtin_print_uint: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.call_builtin_print_uint.arg_inst)));
        }
        break;
        case Yy_Ir_Inst_Tag__break_void: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.break_void.block_to_break_to)));
        }
        break;
        case Yy_Ir_Inst_Tag__break_value: {
            Yy_Ir_Inst_ExtraPl_BreakValue const extra = *genir_extraPl(gi, inst_idx, break_value);
            Yy_Ir_InstOffsetFunction const block_to_break_to = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.block_to_break_to);
            Yy_Ir_InstOffsetFunction const value = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.value);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ", " IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(block_to_break_to),
                    IOF_Arg(value));
        }
        break;
        case Yy_Ir_Inst_Tag__return_void: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "()",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
        }
        break;
        case Yy_Ir_Inst_Tag__return_value: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, inst.small_data.return_value.operand)));
        }
        break;
        case Yy_Ir_Inst_Tag__xunreachable: {
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "()",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
        }
        break;
        case Yy_Ir_Inst_Tag__block: {
            Yy_Ir_Inst_ExtraPl_Block const pl = *genir_extraPl(gi, inst_idx, block);
            u32 const block_end_offset = pl.end_offset;
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "((%u breaks) ",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    pl.break_count_upper_limit);
            debugPrintInstsMany(ctx, gi, inst_idx + 1, block_end_offset - 1, "{", "})");
        }
        break;
        case Yy_Ir_Inst_Tag__root: {
            auto decl_pl = *genir_extraPl(gi, inst_idx, root);
            u32 const end_offset = decl_pl.end_offset;
            auto decl_id = decl_pl.decl_id.decode();
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)));
            debugPrintInstsMany(ctx, gi, inst_idx + 1, end_offset - 1, "({", "})");
        }
        break;
        case Yy_Ir_Inst_Tag__decl_error_skip: {
            auto decl_pl = *genir_extraPl(gi, inst_idx, decl_error_skip);
            auto decl_id = decl_pl.decl_id.decode();
            auto name_sv = debugDeclNameSv(gi, ctx->current_function_head, inst_idx);
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(name: \"" Sv_Fmt "\", {...})",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));
        }
        break;
        case Yy_Ir_Inst_Tag__loop: {
            InstIndex const loop_inner = yy_ir_inst_loop_getLoopInnerInstIndex(inst_idx);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "((%u breaks) {\n",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    inst.small_data.loop.break_count_upper_limit);
            debugPrintInst(ctx, gi, loop_inner);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "})");
        }
        break;
        case Yy_Ir_Inst_Tag__cond_branch: {
            Yy_Ir_Inst_ExtraPl_CondBranch const extra = *genir_extraPl(gi, inst_idx, cond_branch);
            u8 const lparen = '(';
            u8 const rparen = ')';
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "%c(%u breaks), ",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)), lparen,
                    extra.break_count_upper_limit);
            Yy_Ir_InstOffsetFunction const cond = genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.cond);
            InstIndex const then = genir_instAbsSF(inst_idx, extra.then);
            InstIndex const xelse = genir_instAbsSF(inst_idx, extra.xelse);
            fprintf(stderr, "cond: " IOF_Fmt ", then: {\n", IOF_Arg(cond));
            debugPrintInst(ctx, gi, then);
            yy_assert(ctx->indent > 0);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, xelse: {\n");
            debugPrintInst(ctx, gi, xelse);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}%c", rparen);
        }
        break;
        case Yy_Ir_Inst_Tag__decl_val:
        case Yy_Ir_Inst_Tag__decl_ref: {
            auto ref_decl_id = genir_extrasAtTypedNoAssert<Yy_Ir_Inst_ExtraPl_DeclValOrRef>(
                gi, inst.small_data.decl_val_or_ref.extra_pl_index
            )->ref_decl.decode();
            auto ref_inst = gi->decl_ptrs[ref_decl_id]->inst;
            Sv const name_sv = debugDeclNameSv(gi, ctx->current_function_head, ref_inst);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(\"" Sv_Fmt "\", $%u / " DeclId_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv),
                    ref_inst.x, DeclId_Arg(ref_decl_id));
        }
        break;
        case Yy_Ir_Inst_Tag__local_decl_val:
        case Yy_Ir_Inst_Tag__local_decl_ref: {
            InstIndex const ref = genir_instAbsSB(inst_idx, inst.small_data.local_decl_val_or_ref.ref);
            Sv const name_sv = debugDeclNameSv(gi, ctx->current_function_head, ref);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(\"" Sv_Fmt "\", " IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv),
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, ref)));
        }
        break;
        case Yy_Ir_Inst_Tag__struct_decl: {
            auto const struct_decl_extra_idx = genir_extraPlIndex(gi, inst_idx, struct_decl);
            auto const& decl_pl = *genir_extraPl(gi, inst_idx, struct_decl);
            auto decl_id = decl_pl.decl_id.decode();
            Sv const name_sv = genir_identifier_sv(gi, decl_pl.name_z);
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(\"" Sv_Fmt "\", members: [",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));
            ctx->indent += 1;
            for (u32 member_idx = 0; member_idx < decl_pl.member_count; member_idx += 1) {
                fprintf(stderr, "\n");
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                auto const& member_data =
                    *ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_StructDecl>(gi, struct_decl_extra_idx)
                         .nth_ptr(gi, member_idx);

                Sv const member_name_sv = member_data.name_z.map([&](IdentifierId z){
                    return genir_identifier_sv(gi, z);
                }).unwrap_or(sv_lit("_"));
                auto member_block = genir_instAbsSF(inst_idx, member_data.type_eval_block);
                fprintf(stderr, "" Sv_Fmt ": $%u {\n", Sv_Arg(member_name_sv), member_block.x);
                debugPrintInstWithHead(ctx, gi, member_block, None);
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                fprintf(stderr, "},");
            }
            ctx->indent -= 1;
            if (decl_pl.member_count > 0) {
                fprintf(stderr, "\n");
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            }
            fprintf(stderr, "])");
        }
        break;
        case Yy_Ir_Inst_Tag__fn_decl: {
            Yy_Ir_Inst_ExtraPl_FnDecl const decl_pl = *genir_extraPl(gi, inst_idx, fn_decl);
            auto decl_id = decl_pl.decl_id.decode();
            InstIndex const body = genir_getFnDeclBodyInst(gi, inst_idx);
            InstIndex const return_type_block = genir_getFnDeclReturnTypeBlockInst(gi, inst_idx);
            Sv const name_sv = genir_identifier_sv(gi, decl_pl.name_z);
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(\"" Sv_Fmt "\", args: [",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));
            ctx->indent += 1;
            for (u32 arg_idx = 0; arg_idx < decl_pl.arg_count; arg_idx += 1) {
                fprintf(stderr, "\n");
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo const arg_data = genir_getFnDeclArgData(gi, inst_idx, arg_idx);
                Sv const arg_name_sv = arg_data.name_z.map([&](IdentifierId z){
                    return genir_identifier_sv(gi, z);
                }).unwrap_or(sv_lit("_"));
                auto arg_block = genir_instAbsSF(inst_idx, arg_data.type_eval_block);
                fprintf(stderr, "" Sv_Fmt ": $%u {\n", Sv_Arg(arg_name_sv), arg_block.x);
                debugPrintInstWithHead(ctx, gi, arg_block, None);
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                fprintf(stderr, "},");
            }
            ctx->indent -= 1;
            if (decl_pl.arg_count > 0) {
                fprintf(stderr, "\n");
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            }
            fprintf(stderr, "], ret: $%u {\n", return_type_block.x);
            debugPrintInstWithHead(ctx, gi, return_type_block, None);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, body: $%u (%u returns) {\n", body.x, decl_pl.return_count_upper_limit);
            debugPrintInstWithHead(ctx, gi, body, inst_idx);
            yy_assert(ctx->indent > 0);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "})");
        }
        break;
        case Yy_Ir_Inst_Tag__truncate_int: {
            auto const pl = *genir_extraPl(gi, inst_idx, truncate_int);
            auto dest_type_block = genir_instAbsSF(inst_idx, pl.dest_type_block);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(dest_type: $%u {\n",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    dest_type_block.x);
            (void) debugPrintInst(ctx, gi, dest_type_block);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, src: " IOF_Fmt ")",
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, genir_instAbsSB(inst_idx, pl.src_value))));
        }
        break;
        case Yy_Ir_Inst_Tag__promote_int: {
            auto const pl = *genir_extraPl(gi, inst_idx, promote_int);
            auto dest_type_block = genir_instAbsSF(inst_idx, pl.dest_type_block);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(dest_type: $%u {\n",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    dest_type_block.x);
            (void) debugPrintInst(ctx, gi, dest_type_block);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, src: " IOF_Fmt ")",
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, genir_instAbsSB(inst_idx, pl.src_value))));
        }
        break;
        case Yy_Ir_Inst_Tag__ptr_cast: {
            auto const pl = *genir_extraPl(gi, inst_idx, ptr_cast);
            auto dest_type_block = genir_instAbsSF(inst_idx, pl.dest_type_block);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(dest_type: $%u {\n",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    dest_type_block.x);
            (void) debugPrintInst(ctx, gi, dest_type_block);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, src: " IOF_Fmt ")",
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, genir_instAbsSB(inst_idx, pl.src_value))));
        }
        break;
        case Yy_Ir_Inst_Tag__int_to_ptr: {
            auto const pl = *genir_extraPl(gi, inst_idx, int_to_ptr);
            auto dest_type_block = genir_instAbsSF(inst_idx, pl.dest_type_block);
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(dest_type: $%u {\n",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    dest_type_block.x);
            (void) debugPrintInst(ctx, gi, dest_type_block);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "}, src: " IOF_Fmt ")",
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, genir_instAbsSB(inst_idx, pl.src_value))));
        }
        break;
        case Yy_Ir_Inst_Tag__ptr_to_int: {
            auto const src_value = inst.small_data.ptr_to_int.src_value;
            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(" IOF_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    IOF_Arg(genir_instOffsetFunction(ctx->current_block_head, genir_instAbsSB(inst_idx, src_value))));
        }
        break;
        case Yy_Ir_Inst_Tag__global_var_decl: {
            Yy_Ir_Inst_ExtraPl_GlobalVarDecl const decl_pl = *genir_extraPl(gi, inst_idx, global_var_decl);
            auto decl_id = decl_pl.decl_id.decode();
            Sv const name_sv = genir_identifier_sv(gi, decl_pl.name_z);

            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(\"" Sv_Fmt "\", ",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));

            if (decl_pl.type_block.has_value()) {
                auto const type_block = decl_pl.type_block.unwrap().abs(inst_idx);
                fprintf(stderr, "type: $%u {\n", type_block.x);
                debugPrintInstWithHead(ctx, gi, type_block, None);
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                fprintf(stderr, "}, ");
            } else {
                fprintf(stderr, "(infer type), ");
            }

            auto init_block = genir_getGlobalVarDeclInitBlockInst(gi, inst_idx);
            fprintf(stderr, "init_block: $%u {\n", init_block.x);
            debugPrintInstWithHead(ctx, gi, init_block, None);
            yy_assert(ctx->indent > 0);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "})");
        }
        break;
        case Yy_Ir_Inst_Tag__extern_var_decl: {
            Yy_Ir_Inst_ExtraPl_ExternVarDecl const decl_pl = *genir_extraPl(gi, inst_idx, extern_var_decl);
            auto decl_id = decl_pl.decl_id.decode();
            Sv const name_sv = genir_identifier_sv(gi, decl_pl.name_z);
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(\"" Sv_Fmt "\", type: {\n",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));
            auto const type_block = genir_instAbsSF(inst_idx, decl_pl.type_block);
            debugPrintInstWithHead(ctx, gi, type_block, None);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "})");
        }
        break;
        case Yy_Ir_Inst_Tag__extern_fn_decl: {
            Yy_Ir_Inst_ExtraPl_ExternFnDecl const decl_pl = *genir_extraPl(gi, inst_idx, extern_fn_decl);
            auto decl_id = decl_pl.decl_id.decode();
            InstIndex const return_type_block = decl_pl.return_type_eval_block_offset.abs(inst_idx);
            Sv const name_sv = genir_identifier_sv(gi, decl_pl.name_z);
            fprintf(stderr, "" DeclId_Fmt " = $%u = " Sv_Fmt "(\"" Sv_Fmt "\", args: [",
                    DeclId_Arg(decl_id), inst_idx.x,
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    Sv_Arg(name_sv));
            for (u32 arg_idx = 0; arg_idx < decl_pl.arg_count; arg_idx += 1) {
                Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo const arg_data = genir_getExternFnDeclArgData(gi, inst_idx, arg_idx);
                Sv const arg_name_sv = arg_data.name_z.map([&](IdentifierId z){
                    return genir_identifier_sv(gi, z);
                }).unwrap_or(sv_lit("_"));
                auto arg_block = arg_data.type_eval_block.abs(inst_idx);
                fprintf(stderr, "" Sv_Fmt ": $%u {\n", Sv_Arg(arg_name_sv), arg_block.x);
                debugPrintInstWithHead(ctx, gi, arg_block, None);
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                fprintf(stderr, "}%s", (arg_idx + 1 == decl_pl.arg_count) ? "" : ", ");
            }
            fprintf(stderr, "], ret: $%u {\n", return_type_block.x);
            debugPrintInstWithHead(ctx, gi, return_type_block, None);
            fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
            fprintf(stderr, "})");
        }
        break;
        case Yy_Ir_Inst_Tag__fn_arg_decl: {
            u32 const arg_idx = inst.small_data.fn_arg_decl.arg_idx;
            Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo const arg_data = genir_getFnDeclArgData(gi, ctx->current_function_head.unwrap(), arg_idx);
            Sv const name_sv = arg_data.name_z.map([&](IdentifierId z){
                return genir_identifier_sv(gi, z);
            }).unwrap_or(sv_lit("_"));

            fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(arg_idx: %u: " Sv_Fmt ")",
                    IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                    Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                    arg_idx, Sv_Arg(name_sv));
        }
        break;
        case Yy_Ir_Inst_Tag__local_var_decl: {
            Yy_Ir_Inst_ExtraPl_LocalVarDecl const extra = *genir_extraPl(gi, inst_idx, local_var_decl);
            Sv const name_sv = genir_identifier_sv(gi, extra.name_z);
            auto type_self_offset = extra.type_block;
            if (type_self_offset.has_value()) {
                auto const type_block = type_self_offset.unwrap().abs(inst_idx);
                fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(\"" Sv_Fmt "\", type: $%u {\n",
                        IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                        Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                        Sv_Arg(name_sv), type_block.x);
                (void) debugPrintInst(ctx, gi, type_block);
                fprintf(stderr, "%*s", (ctx->indent - 1)*debug_print_insts_indent_size, "");
                fprintf(stderr, "}, initial_value: " IOF_Fmt ")",
                        IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.initial_value)));
            } else {
                fprintf(stderr, "" IOF_Fmt " = " Sv_Fmt "(\"" Sv_Fmt "\", (infer type), initial_value: " IOF_Fmt ")",
                        IOF_Arg(inst_idx.asOffsetFunction(ctx->current_block_head)),
                        Sv_Arg(MetaEnum_sv_short(Yy_Ir_Inst_Tag, inst.tag)),
                        Sv_Arg(name_sv),
                        IOF_Arg(genir_instOffsetFSB(ctx->current_block_head, inst_idx, extra.initial_value)));
            }
        }
        break;
    }
    if (ctx->sema.has_value()) {
        auto const sema = ctx->sema.unwrap();
        auto _ = ctx->scratch.scoped_save();
        ArrayList_Of_u8 buf{ctx->scratch.allocator()};
        auto const& sii = sema->sema_inst_infos[inst_idx];
        if (sii.is_full()) {
            // Assume it has a type.
            auto const type = sii.type3;
            if (sii.specific.tag == SiiSpecific_Tag__decl_like) {
                auto const decl_type =
                    sii.specific.as<SiiSpecific_Tag__decl_like>().unwrap().resolved_decl_type.unwrap();
                fmtSemaTypeId(&buf, sema, decl_type);
                fprintf(stderr, " => (decl): " Sv_Fmt "", Sv_Arg(buf));
                yy_assert(typeIsVoid(sema, type));
            } else {
                fmtSemaTypeId(&buf, sema, type);
                fprintf(stderr, " => " Sv_Fmt "", Sv_Arg(buf));
                if (sii.resolved_comptime_value.has_value()) {
                    auto const& value = sema->comptime_value_pool[sii.resolved_comptime_value.unwrap()];
                    buf.clear();
                    fmtComptimeValue(&buf, sema, value);
                    fprintf(stderr, " = " Sv_Fmt "", Sv_Arg(buf));
                }
            }
        }
    }
    ctx->indent -= 1;
}

static void debugPrintInst(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex inst_idx)
{
    fprintf(stderr, "%*s", ctx->indent*debug_print_insts_indent_size, "");
    debugPrintInstNoLeadingIndentNoFinalNewline(ctx, gi, inst_idx);
    fprintf(stderr, "\n");
}
static void debugPrintInstWithHead(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex inst_idx, Option<InstIndex> new_function_head)
{
    auto old_block_head = ctx->current_block_head;
    auto old_function_head = ctx->current_function_head;
    auto new_block_head = inst_idx;
    ctx->current_block_head = new_block_head;
    ctx->current_function_head = new_function_head;
    debugPrintInst(ctx, gi, inst_idx);
    ctx->current_block_head = old_block_head;
    ctx->current_function_head = old_function_head;
}

static void debugPrintInstsMany(DebugPrintInstsContext *ctx, GenIr *gi, InstIndex start, u32 stop_after_nth, char const *pre, char const *post)
{
    fprintf(stderr, "%s", pre);
    if (stop_after_nth > 0) {
        fprintf(stderr, "\n");
    }
    InstIndex iter = start;
    for (; iter.x < start.x + stop_after_nth;) {
        auto inst = iter;
        iter += genir_instEndOffset(gi, iter);
        debugPrintInst(ctx, gi, inst);
    }
    yy_assert(iter == start + stop_after_nth);
    if (stop_after_nth > 0) {
        fprintf(stderr, "%*s", (yy_max(u32{1}, ctx->indent) - 1)*debug_print_insts_indent_size, "");
    }
    fprintf(stderr, "%s", post);
}

static void hexdump_stderr(Bv buf, u8 const byte_group) {
    u8 const* buf_ptr = buf.ptr;
    auto buf_len = buf.len;
    yy_assert(byte_group > 0);
    static const u8 bytes_per_row = 16;
    u32 const row_max = (intCastFromUsizeToU32(buf_len) + bytes_per_row - 1) / bytes_per_row;
    for (u32 row = 0; row < row_max; row++) {
        u32 const table_idx_min = row * bytes_per_row;
        u32 const table_idx_max =
            yy_min(table_idx_min + bytes_per_row,
                        intCastFromUsizeToU32(buf_len));
        yy_assert(table_idx_max <= buf_len);
        // print in this format: "| 00000010: xx xx xx xx (16 bytes)...     .1di.q.ojf9$@8j?\n"
        fprintf(stderr, "| %08" PRIx64 ": ", (u64)table_idx_min / byte_group);
        // print hex
        for (u32 table_idx = table_idx_min;
                table_idx < table_idx_max;
                table_idx++) {
            char const* separator = (table_idx + 1) % byte_group == 0 ? " " : "";
            u8 const the_byte = buf_ptr[table_idx];
            fprintf(stderr, "%02" PRIx8 "%s", the_byte, separator);
        }
        // separation
        static u8 const space_before_ascii = 1;
        fprintf(stderr, "%*s", space_before_ascii, "");
        // print padding if on the last row
        if (row + 1 == row_max) {
            u32 const padding_size = 3 * (table_idx_min + bytes_per_row - table_idx_max);
            fprintf(stderr, "%*s", padding_size, "");
        }
        // print ascii
        for (u32 table_idx = table_idx_min;
                table_idx < table_idx_max;
                table_idx++) {
            u8 const the_byte = buf_ptr[table_idx];
            u8 const printable_ascii = the_byte >= ' ' && the_byte <= '~' ? the_byte : '.';
            fprintf(stderr, "%c", printable_ascii);
        }
        fprintf(stderr, "\n");
    }
}

void genir_debugPrint(GenIr *gi, Option<Sema*> sema)
{
    (void) gi;
    logDebug("%s()", __func__);

    // insts, extras, string_table

    {
        fprintf(stderr, "gi->insts: [%zu]Ir_Inst =\n", gi->insts.size());
        yy_assert(gi->insts.size()> 0);
        DebugPrintInstsContext printer = {sema, gi->heap};
        debugPrintInst(&printer, gi, InstIndex{0});
    }
    fprintf(stderr, "\n");

    fprintf(stderr, "gi->extras: [%zu]Extra =\n", gi->extras.size());
    hexdump_stderr(gi->extras.span_const().as_bytes(), sizeof(Yy_Ir_Extra));
    fprintf(stderr, "\n");

    fprintf(stderr, "gi->string_table: [%zu]u8 =\n", gi->string_table.size());
    hexdump_stderr(gi->string_table.span_const().as_bytes(), 1);
    fprintf(stderr, "\n");

    fprintf(stderr, "gi->decl_ptrs: [%zu]*Decl =\n", gi->decl_ptrs.size());
    fprintf(stderr, "| sizeof(Decl) = %zu\n", usize(sizeof(Decl)));
    fprintf(stderr, "\n");

    auto memory_usage = MemoryUnit::from_bytes(
        sizeof(Inst) * gi->insts.size() +
        sizeof(Extra) * gi->extras.size() +
        sizeof(u8) * gi->string_table.size() +
        (sizeof(Decl*) + sizeof(Decl)) * gi->decl_ptrs.size()
    );
    fprintf(stderr, "genir memory usage: " MemoryUnit_Fmt "\n", MemoryUnit_Arg(memory_usage));
    fprintf(stderr, "\n");
}

GenIr::GenIr(Yuceyuf_Ast_File* ast, Yy_Allocator heap)
    : filename(ast->filename), ast_file(ast), heap(heap), arena(heap, 0), errors(heap) {}

GenIr::~GenIr() {}


NODISCARD auto yy_debug_gen_ir_from_ast_file(Yuceyuf_Ast_File *ast_file) -> GenIr
{
    GenIr result{ast_file, c_allocator()};
    auto const ret_gi = &result;
    Yuceyuf_AstNode* root = ast_file->root;
    auto const filename = ast_file->filename;
    yy_assert(root->it.tag == Yuceyuf_AstNode_Specific_Tag__root);
    if (genir_astTopRoot(ret_gi, root).is_err()) {
        yy_assert(ret_gi->has_errors());
        logDebug("" Sv_Fmt ": error: genir_astTopRoot() failed", Sv_Arg(filename));
        goto final_error;
    }
    if (verbose_genir) genir_debugPrint(ret_gi, None);
    yy_assert(ret_gi->debug_root_scope_create_count == ret_gi->debug_root_scope_destroy_count);

    // We may have errors, but we can run sema (which will fail with additional errors).
    yy_assert(!result.errors.fatal);
    return result;
final_error:
    result.errors.fatal = true;
    return result;
}




NODISCARD Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo
genir_getFnDeclArgData(GenIr *const gi, InstIndex const fn_decl, u32 const arg_idx)
{
    yy_assert(gi->insts[fn_decl].tag == Yy_Ir_Inst_Tag__fn_decl);
    ExtraIndex const fn_decl_extra_idx = genir_smallPl(gi, fn_decl, fn_decl)->extra_pl_index;
    return *ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_FnDecl>(gi, fn_decl_extra_idx).nth_ptr(gi, arg_idx);
}

NODISCARD Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo
genir_getExternFnDeclArgData(GenIr *const gi, InstIndex const extern_fn_decl, u32 const arg_idx)
{
    yy_assert(gi->insts[extern_fn_decl].tag == Yy_Ir_Inst_Tag__extern_fn_decl);
    ExtraIndex const extern_fn_decl_extra_idx = genir_smallPl(gi, extern_fn_decl, extern_fn_decl)->extra_pl_index;
    return *ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_ExternFnDecl>(gi, extern_fn_decl_extra_idx).nth_ptr(gi, arg_idx);
}

NODISCARD Yy_Ir_Inst_ExtraPl_Call_Arg
genir_getCallArgData(GenIr *const gi, InstIndex const call, u32 const arg_idx)
{
    yy_assert(gi->insts[call].tag == Yy_Ir_Inst_Tag__call);
    ExtraIndex const call_extra_idx = genir_smallPl(gi, call, call)->extra_pl_index;
    return *ir::ExtraTrailingArray<Yy_Ir_Inst_ExtraPl_Call>(gi, call_extra_idx).nth_ptr(gi, arg_idx);
}
