#include "yy/basic.hpp"
#include "yuceyuf/file.hpp"
#include "yy/allocator.hpp"
#include "yy/sv.hpp"
#include "yy/misc.hpp"
#include "yuceyuf/path_builder.hpp"
#include "yy/vec.hpp"

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

Yuceyuf_File const YUCEYUF_AT_FDCWD = { .fd = AT_FDCWD };

// TODO: Return the reason why it failed.
/// Return error when `maximum_file_size` is reached.
NODISCARD YyStatus yuceyuf_read_entire_file_bytes(
    Yy_Allocator allocator,
    Yuceyuf_File at_dir_fd, Sv filename,
    usize maximum_file_size, BvMut *result_contents, int *const result_os_error)
{
    yy_debug_invalidate_ptr_one(result_contents);
    *result_os_error = 0;

    // Copy filename into a cstr buffer.
    char filename_cstr_buf[4096];
    TRY(path_to_cstr(filename, filename_cstr_buf, sizeof(filename_cstr_buf)));

    // Open the file (read only).
    int fd = openat(at_dir_fd.fd, filename_cstr_buf, O_RDONLY);
    defer { close(fd); };

    if (fd < 0) {
        *result_os_error = errno;
        return Yy_Bad;
    }

    // Stat it.
    struct stat stat_buf = {};
    if (fstat(fd, &stat_buf) != 0) {
        *result_os_error = errno;
        return Yy_Bad;
    }

    // Can't read a directory.
    if (S_ISDIR(stat_buf.st_mode)) {
        *result_os_error = EISDIR;
        return Yy_Bad;
    }

    // Get file size hint if it is a regular file.
    usize file_size_hint = 0;
    bool const is_regular_file = S_ISREG(stat_buf.st_mode);
    if (is_regular_file) {
        if (stat_buf.st_size >= SSIZE_MAX - 1 || stat_buf.st_size < 0) {
            *result_os_error = E2BIG;
            return Yy_Bad;
        }
        file_size_hint = (usize)stat_buf.st_size;
    }


    // Allocate a buffer starting out with capacity `file_size_hint + 1` so that if the file
    // size hint is correct, it will only call `read` once.
    auto buf = Vec<u8>::init_capacity(allocator, yy_max(file_size_hint + 1, usize{256}));

    // Read the entire file in a loop until EOF is found or `maximum_file_size` is reached,
    // realloacting the buffer if necessary.
    while (true) {
        // Reallocate buf if necessary.
        if (buf.unused_capacity() == 0) {
            buf.ensure_unused_capacity(buf.size() * 2);
            yy_assert(buf.capacity() > 0);
        }
        // Read the file. `read` can't read more than `SSIZE_MAX` at once.
        usize const amount_to_read = yy_at_most(usize{SSIZE_MAX - 1}, buf.unused_capacity());
        yy_assert(amount_to_read > 0);
        isize const read_res = read(fd, buf.end(), amount_to_read);
        // Check for error.
        if (read_res < 0) {
            *result_os_error = errno;
            return Yy_Bad;
        }
        // Get the number of bytes read.
        usize const bytes_read = (usize)read_res;
        buf.unsafe_set_m_len(buf.size() + bytes_read);
        // Check if `maximum_file_size` is exceeded.
        if (buf.size() > maximum_file_size) {
            *result_os_error = E2BIG;
            return Yy_Bad;
        }
        // Check if it found EOF if it's a regular file.
        if (is_regular_file && bytes_read < amount_to_read)
            break;
        // Check if it found EOF.
        if (bytes_read == 0)
            break;
    }

    // Store the result in `result_contents`
    auto span = buf.leak_to_span();
    result_contents->ptr = span.ptr;
    result_contents->len = span.len;
    return Yy_Ok;
}
