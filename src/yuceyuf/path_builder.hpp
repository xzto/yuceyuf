#ifndef ___INCLUDE_GUARD__1XqYiTqIQaOHzIdlny5B9pVtZbpuFPQEv7mEml7T
#define ___INCLUDE_GUARD__1XqYiTqIQaOHzIdlny5B9pVtZbpuFPQEv7mEml7T

#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/sv.hpp"

NODISCARD YyStatus join_paths_and_canonicalize(
    Yy_Allocator result_allocator, Yy_Allocator scratch,
    u32 fixed_parent_node_count, Sv parent, Sv next, Sv *result);


typedef struct {
    Sv path;
    usize idx;
} PathIterator;

NODISCARD PathIterator path_iter_init(Sv path);
NODISCARD bool path_iter_next(PathIterator *iter, Sv *result);

NODISCARD Sv path_with_single_slashes(
    Yy_Allocator result_allocator, Yy_Allocator scratch,
    Sv path_with_multiple_slashes);

NODISCARD u32 path_count_nodes(Sv path);

NODISCARD YyStatus path_to_cstr(Sv path, char *buf_ptr, usize buf_len);
NODISCARD YyStatus path_get_dirname(Sv path, Sv *result_dir);

#endif//___INCLUDE_GUARD__1XqYiTqIQaOHzIdlny5B9pVtZbpuFPQEv7mEml7T
