#ifndef ___INCLUDE_GUARD__jEHHjNLwhgWe0I5ALF84NZhTuWsULJTGrebXWMOH
#define ___INCLUDE_GUARD__jEHHjNLwhgWe0I5ALF84NZhTuWsULJTGrebXWMOH

#include "yy/basic.hpp"
#include "yy/allocator.hpp"
#include "yy/array_list_printf.hpp"
#include "yuceyuf/genir.hpp"
#include "yuceyuf/sema.hpp"
#include "yy/meta/enum.hpp"

#define Yy_CodegenX64_struct_field_count 5
struct Yy_CodegenX64 {
    Yy_Allocator heap;
    Yy_ArenaCpp arena;
    Sema *sema;
    ArrayList_Of_u8 asm_text;
    Yuceyuf_ErrorBundle errors;

    Yy_CodegenX64(Yy_Allocator heap, Sema* sema);
    ~Yy_CodegenX64();
    Yy_CodegenX64(const Yy_CodegenX64&) = delete;
    Yy_CodegenX64& operator=(const Yy_CodegenX64&) = delete;
    Yy_CodegenX64(Yy_CodegenX64&&) = default;
    Yy_CodegenX64& operator=(Yy_CodegenX64&&) = default;
};

NODISCARD auto yy_codegen_x64_from_sema(
    Yy_Allocator const heap,
    Sema* const sema,
    Sv asm_out_filename
) -> Yy_CodegenX64;

extern bool verbose_codegen;

#endif//___INCLUDE_GUARD__jEHHjNLwhgWe0I5ALF84NZhTuWsULJTGrebXWMOH
