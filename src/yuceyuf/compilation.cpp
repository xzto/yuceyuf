#include "yy/basic.hpp"
#include "yuceyuf/compilation.hpp"
#include "yuceyuf/lexer.hpp"

#include <ranges>

namespace yuceyuf::compilation {

GlobalCompilation::GlobalCompilation(Allocator allocator)
    : identifiers(allocator), lexed_files(allocator), arena(allocator, 2048) {}

NODISCARD auto GlobalCompilation::reserve_lexed_file() -> FileId {
    (void)this;
    return FileId{yy::checked_int_cast<u32>(lexed_files.size()).unwrap()};
}
NODISCARD auto GlobalCompilation::unreserve_lexed_file(FileId id, LexerCtx&& lexer_ctx) -> LexedFile& {
    yy_assert(id.x == lexed_files.size());
    return *allocator_new<LexedFile>(arena.allocator(), std::move(lexer_ctx));
}
NODISCARD auto GlobalCompilation::identifier_sv(IdentifierId id) const -> Sv {
    return identifiers.get_by_index(id.x).key;
}
NODISCARD auto GlobalCompilation::make_identifier(Sv name) -> IdentifierId {
    auto entry = identifiers.get_or_put_entry({}, name);
    if (entry.exists())
        return IdentifierId(yy::checked_int_cast<u32>(entry.unwrap_get().insertion_idx).unwrap());
    yy_assert(!sv_contains_char(name, '\0'));
    auto&& put = std::move(entry).unwrap_put();
    auto new_sv = allocator_dup_sv(arena.allocator(), name);
    auto put_ref = put.put_key_value(new_sv, Void{});
    auto new_id = IdentifierId(yy::checked_int_cast<u32>(put_ref.insertion_idx).unwrap());
    return new_id;
}

}  // namespace yuceyuf::compilation
