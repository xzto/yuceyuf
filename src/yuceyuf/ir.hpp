#ifndef ___INCLUDE_GUARD__z1qiwu1hLcqg19KZIaoXa14og52nXC8DDH6hFN55
#define ___INCLUDE_GUARD__z1qiwu1hLcqg19KZIaoXa14og52nXC8DDH6hFN55

#include "yy/basic.hpp"
#include "yy/option.hpp"
#include "yuceyuf/lexer.hpp"

#include <compare>
#include <cstring>

//! This file contains the definitions for the intermediate representation `Yy_Ir`, which gets generated from `Yuceyuf_AstNode.root`

struct GenIr;

struct Yy_Ir_Extra {
    u32 as_u32;
};
static_assert(sizeof(Yy_Ir_Extra) == sizeof(u32) &&alignof(Yy_Ir_Extra) == alignof(u32), "hardcoded stuff");

struct Yy_Ir_ExtraIndex {
    u32 x;
    friend constexpr auto operator==(Yy_Ir_ExtraIndex, Yy_Ir_ExtraIndex) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_ExtraIndex, Yy_Ir_ExtraIndex{ .x = UINT32_MAX });
};

// TODO: I may want to allow the program to have more than 4GB of constant data in it.
// There are actually two kinds of string data:
// - Data for your program
// - Data for the compiler (identifier names, etc)
// `StringIndex` contains mostly internal compiler data. I may want to create a new type for actual
// runtime data, but maybe it's not necessary inside Ir since it's not analyzed.
struct Yy_Ir_StringIndex {
    u32 x;
    friend constexpr auto operator==(Yy_Ir_StringIndex, Yy_Ir_StringIndex) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_StringIndex, Yy_Ir_StringIndex{ .x = UINT32_MAX });
};

struct Yy_Ir_StringSized0 {
    Yy_Ir_StringIndex start;
    u32 len0;
};


namespace ir {


template <typename T>
    requires yy::trivial_raii<T> && std::is_default_constructible_v<T> && std::has_unique_object_representations_v<T>
struct ByParts {
    static_assert(sizeof(T) % sizeof(u32) == 0);
    static constexpr usize part_count = sizeof(T) / sizeof(u32);
    u32 parts[part_count];

    constexpr ByParts() = default;
    constexpr ByParts(T x) { memcpy(this, &x, sizeof(T)); }
    constexpr ByParts& operator=(T x) { memcpy(this, &x, sizeof(T)); return *this; }
    static constexpr auto encode(T x) -> ByParts { return ByParts(x); }
    NODISCARD constexpr auto decode() const -> T {
        T result;
        memcpy(&result, this, sizeof(T));
        return result;
    }
    constexpr operator T() const { return decode(); }
};

#define DeclId_Fmt "#%" PRIu64
#define DeclId_Arg(arg) ((void)yy_static_assert_types_and_zero(yy_TypeOf(arg) const, DeclId const), (arg).x)
struct DeclId {
    u64 x;

    explicit operator usize() const { return x ;}
    NODISCARD constexpr auto operator==(DeclId other) const -> bool { return x == other.x; }

    // static consteval auto invalid() -> DeclId { return { UINT64_MAX }; }
    // NODISCARD constexpr auto isNotInvalid() const -> bool { return *this != invalid(); }
    // NODISCARD constexpr auto isInvalid() const -> bool { return *this == invalid(); }

    YY_DECL_INVALID_OF_INSIDE_CLASS(DeclId, DeclId{ UINT64_MAX });
};

struct ExtraPlBase {};

template <typename T>
concept extra_pl = std::derived_from<T, ir::ExtraPlBase>;


using ExtraIndex = Yy_Ir_ExtraIndex;
using Extra = Yy_Ir_Extra;

template <ir::extra_pl ParentPl>
struct ExtraTrailingArray {
    ExtraIndex trailing_start;
    u32 trailing_count;

    using TrailingElement = ParentPl::TrailingElement;

    ExtraTrailingArray(GenIr* gi, ExtraIndex parent);

    NODISCARD auto nth_index(u32 index) const -> ExtraIndex {
        yy_assert(index < trailing_count);
        u32 const elem_extra_size = sizeof(TrailingElement) / sizeof(Extra);
        return ExtraIndex { .x = trailing_start.x + index * elem_extra_size };
    }

    NODISCARD auto nth_ptr(GenIr* gi, u32 index) const -> TrailingElement*;
};


}

using ir::ByParts;
using ir::DeclId;




struct Yy_Ir_EntireFile;
struct Yy_Ir_Inst;
struct Yy_Ir_Extra;
struct Yy_Ir_Inst_ExtraPl_StructDecl;
struct Yy_Ir_Inst_ExtraPl_StructDecl_MemberInfo;
struct Yy_Ir_Inst_ExtraPl_FnDecl;
struct Yy_Ir_Inst_ExtraPl_ExternFnDecl;
struct Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo;
struct Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo;
struct Yy_Ir_Inst_ExtraPl_GlobalVarDecl;
struct Yy_Ir_Inst_ExtraPl_ExternVarDecl;
struct Yy_Ir_Inst_ExtraPl_LocalVarDecl;
struct Yy_Ir_Inst_ExtraPl_ConstInt;
struct Yy_Ir_Inst_ExtraPl_ConstCstring;
struct Yy_Ir_Inst_ExtraPl_Assign;
struct Yy_Ir_Inst_ExtraPl_BreakValue;
struct Yy_Ir_Inst_ExtraPl_CondBranch;
struct Yy_Ir_Inst_ExtraPl_Block;
struct Yy_Ir_Inst_ExtraPl_Binary;
struct Yy_Ir_Inst_ExtraPl_Call;
struct Yy_Ir_Inst_ExtraPl_Call_Arg;
struct Yy_Ir_Inst_ExtraPl_TruncateInt;
struct Yy_Ir_Inst_ExtraPl_PromoteInt;
struct Yy_Ir_Inst_ExtraPl_IntToPtr;
struct Yy_Ir_Inst_ExtraPl_PtrCast;
struct Yy_Ir_Inst_ExtraPl_DeclValOrRef;
struct Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef;
struct Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef;
struct Yy_Ir_Inst_ExtraPl_Root;
struct Yy_Ir_Inst_ExtraPl_DeclErrorSkip;
union Yy_Ir_Inst_DEBUG_FullUnionOfPayloads;

union Yy_Ir_Inst_SmallPl;
struct Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_StructDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_FnDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_ExternFnDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_GlobalVarDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_ExternVarDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_LocalVarDecl;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_ConstInt;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_ConstCstring;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_Assign;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_BreakValue;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_CondBranch;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_Block;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_Binary;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_Call;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_TruncateInt;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_PromoteInt;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_IntToPtr;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_PtrCast;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_DeclValOrRef;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_UntypedFieldValOrRef;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_AnalyzedFieldValOrRef;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_Root;
typedef Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex     Yy_Ir_Inst_SmallPl_DeclErrorSkip;
struct Yy_Ir_Inst_SmallPl_PtrToInt;
struct Yy_Ir_Inst_SmallPl_BreakVoid;
struct Yy_Ir_Inst_SmallPl_ReturnValue;
struct Yy_Ir_Inst_SmallPl_CallBuiltinPrintUint;
struct Yy_Ir_Inst_SmallPl_Discard;
struct Yy_Ir_Inst_SmallPl_AddressOf;
struct Yy_Ir_Inst_SmallPl_TypePtrTo;
struct Yy_Ir_Inst_SmallPl_DerefRef;
struct Yy_Ir_Inst_SmallPl_DerefVal;
struct Yy_Ir_Inst_SmallPl_Unary;
struct Yy_Ir_Inst_SmallPl_LocalDeclValOrRef;
struct Yy_Ir_Inst_SmallPl_ConstBool;
struct Yy_Ir_Inst_SmallPl_FnArgDecl;
struct Yy_Ir_Inst_SmallPl_Loop;


// Struct padding is an error inside here.
#ifdef __GNUC__
# pragma GCC diagnostic push
# pragma GCC diagnostic error "-Wpadded"
#endif

/// These all have
#define YY_META_ENUM__NAME Yy_Ir_Inst_Tag
#define YY_META_ENUM__INT u8
#define YY_META_ENUM__ALL_MEMBERS                                  \
    /* See Yy_Ir_Inst_ExtraPl_Root */                                   \
    YY_META_ENUM__MEMBER(root)                                     \
    /* See Yy_Ir_Inst_ExtraPl_DeclErrorSkip */                          \
    YY_META_ENUM__MEMBER(decl_error_skip)                          \
    /* See Yy_Ir_Inst_ExtraPl_StructDecl */                             \
    YY_META_ENUM__MEMBER(struct_decl)                              \
    /* See Yy_Ir_Inst_ExtraPl_FnDecl */                                 \
    YY_META_ENUM__MEMBER(fn_decl)                                  \
    /* See Yy_Ir_Inst_SmallPl_FnArgDecl */                              \
    YY_META_ENUM__MEMBER(fn_arg_decl)                              \
    /* See Yy_Ir_Inst_ExtraPl_GlobalVarDecl */                          \
    YY_META_ENUM__MEMBER(global_var_decl)                          \
    /* See Yy_Ir_Inst_ExtraPl_ExternVarDecl */                          \
    YY_META_ENUM__MEMBER(extern_var_decl)                          \
    /* See Yy_Ir_Inst_ExtraPl_LocalVarDecl */                           \
    YY_META_ENUM__MEMBER(local_var_decl)                           \
    /* See Yy_Ir_Inst_ExtraPl_ExternFnDecl */                           \
    YY_META_ENUM__MEMBER(extern_fn_decl)                           \
    /* See Yy_Ir_Inst_ExtraPl_Block */                                  \
    YY_META_ENUM__MEMBER(block)                                    \
    /* `break_void`: Jump to the end of a `block` */                    \
    /* See Yy_Ir_Inst_SmallPl_BreakVoid */                              \
    YY_META_ENUM__MEMBER(break_void)                               \
    /* `break_value`: Jump to the end of a `block` with a value */      \
    /* See Yy_Ir_Inst_ExtraPl_BreakValue */                             \
    YY_META_ENUM__MEMBER(break_value)                              \
    /* Return from a function.*/                                        \
    /* No payload. */                                                   \
    YY_META_ENUM__MEMBER(return_void)                              \
    /* Return from a function with a value */                           \
    /* See Yy_Ir_Inst_SmallPl_ReturnValue */                            \
    YY_META_ENUM__MEMBER(return_value)                             \
    /* Placeholder that references another inst. */                     \
    /* Yy_Ir_InstOffsetSelfBackward. */                                 \
    YY_META_ENUM__MEMBER(placeholder_ref)                          \
    /* No payload. */                                                   \
    YY_META_ENUM__MEMBER(type_uintword)                            \
    YY_META_ENUM__MEMBER(type_u8)                                  \
    YY_META_ENUM__MEMBER(type_xbool)                               \
    YY_META_ENUM__MEMBER(type_xvoid)                               \
    YY_META_ENUM__MEMBER(type_xnoreturn)                           \
    /* See Yy_Ir_Inst_SmallPl_TypePtrTo */                              \
    YY_META_ENUM__MEMBER(type_ptr_to)                              \
    /* No payload. */                                                   \
    YY_META_ENUM__MEMBER(call_builtin_print_hello_world)           \
    /* See Yy_Ir_Inst_SmallPl_CallBuiltinPrintUint */                   \
    YY_META_ENUM__MEMBER(call_builtin_print_uint)                  \
    /* See Yy_Ir_Inst_ExtraPl_TruncateInt */                            \
    YY_META_ENUM__MEMBER(truncate_int)                             \
    /* See Yy_Ir_Inst_ExtraPl_PromoteInt */                             \
    YY_META_ENUM__MEMBER(promote_int)                              \
    /* See Yy_Ir_Inst_ExtraPl_PtrCast */                                \
    YY_META_ENUM__MEMBER(ptr_cast)                                 \
    /* See Yy_Ir_Inst_ExtraPl_IntToPtr */                               \
    YY_META_ENUM__MEMBER(int_to_ptr)                               \
    /* See Yy_Ir_Inst_SmallPl_PtrToInt */                               \
    YY_META_ENUM__MEMBER(ptr_to_int)                               \
    /* See Yy_Ir_Inst_ExtraPl_ConstInt. */                              \
    YY_META_ENUM__MEMBER(const_int)                                \
    /* See Yy_Ir_Inst_SmallPl_ConstBool. */                             \
    YY_META_ENUM__MEMBER(const_bool)                               \
    /* See Yy_Ir_Inst_ExtraPl_ConstCstring. */                          \
    YY_META_ENUM__MEMBER(const_cstring)                            \
    /* See Yy_Ir_Inst_SmallPl_ConstUndefined. */                        \
    YY_META_ENUM__MEMBER(const_undefined)                          \
    /* See Yy_Ir_Inst_SmallPl_Discard. */                               \
    YY_META_ENUM__MEMBER(discard)                                  \
    /* See Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef. */                  \
    YY_META_ENUM__MEMBER(untyped_field_val)                        \
    YY_META_ENUM__MEMBER(untyped_field_ref)                        \
    /* See Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef. */                 \
    YY_META_ENUM__MEMBER(analyzed_field_val)                       \
    YY_META_ENUM__MEMBER(analyzed_field_ref)                       \
    /* See Yy_Ir_Inst_ExtraPl_DeclValOrRef. */                          \
    YY_META_ENUM__MEMBER(decl_val)                                 \
    YY_META_ENUM__MEMBER(decl_ref)                                 \
    /* See Yy_Ir_Inst_SmallPl_LocalDeclValOrRef. */                     \
    YY_META_ENUM__MEMBER(local_decl_val)                           \
    YY_META_ENUM__MEMBER(local_decl_ref)                           \
    /* See Yy_Ir_Inst_ExtraPl_Call. */                                  \
    YY_META_ENUM__MEMBER(call)                                     \
    /* See Yy_Ir_Inst_SmallPl_AddressOf. */                             \
    YY_META_ENUM__MEMBER(address_of)                               \
    /* See Yy_Ir_Inst_SmallPl_DerefRef. */                              \
    YY_META_ENUM__MEMBER(deref_ref)                                \
    /* See Yy_Ir_Inst_SmallPl_DerefVal. */                              \
    YY_META_ENUM__MEMBER(deref_val)                                \
    /* See Yy_Ir_Inst_ExtraPl_Assign. */                                \
    YY_META_ENUM__MEMBER(assign)                                   \
    /* See Yy_Ir_Inst_ExtraPl_CondBranch. */                            \
    YY_META_ENUM__MEMBER(cond_branch)                              \
    /* No payload. */                                                   \
    YY_META_ENUM__MEMBER(xunreachable)                             \
    /* See Yy_Ir_Inst_SmallPl_Loop. See yy_ir_inst_loop_getLoopInnerInstIndex*/ \
    YY_META_ENUM__MEMBER(loop)                                     \
    /* See Yy_Ir_Inst_SmallPl_Unary for these. */                       \
    YY_META_ENUM__MEMBER(bits_not)                                 \
    YY_META_ENUM__MEMBER(bool_not)                                 \
    /* See Yy_Ir_Inst_ExtraPl_Binary for these. */                      \
    YY_META_ENUM__MEMBER(ptr_add_int) /* lhs: ptr, rhs: uintword */ \
    YY_META_ENUM__MEMBER(ptr_sub_int) /* lhs: ptr, rhs: uintword */ \
    YY_META_ENUM__MEMBER(ptr_sub_ptr) /* lhs: ptr, rhs: uintword */ \
    YY_META_ENUM__MEMBER(add)                                      \
    YY_META_ENUM__MEMBER(sub)                                      \
    YY_META_ENUM__MEMBER(mul)                                      \
    YY_META_ENUM__MEMBER(add_wrap)                                 \
    YY_META_ENUM__MEMBER(sub_wrap)                                 \
    YY_META_ENUM__MEMBER(mul_wrap)                                 \
    YY_META_ENUM__MEMBER(div)                                      \
    YY_META_ENUM__MEMBER(mod)                                      \
    /* TODO `rem` binary operator */                                    \
    YY_META_ENUM__MEMBER(shl)                                      \
    YY_META_ENUM__MEMBER(shr)                                      \
    YY_META_ENUM__MEMBER(bits_and)                                 \
    YY_META_ENUM__MEMBER(bits_or)                                  \
    YY_META_ENUM__MEMBER(bits_xor)                                 \
    YY_META_ENUM__MEMBER(bool_eq)                                  \
    YY_META_ENUM__MEMBER(bool_noteq)                               \
    YY_META_ENUM__MEMBER(bool_lt)                                  \
    YY_META_ENUM__MEMBER(bool_lte)                                 \
    YY_META_ENUM__MEMBER(bool_gt)                                  \
    YY_META_ENUM__MEMBER(bool_gte)                                 \
    YY_EMPTY_MACRO
#include "yy/meta/enum.hpp"

struct Yy_Ir_InstOffsetSelfForward;
struct Yy_Ir_InstOffsetSelfBackward;
struct Yy_Ir_InstOffsetFunction;

struct Yy_Ir_InstIndex {
    u32 x;
    constexpr Yy_Ir_InstIndex() = default;
    explicit constexpr Yy_Ir_InstIndex(u32 _x): x(_x) {}

    explicit operator usize() const { return x ;}
    // TODO: Maybe remove operator+(u32).
    inline auto operator-(u32 other) const -> Yy_Ir_InstIndex { return Yy_Ir_InstIndex{ x - other }; }
    inline auto operator+(u32 other) const -> Yy_Ir_InstIndex { return Yy_Ir_InstIndex{ x + other }; }
    inline auto operator+=(u32 other) -> Yy_Ir_InstIndex& { x += other; return *this; }
    NODISCARD inline auto operator<=>(Yy_Ir_InstIndex other) const -> std::strong_ordering { return x <=> other.x; }
    NODISCARD inline auto asOffsetForwardFrom(Yy_Ir_InstIndex from) const -> Yy_Ir_InstOffsetSelfForward;
    NODISCARD inline auto asOffsetBackwardFrom(Yy_Ir_InstIndex from) const -> Yy_Ir_InstOffsetSelfBackward;
    NODISCARD inline auto asOffsetFunction(Yy_Ir_InstIndex func) const -> Yy_Ir_InstOffsetFunction;

    friend constexpr auto operator==(Yy_Ir_InstIndex, Yy_Ir_InstIndex) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_InstIndex, Yy_Ir_InstIndex{ UINT32_MAX });
};

#define IOF_Fmt "%%%u"
#define IOF_Arg(foo) ((void)yy_static_assert_types_and_zero(yy_TypeOf(foo) const, Yy_Ir_InstOffsetFunction const), (foo).x)

struct Yy_Ir_InstOffsetSelfBackward {
    u32 x;
    NODISCARD inline auto abs(Yy_Ir_InstIndex from) const -> Yy_Ir_InstIndex { return Yy_Ir_InstIndex{ from.x - x }; }
    NODISCARD friend constexpr auto operator==(Yy_Ir_InstOffsetSelfBackward, Yy_Ir_InstOffsetSelfBackward) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_InstOffsetSelfBackward, Yy_Ir_InstOffsetSelfBackward{ .x = UINT32_MAX });
};

struct Yy_Ir_InstOffsetSelfForward {
    u32 x;
    NODISCARD inline auto abs(Yy_Ir_InstIndex from) const -> Yy_Ir_InstIndex { return Yy_Ir_InstIndex{ from.x + x }; }
    NODISCARD friend constexpr auto operator==(Yy_Ir_InstOffsetSelfForward, Yy_Ir_InstOffsetSelfForward) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_InstOffsetSelfForward, Yy_Ir_InstOffsetSelfForward{ .x = UINT32_MAX });
};

struct Yy_Ir_InstOffsetFunction {
    u32 x;
    NODISCARD inline auto abs(Yy_Ir_InstIndex from) const -> Yy_Ir_InstIndex { return Yy_Ir_InstIndex{ x + from.x }; }
    NODISCARD friend constexpr auto operator==(Yy_Ir_InstOffsetFunction, Yy_Ir_InstOffsetFunction) -> bool = default;
    YY_DECL_INVALID_OF_INSIDE_CLASS(Yy_Ir_InstOffsetFunction, Yy_Ir_InstOffsetFunction{ .x = UINT32_MAX });
};



inline auto Yy_Ir_InstIndex::asOffsetForwardFrom(Yy_Ir_InstIndex from) const -> Yy_Ir_InstOffsetSelfForward {
    yy_assert(x >= from.x);
    return { x - from.x };
}
inline auto Yy_Ir_InstIndex::asOffsetBackwardFrom(Yy_Ir_InstIndex from) const -> Yy_Ir_InstOffsetSelfBackward {
    yy_assert(from.x >= x);
    return { from.x - x };
}
inline auto Yy_Ir_InstIndex::asOffsetFunction(Yy_Ir_InstIndex func) const -> Yy_Ir_InstOffsetFunction {
    yy_assert(x >= func.x);
    return { x - func.x };
}


NODISCARD inline Yy_Ir_InstOffsetSelfBackward genir_instOffsetSB(Yy_Ir_InstIndex const self_abs, Yy_Ir_InstIndex const to) { return to.asOffsetBackwardFrom(self_abs); }
NODISCARD inline Yy_Ir_InstOffsetSelfForward genir_instOffsetSF(Yy_Ir_InstIndex const self_abs, Yy_Ir_InstIndex const to) { return to.asOffsetForwardFrom(self_abs); }

NODISCARD inline Yy_Ir_InstIndex genir_instAbsSB(Yy_Ir_InstIndex const self_abs, Yy_Ir_InstOffsetSelfBackward const offset) { return offset.abs(self_abs); }
NODISCARD inline Yy_Ir_InstIndex genir_instAbsSF(Yy_Ir_InstIndex const self_abs, Yy_Ir_InstOffsetSelfForward const offset) { return offset.abs(self_abs); }

NODISCARD inline Yy_Ir_InstOffsetFunction genir_instOffsetFunction(Yy_Ir_InstIndex const func, Yy_Ir_InstIndex const self_abs)
{
    return self_abs.asOffsetFunction(func);
}
NODISCARD inline Yy_Ir_InstOffsetFunction genir_instOffsetFSB(Yy_Ir_InstIndex const func, Yy_Ir_InstIndex const self_abs, Yy_Ir_InstOffsetSelfBackward const offset)
{ return genir_instAbsSB(self_abs, offset).asOffsetFunction(func); }
NODISCARD inline Yy_Ir_InstOffsetFunction genir_instOffsetFSF(Yy_Ir_InstIndex const func, Yy_Ir_InstIndex const self_abs, Yy_Ir_InstOffsetSelfForward const offset)
{ return genir_instAbsSF(self_abs, offset).asOffsetFunction(func); }
NODISCARD inline Yy_Ir_InstIndex genir_instAbsFunction(Yy_Ir_InstIndex const func, Yy_Ir_InstOffsetFunction const self) { return self.abs(func); }


struct Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex {
    Yy_Ir_ExtraIndex extra_pl_index;
};

struct Yy_Ir_Inst_SmallPl_BreakVoid {
    /// The `block/loop/cond_branch` instruction it breaks to.
    Yy_Ir_InstOffsetSelfBackward block_to_break_to;
};

struct Yy_Ir_Inst_SmallPl_CallBuiltinPrintUint {
    Yy_Ir_InstOffsetSelfBackward arg_inst;
};

struct Yy_Ir_Inst_SmallPl_Discard {
    Yy_Ir_InstOffsetSelfBackward inst_to_discard;
};

struct Yy_Ir_Inst_SmallPl_AddressOf {
    Yy_Ir_InstOffsetSelfBackward operand;
};

struct Yy_Ir_Inst_SmallPl_TypePtrTo {
    Yy_Ir_InstOffsetSelfBackward operand;
};

struct Yy_Ir_Inst_SmallPl_DerefRef {
    Yy_Ir_InstOffsetSelfBackward operand;
};

struct Yy_Ir_Inst_SmallPl_DerefVal {
    Yy_Ir_InstOffsetSelfBackward operand;
};

/// Used for unary operators like bool_not, bits_not
struct Yy_Ir_Inst_SmallPl_Unary {
    Yy_Ir_InstOffsetSelfBackward operand;
};

struct Yy_Ir_Inst_SmallPl_Loop {
    u32 break_count_upper_limit;
};

struct Yy_Ir_Inst_SmallPl_LocalDeclValOrRef {
    // TODO Use an offset from the start of the function?
    Yy_Ir_InstOffsetSelfBackward ref;
};

struct Yy_Ir_Inst_SmallPl_ConstBool {
    bool value;
};

struct Yy_Ir_Inst_SmallPl_ReturnValue {
    Yy_Ir_InstOffsetSelfBackward operand;
};

/// fn_arg_decl is an instruction generate once per function argument. These are the first
/// instructions in a function body.
struct Yy_Ir_Inst_SmallPl_FnArgDecl {
    u32 arg_idx;
};

struct Yy_Ir_Inst_SmallPl_PtrToInt {
    Yy_Ir_InstOffsetSelfBackward src_value;
};

static_assert(MetaEnum_count_int(Yy_Ir_Inst_Tag) == 73);
union Yy_Ir_Inst_SmallPl {
    u32 unused;
    Yy_Ir_InstOffsetSelfBackward placeholder_ref;
    Void xunreachable;
    Yy_Ir_Inst_SmallPl_OnlyExtraPlIndex only_extra_pl_index;
    Yy_Ir_Inst_SmallPl_StructDecl struct_decl;
    Yy_Ir_Inst_SmallPl_FnDecl fn_decl;
    Yy_Ir_Inst_SmallPl_FnArgDecl fn_arg_decl;
    Yy_Ir_Inst_SmallPl_GlobalVarDecl global_var_decl;
    Yy_Ir_Inst_SmallPl_ExternVarDecl extern_var_decl;
    Yy_Ir_Inst_SmallPl_LocalVarDecl local_var_decl;
    Yy_Ir_Inst_SmallPl_ExternFnDecl extern_fn_decl;
    Yy_Ir_Inst_SmallPl_BreakVoid break_void;
    Yy_Ir_Inst_SmallPl_ReturnValue return_value;
    Yy_Ir_Inst_SmallPl_ConstInt const_int;
    Yy_Ir_Inst_SmallPl_ConstCstring const_cstring;
    Yy_Ir_Inst_SmallPl_Discard discard;
    Yy_Ir_Inst_SmallPl_CallBuiltinPrintUint call_builtin_print_uint;
    Yy_Ir_Inst_SmallPl_TruncateInt truncate_int;
    Yy_Ir_Inst_SmallPl_PromoteInt promote_int;
    Yy_Ir_Inst_SmallPl_PtrCast ptr_cast;
    Yy_Ir_Inst_SmallPl_IntToPtr int_to_ptr;
    Yy_Ir_Inst_SmallPl_PtrToInt ptr_to_int;
    Yy_Ir_Inst_SmallPl_Call call;
    Yy_Ir_Inst_SmallPl_TypePtrTo type_ptr_to;
    Yy_Ir_Inst_SmallPl_AddressOf address_of;
    Yy_Ir_Inst_SmallPl_DerefRef deref_ref;
    Yy_Ir_Inst_SmallPl_DerefVal deref_val;
    Yy_Ir_Inst_SmallPl_Unary unary;
    Yy_Ir_Inst_SmallPl_Loop loop;
    Yy_Ir_Inst_SmallPl_Root root;
    Yy_Ir_Inst_SmallPl_DeclErrorSkip decl_error_skip;
    Yy_Ir_Inst_SmallPl_DeclValOrRef decl_val_or_ref;
    Yy_Ir_Inst_SmallPl_DeclValOrRef decl_val;
    Yy_Ir_Inst_SmallPl_DeclValOrRef decl_ref;
    Yy_Ir_Inst_SmallPl_UntypedFieldValOrRef untyped_field_val_or_ref;
    Yy_Ir_Inst_SmallPl_UntypedFieldValOrRef untyped_field_val;
    Yy_Ir_Inst_SmallPl_UntypedFieldValOrRef untyped_field_ref;
    Yy_Ir_Inst_SmallPl_AnalyzedFieldValOrRef analyzed_field_val_or_ref;
    Yy_Ir_Inst_SmallPl_AnalyzedFieldValOrRef analyzed_field_val;
    Yy_Ir_Inst_SmallPl_AnalyzedFieldValOrRef analyzed_field_ref;
    Yy_Ir_Inst_SmallPl_LocalDeclValOrRef local_decl_val_or_ref;
    Yy_Ir_Inst_SmallPl_ConstBool const_bool;
    Void const_undefined;
    Yy_Ir_Inst_SmallPl_Assign assign;
    Yy_Ir_Inst_SmallPl_BreakValue break_value;
    Yy_Ir_Inst_SmallPl_CondBranch cond_branch;
    Yy_Ir_Inst_SmallPl_Block block;
    Yy_Ir_Inst_SmallPl_Binary binary;
};
static_assert(sizeof(Yy_Ir_Inst_SmallPl) == 1*sizeof(u32) &&alignof(Yy_Ir_Inst_SmallPl) == alignof(u32), "");

/// A single instruction
struct Yy_Ir_Inst {
    Yy_Ir_Inst_Tag tag;
    u8 untyped_use_count;
    u8 _pad[2]{};
    Yy_Ir_Inst_SmallPl small_data;
};
static_assert(alignof(Yy_Ir_Inst) == alignof(Yy_Ir_Extra), "Yy_Ir_Inst can only contain u32 or smaller types");
static_assert(sizeof(Yy_Ir_Inst) == sizeof(u32) * 2);


struct Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo : ir::ExtraPlBase {
    Option<IdentifierId> name_z;
    /// The type_block is an offset from the fn_decl.
    Yy_Ir_InstOffsetSelfForward type_eval_block;
};

struct Yy_Ir_Inst_ExtraPl_FnDecl : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    /// The name of the function.
    IdentifierId name_z;
    /// A number that is `>=` the number of returns to this function.
    u32 return_count_upper_limit;
    Yy_Ir_InstOffsetSelfForward return_type_eval_block_offset;
    Yy_Ir_InstOffsetSelfForward body_block_offset;
    /// The number of arguments of this function. The arguments are extra trailing data.
    u32 arg_count;

    using TrailingElement = Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo;
    NODISCARD auto trailing_count() const -> u32 { return arg_count; }
};

struct Yy_Ir_Inst_ExtraPl_StructDecl_MemberInfo : ir::ExtraPlBase {
    Option<IdentifierId> name_z;
    /// The type_block is an offset from the struct_decl.
    Yy_Ir_InstOffsetSelfForward type_eval_block;
};

struct Yy_Ir_Inst_ExtraPl_StructDecl : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    u32 end_offset;
    IdentifierId name_z;
    /// The number of members. The members are extra trailing data.
    u32 member_count;

    using TrailingElement = Yy_Ir_Inst_ExtraPl_StructDecl_MemberInfo;
    NODISCARD auto trailing_count() const -> u32 { return member_count; }
};


/// NOTE: This code must be kept in sync with `genir_astExprXwhile`.
NODISCARD inline Yy_Ir_InstIndex yy_ir_inst_loop_getLoopInnerInstIndex(Yy_Ir_InstIndex loop_inst) { return loop_inst + 1; }

struct Yy_Ir_Inst_ExtraPl_GlobalVarDecl : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    IdentifierId name_z;
    Option<Yy_Ir_InstOffsetSelfForward> type_block;
    Yy_Ir_InstOffsetSelfForward init_block;
};

struct Yy_Ir_Inst_ExtraPl_ExternVarDecl : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    IdentifierId name_z;
    Yy_Ir_InstOffsetSelfForward type_block;
};

struct Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo : ir::ExtraPlBase {
    Option<IdentifierId> name_z;
    /// The type_block is an offset from the extern_fn_decl.
    Yy_Ir_InstOffsetSelfForward type_eval_block;
};

struct Yy_Ir_Inst_ExtraPl_ExternFnDecl : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    IdentifierId name_z;
    Yy_Ir_InstOffsetSelfForward return_type_eval_block_offset;
    /// The number of arguments of this function. The arguments are extra trailing data.
    u32 arg_count;

    using TrailingElement = Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo;
    NODISCARD auto trailing_count() const -> u32 { return arg_count; }
};

struct Yy_Ir_Inst_ExtraPl_LocalVarDecl : ir::ExtraPlBase {
    IdentifierId name_z;
    Yy_Ir_InstOffsetSelfBackward initial_value;
    Option<Yy_Ir_InstOffsetSelfForward> type_block;
};
static_assert(yy::trivial_raii<Yy_Ir_Inst_ExtraPl_LocalVarDecl>);


// TODO: This is a good candidate for extra interning. TODO: I could have two separate kinds of
// const_int extras: One for small integers that fit in 32 bits, and another for 64 bit integers.
// Most integers are probably small enough to fit in 32 bits. That would save on memory.
struct Yy_Ir_Inst_ExtraPl_ConstInt : ir::ExtraPlBase {
    ByParts<i64> integer;
};

struct Yy_Ir_Inst_ExtraPl_ConstCstring : ir::ExtraPlBase {
    // TODO: Put this in the _extra_ table, not the identifier table.
    Yy_Ir_StringSized0 string;
};


// TODO: This is a good candidate for extra interning.
struct Yy_Ir_Inst_ExtraPl_Assign : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward lhs;
    Yy_Ir_InstOffsetSelfBackward rhs;
};

struct Yy_Ir_Inst_ExtraPl_BreakValue : ir::ExtraPlBase {
    // The `block/loop/cond_branch` it jumps to
    Yy_Ir_InstOffsetSelfBackward block_to_break_to;
    // The value it returns to the block.
    Yy_Ir_InstOffsetSelfBackward value;
};

/// This instruction ends after the xelse block.
struct Yy_Ir_Inst_ExtraPl_CondBranch : ir::ExtraPlBase {
    /// `cond` comes before the `cond_branch`.
    Yy_Ir_InstOffsetSelfBackward cond;
    /// (inner inst) Always a block
    Yy_Ir_InstOffsetSelfForward then;
    /// (inner inst) Always a block
    Yy_Ir_InstOffsetSelfForward xelse;
    /// An upper limit on the number of instructions that `break` to here.
    u32 break_count_upper_limit;
};

struct Yy_Ir_Inst_ExtraPl_Block : ir::ExtraPlBase {
    /// An offset to the end of the block. If you add it to the index of the block instruction,
    /// you'll get an index to the first instruction that isn't inside the block.
    u32 end_offset;
    /// An upper limit on the number of instructions that `break` to here.
    u32 break_count_upper_limit;
};


/// Payload for binary operators
// TODO: This is a good candidate for extra interning.
struct Yy_Ir_Inst_ExtraPl_Binary : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward lhs;
    Yy_Ir_InstOffsetSelfBackward rhs;
};

struct Yy_Ir_Inst_ExtraPl_Call_Arg : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward inst;
};

// TODO: This is a good candidate for extra interning.
struct Yy_Ir_Inst_ExtraPl_Call : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward callee;
    /// The number of arguments for this call. The arguments are extra trailing data.
    u32 arg_count;

    using TrailingElement = Yy_Ir_Inst_ExtraPl_Call_Arg;
    NODISCARD auto trailing_count() const -> u32 { return arg_count; }
};

struct Yy_Ir_Inst_ExtraPl_TruncateInt : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfForward dest_type_block;
    Yy_Ir_InstOffsetSelfBackward src_value;
};

struct Yy_Ir_Inst_ExtraPl_PromoteInt : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfForward dest_type_block;
    Yy_Ir_InstOffsetSelfBackward src_value;
};

struct Yy_Ir_Inst_ExtraPl_IntToPtr : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfForward dest_type_block;
    Yy_Ir_InstOffsetSelfBackward src_value;
};

struct Yy_Ir_Inst_ExtraPl_PtrCast : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfForward dest_type_block;
    Yy_Ir_InstOffsetSelfBackward src_value;
};

struct Yy_Ir_Inst_ExtraPl_DeclValOrRef : ir::ExtraPlBase {
    ByParts<DeclId> ref_decl;
};

struct Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward container;
    IdentifierId field;
};

struct Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef : ir::ExtraPlBase {
    Yy_Ir_InstOffsetSelfBackward container;
    u32 field_idx;
};

struct Yy_Ir_Inst_ExtraPl_Root : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    u32 end_offset;
};

struct Yy_Ir_Inst_ExtraPl_DeclErrorSkip : ir::ExtraPlBase {
    ByParts<DeclId> decl_id;
    u32 end_offset;
};


/// Make a union of all full payloads to make sure none of them contain pointers.
union Yy_Ir_Inst_DEBUG_FullUnionOfPayloads {
    Yy_Ir_Inst_ExtraPl_StructDecl struct_decl;
    Yy_Ir_Inst_ExtraPl_StructDecl_MemberInfo struct_decl_member_info;
    Yy_Ir_Inst_ExtraPl_FnDecl fn_decl;
    Yy_Ir_Inst_ExtraPl_FnDecl_ArgInfo fn_decl_arg_info;
    Yy_Ir_Inst_ExtraPl_ExternFnDecl extern_fn_decl;
    Yy_Ir_Inst_ExtraPl_ExternFnDecl_ArgInfo extern_fn_decl_arg_info;
    Yy_Ir_Inst_ExtraPl_GlobalVarDecl global_var_decl;
    Yy_Ir_Inst_ExtraPl_ExternVarDecl extern_var_decl;
    Yy_Ir_Inst_ExtraPl_LocalVarDecl local_var_decl;
    Yy_Ir_Inst_ExtraPl_ConstInt const_int;
    Yy_Ir_Inst_ExtraPl_ConstCstring const_cstring;
    Yy_Ir_Inst_ExtraPl_Assign assign;
    Yy_Ir_Inst_ExtraPl_BreakValue break_value;
    Yy_Ir_Inst_ExtraPl_CondBranch cond_branch;
    Yy_Ir_Inst_ExtraPl_Block block;
    Yy_Ir_Inst_ExtraPl_Binary binary;
    Yy_Ir_Inst_ExtraPl_Call call;
    Yy_Ir_Inst_ExtraPl_Call_Arg call_arg;
    Yy_Ir_Inst_ExtraPl_TruncateInt truncate_int;
    Yy_Ir_Inst_ExtraPl_PromoteInt promote_int;
    Yy_Ir_Inst_ExtraPl_IntToPtr int_to_ptr;
    Yy_Ir_Inst_ExtraPl_PtrCast ptr_cast;
    Yy_Ir_Inst_ExtraPl_DeclValOrRef decl_val_or_ref;
    Yy_Ir_Inst_ExtraPl_DeclValOrRef decl_val;
    Yy_Ir_Inst_ExtraPl_DeclValOrRef decl_ref;
    Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef untyped_field_val_or_ref;
    Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef untyped_field_val;
    Yy_Ir_Inst_ExtraPl_UntypedFieldValOrRef untyped_field_ref;
    Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef analyzed_field_val_or_ref;
    Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef analyzed_field_val;
    Yy_Ir_Inst_ExtraPl_AnalyzedFieldValOrRef analyzed_field_ref;
    Yy_Ir_Inst_ExtraPl_Root root;
    Yy_Ir_Inst_ExtraPl_DeclErrorSkip decl_error_skip;
};
static_assert(yy::trivial_raii<Yy_Ir_Inst_DEBUG_FullUnionOfPayloads>);
static_assert(MetaEnum_count_int(Yy_Ir_Inst_Tag) == 73);
static_assert(alignof(Yy_Ir_Inst_DEBUG_FullUnionOfPayloads) == alignof(Yy_Ir_Extra), "Yy_Ir_Inst can only contain u32 or smaller types");

#ifdef __GNUC__
# pragma GCC diagnostic pop // struct padding error
#endif

NODISCARD inline bool
instMayBeImplicitEndOfBlock(Yy_Ir_Inst_Tag inst_tag) {
    return inst_tag == Yy_Ir_Inst_Tag__break_void || inst_tag == Yy_Ir_Inst_Tag__break_value || inst_tag == Yy_Ir_Inst_Tag__return_void;
}

#endif//___INCLUDE_GUARD__z1qiwu1hLcqg19KZIaoXa14og52nXC8DDH6hFN55
