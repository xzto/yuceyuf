#!/usr/bin/env python3
# vim: set sw=4 et:
# pylint: disable=W0511
# pylint: disable=unused-import,too-few-public-methods,invalid-name,global-statement
# pylint: disable=consider-using-f-string,line-too-long,multiple-statements,use-dict-literal
# pylint: disable=too-many-arguments,trailing-newlines,too-many-branches,using-constant-test
# pylint: disable=C0114,C0115,C0116
# pylint: disable=too-many-statements,too-many-return-statements

# TODO: Rewrite this in C or in yuceyuf.

import sys
import io
import difflib
from typing import List, NoReturn, Final, TypeAlias, Dict, Optional, Tuple, Any
from subprocess import Popen, PIPE, check_call, DEVNULL
import subprocess
import os.path
import tempfile
import multiprocessing

TMPDIRNAME = None

class TestEntry:
    filename: str

ARG0: Final[str] = sys.argv[0]
ROOT_DIR: Final[str] = os.path.dirname(ARG0)

TESTS_DIR: Final[str] = 'tests'
CURRENT_RECORDED_CASE_FILE_VERSION: Final[str] = '0'
verbose_tests: bool = False


USAGE_TEXT = f"""
Usage: {ARG0} [command] [args]

Commands:
    test [FILE]...              Test one or more files
    record FILE...              Automatically record a test case

If no command is specified, it will test the {ROOT_DIR}/{TESTS_DIR} directory.
""".strip('\n')

def usage(exit_code: int) -> NoReturn:
    print(f"{USAGE_TEXT}")
    sys.exit(exit_code)

def die(exit_code: int, rest: List[str]) -> NoReturn:
    for x in rest:
        print(x, file=sys.stderr)
    sys.exit(exit_code)


def todo() -> NoReturn:
    assert False, "TODO"

def unreachable() -> NoReturn:
    assert False, "unreachable"

iota_value = 0
def iota() -> int:
    global iota_value
    result = iota_value
    iota_value += 1
    return result

def iota_reset() -> int:
    global iota_value
    result = iota_value
    iota_value = 0
    return result

iota_reset()
Command: TypeAlias = int
Command__test  : Final[Command] = iota()
Command__record: Final[Command] = iota()
Command_COUNT  : Final[Command] = iota_reset()
assert Command_COUNT == 2

Command_NAMES: Final[Dict[Command, str]] = {
    Command__test: 'test',
    Command__record: 'record',
}
assert len(Command_NAMES) == Command_COUNT

def parse_args() -> Tuple[Command, List[str]]:
    args = sys.argv
    if len(args) < 2:
        return Command__test, []
    if args[1] == '-h' or args[1] == '--help':
        print(USAGE_TEXT)
        sys.exit(0)
    if args[1] == Command_NAMES[Command__test]:
        return Command__test, args[2:]
    if args[1] == Command_NAMES[Command__record]:
        return Command__record, args[2:]
    die(1, ["{}: error: unknown command {}".format(ARG0, args[1]), USAGE_TEXT])

def diff_contents(expected, got) -> str:
    return '\n'.join(difflib.unified_diff(expected.split('\n'), got.split('\n'), "expected", "got", lineterm=''))

def read_inline_test_options(filename: str) -> dict:
    result = {}
    with open(filename, mode='r', encoding='utf-8') as f:
        line_count = 0
        while line := f.readline():
            line_count += 1
            match_start = 'yuceyuf-test-option '
            idx = line.find(match_start)
            if idx == -1:
                continue
            rest = line[idx + len(match_start):]
            option_split = rest.strip().split(' ', maxsplit=1)
            option_key = option_split[0].strip()
            option_value = option_split[1].strip() if len(option_split) > 1 else ''
            match option_key:
                case 'run_args':
                    option_value = option_value.split()
                case 'kind':
                    if option_value not in ('run', 'compile'):
                        die(1, [f"{ARG0}:{line_count}: error: bad yuceyuf-test-option '{option_key}' value: '{option_value}'"])
                case _:
                    die(1, [f"{ARG0}:{line_count}: error: unknown yuceyuf-test-option '{option_key}'"])
            result[option_key] = option_value
    return result

def read_recorded_case(filename: str, inline_test_options: Dict[str, Any]) -> Tuple[Dict[str, Any], bool]:
    contents = None
    try:
        with open(filename, mode='rb') as fd:
            contents = fd.read()
    except OSError as e:
        die(1, ["{}: error: failed to read recorded case file '{}'".format(ARG0, filename), "{}".format(e)])
    head, body = contents.split(b'\n\n', maxsplit=1)
    result: Dict[str, Any] = dict()
    for line in head.decode('utf-8').split('\n'):
        line_split = line.split(' ', maxsplit=1)
        key = line_split[0].strip()
        value = line_split[1].strip() if len(line_split) > 1 else ''
        match key:
            case 'version':
                if value != CURRENT_RECORDED_CASE_FILE_VERSION:
                    die(1, ["{}: error: wrong version in recorded test case '{}'".format(ARG0, filename)])
            case 'kind':
                if 'kind' in inline_test_options and inline_test_options['kind'] != value:
                    if verbose_tests:
                        print("{}: error: 'kind' option mismatch from .yuceyuf and .yuceyuf_compiler_test_case files: '{}' vs '{}'".format(
                            filename,
                            inline_test_options['kind'],
                            value
                        ), file=sys.stderr)
                    return None, False
                if value not in ('run', 'compile'):
                    die(1, ["{}: error: invalid '{}' value: '{}'".format(ARG0, key, value)])
                result[key] = value
            case 'exit_code':
                result[key] = int(value)
            case 'stdout_offset' | 'stdout_size' | 'stderr_offset' | 'stderr_size':
                result[key] = int(value)
            case _:
                die(1, ["{}: error: unknown key in recorded test case '{}': '{}'".format(ARG0, filename, key)])
    result['stdout_contents'] = body[result['stdout_offset']:][:result['stdout_size']]
    result['stderr_contents'] = body[result['stderr_offset']:][:result['stderr_size']]
    return result, True

def write_recorded_case(filename: str, kind: str, stdout: bytes, stderr: bytes, exit_code: int):
    recorded_case: Dict[str, Any] = dict()
    body = bytearray()
    recorded_case['version'] = CURRENT_RECORDED_CASE_FILE_VERSION

    recorded_case['kind'] = kind
    recorded_case['exit_code'] = exit_code

    body += b'----- STDOUT BEGIN -----\n'
    recorded_case['stdout_offset'] = len(body)
    body += stdout
    recorded_case['stdout_size'] = len(stdout)
    body += b'\n----- STDOUT END -----\n'

    body += b'\n----- STDERR BEGIN -----\n'
    recorded_case['stderr_offset'] = len(body)
    body += stderr
    recorded_case['stderr_size'] = len(stderr)
    body += b'\n----- STDERR END -----\n'

    with open(filename, 'wb') as fd:
        for k, v in recorded_case.items():
            fd.write("{} {}\n".format(k, v).encode('utf-8'))
        fd.write(b"\n")
        fd.write(body)

def get_pid():
    return multiprocessing.current_process().pid

def COMPILED_BINARY() -> str:
    return 'test_out-{}'.format(get_pid())
def TEST_OUT_ASM() -> str:
    return 'test_out-{}.asm'.format(get_pid())

def do_link_sh() -> int:
    return subprocess.call([
        os.path.join(ROOT_DIR, "link.sh"),
        '--asm', os.path.join(TMPDIRNAME, TEST_OUT_ASM()),
        '--out', os.path.join(TMPDIRNAME, COMPILED_BINARY())
    ], stdout=DEVNULL, stderr=DEVNULL)

# Return (stdout, stderr, exit_code)
def compile_file(filename: str) -> Tuple[bytes, bytes, int]:
    with Popen(["./yuceyuf", "ir-test", filename, '--asm-out', os.path.join(TMPDIRNAME, TEST_OUT_ASM())], stdout=PIPE, stderr=PIPE) as p:
        stdout, stderr = p.communicate()
        exit_code = p.wait()
        if exit_code == 0:
            exit_code = do_link_sh()
        return stdout, stderr, exit_code

def run_compiled_binary(args: List[str]) -> Tuple[bytes, bytes, int]:
    with Popen([os.path.join(TMPDIRNAME, COMPILED_BINARY())] + args, stdout=PIPE, stderr=PIPE) as p:
        stdout, stderr, = p.communicate()
        exit_code = p.wait()
        return stdout, stderr, exit_code

def yuceyuf_file_to_recorded_case_filename(filename_dot_yuceyuf: str) -> str:
    if os.path.isdir(filename_dot_yuceyuf):
        die(1, ["{}: error: the file is a directory: '{}'".format(ARG0, filename_dot_yuceyuf)])
    base, ext_suffix = os.path.splitext(filename_dot_yuceyuf)
    if ext_suffix != '.yuceyuf':
        die(1, ["{}: error: the file must have the '.yuceyuf' extension".format(ARG0)])
    recorded_filename: Final[str] = base + '.yuceyuf_compiler_test_case'
    return recorded_filename

iota_reset()
TestOneFileResult: TypeAlias = int
TestOneFileResult__no_recorded_case  : Final[TestOneFileResult] = iota()
TestOneFileResult__failure           : Final[TestOneFileResult] = iota()
TestOneFileResult__success           : Final[TestOneFileResult] = iota()
TestOneFileResult_COUNT              : Final[TestOneFileResult] = iota_reset()
def test_one_file(filename: str) -> TestOneFileResult:
    recorded_filename: Final[str] = yuceyuf_file_to_recorded_case_filename(filename)
    if not os.path.isfile(recorded_filename):
        if verbose_tests: print("Test with no recorded case: '{}'".format(filename))
        return TestOneFileResult__no_recorded_case
    inline_test_options = read_inline_test_options(filename)
    recorded_case, ok = read_recorded_case(recorded_filename, inline_test_options)
    if not ok:
        return TestOneFileResult__failure

    compilation_stdout, compilation_stderr, compilation_exit_code = compile_file(filename)

    if recorded_case['kind'] == 'compile':
        failed = False
        if compilation_exit_code != recorded_case['exit_code']:
            failed = True
            if verbose_tests: print("Test failed: '{}': expected exit code {}, got {}".format(
                filename, recorded_case['exit_code'], compilation_exit_code), file=sys.stderr)
        if compilation_stdout != recorded_case['stdout_contents']:
            failed = True
            if verbose_tests:
                print("Test failed: '{}': mismatched stdout\n{}\n".format(
                    filename,
                    diff_contents(recorded_case['stdout_contents'].decode('utf-8'), compilation_stdout.decode('utf-8'))
                ), file=sys.stderr)
        if compilation_stderr != recorded_case['stderr_contents']:
            failed = True
            if verbose_tests:
                print("Test failed: '{}': mismatched stderr\n{}\n".format(
                    filename,
                    diff_contents(recorded_case['stderr_contents'].decode('utf-8'), compilation_stderr.decode('utf-8'))
                ), file=sys.stderr)
        if failed:
            return TestOneFileResult__failure
        if verbose_tests:
            print("Test success: '{}'".format(filename))
        return TestOneFileResult__success

    assert recorded_case['kind'] == 'run'

    if compilation_exit_code != 0:
        print("Test failed: '{}': expected to run but failed to compile".format(filename), file=sys.stderr)
        if verbose_tests: print("---- stdout --\n{}\n\n---- stderr --\n{}\n\n---- exit_code --\n{}\n".format(
            compilation_stdout.decode('utf-8'),
            compilation_stderr.decode('utf-8'),
            compilation_exit_code
        ), file=sys.stderr)
        return TestOneFileResult__failure

    run_stdout, run_stderr, run_exit_code = run_compiled_binary(inline_test_options.get('run_args') or [])

    failed = False
    if run_exit_code != recorded_case['exit_code']:
        failed = True
        if verbose_tests: print("Test failed: '{}': expected exit code {}, got {}".format(
            filename, recorded_case['exit_code'], run_exit_code), file=sys.stderr)
    if run_stdout != recorded_case['stdout_contents']:
        failed = True
        if verbose_tests:
            print("Test failed: '{}': mismatched stdout\n{}\n".format(
                filename,
                diff_contents(recorded_case['stdout_contents'].decode('utf-8'), run_stdout.decode('utf-8'))
            ), file=sys.stderr)
    if run_stderr != recorded_case['stderr_contents']:
        failed = True
        if verbose_tests:
            print("Test failed: '{}': mismatched stderr\n{}\n".format(
                filename,
                diff_contents(recorded_case['stderr_contents'].decode('utf-8'), run_stderr.decode('utf-8'))
            ), file=sys.stderr)
    if failed: return TestOneFileResult__failure

    if verbose_tests: print("Test success: '{}'".format(filename))

    return TestOneFileResult__success

def record_one_file(filename: str) -> None:
    recorded_filename: Final[str] = yuceyuf_file_to_recorded_case_filename(filename)
    inline_test_options = read_inline_test_options(filename)

    compilation_stdout, compilation_stderr, compilation_exit_code = compile_file(filename)
    if compilation_exit_code != 0 and inline_test_options.get('kind') == 'run':
        die(1, [f"{ARG0}: error: '{filename}' wants to run, but failed to compile"])
    if compilation_exit_code != 0 or inline_test_options.get('kind') == 'compile':
        write_recorded_case(recorded_filename, 'compile', compilation_stdout, compilation_stderr, compilation_exit_code)
        return
    run_stdout, run_stderr, run_exit_code = run_compiled_binary(inline_test_options.get('run_args') or [])
    write_recorded_case(recorded_filename, 'run', run_stdout, run_stderr, run_exit_code)

def collect_all_yuceyuf_files(file_list: List[str], the_dir: str) -> None:
    for root, dirs, files in os.walk(the_dir):
        _ = dirs
        for f in files:
            f = os.path.join(root, f)
            ext = os.path.splitext(f)[1]
            if ext == '.yuceyuf':
                file_list.append(f)

class TestStats:
    count_no_recorded_case: int = 0
    count_success: int = 0
    count_failure: int = 0
    count_total: int = 0
    list_of_failures: List[str] = []

    def check(self, filename: str, tof: TestOneFileResult) -> None:
        self.count_total += 1
        if tof == TestOneFileResult__no_recorded_case:
            self.count_no_recorded_case += 1
        elif tof == TestOneFileResult__success:
            self.count_success += 1
        elif tof == TestOneFileResult__failure:
            self.count_failure += 1
            self.list_of_failures.append(filename)
        else: unreachable()

def cmd_test(args: List[str]) -> None:
    global verbose_tests
    file_list: List[str] = []
    got_any_file_args = False
    for f in args:
        if f == '--verbose':
            verbose_tests = True
        elif f.startswith('-'):
            die(1, ["{}: error: unknown argument '{}'".format(ARG0, f)])
        elif os.path.isdir(f):
            got_any_file_args = True
            collect_all_yuceyuf_files(file_list, f)
        else:
            got_any_file_args = True
            file_list.append(f)
    if not got_any_file_args:
        collect_all_yuceyuf_files(file_list, os.path.join(ROOT_DIR, TESTS_DIR))

    stats = TestStats()
    if not verbose_tests:
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            for f, tof in zip(file_list, pool.map(test_one_file, file_list)):
                stats.check(f, tof)
    else:
        for f in file_list:
            stats.check(f, test_one_file(f))
    print("Tests end")
    print("No recorded case: {}".format(stats.count_no_recorded_case))
    print("Success: {}".format(stats.count_success))
    print("Failure: {}".format(stats.count_failure))
    print("Total  : {}".format(stats.count_total))
    for f in stats.list_of_failures:
        print("Failed test: {}".format(f))

def cmd_record(args: List[str]) -> None:
    file_list: List[str] = []
    got_any_file_args = False
    for a in args:
        if a.startswith('-'):
            die(1, ["{}: error: unknown argument '{}'".format(ARG0, a)])
        else:
            got_any_file_args = True
            file_list.append(a)
    if not got_any_file_args:
        die(1, ["{}: error: the record command expects at least one file".format(ARG0)])

    for f in file_list:
        record_one_file(f)

def main() -> NoReturn:
    global TMPDIRNAME
    with tempfile.TemporaryDirectory() as tmpdirname:
        TMPDIRNAME = tmpdirname
        command, rest = parse_args()
        if command == Command__test:
            cmd_test(rest)
        elif command == Command__record:
            cmd_record(rest)
        else: unreachable()
        sys.exit(0)

if __name__ == "__main__":
    main()
