bits 64
segment .text
extern main

global _start
_start:
    xor eax, eax
    mov rdi, QWORD [rsp]
    lea rsi, [rsp + 8]
    lea rdx, [rsi + rdi*8 + 8]
    ; mov rdi, 10
    ; mov rsi, 20
    ; mov rdx, 30
    call main
    mov rdi, rax
    call yuceyuf_sys_exit_group
    ud2

# Up to 5 arguments are allowed
global yuceyuf_syscall5
yuceyuf_syscall5:
    mov rax, rdi
    mov rdi, rsi
    mov rsi, rdx
    mov rdx, rcx
    mov rcx, r8
    mov r8, r9
    syscall
    ret

global yuceyuf_sys_write
yuceyuf_sys_write:
    mov rax, 1
    syscall
    ret

global yuceyuf_sys_exit_group
yuceyuf_sys_exit_group:
    mov rax, 231
    syscall
    ret

global yuceyuf_builtin_print_hello_world
yuceyuf_builtin_print_hello_world:
    push rdi
    push rsi
    push rdx
    mov rdi, 1
    mov rsi, hello_ptr
    mov rdx, 13
    call yuceyuf_sys_write
    pop rdx
    pop rsi
    pop rdi
    ret

segment .rodata
hello_ptr: db "Hello, world", 10


segment .data
global yuceyuf_var_989898
yuceyuf_var_989898: dq 989898

segment .bss
global yuceyuf_some_memory
yuceyuf_some_memory: resb 0x10000
