// I couldn't get gcc to output yasm assembly syntax, so I'm including this file here for now.

typedef unsigned char u8;
typedef unsigned long u64;
typedef unsigned long usize;
typedef signed long isize;
typedef signed int i32;

extern isize yuceyuf_sys_write(i32 fd, void const *ptr, usize len);

void yuceyuf_builtin_print_uint(u64 x);
void yuceyuf_builtin_print_uint(u64 x)
{
    u8 buf[32];
    usize buf_len = 0;
    buf[sizeof(buf) - (++buf_len)] = '\n';
    if (x == 0) {
        buf[sizeof(buf) - (++buf_len)] = '0';
    } else {
        while (x != 0) {
            u8 const ch = '0' + (x % 10);
            buf[sizeof(buf) - (++buf_len)] = ch;
            x /= 10;
        }
    }
    yuceyuf_sys_write(1, buf + sizeof(buf) - buf_len, buf_len);
}

void _yuceyuf_memcpy(u8 *a, u8 const *b, usize len);
void _yuceyuf_memcpy(u8 *a, u8 const *b, usize len)
{
    for (usize i = 0; i < len; i++) {
        a[i] = b[i];
    }
}

void _yuceyuf_memset(u8 *a, u8 byte, usize len);
void _yuceyuf_memset(u8 *a, u8 byte, usize len)
{
    for (usize i = 0; i < len; i++) {
        a[i] = byte;
    }
}
